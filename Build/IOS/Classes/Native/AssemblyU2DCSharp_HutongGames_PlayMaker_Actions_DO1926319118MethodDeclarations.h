﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialColor
struct DOTweenMaterialColor_t1926319118;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialColor::.ctor()
extern "C"  void DOTweenMaterialColor__ctor_m3065562576 (DOTweenMaterialColor_t1926319118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialColor::Reset()
extern "C"  void DOTweenMaterialColor_Reset_m1992946615 (DOTweenMaterialColor_t1926319118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialColor::OnEnter()
extern "C"  void DOTweenMaterialColor_OnEnter_m3466209487 (DOTweenMaterialColor_t1926319118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialColor::<OnEnter>m__52()
extern "C"  void DOTweenMaterialColor_U3COnEnterU3Em__52_m1216155303 (DOTweenMaterialColor_t1926319118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialColor::<OnEnter>m__53()
extern "C"  void DOTweenMaterialColor_U3COnEnterU3Em__53_m1357317804 (DOTweenMaterialColor_t1926319118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
