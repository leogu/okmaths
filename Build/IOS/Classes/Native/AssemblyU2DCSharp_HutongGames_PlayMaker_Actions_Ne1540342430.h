﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com774104990.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkDestroy
struct  NetworkDestroy_t1540342430  : public ComponentAction_1_t774104990
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.NetworkDestroy::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkDestroy::removeRPCs
	FsmBool_t664485696 * ___removeRPCs_14;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(NetworkDestroy_t1540342430, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_removeRPCs_14() { return static_cast<int32_t>(offsetof(NetworkDestroy_t1540342430, ___removeRPCs_14)); }
	inline FsmBool_t664485696 * get_removeRPCs_14() const { return ___removeRPCs_14; }
	inline FsmBool_t664485696 ** get_address_of_removeRPCs_14() { return &___removeRPCs_14; }
	inline void set_removeRPCs_14(FsmBool_t664485696 * value)
	{
		___removeRPCs_14 = value;
		Il2CppCodeGenWriteBarrier(&___removeRPCs_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
