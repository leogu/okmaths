﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateFloatV2
struct AnimateFloatV2_t2875542565;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::.ctor()
extern "C"  void AnimateFloatV2__ctor_m877088141 (AnimateFloatV2_t2875542565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::Reset()
extern "C"  void AnimateFloatV2_Reset_m693693038 (AnimateFloatV2_t2875542565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnEnter()
extern "C"  void AnimateFloatV2_OnEnter_m3372070260 (AnimateFloatV2_t2875542565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnExit()
extern "C"  void AnimateFloatV2_OnExit_m4161856776 (AnimateFloatV2_t2875542565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloatV2::OnUpdate()
extern "C"  void AnimateFloatV2_OnUpdate_m3129108761 (AnimateFloatV2_t2875542565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
