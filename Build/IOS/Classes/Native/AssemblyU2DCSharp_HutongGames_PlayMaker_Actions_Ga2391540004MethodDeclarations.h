﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectHasChildren
struct GameObjectHasChildren_t2391540004;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::.ctor()
extern "C"  void GameObjectHasChildren__ctor_m2407248282 (GameObjectHasChildren_t2391540004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::Reset()
extern "C"  void GameObjectHasChildren_Reset_m2828361629 (GameObjectHasChildren_t2391540004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnEnter()
extern "C"  void GameObjectHasChildren_OnEnter_m3125058701 (GameObjectHasChildren_t2391540004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::OnUpdate()
extern "C"  void GameObjectHasChildren_OnUpdate_m993025204 (GameObjectHasChildren_t2391540004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectHasChildren::DoHasChildren()
extern "C"  void GameObjectHasChildren_DoHasChildren_m3732371910 (GameObjectHasChildren_t2391540004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
