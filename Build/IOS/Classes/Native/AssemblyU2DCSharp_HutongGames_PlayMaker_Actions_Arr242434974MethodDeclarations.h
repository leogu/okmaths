﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects
struct ArrayListSendEventToGameObjects_t242434974;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::.ctor()
extern "C"  void ArrayListSendEventToGameObjects__ctor_m4274926944 (ArrayListSendEventToGameObjects_t242434974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::Reset()
extern "C"  void ArrayListSendEventToGameObjects_Reset_m1557771859 (ArrayListSendEventToGameObjects_t242434974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::OnEnter()
extern "C"  void ArrayListSendEventToGameObjects_OnEnter_m4194853891 (ArrayListSendEventToGameObjects_t242434974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::DoSendEvent()
extern "C"  void ArrayListSendEventToGameObjects_DoSendEvent_m974257073 (ArrayListSendEventToGameObjects_t242434974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::sendEventToGO(UnityEngine.GameObject)
extern "C"  void ArrayListSendEventToGameObjects_sendEventToGO_m1665629493 (ArrayListSendEventToGameObjects_t242434974 * __this, GameObject_t1756533147 * ____go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
