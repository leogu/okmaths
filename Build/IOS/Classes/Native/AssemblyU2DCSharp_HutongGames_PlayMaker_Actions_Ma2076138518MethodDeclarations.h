﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetNextHostData
struct MasterServerGetNextHostData_t2076138518;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::.ctor()
extern "C"  void MasterServerGetNextHostData__ctor_m121052850 (MasterServerGetNextHostData_t2076138518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::Reset()
extern "C"  void MasterServerGetNextHostData_Reset_m1474169263 (MasterServerGetNextHostData_t2076138518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::OnEnter()
extern "C"  void MasterServerGetNextHostData_OnEnter_m3119625431 (MasterServerGetNextHostData_t2076138518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetNextHostData::DoGetNextHostData()
extern "C"  void MasterServerGetNextHostData_DoGetNextHostData_m3981288922 (MasterServerGetNextHostData_t2076138518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
