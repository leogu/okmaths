﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.ShowOptions
struct ShowOptions_t1720889473;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ShowOptions::.ctor()
extern "C"  void ShowOptions__ctor_m4014978381 (ShowOptions_t1720889473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
