﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue
struct uGuiSliderSetNormalizedValue_t4221362059;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::.ctor()
extern "C"  void uGuiSliderSetNormalizedValue__ctor_m3431091713 (uGuiSliderSetNormalizedValue_t4221362059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::Reset()
extern "C"  void uGuiSliderSetNormalizedValue_Reset_m1712066136 (uGuiSliderSetNormalizedValue_t4221362059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::OnEnter()
extern "C"  void uGuiSliderSetNormalizedValue_OnEnter_m1451080946 (uGuiSliderSetNormalizedValue_t4221362059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::OnUpdate()
extern "C"  void uGuiSliderSetNormalizedValue_OnUpdate_m740494581 (uGuiSliderSetNormalizedValue_t4221362059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::DoSetValue()
extern "C"  void uGuiSliderSetNormalizedValue_DoSetValue_m570851927 (uGuiSliderSetNormalizedValue_t4221362059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::OnExit()
extern "C"  void uGuiSliderSetNormalizedValue_OnExit_m1673874422 (uGuiSliderSetNormalizedValue_t4221362059 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
