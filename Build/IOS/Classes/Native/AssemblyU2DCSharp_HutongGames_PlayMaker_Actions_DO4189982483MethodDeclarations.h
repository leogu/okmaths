﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLightShadowStrength
struct DOTweenLightShadowStrength_t4189982483;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLightShadowStrength::.ctor()
extern "C"  void DOTweenLightShadowStrength__ctor_m3319303577 (DOTweenLightShadowStrength_t4189982483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightShadowStrength::Reset()
extern "C"  void DOTweenLightShadowStrength_Reset_m983953440 (DOTweenLightShadowStrength_t4189982483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightShadowStrength::OnEnter()
extern "C"  void DOTweenLightShadowStrength_OnEnter_m503695674 (DOTweenLightShadowStrength_t4189982483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightShadowStrength::<OnEnter>m__4C()
extern "C"  void DOTweenLightShadowStrength_U3COnEnterU3Em__4C_m3675697772 (DOTweenLightShadowStrength_t4189982483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightShadowStrength::<OnEnter>m__4D()
extern "C"  void DOTweenLightShadowStrength_U3COnEnterU3Em__4D_m1773586447 (DOTweenLightShadowStrength_t4189982483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
