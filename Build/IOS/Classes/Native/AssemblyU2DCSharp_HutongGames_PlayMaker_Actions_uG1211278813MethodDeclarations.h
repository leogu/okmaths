﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetText
struct uGuiInputFieldSetText_t1211278813;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetText::.ctor()
extern "C"  void uGuiInputFieldSetText__ctor_m3252195435 (uGuiInputFieldSetText_t1211278813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetText::Reset()
extern "C"  void uGuiInputFieldSetText_Reset_m1980327382 (uGuiInputFieldSetText_t1211278813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetText::OnEnter()
extern "C"  void uGuiInputFieldSetText_OnEnter_m2451239464 (uGuiInputFieldSetText_t1211278813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetText::OnUpdate()
extern "C"  void uGuiInputFieldSetText_OnUpdate_m1798927355 (uGuiInputFieldSetText_t1211278813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetText::DoSetTextValue()
extern "C"  void uGuiInputFieldSetText_DoSetTextValue_m525420376 (uGuiInputFieldSetText_t1211278813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetText::OnExit()
extern "C"  void uGuiInputFieldSetText_OnExit_m3984655072 (uGuiInputFieldSetText_t1211278813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
