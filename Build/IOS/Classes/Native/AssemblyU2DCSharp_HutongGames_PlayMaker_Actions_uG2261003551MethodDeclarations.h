﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn
struct uGuiToggleSetIsOn_t2261003551;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::.ctor()
extern "C"  void uGuiToggleSetIsOn__ctor_m1306992479 (uGuiToggleSetIsOn_t2261003551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::Reset()
extern "C"  void uGuiToggleSetIsOn_Reset_m3763550100 (uGuiToggleSetIsOn_t2261003551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::OnEnter()
extern "C"  void uGuiToggleSetIsOn_OnEnter_m945829066 (uGuiToggleSetIsOn_t2261003551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::DoSetValue()
extern "C"  void uGuiToggleSetIsOn_DoSetValue_m3370496121 (uGuiToggleSetIsOn_t2261003551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::OnExit()
extern "C"  void uGuiToggleSetIsOn_OnExit_m3332735746 (uGuiToggleSetIsOn_t2261003551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
