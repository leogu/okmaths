﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties
struct NetworkGetOnFailedToConnectProperties_t4292706780;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::.ctor()
extern "C"  void NetworkGetOnFailedToConnectProperties__ctor_m2345483276 (NetworkGetOnFailedToConnectProperties_t4292706780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::Reset()
extern "C"  void NetworkGetOnFailedToConnectProperties_Reset_m3757830193 (NetworkGetOnFailedToConnectProperties_t4292706780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::OnEnter()
extern "C"  void NetworkGetOnFailedToConnectProperties_OnEnter_m1629164073 (NetworkGetOnFailedToConnectProperties_t4292706780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetOnFailedToConnectProperties::doGetNetworkErrorInfo()
extern "C"  void NetworkGetOnFailedToConnectProperties_doGetNetworkErrorInfo_m808386605 (NetworkGetOnFailedToConnectProperties_t4292706780 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
