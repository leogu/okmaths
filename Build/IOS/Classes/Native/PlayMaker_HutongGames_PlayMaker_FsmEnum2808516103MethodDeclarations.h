﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Enum
struct Enum_t2459695545;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Enum2459695545.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum2808516103.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Object HutongGames.PlayMaker.FsmEnum::get_RawValue()
extern "C"  Il2CppObject * FsmEnum_get_RawValue_m3854241349 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_RawValue(System.Object)
extern "C"  void FsmEnum_set_RawValue_m3734916150 (FsmEnum_t2808516103 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmEnum::get_EnumType()
extern "C"  Type_t * FsmEnum_get_EnumType_m3366370190 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_EnumType(System.Type)
extern "C"  void FsmEnum_set_EnumType_m1264731739 (FsmEnum_t2808516103 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::InitEnumType()
extern "C"  void FsmEnum_InitEnumType_m3833628479 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEnum::get_EnumName()
extern "C"  String_t* FsmEnum_get_EnumName_m3486575884 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_EnumName(System.String)
extern "C"  void FsmEnum_set_EnumName_m3304158537 (FsmEnum_t2808516103 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Enum HutongGames.PlayMaker.FsmEnum::get_Value()
extern "C"  Enum_t2459695545 * FsmEnum_get_Value_m3904328091 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_Value(System.Enum)
extern "C"  void FsmEnum_set_Value_m1697063838 (FsmEnum_t2808516103 * __this, Enum_t2459695545 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::ResetValue()
extern "C"  void FsmEnum_ResetValue_m1793483176 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor()
extern "C"  void FsmEnum__ctor_m1776491182 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor(System.String,System.Type,System.Int32)
extern "C"  void FsmEnum__ctor_m780598690 (FsmEnum_t2808516103 * __this, String_t* ___name0, Type_t * ___enumType1, int32_t ___intValue2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor(System.String)
extern "C"  void FsmEnum__ctor_m3227133356 (FsmEnum_t2808516103 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::.ctor(HutongGames.PlayMaker.FsmEnum)
extern "C"  void FsmEnum__ctor_m518730943 (FsmEnum_t2808516103 * __this, FsmEnum_t2808516103 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmEnum::Clone()
extern "C"  NamedVariable_t3026441313 * FsmEnum_Clone_m683096489 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEnum::ToString()
extern "C"  String_t* FsmEnum_ToString_m3894751405 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmEnum::get_VariableType()
extern "C"  int32_t FsmEnum_get_VariableType_m4063246226 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmEnum::get_ObjectType()
extern "C"  Type_t * FsmEnum_get_ObjectType_m2101725400 (FsmEnum_t2808516103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEnum::set_ObjectType(System.Type)
extern "C"  void FsmEnum_set_ObjectType_m287512889 (FsmEnum_t2808516103 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEnum::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmEnum_TestTypeConstraint_m3080898668 (FsmEnum_t2808516103 * __this, int32_t ___variableType0, Type_t * ____enumType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.FsmEnum::op_Implicit(System.Enum)
extern "C"  FsmEnum_t2808516103 * FsmEnum_op_Implicit_m595213435 (Il2CppObject * __this /* static, unused */, Enum_t2459695545 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
