﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m485765182(__this, ___l0, method) ((  void (*) (Enumerator_t4123095061 *, List_1_t293398091 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m881138012(__this, method) ((  void (*) (Enumerator_t4123095061 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m540688482(__this, method) ((  Il2CppObject * (*) (Enumerator_t4123095061 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::Dispose()
#define Enumerator_Dispose_m2506302615(__this, method) ((  void (*) (Enumerator_t4123095061 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::VerifyState()
#define Enumerator_VerifyState_m2540925164(__this, method) ((  void (*) (Enumerator_t4123095061 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::MoveNext()
#define Enumerator_MoveNext_m1561074909(__this, method) ((  bool (*) (Enumerator_t4123095061 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmTemplateControl>::get_Current()
#define Enumerator_get_Current_m306264929(__this, method) ((  FsmTemplateControl_t924276959 * (*) (Enumerator_t4123095061 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
