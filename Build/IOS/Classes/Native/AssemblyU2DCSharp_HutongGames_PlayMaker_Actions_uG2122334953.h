﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent
struct  uGuiInputFieldOnSubmitEvent_t2122334953  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::sendEvent
	FsmEvent_t1258573736 * ___sendEvent_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::text
	FsmString_t2414474701 * ___text_13;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::_inputField
	InputField_t1631627530 * ____inputField_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldOnSubmitEvent_t2122334953, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_sendEvent_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldOnSubmitEvent_t2122334953, ___sendEvent_12)); }
	inline FsmEvent_t1258573736 * get_sendEvent_12() const { return ___sendEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_sendEvent_12() { return &___sendEvent_12; }
	inline void set_sendEvent_12(FsmEvent_t1258573736 * value)
	{
		___sendEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_12, value);
	}

	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldOnSubmitEvent_t2122334953, ___text_13)); }
	inline FsmString_t2414474701 * get_text_13() const { return ___text_13; }
	inline FsmString_t2414474701 ** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(FsmString_t2414474701 * value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier(&___text_13, value);
	}

	inline static int32_t get_offset_of__inputField_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldOnSubmitEvent_t2122334953, ____inputField_14)); }
	inline InputField_t1631627530 * get__inputField_14() const { return ____inputField_14; }
	inline InputField_t1631627530 ** get_address_of__inputField_14() { return &____inputField_14; }
	inline void set__inputField_14(InputField_t1631627530 * value)
	{
		____inputField_14 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
