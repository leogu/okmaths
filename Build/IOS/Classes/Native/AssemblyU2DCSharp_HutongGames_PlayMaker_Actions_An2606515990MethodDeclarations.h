﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorStartPlayback
struct AnimatorStartPlayback_t2606515990;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorStartPlayback::.ctor()
extern "C"  void AnimatorStartPlayback__ctor_m117753332 (AnimatorStartPlayback_t2606515990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartPlayback::Reset()
extern "C"  void AnimatorStartPlayback_Reset_m585257423 (AnimatorStartPlayback_t2606515990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartPlayback::OnEnter()
extern "C"  void AnimatorStartPlayback_OnEnter_m1866614607 (AnimatorStartPlayback_t2606515990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
