﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath
struct DOTweenTransformLocalPath_t2632274708;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::.ctor()
extern "C"  void DOTweenTransformLocalPath__ctor_m2472370094 (DOTweenTransformLocalPath_t2632274708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::Reset()
extern "C"  void DOTweenTransformLocalPath_Reset_m1224567889 (DOTweenTransformLocalPath_t2632274708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::OnEnter()
extern "C"  void DOTweenTransformLocalPath_OnEnter_m3810660681 (DOTweenTransformLocalPath_t2632274708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::<OnEnter>m__BC()
extern "C"  void DOTweenTransformLocalPath_U3COnEnterU3Em__BC_m2678036659 (DOTweenTransformLocalPath_t2632274708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::<OnEnter>m__BD()
extern "C"  void DOTweenTransformLocalPath_U3COnEnterU3Em__BD_m2678036952 (DOTweenTransformLocalPath_t2632274708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
