﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// System.Type
struct Type_t;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject2785794313.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Type HutongGames.PlayMaker.FsmObject::get_ObjectType()
extern "C"  Type_t * FsmObject_get_ObjectType_m1109065422 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::set_ObjectType(System.Type)
extern "C"  void FsmObject_set_ObjectType_m2122109403 (FsmObject_t2785794313 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmObject::get_TypeName()
extern "C"  String_t* FsmObject_get_TypeName_m3226750607 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object HutongGames.PlayMaker.FsmObject::get_Value()
extern "C"  Object_t1021602117 * FsmObject_get_Value_m1585581971 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::set_Value(UnityEngine.Object)
extern "C"  void FsmObject_set_Value_m4152526958 (FsmObject_t2785794313 * __this, Object_t1021602117 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmObject::get_RawValue()
extern "C"  Il2CppObject * FsmObject_get_RawValue_m3960601563 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::set_RawValue(System.Object)
extern "C"  void FsmObject_set_RawValue_m2345710424 (FsmObject_t2785794313 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::.ctor()
extern "C"  void FsmObject__ctor_m2731248784 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::.ctor(System.String)
extern "C"  void FsmObject__ctor_m1841001134 (FsmObject_t2785794313 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmObject::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmObject__ctor_m4134475391 (FsmObject_t2785794313 * __this, FsmObject_t2785794313 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmObject::Clone()
extern "C"  NamedVariable_t3026441313 * FsmObject_Clone_m1725675903 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmObject::get_VariableType()
extern "C"  int32_t FsmObject_get_VariableType_m2759721168 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmObject::ToString()
extern "C"  String_t* FsmObject_ToString_m1205928899 (FsmObject_t2785794313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmObject::op_Implicit(UnityEngine.Object)
extern "C"  FsmObject_t2785794313 * FsmObject_op_Implicit_m3207094985 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmObject::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmObject_TestTypeConstraint_m1244842514 (FsmObject_t2785794313 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
