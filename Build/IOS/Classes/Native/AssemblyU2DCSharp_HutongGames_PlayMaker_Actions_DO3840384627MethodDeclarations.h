﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialTilingProperty
struct DOTweenMaterialTilingProperty_t3840384627;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTilingProperty::.ctor()
extern "C"  void DOTweenMaterialTilingProperty__ctor_m2039791413 (DOTweenMaterialTilingProperty_t3840384627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTilingProperty::Reset()
extern "C"  void DOTweenMaterialTilingProperty_Reset_m2628766344 (DOTweenMaterialTilingProperty_t3840384627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTilingProperty::OnEnter()
extern "C"  void DOTweenMaterialTilingProperty_OnEnter_m2550822138 (DOTweenMaterialTilingProperty_t3840384627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTilingProperty::<OnEnter>m__60()
extern "C"  void DOTweenMaterialTilingProperty_U3COnEnterU3Em__60_m2692532681 (DOTweenMaterialTilingProperty_t3840384627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTilingProperty::<OnEnter>m__61()
extern "C"  void DOTweenMaterialTilingProperty_U3COnEnterU3Em__61_m2692532776 (DOTweenMaterialTilingProperty_t3840384627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
