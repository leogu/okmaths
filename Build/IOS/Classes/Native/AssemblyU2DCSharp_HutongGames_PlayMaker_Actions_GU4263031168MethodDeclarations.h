﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIBox
struct GUIBox_t4263031168;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIBox::.ctor()
extern "C"  void GUIBox__ctor_m3219586668 (GUIBox_t4263031168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIBox::OnGUI()
extern "C"  void GUIBox_OnGUI_m655970808 (GUIBox_t4263031168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
