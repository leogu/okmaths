﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>
struct Queue_1_t447205353;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>
struct IEnumerator_1_t2398039641;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// HutongGames.Extensions.TextureExtensions/Point[]
struct PointU5BU5D_t1499831875;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions_627548518.h"
#include "System_System_Collections_Generic_Queue_1_Enumerato957268433.h"

// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::.ctor()
extern "C"  void Queue_1__ctor_m3186652032_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3186652032(__this, method) ((  void (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1__ctor_m3186652032_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Queue_1_System_Collections_ICollection_CopyTo_m2310117141_gshared (Queue_1_t447205353 * __this, Il2CppArray * ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m2310117141(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t447205353 *, Il2CppArray *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m2310117141_gshared)(__this, ___array0, ___idx1, method)
// System.Boolean System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m3463525277_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m3463525277(__this, method) ((  bool (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m3463525277_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Queue_1_System_Collections_ICollection_get_SyncRoot_m160408981_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m160408981(__this, method) ((  Il2CppObject * (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m160408981_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m997390113_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m997390113(__this, method) ((  Il2CppObject* (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m997390113_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Queue_1_System_Collections_IEnumerable_GetEnumerator_m2465395008_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m2465395008(__this, method) ((  Il2CppObject * (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m2465395008_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::CopyTo(T[],System.Int32)
extern "C"  void Queue_1_CopyTo_m3414123030_gshared (Queue_1_t447205353 * __this, PointU5BU5D_t1499831875* ___array0, int32_t ___idx1, const MethodInfo* method);
#define Queue_1_CopyTo_m3414123030(__this, ___array0, ___idx1, method) ((  void (*) (Queue_1_t447205353 *, PointU5BU5D_t1499831875*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3414123030_gshared)(__this, ___array0, ___idx1, method)
// T System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::Dequeue()
extern "C"  Point_t627548518  Queue_1_Dequeue_m3388281554_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_Dequeue_m3388281554(__this, method) ((  Point_t627548518  (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_Dequeue_m3388281554_gshared)(__this, method)
// T System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::Peek()
extern "C"  Point_t627548518  Queue_1_Peek_m3272753425_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_Peek_m3272753425(__this, method) ((  Point_t627548518  (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_Peek_m3272753425_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::Enqueue(T)
extern "C"  void Queue_1_Enqueue_m1362892191_gshared (Queue_1_t447205353 * __this, Point_t627548518  ___item0, const MethodInfo* method);
#define Queue_1_Enqueue_m1362892191(__this, ___item0, method) ((  void (*) (Queue_1_t447205353 *, Point_t627548518 , const MethodInfo*))Queue_1_Enqueue_m1362892191_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::SetCapacity(System.Int32)
extern "C"  void Queue_1_SetCapacity_m3486653202_gshared (Queue_1_t447205353 * __this, int32_t ___new_size0, const MethodInfo* method);
#define Queue_1_SetCapacity_m3486653202(__this, ___new_size0, method) ((  void (*) (Queue_1_t447205353 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m3486653202_gshared)(__this, ___new_size0, method)
// System.Int32 System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m3467033922_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m3467033922(__this, method) ((  int32_t (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_get_Count_m3467033922_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>::GetEnumerator()
extern "C"  Enumerator_t957268433  Queue_1_GetEnumerator_m886134602_gshared (Queue_1_t447205353 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m886134602(__this, method) ((  Enumerator_t957268433  (*) (Queue_1_t447205353 *, const MethodInfo*))Queue_1_GetEnumerator_m886134602_gshared)(__this, method)
