﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll
struct PlayerPrefsDeleteAll_t1827851911;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll::.ctor()
extern "C"  void PlayerPrefsDeleteAll__ctor_m2039151095 (PlayerPrefsDeleteAll_t1827851911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll::Reset()
extern "C"  void PlayerPrefsDeleteAll_Reset_m350482832 (PlayerPrefsDeleteAll_t1827851911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteAll::OnEnter()
extern "C"  void PlayerPrefsDeleteAll_OnEnter_m122067974 (PlayerPrefsDeleteAll_t1827851911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
