﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindGameObject
struct FindGameObject_t2228016812;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindGameObject::.ctor()
extern "C"  void FindGameObject__ctor_m943414962 (FindGameObject_t2228016812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::Reset()
extern "C"  void FindGameObject_Reset_m4164697201 (FindGameObject_t2228016812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::OnEnter()
extern "C"  void FindGameObject_OnEnter_m2080147129 (FindGameObject_t2228016812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindGameObject::Find()
extern "C"  void FindGameObject_Find_m2506213201 (FindGameObject_t2228016812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.FindGameObject::ErrorCheck()
extern "C"  String_t* FindGameObject_ErrorCheck_m548721975 (FindGameObject_t2228016812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
