﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<HutongGames.Extensions.TextureExtensions/Point>
struct Queue_1_t447205353;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerato957268433.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions_627548518.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m3252327655_gshared (Enumerator_t957268433 * __this, Queue_1_t447205353 * ___q0, const MethodInfo* method);
#define Enumerator__ctor_m3252327655(__this, ___q0, method) ((  void (*) (Enumerator_t957268433 *, Queue_1_t447205353 *, const MethodInfo*))Enumerator__ctor_m3252327655_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3970315272_gshared (Enumerator_t957268433 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3970315272(__this, method) ((  void (*) (Enumerator_t957268433 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3970315272_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1152605408_gshared (Enumerator_t957268433 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1152605408(__this, method) ((  Il2CppObject * (*) (Enumerator_t957268433 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1152605408_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::Dispose()
extern "C"  void Enumerator_Dispose_m1349617133_gshared (Enumerator_t957268433 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1349617133(__this, method) ((  void (*) (Enumerator_t957268433 *, const MethodInfo*))Enumerator_Dispose_m1349617133_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2113385624_gshared (Enumerator_t957268433 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2113385624(__this, method) ((  bool (*) (Enumerator_t957268433 *, const MethodInfo*))Enumerator_MoveNext_m2113385624_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<HutongGames.Extensions.TextureExtensions/Point>::get_Current()
extern "C"  Point_t627548518  Enumerator_get_Current_m3186429829_gshared (Enumerator_t957268433 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3186429829(__this, method) ((  Point_t627548518  (*) (Enumerator_t957268433 *, const MethodInfo*))Enumerator_get_Current_m3186429829_gshared)(__this, method)
