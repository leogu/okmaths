﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump
struct DOTweenTransformLocalJump_t3237588607;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::.ctor()
extern "C"  void DOTweenTransformLocalJump__ctor_m3299802239 (DOTweenTransformLocalJump_t3237588607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::Reset()
extern "C"  void DOTweenTransformLocalJump_Reset_m3860356180 (DOTweenTransformLocalJump_t3237588607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::OnEnter()
extern "C"  void DOTweenTransformLocalJump_OnEnter_m3097596906 (DOTweenTransformLocalJump_t3237588607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::<OnEnter>m__B2()
extern "C"  void DOTweenTransformLocalJump_U3COnEnterU3Em__B2_m138080841 (DOTweenTransformLocalJump_t3237588607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::<OnEnter>m__B3()
extern "C"  void DOTweenTransformLocalJump_U3COnEnterU3Em__B3_m138080874 (DOTweenTransformLocalJump_t3237588607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
