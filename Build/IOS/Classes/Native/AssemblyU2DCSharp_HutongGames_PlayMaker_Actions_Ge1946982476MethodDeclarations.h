﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTransform
struct GetTransform_t1946982476;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTransform::.ctor()
extern "C"  void GetTransform__ctor_m1276109308 (GetTransform_t1946982476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::Reset()
extern "C"  void GetTransform_Reset_m2993477397 (GetTransform_t1946982476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::OnEnter()
extern "C"  void GetTransform_OnEnter_m4236376309 (GetTransform_t1946982476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::OnUpdate()
extern "C"  void GetTransform_OnUpdate_m982633330 (GetTransform_t1946982476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTransform::DoGetGameObjectName()
extern "C"  void GetTransform_DoGetGameObjectName_m3974548317 (GetTransform_t1946982476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
