﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableRemove
struct HashTableRemove_t637629068;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableRemove::.ctor()
extern "C"  void HashTableRemove__ctor_m2877725458 (HashTableRemove_t637629068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableRemove::Reset()
extern "C"  void HashTableRemove_Reset_m2895544165 (HashTableRemove_t637629068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableRemove::OnEnter()
extern "C"  void HashTableRemove_OnEnter_m3494400629 (HashTableRemove_t637629068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableRemove::doHashTableRemove()
extern "C"  void HashTableRemove_doHashTableRemove_m3944780381 (HashTableRemove_t637629068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
