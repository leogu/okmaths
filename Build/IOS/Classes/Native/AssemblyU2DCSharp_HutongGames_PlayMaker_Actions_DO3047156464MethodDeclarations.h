﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteById
struct DOTweenControlMethodsCompleteById_t3047156464;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteById::.ctor()
extern "C"  void DOTweenControlMethodsCompleteById__ctor_m3438415632 (DOTweenControlMethodsCompleteById_t3047156464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteById::Reset()
extern "C"  void DOTweenControlMethodsCompleteById_Reset_m3085943245 (DOTweenControlMethodsCompleteById_t3047156464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteById::OnEnter()
extern "C"  void DOTweenControlMethodsCompleteById_OnEnter_m3830879789 (DOTweenControlMethodsCompleteById_t3047156464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
