﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetButtonUp
struct GetButtonUp_t1852674409;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::.ctor()
extern "C"  void GetButtonUp__ctor_m1627902899 (GetButtonUp_t1852674409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::Reset()
extern "C"  void GetButtonUp_Reset_m498732286 (GetButtonUp_t1852674409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonUp::OnUpdate()
extern "C"  void GetButtonUp_OnUpdate_m2223835091 (GetButtonUp_t1852674409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
