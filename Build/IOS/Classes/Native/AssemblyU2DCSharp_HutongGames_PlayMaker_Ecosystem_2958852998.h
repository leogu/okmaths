﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct PlayMakerEvent_t1571596806;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge
struct  TransformEventsBridge_t2958852998  : public MonoBehaviour_t1158329972
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::eventTarget
	PlayMakerEventTarget_t2104288969 * ___eventTarget_2;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::parentChangedEvent
	PlayMakerEvent_t1571596806 * ___parentChangedEvent_3;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::childrenChangedEvent
	PlayMakerEvent_t1571596806 * ___childrenChangedEvent_4;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::debug
	bool ___debug_5;

public:
	inline static int32_t get_offset_of_eventTarget_2() { return static_cast<int32_t>(offsetof(TransformEventsBridge_t2958852998, ___eventTarget_2)); }
	inline PlayMakerEventTarget_t2104288969 * get_eventTarget_2() const { return ___eventTarget_2; }
	inline PlayMakerEventTarget_t2104288969 ** get_address_of_eventTarget_2() { return &___eventTarget_2; }
	inline void set_eventTarget_2(PlayMakerEventTarget_t2104288969 * value)
	{
		___eventTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_2, value);
	}

	inline static int32_t get_offset_of_parentChangedEvent_3() { return static_cast<int32_t>(offsetof(TransformEventsBridge_t2958852998, ___parentChangedEvent_3)); }
	inline PlayMakerEvent_t1571596806 * get_parentChangedEvent_3() const { return ___parentChangedEvent_3; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_parentChangedEvent_3() { return &___parentChangedEvent_3; }
	inline void set_parentChangedEvent_3(PlayMakerEvent_t1571596806 * value)
	{
		___parentChangedEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___parentChangedEvent_3, value);
	}

	inline static int32_t get_offset_of_childrenChangedEvent_4() { return static_cast<int32_t>(offsetof(TransformEventsBridge_t2958852998, ___childrenChangedEvent_4)); }
	inline PlayMakerEvent_t1571596806 * get_childrenChangedEvent_4() const { return ___childrenChangedEvent_4; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_childrenChangedEvent_4() { return &___childrenChangedEvent_4; }
	inline void set_childrenChangedEvent_4(PlayMakerEvent_t1571596806 * value)
	{
		___childrenChangedEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___childrenChangedEvent_4, value);
	}

	inline static int32_t get_offset_of_debug_5() { return static_cast<int32_t>(offsetof(TransformEventsBridge_t2958852998, ___debug_5)); }
	inline bool get_debug_5() const { return ___debug_5; }
	inline bool* get_address_of_debug_5() { return &___debug_5; }
	inline void set_debug_5(bool value)
	{
		___debug_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
