﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayGetNext
struct ArrayGetNext_t3239640766;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::.ctor()
extern "C"  void ArrayGetNext__ctor_m1834610218 (ArrayGetNext_t3239640766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::Reset()
extern "C"  void ArrayGetNext_Reset_m3551974083 (ArrayGetNext_t3239640766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::OnEnter()
extern "C"  void ArrayGetNext_OnEnter_m2581812899 (ArrayGetNext_t3239640766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayGetNext::DoGetNextItem()
extern "C"  void ArrayGetNext_DoGetNextItem_m827006477 (ArrayGetNext_t3239640766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
