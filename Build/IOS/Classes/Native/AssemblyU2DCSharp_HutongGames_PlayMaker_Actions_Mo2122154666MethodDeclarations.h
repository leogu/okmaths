﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MoveObject
struct MoveObject_t2122154666;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MoveObject::.ctor()
extern "C"  void MoveObject__ctor_m1167823422 (MoveObject_t2122154666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveObject::Reset()
extern "C"  void MoveObject_Reset_m981684463 (MoveObject_t2122154666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveObject::OnEnter()
extern "C"  void MoveObject_OnEnter_m1567214351 (MoveObject_t2122154666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveObject::OnUpdate()
extern "C"  void MoveObject_OnUpdate_m4278772120 (MoveObject_t2122154666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
