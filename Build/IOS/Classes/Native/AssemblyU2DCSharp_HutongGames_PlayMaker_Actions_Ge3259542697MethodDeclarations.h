﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetProperty
struct GetProperty_t3259542697;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetProperty::.ctor()
extern "C"  void GetProperty__ctor_m2744646263 (GetProperty_t3259542697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetProperty::Reset()
extern "C"  void GetProperty_Reset_m1628057738 (GetProperty_t3259542697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetProperty::OnEnter()
extern "C"  void GetProperty_OnEnter_m2948669172 (GetProperty_t3259542697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetProperty::OnUpdate()
extern "C"  void GetProperty_OnUpdate_m3752392463 (GetProperty_t3259542697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
