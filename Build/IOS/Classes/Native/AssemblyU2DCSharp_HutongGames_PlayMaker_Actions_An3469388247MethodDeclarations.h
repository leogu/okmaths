﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateFsmAction
struct AnimateFsmAction_t3469388247;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::.ctor()
extern "C"  void AnimateFsmAction__ctor_m1598202491 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::Reset()
extern "C"  void AnimateFsmAction_Reset_m3935434684 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::OnEnter()
extern "C"  void AnimateFsmAction_OnEnter_m91027234 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::Init()
extern "C"  void AnimateFsmAction_Init_m3196063425 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::OnUpdate()
extern "C"  void AnimateFsmAction_OnUpdate_m1059254795 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::CheckStart()
extern "C"  void AnimateFsmAction_CheckStart_m2939878333 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::UpdateTime()
extern "C"  void AnimateFsmAction_UpdateTime_m386986279 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::UpdateAnimation()
extern "C"  void AnimateFsmAction_UpdateAnimation_m3225840510 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFsmAction::CheckFinished()
extern "C"  void AnimateFsmAction_CheckFinished_m757361255 (AnimateFsmAction_t3469388247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
