﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionSlerp
struct QuaternionSlerp_t3176095846;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::.ctor()
extern "C"  void QuaternionSlerp__ctor_m922547226 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::Reset()
extern "C"  void QuaternionSlerp_Reset_m3305049127 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnEnter()
extern "C"  void QuaternionSlerp_OnEnter_m1164282983 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnUpdate()
extern "C"  void QuaternionSlerp_OnUpdate_m2387953244 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnLateUpdate()
extern "C"  void QuaternionSlerp_OnLateUpdate_m3400249808 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::OnFixedUpdate()
extern "C"  void QuaternionSlerp_OnFixedUpdate_m3058725214 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionSlerp::DoQuatSlerp()
extern "C"  void QuaternionSlerp_DoQuatSlerp_m1992206728 (QuaternionSlerp_t3176095846 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
