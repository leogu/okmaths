﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderSetDirection
struct  uGuiSliderSetDirection_t238648630  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// UnityEngine.UI.Slider/Direction HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::direction
	int32_t ___direction_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::_slider
	Slider_t297367283 * ____slider_14;
	// UnityEngine.UI.Slider/Direction HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::_originalValue
	int32_t ____originalValue_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderSetDirection_t238648630, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_direction_12() { return static_cast<int32_t>(offsetof(uGuiSliderSetDirection_t238648630, ___direction_12)); }
	inline int32_t get_direction_12() const { return ___direction_12; }
	inline int32_t* get_address_of_direction_12() { return &___direction_12; }
	inline void set_direction_12(int32_t value)
	{
		___direction_12 = value;
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiSliderSetDirection_t238648630, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__slider_14() { return static_cast<int32_t>(offsetof(uGuiSliderSetDirection_t238648630, ____slider_14)); }
	inline Slider_t297367283 * get__slider_14() const { return ____slider_14; }
	inline Slider_t297367283 ** get_address_of__slider_14() { return &____slider_14; }
	inline void set__slider_14(Slider_t297367283 * value)
	{
		____slider_14 = value;
		Il2CppCodeGenWriteBarrier(&____slider_14, value);
	}

	inline static int32_t get_offset_of__originalValue_15() { return static_cast<int32_t>(offsetof(uGuiSliderSetDirection_t238648630, ____originalValue_15)); }
	inline int32_t get__originalValue_15() const { return ____originalValue_15; }
	inline int32_t* get_address_of__originalValue_15() { return &____originalValue_15; }
	inline void set__originalValue_15(int32_t value)
	{
		____originalValue_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
