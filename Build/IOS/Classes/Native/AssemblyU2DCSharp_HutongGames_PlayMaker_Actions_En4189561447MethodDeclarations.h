﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableAnimation
struct EnableAnimation_t4189561447;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::.ctor()
extern "C"  void EnableAnimation__ctor_m494995029 (EnableAnimation_t4189561447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::Reset()
extern "C"  void EnableAnimation_Reset_m2970995680 (EnableAnimation_t4189561447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::OnEnter()
extern "C"  void EnableAnimation_OnEnter_m2042654914 (EnableAnimation_t4189561447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::DoEnableAnimation(UnityEngine.GameObject)
extern "C"  void EnableAnimation_DoEnableAnimation_m835422421 (EnableAnimation_t4189561447 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableAnimation::OnExit()
extern "C"  void EnableAnimation_OnExit_m315542486 (EnableAnimation_t4189561447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
