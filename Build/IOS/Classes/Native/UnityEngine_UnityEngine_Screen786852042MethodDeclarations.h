﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t665107673;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Resolution3693662728.h"

// UnityEngine.Resolution[] UnityEngine.Screen::get_resolutions()
extern "C"  ResolutionU5BU5D_t665107673* Screen_get_resolutions_m3563081577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t3693662728  Screen_get_currentResolution_m2361090437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)
extern "C"  void Screen_SetResolution_m2850169807 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, bool ___fullscreen2, int32_t ___preferredRefreshRate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean)
extern "C"  void Screen_SetResolution_m55027544 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, bool ___fullscreen2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3345126327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_sleepTimeout()
extern "C"  int32_t Screen_get_sleepTimeout_m405361946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
