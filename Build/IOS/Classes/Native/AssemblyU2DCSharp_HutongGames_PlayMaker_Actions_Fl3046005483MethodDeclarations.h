﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatClamp
struct FloatClamp_t3046005483;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatClamp::.ctor()
extern "C"  void FloatClamp__ctor_m2369614497 (FloatClamp_t3046005483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::Reset()
extern "C"  void FloatClamp_Reset_m650588920 (FloatClamp_t3046005483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnEnter()
extern "C"  void FloatClamp_OnEnter_m3631914066 (FloatClamp_t3046005483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::OnUpdate()
extern "C"  void FloatClamp_OnUpdate_m4051655829 (FloatClamp_t3046005483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatClamp::DoClamp()
extern "C"  void FloatClamp_DoClamp_m3786412969 (FloatClamp_t3046005483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
