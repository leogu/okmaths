﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectCompareTag
struct GameObjectCompareTag_t3478618144;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::.ctor()
extern "C"  void GameObjectCompareTag__ctor_m1802478088 (GameObjectCompareTag_t3478618144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::Reset()
extern "C"  void GameObjectCompareTag_Reset_m4137270793 (GameObjectCompareTag_t3478618144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnEnter()
extern "C"  void GameObjectCompareTag_OnEnter_m1684259289 (GameObjectCompareTag_t3478618144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::OnUpdate()
extern "C"  void GameObjectCompareTag_OnUpdate_m3742165502 (GameObjectCompareTag_t3478618144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompareTag::DoCompareTag()
extern "C"  void GameObjectCompareTag_DoCompareTag_m3818734798 (GameObjectCompareTag_t3478618144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
