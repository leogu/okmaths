﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// DG.Tweening.DOTweenAnimation
struct DOTweenAnimation_t858634588;
// PlayMakerArrayListProxy
struct PlayMakerArrayListProxy_t4012441185;
// PlayMakerCollectionProxy
struct PlayMakerCollectionProxy_t398511462;
// PlayMakerHashTableProxy
struct PlayMakerHashTableProxy_t3073922234;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimation858634588.h"
#include "AssemblyU2DCSharp_PlayMakerArrayListProxy4012441185.h"
#include "AssemblyU2DCSharp_PlayMakerCollectionProxy398511462.h"
#include "AssemblyU2DCSharp_PlayMakerHashTableProxy3073922234.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani322151382.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2535927234.h"

#pragma once
// DG.Tweening.DOTweenAnimation[]
struct DOTweenAnimationU5BU5D_t3410086325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DOTweenAnimation_t858634588 * m_Items[1];

public:
	inline DOTweenAnimation_t858634588 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DOTweenAnimation_t858634588 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DOTweenAnimation_t858634588 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayMakerArrayListProxy[]
struct PlayMakerArrayListProxyU5BU5D_t2412052220  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayMakerArrayListProxy_t4012441185 * m_Items[1];

public:
	inline PlayMakerArrayListProxy_t4012441185 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayMakerArrayListProxy_t4012441185 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayMakerArrayListProxy_t4012441185 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayMakerCollectionProxy[]
struct PlayMakerCollectionProxyU5BU5D_t294011459  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayMakerCollectionProxy_t398511462 * m_Items[1];

public:
	inline PlayMakerCollectionProxy_t398511462 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayMakerCollectionProxy_t398511462 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayMakerCollectionProxy_t398511462 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// PlayMakerHashTableProxy[]
struct PlayMakerHashTableProxyU5BU5D_t2020749087  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayMakerHashTableProxy_t3073922234 * m_Items[1];

public:
	inline PlayMakerHashTableProxy_t3073922234 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayMakerHashTableProxy_t3073922234 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayMakerHashTableProxy_t3073922234 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation[]
struct CalculationU5BU5D_t2178323475  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation[]
struct CalculationU5BU5D_t15526135  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
