﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSpeed
struct GetSpeed_t3829002569;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSpeed::.ctor()
extern "C"  void GetSpeed__ctor_m1465079331 (GetSpeed_t3829002569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::Reset()
extern "C"  void GetSpeed_Reset_m3424472186 (GetSpeed_t3829002569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::OnEnter()
extern "C"  void GetSpeed_OnEnter_m2223320836 (GetSpeed_t3829002569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::OnUpdate()
extern "C"  void GetSpeed_OnUpdate_m615395347 (GetSpeed_t3829002569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed::DoGetSpeed()
extern "C"  void GetSpeed_DoGetSpeed_m2931627521 (GetSpeed_t3829002569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
