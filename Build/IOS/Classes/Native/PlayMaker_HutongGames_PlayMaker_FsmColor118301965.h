﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmColor
struct  FsmColor_t118301965  : public NamedVariable_t3026441313
{
public:
	// UnityEngine.Color HutongGames.PlayMaker.FsmColor::value
	Color_t2020392075  ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmColor_t118301965, ___value_5)); }
	inline Color_t2020392075  get_value_5() const { return ___value_5; }
	inline Color_t2020392075 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Color_t2020392075  value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
