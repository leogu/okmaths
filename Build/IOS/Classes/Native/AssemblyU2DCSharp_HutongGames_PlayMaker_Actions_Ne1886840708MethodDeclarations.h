﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetLogLevel
struct NetworkSetLogLevel_t1886840708;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetLogLevel::.ctor()
extern "C"  void NetworkSetLogLevel__ctor_m1914940434 (NetworkSetLogLevel_t1886840708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetLogLevel::Reset()
extern "C"  void NetworkSetLogLevel_Reset_m2713873745 (NetworkSetLogLevel_t1886840708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetLogLevel::OnEnter()
extern "C"  void NetworkSetLogLevel_OnEnter_m3972433 (NetworkSetLogLevel_t1886840708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
