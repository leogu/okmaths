﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetStringLength
struct GetStringLength_t602337949;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetStringLength::.ctor()
extern "C"  void GetStringLength__ctor_m63366265 (GetStringLength_t602337949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::Reset()
extern "C"  void GetStringLength_Reset_m590111550 (GetStringLength_t602337949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::OnEnter()
extern "C"  void GetStringLength_OnEnter_m3258492988 (GetStringLength_t602337949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::OnUpdate()
extern "C"  void GetStringLength_OnUpdate_m2790827397 (GetStringLength_t602337949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLength::DoGetStringLength()
extern "C"  void GetStringLength_DoGetStringLength_m3133858417 (GetStringLength_t602337949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
