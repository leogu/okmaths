﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerStay
struct PlayMakerTriggerStay_t2768254945;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void PlayMakerTriggerStay::OnTriggerStay(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerStay_OnTriggerStay_m3328207047 (PlayMakerTriggerStay_t2768254945 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerStay::.ctor()
extern "C"  void PlayMakerTriggerStay__ctor_m221491136 (PlayMakerTriggerStay_t2768254945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
