﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorBool
struct SetAnimatorBool_t1087961285;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::.ctor()
extern "C"  void SetAnimatorBool__ctor_m2303880349 (SetAnimatorBool_t1087961285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::Reset()
extern "C"  void SetAnimatorBool_Reset_m2134115650 (SetAnimatorBool_t1087961285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::OnEnter()
extern "C"  void SetAnimatorBool_OnEnter_m3231680096 (SetAnimatorBool_t1087961285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::OnActionUpdate()
extern "C"  void SetAnimatorBool_OnActionUpdate_m2387070539 (SetAnimatorBool_t1087961285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorBool::SetParameter()
extern "C"  void SetAnimatorBool_SetParameter_m2471012474 (SetAnimatorBool_t1087961285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
