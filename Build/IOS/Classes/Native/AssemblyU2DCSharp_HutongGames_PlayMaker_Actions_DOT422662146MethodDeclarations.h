﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardById
struct DOTweenControlMethodsPlayForwardById_t422662146;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardById::.ctor()
extern "C"  void DOTweenControlMethodsPlayForwardById__ctor_m2836233596 (DOTweenControlMethodsPlayForwardById_t422662146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardById::Reset()
extern "C"  void DOTweenControlMethodsPlayForwardById_Reset_m1761174187 (DOTweenControlMethodsPlayForwardById_t422662146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardById::OnEnter()
extern "C"  void DOTweenControlMethodsPlayForwardById_OnEnter_m119726019 (DOTweenControlMethodsPlayForwardById_t422662146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
