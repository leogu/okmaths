﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformPunchScale
struct DOTweenTransformPunchScale_t831519160;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchScale::.ctor()
extern "C"  void DOTweenTransformPunchScale__ctor_m362979194 (DOTweenTransformPunchScale_t831519160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchScale::Reset()
extern "C"  void DOTweenTransformPunchScale_Reset_m178211649 (DOTweenTransformPunchScale_t831519160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchScale::OnEnter()
extern "C"  void DOTweenTransformPunchScale_OnEnter_m3511655673 (DOTweenTransformPunchScale_t831519160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchScale::<OnEnter>m__D2()
extern "C"  void DOTweenTransformPunchScale_U3COnEnterU3Em__D2_m3975511648 (DOTweenTransformPunchScale_t831519160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchScale::<OnEnter>m__D3()
extern "C"  void DOTweenTransformPunchScale_U3COnEnterU3Em__D3_m4116674149 (DOTweenTransformPunchScale_t831519160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
