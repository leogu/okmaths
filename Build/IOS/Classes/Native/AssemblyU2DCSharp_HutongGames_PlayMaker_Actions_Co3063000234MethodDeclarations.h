﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Collision2dEvent
struct Collision2dEvent_t3063000234;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::.ctor()
extern "C"  void Collision2dEvent__ctor_m2447070718 (Collision2dEvent_t3063000234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::Reset()
extern "C"  void Collision2dEvent_Reset_m2265540143 (Collision2dEvent_t3063000234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::OnPreprocess()
extern "C"  void Collision2dEvent_OnPreprocess_m359730759 (Collision2dEvent_t3063000234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::StoreCollisionInfo(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_StoreCollisionInfo_m659934842 (Collision2dEvent_t3063000234 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_DoCollisionEnter2D_m3575129852 (Collision2dEvent_t3063000234 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_DoCollisionStay2D_m3967046445 (Collision2dEvent_t3063000234 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void Collision2dEvent_DoCollisionExit2D_m728255558 (Collision2dEvent_t3063000234 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Collision2dEvent::DoParticleCollision(UnityEngine.GameObject)
extern "C"  void Collision2dEvent_DoParticleCollision_m1457471871 (Collision2dEvent_t3063000234 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.Collision2dEvent::ErrorCheck()
extern "C"  String_t* Collision2dEvent_ErrorCheck_m1624803437 (Collision2dEvent_t3063000234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
