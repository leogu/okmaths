﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Operator
struct Vector3Operator_t3982675204;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::.ctor()
extern "C"  void Vector3Operator__ctor_m288833478 (Vector3Operator_t3982675204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::Reset()
extern "C"  void Vector3Operator_Reset_m2617180057 (Vector3Operator_t3982675204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::OnEnter()
extern "C"  void Vector3Operator_OnEnter_m377079417 (Vector3Operator_t3982675204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::OnUpdate()
extern "C"  void Vector3Operator_OnUpdate_m2171957304 (Vector3Operator_t3982675204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Operator::DoVector3Operator()
extern "C"  void Vector3Operator_DoVector3Operator_m4114391613 (Vector3Operator_t3982675204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
