﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion
struct GetAnimatorApplyRootMotion_t3550329075;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::.ctor()
extern "C"  void GetAnimatorApplyRootMotion__ctor_m536770207 (GetAnimatorApplyRootMotion_t3550329075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::Reset()
extern "C"  void GetAnimatorApplyRootMotion_Reset_m354731800 (GetAnimatorApplyRootMotion_t3550329075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::OnEnter()
extern "C"  void GetAnimatorApplyRootMotion_OnEnter_m2746724734 (GetAnimatorApplyRootMotion_t3550329075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorApplyRootMotion::GetApplyMotionRoot()
extern "C"  void GetAnimatorApplyRootMotion_GetApplyMotionRoot_m3066442741 (GetAnimatorApplyRootMotion_t3550329075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
