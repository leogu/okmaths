﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles
struct GetQuaternionEulerAngles_t1930529559;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::.ctor()
extern "C"  void GetQuaternionEulerAngles__ctor_m4017642077 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::Reset()
extern "C"  void GetQuaternionEulerAngles_Reset_m2940709948 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnEnter()
extern "C"  void GetQuaternionEulerAngles_OnEnter_m3008982990 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnUpdate()
extern "C"  void GetQuaternionEulerAngles_OnUpdate_m3552831337 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnLateUpdate()
extern "C"  void GetQuaternionEulerAngles_OnLateUpdate_m555117285 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::OnFixedUpdate()
extern "C"  void GetQuaternionEulerAngles_OnFixedUpdate_m1590533227 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionEulerAngles::GetQuatEuler()
extern "C"  void GetQuaternionEulerAngles_GetQuatEuler_m491365253 (GetQuaternionEulerAngles_t1930529559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
