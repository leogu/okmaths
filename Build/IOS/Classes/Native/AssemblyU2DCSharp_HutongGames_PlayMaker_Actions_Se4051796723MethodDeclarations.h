﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMouseCursor
struct SetMouseCursor_t4051796723;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::.ctor()
extern "C"  void SetMouseCursor__ctor_m2506859159 (SetMouseCursor_t4051796723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::Reset()
extern "C"  void SetMouseCursor_Reset_m1671380416 (SetMouseCursor_t4051796723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMouseCursor::OnEnter()
extern "C"  void SetMouseCursor_OnEnter_m109779646 (SetMouseCursor_t4051796723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
