﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableContains
struct HashTableContains_t970090917;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableContains::.ctor()
extern "C"  void HashTableContains__ctor_m3057531837 (HashTableContains_t970090917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContains::Reset()
extern "C"  void HashTableContains_Reset_m2874698978 (HashTableContains_t970090917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContains::OnEnter()
extern "C"  void HashTableContains_OnEnter_m1316509712 (HashTableContains_t970090917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContains::checkIfContainsKey()
extern "C"  void HashTableContains_checkIfContainsKey_m1671659376 (HashTableContains_t970090917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
