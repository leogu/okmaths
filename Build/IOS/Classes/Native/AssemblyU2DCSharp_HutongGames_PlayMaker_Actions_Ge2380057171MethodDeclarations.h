﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVectorLength
struct GetVectorLength_t2380057171;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::.ctor()
extern "C"  void GetVectorLength__ctor_m235972641 (GetVectorLength_t2380057171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::Reset()
extern "C"  void GetVectorLength_Reset_m3401756916 (GetVectorLength_t2380057171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::OnEnter()
extern "C"  void GetVectorLength_OnEnter_m3166071070 (GetVectorLength_t2380057171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVectorLength::DoVectorLength()
extern "C"  void GetVectorLength_DoVectorLength_m4148757059 (GetVectorLength_t2380057171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
