﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindClosest
struct FindClosest_t692058282;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindClosest::.ctor()
extern "C"  void FindClosest__ctor_m3259561748 (FindClosest_t692058282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindClosest::Reset()
extern "C"  void FindClosest_Reset_m2154461439 (FindClosest_t692058282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindClosest::OnEnter()
extern "C"  void FindClosest_OnEnter_m1189141663 (FindClosest_t692058282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindClosest::OnUpdate()
extern "C"  void FindClosest_OnUpdate_m59840274 (FindClosest_t692058282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindClosest::DoFindClosest()
extern "C"  void FindClosest_DoFindClosest_m2355610361 (FindClosest_t692058282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
