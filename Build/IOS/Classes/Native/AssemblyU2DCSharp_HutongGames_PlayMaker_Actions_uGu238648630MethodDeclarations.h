﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderSetDirection
struct uGuiSliderSetDirection_t238648630;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::.ctor()
extern "C"  void uGuiSliderSetDirection__ctor_m1223039198 (uGuiSliderSetDirection_t238648630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::Reset()
extern "C"  void uGuiSliderSetDirection_Reset_m3832577375 (uGuiSliderSetDirection_t238648630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::OnEnter()
extern "C"  void uGuiSliderSetDirection_OnEnter_m3956815007 (uGuiSliderSetDirection_t238648630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::DoSetValue()
extern "C"  void uGuiSliderSetDirection_DoSetValue_m2273604512 (uGuiSliderSetDirection_t238648630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetDirection::OnExit()
extern "C"  void uGuiSliderSetDirection_OnExit_m292272727 (uGuiSliderSetDirection_t238648630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
