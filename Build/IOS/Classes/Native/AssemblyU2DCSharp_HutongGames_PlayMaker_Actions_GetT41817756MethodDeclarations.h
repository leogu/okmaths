﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTrigger2dInfo
struct GetTrigger2dInfo_t41817756;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::.ctor()
extern "C"  void GetTrigger2dInfo__ctor_m3867946518 (GetTrigger2dInfo_t41817756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::Reset()
extern "C"  void GetTrigger2dInfo_Reset_m3681818053 (GetTrigger2dInfo_t41817756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::StoreTriggerInfo()
extern "C"  void GetTrigger2dInfo_StoreTriggerInfo_m222116917 (GetTrigger2dInfo_t41817756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTrigger2dInfo::OnEnter()
extern "C"  void GetTrigger2dInfo_OnEnter_m933876445 (GetTrigger2dInfo_t41817756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
