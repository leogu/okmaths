﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimationSpeed
struct SetAnimationSpeed_t3482002817;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::.ctor()
extern "C"  void SetAnimationSpeed__ctor_m4204169609 (SetAnimationSpeed_t3482002817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::Reset()
extern "C"  void SetAnimationSpeed_Reset_m3879124886 (SetAnimationSpeed_t3482002817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::OnEnter()
extern "C"  void SetAnimationSpeed_OnEnter_m1450865180 (SetAnimationSpeed_t3482002817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::OnUpdate()
extern "C"  void SetAnimationSpeed_OnUpdate_m3064366373 (SetAnimationSpeed_t3482002817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationSpeed::DoSetAnimationSpeed(UnityEngine.GameObject)
extern "C"  void SetAnimationSpeed_DoSetAnimationSpeed_m2534932237 (SetAnimationSpeed_t3482002817 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
