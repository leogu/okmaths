﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnumCompare
struct EnumCompare_t3562712762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnumCompare::.ctor()
extern "C"  void EnumCompare__ctor_m2694657158 (EnumCompare_t3562712762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::Reset()
extern "C"  void EnumCompare_Reset_m1648809723 (EnumCompare_t3562712762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::OnEnter()
extern "C"  void EnumCompare_OnEnter_m397569531 (EnumCompare_t3562712762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::OnUpdate()
extern "C"  void EnumCompare_OnUpdate_m3827366472 (EnumCompare_t3562712762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumCompare::DoEnumCompare()
extern "C"  void EnumCompare_DoEnumCompare_m2738085209 (EnumCompare_t3562712762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
