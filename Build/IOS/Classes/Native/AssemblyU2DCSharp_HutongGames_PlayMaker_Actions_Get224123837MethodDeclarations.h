﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTan
struct GetTan_t224123837;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTan::.ctor()
extern "C"  void GetTan__ctor_m998756111 (GetTan_t224123837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::Reset()
extern "C"  void GetTan_Reset_m3568790926 (GetTan_t224123837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::OnEnter()
extern "C"  void GetTan_OnEnter_m1122179512 (GetTan_t224123837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::OnUpdate()
extern "C"  void GetTan_OnUpdate_m1666407039 (GetTan_t224123837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTan::DoTan()
extern "C"  void GetTan_DoTan_m2166951043 (GetTan_t224123837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
