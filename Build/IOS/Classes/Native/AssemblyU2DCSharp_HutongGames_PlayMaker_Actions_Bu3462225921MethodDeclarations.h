﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BuildString
struct BuildString_t3462225921;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BuildString::.ctor()
extern "C"  void BuildString__ctor_m91940713 (BuildString_t3462225921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::Reset()
extern "C"  void BuildString_Reset_m3209847126 (BuildString_t3462225921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::OnEnter()
extern "C"  void BuildString_OnEnter_m1663898412 (BuildString_t3462225921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::OnUpdate()
extern "C"  void BuildString_OnUpdate_m4083712165 (BuildString_t3462225921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BuildString::DoBuildString()
extern "C"  void BuildString_DoBuildString_m2475934849 (BuildString_t3462225921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
