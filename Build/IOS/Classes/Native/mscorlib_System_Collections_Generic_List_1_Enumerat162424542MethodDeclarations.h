﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m366330893(__this, ___l0, method) ((  void (*) (Enumerator_t162424542 *, List_1_t627694868 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3867774717(__this, method) ((  void (*) (Enumerator_t162424542 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1119529753(__this, method) ((  Il2CppObject * (*) (Enumerator_t162424542 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::Dispose()
#define Enumerator_Dispose_m1979685060(__this, method) ((  void (*) (Enumerator_t162424542 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::VerifyState()
#define Enumerator_VerifyState_m421394435(__this, method) ((  void (*) (Enumerator_t162424542 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::MoveNext()
#define Enumerator_MoveNext_m3648504780(__this, method) ((  bool (*) (Enumerator_t162424542 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmEvent>::get_Current()
#define Enumerator_get_Current_m1351792750(__this, method) ((  FsmEvent_t1258573736 * (*) (Enumerator_t162424542 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
