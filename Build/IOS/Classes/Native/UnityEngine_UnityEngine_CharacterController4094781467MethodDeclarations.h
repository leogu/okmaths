﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.CharacterController
struct CharacterController_t4094781467;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"

// System.Boolean UnityEngine.CharacterController::SimpleMove(UnityEngine.Vector3)
extern "C"  bool CharacterController_SimpleMove_m3872077378 (CharacterController_t4094781467 * __this, Vector3_t2243707580  ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  bool CharacterController_INTERNAL_CALL_SimpleMove_m63258103 (Il2CppObject * __this /* static, unused */, CharacterController_t4094781467 * ___self0, Vector3_t2243707580 * ___speed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m3456882757 (CharacterController_t4094781467 * __this, Vector3_t2243707580  ___motion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  int32_t CharacterController_INTERNAL_CALL_Move_m2826125634 (Il2CppObject * __this /* static, unused */, CharacterController_t4094781467 * ___self0, Vector3_t2243707580 * ___motion1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m2594228107 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::get_collisionFlags()
extern "C"  int32_t CharacterController_get_collisionFlags_m3772321073 (CharacterController_t4094781467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_radius(System.Single)
extern "C"  void CharacterController_set_radius_m1695843322 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_height(System.Single)
extern "C"  void CharacterController_set_height_m2274211287 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_center(UnityEngine.Vector3)
extern "C"  void CharacterController_set_center_m634679819 (CharacterController_t4094781467 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void CharacterController_INTERNAL_set_center_m1598929593 (CharacterController_t4094781467 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_slopeLimit(System.Single)
extern "C"  void CharacterController_set_slopeLimit_m1597209790 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_stepOffset(System.Single)
extern "C"  void CharacterController_set_stepOffset_m3474051307 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CharacterController::set_detectCollisions(System.Boolean)
extern "C"  void CharacterController_set_detectCollisions_m1206451172 (CharacterController_t4094781467 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
