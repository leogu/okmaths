﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionTarget
struct ActionTarget_t967701655;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionTarget967701655.h"

// System.Type HutongGames.PlayMaker.ActionTarget::get_ObjectType()
extern "C"  Type_t * ActionTarget_get_ObjectType_m112725364 (ActionTarget_t967701655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionTarget::get_FieldName()
extern "C"  String_t* ActionTarget_get_FieldName_m872372603 (ActionTarget_t967701655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionTarget::get_AllowPrefabs()
extern "C"  bool ActionTarget_get_AllowPrefabs_m64288315 (ActionTarget_t967701655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionTarget::.ctor(System.Type,System.String,System.Boolean)
extern "C"  void ActionTarget__ctor_m2637909266 (ActionTarget_t967701655 * __this, Type_t * ___objectType0, String_t* ___fieldName1, bool ___allowPrefabs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionTarget::IsSameAs(HutongGames.PlayMaker.ActionTarget)
extern "C"  bool ActionTarget_IsSameAs_m922325897 (ActionTarget_t967701655 * __this, ActionTarget_t967701655 * ___actionTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionTarget::ToString()
extern "C"  String_t* ActionTarget_ToString_m2362490589 (ActionTarget_t967701655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
