﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime
struct GetAnimatorPlayBackTime_t538113597;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::.ctor()
extern "C"  void GetAnimatorPlayBackTime__ctor_m3894713709 (GetAnimatorPlayBackTime_t538113597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::Reset()
extern "C"  void GetAnimatorPlayBackTime_Reset_m3697546162 (GetAnimatorPlayBackTime_t538113597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnEnter()
extern "C"  void GetAnimatorPlayBackTime_OnEnter_m1775739288 (GetAnimatorPlayBackTime_t538113597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::OnUpdate()
extern "C"  void GetAnimatorPlayBackTime_OnUpdate_m3991311241 (GetAnimatorPlayBackTime_t538113597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackTime::GetPlayBackTime()
extern "C"  void GetAnimatorPlayBackTime_GetPlayBackTime_m1471527701 (GetAnimatorPlayBackTime_t538113597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
