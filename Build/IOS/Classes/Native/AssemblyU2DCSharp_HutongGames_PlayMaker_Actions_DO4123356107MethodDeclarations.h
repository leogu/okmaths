﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformMoveX
struct DOTweenTransformMoveX_t4123356107;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveX::.ctor()
extern "C"  void DOTweenTransformMoveX__ctor_m1344182389 (DOTweenTransformMoveX_t4123356107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveX::Reset()
extern "C"  void DOTweenTransformMoveX_Reset_m1811674568 (DOTweenTransformMoveX_t4123356107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveX::OnEnter()
extern "C"  void DOTweenTransformMoveX_OnEnter_m3683330722 (DOTweenTransformMoveX_t4123356107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveX::<OnEnter>m__C6()
extern "C"  void DOTweenTransformMoveX_U3COnEnterU3Em__C6_m3635749364 (DOTweenTransformMoveX_t4123356107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveX::<OnEnter>m__C7()
extern "C"  void DOTweenTransformMoveX_U3COnEnterU3Em__C7_m3635749459 (DOTweenTransformMoveX_t4123356107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
