﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraRect
struct DOTweenCameraRect_t2477401429;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraRect::.ctor()
extern "C"  void DOTweenCameraRect__ctor_m2146582729 (DOTweenCameraRect_t2477401429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraRect::Reset()
extern "C"  void DOTweenCameraRect_Reset_m2707131950 (DOTweenCameraRect_t2477401429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraRect::OnEnter()
extern "C"  void DOTweenCameraRect_OnEnter_m3696316484 (DOTweenCameraRect_t2477401429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraRect::<OnEnter>m__30()
extern "C"  void DOTweenCameraRect_U3COnEnterU3Em__30_m2143242108 (DOTweenCameraRect_t2477401429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraRect::<OnEnter>m__31()
extern "C"  void DOTweenCameraRect_U3COnEnterU3Em__31_m2143242013 (DOTweenCameraRect_t2477401429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
