﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll
struct  DOTweenControlMethodsKillAll_t2793033768  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll::complete
	FsmBool_t664485696 * ___complete_11;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll::idsToExclude
	FsmStringU5BU5D_t2699231328* ___idsToExclude_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll::debugThis
	FsmBool_t664485696 * ___debugThis_13;

public:
	inline static int32_t get_offset_of_complete_11() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsKillAll_t2793033768, ___complete_11)); }
	inline FsmBool_t664485696 * get_complete_11() const { return ___complete_11; }
	inline FsmBool_t664485696 ** get_address_of_complete_11() { return &___complete_11; }
	inline void set_complete_11(FsmBool_t664485696 * value)
	{
		___complete_11 = value;
		Il2CppCodeGenWriteBarrier(&___complete_11, value);
	}

	inline static int32_t get_offset_of_idsToExclude_12() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsKillAll_t2793033768, ___idsToExclude_12)); }
	inline FsmStringU5BU5D_t2699231328* get_idsToExclude_12() const { return ___idsToExclude_12; }
	inline FsmStringU5BU5D_t2699231328** get_address_of_idsToExclude_12() { return &___idsToExclude_12; }
	inline void set_idsToExclude_12(FsmStringU5BU5D_t2699231328* value)
	{
		___idsToExclude_12 = value;
		Il2CppCodeGenWriteBarrier(&___idsToExclude_12, value);
	}

	inline static int32_t get_offset_of_debugThis_13() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsKillAll_t2793033768, ___debugThis_13)); }
	inline FsmBool_t664485696 * get_debugThis_13() const { return ___debugThis_13; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_13() { return &___debugThis_13; }
	inline void set_debugThis_13(FsmBool_t664485696 * value)
	{
		___debugThis_13 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
