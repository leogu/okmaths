﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t3371940109;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0__ctor_m2591525404 (U3CU3Ec__DisplayClass8_0_t3371940109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0::<DOMinSize>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m3735793539 (U3CU3Ec__DisplayClass8_0_t3371940109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m2021315045 (U3CU3Ec__DisplayClass8_0_t3371940109 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
