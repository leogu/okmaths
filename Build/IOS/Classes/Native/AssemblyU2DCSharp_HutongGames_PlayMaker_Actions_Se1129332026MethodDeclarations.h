﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightType
struct SetLightType_t1129332026;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightType::.ctor()
extern "C"  void SetLightType__ctor_m766142556 (SetLightType_t1129332026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightType::Reset()
extern "C"  void SetLightType_Reset_m2730143531 (SetLightType_t1129332026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightType::OnEnter()
extern "C"  void SetLightType_OnEnter_m2435122315 (SetLightType_t1129332026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightType::DoSetLightType()
extern "C"  void SetLightType_DoSetLightType_m331781633 (SetLightType_t1129332026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
