﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListShuffle
struct  ArrayListShuffle_t350463858  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListShuffle::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListShuffle::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListShuffle::startIndex
	FsmInt_t1273009179 * ___startIndex_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListShuffle::shufflingRange
	FsmInt_t1273009179 * ___shufflingRange_15;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListShuffle_t350463858, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListShuffle_t350463858, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_startIndex_14() { return static_cast<int32_t>(offsetof(ArrayListShuffle_t350463858, ___startIndex_14)); }
	inline FsmInt_t1273009179 * get_startIndex_14() const { return ___startIndex_14; }
	inline FsmInt_t1273009179 ** get_address_of_startIndex_14() { return &___startIndex_14; }
	inline void set_startIndex_14(FsmInt_t1273009179 * value)
	{
		___startIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_14, value);
	}

	inline static int32_t get_offset_of_shufflingRange_15() { return static_cast<int32_t>(offsetof(ArrayListShuffle_t350463858, ___shufflingRange_15)); }
	inline FsmInt_t1273009179 * get_shufflingRange_15() const { return ___shufflingRange_15; }
	inline FsmInt_t1273009179 ** get_address_of_shufflingRange_15() { return &___shufflingRange_15; }
	inline void set_shufflingRange_15(FsmInt_t1273009179 * value)
	{
		___shufflingRange_15 = value;
		Il2CppCodeGenWriteBarrier(&___shufflingRange_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
