﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget
struct GetAnimatorIsMatchingTarget_t752236675;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::.ctor()
extern "C"  void GetAnimatorIsMatchingTarget__ctor_m441680351 (GetAnimatorIsMatchingTarget_t752236675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::Reset()
extern "C"  void GetAnimatorIsMatchingTarget_Reset_m3559590756 (GetAnimatorIsMatchingTarget_t752236675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnEnter()
extern "C"  void GetAnimatorIsMatchingTarget_OnEnter_m320953714 (GetAnimatorIsMatchingTarget_t752236675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::OnActionUpdate()
extern "C"  void GetAnimatorIsMatchingTarget_OnActionUpdate_m1489845049 (GetAnimatorIsMatchingTarget_t752236675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsMatchingTarget::DoCheckIsMatchingActive()
extern "C"  void GetAnimatorIsMatchingTarget_DoCheckIsMatchingActive_m1381364847 (GetAnimatorIsMatchingTarget_t752236675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
