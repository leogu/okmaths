﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale
struct  DOTweenTransformShakeScale_t131829328  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::strength
	FsmVector3_t3996534004 * ___strength_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::vibrato
	FsmInt_t1273009179 * ___vibrato_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::randomness
	FsmFloat_t937133978 * ___randomness_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::duration
	FsmFloat_t937133978 * ___duration_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::startDelay
	FsmFloat_t937133978 * ___startDelay_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::startEvent
	FsmEvent_t1258573736 * ___startEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_20;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::tweenIdDescription
	String_t* ___tweenIdDescription_21;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::tweenIdType
	int32_t ___tweenIdType_22;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::stringAsId
	FsmString_t2414474701 * ___stringAsId_23;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::tagAsId
	FsmString_t2414474701 * ___tagAsId_24;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::selectedEase
	int32_t ___selectedEase_25;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::easeType
	int32_t ___easeType_26;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_27;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_28;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::loops
	FsmInt_t1273009179 * ___loops_29;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::loopType
	int32_t ___loopType_30;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_31;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::recyclable
	FsmBool_t664485696 * ___recyclable_32;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::updateType
	int32_t ___updateType_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::debugThis
	FsmBool_t664485696 * ___debugThis_35;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::tweener
	Tweener_t760404022 * ___tweener_36;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_strength_12() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___strength_12)); }
	inline FsmVector3_t3996534004 * get_strength_12() const { return ___strength_12; }
	inline FsmVector3_t3996534004 ** get_address_of_strength_12() { return &___strength_12; }
	inline void set_strength_12(FsmVector3_t3996534004 * value)
	{
		___strength_12 = value;
		Il2CppCodeGenWriteBarrier(&___strength_12, value);
	}

	inline static int32_t get_offset_of_vibrato_13() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___vibrato_13)); }
	inline FsmInt_t1273009179 * get_vibrato_13() const { return ___vibrato_13; }
	inline FsmInt_t1273009179 ** get_address_of_vibrato_13() { return &___vibrato_13; }
	inline void set_vibrato_13(FsmInt_t1273009179 * value)
	{
		___vibrato_13 = value;
		Il2CppCodeGenWriteBarrier(&___vibrato_13, value);
	}

	inline static int32_t get_offset_of_randomness_14() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___randomness_14)); }
	inline FsmFloat_t937133978 * get_randomness_14() const { return ___randomness_14; }
	inline FsmFloat_t937133978 ** get_address_of_randomness_14() { return &___randomness_14; }
	inline void set_randomness_14(FsmFloat_t937133978 * value)
	{
		___randomness_14 = value;
		Il2CppCodeGenWriteBarrier(&___randomness_14, value);
	}

	inline static int32_t get_offset_of_duration_15() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___duration_15)); }
	inline FsmFloat_t937133978 * get_duration_15() const { return ___duration_15; }
	inline FsmFloat_t937133978 ** get_address_of_duration_15() { return &___duration_15; }
	inline void set_duration_15(FsmFloat_t937133978 * value)
	{
		___duration_15 = value;
		Il2CppCodeGenWriteBarrier(&___duration_15, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_16() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___setSpeedBased_16)); }
	inline FsmBool_t664485696 * get_setSpeedBased_16() const { return ___setSpeedBased_16; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_16() { return &___setSpeedBased_16; }
	inline void set_setSpeedBased_16(FsmBool_t664485696 * value)
	{
		___setSpeedBased_16 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_16, value);
	}

	inline static int32_t get_offset_of_startDelay_17() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___startDelay_17)); }
	inline FsmFloat_t937133978 * get_startDelay_17() const { return ___startDelay_17; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_17() { return &___startDelay_17; }
	inline void set_startDelay_17(FsmFloat_t937133978 * value)
	{
		___startDelay_17 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_17, value);
	}

	inline static int32_t get_offset_of_startEvent_18() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___startEvent_18)); }
	inline FsmEvent_t1258573736 * get_startEvent_18() const { return ___startEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_18() { return &___startEvent_18; }
	inline void set_startEvent_18(FsmEvent_t1258573736 * value)
	{
		___startEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_18, value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___finishEvent_19)); }
	inline FsmEvent_t1258573736 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t1258573736 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_19, value);
	}

	inline static int32_t get_offset_of_finishImmediately_20() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___finishImmediately_20)); }
	inline FsmBool_t664485696 * get_finishImmediately_20() const { return ___finishImmediately_20; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_20() { return &___finishImmediately_20; }
	inline void set_finishImmediately_20(FsmBool_t664485696 * value)
	{
		___finishImmediately_20 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_20, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_21() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___tweenIdDescription_21)); }
	inline String_t* get_tweenIdDescription_21() const { return ___tweenIdDescription_21; }
	inline String_t** get_address_of_tweenIdDescription_21() { return &___tweenIdDescription_21; }
	inline void set_tweenIdDescription_21(String_t* value)
	{
		___tweenIdDescription_21 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_21, value);
	}

	inline static int32_t get_offset_of_tweenIdType_22() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___tweenIdType_22)); }
	inline int32_t get_tweenIdType_22() const { return ___tweenIdType_22; }
	inline int32_t* get_address_of_tweenIdType_22() { return &___tweenIdType_22; }
	inline void set_tweenIdType_22(int32_t value)
	{
		___tweenIdType_22 = value;
	}

	inline static int32_t get_offset_of_stringAsId_23() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___stringAsId_23)); }
	inline FsmString_t2414474701 * get_stringAsId_23() const { return ___stringAsId_23; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_23() { return &___stringAsId_23; }
	inline void set_stringAsId_23(FsmString_t2414474701 * value)
	{
		___stringAsId_23 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_23, value);
	}

	inline static int32_t get_offset_of_tagAsId_24() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___tagAsId_24)); }
	inline FsmString_t2414474701 * get_tagAsId_24() const { return ___tagAsId_24; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_24() { return &___tagAsId_24; }
	inline void set_tagAsId_24(FsmString_t2414474701 * value)
	{
		___tagAsId_24 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_24, value);
	}

	inline static int32_t get_offset_of_selectedEase_25() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___selectedEase_25)); }
	inline int32_t get_selectedEase_25() const { return ___selectedEase_25; }
	inline int32_t* get_address_of_selectedEase_25() { return &___selectedEase_25; }
	inline void set_selectedEase_25(int32_t value)
	{
		___selectedEase_25 = value;
	}

	inline static int32_t get_offset_of_easeType_26() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___easeType_26)); }
	inline int32_t get_easeType_26() const { return ___easeType_26; }
	inline int32_t* get_address_of_easeType_26() { return &___easeType_26; }
	inline void set_easeType_26(int32_t value)
	{
		___easeType_26 = value;
	}

	inline static int32_t get_offset_of_animationCurve_27() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___animationCurve_27)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_27() const { return ___animationCurve_27; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_27() { return &___animationCurve_27; }
	inline void set_animationCurve_27(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_27 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_27, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_28() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___loopsDescriptionArea_28)); }
	inline String_t* get_loopsDescriptionArea_28() const { return ___loopsDescriptionArea_28; }
	inline String_t** get_address_of_loopsDescriptionArea_28() { return &___loopsDescriptionArea_28; }
	inline void set_loopsDescriptionArea_28(String_t* value)
	{
		___loopsDescriptionArea_28 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_28, value);
	}

	inline static int32_t get_offset_of_loops_29() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___loops_29)); }
	inline FsmInt_t1273009179 * get_loops_29() const { return ___loops_29; }
	inline FsmInt_t1273009179 ** get_address_of_loops_29() { return &___loops_29; }
	inline void set_loops_29(FsmInt_t1273009179 * value)
	{
		___loops_29 = value;
		Il2CppCodeGenWriteBarrier(&___loops_29, value);
	}

	inline static int32_t get_offset_of_loopType_30() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___loopType_30)); }
	inline int32_t get_loopType_30() const { return ___loopType_30; }
	inline int32_t* get_address_of_loopType_30() { return &___loopType_30; }
	inline void set_loopType_30(int32_t value)
	{
		___loopType_30 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_31() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___autoKillOnCompletion_31)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_31() const { return ___autoKillOnCompletion_31; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_31() { return &___autoKillOnCompletion_31; }
	inline void set_autoKillOnCompletion_31(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_31 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_31, value);
	}

	inline static int32_t get_offset_of_recyclable_32() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___recyclable_32)); }
	inline FsmBool_t664485696 * get_recyclable_32() const { return ___recyclable_32; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_32() { return &___recyclable_32; }
	inline void set_recyclable_32(FsmBool_t664485696 * value)
	{
		___recyclable_32 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_32, value);
	}

	inline static int32_t get_offset_of_updateType_33() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___updateType_33)); }
	inline int32_t get_updateType_33() const { return ___updateType_33; }
	inline int32_t* get_address_of_updateType_33() { return &___updateType_33; }
	inline void set_updateType_33(int32_t value)
	{
		___updateType_33 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_34() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___isIndependentUpdate_34)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_34() const { return ___isIndependentUpdate_34; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_34() { return &___isIndependentUpdate_34; }
	inline void set_isIndependentUpdate_34(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_34 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_34, value);
	}

	inline static int32_t get_offset_of_debugThis_35() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___debugThis_35)); }
	inline FsmBool_t664485696 * get_debugThis_35() const { return ___debugThis_35; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_35() { return &___debugThis_35; }
	inline void set_debugThis_35(FsmBool_t664485696 * value)
	{
		___debugThis_35 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_35, value);
	}

	inline static int32_t get_offset_of_tweener_36() { return static_cast<int32_t>(offsetof(DOTweenTransformShakeScale_t131829328, ___tweener_36)); }
	inline Tweener_t760404022 * get_tweener_36() const { return ___tweener_36; }
	inline Tweener_t760404022 ** get_address_of_tweener_36() { return &___tweener_36; }
	inline void set_tweener_36(Tweener_t760404022 * value)
	{
		___tweener_36 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
