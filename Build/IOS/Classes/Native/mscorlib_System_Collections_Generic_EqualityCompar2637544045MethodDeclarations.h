﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.RaycastHit2D>
struct EqualityComparer_1_t2637544045;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m1784823850_gshared (EqualityComparer_1_t2637544045 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m1784823850(__this, method) ((  void (*) (EqualityComparer_1_t2637544045 *, const MethodInfo*))EqualityComparer_1__ctor_m1784823850_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.RaycastHit2D>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m688343821_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m688343821(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m688343821_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.RaycastHit2D>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094337057_gshared (EqualityComparer_1_t2637544045 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094337057(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2637544045 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1094337057_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.RaycastHit2D>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1140571527_gshared (EqualityComparer_1_t2637544045 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1140571527(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2637544045 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m1140571527_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.RaycastHit2D>::get_Default()
extern "C"  EqualityComparer_1_t2637544045 * EqualityComparer_1_get_Default_m653497486_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m653497486(__this /* static, unused */, method) ((  EqualityComparer_1_t2637544045 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m653497486_gshared)(__this /* static, unused */, method)
