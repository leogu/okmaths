﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToInt
struct ConvertBoolToInt_t1637719225;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::.ctor()
extern "C"  void ConvertBoolToInt__ctor_m2783469683 (ConvertBoolToInt_t1637719225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::Reset()
extern "C"  void ConvertBoolToInt_Reset_m1064169546 (ConvertBoolToInt_t1637719225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::OnEnter()
extern "C"  void ConvertBoolToInt_OnEnter_m1554390436 (ConvertBoolToInt_t1637719225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::OnUpdate()
extern "C"  void ConvertBoolToInt_OnUpdate_m555097027 (ConvertBoolToInt_t1637719225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToInt::DoConvertBoolToInt()
extern "C"  void ConvertBoolToInt_DoConvertBoolToInt_m1137780673 (ConvertBoolToInt_t1637719225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
