﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiTransitionGetType
struct uGuiTransitionGetType_t1900312903;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionGetType::.ctor()
extern "C"  void uGuiTransitionGetType__ctor_m1580304333 (uGuiTransitionGetType_t1900312903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionGetType::Reset()
extern "C"  void uGuiTransitionGetType_Reset_m2063977992 (uGuiTransitionGetType_t1900312903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionGetType::OnEnter()
extern "C"  void uGuiTransitionGetType_OnEnter_m1695958418 (uGuiTransitionGetType_t1900312903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionGetType::DoGetValue()
extern "C"  void uGuiTransitionGetType_DoGetValue_m4255854803 (uGuiTransitionGetType_t1900312903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
