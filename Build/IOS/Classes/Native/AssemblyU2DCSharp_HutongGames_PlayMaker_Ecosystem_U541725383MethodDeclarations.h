﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.Owner
struct Owner_t541725383;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.Owner::.ctor()
extern "C"  void Owner__ctor_m3854551315 (Owner_t541725383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
