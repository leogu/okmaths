﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetControllerCollisionFlags
struct GetControllerCollisionFlags_t73954747;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::.ctor()
extern "C"  void GetControllerCollisionFlags__ctor_m2846830233 (GetControllerCollisionFlags_t73954747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::Reset()
extern "C"  void GetControllerCollisionFlags_Reset_m2719079484 (GetControllerCollisionFlags_t73954747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetControllerCollisionFlags::OnUpdate()
extern "C"  void GetControllerCollisionFlags_OnUpdate_m2969994781 (GetControllerCollisionFlags_t73954747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
