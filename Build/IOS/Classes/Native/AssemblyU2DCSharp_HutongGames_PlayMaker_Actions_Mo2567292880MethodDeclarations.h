﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MousePick2d
struct MousePick2d_t2567292880;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MousePick2d::.ctor()
extern "C"  void MousePick2d__ctor_m277952006 (MousePick2d_t2567292880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2d::Reset()
extern "C"  void MousePick2d_Reset_m1855768209 (MousePick2d_t2567292880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2d::OnEnter()
extern "C"  void MousePick2d_OnEnter_m1144253113 (MousePick2d_t2567292880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2d::OnUpdate()
extern "C"  void MousePick2d_OnUpdate_m2496097288 (MousePick2d_t2567292880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick2d::DoMousePick2d()
extern "C"  void MousePick2d_DoMousePick2d_m2005797389 (MousePick2d_t2567292880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
