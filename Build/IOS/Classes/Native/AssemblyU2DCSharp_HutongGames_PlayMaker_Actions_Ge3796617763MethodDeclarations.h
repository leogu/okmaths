﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector
struct GetQuaternionMultipliedByVector_t3796617763;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::.ctor()
extern "C"  void GetQuaternionMultipliedByVector__ctor_m2705100435 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::Reset()
extern "C"  void GetQuaternionMultipliedByVector_Reset_m3396847200 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnEnter()
extern "C"  void GetQuaternionMultipliedByVector_OnEnter_m3689754814 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnUpdate_m1243389923 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnLateUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnLateUpdate_m269573207 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::OnFixedUpdate()
extern "C"  void GetQuaternionMultipliedByVector_OnFixedUpdate_m1139990937 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByVector::DoQuatMult()
extern "C"  void GetQuaternionMultipliedByVector_DoQuatMult_m2565350433 (GetQuaternionMultipliedByVector_t3796617763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
