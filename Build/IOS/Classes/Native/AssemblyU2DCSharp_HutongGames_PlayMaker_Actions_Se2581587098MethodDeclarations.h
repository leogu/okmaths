﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTextureOffset
struct SetTextureOffset_t2581587098;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::.ctor()
extern "C"  void SetTextureOffset__ctor_m1595703704 (SetTextureOffset_t2581587098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::Reset()
extern "C"  void SetTextureOffset_Reset_m3930481055 (SetTextureOffset_t2581587098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::OnEnter()
extern "C"  void SetTextureOffset_OnEnter_m3673733783 (SetTextureOffset_t2581587098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::OnUpdate()
extern "C"  void SetTextureOffset_OnUpdate_m2278631750 (SetTextureOffset_t2581587098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureOffset::DoSetTextureOffset()
extern "C"  void SetTextureOffset_DoSetTextureOffset_m3220289025 (SetTextureOffset_t2581587098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
