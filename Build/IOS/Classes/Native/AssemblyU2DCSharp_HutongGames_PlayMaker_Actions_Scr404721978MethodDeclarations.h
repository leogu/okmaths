﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScreenSetResolution
struct ScreenSetResolution_t404721978;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScreenSetResolution::.ctor()
extern "C"  void ScreenSetResolution__ctor_m573566024 (ScreenSetResolution_t404721978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenSetResolution::Reset()
extern "C"  void ScreenSetResolution_Reset_m3596657083 (ScreenSetResolution_t404721978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenSetResolution::OnEnter()
extern "C"  void ScreenSetResolution_OnEnter_m4250725027 (ScreenSetResolution_t404721978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
