﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RewardAd_menu
struct RewardAd_menu_t3222548784;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen456670012.h"

// System.Void RewardAd_menu::.ctor()
extern "C"  void RewardAd_menu__ctor_m2496481793 (RewardAd_menu_t3222548784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RewardAd_menu::AdShow()
extern "C"  void RewardAd_menu_AdShow_m945105799 (RewardAd_menu_t3222548784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RewardAd_menu::HandleShowResult(UnityEngine.Advertisements.ShowResult)
extern "C"  void RewardAd_menu_HandleShowResult_m4011103322 (RewardAd_menu_t3222548784 * __this, int32_t ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
