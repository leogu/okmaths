﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.AsyncExec::.cctor()
extern "C"  void AsyncExec__cctor_m2415655067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour UnityEngine.Advertisements.AsyncExec::get_coroutineHost()
extern "C"  MonoBehaviour_t1158329972 * AsyncExec_get_coroutineHost_m1572365565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.Advertisements.AsyncExec::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * AsyncExec_StartCoroutine_m979833225 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
