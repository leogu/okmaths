﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Multiply
struct Vector3Multiply_t465395352;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::.ctor()
extern "C"  void Vector3Multiply__ctor_m3071073770 (Vector3Multiply_t465395352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::Reset()
extern "C"  void Vector3Multiply_Reset_m1104453813 (Vector3Multiply_t465395352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::OnEnter()
extern "C"  void Vector3Multiply_OnEnter_m1995884877 (Vector3Multiply_t465395352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Multiply::OnUpdate()
extern "C"  void Vector3Multiply_OnUpdate_m3687118948 (Vector3Multiply_t465395352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
