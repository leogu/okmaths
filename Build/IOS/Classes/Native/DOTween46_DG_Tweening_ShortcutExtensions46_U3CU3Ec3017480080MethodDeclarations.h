﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t3017480080;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m2864467495 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
