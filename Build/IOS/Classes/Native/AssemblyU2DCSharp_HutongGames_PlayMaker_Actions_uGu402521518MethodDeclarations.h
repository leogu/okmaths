﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit
struct uGuiInputFieldGetCharacterLimit_t402521518;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::.ctor()
extern "C"  void uGuiInputFieldGetCharacterLimit__ctor_m1817245234 (uGuiInputFieldGetCharacterLimit_t402521518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::Reset()
extern "C"  void uGuiInputFieldGetCharacterLimit_Reset_m3229580239 (uGuiInputFieldGetCharacterLimit_t402521518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::OnEnter()
extern "C"  void uGuiInputFieldGetCharacterLimit_OnEnter_m1376366783 (uGuiInputFieldGetCharacterLimit_t402521518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::OnUpdate()
extern "C"  void uGuiInputFieldGetCharacterLimit_OnUpdate_m1078041460 (uGuiInputFieldGetCharacterLimit_t402521518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::DoGetValue()
extern "C"  void uGuiInputFieldGetCharacterLimit_DoGetValue_m131102716 (uGuiInputFieldGetCharacterLimit_t402521518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
