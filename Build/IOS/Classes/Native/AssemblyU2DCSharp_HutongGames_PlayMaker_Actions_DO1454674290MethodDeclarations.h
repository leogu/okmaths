﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformJump
struct DOTweenTransformJump_t1454674290;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformJump::.ctor()
extern "C"  void DOTweenTransformJump__ctor_m2804556920 (DOTweenTransformJump_t1454674290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformJump::Reset()
extern "C"  void DOTweenTransformJump_Reset_m1967993855 (DOTweenTransformJump_t1454674290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformJump::OnEnter()
extern "C"  void DOTweenTransformJump_OnEnter_m3361249983 (DOTweenTransformJump_t1454674290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformJump::<OnEnter>m__B0()
extern "C"  void DOTweenTransformJump_U3COnEnterU3Em__B0_m3176518266 (DOTweenTransformJump_t1454674290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformJump::<OnEnter>m__B1()
extern "C"  void DOTweenTransformJump_U3COnEnterU3Em__B1_m427431935 (DOTweenTransformJump_t1454674290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
