﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayerCount
struct GetAnimatorLayerCount_t3104820689;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::.ctor()
extern "C"  void GetAnimatorLayerCount__ctor_m1860046179 (GetAnimatorLayerCount_t3104820689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::Reset()
extern "C"  void GetAnimatorLayerCount_Reset_m3617216174 (GetAnimatorLayerCount_t3104820689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::OnEnter()
extern "C"  void GetAnimatorLayerCount_OnEnter_m3203580648 (GetAnimatorLayerCount_t3104820689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerCount::DoGetLayerCount()
extern "C"  void GetAnimatorLayerCount_DoGetLayerCount_m2830720774 (GetAnimatorLayerCount_t3104820689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
