﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById
struct  DOTweenControlMethodsRestartById_t1315485730  : public FsmStateAction_t2862378169
{
public:
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::tweenIdType
	int32_t ___tweenIdType_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::stringAsId
	FsmString_t2414474701 * ___stringAsId_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::tagAsId
	FsmString_t2414474701 * ___tagAsId_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::gameObjectAsId
	FsmGameObject_t3097142863 * ___gameObjectAsId_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::includeDelay
	FsmBool_t664485696 * ___includeDelay_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::debugThis
	FsmBool_t664485696 * ___debugThis_16;

public:
	inline static int32_t get_offset_of_tweenIdType_11() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsRestartById_t1315485730, ___tweenIdType_11)); }
	inline int32_t get_tweenIdType_11() const { return ___tweenIdType_11; }
	inline int32_t* get_address_of_tweenIdType_11() { return &___tweenIdType_11; }
	inline void set_tweenIdType_11(int32_t value)
	{
		___tweenIdType_11 = value;
	}

	inline static int32_t get_offset_of_stringAsId_12() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsRestartById_t1315485730, ___stringAsId_12)); }
	inline FsmString_t2414474701 * get_stringAsId_12() const { return ___stringAsId_12; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_12() { return &___stringAsId_12; }
	inline void set_stringAsId_12(FsmString_t2414474701 * value)
	{
		___stringAsId_12 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_12, value);
	}

	inline static int32_t get_offset_of_tagAsId_13() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsRestartById_t1315485730, ___tagAsId_13)); }
	inline FsmString_t2414474701 * get_tagAsId_13() const { return ___tagAsId_13; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_13() { return &___tagAsId_13; }
	inline void set_tagAsId_13(FsmString_t2414474701 * value)
	{
		___tagAsId_13 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_13, value);
	}

	inline static int32_t get_offset_of_gameObjectAsId_14() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsRestartById_t1315485730, ___gameObjectAsId_14)); }
	inline FsmGameObject_t3097142863 * get_gameObjectAsId_14() const { return ___gameObjectAsId_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObjectAsId_14() { return &___gameObjectAsId_14; }
	inline void set_gameObjectAsId_14(FsmGameObject_t3097142863 * value)
	{
		___gameObjectAsId_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectAsId_14, value);
	}

	inline static int32_t get_offset_of_includeDelay_15() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsRestartById_t1315485730, ___includeDelay_15)); }
	inline FsmBool_t664485696 * get_includeDelay_15() const { return ___includeDelay_15; }
	inline FsmBool_t664485696 ** get_address_of_includeDelay_15() { return &___includeDelay_15; }
	inline void set_includeDelay_15(FsmBool_t664485696 * value)
	{
		___includeDelay_15 = value;
		Il2CppCodeGenWriteBarrier(&___includeDelay_15, value);
	}

	inline static int32_t get_offset_of_debugThis_16() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsRestartById_t1315485730, ___debugThis_16)); }
	inline FsmBool_t664485696 * get_debugThis_16() const { return ___debugThis_16; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_16() { return &___debugThis_16; }
	inline void set_debugThis_16(FsmBool_t664485696 * value)
	{
		___debugThis_16 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
