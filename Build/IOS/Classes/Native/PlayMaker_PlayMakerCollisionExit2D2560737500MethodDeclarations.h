﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionExit2D
struct PlayMakerCollisionExit2D_t2560737500;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void PlayMakerCollisionExit2D::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionExit2D_OnCollisionExit2D_m4121418717 (PlayMakerCollisionExit2D_t2560737500 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionExit2D::.ctor()
extern "C"  void PlayMakerCollisionExit2D__ctor_m4147787879 (PlayMakerCollisionExit2D_t2560737500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
