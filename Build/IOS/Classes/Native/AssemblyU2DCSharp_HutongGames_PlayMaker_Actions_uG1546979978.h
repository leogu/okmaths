﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture
struct  uGuiRawImageSetTexture_t1546979978  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::texture
	FsmTexture_t3372293163 * ___texture_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.RawImage HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::_texture
	RawImage_t2749640213 * ____texture_14;
	// UnityEngine.Texture HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::_originalTexture
	Texture_t2243626319 * ____originalTexture_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiRawImageSetTexture_t1546979978, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_texture_12() { return static_cast<int32_t>(offsetof(uGuiRawImageSetTexture_t1546979978, ___texture_12)); }
	inline FsmTexture_t3372293163 * get_texture_12() const { return ___texture_12; }
	inline FsmTexture_t3372293163 ** get_address_of_texture_12() { return &___texture_12; }
	inline void set_texture_12(FsmTexture_t3372293163 * value)
	{
		___texture_12 = value;
		Il2CppCodeGenWriteBarrier(&___texture_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiRawImageSetTexture_t1546979978, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__texture_14() { return static_cast<int32_t>(offsetof(uGuiRawImageSetTexture_t1546979978, ____texture_14)); }
	inline RawImage_t2749640213 * get__texture_14() const { return ____texture_14; }
	inline RawImage_t2749640213 ** get_address_of__texture_14() { return &____texture_14; }
	inline void set__texture_14(RawImage_t2749640213 * value)
	{
		____texture_14 = value;
		Il2CppCodeGenWriteBarrier(&____texture_14, value);
	}

	inline static int32_t get_offset_of__originalTexture_15() { return static_cast<int32_t>(offsetof(uGuiRawImageSetTexture_t1546979978, ____originalTexture_15)); }
	inline Texture_t2243626319 * get__originalTexture_15() const { return ____originalTexture_15; }
	inline Texture_t2243626319 ** get_address_of__originalTexture_15() { return &____originalTexture_15; }
	inline void set__originalTexture_15(Texture_t2243626319 * value)
	{
		____originalTexture_15 = value;
		Il2CppCodeGenWriteBarrier(&____originalTexture_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
