﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerSettings
struct ControllerSettings_t324675351;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::.ctor()
extern "C"  void ControllerSettings__ctor_m2975862271 (ControllerSettings_t324675351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::Reset()
extern "C"  void ControllerSettings_Reset_m640788872 (ControllerSettings_t324675351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::OnEnter()
extern "C"  void ControllerSettings_OnEnter_m4289818070 (ControllerSettings_t324675351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::OnUpdate()
extern "C"  void ControllerSettings_OnUpdate_m730696231 (ControllerSettings_t324675351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSettings::DoControllerSettings()
extern "C"  void ControllerSettings_DoControllerSettings_m344739713 (ControllerSettings_t324675351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
