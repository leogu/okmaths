﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue
struct  uGuiScrollbarGetValue_t2028754341  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::value
	FsmFloat_t937133978 * ___value_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::_scrollbar
	Scrollbar_t3248359358 * ____scrollbar_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiScrollbarGetValue_t2028754341, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiScrollbarGetValue_t2028754341, ___value_12)); }
	inline FsmFloat_t937133978 * get_value_12() const { return ___value_12; }
	inline FsmFloat_t937133978 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmFloat_t937133978 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(uGuiScrollbarGetValue_t2028754341, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__scrollbar_14() { return static_cast<int32_t>(offsetof(uGuiScrollbarGetValue_t2028754341, ____scrollbar_14)); }
	inline Scrollbar_t3248359358 * get__scrollbar_14() const { return ____scrollbar_14; }
	inline Scrollbar_t3248359358 ** get_address_of__scrollbar_14() { return &____scrollbar_14; }
	inline void set__scrollbar_14(Scrollbar_t3248359358 * value)
	{
		____scrollbar_14 = value;
		Il2CppCodeGenWriteBarrier(&____scrollbar_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
