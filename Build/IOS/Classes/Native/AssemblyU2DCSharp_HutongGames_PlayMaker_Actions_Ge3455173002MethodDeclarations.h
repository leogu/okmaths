﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCullingMode
struct GetAnimatorCullingMode_t3455173002;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::.ctor()
extern "C"  void GetAnimatorCullingMode__ctor_m1466560904 (GetAnimatorCullingMode_t3455173002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::Reset()
extern "C"  void GetAnimatorCullingMode_Reset_m3798615759 (GetAnimatorCullingMode_t3455173002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::OnEnter()
extern "C"  void GetAnimatorCullingMode_OnEnter_m4180379143 (GetAnimatorCullingMode_t3455173002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCullingMode::DoCheckCulling()
extern "C"  void GetAnimatorCullingMode_DoCheckCulling_m2187948253 (GetAnimatorCullingMode_t3455173002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
