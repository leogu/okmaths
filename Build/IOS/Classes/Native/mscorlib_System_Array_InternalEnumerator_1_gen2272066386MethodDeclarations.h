﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2272066386.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21413314124.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3988125135_gshared (InternalEnumerator_1_t2272066386 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3988125135(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2272066386 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3988125135_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m480954479_gshared (InternalEnumerator_1_t2272066386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m480954479(__this, method) ((  void (*) (InternalEnumerator_1_t2272066386 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m480954479_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1133414503_gshared (InternalEnumerator_1_t2272066386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1133414503(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2272066386 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1133414503_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2611503668_gshared (InternalEnumerator_1_t2272066386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2611503668(__this, method) ((  void (*) (InternalEnumerator_1_t2272066386 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2611503668_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3228408955_gshared (InternalEnumerator_1_t2272066386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3228408955(__this, method) ((  bool (*) (InternalEnumerator_1_t2272066386 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3228408955_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>::get_Current()
extern "C"  KeyValuePair_2_t1413314124  InternalEnumerator_1_get_Current_m1051933472_gshared (InternalEnumerator_1_t2272066386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1051933472(__this, method) ((  KeyValuePair_2_t1413314124  (*) (InternalEnumerator_1_t2272066386 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1051933472_gshared)(__this, method)
