﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPunchScale
struct iTweenPunchScale_t1829239666;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::.ctor()
extern "C"  void iTweenPunchScale__ctor_m1343801028 (iTweenPunchScale_t1829239666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::Reset()
extern "C"  void iTweenPunchScale_Reset_m2141385955 (iTweenPunchScale_t1829239666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::OnEnter()
extern "C"  void iTweenPunchScale_OnEnter_m1995734723 (iTweenPunchScale_t1829239666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::OnExit()
extern "C"  void iTweenPunchScale_OnExit_m2678642555 (iTweenPunchScale_t1829239666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchScale::DoiTween()
extern "C"  void iTweenPunchScale_DoiTween_m383790293 (iTweenPunchScale_t1829239666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
