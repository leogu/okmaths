﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddTorque2d
struct AddTorque2d_t321609335;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::.ctor()
extern "C"  void AddTorque2d__ctor_m2892841213 (AddTorque2d_t321609335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnPreprocess()
extern "C"  void AddTorque2d_OnPreprocess_m3906834368 (AddTorque2d_t321609335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::Reset()
extern "C"  void AddTorque2d_Reset_m2934721048 (AddTorque2d_t321609335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnEnter()
extern "C"  void AddTorque2d_OnEnter_m1021942418 (AddTorque2d_t321609335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::OnFixedUpdate()
extern "C"  void AddTorque2d_OnFixedUpdate_m126767095 (AddTorque2d_t321609335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque2d::DoAddTorque()
extern "C"  void AddTorque2d_DoAddTorque_m4197282199 (AddTorque2d_t321609335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
