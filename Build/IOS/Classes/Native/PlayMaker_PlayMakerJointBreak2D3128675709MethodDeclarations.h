﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerJointBreak2D
struct PlayMakerJointBreak2D_t3128675709;
// UnityEngine.Joint2D
struct Joint2D_t854621618;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Joint2D854621618.h"

// System.Void PlayMakerJointBreak2D::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  void PlayMakerJointBreak2D_OnJointBreak2D_m1506345733 (PlayMakerJointBreak2D_t3128675709 * __this, Joint2D_t854621618 * ___brokenJoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerJointBreak2D::.ctor()
extern "C"  void PlayMakerJointBreak2D__ctor_m3549857740 (PlayMakerJointBreak2D_t3128675709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
