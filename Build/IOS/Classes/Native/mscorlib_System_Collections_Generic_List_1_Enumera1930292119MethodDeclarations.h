﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1765108910(__this, ___l0, method) ((  void (*) (Enumerator_t1930292119 *, List_1_t2395562445 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m61031644(__this, method) ((  void (*) (Enumerator_t1930292119 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2903993196(__this, method) ((  Il2CppObject * (*) (Enumerator_t1930292119 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::Dispose()
#define Enumerator_Dispose_m4063062081(__this, method) ((  void (*) (Enumerator_t1930292119 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::VerifyState()
#define Enumerator_VerifyState_m4144976560(__this, method) ((  void (*) (Enumerator_t1930292119 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::MoveNext()
#define Enumerator_MoveNext_m1751844752(__this, method) ((  bool (*) (Enumerator_t1930292119 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.NamedVariable>::get_Current()
#define Enumerator_get_Current_m2151877489(__this, method) ((  NamedVariable_t3026441313 * (*) (Enumerator_t1930292119 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
