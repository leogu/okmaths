﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption3848444473.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject3097142863.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault2023674184.h"

// HutongGames.PlayMaker.OwnerDefaultOption HutongGames.PlayMaker.FsmOwnerDefault::get_OwnerOption()
extern "C"  int32_t FsmOwnerDefault_get_OwnerOption_m3058082974 (FsmOwnerDefault_t2023674184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::set_OwnerOption(HutongGames.PlayMaker.OwnerDefaultOption)
extern "C"  void FsmOwnerDefault_set_OwnerOption_m3164177567 (FsmOwnerDefault_t2023674184 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmOwnerDefault::get_GameObject()
extern "C"  FsmGameObject_t3097142863 * FsmOwnerDefault_get_GameObject_m1490419887 (FsmOwnerDefault_t2023674184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::set_GameObject(HutongGames.PlayMaker.FsmGameObject)
extern "C"  void FsmOwnerDefault_set_GameObject_m735112238 (FsmOwnerDefault_t2023674184 * __this, FsmGameObject_t3097142863 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::.ctor()
extern "C"  void FsmOwnerDefault__ctor_m3513885059 (FsmOwnerDefault_t2023674184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmOwnerDefault::.ctor(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  void FsmOwnerDefault__ctor_m884709755 (FsmOwnerDefault_t2023674184 * __this, FsmOwnerDefault_t2023674184 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
