﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetHaloStrength
struct SetHaloStrength_t3078548475;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::.ctor()
extern "C"  void SetHaloStrength__ctor_m1987884655 (SetHaloStrength_t3078548475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::Reset()
extern "C"  void SetHaloStrength_Reset_m4215107028 (SetHaloStrength_t3078548475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::OnEnter()
extern "C"  void SetHaloStrength_OnEnter_m1894209818 (SetHaloStrength_t3078548475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::OnUpdate()
extern "C"  void SetHaloStrength_OnUpdate_m332704935 (SetHaloStrength_t3078548475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetHaloStrength::DoSetHaloStrength()
extern "C"  void SetHaloStrength_DoSetHaloStrength_m78246429 (SetHaloStrength_t3078548475 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
