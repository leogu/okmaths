﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMaterialColor
struct SetMaterialColor_t1899227458;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::.ctor()
extern "C"  void SetMaterialColor__ctor_m2688582300 (SetMaterialColor_t1899227458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::Reset()
extern "C"  void SetMaterialColor_Reset_m1003734411 (SetMaterialColor_t1899227458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::OnEnter()
extern "C"  void SetMaterialColor_OnEnter_m2434535571 (SetMaterialColor_t1899227458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::OnUpdate()
extern "C"  void SetMaterialColor_OnUpdate_m3813887066 (SetMaterialColor_t1899227458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialColor::DoSetMaterialColor()
extern "C"  void SetMaterialColor_DoSetMaterialColor_m627325505 (SetMaterialColor_t1899227458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
