﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetDeviceAcceleration
struct GetDeviceAcceleration_t4046517398;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::.ctor()
extern "C"  void GetDeviceAcceleration__ctor_m3021867188 (GetDeviceAcceleration_t4046517398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::Reset()
extern "C"  void GetDeviceAcceleration_Reset_m3769995855 (GetDeviceAcceleration_t4046517398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::OnEnter()
extern "C"  void GetDeviceAcceleration_OnEnter_m2799225231 (GetDeviceAcceleration_t4046517398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::OnUpdate()
extern "C"  void GetDeviceAcceleration_OnUpdate_m3762322242 (GetDeviceAcceleration_t4046517398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceAcceleration::DoGetDeviceAcceleration()
extern "C"  void GetDeviceAcceleration_DoGetDeviceAcceleration_m2438269913 (GetDeviceAcceleration_t4046517398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
