﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseAll
struct DOTweenControlMethodsTogglePauseAll_t3702429102;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseAll::.ctor()
extern "C"  void DOTweenControlMethodsTogglePauseAll__ctor_m148026246 (DOTweenControlMethodsTogglePauseAll_t3702429102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseAll::Reset()
extern "C"  void DOTweenControlMethodsTogglePauseAll_Reset_m698845067 (DOTweenControlMethodsTogglePauseAll_t3702429102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseAll::OnEnter()
extern "C"  void DOTweenControlMethodsTogglePauseAll_OnEnter_m3081543291 (DOTweenControlMethodsTogglePauseAll_t3702429102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
