﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetScale
struct SetScale_t179644456;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetScale::.ctor()
extern "C"  void SetScale__ctor_m623589046 (SetScale_t179644456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::Reset()
extern "C"  void SetScale_Reset_m3233129005 (SetScale_t179644456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnEnter()
extern "C"  void SetScale_OnEnter_m730561189 (SetScale_t179644456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnUpdate()
extern "C"  void SetScale_OnUpdate_m1274221880 (SetScale_t179644456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::OnLateUpdate()
extern "C"  void SetScale_OnLateUpdate_m2667260004 (SetScale_t179644456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetScale::DoSetScale()
extern "C"  void SetScale_DoSetScale_m160410689 (SetScale_t179644456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
