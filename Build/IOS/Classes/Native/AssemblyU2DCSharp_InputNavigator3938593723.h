﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t3340676209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputNavigator
struct  InputNavigator_t3938593723  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventSystem InputNavigator::system
	EventSystem_t3466835263 * ___system_2;
	// UnityEngine.KeyCode[] InputNavigator::NavigationKeys
	KeyCodeU5BU5D_t3340676209* ___NavigationKeys_3;
	// System.Boolean InputNavigator::_keyDown
	bool ____keyDown_4;

public:
	inline static int32_t get_offset_of_system_2() { return static_cast<int32_t>(offsetof(InputNavigator_t3938593723, ___system_2)); }
	inline EventSystem_t3466835263 * get_system_2() const { return ___system_2; }
	inline EventSystem_t3466835263 ** get_address_of_system_2() { return &___system_2; }
	inline void set_system_2(EventSystem_t3466835263 * value)
	{
		___system_2 = value;
		Il2CppCodeGenWriteBarrier(&___system_2, value);
	}

	inline static int32_t get_offset_of_NavigationKeys_3() { return static_cast<int32_t>(offsetof(InputNavigator_t3938593723, ___NavigationKeys_3)); }
	inline KeyCodeU5BU5D_t3340676209* get_NavigationKeys_3() const { return ___NavigationKeys_3; }
	inline KeyCodeU5BU5D_t3340676209** get_address_of_NavigationKeys_3() { return &___NavigationKeys_3; }
	inline void set_NavigationKeys_3(KeyCodeU5BU5D_t3340676209* value)
	{
		___NavigationKeys_3 = value;
		Il2CppCodeGenWriteBarrier(&___NavigationKeys_3, value);
	}

	inline static int32_t get_offset_of__keyDown_4() { return static_cast<int32_t>(offsetof(InputNavigator_t3938593723, ____keyDown_4)); }
	inline bool get__keyDown_4() const { return ____keyDown_4; }
	inline bool* get_address_of__keyDown_4() { return &____keyDown_4; }
	inline void set__keyDown_4(bool value)
	{
		____keyDown_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
