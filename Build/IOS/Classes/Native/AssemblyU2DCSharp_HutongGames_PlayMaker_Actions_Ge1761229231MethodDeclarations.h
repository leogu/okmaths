﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRectFields
struct GetRectFields_t1761229231;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRectFields::.ctor()
extern "C"  void GetRectFields__ctor_m842818777 (GetRectFields_t1761229231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::Reset()
extern "C"  void GetRectFields_Reset_m1528367268 (GetRectFields_t1761229231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::OnEnter()
extern "C"  void GetRectFields_OnEnter_m2973852486 (GetRectFields_t1761229231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::OnUpdate()
extern "C"  void GetRectFields_OnUpdate_m3360591021 (GetRectFields_t1761229231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRectFields::DoGetRectFields()
extern "C"  void GetRectFields_DoGetRectFields_m173938129 (GetRectFields_t1761229231 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
