﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MousePick
struct MousePick_t1663739482;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MousePick::.ctor()
extern "C"  void MousePick__ctor_m3591851984 (MousePick_t1663739482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::Reset()
extern "C"  void MousePick_Reset_m1881790259 (MousePick_t1663739482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::OnEnter()
extern "C"  void MousePick_OnEnter_m264730883 (MousePick_t1663739482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::OnUpdate()
extern "C"  void MousePick_OnUpdate_m3989696222 (MousePick_t1663739482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePick::DoMousePick()
extern "C"  void MousePick_DoMousePick_m220525673 (MousePick_t1663739482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
