﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMaterial
struct GetMaterial_t4064778829;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMaterial::.ctor()
extern "C"  void GetMaterial__ctor_m4019477587 (GetMaterial_t4064778829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::Reset()
extern "C"  void GetMaterial_Reset_m313481262 (GetMaterial_t4064778829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::OnEnter()
extern "C"  void GetMaterial_OnEnter_m3455411608 (GetMaterial_t4064778829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterial::DoGetMaterial()
extern "C"  void GetMaterial_DoGetMaterial_m402469917 (GetMaterial_t4064778829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
