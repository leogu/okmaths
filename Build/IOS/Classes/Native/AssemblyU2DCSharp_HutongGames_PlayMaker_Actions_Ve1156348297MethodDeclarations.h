﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Add
struct Vector3Add_t1156348297;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Add::.ctor()
extern "C"  void Vector3Add__ctor_m37117549 (Vector3Add_t1156348297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::Reset()
extern "C"  void Vector3Add_Reset_m217792470 (Vector3Add_t1156348297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::OnEnter()
extern "C"  void Vector3Add_OnEnter_m271235076 (Vector3Add_t1156348297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::OnUpdate()
extern "C"  void Vector3Add_OnUpdate_m4131556121 (Vector3Add_t1156348297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Add::DoVector3Add()
extern "C"  void Vector3Add_DoVector3Add_m3089014209 (Vector3Add_t1156348297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
