﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorStartRecording
struct AnimatorStartRecording_t1381290308;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::.ctor()
extern "C"  void AnimatorStartRecording__ctor_m2531511634 (AnimatorStartRecording_t1381290308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::Reset()
extern "C"  void AnimatorStartRecording_Reset_m809543217 (AnimatorStartRecording_t1381290308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStartRecording::OnEnter()
extern "C"  void AnimatorStartRecording_OnEnter_m1890715473 (AnimatorStartRecording_t1381290308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
