﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomColor
struct SelectRandomColor_t1687601772;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomColor::.ctor()
extern "C"  void SelectRandomColor__ctor_m3267412616 (SelectRandomColor_t1687601772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomColor::Reset()
extern "C"  void SelectRandomColor_Reset_m3831295373 (SelectRandomColor_t1687601772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomColor::OnEnter()
extern "C"  void SelectRandomColor_OnEnter_m601626989 (SelectRandomColor_t1687601772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomColor::DoSelectRandomColor()
extern "C"  void SelectRandomColor_DoSelectRandomColor_m3893593773 (SelectRandomColor_t1687601772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
