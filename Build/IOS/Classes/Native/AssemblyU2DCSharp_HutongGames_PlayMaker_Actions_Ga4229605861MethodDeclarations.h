﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectIsVisible
struct GameObjectIsVisible_t4229605861;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::.ctor()
extern "C"  void GameObjectIsVisible__ctor_m229405401 (GameObjectIsVisible_t4229605861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::Reset()
extern "C"  void GameObjectIsVisible_Reset_m3537983902 (GameObjectIsVisible_t4229605861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnEnter()
extern "C"  void GameObjectIsVisible_OnEnter_m2105944260 (GameObjectIsVisible_t4229605861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::OnUpdate()
extern "C"  void GameObjectIsVisible_OnUpdate_m3176849053 (GameObjectIsVisible_t4229605861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsVisible::DoIsVisible()
extern "C"  void GameObjectIsVisible_DoIsVisible_m1524478380 (GameObjectIsVisible_t4229605861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
