﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringJoin
struct StringJoin_t3100161295;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringJoin::.ctor()
extern "C"  void StringJoin__ctor_m1550226629 (StringJoin_t3100161295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringJoin::OnEnter()
extern "C"  void StringJoin_OnEnter_m2917030326 (StringJoin_t3100161295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
