﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraAspect
struct DOTweenCameraAspect_t3747723447;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraAspect::.ctor()
extern "C"  void DOTweenCameraAspect__ctor_m478948881 (DOTweenCameraAspect_t3747723447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraAspect::Reset()
extern "C"  void DOTweenCameraAspect_Reset_m1164505324 (DOTweenCameraAspect_t3747723447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraAspect::OnEnter()
extern "C"  void DOTweenCameraAspect_OnEnter_m3851863054 (DOTweenCameraAspect_t3747723447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraAspect::<OnEnter>m__22()
extern "C"  void DOTweenCameraAspect_U3COnEnterU3Em__22_m3569557687 (DOTweenCameraAspect_t3747723447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraAspect::<OnEnter>m__23()
extern "C"  void DOTweenCameraAspect_U3COnEnterU3Em__23_m3569557654 (DOTweenCameraAspect_t3747723447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
