﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatMultiply
struct FloatMultiply_t369067858;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::.ctor()
extern "C"  void FloatMultiply__ctor_m1688799916 (FloatMultiply_t369067858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::Reset()
extern "C"  void FloatMultiply_Reset_m1716332391 (FloatMultiply_t369067858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnEnter()
extern "C"  void FloatMultiply_OnEnter_m3431151671 (FloatMultiply_t369067858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatMultiply::OnUpdate()
extern "C"  void FloatMultiply_OnUpdate_m4203524778 (FloatMultiply_t369067858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
