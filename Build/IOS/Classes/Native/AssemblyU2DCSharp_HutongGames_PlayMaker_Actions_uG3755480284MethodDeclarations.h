﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderGetValue
struct uGuiSliderGetValue_t3755480284;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetValue::.ctor()
extern "C"  void uGuiSliderGetValue__ctor_m2776565850 (uGuiSliderGetValue_t3755480284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetValue::Reset()
extern "C"  void uGuiSliderGetValue_Reset_m1052940073 (uGuiSliderGetValue_t3755480284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetValue::OnEnter()
extern "C"  void uGuiSliderGetValue_OnEnter_m1388651865 (uGuiSliderGetValue_t3755480284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetValue::OnUpdate()
extern "C"  void uGuiSliderGetValue_OnUpdate_m2020182180 (uGuiSliderGetValue_t3755480284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetValue::DoGetValue()
extern "C"  void uGuiSliderGetValue_DoGetValue_m850496508 (uGuiSliderGetValue_t3755480284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
