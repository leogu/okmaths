﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SmoothLookAt
struct SmoothLookAt_t2808640686;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::.ctor()
extern "C"  void SmoothLookAt__ctor_m3807151024 (SmoothLookAt_t2808640686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::Reset()
extern "C"  void SmoothLookAt_Reset_m955590647 (SmoothLookAt_t2808640686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::OnEnter()
extern "C"  void SmoothLookAt_OnEnter_m2665308911 (SmoothLookAt_t2808640686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::OnLateUpdate()
extern "C"  void SmoothLookAt_OnLateUpdate_m1737322138 (SmoothLookAt_t2808640686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt::DoSmoothLookAt()
extern "C"  void SmoothLookAt_DoSmoothLookAt_m646597697 (SmoothLookAt_t2808640686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
