﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiTextGetText
struct uGuiTextGetText_t2404688918;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiTextGetText::.ctor()
extern "C"  void uGuiTextGetText__ctor_m922248242 (uGuiTextGetText_t2404688918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextGetText::Reset()
extern "C"  void uGuiTextGetText_Reset_m584148527 (uGuiTextGetText_t2404688918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextGetText::OnEnter()
extern "C"  void uGuiTextGetText_OnEnter_m3799985959 (uGuiTextGetText_t2404688918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextGetText::OnUpdate()
extern "C"  void uGuiTextGetText_OnUpdate_m2378154508 (uGuiTextGetText_t2404688918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextGetText::DoGetTextValue()
extern "C"  void uGuiTextGetText_DoGetTextValue_m3783818837 (uGuiTextGetText_t2404688918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
