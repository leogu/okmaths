﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2844776886(__this, ___l0, method) ((  void (*) (Enumerator_t1689645119 *, List_1_t2154915445 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1033041636(__this, method) ((  void (*) (Enumerator_t1689645119 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2918844420(__this, method) ((  Il2CppObject * (*) (Enumerator_t1689645119 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::Dispose()
#define Enumerator_Dispose_m4204813001(__this, method) ((  void (*) (Enumerator_t1689645119 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::VerifyState()
#define Enumerator_VerifyState_m444054920(__this, method) ((  void (*) (Enumerator_t1689645119 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::MoveNext()
#define Enumerator_MoveNext_m2085654887(__this, method) ((  bool (*) (Enumerator_t1689645119 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmObject>::get_Current()
#define Enumerator_get_Current_m3812014891(__this, method) ((  FsmObject_t2785794313 * (*) (Enumerator_t1689645119 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
