﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerFixedUpdate
struct PlayMakerFixedUpdate_t960084629;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerFixedUpdate::FixedUpdate()
extern "C"  void PlayMakerFixedUpdate_FixedUpdate_m535311859 (PlayMakerFixedUpdate_t960084629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFixedUpdate::.ctor()
extern "C"  void PlayMakerFixedUpdate__ctor_m2011563730 (PlayMakerFixedUpdate_t960084629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
