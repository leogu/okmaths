﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolAnyTrue
struct BoolAnyTrue_t748534252;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::.ctor()
extern "C"  void BoolAnyTrue__ctor_m708794632 (BoolAnyTrue_t748534252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::Reset()
extern "C"  void BoolAnyTrue_Reset_m1390819181 (BoolAnyTrue_t748534252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::OnEnter()
extern "C"  void BoolAnyTrue_OnEnter_m3149528621 (BoolAnyTrue_t748534252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::OnUpdate()
extern "C"  void BoolAnyTrue_OnUpdate_m763778838 (BoolAnyTrue_t748534252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAnyTrue::DoAnyTrue()
extern "C"  void BoolAnyTrue_DoAnyTrue_m2117199167 (BoolAnyTrue_t748534252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
