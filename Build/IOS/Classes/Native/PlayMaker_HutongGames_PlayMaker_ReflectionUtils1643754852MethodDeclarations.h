﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Assembly[]
struct AssemblyU5BU5D_t1984278467;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t4238939941;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo>
struct List_1_t3412218392;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t125053523;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1736152084;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"

// System.Reflection.Assembly[] HutongGames.PlayMaker.ReflectionUtils::GetLoadedAssemblies()
extern "C"  AssemblyU5BU5D_t1984278467* ReflectionUtils_GetLoadedAssemblies_m2692623861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ReflectionUtils::GetGlobalType(System.String)
extern "C"  Type_t * ReflectionUtils_GetGlobalType_m1911790774 (Il2CppObject * __this /* static, unused */, String_t* ___typeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ReflectionUtils::GetPropertyType(System.Type,System.String)
extern "C"  Type_t * ReflectionUtils_GetPropertyType_m4070917337 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo[] HutongGames.PlayMaker.ReflectionUtils::GetMemberInfo(System.Type,System.String)
extern "C"  MemberInfoU5BU5D_t4238939941* ReflectionUtils_GetMemberInfo_m68661861 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ReflectionUtils::CanReadMemberValue(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_CanReadMemberValue_m2961532242 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ReflectionUtils::CanSetMemberValue(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_CanSetMemberValue_m3831386550 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ReflectionUtils::CanGetMemberValue(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_CanGetMemberValue_m3872770586 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ReflectionUtils::GetMemberUnderlyingType(System.Reflection.MemberInfo)
extern "C"  Type_t * ReflectionUtils_GetMemberUnderlyingType_m2768592082 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo[],System.Object)
extern "C"  Il2CppObject * ReflectionUtils_GetMemberValue_m3253355149 (Il2CppObject * __this /* static, unused */, MemberInfoU5BU5D_t4238939941* ___memberInfo0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern "C"  Il2CppObject * ReflectionUtils_GetMemberValue_m704168243 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern "C"  void ReflectionUtils_SetMemberValue_m3898621646 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo[],System.Object,System.Object)
extern "C"  void ReflectionUtils_SetMemberValue_m173078098 (Il2CppObject * __this /* static, unused */, MemberInfoU5BU5D_t4238939941* ___memberInfo0, Il2CppObject * ___target1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::SetBoxedMemberValue(System.Object,System.Reflection.MemberInfo,System.Object,System.Reflection.MemberInfo,System.Object)
extern "C"  void ReflectionUtils_SetBoxedMemberValue_m1672084082 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___parent0, MemberInfo_t * ___targetInfo1, Il2CppObject * ___target2, MemberInfo_t * ___propertyInfo3, Il2CppObject * ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> HutongGames.PlayMaker.ReflectionUtils::GetFieldsAndProperties(System.Type,System.Reflection.BindingFlags)
extern "C"  List_1_t3412218392 * ReflectionUtils_GetFieldsAndProperties_m2000583010 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] HutongGames.PlayMaker.ReflectionUtils::GetPublicFields(System.Type)
extern "C"  FieldInfoU5BU5D_t125053523* ReflectionUtils_GetPublicFields_m2659923511 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] HutongGames.PlayMaker.ReflectionUtils::GetPublicProperties(System.Type)
extern "C"  PropertyInfoU5BU5D_t1736152084* ReflectionUtils_GetPublicProperties_m2312611076 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ReflectionUtils::.cctor()
extern "C"  void ReflectionUtils__cctor_m1134830780 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
