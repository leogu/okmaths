﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetPosition
struct SetPosition_t758614651;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetPosition::.ctor()
extern "C"  void SetPosition__ctor_m3017367937 (SetPosition_t758614651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::Reset()
extern "C"  void SetPosition_Reset_m300225524 (SetPosition_t758614651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnEnter()
extern "C"  void SetPosition_OnEnter_m1350190534 (SetPosition_t758614651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnUpdate()
extern "C"  void SetPosition_OnUpdate_m475930525 (SetPosition_t758614651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::OnLateUpdate()
extern "C"  void SetPosition_OnLateUpdate_m2367285177 (SetPosition_t758614651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetPosition::DoSetPosition()
extern "C"  void SetPosition_DoSetPosition_m3050826785 (SetPosition_t758614651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
