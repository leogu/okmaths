﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Blink
struct Blink_t4107698928;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Blink::.ctor()
extern "C"  void Blink__ctor_m3698990704 (Blink_t4107698928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::Reset()
extern "C"  void Blink_Reset_m1847736813 (Blink_t4107698928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::OnEnter()
extern "C"  void Blink_OnEnter_m1961852413 (Blink_t4107698928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::OnUpdate()
extern "C"  void Blink_OnUpdate_m1095483286 (Blink_t4107698928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Blink::UpdateBlinkState(System.Boolean)
extern "C"  void Blink_UpdateBlinkState_m1223706263 (Blink_t4107698928 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
