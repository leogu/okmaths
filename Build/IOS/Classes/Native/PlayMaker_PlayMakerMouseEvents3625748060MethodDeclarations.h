﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerMouseEvents
struct PlayMakerMouseEvents_t3625748060;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerMouseEvents::OnMouseEnter()
extern "C"  void PlayMakerMouseEvents_OnMouseEnter_m2895234989 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseDown()
extern "C"  void PlayMakerMouseEvents_OnMouseDown_m3723689487 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseUp()
extern "C"  void PlayMakerMouseEvents_OnMouseUp_m1713886120 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseUpAsButton()
extern "C"  void PlayMakerMouseEvents_OnMouseUpAsButton_m3605824020 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseExit()
extern "C"  void PlayMakerMouseEvents_OnMouseExit_m3008400505 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseDrag()
extern "C"  void PlayMakerMouseEvents_OnMouseDrag_m1969676181 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::OnMouseOver()
extern "C"  void PlayMakerMouseEvents_OnMouseOver_m2281620839 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerMouseEvents::.ctor()
extern "C"  void PlayMakerMouseEvents__ctor_m649736457 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
