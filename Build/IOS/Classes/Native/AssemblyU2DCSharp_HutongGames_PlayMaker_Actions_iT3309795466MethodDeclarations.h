﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveAdd
struct iTweenMoveAdd_t3309795466;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::.ctor()
extern "C"  void iTweenMoveAdd__ctor_m1785820788 (iTweenMoveAdd_t3309795466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::Reset()
extern "C"  void iTweenMoveAdd_Reset_m4261817215 (iTweenMoveAdd_t3309795466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::OnEnter()
extern "C"  void iTweenMoveAdd_OnEnter_m1690162239 (iTweenMoveAdd_t3309795466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::OnExit()
extern "C"  void iTweenMoveAdd_OnExit_m653635991 (iTweenMoveAdd_t3309795466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveAdd::DoiTween()
extern "C"  void iTweenMoveAdd_DoiTween_m3279647857 (iTweenMoveAdd_t3309795466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
