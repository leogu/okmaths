﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenResume
struct iTweenResume_t4280322891;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenResume::.ctor()
extern "C"  void iTweenResume__ctor_m1109025907 (iTweenResume_t4280322891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::Reset()
extern "C"  void iTweenResume_Reset_m32099924 (iTweenResume_t4280322891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::OnEnter()
extern "C"  void iTweenResume_OnEnter_m3172940442 (iTweenResume_t4280322891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenResume::DoiTween()
extern "C"  void iTweenResume_DoiTween_m991940484 (iTweenResume_t4280322891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
