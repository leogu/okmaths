﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t1528749055;
// System.Collections.Generic.IEnumerable`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerable_1_t2451754968;
// System.Collections.Generic.IEnumerator`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerator_1_t3930119046;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<HutongGames.PlayMaker.ParamDataType>
struct ICollection_1_t3111703228;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>
struct ReadOnlyCollection_1_t2345413615;
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t835534274;
// System.Predicate`1<HutongGames.PlayMaker.ParamDataType>
struct Predicate_1_t602598038;
// System.Comparison`1<HutongGames.PlayMaker.ParamDataType>
struct Comparison_1_t3421366774;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1063478729.h"

// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void List_1__ctor_m3481196486_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1__ctor_m3481196486(__this, method) ((  void (*) (List_1_t1528749055 *, const MethodInfo*))List_1__ctor_m3481196486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1526017166_gshared (List_1_t1528749055 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m1526017166(__this, ___collection0, method) ((  void (*) (List_1_t1528749055 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1526017166_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1354376854_gshared (List_1_t1528749055 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1354376854(__this, ___capacity0, method) ((  void (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1__ctor_m1354376854_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::.cctor()
extern "C"  void List_1__cctor_m3516433158_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m3516433158(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3516433158_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3158425759_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3158425759(__this, method) ((  Il2CppObject* (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3158425759_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m165170955_gshared (List_1_t1528749055 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m165170955(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1528749055 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m165170955_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1129691088_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1129691088(__this, method) ((  Il2CppObject * (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1129691088_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3363212491_gshared (List_1_t1528749055 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3363212491(__this, ___item0, method) ((  int32_t (*) (List_1_t1528749055 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3363212491_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3904499971_gshared (List_1_t1528749055 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3904499971(__this, ___item0, method) ((  bool (*) (List_1_t1528749055 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3904499971_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3722978149_gshared (List_1_t1528749055 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3722978149(__this, ___item0, method) ((  int32_t (*) (List_1_t1528749055 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3722978149_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3562038330_gshared (List_1_t1528749055 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3562038330(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1528749055 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3562038330_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m106940724_gshared (List_1_t1528749055 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m106940724(__this, ___item0, method) ((  void (*) (List_1_t1528749055 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m106940724_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m130734972_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m130734972(__this, method) ((  bool (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m130734972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1867324891_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1867324891(__this, method) ((  bool (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1867324891_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2862347675_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2862347675(__this, method) ((  Il2CppObject * (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2862347675_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3133366374_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3133366374(__this, method) ((  bool (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3133366374_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m223039535_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m223039535(__this, method) ((  bool (*) (List_1_t1528749055 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m223039535_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2912608738_gshared (List_1_t1528749055 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2912608738(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2912608738_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2147423573_gshared (List_1_t1528749055 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2147423573(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1528749055 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2147423573_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Add(T)
extern "C"  void List_1_Add_m1284391842_gshared (List_1_t1528749055 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Add_m1284391842(__this, ___item0, method) ((  void (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_Add_m1284391842_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m612192711_gshared (List_1_t1528749055 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m612192711(__this, ___newCount0, method) ((  void (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m612192711_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2222629086_gshared (List_1_t1528749055 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2222629086(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t1528749055 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2222629086_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4163530287_gshared (List_1_t1528749055 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m4163530287(__this, ___collection0, method) ((  void (*) (List_1_t1528749055 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4163530287_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2196347775_gshared (List_1_t1528749055 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2196347775(__this, ___enumerable0, method) ((  void (*) (List_1_t1528749055 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2196347775_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1391895312_gshared (List_1_t1528749055 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1391895312(__this, ___collection0, method) ((  void (*) (List_1_t1528749055 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1391895312_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2345413615 * List_1_AsReadOnly_m2821367191_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2821367191(__this, method) ((  ReadOnlyCollection_1_t2345413615 * (*) (List_1_t1528749055 *, const MethodInfo*))List_1_AsReadOnly_m2821367191_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Clear()
extern "C"  void List_1_Clear_m3565582709_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_Clear_m3565582709(__this, method) ((  void (*) (List_1_t1528749055 *, const MethodInfo*))List_1_Clear_m3565582709_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Contains(T)
extern "C"  bool List_1_Contains_m3795799248_gshared (List_1_t1528749055 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Contains_m3795799248(__this, ___item0, method) ((  bool (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_Contains_m3795799248_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3019719866_gshared (List_1_t1528749055 * __this, ParamDataTypeU5BU5D_t835534274* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m3019719866(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t1528749055 *, ParamDataTypeU5BU5D_t835534274*, int32_t, const MethodInfo*))List_1_CopyTo_m3019719866_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Find(System.Predicate`1<T>)
extern "C"  int32_t List_1_Find_m4188515934_gshared (List_1_t1528749055 * __this, Predicate_1_t602598038 * ___match0, const MethodInfo* method);
#define List_1_Find_m4188515934(__this, ___match0, method) ((  int32_t (*) (List_1_t1528749055 *, Predicate_1_t602598038 *, const MethodInfo*))List_1_Find_m4188515934_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1905576939_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t602598038 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1905576939(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t602598038 *, const MethodInfo*))List_1_CheckMatch_m1905576939_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1544653270_gshared (List_1_t1528749055 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t602598038 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1544653270(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t1528749055 *, int32_t, int32_t, Predicate_1_t602598038 *, const MethodInfo*))List_1_GetIndex_m1544653270_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::GetEnumerator()
extern "C"  Enumerator_t1063478729  List_1_GetEnumerator_m2238301103_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2238301103(__this, method) ((  Enumerator_t1063478729  (*) (List_1_t1528749055 *, const MethodInfo*))List_1_GetEnumerator_m2238301103_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3173612908_gshared (List_1_t1528749055 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3173612908(__this, ___item0, method) ((  int32_t (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_IndexOf_m3173612908_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2778668887_gshared (List_1_t1528749055 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2778668887(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t1528749055 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2778668887_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m814160354_gshared (List_1_t1528749055 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m814160354(__this, ___index0, method) ((  void (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_CheckIndex_m814160354_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2388228913_gshared (List_1_t1528749055 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define List_1_Insert_m2388228913(__this, ___index0, ___item1, method) ((  void (*) (List_1_t1528749055 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m2388228913_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2636521724_gshared (List_1_t1528749055 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2636521724(__this, ___collection0, method) ((  void (*) (List_1_t1528749055 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2636521724_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Remove(T)
extern "C"  bool List_1_Remove_m948653449_gshared (List_1_t1528749055 * __this, int32_t ___item0, const MethodInfo* method);
#define List_1_Remove_m948653449(__this, ___item0, method) ((  bool (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_Remove_m948653449_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4290353619_gshared (List_1_t1528749055 * __this, Predicate_1_t602598038 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4290353619(__this, ___match0, method) ((  int32_t (*) (List_1_t1528749055 *, Predicate_1_t602598038 *, const MethodInfo*))List_1_RemoveAll_m4290353619_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m1182578253_gshared (List_1_t1528749055 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m1182578253(__this, ___index0, method) ((  void (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1182578253_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1629814214_gshared (List_1_t1528749055 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1629814214(__this, ___index0, ___count1, method) ((  void (*) (List_1_t1528749055 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1629814214_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Reverse()
extern "C"  void List_1_Reverse_m364447135_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_Reverse_m364447135(__this, method) ((  void (*) (List_1_t1528749055 *, const MethodInfo*))List_1_Reverse_m364447135_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Sort()
extern "C"  void List_1_Sort_m2216318233_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_Sort_m2216318233(__this, method) ((  void (*) (List_1_t1528749055 *, const MethodInfo*))List_1_Sort_m2216318233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m632073118_gshared (List_1_t1528749055 * __this, Comparison_1_t3421366774 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m632073118(__this, ___comparison0, method) ((  void (*) (List_1_t1528749055 *, Comparison_1_t3421366774 *, const MethodInfo*))List_1_Sort_m632073118_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::ToArray()
extern "C"  ParamDataTypeU5BU5D_t835534274* List_1_ToArray_m3503872334_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_ToArray_m3503872334(__this, method) ((  ParamDataTypeU5BU5D_t835534274* (*) (List_1_t1528749055 *, const MethodInfo*))List_1_ToArray_m3503872334_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1639321120_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1639321120(__this, method) ((  void (*) (List_1_t1528749055 *, const MethodInfo*))List_1_TrimExcess_m1639321120_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1939959114_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1939959114(__this, method) ((  int32_t (*) (List_1_t1528749055 *, const MethodInfo*))List_1_get_Capacity_m1939959114_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m640353135_gshared (List_1_t1528749055 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m640353135(__this, ___value0, method) ((  void (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_set_Capacity_m640353135_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::get_Count()
extern "C"  int32_t List_1_get_Count_m3360506876_gshared (List_1_t1528749055 * __this, const MethodInfo* method);
#define List_1_get_Count_m3360506876(__this, method) ((  int32_t (*) (List_1_t1528749055 *, const MethodInfo*))List_1_get_Count_m3360506876_gshared)(__this, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m21497343_gshared (List_1_t1528749055 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m21497343(__this, ___index0, method) ((  int32_t (*) (List_1_t1528749055 *, int32_t, const MethodInfo*))List_1_get_Item_m21497343_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m749871984_gshared (List_1_t1528749055 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define List_1_set_Item_m749871984(__this, ___index0, ___value1, method) ((  void (*) (List_1_t1528749055 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m749871984_gshared)(__this, ___index0, ___value1, method)
