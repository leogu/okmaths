﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Explosion
struct Explosion_t472586591;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.Explosion::.ctor()
extern "C"  void Explosion__ctor_m316739007 (Explosion_t472586591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::Reset()
extern "C"  void Explosion_Reset_m3588175316 (Explosion_t472586591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnPreprocess()
extern "C"  void Explosion_OnPreprocess_m3237135468 (Explosion_t472586591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnEnter()
extern "C"  void Explosion_OnEnter_m1709530810 (Explosion_t472586591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::OnFixedUpdate()
extern "C"  void Explosion_OnFixedUpdate_m1188647765 (Explosion_t472586591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Explosion::DoExplosion()
extern "C"  void Explosion_DoExplosion_m3829468637 (Explosion_t472586591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.Explosion::ShouldApplyForce(UnityEngine.GameObject)
extern "C"  bool Explosion_ShouldApplyForce_m448620423 (Explosion_t472586591 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
