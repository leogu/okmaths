﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringCompare
struct StringCompare_t1547997036;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringCompare::.ctor()
extern "C"  void StringCompare__ctor_m510460926 (StringCompare_t1547997036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::Reset()
extern "C"  void StringCompare_Reset_m2901585313 (StringCompare_t1547997036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::OnEnter()
extern "C"  void StringCompare_OnEnter_m1983988193 (StringCompare_t1547997036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::OnUpdate()
extern "C"  void StringCompare_OnUpdate_m405278064 (StringCompare_t1547997036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringCompare::DoStringCompare()
extern "C"  void StringCompare_DoStringCompare_m3607421469 (StringCompare_t1547997036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
