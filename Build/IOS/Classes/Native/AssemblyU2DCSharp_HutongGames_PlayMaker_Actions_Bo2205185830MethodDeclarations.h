﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolChanged
struct BoolChanged_t2205185830;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolChanged::.ctor()
extern "C"  void BoolChanged__ctor_m3573461526 (BoolChanged_t2205185830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolChanged::Reset()
extern "C"  void BoolChanged_Reset_m4289302683 (BoolChanged_t2205185830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolChanged::OnEnter()
extern "C"  void BoolChanged_OnEnter_m3605691315 (BoolChanged_t2205185830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolChanged::OnUpdate()
extern "C"  void BoolChanged_OnUpdate_m926373728 (BoolChanged_t2205185830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
