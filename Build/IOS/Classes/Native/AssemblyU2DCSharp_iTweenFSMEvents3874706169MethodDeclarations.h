﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iTweenFSMEvents
struct iTweenFSMEvents_t3874706169;

#include "codegen/il2cpp-codegen.h"

// System.Void iTweenFSMEvents::.ctor()
extern "C"  void iTweenFSMEvents__ctor_m3718146788 (iTweenFSMEvents_t3874706169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenFSMEvents::.cctor()
extern "C"  void iTweenFSMEvents__cctor_m680300739 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenFSMEvents::iTweenOnStart(System.Int32)
extern "C"  void iTweenFSMEvents_iTweenOnStart_m2578793228 (iTweenFSMEvents_t3874706169 * __this, int32_t ___aniTweenID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenFSMEvents::iTweenOnComplete(System.Int32)
extern "C"  void iTweenFSMEvents_iTweenOnComplete_m3355531333 (iTweenFSMEvents_t3874706169 * __this, int32_t ___aniTweenID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
