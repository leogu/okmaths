﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenImageFade
struct DOTweenImageFade_t4178804613;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFade::.ctor()
extern "C"  void DOTweenImageFade__ctor_m3820318789 (DOTweenImageFade_t4178804613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFade::Reset()
extern "C"  void DOTweenImageFade_Reset_m597167926 (DOTweenImageFade_t4178804613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFade::OnEnter()
extern "C"  void DOTweenImageFade_OnEnter_m1535909396 (DOTweenImageFade_t4178804613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFade::<OnEnter>m__3C()
extern "C"  void DOTweenImageFade_U3COnEnterU3Em__3C_m1370368947 (DOTweenImageFade_t4178804613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFade::<OnEnter>m__3D()
extern "C"  void DOTweenImageFade_U3COnEnterU3Em__3D_m106812984 (DOTweenImageFade_t4178804613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
