﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayAdd
struct ArrayAdd_t1320648502;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::.ctor()
extern "C"  void ArrayAdd__ctor_m3713144576 (ArrayAdd_t1320648502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::Reset()
extern "C"  void ArrayAdd_Reset_m1379159847 (ArrayAdd_t1320648502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::OnEnter()
extern "C"  void ArrayAdd_OnEnter_m828347255 (ArrayAdd_t1320648502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAdd::DoAddValue()
extern "C"  void ArrayAdd_DoAddValue_m2144866399 (ArrayAdd_t1320648502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
