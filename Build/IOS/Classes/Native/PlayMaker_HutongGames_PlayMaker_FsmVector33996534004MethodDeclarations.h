﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector33996534004.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVector3::get_Value()
extern "C"  Vector3_t2243707580  FsmVector3_get_Value_m4242600139 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::set_Value(UnityEngine.Vector3)
extern "C"  void FsmVector3_set_Value_m1785770740 (FsmVector3_t3996534004 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmVector3::get_RawValue()
extern "C"  Il2CppObject * FsmVector3_get_RawValue_m2335132248 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::set_RawValue(System.Object)
extern "C"  void FsmVector3_set_RawValue_m2457795503 (FsmVector3_t3996534004 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::.ctor()
extern "C"  void FsmVector3__ctor_m2525545399 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::.ctor(System.String)
extern "C"  void FsmVector3__ctor_m3962183865 (FsmVector3_t3996534004 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::.ctor(HutongGames.PlayMaker.FsmVector3)
extern "C"  void FsmVector3__ctor_m3104774171 (FsmVector3_t3996534004 * __this, FsmVector3_t3996534004 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVector3::Clone()
extern "C"  NamedVariable_t3026441313 * FsmVector3_Clone_m3228570008 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVector3::get_VariableType()
extern "C"  int32_t FsmVector3_get_VariableType_m1271588475 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVector3::ToString()
extern "C"  String_t* FsmVector3_ToString_m2841023184 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmVector3::op_Implicit(UnityEngine.Vector3)
extern "C"  FsmVector3_t3996534004 * FsmVector3_op_Implicit_m517215360 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
