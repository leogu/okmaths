﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetButton
struct GetButton_t727365274;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetButton::.ctor()
extern "C"  void GetButton__ctor_m3125813026 (GetButton_t727365274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::Reset()
extern "C"  void GetButton_Reset_m1072879919 (GetButton_t727365274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::OnEnter()
extern "C"  void GetButton_OnEnter_m1714871959 (GetButton_t727365274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::OnUpdate()
extern "C"  void GetButton_OnUpdate_m997979084 (GetButton_t727365274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButton::DoGetButton()
extern "C"  void GetButton_DoGetButton_m856098473 (GetButton_t727365274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
