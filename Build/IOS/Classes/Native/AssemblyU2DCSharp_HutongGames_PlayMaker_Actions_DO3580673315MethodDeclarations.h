﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveY
struct DOTweenRigidbody2DMoveY_t3580673315;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveY::.ctor()
extern "C"  void DOTweenRigidbody2DMoveY__ctor_m1565081979 (DOTweenRigidbody2DMoveY_t3580673315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveY::Reset()
extern "C"  void DOTweenRigidbody2DMoveY_Reset_m3831202872 (DOTweenRigidbody2DMoveY_t3580673315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveY::OnEnter()
extern "C"  void DOTweenRigidbody2DMoveY_OnEnter_m3074749262 (DOTweenRigidbody2DMoveY_t3580673315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveY::<OnEnter>m__7E()
extern "C"  void DOTweenRigidbody2DMoveY_U3COnEnterU3Em__7E_m411304107 (DOTweenRigidbody2DMoveY_t3580673315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveY::<OnEnter>m__7F()
extern "C"  void DOTweenRigidbody2DMoveY_U3COnEnterU3Em__7F_m411304136 (DOTweenRigidbody2DMoveY_t3580673315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
