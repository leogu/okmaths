﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateUpdate
struct iTweenRotateUpdate_t1486249478;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::.ctor()
extern "C"  void iTweenRotateUpdate__ctor_m908054796 (iTweenRotateUpdate_t1486249478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::Reset()
extern "C"  void iTweenRotateUpdate_Reset_m114002379 (iTweenRotateUpdate_t1486249478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnEnter()
extern "C"  void iTweenRotateUpdate_OnEnter_m721877123 (iTweenRotateUpdate_t1486249478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnExit()
extern "C"  void iTweenRotateUpdate_OnExit_m458020939 (iTweenRotateUpdate_t1486249478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::OnUpdate()
extern "C"  void iTweenRotateUpdate_OnUpdate_m892598746 (iTweenRotateUpdate_t1486249478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateUpdate::DoiTween()
extern "C"  void iTweenRotateUpdate_DoiTween_m3585011409 (iTweenRotateUpdate_t1486249478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
