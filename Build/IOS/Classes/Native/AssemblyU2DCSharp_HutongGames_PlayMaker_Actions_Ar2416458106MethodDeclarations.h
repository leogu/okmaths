﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue
struct ArrayListGetNearestFloatValue_t2416458106;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::.ctor()
extern "C"  void ArrayListGetNearestFloatValue__ctor_m2563179246 (ArrayListGetNearestFloatValue_t2416458106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::Reset()
extern "C"  void ArrayListGetNearestFloatValue_Reset_m556649555 (ArrayListGetNearestFloatValue_t2416458106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::OnEnter()
extern "C"  void ArrayListGetNearestFloatValue_OnEnter_m3068650251 (ArrayListGetNearestFloatValue_t2416458106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::OnUpdate()
extern "C"  void ArrayListGetNearestFloatValue_OnUpdate_m4062961256 (ArrayListGetNearestFloatValue_t2416458106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::DoGetNearestValue()
extern "C"  void ArrayListGetNearestFloatValue_DoGetNearestValue_m3496168998 (ArrayListGetNearestFloatValue_t2416458106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
