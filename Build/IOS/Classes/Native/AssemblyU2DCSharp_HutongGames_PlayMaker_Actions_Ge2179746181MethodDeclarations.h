﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseX
struct GetMouseX_t2179746181;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseX::.ctor()
extern "C"  void GetMouseX__ctor_m2580169209 (GetMouseX_t2179746181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::Reset()
extern "C"  void GetMouseX_Reset_m1401355230 (GetMouseX_t2179746181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::OnEnter()
extern "C"  void GetMouseX_OnEnter_m3321076452 (GetMouseX_t2179746181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::OnUpdate()
extern "C"  void GetMouseX_OnUpdate_m1403629149 (GetMouseX_t2179746181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseX::DoGetMouseX()
extern "C"  void GetMouseX_DoGetMouseX_m4185344785 (GetMouseX_t2179746181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
