﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent
struct  uGuiScrollbarOnClickEvent_t489128661  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::sendEvent
	FsmEvent_t1258573736 * ___sendEvent_12;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::_scrollbar
	Scrollbar_t3248359358 * ____scrollbar_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiScrollbarOnClickEvent_t489128661, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_sendEvent_12() { return static_cast<int32_t>(offsetof(uGuiScrollbarOnClickEvent_t489128661, ___sendEvent_12)); }
	inline FsmEvent_t1258573736 * get_sendEvent_12() const { return ___sendEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_sendEvent_12() { return &___sendEvent_12; }
	inline void set_sendEvent_12(FsmEvent_t1258573736 * value)
	{
		___sendEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_12, value);
	}

	inline static int32_t get_offset_of__scrollbar_13() { return static_cast<int32_t>(offsetof(uGuiScrollbarOnClickEvent_t489128661, ____scrollbar_13)); }
	inline Scrollbar_t3248359358 * get__scrollbar_13() const { return ____scrollbar_13; }
	inline Scrollbar_t3248359358 ** get_address_of__scrollbar_13() { return &____scrollbar_13; }
	inline void set__scrollbar_13(Scrollbar_t3248359358 * value)
	{
		____scrollbar_13 = value;
		Il2CppCodeGenWriteBarrier(&____scrollbar_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
