﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t3371940140;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0__ctor_m2554685885 (U3CU3Ec__DisplayClass9_0_t3371940140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_m1229978689 (U3CU3Ec__DisplayClass9_0_t3371940140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m3961975735 (U3CU3Ec__DisplayClass9_0_t3371940140 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
