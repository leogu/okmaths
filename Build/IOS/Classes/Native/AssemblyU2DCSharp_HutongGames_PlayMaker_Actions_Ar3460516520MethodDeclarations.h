﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetNext
struct ArrayListGetNext_t3460516520;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNext::.ctor()
extern "C"  void ArrayListGetNext__ctor_m308249646 (ArrayListGetNext_t3460516520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNext::Reset()
extern "C"  void ArrayListGetNext_Reset_m1105830613 (ArrayListGetNext_t3460516520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNext::OnEnter()
extern "C"  void ArrayListGetNext_OnEnter_m2191494517 (ArrayListGetNext_t3460516520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNext::DoGetNextItem()
extern "C"  void ArrayListGetNext_DoGetNextItem_m850988047 (ArrayListGetNext_t3460516520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetNext::GetItemAtIndex()
extern "C"  void ArrayListGetNext_GetItemAtIndex_m4028466314 (ArrayListGetNext_t3460516520 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
