﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.AsyncExec/CoroutineHostMonoBehaviour
struct CoroutineHostMonoBehaviour_t291073519;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.AsyncExec/CoroutineHostMonoBehaviour::.ctor()
extern "C"  void CoroutineHostMonoBehaviour__ctor_m3170070935 (CoroutineHostMonoBehaviour_t291073519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
