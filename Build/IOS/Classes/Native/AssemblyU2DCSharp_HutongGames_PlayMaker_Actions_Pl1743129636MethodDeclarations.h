﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayAnimation
struct PlayAnimation_t1743129636;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::.ctor()
extern "C"  void PlayAnimation__ctor_m173418970 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::Reset()
extern "C"  void PlayAnimation_Reset_m812388029 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::OnEnter()
extern "C"  void PlayAnimation_OnEnter_m2581350989 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::DoPlayAnimation()
extern "C"  void PlayAnimation_DoPlayAnimation_m3931405213 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::OnUpdate()
extern "C"  void PlayAnimation_OnUpdate_m1681793044 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::OnExit()
extern "C"  void PlayAnimation_OnExit_m2694958361 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayAnimation::StopAnimation()
extern "C"  void PlayAnimation_StopAnimation_m2323538104 (PlayAnimation_t1743129636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
