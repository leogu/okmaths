﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Invert
struct Vector3Invert_t3380379926;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::.ctor()
extern "C"  void Vector3Invert__ctor_m3263125874 (Vector3Invert_t3380379926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::Reset()
extern "C"  void Vector3Invert_Reset_m476562703 (Vector3Invert_t3380379926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::OnEnter()
extern "C"  void Vector3Invert_OnEnter_m3795765143 (Vector3Invert_t3380379926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Invert::OnUpdate()
extern "C"  void Vector3Invert_OnUpdate_m1258905452 (Vector3Invert_t3380379926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
