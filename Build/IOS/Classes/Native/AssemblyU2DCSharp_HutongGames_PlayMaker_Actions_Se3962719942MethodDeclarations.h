﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger
struct SetCollider2dIsTrigger_t3962719942;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::.ctor()
extern "C"  void SetCollider2dIsTrigger__ctor_m3363361018 (SetCollider2dIsTrigger_t3962719942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::Reset()
extern "C"  void SetCollider2dIsTrigger_Reset_m1919953971 (SetCollider2dIsTrigger_t3962719942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::OnEnter()
extern "C"  void SetCollider2dIsTrigger_OnEnter_m2920120635 (SetCollider2dIsTrigger_t3962719942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCollider2dIsTrigger::DoSetIsTrigger()
extern "C"  void SetCollider2dIsTrigger_DoSetIsTrigger_m3931150077 (SetCollider2dIsTrigger_t3962719942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
