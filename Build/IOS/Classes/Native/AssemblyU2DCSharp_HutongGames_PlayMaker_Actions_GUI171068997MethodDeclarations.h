﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutButton
struct GUILayoutButton_t171068997;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutButton::.ctor()
extern "C"  void GUILayoutButton__ctor_m1976327761 (GUILayoutButton_t171068997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutButton::Reset()
extern "C"  void GUILayoutButton_Reset_m78686854 (GUILayoutButton_t171068997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutButton::OnGUI()
extern "C"  void GUILayoutButton_OnGUI_m2352756623 (GUILayoutButton_t171068997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
