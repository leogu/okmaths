﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject
struct uGuiGetSelectedGameObject_t1709831496;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::.ctor()
extern "C"  void uGuiGetSelectedGameObject__ctor_m3863648160 (uGuiGetSelectedGameObject_t1709831496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::Reset()
extern "C"  void uGuiGetSelectedGameObject_Reset_m3538603933 (uGuiGetSelectedGameObject_t1709831496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::OnEnter()
extern "C"  void uGuiGetSelectedGameObject_OnEnter_m3097805381 (uGuiGetSelectedGameObject_t1709831496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::OnUpdate()
extern "C"  void uGuiGetSelectedGameObject_OnUpdate_m1898900254 (uGuiGetSelectedGameObject_t1709831496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::GetCurrentSelectedGameObject()
extern "C"  void uGuiGetSelectedGameObject_GetCurrentSelectedGameObject_m2435793133 (uGuiGetSelectedGameObject_t1709831496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
