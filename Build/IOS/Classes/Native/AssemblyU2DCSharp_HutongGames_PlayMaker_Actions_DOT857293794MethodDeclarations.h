﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClearCatchedTweens
struct DOTweenAdditionalMethodsClearCatchedTweens_t857293794;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClearCatchedTweens::.ctor()
extern "C"  void DOTweenAdditionalMethodsClearCatchedTweens__ctor_m2838749488 (DOTweenAdditionalMethodsClearCatchedTweens_t857293794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClearCatchedTweens::Reset()
extern "C"  void DOTweenAdditionalMethodsClearCatchedTweens_Reset_m263308807 (DOTweenAdditionalMethodsClearCatchedTweens_t857293794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClearCatchedTweens::OnEnter()
extern "C"  void DOTweenAdditionalMethodsClearCatchedTweens_OnEnter_m261299615 (DOTweenAdditionalMethodsClearCatchedTweens_t857293794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
