﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateFloat
struct DOTweenAnimateFloat_t713198095;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::.ctor()
extern "C"  void DOTweenAnimateFloat__ctor_m3016791143 (DOTweenAnimateFloat_t713198095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::Reset()
extern "C"  void DOTweenAnimateFloat_Reset_m2704583212 (DOTweenAnimateFloat_t713198095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::OnEnter()
extern "C"  void DOTweenAnimateFloat_OnEnter_m1531297834 (DOTweenAnimateFloat_t713198095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::<OnEnter>m__4()
extern "C"  float DOTweenAnimateFloat_U3COnEnterU3Em__4_m3569806871 (DOTweenAnimateFloat_t713198095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::<OnEnter>m__5(System.Single)
extern "C"  void DOTweenAnimateFloat_U3COnEnterU3Em__5_m1979639573 (DOTweenAnimateFloat_t713198095 * __this, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::<OnEnter>m__6()
extern "C"  void DOTweenAnimateFloat_U3COnEnterU3Em__6_m9377755 (DOTweenAnimateFloat_t713198095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateFloat::<OnEnter>m__7()
extern "C"  void DOTweenAnimateFloat_U3COnEnterU3Em__7_m1555258720 (DOTweenAnimateFloat_t713198095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
