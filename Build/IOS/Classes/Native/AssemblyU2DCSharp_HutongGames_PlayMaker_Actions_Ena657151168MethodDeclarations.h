﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableGUI
struct EnableGUI_t657151168;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnableGUI::.ctor()
extern "C"  void EnableGUI__ctor_m2213797468 (EnableGUI_t657151168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::Reset()
extern "C"  void EnableGUI_Reset_m2777688409 (EnableGUI_t657151168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableGUI::OnEnter()
extern "C"  void EnableGUI_OnEnter_m192344321 (EnableGUI_t657151168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
