﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetOwner
struct GetOwner_t110173485;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetOwner::.ctor()
extern "C"  void GetOwner__ctor_m2181391913 (GetOwner_t110173485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetOwner::Reset()
extern "C"  void GetOwner_Reset_m4143217466 (GetOwner_t110173485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetOwner::OnEnter()
extern "C"  void GetOwner_OnEnter_m1815516280 (GetOwner_t110173485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
