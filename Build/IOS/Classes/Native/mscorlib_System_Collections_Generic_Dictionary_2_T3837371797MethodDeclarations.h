﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D>
struct Transform_1_t3837371797;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m154372867_gshared (Transform_1_t3837371797 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m154372867(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3837371797 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m154372867_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D>::Invoke(TKey,TValue)
extern "C"  RaycastHit2D_t4063908774  Transform_1_Invoke_m3567298359_gshared (Transform_1_t3837371797 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m3567298359(__this, ___key0, ___value1, method) ((  RaycastHit2D_t4063908774  (*) (Transform_1_t3837371797 *, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Transform_1_Invoke_m3567298359_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1272306584_gshared (Transform_1_t3837371797 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1272306584(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3837371797 *, Il2CppObject *, RaycastHit2D_t4063908774 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1272306584_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D>::EndInvoke(System.IAsyncResult)
extern "C"  RaycastHit2D_t4063908774  Transform_1_EndInvoke_m1822297945_gshared (Transform_1_t3837371797 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m1822297945(__this, ___result0, method) ((  RaycastHit2D_t4063908774  (*) (Transform_1_t3837371797 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m1822297945_gshared)(__this, ___result0, method)
