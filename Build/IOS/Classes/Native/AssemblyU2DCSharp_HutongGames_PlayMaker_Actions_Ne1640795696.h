﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetSendRate
struct  NetworkSetSendRate_t1640795696  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.NetworkSetSendRate::sendRate
	FsmFloat_t937133978 * ___sendRate_11;

public:
	inline static int32_t get_offset_of_sendRate_11() { return static_cast<int32_t>(offsetof(NetworkSetSendRate_t1640795696, ___sendRate_11)); }
	inline FsmFloat_t937133978 * get_sendRate_11() const { return ___sendRate_11; }
	inline FsmFloat_t937133978 ** get_address_of_sendRate_11() { return &___sendRate_11; }
	inline void set_sendRate_11(FsmFloat_t937133978 * value)
	{
		___sendRate_11 = value;
		Il2CppCodeGenWriteBarrier(&___sendRate_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
