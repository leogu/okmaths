﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmEventOptions
struct FsmEventOptions_t1556611294;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::.ctor()
extern "C"  void FsmEventOptions__ctor_m2183912350 (FsmEventOptions_t1556611294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::Reset()
extern "C"  void FsmEventOptions_Reset_m190223411 (FsmEventOptions_t1556611294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmEventOptions::OnUpdate()
extern "C"  void FsmEventOptions_OnUpdate_m590107272 (FsmEventOptions_t1556611294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
