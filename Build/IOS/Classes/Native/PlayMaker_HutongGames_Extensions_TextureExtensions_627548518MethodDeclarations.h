﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.Extensions.TextureExtensions/Point
struct Point_t627548518;
struct Point_t627548518_marshaled_pinvoke;
struct Point_t627548518_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions_627548518.h"

// System.Void HutongGames.Extensions.TextureExtensions/Point::.ctor(System.Int16,System.Int16)
extern "C"  void Point__ctor_m2947071929 (Point_t627548518 * __this, int16_t ___aX0, int16_t ___aY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.Extensions.TextureExtensions/Point::.ctor(System.Int32,System.Int32)
extern "C"  void Point__ctor_m3938160925 (Point_t627548518 * __this, int32_t ___aX0, int32_t ___aY1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Point_t627548518;
struct Point_t627548518_marshaled_pinvoke;

extern "C" void Point_t627548518_marshal_pinvoke(const Point_t627548518& unmarshaled, Point_t627548518_marshaled_pinvoke& marshaled);
extern "C" void Point_t627548518_marshal_pinvoke_back(const Point_t627548518_marshaled_pinvoke& marshaled, Point_t627548518& unmarshaled);
extern "C" void Point_t627548518_marshal_pinvoke_cleanup(Point_t627548518_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Point_t627548518;
struct Point_t627548518_marshaled_com;

extern "C" void Point_t627548518_marshal_com(const Point_t627548518& unmarshaled, Point_t627548518_marshaled_com& marshaled);
extern "C" void Point_t627548518_marshal_com_back(const Point_t627548518_marshaled_com& marshaled, Point_t627548518& unmarshaled);
extern "C" void Point_t627548518_marshal_com_cleanup(Point_t627548518_marshaled_com& marshaled);
