﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayTransferValue
struct ArrayTransferValue_t808037777;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::.ctor()
extern "C"  void ArrayTransferValue__ctor_m1085662363 (ArrayTransferValue_t808037777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::Reset()
extern "C"  void ArrayTransferValue_Reset_m3661904482 (ArrayTransferValue_t808037777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::OnEnter()
extern "C"  void ArrayTransferValue_OnEnter_m4211166092 (ArrayTransferValue_t808037777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayTransferValue::DoTransferValue()
extern "C"  void ArrayTransferValue_DoTransferValue_m516278470 (ArrayTransferValue_t808037777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
