﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetJointBreakInfo
struct GetJointBreakInfo_t2418545631;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::.ctor()
extern "C"  void GetJointBreakInfo__ctor_m874541099 (GetJointBreakInfo_t2418545631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::Reset()
extern "C"  void GetJointBreakInfo_Reset_m667650840 (GetJointBreakInfo_t2418545631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreakInfo::OnEnter()
extern "C"  void GetJointBreakInfo_OnEnter_m968901950 (GetJointBreakInfo_t2418545631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
