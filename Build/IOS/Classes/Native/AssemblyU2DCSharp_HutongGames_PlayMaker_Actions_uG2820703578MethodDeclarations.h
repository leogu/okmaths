﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers
struct uGuiSliderGetWholeNumbers_t2820703578;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::.ctor()
extern "C"  void uGuiSliderGetWholeNumbers__ctor_m2846636196 (uGuiSliderGetWholeNumbers_t2820703578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::Reset()
extern "C"  void uGuiSliderGetWholeNumbers_Reset_m2874168175 (uGuiSliderGetWholeNumbers_t2820703578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::OnEnter()
extern "C"  void uGuiSliderGetWholeNumbers_OnEnter_m2985583935 (uGuiSliderGetWholeNumbers_t2820703578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::DoGetValue()
extern "C"  void uGuiSliderGetWholeNumbers_DoGetValue_m3508937626 (uGuiSliderGetWholeNumbers_t2820703578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
