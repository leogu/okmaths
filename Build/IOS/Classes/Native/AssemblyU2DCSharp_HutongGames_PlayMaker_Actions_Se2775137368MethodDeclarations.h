﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorSpeed
struct SetAnimatorSpeed_t2775137368;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::.ctor()
extern "C"  void SetAnimatorSpeed__ctor_m4020133776 (SetAnimatorSpeed_t2775137368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::Reset()
extern "C"  void SetAnimatorSpeed_Reset_m1447430945 (SetAnimatorSpeed_t2775137368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::OnEnter()
extern "C"  void SetAnimatorSpeed_OnEnter_m451983889 (SetAnimatorSpeed_t2775137368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::OnUpdate()
extern "C"  void SetAnimatorSpeed_OnUpdate_m3809115366 (SetAnimatorSpeed_t2775137368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorSpeed::DoPlaybackSpeed()
extern "C"  void SetAnimatorSpeed_DoPlaybackSpeed_m2210846151 (SetAnimatorSpeed_t2775137368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
