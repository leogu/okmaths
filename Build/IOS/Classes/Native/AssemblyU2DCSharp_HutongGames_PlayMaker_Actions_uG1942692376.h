﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSetIsInteractable
struct  uGuiSetIsInteractable_t1942692376  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::isInteractable
	FsmBool_t664485696 * ___isInteractable_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::_selectable
	Selectable_t1490392188 * ____selectable_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::_originalState
	bool ____originalState_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSetIsInteractable_t1942692376, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isInteractable_12() { return static_cast<int32_t>(offsetof(uGuiSetIsInteractable_t1942692376, ___isInteractable_12)); }
	inline FsmBool_t664485696 * get_isInteractable_12() const { return ___isInteractable_12; }
	inline FsmBool_t664485696 ** get_address_of_isInteractable_12() { return &___isInteractable_12; }
	inline void set_isInteractable_12(FsmBool_t664485696 * value)
	{
		___isInteractable_12 = value;
		Il2CppCodeGenWriteBarrier(&___isInteractable_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiSetIsInteractable_t1942692376, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__selectable_14() { return static_cast<int32_t>(offsetof(uGuiSetIsInteractable_t1942692376, ____selectable_14)); }
	inline Selectable_t1490392188 * get__selectable_14() const { return ____selectable_14; }
	inline Selectable_t1490392188 ** get_address_of__selectable_14() { return &____selectable_14; }
	inline void set__selectable_14(Selectable_t1490392188 * value)
	{
		____selectable_14 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_14, value);
	}

	inline static int32_t get_offset_of__originalState_15() { return static_cast<int32_t>(offsetof(uGuiSetIsInteractable_t1942692376, ____originalState_15)); }
	inline bool get__originalState_15() const { return ____originalState_15; }
	inline bool* get_address_of__originalState_15() { return &____originalState_15; }
	inline void set__originalState_15(bool value)
	{
		____originalState_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
