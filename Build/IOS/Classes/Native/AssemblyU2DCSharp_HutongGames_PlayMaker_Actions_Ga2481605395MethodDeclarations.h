﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectTagSwitch
struct GameObjectTagSwitch_t2481605395;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::.ctor()
extern "C"  void GameObjectTagSwitch__ctor_m947547605 (GameObjectTagSwitch_t2481605395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::Reset()
extern "C"  void GameObjectTagSwitch_Reset_m3251816424 (GameObjectTagSwitch_t2481605395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnEnter()
extern "C"  void GameObjectTagSwitch_OnEnter_m3610394714 (GameObjectTagSwitch_t2481605395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::OnUpdate()
extern "C"  void GameObjectTagSwitch_OnUpdate_m2555706153 (GameObjectTagSwitch_t2481605395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectTagSwitch::DoTagSwitch()
extern "C"  void GameObjectTagSwitch_DoTagSwitch_m3403183164 (GameObjectTagSwitch_t2481605395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
