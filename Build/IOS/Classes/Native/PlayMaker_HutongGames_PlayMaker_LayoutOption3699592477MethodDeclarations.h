﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t3699592477;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t4183744904;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption3699592477.h"

// System.Void HutongGames.PlayMaker.LayoutOption::.ctor()
extern "C"  void LayoutOption__ctor_m177023146 (LayoutOption_t3699592477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.LayoutOption::.ctor(HutongGames.PlayMaker.LayoutOption)
extern "C"  void LayoutOption__ctor_m3932172027 (LayoutOption_t3699592477 * __this, LayoutOption_t3699592477 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.LayoutOption::ResetParameters()
extern "C"  void LayoutOption_ResetParameters_m466038959 (LayoutOption_t3699592477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption HutongGames.PlayMaker.LayoutOption::GetGUILayoutOption()
extern "C"  GUILayoutOption_t4183744904 * LayoutOption_GetGUILayoutOption_m1196300342 (LayoutOption_t3699592477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
