﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_3678286247.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.Owner
struct  Owner_t541725383  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.OwnerSelectionOptions HutongGames.PlayMaker.Ecosystem.Utils.Owner::selection
	int32_t ___selection_0;
	// UnityEngine.GameObject HutongGames.PlayMaker.Ecosystem.Utils.Owner::gameObject
	GameObject_t1756533147 * ___gameObject_1;

public:
	inline static int32_t get_offset_of_selection_0() { return static_cast<int32_t>(offsetof(Owner_t541725383, ___selection_0)); }
	inline int32_t get_selection_0() const { return ___selection_0; }
	inline int32_t* get_address_of_selection_0() { return &___selection_0; }
	inline void set_selection_0(int32_t value)
	{
		___selection_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(Owner_t541725383, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
