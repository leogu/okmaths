﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsPointerOverUiObject
struct IsPointerOverUiObject_t3995441740;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsPointerOverUiObject::.ctor()
extern "C"  void IsPointerOverUiObject__ctor_m2208123668 (IsPointerOverUiObject_t3995441740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsPointerOverUiObject::Reset()
extern "C"  void IsPointerOverUiObject_Reset_m3716514889 (IsPointerOverUiObject_t3995441740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsPointerOverUiObject::OnEnter()
extern "C"  void IsPointerOverUiObject_OnEnter_m3340705289 (IsPointerOverUiObject_t3995441740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsPointerOverUiObject::OnUpdate()
extern "C"  void IsPointerOverUiObject_OnUpdate_m1700705466 (IsPointerOverUiObject_t3995441740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsPointerOverUiObject::DoCheckPointer()
extern "C"  void IsPointerOverUiObject_DoCheckPointer_m4194191286 (IsPointerOverUiObject_t3995441740 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
