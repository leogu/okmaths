﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTag
struct GetTag_t2146438138;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTag::.ctor()
extern "C"  void GetTag__ctor_m2322239196 (GetTag_t2146438138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::Reset()
extern "C"  void GetTag_Reset_m4280828171 (GetTag_t2146438138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::OnEnter()
extern "C"  void GetTag_OnEnter_m4121205963 (GetTag_t2146438138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::OnUpdate()
extern "C"  void GetTag_OnUpdate_m1947171714 (GetTag_t2146438138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTag::DoGetTag()
extern "C"  void GetTag_DoGetTag_m1186040737 (GetTag_t2146438138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
