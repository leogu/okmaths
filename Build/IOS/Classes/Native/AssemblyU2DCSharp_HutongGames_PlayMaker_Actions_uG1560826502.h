﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues
struct  uGuiLayoutElementGetValues_t1560826502  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::ignoreLayout
	FsmBool_t664485696 * ___ignoreLayout_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::minWidthEnabled
	FsmBool_t664485696 * ___minWidthEnabled_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::minWidth
	FsmFloat_t937133978 * ___minWidth_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::minHeightEnabled
	FsmBool_t664485696 * ___minHeightEnabled_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::minHeight
	FsmFloat_t937133978 * ___minHeight_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::preferredWidthEnabled
	FsmBool_t664485696 * ___preferredWidthEnabled_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::preferredWidth
	FsmFloat_t937133978 * ___preferredWidth_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::preferredHeightEnabled
	FsmBool_t664485696 * ___preferredHeightEnabled_19;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::preferredHeight
	FsmFloat_t937133978 * ___preferredHeight_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::flexibleWidthEnabled
	FsmBool_t664485696 * ___flexibleWidthEnabled_21;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::flexibleWidth
	FsmFloat_t937133978 * ___flexibleWidth_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::flexibleHeightEnabled
	FsmBool_t664485696 * ___flexibleHeightEnabled_23;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::flexibleHeight
	FsmFloat_t937133978 * ___flexibleHeight_24;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::everyFrame
	bool ___everyFrame_25;
	// UnityEngine.UI.LayoutElement HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::_layoutElement
	LayoutElement_t2808691390 * ____layoutElement_26;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_ignoreLayout_12() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___ignoreLayout_12)); }
	inline FsmBool_t664485696 * get_ignoreLayout_12() const { return ___ignoreLayout_12; }
	inline FsmBool_t664485696 ** get_address_of_ignoreLayout_12() { return &___ignoreLayout_12; }
	inline void set_ignoreLayout_12(FsmBool_t664485696 * value)
	{
		___ignoreLayout_12 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreLayout_12, value);
	}

	inline static int32_t get_offset_of_minWidthEnabled_13() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___minWidthEnabled_13)); }
	inline FsmBool_t664485696 * get_minWidthEnabled_13() const { return ___minWidthEnabled_13; }
	inline FsmBool_t664485696 ** get_address_of_minWidthEnabled_13() { return &___minWidthEnabled_13; }
	inline void set_minWidthEnabled_13(FsmBool_t664485696 * value)
	{
		___minWidthEnabled_13 = value;
		Il2CppCodeGenWriteBarrier(&___minWidthEnabled_13, value);
	}

	inline static int32_t get_offset_of_minWidth_14() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___minWidth_14)); }
	inline FsmFloat_t937133978 * get_minWidth_14() const { return ___minWidth_14; }
	inline FsmFloat_t937133978 ** get_address_of_minWidth_14() { return &___minWidth_14; }
	inline void set_minWidth_14(FsmFloat_t937133978 * value)
	{
		___minWidth_14 = value;
		Il2CppCodeGenWriteBarrier(&___minWidth_14, value);
	}

	inline static int32_t get_offset_of_minHeightEnabled_15() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___minHeightEnabled_15)); }
	inline FsmBool_t664485696 * get_minHeightEnabled_15() const { return ___minHeightEnabled_15; }
	inline FsmBool_t664485696 ** get_address_of_minHeightEnabled_15() { return &___minHeightEnabled_15; }
	inline void set_minHeightEnabled_15(FsmBool_t664485696 * value)
	{
		___minHeightEnabled_15 = value;
		Il2CppCodeGenWriteBarrier(&___minHeightEnabled_15, value);
	}

	inline static int32_t get_offset_of_minHeight_16() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___minHeight_16)); }
	inline FsmFloat_t937133978 * get_minHeight_16() const { return ___minHeight_16; }
	inline FsmFloat_t937133978 ** get_address_of_minHeight_16() { return &___minHeight_16; }
	inline void set_minHeight_16(FsmFloat_t937133978 * value)
	{
		___minHeight_16 = value;
		Il2CppCodeGenWriteBarrier(&___minHeight_16, value);
	}

	inline static int32_t get_offset_of_preferredWidthEnabled_17() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___preferredWidthEnabled_17)); }
	inline FsmBool_t664485696 * get_preferredWidthEnabled_17() const { return ___preferredWidthEnabled_17; }
	inline FsmBool_t664485696 ** get_address_of_preferredWidthEnabled_17() { return &___preferredWidthEnabled_17; }
	inline void set_preferredWidthEnabled_17(FsmBool_t664485696 * value)
	{
		___preferredWidthEnabled_17 = value;
		Il2CppCodeGenWriteBarrier(&___preferredWidthEnabled_17, value);
	}

	inline static int32_t get_offset_of_preferredWidth_18() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___preferredWidth_18)); }
	inline FsmFloat_t937133978 * get_preferredWidth_18() const { return ___preferredWidth_18; }
	inline FsmFloat_t937133978 ** get_address_of_preferredWidth_18() { return &___preferredWidth_18; }
	inline void set_preferredWidth_18(FsmFloat_t937133978 * value)
	{
		___preferredWidth_18 = value;
		Il2CppCodeGenWriteBarrier(&___preferredWidth_18, value);
	}

	inline static int32_t get_offset_of_preferredHeightEnabled_19() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___preferredHeightEnabled_19)); }
	inline FsmBool_t664485696 * get_preferredHeightEnabled_19() const { return ___preferredHeightEnabled_19; }
	inline FsmBool_t664485696 ** get_address_of_preferredHeightEnabled_19() { return &___preferredHeightEnabled_19; }
	inline void set_preferredHeightEnabled_19(FsmBool_t664485696 * value)
	{
		___preferredHeightEnabled_19 = value;
		Il2CppCodeGenWriteBarrier(&___preferredHeightEnabled_19, value);
	}

	inline static int32_t get_offset_of_preferredHeight_20() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___preferredHeight_20)); }
	inline FsmFloat_t937133978 * get_preferredHeight_20() const { return ___preferredHeight_20; }
	inline FsmFloat_t937133978 ** get_address_of_preferredHeight_20() { return &___preferredHeight_20; }
	inline void set_preferredHeight_20(FsmFloat_t937133978 * value)
	{
		___preferredHeight_20 = value;
		Il2CppCodeGenWriteBarrier(&___preferredHeight_20, value);
	}

	inline static int32_t get_offset_of_flexibleWidthEnabled_21() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___flexibleWidthEnabled_21)); }
	inline FsmBool_t664485696 * get_flexibleWidthEnabled_21() const { return ___flexibleWidthEnabled_21; }
	inline FsmBool_t664485696 ** get_address_of_flexibleWidthEnabled_21() { return &___flexibleWidthEnabled_21; }
	inline void set_flexibleWidthEnabled_21(FsmBool_t664485696 * value)
	{
		___flexibleWidthEnabled_21 = value;
		Il2CppCodeGenWriteBarrier(&___flexibleWidthEnabled_21, value);
	}

	inline static int32_t get_offset_of_flexibleWidth_22() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___flexibleWidth_22)); }
	inline FsmFloat_t937133978 * get_flexibleWidth_22() const { return ___flexibleWidth_22; }
	inline FsmFloat_t937133978 ** get_address_of_flexibleWidth_22() { return &___flexibleWidth_22; }
	inline void set_flexibleWidth_22(FsmFloat_t937133978 * value)
	{
		___flexibleWidth_22 = value;
		Il2CppCodeGenWriteBarrier(&___flexibleWidth_22, value);
	}

	inline static int32_t get_offset_of_flexibleHeightEnabled_23() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___flexibleHeightEnabled_23)); }
	inline FsmBool_t664485696 * get_flexibleHeightEnabled_23() const { return ___flexibleHeightEnabled_23; }
	inline FsmBool_t664485696 ** get_address_of_flexibleHeightEnabled_23() { return &___flexibleHeightEnabled_23; }
	inline void set_flexibleHeightEnabled_23(FsmBool_t664485696 * value)
	{
		___flexibleHeightEnabled_23 = value;
		Il2CppCodeGenWriteBarrier(&___flexibleHeightEnabled_23, value);
	}

	inline static int32_t get_offset_of_flexibleHeight_24() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___flexibleHeight_24)); }
	inline FsmFloat_t937133978 * get_flexibleHeight_24() const { return ___flexibleHeight_24; }
	inline FsmFloat_t937133978 ** get_address_of_flexibleHeight_24() { return &___flexibleHeight_24; }
	inline void set_flexibleHeight_24(FsmFloat_t937133978 * value)
	{
		___flexibleHeight_24 = value;
		Il2CppCodeGenWriteBarrier(&___flexibleHeight_24, value);
	}

	inline static int32_t get_offset_of_everyFrame_25() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ___everyFrame_25)); }
	inline bool get_everyFrame_25() const { return ___everyFrame_25; }
	inline bool* get_address_of_everyFrame_25() { return &___everyFrame_25; }
	inline void set_everyFrame_25(bool value)
	{
		___everyFrame_25 = value;
	}

	inline static int32_t get_offset_of__layoutElement_26() { return static_cast<int32_t>(offsetof(uGuiLayoutElementGetValues_t1560826502, ____layoutElement_26)); }
	inline LayoutElement_t2808691390 * get__layoutElement_26() const { return ____layoutElement_26; }
	inline LayoutElement_t2808691390 ** get_address_of__layoutElement_26() { return &____layoutElement_26; }
	inline void set__layoutElement_26(LayoutElement_t2808691390 * value)
	{
		____layoutElement_26 = value;
		Il2CppCodeGenWriteBarrier(&____layoutElement_26, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
