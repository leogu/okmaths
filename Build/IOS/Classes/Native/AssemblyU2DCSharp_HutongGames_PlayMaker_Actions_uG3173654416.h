﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiImageGetSprite
struct  uGuiImageGetSprite_t3173654416  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiImageGetSprite::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.uGuiImageGetSprite::sprite
	FsmObject_t2785794313 * ___sprite_12;
	// UnityEngine.UI.Image HutongGames.PlayMaker.Actions.uGuiImageGetSprite::_image
	Image_t2042527209 * ____image_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiImageGetSprite_t3173654416, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_sprite_12() { return static_cast<int32_t>(offsetof(uGuiImageGetSprite_t3173654416, ___sprite_12)); }
	inline FsmObject_t2785794313 * get_sprite_12() const { return ___sprite_12; }
	inline FsmObject_t2785794313 ** get_address_of_sprite_12() { return &___sprite_12; }
	inline void set_sprite_12(FsmObject_t2785794313 * value)
	{
		___sprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_12, value);
	}

	inline static int32_t get_offset_of__image_13() { return static_cast<int32_t>(offsetof(uGuiImageGetSprite_t3173654416, ____image_13)); }
	inline Image_t2042527209 * get__image_13() const { return ____image_13; }
	inline Image_t2042527209 ** get_address_of__image_13() { return &____image_13; }
	inline void set__image_13(Image_t2042527209 * value)
	{
		____image_13 = value;
		Il2CppCodeGenWriteBarrier(&____image_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
