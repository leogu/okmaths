﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetChild
struct GetChild_t2518066502;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.GetChild::.ctor()
extern "C"  void GetChild__ctor_m3816890286 (GetChild_t2518066502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChild::Reset()
extern "C"  void GetChild_Reset_m2739964303 (GetChild_t2518066502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChild::OnEnter()
extern "C"  void GetChild_OnEnter_m679137023 (GetChild_t2518066502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetChild::DoGetChildByName(UnityEngine.GameObject,System.String,System.String)
extern "C"  GameObject_t1756533147 * GetChild_DoGetChildByName_m1355339518 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___root0, String_t* ___name1, String_t* ___tag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.GetChild::ErrorCheck()
extern "C"  String_t* GetChild_ErrorCheck_m248475225 (GetChild_t2518066502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
