﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.utils.Comment
struct Comment_t4162749105;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Ecosystem.utils.Comment::.ctor()
extern "C"  void Comment__ctor_m46651509 (Comment_t4162749105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
