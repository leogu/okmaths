﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SmoothLookAt2d
struct SmoothLookAt2d_t195500108;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt2d::.ctor()
extern "C"  void SmoothLookAt2d__ctor_m3335531146 (SmoothLookAt2d_t195500108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt2d::Reset()
extern "C"  void SmoothLookAt2d_Reset_m1609740697 (SmoothLookAt2d_t195500108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt2d::OnEnter()
extern "C"  void SmoothLookAt2d_OnEnter_m2576159689 (SmoothLookAt2d_t195500108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt2d::OnLateUpdate()
extern "C"  void SmoothLookAt2d_OnLateUpdate_m390849304 (SmoothLookAt2d_t195500108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAt2d::DoSmoothLookAt()
extern "C"  void SmoothLookAt2d_DoSmoothLookAt_m41415199 (SmoothLookAt2d_t195500108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
