﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// admob.AdmobEvent
struct AdmobEvent_t4123996035;

#include "codegen/il2cpp-codegen.h"

// System.Void admob.AdmobEvent::.ctor()
extern "C"  void AdmobEvent__ctor_m2257084687 (AdmobEvent_t4123996035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admob.AdmobEvent::.cctor()
extern "C"  void AdmobEvent__cctor_m2614563490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
