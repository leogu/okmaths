﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey
struct PlayerPrefsDeleteKey_t3205719175;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey::.ctor()
extern "C"  void PlayerPrefsDeleteKey__ctor_m641420441 (PlayerPrefsDeleteKey_t3205719175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey::Reset()
extern "C"  void PlayerPrefsDeleteKey_Reset_m4100386200 (PlayerPrefsDeleteKey_t3205719175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsDeleteKey::OnEnter()
extern "C"  void PlayerPrefsDeleteKey_OnEnter_m1326073346 (PlayerPrefsDeleteKey_t3205719175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
