﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll
struct DOTweenControlMethodsGoToAll_t3559548431;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll::.ctor()
extern "C"  void DOTweenControlMethodsGoToAll__ctor_m2015007641 (DOTweenControlMethodsGoToAll_t3559548431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll::Reset()
extern "C"  void DOTweenControlMethodsGoToAll_Reset_m54839576 (DOTweenControlMethodsGoToAll_t3559548431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll::OnEnter()
extern "C"  void DOTweenControlMethodsGoToAll_OnEnter_m1497547146 (DOTweenControlMethodsGoToAll_t3559548431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
