﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseButtonUp
struct GetMouseButtonUp_t3209705430;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::.ctor()
extern "C"  void GetMouseButtonUp__ctor_m3152974614 (GetMouseButtonUp_t3209705430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::Reset()
extern "C"  void GetMouseButtonUp_Reset_m817401831 (GetMouseButtonUp_t3209705430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::OnEnter()
extern "C"  void GetMouseButtonUp_OnEnter_m3525201999 (GetMouseButtonUp_t3209705430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::OnUpdate()
extern "C"  void GetMouseButtonUp_OnUpdate_m742916296 (GetMouseButtonUp_t3209705430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonUp::DoGetMouseButtonUp()
extern "C"  void GetMouseButtonUp_DoGetMouseButtonUp_m767295937 (GetMouseButtonUp_t3209705430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
