﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.UseGUILayout
struct  UseGUILayout_t3434851388  : public FsmStateAction_t2862378169
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.UseGUILayout::turnOffGUIlayout
	bool ___turnOffGUIlayout_11;

public:
	inline static int32_t get_offset_of_turnOffGUIlayout_11() { return static_cast<int32_t>(offsetof(UseGUILayout_t3434851388, ___turnOffGUIlayout_11)); }
	inline bool get_turnOffGUIlayout_11() const { return ___turnOffGUIlayout_11; }
	inline bool* get_address_of_turnOffGUIlayout_11() { return &___turnOffGUIlayout_11; }
	inline void set_turnOffGUIlayout_11(bool value)
	{
		___turnOffGUIlayout_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
