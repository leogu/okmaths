﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseAll
struct DOTweenControlMethodsPauseAll_t2785477512;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseAll::.ctor()
extern "C"  void DOTweenControlMethodsPauseAll__ctor_m1692602210 (DOTweenControlMethodsPauseAll_t2785477512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseAll::Reset()
extern "C"  void DOTweenControlMethodsPauseAll_Reset_m576009693 (DOTweenControlMethodsPauseAll_t2785477512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseAll::OnEnter()
extern "C"  void DOTweenControlMethodsPauseAll_OnEnter_m2590605229 (DOTweenControlMethodsPauseAll_t2785477512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
