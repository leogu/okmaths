﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerRequestHostList
struct MasterServerRequestHostList_t585579516;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::.ctor()
extern "C"  void MasterServerRequestHostList__ctor_m2142695066 (MasterServerRequestHostList_t585579516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::Reset()
extern "C"  void MasterServerRequestHostList_Reset_m1100158301 (MasterServerRequestHostList_t585579516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::OnEnter()
extern "C"  void MasterServerRequestHostList_OnEnter_m3078508981 (MasterServerRequestHostList_t585579516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::OnUpdate()
extern "C"  void MasterServerRequestHostList_OnUpdate_m58439804 (MasterServerRequestHostList_t585579516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::DoMasterServerRequestHost()
extern "C"  void MasterServerRequestHostList_DoMasterServerRequestHost_m1427084947 (MasterServerRequestHostList_t585579516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRequestHostList::WatchServerRequestHost()
extern "C"  void MasterServerRequestHostList_WatchServerRequestHost_m3278555569 (MasterServerRequestHostList_t585579516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
