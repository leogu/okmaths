﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVelocity2d
struct GetVelocity2d_t3435973141;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::.ctor()
extern "C"  void GetVelocity2d__ctor_m3015925877 (GetVelocity2d_t3435973141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::Reset()
extern "C"  void GetVelocity2d_Reset_m985313706 (GetVelocity2d_t3435973141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::OnEnter()
extern "C"  void GetVelocity2d_OnEnter_m3800689584 (GetVelocity2d_t3435973141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::OnUpdate()
extern "C"  void GetVelocity2d_OnUpdate_m1695026833 (GetVelocity2d_t3435973141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity2d::DoGetVelocity()
extern "C"  void GetVelocity2d_DoGetVelocity_m84413911 (GetVelocity2d_t3435973141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
