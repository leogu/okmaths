﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmVector2
struct SetFsmVector2_t3916649477;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::.ctor()
extern "C"  void SetFsmVector2__ctor_m754602807 (SetFsmVector2_t3916649477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::Reset()
extern "C"  void SetFsmVector2_Reset_m1330999642 (SetFsmVector2_t3916649477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::OnEnter()
extern "C"  void SetFsmVector2_OnEnter_m194384588 (SetFsmVector2_t3916649477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::DoSetFsmVector2()
extern "C"  void SetFsmVector2_DoSetFsmVector2_m1895232077 (SetFsmVector2_t3916649477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector2::OnUpdate()
extern "C"  void SetFsmVector2_OnUpdate_m793914807 (SetFsmVector2_t3916649477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
