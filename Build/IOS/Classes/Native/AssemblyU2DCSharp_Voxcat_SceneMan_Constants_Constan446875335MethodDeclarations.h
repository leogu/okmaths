﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Voxcat.SceneMan.Constants.Constants
struct Constants_t446875335;

#include "codegen/il2cpp-codegen.h"

// System.Void Voxcat.SceneMan.Constants.Constants::.ctor()
extern "C"  void Constants__ctor_m3279755370 (Constants_t446875335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Voxcat.SceneMan.Constants.Constants::.cctor()
extern "C"  void Constants__cctor_m3175937717 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
