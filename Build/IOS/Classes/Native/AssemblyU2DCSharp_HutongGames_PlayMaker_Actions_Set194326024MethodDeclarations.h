﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRandomMaterial
struct SetRandomMaterial_t194326024;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::.ctor()
extern "C"  void SetRandomMaterial__ctor_m3833495060 (SetRandomMaterial_t194326024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::Reset()
extern "C"  void SetRandomMaterial_Reset_m102409761 (SetRandomMaterial_t194326024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::OnEnter()
extern "C"  void SetRandomMaterial_OnEnter_m2103014985 (SetRandomMaterial_t194326024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomMaterial::DoSetRandomMaterial()
extern "C"  void SetRandomMaterial_DoSetRandomMaterial_m354181821 (SetRandomMaterial_t194326024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
