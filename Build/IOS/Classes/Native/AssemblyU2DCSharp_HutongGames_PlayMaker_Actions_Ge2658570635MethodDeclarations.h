﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmVariables
struct GetFsmVariables_t2658570635;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::.ctor()
extern "C"  void GetFsmVariables__ctor_m1389864169 (GetFsmVariables_t2658570635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::Reset()
extern "C"  void GetFsmVariables_Reset_m4021143852 (GetFsmVariables_t2658570635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::InitFsmVars()
extern "C"  void GetFsmVariables_InitFsmVars_m3858112917 (GetFsmVariables_t2658570635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::OnEnter()
extern "C"  void GetFsmVariables_OnEnter_m1575502118 (GetFsmVariables_t2658570635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::OnUpdate()
extern "C"  void GetFsmVariables_OnUpdate_m823763629 (GetFsmVariables_t2658570635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariables::DoGetFsmVariables()
extern "C"  void GetFsmVariables_DoGetFsmVariables_m3043480993 (GetFsmVariables_t2658570635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
