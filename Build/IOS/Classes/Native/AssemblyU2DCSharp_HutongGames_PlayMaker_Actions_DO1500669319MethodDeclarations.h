﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos
struct DOTweenRectTransformAnchorPos_t1500669319;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos::.ctor()
extern "C"  void DOTweenRectTransformAnchorPos__ctor_m1300896353 (DOTweenRectTransformAnchorPos_t1500669319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos::Reset()
extern "C"  void DOTweenRectTransformAnchorPos_Reset_m53093884 (DOTweenRectTransformAnchorPos_t1500669319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos::OnEnter()
extern "C"  void DOTweenRectTransformAnchorPos_OnEnter_m3806515486 (DOTweenRectTransformAnchorPos_t1500669319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos::<OnEnter>m__68()
extern "C"  void DOTweenRectTransformAnchorPos_U3COnEnterU3Em__68_m2327020069 (DOTweenRectTransformAnchorPos_t1500669319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos::<OnEnter>m__69()
extern "C"  void DOTweenRectTransformAnchorPos_U3COnEnterU3Em__69_m2327020036 (DOTweenRectTransformAnchorPos_t1500669319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
