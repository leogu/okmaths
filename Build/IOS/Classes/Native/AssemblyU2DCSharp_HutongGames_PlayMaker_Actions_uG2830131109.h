﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiGraphicSetColor
struct  uGuiGraphicSetColor_t2830131109  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::color
	FsmColor_t118301965 * ___color_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::red
	FsmFloat_t937133978 * ___red_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::green
	FsmFloat_t937133978 * ___green_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::blue
	FsmFloat_t937133978 * ___blue_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::alpha
	FsmFloat_t937133978 * ___alpha_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_17;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::_component
	Graphic_t2426225576 * ____component_19;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::_originalColor
	Color_t2020392075  ____originalColor_20;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_color_12() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___color_12)); }
	inline FsmColor_t118301965 * get_color_12() const { return ___color_12; }
	inline FsmColor_t118301965 ** get_address_of_color_12() { return &___color_12; }
	inline void set_color_12(FsmColor_t118301965 * value)
	{
		___color_12 = value;
		Il2CppCodeGenWriteBarrier(&___color_12, value);
	}

	inline static int32_t get_offset_of_red_13() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___red_13)); }
	inline FsmFloat_t937133978 * get_red_13() const { return ___red_13; }
	inline FsmFloat_t937133978 ** get_address_of_red_13() { return &___red_13; }
	inline void set_red_13(FsmFloat_t937133978 * value)
	{
		___red_13 = value;
		Il2CppCodeGenWriteBarrier(&___red_13, value);
	}

	inline static int32_t get_offset_of_green_14() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___green_14)); }
	inline FsmFloat_t937133978 * get_green_14() const { return ___green_14; }
	inline FsmFloat_t937133978 ** get_address_of_green_14() { return &___green_14; }
	inline void set_green_14(FsmFloat_t937133978 * value)
	{
		___green_14 = value;
		Il2CppCodeGenWriteBarrier(&___green_14, value);
	}

	inline static int32_t get_offset_of_blue_15() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___blue_15)); }
	inline FsmFloat_t937133978 * get_blue_15() const { return ___blue_15; }
	inline FsmFloat_t937133978 ** get_address_of_blue_15() { return &___blue_15; }
	inline void set_blue_15(FsmFloat_t937133978 * value)
	{
		___blue_15 = value;
		Il2CppCodeGenWriteBarrier(&___blue_15, value);
	}

	inline static int32_t get_offset_of_alpha_16() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___alpha_16)); }
	inline FsmFloat_t937133978 * get_alpha_16() const { return ___alpha_16; }
	inline FsmFloat_t937133978 ** get_address_of_alpha_16() { return &___alpha_16; }
	inline void set_alpha_16(FsmFloat_t937133978 * value)
	{
		___alpha_16 = value;
		Il2CppCodeGenWriteBarrier(&___alpha_16, value);
	}

	inline static int32_t get_offset_of_resetOnExit_17() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___resetOnExit_17)); }
	inline FsmBool_t664485696 * get_resetOnExit_17() const { return ___resetOnExit_17; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_17() { return &___resetOnExit_17; }
	inline void set_resetOnExit_17(FsmBool_t664485696 * value)
	{
		___resetOnExit_17 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_17, value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of__component_19() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ____component_19)); }
	inline Graphic_t2426225576 * get__component_19() const { return ____component_19; }
	inline Graphic_t2426225576 ** get_address_of__component_19() { return &____component_19; }
	inline void set__component_19(Graphic_t2426225576 * value)
	{
		____component_19 = value;
		Il2CppCodeGenWriteBarrier(&____component_19, value);
	}

	inline static int32_t get_offset_of__originalColor_20() { return static_cast<int32_t>(offsetof(uGuiGraphicSetColor_t2830131109, ____originalColor_20)); }
	inline Color_t2020392075  get__originalColor_20() const { return ____originalColor_20; }
	inline Color_t2020392075 * get_address_of__originalColor_20() { return &____originalColor_20; }
	inline void set__originalColor_20(Color_t2020392075  value)
	{
		____originalColor_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
