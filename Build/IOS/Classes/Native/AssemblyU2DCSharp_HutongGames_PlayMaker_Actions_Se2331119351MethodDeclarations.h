﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightSpotAngle
struct SetLightSpotAngle_t2331119351;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::.ctor()
extern "C"  void SetLightSpotAngle__ctor_m777823359 (SetLightSpotAngle_t2331119351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::Reset()
extern "C"  void SetLightSpotAngle_Reset_m465614932 (SetLightSpotAngle_t2331119351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::OnEnter()
extern "C"  void SetLightSpotAngle_OnEnter_m660628626 (SetLightSpotAngle_t2331119351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::OnUpdate()
extern "C"  void SetLightSpotAngle_OnUpdate_m2882546927 (SetLightSpotAngle_t2331119351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightSpotAngle::DoSetLightRange()
extern "C"  void SetLightSpotAngle_DoSetLightRange_m2260814151 (SetLightSpotAngle_t2331119351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
