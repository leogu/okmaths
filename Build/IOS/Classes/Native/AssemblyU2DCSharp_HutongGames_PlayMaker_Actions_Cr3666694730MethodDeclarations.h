﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CreateEmptyObject
struct CreateEmptyObject_t3666694730;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CreateEmptyObject::.ctor()
extern "C"  void CreateEmptyObject__ctor_m1154297940 (CreateEmptyObject_t3666694730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateEmptyObject::Reset()
extern "C"  void CreateEmptyObject_Reset_m3761503327 (CreateEmptyObject_t3666694730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateEmptyObject::OnEnter()
extern "C"  void CreateEmptyObject_OnEnter_m1068319727 (CreateEmptyObject_t3666694730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
