﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections
struct NetworkGetMaximumConnections_t1451949259;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::.ctor()
extern "C"  void NetworkGetMaximumConnections__ctor_m1658217323 (NetworkGetMaximumConnections_t1451949259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::Reset()
extern "C"  void NetworkGetMaximumConnections_Reset_m2450620828 (NetworkGetMaximumConnections_t1451949259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMaximumConnections::OnEnter()
extern "C"  void NetworkGetMaximumConnections_OnEnter_m4185655242 (NetworkGetMaximumConnections_t1451949259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
