﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3243659616(__this, ___l0, method) ((  void (*) (Enumerator_t3525565663 *, List_1_t3990835989 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1828524114(__this, method) ((  void (*) (Enumerator_t3525565663 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m336117946(__this, method) ((  Il2CppObject * (*) (Enumerator_t3525565663 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::Dispose()
#define Enumerator_Dispose_m3368358157(__this, method) ((  void (*) (Enumerator_t3525565663 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::VerifyState()
#define Enumerator_VerifyState_m1737348758(__this, method) ((  void (*) (Enumerator_t3525565663 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::MoveNext()
#define Enumerator_MoveNext_m600868495(__this, method) ((  bool (*) (Enumerator_t3525565663 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmAnimationCurve>::get_Current()
#define Enumerator_get_Current_m3279543731(__this, method) ((  FsmAnimationCurve_t326747561 * (*) (Enumerator_t3525565663 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
