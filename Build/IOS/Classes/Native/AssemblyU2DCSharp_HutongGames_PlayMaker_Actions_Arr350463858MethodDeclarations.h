﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListShuffle
struct ArrayListShuffle_t350463858;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListShuffle::.ctor()
extern "C"  void ArrayListShuffle__ctor_m1019894998 (ArrayListShuffle_t350463858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListShuffle::Reset()
extern "C"  void ArrayListShuffle_Reset_m838939383 (ArrayListShuffle_t350463858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListShuffle::OnEnter()
extern "C"  void ArrayListShuffle_OnEnter_m2213526119 (ArrayListShuffle_t350463858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListShuffle::DoArrayListShuffle(System.Collections.ArrayList)
extern "C"  void ArrayListShuffle_DoArrayListShuffle_m1293890724 (ArrayListShuffle_t350463858 * __this, ArrayList_t4252133567 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
