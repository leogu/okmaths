﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2ClampMagnitude
struct Vector2ClampMagnitude_t3782001690;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::.ctor()
extern "C"  void Vector2ClampMagnitude__ctor_m2903697382 (Vector2ClampMagnitude_t3782001690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::Reset()
extern "C"  void Vector2ClampMagnitude_Reset_m2733927963 (Vector2ClampMagnitude_t3782001690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::OnEnter()
extern "C"  void Vector2ClampMagnitude_OnEnter_m1551416091 (Vector2ClampMagnitude_t3782001690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::OnUpdate()
extern "C"  void Vector2ClampMagnitude_OnUpdate_m1114603240 (Vector2ClampMagnitude_t3782001690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2ClampMagnitude::DoVector2ClampMagnitude()
extern "C"  void Vector2ClampMagnitude_DoVector2ClampMagnitude_m1550753209 (Vector2ClampMagnitude_t3782001690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
