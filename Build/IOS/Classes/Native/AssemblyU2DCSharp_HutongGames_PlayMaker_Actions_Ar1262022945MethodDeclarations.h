﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSet
struct ArrayListSet_t1262022945;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSet::.ctor()
extern "C"  void ArrayListSet__ctor_m1376010355 (ArrayListSet_t1262022945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSet::Reset()
extern "C"  void ArrayListSet_Reset_m301804426 (ArrayListSet_t1262022945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSet::OnEnter()
extern "C"  void ArrayListSet_OnEnter_m1812942684 (ArrayListSet_t1262022945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSet::OnUpdate()
extern "C"  void ArrayListSet_OnUpdate_m3179898395 (ArrayListSet_t1262022945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSet::SetToArrayList()
extern "C"  void ArrayListSet_SetToArrayList_m739546781 (ArrayListSet_t1262022945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
