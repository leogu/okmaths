﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFogDensity
struct SetFogDensity_t3867228106;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::.ctor()
extern "C"  void SetFogDensity__ctor_m3494912758 (SetFogDensity_t3867228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::Reset()
extern "C"  void SetFogDensity_Reset_m3349222091 (SetFogDensity_t3867228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::OnEnter()
extern "C"  void SetFogDensity_OnEnter_m909774171 (SetFogDensity_t3867228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::OnUpdate()
extern "C"  void SetFogDensity_OnUpdate_m2299801848 (SetFogDensity_t3867228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogDensity::DoSetFogDensity()
extern "C"  void SetFogDensity_DoSetFogDensity_m1637368249 (SetFogDensity_t3867228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
