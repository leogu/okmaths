﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveY
struct DOTweenTransformLocalMoveY_t249885831;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveY::.ctor()
extern "C"  void DOTweenTransformLocalMoveY__ctor_m3293676441 (DOTweenTransformLocalMoveY_t249885831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveY::Reset()
extern "C"  void DOTweenTransformLocalMoveY_Reset_m72121816 (DOTweenTransformLocalMoveY_t249885831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveY::OnEnter()
extern "C"  void DOTweenTransformLocalMoveY_OnEnter_m1191686002 (DOTweenTransformLocalMoveY_t249885831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveY::<OnEnter>m__B8()
extern "C"  void DOTweenTransformLocalMoveY_U3COnEnterU3Em__B8_m3969597593 (DOTweenTransformLocalMoveY_t249885831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveY::<OnEnter>m__B9()
extern "C"  void DOTweenTransformLocalMoveY_U3COnEnterU3Em__B9_m3828435092 (DOTweenTransformLocalMoveY_t249885831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
