﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName
struct GetAnimatorCurrentTransitionInfoIsUserName_t1957114927;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName__ctor_m635970343 (GetAnimatorCurrentTransitionInfoIsUserName_t1957114927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_Reset_m823185696 (GetAnimatorCurrentTransitionInfoIsUserName_t1957114927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnEnter()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnEnter_m3080264638 (GetAnimatorCurrentTransitionInfoIsUserName_t1957114927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::OnActionUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_OnActionUpdate_m3246601349 (GetAnimatorCurrentTransitionInfoIsUserName_t1957114927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsUserName::IsName()
extern "C"  void GetAnimatorCurrentTransitionInfoIsUserName_IsName_m1855619896 (GetAnimatorCurrentTransitionInfoIsUserName_t1957114927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
