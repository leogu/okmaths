﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleFrom
struct iTweenScaleFrom_t1258603452;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::.ctor()
extern "C"  void iTweenScaleFrom__ctor_m2718723226 (iTweenScaleFrom_t1258603452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::Reset()
extern "C"  void iTweenScaleFrom_Reset_m1496829437 (iTweenScaleFrom_t1258603452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::OnEnter()
extern "C"  void iTweenScaleFrom_OnEnter_m3516451365 (iTweenScaleFrom_t1258603452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::OnExit()
extern "C"  void iTweenScaleFrom_OnExit_m2960680017 (iTweenScaleFrom_t1258603452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleFrom::DoiTween()
extern "C"  void iTweenScaleFrom_DoiTween_m1059656063 (iTweenScaleFrom_t1258603452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
