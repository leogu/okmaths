﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorInt
struct GetAnimatorInt_t3251388236;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::.ctor()
extern "C"  void GetAnimatorInt__ctor_m3596388042 (GetAnimatorInt_t3251388236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::Reset()
extern "C"  void GetAnimatorInt_Reset_m94900377 (GetAnimatorInt_t3251388236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnEnter()
extern "C"  void GetAnimatorInt_OnEnter_m3800342793 (GetAnimatorInt_t3251388236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::OnActionUpdate()
extern "C"  void GetAnimatorInt_OnActionUpdate_m2604112278 (GetAnimatorInt_t3251388236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorInt::GetParameter()
extern "C"  void GetAnimatorInt_GetParameter_m3407538367 (GetAnimatorInt_t3251388236 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
