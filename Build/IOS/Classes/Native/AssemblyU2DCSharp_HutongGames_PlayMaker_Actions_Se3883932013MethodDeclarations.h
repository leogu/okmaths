﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGameObject
struct SetGameObject_t3883932013;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGameObject::.ctor()
extern "C"  void SetGameObject__ctor_m2827274311 (SetGameObject_t3883932013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::Reset()
extern "C"  void SetGameObject_Reset_m1760655562 (SetGameObject_t3883932013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::OnEnter()
extern "C"  void SetGameObject_OnEnter_m2870131380 (SetGameObject_t3883932013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameObject::OnUpdate()
extern "C"  void SetGameObject_OnUpdate_m697534351 (SetGameObject_t3883932013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
