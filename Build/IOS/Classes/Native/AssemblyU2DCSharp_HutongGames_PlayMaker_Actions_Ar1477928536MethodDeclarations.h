﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListRemoveRange
struct ArrayListRemoveRange_t1477928536;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveRange::.ctor()
extern "C"  void ArrayListRemoveRange__ctor_m135938258 (ArrayListRemoveRange_t1477928536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveRange::Reset()
extern "C"  void ArrayListRemoveRange_Reset_m3596215209 (ArrayListRemoveRange_t1477928536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveRange::OnEnter()
extern "C"  void ArrayListRemoveRange_OnEnter_m1274649881 (ArrayListRemoveRange_t1477928536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveRange::doArrayListRemoveRange()
extern "C"  void ArrayListRemoveRange_doArrayListRemoveRange_m2224714785 (ArrayListRemoveRange_t1477928536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
