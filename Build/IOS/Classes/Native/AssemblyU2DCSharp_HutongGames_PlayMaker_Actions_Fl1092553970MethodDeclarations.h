﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatSubtract
struct FloatSubtract_t1092553970;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::.ctor()
extern "C"  void FloatSubtract__ctor_m2463837092 (FloatSubtract_t1092553970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::Reset()
extern "C"  void FloatSubtract_Reset_m3102785295 (FloatSubtract_t1092553970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnEnter()
extern "C"  void FloatSubtract_OnEnter_m855184407 (FloatSubtract_t1092553970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::OnUpdate()
extern "C"  void FloatSubtract_OnUpdate_m781894026 (FloatSubtract_t1092553970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSubtract::DoFloatSubtract()
extern "C"  void FloatSubtract_DoFloatSubtract_m204435257 (FloatSubtract_t1092553970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
