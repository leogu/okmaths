﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndVertical
struct GUILayoutEndVertical_t2297548810;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::.ctor()
extern "C"  void GUILayoutEndVertical__ctor_m2628921172 (GUILayoutEndVertical_t2297548810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::Reset()
extern "C"  void GUILayoutEndVertical_Reset_m1556601651 (GUILayoutEndVertical_t2297548810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndVertical::OnGUI()
extern "C"  void GUILayoutEndVertical_OnGUI_m1320511340 (GUILayoutEndVertical_t2297548810 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
