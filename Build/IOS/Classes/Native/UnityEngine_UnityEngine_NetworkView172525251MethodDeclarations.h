﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NetworkView
struct NetworkView_t172525251;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"

// System.Void UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)
extern "C"  void NetworkView_Internal_GetViewID_m634412743 (NetworkView_t172525251 * __this, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkViewID UnityEngine.NetworkView::get_viewID()
extern "C"  NetworkViewID_t3942988548  NetworkView_get_viewID_m1244180137 (NetworkView_t172525251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NetworkView::get_isMine()
extern "C"  bool NetworkView_get_isMine_m1275126880 (NetworkView_t172525251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer UnityEngine.NetworkView::get_owner()
extern "C"  NetworkPlayer_t1243528291  NetworkView_get_owner_m4018248619 (NetworkView_t172525251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t172525251 * NetworkView_Find_m143547961 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t172525251 * NetworkView_INTERNAL_CALL_Find_m526018730 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
