﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiImageSetSprite
struct uGuiImageSetSprite_t842539340;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiImageSetSprite::.ctor()
extern "C"  void uGuiImageSetSprite__ctor_m1470360864 (uGuiImageSetSprite_t842539340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageSetSprite::Reset()
extern "C"  void uGuiImageSetSprite_Reset_m4039261113 (uGuiImageSetSprite_t842539340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageSetSprite::OnEnter()
extern "C"  void uGuiImageSetSprite_OnEnter_m3791039665 (uGuiImageSetSprite_t842539340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageSetSprite::DoSetImageSourceValue()
extern "C"  void uGuiImageSetSprite_DoSetImageSourceValue_m2533626398 (uGuiImageSetSprite_t842539340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageSetSprite::OnExit()
extern "C"  void uGuiImageSetSprite_OnExit_m164002069 (uGuiImageSetSprite_t842539340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
