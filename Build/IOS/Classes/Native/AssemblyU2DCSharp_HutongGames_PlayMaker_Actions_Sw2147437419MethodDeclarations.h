﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SwipeGestureEvent
struct SwipeGestureEvent_t2147437419;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"

// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::.ctor()
extern "C"  void SwipeGestureEvent__ctor_m2483461365 (SwipeGestureEvent_t2147437419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::Reset()
extern "C"  void SwipeGestureEvent_Reset_m3207490920 (SwipeGestureEvent_t2147437419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::OnEnter()
extern "C"  void SwipeGestureEvent_OnEnter_m887041842 (SwipeGestureEvent_t2147437419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::OnUpdate()
extern "C"  void SwipeGestureEvent_OnUpdate_m440510257 (SwipeGestureEvent_t2147437419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SwipeGestureEvent::TestForSwipeGesture(UnityEngine.Touch)
extern "C"  void SwipeGestureEvent_TestForSwipeGesture_m568710009 (SwipeGestureEvent_t2147437419 * __this, Touch_t407273883  ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
