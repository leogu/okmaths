﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldActivate
struct uGuiInputFieldActivate_t771690489;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldActivate::.ctor()
extern "C"  void uGuiInputFieldActivate__ctor_m2972080337 (uGuiInputFieldActivate_t771690489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldActivate::Reset()
extern "C"  void uGuiInputFieldActivate_Reset_m357086570 (uGuiInputFieldActivate_t771690489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldActivate::OnEnter()
extern "C"  void uGuiInputFieldActivate_OnEnter_m1188515448 (uGuiInputFieldActivate_t771690489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldActivate::DoAction()
extern "C"  void uGuiInputFieldActivate_DoAction_m3669334478 (uGuiInputFieldActivate_t771690489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldActivate::OnExit()
extern "C"  void uGuiInputFieldActivate_OnExit_m3883931204 (uGuiInputFieldActivate_t771690489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
