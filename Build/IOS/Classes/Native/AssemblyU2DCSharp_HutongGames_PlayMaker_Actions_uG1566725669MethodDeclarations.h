﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha
struct uGuiCanvasGroupSetAlpha_t1566725669;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::.ctor()
extern "C"  void uGuiCanvasGroupSetAlpha__ctor_m883817725 (uGuiCanvasGroupSetAlpha_t1566725669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::Reset()
extern "C"  void uGuiCanvasGroupSetAlpha_Reset_m2392209442 (uGuiCanvasGroupSetAlpha_t1566725669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::OnEnter()
extern "C"  void uGuiCanvasGroupSetAlpha_OnEnter_m2587954688 (uGuiCanvasGroupSetAlpha_t1566725669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::OnUpdate()
extern "C"  void uGuiCanvasGroupSetAlpha_OnUpdate_m763436961 (uGuiCanvasGroupSetAlpha_t1566725669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::DoSetValue()
extern "C"  void uGuiCanvasGroupSetAlpha_DoSetValue_m3716290643 (uGuiCanvasGroupSetAlpha_t1566725669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::OnExit()
extern "C"  void uGuiCanvasGroupSetAlpha_OnExit_m4206928332 (uGuiCanvasGroupSetAlpha_t1566725669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
