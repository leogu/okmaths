﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetLastPing
struct NetworkGetLastPing_t765469144;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetLastPing::.ctor()
extern "C"  void NetworkGetLastPing__ctor_m505461530 (NetworkGetLastPing_t765469144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLastPing::Reset()
extern "C"  void NetworkGetLastPing_Reset_m2841038145 (NetworkGetLastPing_t765469144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLastPing::OnEnter()
extern "C"  void NetworkGetLastPing_OnEnter_m2681745881 (NetworkGetLastPing_t765469144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLastPing::OnUpdate()
extern "C"  void NetworkGetLastPing_OnUpdate_m1661794276 (NetworkGetLastPing_t765469144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLastPing::GetLastPing()
extern "C"  void NetworkGetLastPing_GetLastPing_m3812961282 (NetworkGetLastPing_t765469144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
