﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUITextureColor
struct SetGUITextureColor_t439899611;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::.ctor()
extern "C"  void SetGUITextureColor__ctor_m4220975461 (SetGUITextureColor_t439899611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::Reset()
extern "C"  void SetGUITextureColor_Reset_m995895116 (SetGUITextureColor_t439899611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::OnEnter()
extern "C"  void SetGUITextureColor_OnEnter_m2973580422 (SetGUITextureColor_t439899611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::OnUpdate()
extern "C"  void SetGUITextureColor_OnUpdate_m839019585 (SetGUITextureColor_t439899611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureColor::DoSetGUITextureColor()
extern "C"  void SetGUITextureColor_DoSetGUITextureColor_m1326907137 (SetGUITextureColor_t439899611 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
