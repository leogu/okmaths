﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight
struct ArrayListGetClosestGameObjectInSight_t2697091775;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight::.ctor()
extern "C"  void ArrayListGetClosestGameObjectInSight__ctor_m1799004471 (ArrayListGetClosestGameObjectInSight_t2697091775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight::Reset()
extern "C"  void ArrayListGetClosestGameObjectInSight_Reset_m1983776112 (ArrayListGetClosestGameObjectInSight_t2697091775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight::OnEnter()
extern "C"  void ArrayListGetClosestGameObjectInSight_OnEnter_m3288275406 (ArrayListGetClosestGameObjectInSight_t2697091775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight::OnUpdate()
extern "C"  void ArrayListGetClosestGameObjectInSight_OnUpdate_m1641797375 (ArrayListGetClosestGameObjectInSight_t2697091775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight::DoFindClosestGo()
extern "C"  void ArrayListGetClosestGameObjectInSight_DoFindClosestGo_m3698706360 (ArrayListGetClosestGameObjectInSight_t2697091775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObjectInSight::DoLineCast(UnityEngine.GameObject)
extern "C"  bool ArrayListGetClosestGameObjectInSight_DoLineCast_m2805290295 (ArrayListGetClosestGameObjectInSight_t2697091775 * __this, GameObject_t1756533147 * ___toGameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
