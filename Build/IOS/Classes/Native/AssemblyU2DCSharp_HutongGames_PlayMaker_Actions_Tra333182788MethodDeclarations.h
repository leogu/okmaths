﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Translate
struct Translate_t333182788;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Translate::.ctor()
extern "C"  void Translate__ctor_m2395183408 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::Reset()
extern "C"  void Translate_Reset_m521612805 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnPreprocess()
extern "C"  void Translate_OnPreprocess_m1392436717 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnEnter()
extern "C"  void Translate_OnEnter_m468967381 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnUpdate()
extern "C"  void Translate_OnUpdate_m2964721022 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnLateUpdate()
extern "C"  void Translate_OnLateUpdate_m3020568162 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::OnFixedUpdate()
extern "C"  void Translate_OnFixedUpdate_m2067940644 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Translate::DoTranslate()
extern "C"  void Translate_DoTranslate_m552439117 (Translate_t333182788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
