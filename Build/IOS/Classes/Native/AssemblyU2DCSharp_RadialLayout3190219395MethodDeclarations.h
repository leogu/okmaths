﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RadialLayout
struct RadialLayout_t3190219395;

#include "codegen/il2cpp-codegen.h"

// System.Void RadialLayout::.ctor()
extern "C"  void RadialLayout__ctor_m1625504540 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialLayout::OnEnable()
extern "C"  void RadialLayout_OnEnable_m1774899056 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialLayout::SetLayoutHorizontal()
extern "C"  void RadialLayout_SetLayoutHorizontal_m485943682 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialLayout::SetLayoutVertical()
extern "C"  void RadialLayout_SetLayoutVertical_m4152727748 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialLayout::CalculateLayoutInputVertical()
extern "C"  void RadialLayout_CalculateLayoutInputVertical_m181219130 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialLayout::CalculateLayoutInputHorizontal()
extern "C"  void RadialLayout_CalculateLayoutInputHorizontal_m1439542780 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialLayout::CalculateRadial()
extern "C"  void RadialLayout_CalculateRadial_m1536200425 (RadialLayout_t3190219395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
