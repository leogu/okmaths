﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BaseLogAction
struct BaseLogAction_t2924498175;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BaseLogAction::.ctor()
extern "C"  void BaseLogAction__ctor_m223766493 (BaseLogAction_t2924498175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseLogAction::Reset()
extern "C"  void BaseLogAction_Reset_m120093240 (BaseLogAction_t2924498175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
