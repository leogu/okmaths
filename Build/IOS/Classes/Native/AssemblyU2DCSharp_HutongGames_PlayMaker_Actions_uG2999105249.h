﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue
struct  uGuiScrollbarSetValue_t2999105249  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::value
	FsmFloat_t937133978 * ___value_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::_scrollbar
	Scrollbar_t3248359358 * ____scrollbar_15;
	// System.Single HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::_originalValue
	float ____originalValue_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetValue_t2999105249, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetValue_t2999105249, ___value_12)); }
	inline FsmFloat_t937133978 * get_value_12() const { return ___value_12; }
	inline FsmFloat_t937133978 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmFloat_t937133978 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetValue_t2999105249, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetValue_t2999105249, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__scrollbar_15() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetValue_t2999105249, ____scrollbar_15)); }
	inline Scrollbar_t3248359358 * get__scrollbar_15() const { return ____scrollbar_15; }
	inline Scrollbar_t3248359358 ** get_address_of__scrollbar_15() { return &____scrollbar_15; }
	inline void set__scrollbar_15(Scrollbar_t3248359358 * value)
	{
		____scrollbar_15 = value;
		Il2CppCodeGenWriteBarrier(&____scrollbar_15, value);
	}

	inline static int32_t get_offset_of__originalValue_16() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetValue_t2999105249, ____originalValue_16)); }
	inline float get__originalValue_16() const { return ____originalValue_16; }
	inline float* get_address_of__originalValue_16() { return &____originalValue_16; }
	inline void set__originalValue_16(float value)
	{
		____originalValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
