﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip
struct CapturePoseAsAnimationClip_t2661379439;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"

// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::.ctor()
extern "C"  void CapturePoseAsAnimationClip__ctor_m3218309437 (CapturePoseAsAnimationClip_t2661379439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::Reset()
extern "C"  void CapturePoseAsAnimationClip_Reset_m882155548 (CapturePoseAsAnimationClip_t2661379439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::OnEnter()
extern "C"  void CapturePoseAsAnimationClip_OnEnter_m1386288534 (CapturePoseAsAnimationClip_t2661379439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::DoCaptureAnimationClip()
extern "C"  void CapturePoseAsAnimationClip_DoCaptureAnimationClip_m3106247204 (CapturePoseAsAnimationClip_t2661379439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CaptureTransform(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CaptureTransform_m943277203 (CapturePoseAsAnimationClip_t2661379439 * __this, Transform_t3275118058 * ___transform0, String_t* ___path1, AnimationClip_t3510324950 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CapturePosition(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CapturePosition_m1159518324 (CapturePoseAsAnimationClip_t2661379439 * __this, Transform_t3275118058 * ___transform0, String_t* ___path1, AnimationClip_t3510324950 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CaptureRotation(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CaptureRotation_m4139595907 (CapturePoseAsAnimationClip_t2661379439 * __this, Transform_t3275118058 * ___transform0, String_t* ___path1, AnimationClip_t3510324950 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::CaptureScale(UnityEngine.Transform,System.String,UnityEngine.AnimationClip)
extern "C"  void CapturePoseAsAnimationClip_CaptureScale_m2101645075 (CapturePoseAsAnimationClip_t2661379439 * __this, Transform_t3275118058 * ___transform0, String_t* ___path1, AnimationClip_t3510324950 * ___clip2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CapturePoseAsAnimationClip::SetConstantCurve(UnityEngine.AnimationClip,System.String,System.String,System.Single)
extern "C"  void CapturePoseAsAnimationClip_SetConstantCurve_m2450133194 (CapturePoseAsAnimationClip_t2661379439 * __this, AnimationClip_t3510324950 * ___clip0, String_t* ___childPath1, String_t* ___propertyPath2, float ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
