﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Wait
struct Wait_t98790123;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Wait::.ctor()
extern "C"  void Wait__ctor_m1060358453 (Wait_t98790123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Wait::Reset()
extern "C"  void Wait_Reset_m3907024892 (Wait_t98790123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Wait::OnEnter()
extern "C"  void Wait_OnEnter_m864584662 (Wait_t98790123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Wait::OnUpdate()
extern "C"  void Wait_OnUpdate_m1085155857 (Wait_t98790123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
