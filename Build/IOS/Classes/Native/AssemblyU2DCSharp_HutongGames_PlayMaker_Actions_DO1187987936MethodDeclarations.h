﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLayoutElementPreferredSize
struct DOTweenLayoutElementPreferredSize_t1187987936;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementPreferredSize::.ctor()
extern "C"  void DOTweenLayoutElementPreferredSize__ctor_m2089124360 (DOTweenLayoutElementPreferredSize_t1187987936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementPreferredSize::Reset()
extern "C"  void DOTweenLayoutElementPreferredSize_Reset_m1943446101 (DOTweenLayoutElementPreferredSize_t1187987936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementPreferredSize::OnEnter()
extern "C"  void DOTweenLayoutElementPreferredSize_OnEnter_m1763946349 (DOTweenLayoutElementPreferredSize_t1187987936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementPreferredSize::<OnEnter>m__44()
extern "C"  void DOTweenLayoutElementPreferredSize_U3COnEnterU3Em__44_m186474648 (DOTweenLayoutElementPreferredSize_t1187987936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementPreferredSize::<OnEnter>m__45()
extern "C"  void DOTweenLayoutElementPreferredSize_U3COnEnterU3Em__45_m186474743 (DOTweenLayoutElementPreferredSize_t1187987936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
