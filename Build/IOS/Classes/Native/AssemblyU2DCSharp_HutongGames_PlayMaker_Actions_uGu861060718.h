﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2652774230.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSetColorBlock
struct  uGuiSetColorBlock_t861060718  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSetColorBlock::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSetColorBlock::fadeDuration
	FsmFloat_t937133978 * ___fadeDuration_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSetColorBlock::colorMultiplier
	FsmFloat_t937133978 * ___colorMultiplier_13;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiSetColorBlock::normalColor
	FsmColor_t118301965 * ___normalColor_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiSetColorBlock::pressedColor
	FsmColor_t118301965 * ___pressedColor_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiSetColorBlock::highlightedColor
	FsmColor_t118301965 * ___highlightedColor_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiSetColorBlock::disabledColor
	FsmColor_t118301965 * ___disabledColor_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSetColorBlock::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_18;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSetColorBlock::everyFrame
	bool ___everyFrame_19;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiSetColorBlock::_selectable
	Selectable_t1490392188 * ____selectable_20;
	// UnityEngine.UI.ColorBlock HutongGames.PlayMaker.Actions.uGuiSetColorBlock::_colorBlock
	ColorBlock_t2652774230  ____colorBlock_21;
	// UnityEngine.UI.ColorBlock HutongGames.PlayMaker.Actions.uGuiSetColorBlock::_originalColorBlock
	ColorBlock_t2652774230  ____originalColorBlock_22;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_fadeDuration_12() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___fadeDuration_12)); }
	inline FsmFloat_t937133978 * get_fadeDuration_12() const { return ___fadeDuration_12; }
	inline FsmFloat_t937133978 ** get_address_of_fadeDuration_12() { return &___fadeDuration_12; }
	inline void set_fadeDuration_12(FsmFloat_t937133978 * value)
	{
		___fadeDuration_12 = value;
		Il2CppCodeGenWriteBarrier(&___fadeDuration_12, value);
	}

	inline static int32_t get_offset_of_colorMultiplier_13() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___colorMultiplier_13)); }
	inline FsmFloat_t937133978 * get_colorMultiplier_13() const { return ___colorMultiplier_13; }
	inline FsmFloat_t937133978 ** get_address_of_colorMultiplier_13() { return &___colorMultiplier_13; }
	inline void set_colorMultiplier_13(FsmFloat_t937133978 * value)
	{
		___colorMultiplier_13 = value;
		Il2CppCodeGenWriteBarrier(&___colorMultiplier_13, value);
	}

	inline static int32_t get_offset_of_normalColor_14() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___normalColor_14)); }
	inline FsmColor_t118301965 * get_normalColor_14() const { return ___normalColor_14; }
	inline FsmColor_t118301965 ** get_address_of_normalColor_14() { return &___normalColor_14; }
	inline void set_normalColor_14(FsmColor_t118301965 * value)
	{
		___normalColor_14 = value;
		Il2CppCodeGenWriteBarrier(&___normalColor_14, value);
	}

	inline static int32_t get_offset_of_pressedColor_15() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___pressedColor_15)); }
	inline FsmColor_t118301965 * get_pressedColor_15() const { return ___pressedColor_15; }
	inline FsmColor_t118301965 ** get_address_of_pressedColor_15() { return &___pressedColor_15; }
	inline void set_pressedColor_15(FsmColor_t118301965 * value)
	{
		___pressedColor_15 = value;
		Il2CppCodeGenWriteBarrier(&___pressedColor_15, value);
	}

	inline static int32_t get_offset_of_highlightedColor_16() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___highlightedColor_16)); }
	inline FsmColor_t118301965 * get_highlightedColor_16() const { return ___highlightedColor_16; }
	inline FsmColor_t118301965 ** get_address_of_highlightedColor_16() { return &___highlightedColor_16; }
	inline void set_highlightedColor_16(FsmColor_t118301965 * value)
	{
		___highlightedColor_16 = value;
		Il2CppCodeGenWriteBarrier(&___highlightedColor_16, value);
	}

	inline static int32_t get_offset_of_disabledColor_17() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___disabledColor_17)); }
	inline FsmColor_t118301965 * get_disabledColor_17() const { return ___disabledColor_17; }
	inline FsmColor_t118301965 ** get_address_of_disabledColor_17() { return &___disabledColor_17; }
	inline void set_disabledColor_17(FsmColor_t118301965 * value)
	{
		___disabledColor_17 = value;
		Il2CppCodeGenWriteBarrier(&___disabledColor_17, value);
	}

	inline static int32_t get_offset_of_resetOnExit_18() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___resetOnExit_18)); }
	inline FsmBool_t664485696 * get_resetOnExit_18() const { return ___resetOnExit_18; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_18() { return &___resetOnExit_18; }
	inline void set_resetOnExit_18(FsmBool_t664485696 * value)
	{
		___resetOnExit_18 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_18, value);
	}

	inline static int32_t get_offset_of_everyFrame_19() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ___everyFrame_19)); }
	inline bool get_everyFrame_19() const { return ___everyFrame_19; }
	inline bool* get_address_of_everyFrame_19() { return &___everyFrame_19; }
	inline void set_everyFrame_19(bool value)
	{
		___everyFrame_19 = value;
	}

	inline static int32_t get_offset_of__selectable_20() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ____selectable_20)); }
	inline Selectable_t1490392188 * get__selectable_20() const { return ____selectable_20; }
	inline Selectable_t1490392188 ** get_address_of__selectable_20() { return &____selectable_20; }
	inline void set__selectable_20(Selectable_t1490392188 * value)
	{
		____selectable_20 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_20, value);
	}

	inline static int32_t get_offset_of__colorBlock_21() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ____colorBlock_21)); }
	inline ColorBlock_t2652774230  get__colorBlock_21() const { return ____colorBlock_21; }
	inline ColorBlock_t2652774230 * get_address_of__colorBlock_21() { return &____colorBlock_21; }
	inline void set__colorBlock_21(ColorBlock_t2652774230  value)
	{
		____colorBlock_21 = value;
	}

	inline static int32_t get_offset_of__originalColorBlock_22() { return static_cast<int32_t>(offsetof(uGuiSetColorBlock_t861060718, ____originalColorBlock_22)); }
	inline ColorBlock_t2652774230  get__originalColorBlock_22() const { return ____originalColorBlock_22; }
	inline ColorBlock_t2652774230 * get_address_of__originalColorBlock_22() { return &____originalColorBlock_22; }
	inline void set__originalColorBlock_22(ColorBlock_t2652774230  value)
	{
		____originalColorBlock_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
