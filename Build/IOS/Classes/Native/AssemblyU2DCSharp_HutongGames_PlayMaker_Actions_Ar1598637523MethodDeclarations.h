﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListReverse
struct ArrayListReverse_t1598637523;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListReverse::.ctor()
extern "C"  void ArrayListReverse__ctor_m196356633 (ArrayListReverse_t1598637523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListReverse::Reset()
extern "C"  void ArrayListReverse_Reset_m993937600 (ArrayListReverse_t1598637523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListReverse::OnEnter()
extern "C"  void ArrayListReverse_OnEnter_m62996682 (ArrayListReverse_t1598637523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListReverse::DoArrayListReverse()
extern "C"  void ArrayListReverse_DoArrayListReverse_m3926506497 (ArrayListReverse_t1598637523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
