﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump
struct DOTweenRigidbody2DJump_t9691787;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::.ctor()
extern "C"  void DOTweenRigidbody2DJump__ctor_m3865532745 (DOTweenRigidbody2DJump_t9691787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::Reset()
extern "C"  void DOTweenRigidbody2DJump_Reset_m1018568080 (DOTweenRigidbody2DJump_t9691787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::OnEnter()
extern "C"  void DOTweenRigidbody2DJump_OnEnter_m2012712114 (DOTweenRigidbody2DJump_t9691787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::<OnEnter>m__78()
extern "C"  void DOTweenRigidbody2DJump_U3COnEnterU3Em__78_m882704894 (DOTweenRigidbody2DJump_t9691787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::<OnEnter>m__79()
extern "C"  void DOTweenRigidbody2DJump_U3COnEnterU3Em__79_m741542393 (DOTweenRigidbody2DJump_t9691787 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
