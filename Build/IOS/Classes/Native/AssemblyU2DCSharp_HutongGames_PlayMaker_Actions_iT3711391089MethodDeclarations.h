﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPunchPosition
struct iTweenPunchPosition_t3711391089;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::.ctor()
extern "C"  void iTweenPunchPosition__ctor_m2179103947 (iTweenPunchPosition_t3711391089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::Reset()
extern "C"  void iTweenPunchPosition_Reset_m360141030 (iTweenPunchPosition_t3711391089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::OnEnter()
extern "C"  void iTweenPunchPosition_OnEnter_m1784215048 (iTweenPunchPosition_t3711391089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::OnExit()
extern "C"  void iTweenPunchPosition_OnExit_m4069208608 (iTweenPunchPosition_t3711391089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchPosition::DoiTween()
extern "C"  void iTweenPunchPosition_DoiTween_m28049690 (iTweenPunchPosition_t3711391089 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
