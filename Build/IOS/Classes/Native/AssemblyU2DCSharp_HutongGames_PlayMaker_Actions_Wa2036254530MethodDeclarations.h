﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d
struct WakeAllRigidBodies2d_t2036254530;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::.ctor()
extern "C"  void WakeAllRigidBodies2d__ctor_m1933120400 (WakeAllRigidBodies2d_t2036254530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::Reset()
extern "C"  void WakeAllRigidBodies2d_Reset_m4265750215 (WakeAllRigidBodies2d_t2036254530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::OnEnter()
extern "C"  void WakeAllRigidBodies2d_OnEnter_m2396972815 (WakeAllRigidBodies2d_t2036254530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::OnUpdate()
extern "C"  void WakeAllRigidBodies2d_OnUpdate_m4064076190 (WakeAllRigidBodies2d_t2036254530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies2d::DoWakeAll()
extern "C"  void WakeAllRigidBodies2d_DoWakeAll_m1965574156 (WakeAllRigidBodies2d_t2036254530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
