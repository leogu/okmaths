﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue
struct  ArrayListGetNearestFloatValue_t2416458106  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::floatValue
	FsmFloat_t937133978 * ___floatValue_14;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::everyframe
	bool ___everyframe_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::nearestIndex
	FsmInt_t1273009179 * ___nearestIndex_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::nearestValue
	FsmFloat_t937133978 * ___nearestValue_17;
	// System.Collections.Generic.List`1<System.Single> HutongGames.PlayMaker.Actions.ArrayListGetNearestFloatValue::_floats
	List_1_t1445631064 * ____floats_18;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_floatValue_14() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ___floatValue_14)); }
	inline FsmFloat_t937133978 * get_floatValue_14() const { return ___floatValue_14; }
	inline FsmFloat_t937133978 ** get_address_of_floatValue_14() { return &___floatValue_14; }
	inline void set_floatValue_14(FsmFloat_t937133978 * value)
	{
		___floatValue_14 = value;
		Il2CppCodeGenWriteBarrier(&___floatValue_14, value);
	}

	inline static int32_t get_offset_of_everyframe_15() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ___everyframe_15)); }
	inline bool get_everyframe_15() const { return ___everyframe_15; }
	inline bool* get_address_of_everyframe_15() { return &___everyframe_15; }
	inline void set_everyframe_15(bool value)
	{
		___everyframe_15 = value;
	}

	inline static int32_t get_offset_of_nearestIndex_16() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ___nearestIndex_16)); }
	inline FsmInt_t1273009179 * get_nearestIndex_16() const { return ___nearestIndex_16; }
	inline FsmInt_t1273009179 ** get_address_of_nearestIndex_16() { return &___nearestIndex_16; }
	inline void set_nearestIndex_16(FsmInt_t1273009179 * value)
	{
		___nearestIndex_16 = value;
		Il2CppCodeGenWriteBarrier(&___nearestIndex_16, value);
	}

	inline static int32_t get_offset_of_nearestValue_17() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ___nearestValue_17)); }
	inline FsmFloat_t937133978 * get_nearestValue_17() const { return ___nearestValue_17; }
	inline FsmFloat_t937133978 ** get_address_of_nearestValue_17() { return &___nearestValue_17; }
	inline void set_nearestValue_17(FsmFloat_t937133978 * value)
	{
		___nearestValue_17 = value;
		Il2CppCodeGenWriteBarrier(&___nearestValue_17, value);
	}

	inline static int32_t get_offset_of__floats_18() { return static_cast<int32_t>(offsetof(ArrayListGetNearestFloatValue_t2416458106, ____floats_18)); }
	inline List_1_t1445631064 * get__floats_18() const { return ____floats_18; }
	inline List_1_t1445631064 ** get_address_of__floats_18() { return &____floats_18; }
	inline void set__floats_18(List_1_t1445631064 * value)
	{
		____floats_18 = value;
		Il2CppCodeGenWriteBarrier(&____floats_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
