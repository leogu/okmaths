﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetKeyDown
struct GetKeyDown_t3674957585;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::.ctor()
extern "C"  void GetKeyDown__ctor_m538877509 (GetKeyDown_t3674957585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::Reset()
extern "C"  void GetKeyDown_Reset_m722562654 (GetKeyDown_t3674957585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyDown::OnUpdate()
extern "C"  void GetKeyDown_OnUpdate_m1522799377 (GetKeyDown_t3674957585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
