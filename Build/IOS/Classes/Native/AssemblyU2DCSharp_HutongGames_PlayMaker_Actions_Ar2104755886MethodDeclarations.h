﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListInsert
struct ArrayListInsert_t2104755886;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListInsert::.ctor()
extern "C"  void ArrayListInsert__ctor_m1018126086 (ArrayListInsert_t2104755886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListInsert::Reset()
extern "C"  void ArrayListInsert_Reset_m885275179 (ArrayListInsert_t2104755886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListInsert::OnEnter()
extern "C"  void ArrayListInsert_OnEnter_m1391047867 (ArrayListInsert_t2104755886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListInsert::doArrayListInsert()
extern "C"  void ArrayListInsert_doArrayListInsert_m1827492345 (ArrayListInsert_t2104755886 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
