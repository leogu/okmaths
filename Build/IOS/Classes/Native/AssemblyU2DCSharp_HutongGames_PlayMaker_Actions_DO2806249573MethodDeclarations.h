﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTextFade
struct DOTweenTextFade_t2806249573;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTextFade::.ctor()
extern "C"  void DOTweenTextFade__ctor_m3814929411 (DOTweenTextFade_t2806249573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextFade::Reset()
extern "C"  void DOTweenTextFade_Reset_m268082334 (DOTweenTextFade_t2806249573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextFade::OnEnter()
extern "C"  void DOTweenTextFade_OnEnter_m3200265792 (DOTweenTextFade_t2806249573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextFade::<OnEnter>m__9E()
extern "C"  void DOTweenTextFade_U3COnEnterU3Em__9E_m3869279173 (DOTweenTextFade_t2806249573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextFade::<OnEnter>m__9F()
extern "C"  void DOTweenTextFade_U3COnEnterU3Em__9F_m3869279272 (DOTweenTextFade_t2806249573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
