﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString2414474701.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.String HutongGames.PlayMaker.FsmString::get_Value()
extern "C"  String_t* FsmString_get_Value_m3775166715 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::set_Value(System.String)
extern "C"  void FsmString_set_Value_m1767060322 (FsmString_t2414474701 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmString::get_RawValue()
extern "C"  Il2CppObject * FsmString_get_RawValue_m4174395375 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::set_RawValue(System.Object)
extern "C"  void FsmString_set_RawValue_m3536745086 (FsmString_t2414474701 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::.ctor()
extern "C"  void FsmString__ctor_m1192568582 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::.ctor(System.String)
extern "C"  void FsmString__ctor_m596080948 (FsmString_t2414474701 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::.ctor(HutongGames.PlayMaker.FsmString)
extern "C"  void FsmString__ctor_m530730399 (FsmString_t2414474701 * __this, FsmString_t2414474701 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmString::Clone()
extern "C"  NamedVariable_t3026441313 * FsmString_Clone_m1355234315 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmString::get_VariableType()
extern "C"  int32_t FsmString_get_VariableType_m849678918 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmString::ToString()
extern "C"  String_t* FsmString_ToString_m2366060807 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmString::op_Implicit(System.String)
extern "C"  FsmString_t2414474701 * FsmString_op_Implicit_m1196227529 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmString::IsNullOrEmpty(HutongGames.PlayMaker.FsmString)
extern "C"  bool FsmString_IsNullOrEmpty_m1175937518 (Il2CppObject * __this /* static, unused */, FsmString_t2414474701 * ___fsmString0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
