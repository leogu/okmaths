﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21413314124.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m50054624_gshared (KeyValuePair_2_t1413314124 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m50054624(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1413314124 *, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))KeyValuePair_2__ctor_m50054624_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2460284350_gshared (KeyValuePair_2_t1413314124 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2460284350(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1413314124 *, const MethodInfo*))KeyValuePair_2_get_Key_m2460284350_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3052986025_gshared (KeyValuePair_2_t1413314124 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3052986025(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1413314124 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3052986025_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::get_Value()
extern "C"  RaycastHit2D_t4063908774  KeyValuePair_2_get_Value_m2648938910_gshared (KeyValuePair_2_t1413314124 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2648938910(__this, method) ((  RaycastHit2D_t4063908774  (*) (KeyValuePair_2_t1413314124 *, const MethodInfo*))KeyValuePair_2_get_Value_m2648938910_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3606416665_gshared (KeyValuePair_2_t1413314124 * __this, RaycastHit2D_t4063908774  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3606416665(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1413314124 *, RaycastHit2D_t4063908774 , const MethodInfo*))KeyValuePair_2_set_Value_m3606416665_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1150022243_gshared (KeyValuePair_2_t1413314124 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m1150022243(__this, method) ((  String_t* (*) (KeyValuePair_2_t1413314124 *, const MethodInfo*))KeyValuePair_2_ToString_m1150022243_gshared)(__this, method)
