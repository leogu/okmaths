﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t1021602117;
// HutongGames.PlayMaker.FsmDebugUtility
struct FsmDebugUtility_t882664281;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"

// System.Void HutongGames.PlayMaker.FsmDebugUtility::Log(HutongGames.PlayMaker.Fsm,System.String,System.Boolean)
extern "C"  void FsmDebugUtility_Log_m2171449719 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, String_t* ___text1, bool ___frameCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmDebugUtility::Log(System.String,System.Boolean)
extern "C"  void FsmDebugUtility_Log_m919598121 (Il2CppObject * __this /* static, unused */, String_t* ___text0, bool ___frameCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmDebugUtility::Log(UnityEngine.Object,System.String)
extern "C"  void FsmDebugUtility_Log_m248782008 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmDebugUtility::.ctor()
extern "C"  void FsmDebugUtility__ctor_m1718067542 (FsmDebugUtility_t882664281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
