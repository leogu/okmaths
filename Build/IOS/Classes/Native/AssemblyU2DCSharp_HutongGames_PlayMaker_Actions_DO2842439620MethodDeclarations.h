﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty
struct DOTweenMaterialFloatProperty_t2842439620;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::.ctor()
extern "C"  void DOTweenMaterialFloatProperty__ctor_m1368825328 (DOTweenMaterialFloatProperty_t2842439620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::Reset()
extern "C"  void DOTweenMaterialFloatProperty_Reset_m2199693513 (DOTweenMaterialFloatProperty_t2842439620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::OnEnter()
extern "C"  void DOTweenMaterialFloatProperty_OnEnter_m3575731817 (DOTweenMaterialFloatProperty_t2842439620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::<OnEnter>m__58()
extern "C"  void DOTweenMaterialFloatProperty_U3COnEnterU3Em__58_m1915399827 (DOTweenMaterialFloatProperty_t2842439620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::<OnEnter>m__59()
extern "C"  void DOTweenMaterialFloatProperty_U3COnEnterU3Em__59_m369518862 (DOTweenMaterialFloatProperty_t2842439620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
