﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRandomObject
struct GetRandomObject_t1887607794;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::.ctor()
extern "C"  void GetRandomObject__ctor_m4219125666 (GetRandomObject_t1887607794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::Reset()
extern "C"  void GetRandomObject_Reset_m3064368591 (GetRandomObject_t1887607794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::OnEnter()
extern "C"  void GetRandomObject_OnEnter_m2896028351 (GetRandomObject_t1887607794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::OnUpdate()
extern "C"  void GetRandomObject_OnUpdate_m3620961972 (GetRandomObject_t1887607794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomObject::DoGetRandomObject()
extern "C"  void GetRandomObject_DoGetRandomObject_m432255305 (GetRandomObject_t1887607794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
