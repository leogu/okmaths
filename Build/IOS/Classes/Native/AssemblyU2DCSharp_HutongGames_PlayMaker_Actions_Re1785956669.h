﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Camera
struct Camera_t189460977;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint
struct  RectTransformContainsScreenPoint_t1785956669  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::screenPointVector2
	FsmVector2_t2430450063 * ___screenPointVector2_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::orScreenPointVector3
	FsmVector3_t3996534004 * ___orScreenPointVector3_13;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::normalizedScreenPoint
	bool ___normalizedScreenPoint_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::camera
	FsmGameObject_t3097142863 * ___camera_15;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::everyFrame
	bool ___everyFrame_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::isContained
	FsmBool_t664485696 * ___isContained_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::isContainedEvent
	FsmEvent_t1258573736 * ___isContainedEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::isNotContainedEvent
	FsmEvent_t1258573736 * ___isNotContainedEvent_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::_rt
	RectTransform_t3349966182 * ____rt_20;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::_camera
	Camera_t189460977 * ____camera_21;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_screenPointVector2_12() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___screenPointVector2_12)); }
	inline FsmVector2_t2430450063 * get_screenPointVector2_12() const { return ___screenPointVector2_12; }
	inline FsmVector2_t2430450063 ** get_address_of_screenPointVector2_12() { return &___screenPointVector2_12; }
	inline void set_screenPointVector2_12(FsmVector2_t2430450063 * value)
	{
		___screenPointVector2_12 = value;
		Il2CppCodeGenWriteBarrier(&___screenPointVector2_12, value);
	}

	inline static int32_t get_offset_of_orScreenPointVector3_13() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___orScreenPointVector3_13)); }
	inline FsmVector3_t3996534004 * get_orScreenPointVector3_13() const { return ___orScreenPointVector3_13; }
	inline FsmVector3_t3996534004 ** get_address_of_orScreenPointVector3_13() { return &___orScreenPointVector3_13; }
	inline void set_orScreenPointVector3_13(FsmVector3_t3996534004 * value)
	{
		___orScreenPointVector3_13 = value;
		Il2CppCodeGenWriteBarrier(&___orScreenPointVector3_13, value);
	}

	inline static int32_t get_offset_of_normalizedScreenPoint_14() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___normalizedScreenPoint_14)); }
	inline bool get_normalizedScreenPoint_14() const { return ___normalizedScreenPoint_14; }
	inline bool* get_address_of_normalizedScreenPoint_14() { return &___normalizedScreenPoint_14; }
	inline void set_normalizedScreenPoint_14(bool value)
	{
		___normalizedScreenPoint_14 = value;
	}

	inline static int32_t get_offset_of_camera_15() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___camera_15)); }
	inline FsmGameObject_t3097142863 * get_camera_15() const { return ___camera_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_camera_15() { return &___camera_15; }
	inline void set_camera_15(FsmGameObject_t3097142863 * value)
	{
		___camera_15 = value;
		Il2CppCodeGenWriteBarrier(&___camera_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_isContained_17() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___isContained_17)); }
	inline FsmBool_t664485696 * get_isContained_17() const { return ___isContained_17; }
	inline FsmBool_t664485696 ** get_address_of_isContained_17() { return &___isContained_17; }
	inline void set_isContained_17(FsmBool_t664485696 * value)
	{
		___isContained_17 = value;
		Il2CppCodeGenWriteBarrier(&___isContained_17, value);
	}

	inline static int32_t get_offset_of_isContainedEvent_18() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___isContainedEvent_18)); }
	inline FsmEvent_t1258573736 * get_isContainedEvent_18() const { return ___isContainedEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_isContainedEvent_18() { return &___isContainedEvent_18; }
	inline void set_isContainedEvent_18(FsmEvent_t1258573736 * value)
	{
		___isContainedEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___isContainedEvent_18, value);
	}

	inline static int32_t get_offset_of_isNotContainedEvent_19() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ___isNotContainedEvent_19)); }
	inline FsmEvent_t1258573736 * get_isNotContainedEvent_19() const { return ___isNotContainedEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotContainedEvent_19() { return &___isNotContainedEvent_19; }
	inline void set_isNotContainedEvent_19(FsmEvent_t1258573736 * value)
	{
		___isNotContainedEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___isNotContainedEvent_19, value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ____rt_20)); }
	inline RectTransform_t3349966182 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3349966182 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3349966182 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier(&____rt_20, value);
	}

	inline static int32_t get_offset_of__camera_21() { return static_cast<int32_t>(offsetof(RectTransformContainsScreenPoint_t1785956669, ____camera_21)); }
	inline Camera_t189460977 * get__camera_21() const { return ____camera_21; }
	inline Camera_t189460977 ** get_address_of__camera_21() { return &____camera_21; }
	inline void set__camera_21(Camera_t189460977 * value)
	{
		____camera_21 = value;
		Il2CppCodeGenWriteBarrier(&____camera_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
