﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties
struct  uGuiCanvasGroupSetProperties_t3956917360  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::alpha
	FsmFloat_t937133978 * ___alpha_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::interactable
	FsmBool_t664485696 * ___interactable_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::blocksRaycasts
	FsmBool_t664485696 * ___blocksRaycasts_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::ignoreParentGroup
	FsmBool_t664485696 * ___ignoreParentGroup_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_16;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::everyFrame
	bool ___everyFrame_17;
	// UnityEngine.CanvasGroup HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::_comp
	CanvasGroup_t3296560743 * ____comp_18;
	// System.Single HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::_originalAlpha
	float ____originalAlpha_19;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::_originalInteractable
	bool ____originalInteractable_20;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::_originalBlocksRaycasts
	bool ____originalBlocksRaycasts_21;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::_originalIgnoreParentGroup
	bool ____originalIgnoreParentGroup_22;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_alpha_12() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___alpha_12)); }
	inline FsmFloat_t937133978 * get_alpha_12() const { return ___alpha_12; }
	inline FsmFloat_t937133978 ** get_address_of_alpha_12() { return &___alpha_12; }
	inline void set_alpha_12(FsmFloat_t937133978 * value)
	{
		___alpha_12 = value;
		Il2CppCodeGenWriteBarrier(&___alpha_12, value);
	}

	inline static int32_t get_offset_of_interactable_13() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___interactable_13)); }
	inline FsmBool_t664485696 * get_interactable_13() const { return ___interactable_13; }
	inline FsmBool_t664485696 ** get_address_of_interactable_13() { return &___interactable_13; }
	inline void set_interactable_13(FsmBool_t664485696 * value)
	{
		___interactable_13 = value;
		Il2CppCodeGenWriteBarrier(&___interactable_13, value);
	}

	inline static int32_t get_offset_of_blocksRaycasts_14() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___blocksRaycasts_14)); }
	inline FsmBool_t664485696 * get_blocksRaycasts_14() const { return ___blocksRaycasts_14; }
	inline FsmBool_t664485696 ** get_address_of_blocksRaycasts_14() { return &___blocksRaycasts_14; }
	inline void set_blocksRaycasts_14(FsmBool_t664485696 * value)
	{
		___blocksRaycasts_14 = value;
		Il2CppCodeGenWriteBarrier(&___blocksRaycasts_14, value);
	}

	inline static int32_t get_offset_of_ignoreParentGroup_15() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___ignoreParentGroup_15)); }
	inline FsmBool_t664485696 * get_ignoreParentGroup_15() const { return ___ignoreParentGroup_15; }
	inline FsmBool_t664485696 ** get_address_of_ignoreParentGroup_15() { return &___ignoreParentGroup_15; }
	inline void set_ignoreParentGroup_15(FsmBool_t664485696 * value)
	{
		___ignoreParentGroup_15 = value;
		Il2CppCodeGenWriteBarrier(&___ignoreParentGroup_15, value);
	}

	inline static int32_t get_offset_of_resetOnExit_16() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___resetOnExit_16)); }
	inline FsmBool_t664485696 * get_resetOnExit_16() const { return ___resetOnExit_16; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_16() { return &___resetOnExit_16; }
	inline void set_resetOnExit_16(FsmBool_t664485696 * value)
	{
		___resetOnExit_16 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}

	inline static int32_t get_offset_of__comp_18() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ____comp_18)); }
	inline CanvasGroup_t3296560743 * get__comp_18() const { return ____comp_18; }
	inline CanvasGroup_t3296560743 ** get_address_of__comp_18() { return &____comp_18; }
	inline void set__comp_18(CanvasGroup_t3296560743 * value)
	{
		____comp_18 = value;
		Il2CppCodeGenWriteBarrier(&____comp_18, value);
	}

	inline static int32_t get_offset_of__originalAlpha_19() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ____originalAlpha_19)); }
	inline float get__originalAlpha_19() const { return ____originalAlpha_19; }
	inline float* get_address_of__originalAlpha_19() { return &____originalAlpha_19; }
	inline void set__originalAlpha_19(float value)
	{
		____originalAlpha_19 = value;
	}

	inline static int32_t get_offset_of__originalInteractable_20() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ____originalInteractable_20)); }
	inline bool get__originalInteractable_20() const { return ____originalInteractable_20; }
	inline bool* get_address_of__originalInteractable_20() { return &____originalInteractable_20; }
	inline void set__originalInteractable_20(bool value)
	{
		____originalInteractable_20 = value;
	}

	inline static int32_t get_offset_of__originalBlocksRaycasts_21() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ____originalBlocksRaycasts_21)); }
	inline bool get__originalBlocksRaycasts_21() const { return ____originalBlocksRaycasts_21; }
	inline bool* get_address_of__originalBlocksRaycasts_21() { return &____originalBlocksRaycasts_21; }
	inline void set__originalBlocksRaycasts_21(bool value)
	{
		____originalBlocksRaycasts_21 = value;
	}

	inline static int32_t get_offset_of__originalIgnoreParentGroup_22() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetProperties_t3956917360, ____originalIgnoreParentGroup_22)); }
	inline bool get__originalIgnoreParentGroup_22() const { return ____originalIgnoreParentGroup_22; }
	inline bool* get_address_of__originalIgnoreParentGroup_22() { return &____originalIgnoreParentGroup_22; }
	inline void set__originalIgnoreParentGroup_22(bool value)
	{
		____originalIgnoreParentGroup_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
