﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor
struct uGuiInputFieldSetSelectionColor_t3282464375;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::.ctor()
extern "C"  void uGuiInputFieldSetSelectionColor__ctor_m3844822975 (uGuiInputFieldSetSelectionColor_t3282464375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::Reset()
extern "C"  void uGuiInputFieldSetSelectionColor_Reset_m3687893812 (uGuiInputFieldSetSelectionColor_t3282464375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::OnEnter()
extern "C"  void uGuiInputFieldSetSelectionColor_OnEnter_m3154026002 (uGuiInputFieldSetSelectionColor_t3282464375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::OnUpdate()
extern "C"  void uGuiInputFieldSetSelectionColor_OnUpdate_m3514720847 (uGuiInputFieldSetSelectionColor_t3282464375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::DoSetValue()
extern "C"  void uGuiInputFieldSetSelectionColor_DoSetValue_m3322193809 (uGuiInputFieldSetSelectionColor_t3282464375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::OnExit()
extern "C"  void uGuiInputFieldSetSelectionColor_OnExit_m1513330250 (uGuiInputFieldSetSelectionColor_t3282464375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
