﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLayoutElementMinSize
struct DOTweenLayoutElementMinSize_t943005423;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementMinSize::.ctor()
extern "C"  void DOTweenLayoutElementMinSize__ctor_m3264174471 (DOTweenLayoutElementMinSize_t943005423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementMinSize::Reset()
extern "C"  void DOTweenLayoutElementMinSize_Reset_m3983332268 (DOTweenLayoutElementMinSize_t943005423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementMinSize::OnEnter()
extern "C"  void DOTweenLayoutElementMinSize_OnEnter_m561647690 (DOTweenLayoutElementMinSize_t943005423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementMinSize::<OnEnter>m__42()
extern "C"  void DOTweenLayoutElementMinSize_U3COnEnterU3Em__42_m2500337527 (DOTweenLayoutElementMinSize_t943005423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementMinSize::<OnEnter>m__43()
extern "C"  void DOTweenLayoutElementMinSize_U3COnEnterU3Em__43_m2500337560 (DOTweenLayoutElementMinSize_t943005423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
