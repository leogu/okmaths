﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_t3299805272;

#include "codegen/il2cpp-codegen.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass30_0__ctor_m105447435 (U3CU3Ec__DisplayClass30_0_t3299805272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::<DOValue>b__0()
extern "C"  float U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__0_m955469785 (U3CU3Ec__DisplayClass30_0_t3299805272 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::<DOValue>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__1_m1756672311 (U3CU3Ec__DisplayClass30_0_t3299805272 * __this, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
