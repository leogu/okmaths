﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter
struct uGuiCanvasEnableRaycastFilter_t2839241966;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::.ctor()
extern "C"  void uGuiCanvasEnableRaycastFilter__ctor_m2598485232 (uGuiCanvasEnableRaycastFilter_t2839241966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::Reset()
extern "C"  void uGuiCanvasEnableRaycastFilter_Reset_m4176297443 (uGuiCanvasEnableRaycastFilter_t2839241966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::OnEnter()
extern "C"  void uGuiCanvasEnableRaycastFilter_OnEnter_m411884947 (uGuiCanvasEnableRaycastFilter_t2839241966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::OnUpdate()
extern "C"  void uGuiCanvasEnableRaycastFilter_OnUpdate_m2190375022 (uGuiCanvasEnableRaycastFilter_t2839241966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::DoAction()
extern "C"  void uGuiCanvasEnableRaycastFilter_DoAction_m374048123 (uGuiCanvasEnableRaycastFilter_t2839241966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::OnExit()
extern "C"  void uGuiCanvasEnableRaycastFilter_OnExit_m3903404067 (uGuiCanvasEnableRaycastFilter_t2839241966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
