﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition
struct RectTransformSetLocalPosition_t1471072400;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::.ctor()
extern "C"  void RectTransformSetLocalPosition__ctor_m3332863684 (RectTransformSetLocalPosition_t1471072400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::Reset()
extern "C"  void RectTransformSetLocalPosition_Reset_m2309329233 (RectTransformSetLocalPosition_t1471072400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::OnEnter()
extern "C"  void RectTransformSetLocalPosition_OnEnter_m2456651793 (RectTransformSetLocalPosition_t1471072400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::OnActionUpdate()
extern "C"  void RectTransformSetLocalPosition_OnActionUpdate_m3681771312 (RectTransformSetLocalPosition_t1471072400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalPosition::DoSetValues()
extern "C"  void RectTransformSetLocalPosition_DoSetValues_m2956246249 (RectTransformSetLocalPosition_t1471072400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
