﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetPlayerPing
struct NetworkGetPlayerPing_t1161235545;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::.ctor()
extern "C"  void NetworkGetPlayerPing__ctor_m4266626757 (NetworkGetPlayerPing_t1161235545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::Reset()
extern "C"  void NetworkGetPlayerPing_Reset_m3194575006 (NetworkGetPlayerPing_t1161235545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::OnEnter()
extern "C"  void NetworkGetPlayerPing_OnEnter_m151658276 (NetworkGetPlayerPing_t1161235545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::OnUpdate()
extern "C"  void NetworkGetPlayerPing_OnUpdate_m2507213609 (NetworkGetPlayerPing_t1161235545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetPlayerPing::GetAveragePing()
extern "C"  void NetworkGetPlayerPing_GetAveragePing_m2511317434 (NetworkGetPlayerPing_t1161235545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
