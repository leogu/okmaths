﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction3696775921.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection
struct  uGuiScrollbarSetDirection_t4203356181  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// UnityEngine.UI.Scrollbar/Direction HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::direction
	int32_t ___direction_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Scrollbar HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::_scrollbar
	Scrollbar_t3248359358 * ____scrollbar_14;
	// UnityEngine.UI.Scrollbar/Direction HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::_originalValue
	int32_t ____originalValue_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetDirection_t4203356181, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_direction_12() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetDirection_t4203356181, ___direction_12)); }
	inline int32_t get_direction_12() const { return ___direction_12; }
	inline int32_t* get_address_of_direction_12() { return &___direction_12; }
	inline void set_direction_12(int32_t value)
	{
		___direction_12 = value;
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetDirection_t4203356181, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__scrollbar_14() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetDirection_t4203356181, ____scrollbar_14)); }
	inline Scrollbar_t3248359358 * get__scrollbar_14() const { return ____scrollbar_14; }
	inline Scrollbar_t3248359358 ** get_address_of__scrollbar_14() { return &____scrollbar_14; }
	inline void set__scrollbar_14(Scrollbar_t3248359358 * value)
	{
		____scrollbar_14 = value;
		Il2CppCodeGenWriteBarrier(&____scrollbar_14, value);
	}

	inline static int32_t get_offset_of__originalValue_15() { return static_cast<int32_t>(offsetof(uGuiScrollbarSetDirection_t4203356181, ____originalValue_15)); }
	inline int32_t get__originalValue_15() const { return ____originalValue_15; }
	inline int32_t* get_address_of__originalValue_15() { return &____originalValue_15; }
	inline void set__originalValue_15(int32_t value)
	{
		____originalValue_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
