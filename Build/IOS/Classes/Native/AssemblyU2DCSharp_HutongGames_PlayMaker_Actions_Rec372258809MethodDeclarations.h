﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint
struct RectTransformPixelAdjustPoint_t372258809;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::.ctor()
extern "C"  void RectTransformPixelAdjustPoint__ctor_m74292675 (RectTransformPixelAdjustPoint_t372258809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::Reset()
extern "C"  void RectTransformPixelAdjustPoint_Reset_m2526210350 (RectTransformPixelAdjustPoint_t372258809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::OnEnter()
extern "C"  void RectTransformPixelAdjustPoint_OnEnter_m4015706048 (RectTransformPixelAdjustPoint_t372258809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::OnActionUpdate()
extern "C"  void RectTransformPixelAdjustPoint_OnActionUpdate_m1578621289 (RectTransformPixelAdjustPoint_t372258809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::DoAction()
extern "C"  void RectTransformPixelAdjustPoint_DoAction_m926505782 (RectTransformPixelAdjustPoint_t372258809 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
