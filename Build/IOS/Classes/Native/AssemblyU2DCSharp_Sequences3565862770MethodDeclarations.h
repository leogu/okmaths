﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Sequences
struct Sequences_t3565862770;

#include "codegen/il2cpp-codegen.h"

// System.Void Sequences::.ctor()
extern "C"  void Sequences__ctor_m3656502167 (Sequences_t3565862770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sequences::Start()
extern "C"  void Sequences_Start_m2369417315 (Sequences_t3565862770 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
