﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetTime
struct NetworkGetTime_t343619277;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetTime::.ctor()
extern "C"  void NetworkGetTime__ctor_m3359175327 (NetworkGetTime_t343619277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetTime::Reset()
extern "C"  void NetworkGetTime_Reset_m1639654846 (NetworkGetTime_t343619277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetTime::OnEnter()
extern "C"  void NetworkGetTime_OnEnter_m3293517960 (NetworkGetTime_t343619277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
