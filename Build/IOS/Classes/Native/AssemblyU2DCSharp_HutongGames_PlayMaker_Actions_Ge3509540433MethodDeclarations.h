﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextOverlapArea2d
struct GetNextOverlapArea2d_t3509540433;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::.ctor()
extern "C"  void GetNextOverlapArea2d__ctor_m1592259725 (GetNextOverlapArea2d_t3509540433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::Reset()
extern "C"  void GetNextOverlapArea2d_Reset_m2425867542 (GetNextOverlapArea2d_t3509540433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::OnEnter()
extern "C"  void GetNextOverlapArea2d_OnEnter_m1817671388 (GetNextOverlapArea2d_t3509540433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::DoGetNextCollider()
extern "C"  void GetNextOverlapArea2d_DoGetNextCollider_m2685765805 (GetNextOverlapArea2d_t3509540433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapArea2d::GetOverlapAreaAll()
extern "C"  Collider2DU5BU5D_t3535523695* GetNextOverlapArea2d_GetOverlapAreaAll_m729978844 (GetNextOverlapArea2d_t3509540433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
