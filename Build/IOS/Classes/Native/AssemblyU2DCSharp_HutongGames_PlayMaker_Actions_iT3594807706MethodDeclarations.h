﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenShakeRotation
struct iTweenShakeRotation_t3594807706;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::.ctor()
extern "C"  void iTweenShakeRotation__ctor_m708705028 (iTweenShakeRotation_t3594807706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::Reset()
extern "C"  void iTweenShakeRotation_Reset_m760324207 (iTweenShakeRotation_t3594807706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::OnEnter()
extern "C"  void iTweenShakeRotation_OnEnter_m2476981007 (iTweenShakeRotation_t3594807706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::OnExit()
extern "C"  void iTweenShakeRotation_OnExit_m32326951 (iTweenShakeRotation_t3594807706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeRotation::DoiTween()
extern "C"  void iTweenShakeRotation_DoiTween_m1906660001 (iTweenShakeRotation_t3594807706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
