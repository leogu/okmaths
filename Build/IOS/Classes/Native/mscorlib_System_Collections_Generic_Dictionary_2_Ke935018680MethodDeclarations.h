﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1844499377MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2755311687(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t935018680 *, Dictionary_2_t2746488205 *, const MethodInfo*))KeyCollection__ctor_m331708235_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3799271973(__this, ___item0, method) ((  void (*) (KeyCollection_t935018680 *, Fsm_t917886356 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m453544545_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3166225514(__this, method) ((  void (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1228884376_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1965038123(__this, ___item0, method) ((  bool (*) (KeyCollection_t935018680 *, Fsm_t917886356 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m877625383_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2129927430(__this, ___item0, method) ((  bool (*) (KeyCollection_t935018680 *, Fsm_t917886356 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2202014108_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m848448334(__this, method) ((  Il2CppObject* (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3038075768_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m1356018248(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t935018680 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1903150270_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3818290685(__this, method) ((  Il2CppObject * (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2155406921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m131930332(__this, method) ((  bool (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m353679046_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2954525416(__this, method) ((  bool (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3434268198_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3013280190(__this, method) ((  Il2CppObject * (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m915026728_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m4026972370(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t935018680 *, FsmU5BU5D_t2705153437*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4264182144_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::GetEnumerator()
#define KeyCollection_GetEnumerator_m1091777215(__this, method) ((  Enumerator_t1141024347  (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_GetEnumerator_m3783561107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Count()
#define KeyCollection_get_Count_m516008452(__this, method) ((  int32_t (*) (KeyCollection_t935018680 *, const MethodInfo*))KeyCollection_get_Count_m1331222946_gshared)(__this, method)
