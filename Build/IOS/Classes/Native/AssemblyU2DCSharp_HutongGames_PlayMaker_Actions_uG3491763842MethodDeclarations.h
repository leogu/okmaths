﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiGetColorBlock
struct uGuiGetColorBlock_t3491763842;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiGetColorBlock::.ctor()
extern "C"  void uGuiGetColorBlock__ctor_m3853259292 (uGuiGetColorBlock_t3491763842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetColorBlock::Reset()
extern "C"  void uGuiGetColorBlock_Reset_m2631361015 (uGuiGetColorBlock_t3491763842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetColorBlock::OnEnter()
extern "C"  void uGuiGetColorBlock_OnEnter_m2301427431 (uGuiGetColorBlock_t3491763842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetColorBlock::OnUpdate()
extern "C"  void uGuiGetColorBlock_OnUpdate_m1144411482 (uGuiGetColorBlock_t3491763842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetColorBlock::DoGetValue()
extern "C"  void uGuiGetColorBlock_DoGetValue_m723659010 (uGuiGetColorBlock_t3491763842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
