﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Subtract
struct Vector3Subtract_t2831225480;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::.ctor()
extern "C"  void Vector3Subtract__ctor_m1658974946 (Vector3Subtract_t2831225480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::Reset()
extern "C"  void Vector3Subtract_Reset_m2562378653 (Vector3Subtract_t2831225480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::OnEnter()
extern "C"  void Vector3Subtract_OnEnter_m840120445 (Vector3Subtract_t2831225480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Subtract::OnUpdate()
extern "C"  void Vector3Subtract_OnUpdate_m843446260 (Vector3Subtract_t2831225480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
