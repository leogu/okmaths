﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListCount
struct ArrayListCount_t3103064790;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListCount::.ctor()
extern "C"  void ArrayListCount__ctor_m174227296 (ArrayListCount_t3103064790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCount::Reset()
extern "C"  void ArrayListCount_Reset_m2743983591 (ArrayListCount_t3103064790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCount::OnEnter()
extern "C"  void ArrayListCount_OnEnter_m3357849127 (ArrayListCount_t3103064790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCount::getArrayListCount()
extern "C"  void ArrayListCount_getArrayListCount_m280972484 (ArrayListCount_t3103064790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
