﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3018380185.h"
#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m425544804_gshared (InternalEnumerator_1_t3018380185 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m425544804(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3018380185 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m425544804_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2905966948_gshared (InternalEnumerator_1_t3018380185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2905966948(__this, method) ((  void (*) (InternalEnumerator_1_t3018380185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2905966948_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690032154_gshared (InternalEnumerator_1_t3018380185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690032154(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3018380185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690032154_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2394538639_gshared (InternalEnumerator_1_t3018380185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2394538639(__this, method) ((  void (*) (InternalEnumerator_1_t3018380185 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2394538639_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3679920460_gshared (InternalEnumerator_1_t3018380185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3679920460(__this, method) ((  bool (*) (InternalEnumerator_1_t3018380185 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3679920460_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.ParamDataType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3625838387_gshared (InternalEnumerator_1_t3018380185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3625838387(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3018380185 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3625838387_gshared)(__this, method)
