﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiButtonOnClickEvent
struct uGuiButtonOnClickEvent_t925616161;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiButtonOnClickEvent::.ctor()
extern "C"  void uGuiButtonOnClickEvent__ctor_m2046414601 (uGuiButtonOnClickEvent_t925616161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiButtonOnClickEvent::Reset()
extern "C"  void uGuiButtonOnClickEvent_Reset_m605962930 (uGuiButtonOnClickEvent_t925616161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiButtonOnClickEvent::OnEnter()
extern "C"  void uGuiButtonOnClickEvent_OnEnter_m2935397536 (uGuiButtonOnClickEvent_t925616161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiButtonOnClickEvent::OnExit()
extern "C"  void uGuiButtonOnClickEvent_OnExit_m2911759148 (uGuiButtonOnClickEvent_t925616161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiButtonOnClickEvent::DoOnClick()
extern "C"  void uGuiButtonOnClickEvent_DoOnClick_m795715293 (uGuiButtonOnClickEvent_t925616161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
