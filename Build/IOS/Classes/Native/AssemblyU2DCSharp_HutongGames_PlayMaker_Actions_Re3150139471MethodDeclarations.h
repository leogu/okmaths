﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RewindAnimation
struct RewindAnimation_t3150139471;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RewindAnimation::.ctor()
extern "C"  void RewindAnimation__ctor_m2429130319 (RewindAnimation_t3150139471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RewindAnimation::Reset()
extern "C"  void RewindAnimation_Reset_m435432932 (RewindAnimation_t3150139471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RewindAnimation::OnEnter()
extern "C"  void RewindAnimation_OnEnter_m3549698554 (RewindAnimation_t3150139471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RewindAnimation::DoRewindAnimation()
extern "C"  void RewindAnimation_DoRewindAnimation_m1834574813 (RewindAnimation_t3150139471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
