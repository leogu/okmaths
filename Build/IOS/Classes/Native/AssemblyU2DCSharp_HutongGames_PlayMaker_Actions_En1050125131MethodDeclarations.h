﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnumSwitch
struct EnumSwitch_t1050125131;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::.ctor()
extern "C"  void EnumSwitch__ctor_m758225447 (EnumSwitch_t1050125131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::Reset()
extern "C"  void EnumSwitch_Reset_m573447408 (EnumSwitch_t1050125131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::OnEnter()
extern "C"  void EnumSwitch_OnEnter_m1386614726 (EnumSwitch_t1050125131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::OnUpdate()
extern "C"  void EnumSwitch_OnUpdate_m1861903575 (EnumSwitch_t1050125131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnumSwitch::DoEnumSwitch()
extern "C"  void EnumSwitch_DoEnumSwitch_m3686221633 (EnumSwitch_t1050125131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
