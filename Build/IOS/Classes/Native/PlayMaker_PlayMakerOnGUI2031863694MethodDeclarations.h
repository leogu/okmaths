﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerOnGUI
struct PlayMakerOnGUI_t2031863694;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerOnGUI::Start()
extern "C"  void PlayMakerOnGUI_Start_m3270530941 (PlayMakerOnGUI_t2031863694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::OnGUI()
extern "C"  void PlayMakerOnGUI_OnGUI_m2763967087 (PlayMakerOnGUI_t2031863694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::DoEditGUI()
extern "C"  void PlayMakerOnGUI_DoEditGUI_m2385310713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::.ctor()
extern "C"  void PlayMakerOnGUI__ctor_m3852551493 (PlayMakerOnGUI_t2031863694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
