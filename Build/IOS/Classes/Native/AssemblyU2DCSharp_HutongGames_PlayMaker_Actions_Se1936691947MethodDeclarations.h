﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetIsKinematic
struct SetIsKinematic_t1936691947;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::.ctor()
extern "C"  void SetIsKinematic__ctor_m3441299913 (SetIsKinematic_t1936691947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::Reset()
extern "C"  void SetIsKinematic_Reset_m1758087632 (SetIsKinematic_t1936691947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::OnEnter()
extern "C"  void SetIsKinematic_OnEnter_m4224249698 (SetIsKinematic_t1936691947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic::DoSetIsKinematic()
extern "C"  void SetIsKinematic_DoSetIsKinematic_m1396338113 (SetIsKinematic_t1936691947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
