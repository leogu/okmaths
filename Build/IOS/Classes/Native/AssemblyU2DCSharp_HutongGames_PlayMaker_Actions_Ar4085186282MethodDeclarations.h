﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListTakeSnapShot
struct ArrayListTakeSnapShot_t4085186282;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListTakeSnapShot::.ctor()
extern "C"  void ArrayListTakeSnapShot__ctor_m4273915180 (ArrayListTakeSnapShot_t4085186282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListTakeSnapShot::Reset()
extern "C"  void ArrayListTakeSnapShot_Reset_m4136433767 (ArrayListTakeSnapShot_t4085186282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListTakeSnapShot::OnEnter()
extern "C"  void ArrayListTakeSnapShot_OnEnter_m2256810063 (ArrayListTakeSnapShot_t4085186282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListTakeSnapShot::DoArrayListTakeSnapShot()
extern "C"  void ArrayListTakeSnapShot_DoArrayListTakeSnapShot_m687455449 (ArrayListTakeSnapShot_t4085186282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
