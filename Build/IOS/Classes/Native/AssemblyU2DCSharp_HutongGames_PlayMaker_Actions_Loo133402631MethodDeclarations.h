﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LookAt2dGameObject
struct LookAt2dGameObject_t133402631;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LookAt2dGameObject::.ctor()
extern "C"  void LookAt2dGameObject__ctor_m1003768973 (LookAt2dGameObject_t133402631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2dGameObject::Reset()
extern "C"  void LookAt2dGameObject_Reset_m1834086124 (LookAt2dGameObject_t133402631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2dGameObject::OnEnter()
extern "C"  void LookAt2dGameObject_OnEnter_m123415278 (LookAt2dGameObject_t133402631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2dGameObject::OnUpdate()
extern "C"  void LookAt2dGameObject_OnUpdate_m3626585593 (LookAt2dGameObject_t133402631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt2dGameObject::DoLookAt()
extern "C"  void LookAt2dGameObject_DoLookAt_m2909562132 (LookAt2dGameObject_t133402631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
