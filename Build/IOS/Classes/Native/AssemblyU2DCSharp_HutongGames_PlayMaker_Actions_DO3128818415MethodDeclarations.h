﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraShakeRotation
struct DOTweenCameraShakeRotation_t3128818415;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakeRotation::.ctor()
extern "C"  void DOTweenCameraShakeRotation__ctor_m4011709885 (DOTweenCameraShakeRotation_t3128818415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakeRotation::Reset()
extern "C"  void DOTweenCameraShakeRotation_Reset_m2289741468 (DOTweenCameraShakeRotation_t3128818415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakeRotation::OnEnter()
extern "C"  void DOTweenCameraShakeRotation_OnEnter_m3278727206 (DOTweenCameraShakeRotation_t3128818415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakeRotation::<OnEnter>m__34()
extern "C"  void DOTweenCameraShakeRotation_U3COnEnterU3Em__34_m183353954 (DOTweenCameraShakeRotation_t3128818415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakeRotation::<OnEnter>m__35()
extern "C"  void DOTweenCameraShakeRotation_U3COnEnterU3Em__35_m42191453 (DOTweenCameraShakeRotation_t3128818415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
