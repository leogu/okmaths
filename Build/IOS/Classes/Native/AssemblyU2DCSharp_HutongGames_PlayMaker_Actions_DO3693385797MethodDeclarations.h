﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenImageBlendableColor
struct DOTweenImageBlendableColor_t3693385797;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenImageBlendableColor::.ctor()
extern "C"  void DOTweenImageBlendableColor__ctor_m961967737 (DOTweenImageBlendableColor_t3693385797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageBlendableColor::Reset()
extern "C"  void DOTweenImageBlendableColor_Reset_m3570994250 (DOTweenImageBlendableColor_t3693385797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageBlendableColor::OnEnter()
extern "C"  void DOTweenImageBlendableColor_OnEnter_m1287319760 (DOTweenImageBlendableColor_t3693385797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageBlendableColor::<OnEnter>m__38()
extern "C"  void DOTweenImageBlendableColor_U3COnEnterU3Em__38_m3183005496 (DOTweenImageBlendableColor_t3693385797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageBlendableColor::<OnEnter>m__39()
extern "C"  void DOTweenImageBlendableColor_U3COnEnterU3Em__39_m3324167997 (DOTweenImageBlendableColor_t3693385797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
