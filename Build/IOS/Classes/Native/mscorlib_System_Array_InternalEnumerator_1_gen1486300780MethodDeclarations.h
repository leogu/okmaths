﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1486300780.h"
#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions_627548518.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3796912397_gshared (InternalEnumerator_1_t1486300780 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3796912397(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1486300780 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3796912397_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m599967529_gshared (InternalEnumerator_1_t1486300780 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m599967529(__this, method) ((  void (*) (InternalEnumerator_1_t1486300780 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m599967529_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m170705249_gshared (InternalEnumerator_1_t1486300780 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m170705249(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1486300780 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m170705249_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m43735078_gshared (InternalEnumerator_1_t1486300780 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m43735078(__this, method) ((  void (*) (InternalEnumerator_1_t1486300780 *, const MethodInfo*))InternalEnumerator_1_Dispose_m43735078_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1705990377_gshared (InternalEnumerator_1_t1486300780 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1705990377(__this, method) ((  bool (*) (InternalEnumerator_1_t1486300780 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1705990377_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.Extensions.TextureExtensions/Point>::get_Current()
extern "C"  Point_t627548518  InternalEnumerator_1_get_Current_m1884395932_gshared (InternalEnumerator_1_t1486300780 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1884395932(__this, method) ((  Point_t627548518  (*) (InternalEnumerator_1_t1486300780 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1884395932_gshared)(__this, method)
