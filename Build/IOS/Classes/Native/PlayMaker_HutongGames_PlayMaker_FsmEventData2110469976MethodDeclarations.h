﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t2110469976;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData2110469976.h"

// System.Void HutongGames.PlayMaker.FsmEventData::.ctor()
extern "C"  void FsmEventData__ctor_m731103841 (FsmEventData_t2110469976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventData::.ctor(HutongGames.PlayMaker.FsmEventData)
extern "C"  void FsmEventData__ctor_m244184539 (FsmEventData_t2110469976 * __this, FsmEventData_t2110469976 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventData::DebugLog()
extern "C"  void FsmEventData_DebugLog_m728056590 (FsmEventData_t2110469976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
