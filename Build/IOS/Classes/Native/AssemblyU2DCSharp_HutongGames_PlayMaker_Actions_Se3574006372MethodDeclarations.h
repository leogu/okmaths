﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector2Value
struct SetVector2Value_t3574006372;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::.ctor()
extern "C"  void SetVector2Value__ctor_m2527795698 (SetVector2Value_t3574006372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::Reset()
extern "C"  void SetVector2Value_Reset_m1243324965 (SetVector2Value_t3574006372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::OnEnter()
extern "C"  void SetVector2Value_OnEnter_m1420088781 (SetVector2Value_t3574006372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2Value::OnUpdate()
extern "C"  void SetVector2Value_OnUpdate_m2010136180 (SetVector2Value_t3574006372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
