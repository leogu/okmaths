﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan2FromVector3
struct GetAtan2FromVector3_t2478077212;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::.ctor()
extern "C"  void GetAtan2FromVector3__ctor_m2286362086 (GetAtan2FromVector3_t2478077212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::Reset()
extern "C"  void GetAtan2FromVector3_Reset_m3103119801 (GetAtan2FromVector3_t2478077212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::OnEnter()
extern "C"  void GetAtan2FromVector3_OnEnter_m2109655041 (GetAtan2FromVector3_t2478077212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::OnUpdate()
extern "C"  void GetAtan2FromVector3_OnUpdate_m4290272224 (GetAtan2FromVector3_t2478077212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector3::DoATan()
extern "C"  void GetAtan2FromVector3_DoATan_m1872809813 (GetAtan2FromVector3_t2478077212 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
