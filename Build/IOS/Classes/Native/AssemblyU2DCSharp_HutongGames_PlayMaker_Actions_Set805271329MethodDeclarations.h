﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVelocity2d
struct SetVelocity2d_t805271329;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::.ctor()
extern "C"  void SetVelocity2d__ctor_m3426392193 (SetVelocity2d_t805271329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::Reset()
extern "C"  void SetVelocity2d_Reset_m1395771838 (SetVelocity2d_t805271329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::Awake()
extern "C"  void SetVelocity2d_Awake_m1482589874 (SetVelocity2d_t805271329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::OnEnter()
extern "C"  void SetVelocity2d_OnEnter_m165938492 (SetVelocity2d_t805271329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::OnFixedUpdate()
extern "C"  void SetVelocity2d_OnFixedUpdate_m2903625943 (SetVelocity2d_t805271329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity2d::DoSetVelocity()
extern "C"  void SetVelocity2d_DoSetVelocity_m3356327167 (SetVelocity2d_t805271329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
