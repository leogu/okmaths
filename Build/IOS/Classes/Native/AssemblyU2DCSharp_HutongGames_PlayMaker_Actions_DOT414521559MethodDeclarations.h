﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteAll
struct DOTweenControlMethodsCompleteAll_t414521559;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteAll::.ctor()
extern "C"  void DOTweenControlMethodsCompleteAll__ctor_m3974177459 (DOTweenControlMethodsCompleteAll_t414521559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteAll::Reset()
extern "C"  void DOTweenControlMethodsCompleteAll_Reset_m3141919812 (DOTweenControlMethodsCompleteAll_t414521559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsCompleteAll::OnEnter()
extern "C"  void DOTweenControlMethodsCompleteAll_OnEnter_m1439797330 (DOTweenControlMethodsCompleteAll_t414521559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
