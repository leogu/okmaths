﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFogColor
struct SetFogColor_t2065187483;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFogColor::.ctor()
extern "C"  void SetFogColor__ctor_m1872018307 (SetFogColor_t2065187483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::Reset()
extern "C"  void SetFogColor_Reset_m2615270992 (SetFogColor_t2065187483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::OnEnter()
extern "C"  void SetFogColor_OnEnter_m3189594438 (SetFogColor_t2065187483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::OnUpdate()
extern "C"  void SetFogColor_OnUpdate_m4048437547 (SetFogColor_t2065187483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFogColor::DoSetFogColor()
extern "C"  void SetFogColor_DoSetFogColor_m2686219629 (SetFogColor_t2065187483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
