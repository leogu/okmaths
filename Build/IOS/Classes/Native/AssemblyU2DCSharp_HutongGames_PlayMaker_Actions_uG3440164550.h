﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers
struct  uGuiSliderSetWholeNumbers_t3440164550  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::wholeNumbers
	FsmBool_t664485696 * ___wholeNumbers_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::_slider
	Slider_t297367283 * ____slider_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::_originalValue
	bool ____originalValue_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderSetWholeNumbers_t3440164550, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_wholeNumbers_12() { return static_cast<int32_t>(offsetof(uGuiSliderSetWholeNumbers_t3440164550, ___wholeNumbers_12)); }
	inline FsmBool_t664485696 * get_wholeNumbers_12() const { return ___wholeNumbers_12; }
	inline FsmBool_t664485696 ** get_address_of_wholeNumbers_12() { return &___wholeNumbers_12; }
	inline void set_wholeNumbers_12(FsmBool_t664485696 * value)
	{
		___wholeNumbers_12 = value;
		Il2CppCodeGenWriteBarrier(&___wholeNumbers_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiSliderSetWholeNumbers_t3440164550, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__slider_14() { return static_cast<int32_t>(offsetof(uGuiSliderSetWholeNumbers_t3440164550, ____slider_14)); }
	inline Slider_t297367283 * get__slider_14() const { return ____slider_14; }
	inline Slider_t297367283 ** get_address_of__slider_14() { return &____slider_14; }
	inline void set__slider_14(Slider_t297367283 * value)
	{
		____slider_14 = value;
		Il2CppCodeGenWriteBarrier(&____slider_14, value);
	}

	inline static int32_t get_offset_of__originalValue_15() { return static_cast<int32_t>(offsetof(uGuiSliderSetWholeNumbers_t3440164550, ____originalValue_15)); }
	inline bool get__originalValue_15() const { return ____originalValue_15; }
	inline bool* get_address_of__originalValue_15() { return &____originalValue_15; }
	inline void set__originalValue_15(bool value)
	{
		____originalValue_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
