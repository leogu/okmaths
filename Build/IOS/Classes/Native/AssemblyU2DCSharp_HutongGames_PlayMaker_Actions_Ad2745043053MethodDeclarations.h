﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddExplosionForce
struct AddExplosionForce_t2745043053;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::.ctor()
extern "C"  void AddExplosionForce__ctor_m2262765167 (AddExplosionForce_t2745043053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::Reset()
extern "C"  void AddExplosionForce_Reset_m2707944098 (AddExplosionForce_t2745043053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnPreprocess()
extern "C"  void AddExplosionForce_OnPreprocess_m2383898706 (AddExplosionForce_t2745043053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnEnter()
extern "C"  void AddExplosionForce_OnEnter_m3975789188 (AddExplosionForce_t2745043053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::OnFixedUpdate()
extern "C"  void AddExplosionForce_OnFixedUpdate_m2115395565 (AddExplosionForce_t2745043053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddExplosionForce::DoAddExplosionForce()
extern "C"  void AddExplosionForce_DoAddExplosionForce_m234920973 (AddExplosionForce_t2745043053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
