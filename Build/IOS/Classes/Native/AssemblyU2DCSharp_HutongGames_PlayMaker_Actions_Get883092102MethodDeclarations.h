﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetCollision2dInfo
struct GetCollision2dInfo_t883092102;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::.ctor()
extern "C"  void GetCollision2dInfo__ctor_m416121858 (GetCollision2dInfo_t883092102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::Reset()
extern "C"  void GetCollision2dInfo_Reset_m3918966219 (GetCollision2dInfo_t883092102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::StoreCollisionInfo()
extern "C"  void GetCollision2dInfo_StoreCollisionInfo_m3400782485 (GetCollision2dInfo_t883092102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollision2dInfo::OnEnter()
extern "C"  void GetCollision2dInfo_OnEnter_m2362289851 (GetCollision2dInfo_t883092102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
