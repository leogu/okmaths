﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>
struct DefaultComparer_t648898156;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void DefaultComparer__ctor_m3497693973_gshared (DefaultComparer_t648898156 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3497693973(__this, method) ((  void (*) (DefaultComparer_t648898156 *, const MethodInfo*))DefaultComparer__ctor_m3497693973_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3609182536_gshared (DefaultComparer_t648898156 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3609182536(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t648898156 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m3609182536_gshared)(__this, ___x0, ___y1, method)
