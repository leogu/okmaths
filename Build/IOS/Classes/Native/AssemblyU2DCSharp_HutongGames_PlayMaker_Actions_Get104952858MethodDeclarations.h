﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition
struct GetAnimatorIsLayerInTransition_t104952858;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::.ctor()
extern "C"  void GetAnimatorIsLayerInTransition__ctor_m2812157902 (GetAnimatorIsLayerInTransition_t104952858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::Reset()
extern "C"  void GetAnimatorIsLayerInTransition_Reset_m2012712799 (GetAnimatorIsLayerInTransition_t104952858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnEnter()
extern "C"  void GetAnimatorIsLayerInTransition_OnEnter_m2815211663 (GetAnimatorIsLayerInTransition_t104952858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::OnActionUpdate()
extern "C"  void GetAnimatorIsLayerInTransition_OnActionUpdate_m4200660258 (GetAnimatorIsLayerInTransition_t104952858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsLayerInTransition::DoCheckIsInTransition()
extern "C"  void GetAnimatorIsLayerInTransition_DoCheckIsInTransition_m202133965 (GetAnimatorIsLayerInTransition_t104952858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
