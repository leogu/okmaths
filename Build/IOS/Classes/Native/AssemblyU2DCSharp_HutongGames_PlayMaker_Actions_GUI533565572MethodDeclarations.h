﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIElementHitTest
struct GUIElementHitTest_t533565572;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::.ctor()
extern "C"  void GUIElementHitTest__ctor_m3340919012 (GUIElementHitTest_t533565572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::Reset()
extern "C"  void GUIElementHitTest_Reset_m589499641 (GUIElementHitTest_t533565572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::OnEnter()
extern "C"  void GUIElementHitTest_OnEnter_m3156806465 (GUIElementHitTest_t533565572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::OnUpdate()
extern "C"  void GUIElementHitTest_OnUpdate_m4089473986 (GUIElementHitTest_t533565572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIElementHitTest::DoHitTest()
extern "C"  void GUIElementHitTest_DoHitTest_m3937888910 (GUIElementHitTest_t533565572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
