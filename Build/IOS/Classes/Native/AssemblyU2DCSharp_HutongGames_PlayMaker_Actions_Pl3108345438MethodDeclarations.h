﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayRandomSound
struct PlayRandomSound_t3108345438;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::.ctor()
extern "C"  void PlayRandomSound__ctor_m1755549494 (PlayRandomSound_t3108345438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::Reset()
extern "C"  void PlayRandomSound_Reset_m3997575995 (PlayRandomSound_t3108345438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::OnEnter()
extern "C"  void PlayRandomSound_OnEnter_m3740274907 (PlayRandomSound_t3108345438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomSound::DoPlayRandomClip()
extern "C"  void PlayRandomSound_DoPlayRandomClip_m2644767466 (PlayRandomSound_t3108345438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
