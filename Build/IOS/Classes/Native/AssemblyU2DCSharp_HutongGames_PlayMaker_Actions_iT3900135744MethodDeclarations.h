﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenFsmAction
struct iTweenFsmAction_t3900135744;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault2023674184.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::.ctor()
extern "C"  void iTweenFsmAction__ctor_m1296456232 (iTweenFsmAction_t3900135744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::Reset()
extern "C"  void iTweenFsmAction_Reset_m2673646677 (iTweenFsmAction_t3900135744 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::OnEnteriTween(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  void iTweenFsmAction_OnEnteriTween_m4137841385 (iTweenFsmAction_t3900135744 * __this, FsmOwnerDefault_t2023674184 * ___anOwner0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::IsLoop(System.Boolean)
extern "C"  void iTweenFsmAction_IsLoop_m2229394385 (iTweenFsmAction_t3900135744 * __this, bool ___aValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenFsmAction::OnExitiTween(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  void iTweenFsmAction_OnExitiTween_m779453157 (iTweenFsmAction_t3900135744 * __this, FsmOwnerDefault_t2023674184 * ___anOwner0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
