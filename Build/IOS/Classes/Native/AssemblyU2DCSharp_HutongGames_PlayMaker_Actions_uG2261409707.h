﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn
struct  uGuiToggleGetIsOn_t2261409707  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::value
	FsmBool_t664485696 * ___value_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::isOnEvent
	FsmEvent_t1258573736 * ___isOnEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::isOffEvent
	FsmEvent_t1258573736 * ___isOffEvent_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::_toggle
	Toggle_t3976754468 * ____toggle_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiToggleGetIsOn_t2261409707, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiToggleGetIsOn_t2261409707, ___value_12)); }
	inline FsmBool_t664485696 * get_value_12() const { return ___value_12; }
	inline FsmBool_t664485696 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmBool_t664485696 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_isOnEvent_13() { return static_cast<int32_t>(offsetof(uGuiToggleGetIsOn_t2261409707, ___isOnEvent_13)); }
	inline FsmEvent_t1258573736 * get_isOnEvent_13() const { return ___isOnEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isOnEvent_13() { return &___isOnEvent_13; }
	inline void set_isOnEvent_13(FsmEvent_t1258573736 * value)
	{
		___isOnEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isOnEvent_13, value);
	}

	inline static int32_t get_offset_of_isOffEvent_14() { return static_cast<int32_t>(offsetof(uGuiToggleGetIsOn_t2261409707, ___isOffEvent_14)); }
	inline FsmEvent_t1258573736 * get_isOffEvent_14() const { return ___isOffEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isOffEvent_14() { return &___isOffEvent_14; }
	inline void set_isOffEvent_14(FsmEvent_t1258573736 * value)
	{
		___isOffEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isOffEvent_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(uGuiToggleGetIsOn_t2261409707, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of__toggle_16() { return static_cast<int32_t>(offsetof(uGuiToggleGetIsOn_t2261409707, ____toggle_16)); }
	inline Toggle_t3976754468 * get__toggle_16() const { return ____toggle_16; }
	inline Toggle_t3976754468 ** get_address_of__toggle_16() { return &____toggle_16; }
	inline void set__toggle_16(Toggle_t3976754468 * value)
	{
		____toggle_16 = value;
		Il2CppCodeGenWriteBarrier(&____toggle_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
