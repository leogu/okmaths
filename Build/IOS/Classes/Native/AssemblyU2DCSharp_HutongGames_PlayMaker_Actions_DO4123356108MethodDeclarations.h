﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformMoveY
struct DOTweenTransformMoveY_t4123356108;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveY::.ctor()
extern "C"  void DOTweenTransformMoveY__ctor_m1343068342 (DOTweenTransformMoveY_t4123356108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveY::Reset()
extern "C"  void DOTweenTransformMoveY_Reset_m1810560521 (DOTweenTransformMoveY_t4123356108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveY::OnEnter()
extern "C"  void DOTweenTransformMoveY_OnEnter_m3646497473 (DOTweenTransformMoveY_t4123356108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveY::<OnEnter>m__C8()
extern "C"  void DOTweenTransformMoveY_U3COnEnterU3Em__C8_m4252029705 (DOTweenTransformMoveY_t4123356108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveY::<OnEnter>m__C9()
extern "C"  void DOTweenTransformMoveY_U3COnEnterU3Em__C9_m4252029610 (DOTweenTransformMoveY_t4123356108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
