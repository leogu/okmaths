﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject
struct GetAnimatorBoneGameObject_t2114940722;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::.ctor()
extern "C"  void GetAnimatorBoneGameObject__ctor_m924168272 (GetAnimatorBoneGameObject_t2114940722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::Reset()
extern "C"  void GetAnimatorBoneGameObject_Reset_m4126616851 (GetAnimatorBoneGameObject_t2114940722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::OnEnter()
extern "C"  void GetAnimatorBoneGameObject_OnEnter_m31307051 (GetAnimatorBoneGameObject_t2114940722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBoneGameObject::GetBoneTransform()
extern "C"  void GetAnimatorBoneGameObject_GetBoneTransform_m783016568 (GetAnimatorBoneGameObject_t2114940722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
