﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomVector3
struct SelectRandomVector3_t3542404785;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::.ctor()
extern "C"  void SelectRandomVector3__ctor_m4054884471 (SelectRandomVector3_t3542404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::Reset()
extern "C"  void SelectRandomVector3_Reset_m1908910954 (SelectRandomVector3_t3542404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::OnEnter()
extern "C"  void SelectRandomVector3_OnEnter_m3006359676 (SelectRandomVector3_t3542404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomVector3::DoSelectRandomColor()
extern "C"  void SelectRandomVector3_DoSelectRandomColor_m904214824 (SelectRandomVector3_t3542404785 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
