﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyHashTable
struct DestroyHashTable_t3610008612;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyHashTable::.ctor()
extern "C"  void DestroyHashTable__ctor_m1269611570 (DestroyHashTable_t3610008612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyHashTable::Reset()
extern "C"  void DestroyHashTable_Reset_m1456524081 (DestroyHashTable_t3610008612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyHashTable::OnEnter()
extern "C"  void DestroyHashTable_OnEnter_m3076373793 (DestroyHashTable_t3610008612 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyHashTable::DoDestroyHashTable(UnityEngine.GameObject)
extern "C"  void DestroyHashTable_DoDestroyHashTable_m1720156525 (DestroyHashTable_t3610008612 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
