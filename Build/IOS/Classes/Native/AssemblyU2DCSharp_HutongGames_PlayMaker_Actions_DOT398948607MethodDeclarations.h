﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindAll
struct DOTweenControlMethodsRewindAll_t398948607;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindAll::.ctor()
extern "C"  void DOTweenControlMethodsRewindAll__ctor_m2340447689 (DOTweenControlMethodsRewindAll_t398948607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindAll::Reset()
extern "C"  void DOTweenControlMethodsRewindAll_Reset_m1548276936 (DOTweenControlMethodsRewindAll_t398948607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindAll::OnEnter()
extern "C"  void DOTweenControlMethodsRewindAll_OnEnter_m1434852266 (DOTweenControlMethodsRewindAll_t398948607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
