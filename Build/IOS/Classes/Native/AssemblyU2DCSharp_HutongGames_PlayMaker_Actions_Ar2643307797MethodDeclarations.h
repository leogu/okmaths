﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListCreate
struct ArrayListCreate_t2643307797;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListCreate::.ctor()
extern "C"  void ArrayListCreate__ctor_m2986270509 (ArrayListCreate_t2643307797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCreate::Reset()
extern "C"  void ArrayListCreate_Reset_m2624080626 (ArrayListCreate_t2643307797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCreate::OnEnter()
extern "C"  void ArrayListCreate_OnEnter_m1868743920 (ArrayListCreate_t2643307797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCreate::OnExit()
extern "C"  void ArrayListCreate_OnExit_m1664466300 (ArrayListCreate_t2643307797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCreate::DoAddPlayMakerArrayList()
extern "C"  void ArrayListCreate_DoAddPlayMakerArrayList_m564794510 (ArrayListCreate_t2643307797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
