﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAxisVector
struct GetAxisVector_t1054043902;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAxisVector::.ctor()
extern "C"  void GetAxisVector__ctor_m344998916 (GetAxisVector_t1054043902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxisVector::Reset()
extern "C"  void GetAxisVector_Reset_m2736110399 (GetAxisVector_t1054043902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxisVector::OnUpdate()
extern "C"  void GetAxisVector_OnUpdate_m2868449242 (GetAxisVector_t1054043902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
