﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiImageGetSprite
struct uGuiImageGetSprite_t3173654416;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiImageGetSprite::.ctor()
extern "C"  void uGuiImageGetSprite__ctor_m2908914788 (uGuiImageGetSprite_t3173654416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageGetSprite::Reset()
extern "C"  void uGuiImageGetSprite_Reset_m1831409621 (uGuiImageGetSprite_t3173654416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageGetSprite::OnEnter()
extern "C"  void uGuiImageGetSprite_OnEnter_m780717173 (uGuiImageGetSprite_t3173654416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiImageGetSprite::DoSetImageSourceValue()
extern "C"  void uGuiImageGetSprite_DoSetImageSourceValue_m1199453498 (uGuiImageGetSprite_t3173654416 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
