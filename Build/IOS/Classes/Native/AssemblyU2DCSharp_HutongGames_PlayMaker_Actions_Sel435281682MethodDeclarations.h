﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomGameObject
struct SelectRandomGameObject_t435281682;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::.ctor()
extern "C"  void SelectRandomGameObject__ctor_m2720516738 (SelectRandomGameObject_t435281682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::Reset()
extern "C"  void SelectRandomGameObject_Reset_m1651995323 (SelectRandomGameObject_t435281682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::OnEnter()
extern "C"  void SelectRandomGameObject_OnEnter_m3378101259 (SelectRandomGameObject_t435281682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomGameObject::DoSelectRandomGameObject()
extern "C"  void SelectRandomGameObject_DoSelectRandomGameObject_m1064615393 (SelectRandomGameObject_t435281682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
