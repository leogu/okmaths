﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Flare
struct Flare_t2826966314;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LightType2348405332.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Flare2826966314.h"

// System.Void UnityEngine.Light::set_type(UnityEngine.LightType)
extern "C"  void Light_set_type_m726202667 (Light_t494725636 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Light::get_color()
extern "C"  Color_t2020392075  Light_get_color_m2617173338 (Light_t494725636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C"  void Light_set_color_m3975955621 (Light_t494725636 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void Light_INTERNAL_get_color_m2896497767 (Light_t494725636 * __this, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void Light_INTERNAL_set_color_m2851094611 (Light_t494725636 * __this, Color_t2020392075 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_intensity()
extern "C"  float Light_get_intensity_m392824783 (Light_t494725636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C"  void Light_set_intensity_m1790590300 (Light_t494725636 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_shadowStrength()
extern "C"  float Light_get_shadowStrength_m1628863523 (Light_t494725636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_shadowStrength(System.Single)
extern "C"  void Light_set_shadowStrength_m677056102 (Light_t494725636 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_range(System.Single)
extern "C"  void Light_set_range_m2021777728 (Light_t494725636 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_spotAngle(System.Single)
extern "C"  void Light_set_spotAngle_m1540530102 (Light_t494725636 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
extern "C"  void Light_set_cookie_m511274426 (Light_t494725636 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_flare(UnityEngine.Flare)
extern "C"  void Light_set_flare_m439759877 (Light_t494725636 * __this, Flare_t2826966314 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
