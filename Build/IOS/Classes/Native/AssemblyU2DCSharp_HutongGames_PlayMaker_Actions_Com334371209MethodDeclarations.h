﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Comment
struct Comment_t334371209;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Comment::.ctor()
extern "C"  void Comment__ctor_m1425877717 (Comment_t334371209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Comment::Reset()
extern "C"  void Comment_Reset_m2107909954 (Comment_t334371209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Comment::OnEnter()
extern "C"  void Comment_OnEnter_m1044207976 (Comment_t334371209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
