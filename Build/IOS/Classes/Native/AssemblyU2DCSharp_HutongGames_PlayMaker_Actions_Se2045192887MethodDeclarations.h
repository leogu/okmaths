﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetBoolValue
struct SetBoolValue_t2045192887;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::.ctor()
extern "C"  void SetBoolValue__ctor_m3295671253 (SetBoolValue_t2045192887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::Reset()
extern "C"  void SetBoolValue_Reset_m3481002500 (SetBoolValue_t2045192887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::OnEnter()
extern "C"  void SetBoolValue_OnEnter_m2155653550 (SetBoolValue_t2045192887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBoolValue::OnUpdate()
extern "C"  void SetBoolValue_OnUpdate_m2281848201 (SetBoolValue_t2045192887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
