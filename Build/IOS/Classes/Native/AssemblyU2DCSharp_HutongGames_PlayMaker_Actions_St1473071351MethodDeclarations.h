﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringChanged
struct StringChanged_t1473071351;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringChanged::.ctor()
extern "C"  void StringChanged__ctor_m1498727045 (StringChanged_t1473071351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringChanged::Reset()
extern "C"  void StringChanged_Reset_m1370983984 (StringChanged_t1473071351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringChanged::OnEnter()
extern "C"  void StringChanged_OnEnter_m1387375426 (StringChanged_t1473071351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringChanged::OnUpdate()
extern "C"  void StringChanged_OnUpdate_m1985726241 (StringChanged_t1473071351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
