﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.InverseTransformPoint
struct InverseTransformPoint_t3837501350;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::.ctor()
extern "C"  void InverseTransformPoint__ctor_m4267754648 (InverseTransformPoint_t3837501350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::Reset()
extern "C"  void InverseTransformPoint_Reset_m1550607515 (InverseTransformPoint_t3837501350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::OnEnter()
extern "C"  void InverseTransformPoint_OnEnter_m3958713035 (InverseTransformPoint_t3837501350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::OnUpdate()
extern "C"  void InverseTransformPoint_OnUpdate_m3791727414 (InverseTransformPoint_t3837501350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformPoint::DoInverseTransformPoint()
extern "C"  void InverseTransformPoint_DoInverseTransformPoint_m537316137 (InverseTransformPoint_t3837501350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
