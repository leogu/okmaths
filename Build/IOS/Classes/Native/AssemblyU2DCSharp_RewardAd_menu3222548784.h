﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RewardAd_menu
struct  RewardAd_menu_t3222548784  : public MonoBehaviour_t1158329972
{
public:
	// System.String RewardAd_menu::zoneId
	String_t* ___zoneId_2;

public:
	inline static int32_t get_offset_of_zoneId_2() { return static_cast<int32_t>(offsetof(RewardAd_menu_t3222548784, ___zoneId_2)); }
	inline String_t* get_zoneId_2() const { return ___zoneId_2; }
	inline String_t** get_address_of_zoneId_2() { return &___zoneId_2; }
	inline void set_zoneId_2(String_t* value)
	{
		___zoneId_2 = value;
		Il2CppCodeGenWriteBarrier(&___zoneId_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
