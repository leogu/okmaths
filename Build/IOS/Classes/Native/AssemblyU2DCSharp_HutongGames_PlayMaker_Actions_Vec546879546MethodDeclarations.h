﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3AddXYZ
struct Vector3AddXYZ_t546879546;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3AddXYZ::.ctor()
extern "C"  void Vector3AddXYZ__ctor_m56278000 (Vector3AddXYZ_t546879546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3AddXYZ::Reset()
extern "C"  void Vector3AddXYZ_Reset_m2205271283 (Vector3AddXYZ_t546879546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3AddXYZ::OnEnter()
extern "C"  void Vector3AddXYZ_OnEnter_m4197077731 (Vector3AddXYZ_t546879546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3AddXYZ::OnUpdate()
extern "C"  void Vector3AddXYZ_OnUpdate_m1911475934 (Vector3AddXYZ_t546879546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3AddXYZ::DoVector3AddXYZ()
extern "C"  void Vector3AddXYZ_DoVector3AddXYZ_m2943547945 (Vector3AddXYZ_t546879546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
