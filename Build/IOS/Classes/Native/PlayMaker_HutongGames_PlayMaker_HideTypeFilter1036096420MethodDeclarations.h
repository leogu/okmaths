﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.HideTypeFilter
struct HideTypeFilter_t1036096420;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.HideTypeFilter::.ctor()
extern "C"  void HideTypeFilter__ctor_m598921205 (HideTypeFilter_t1036096420 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
