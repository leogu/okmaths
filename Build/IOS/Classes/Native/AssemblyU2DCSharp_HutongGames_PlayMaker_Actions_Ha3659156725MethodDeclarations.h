﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableActions
struct HashTableActions_t3659156725;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PlayMakerHashTableProxy
struct PlayMakerHashTableProxy_t3073922234;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PlayMakerHashTableProxy3073922234.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableActions::.ctor()
extern "C"  void HashTableActions__ctor_m2938182955 (HashTableActions_t3659156725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.HashTableActions::SetUpHashTableProxyPointer(UnityEngine.GameObject,System.String)
extern "C"  bool HashTableActions_SetUpHashTableProxyPointer_m4053896077 (HashTableActions_t3659156725 * __this, GameObject_t1756533147 * ___aProxyGO0, String_t* ___nameReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.HashTableActions::SetUpHashTableProxyPointer(PlayMakerHashTableProxy,System.String)
extern "C"  bool HashTableActions_SetUpHashTableProxyPointer_m1811059549 (HashTableActions_t3659156725 * __this, PlayMakerHashTableProxy_t3073922234 * ___aProxy0, String_t* ___nameReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.HashTableActions::isProxyValid()
extern "C"  bool HashTableActions_isProxyValid_m1263821639 (HashTableActions_t3659156725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
