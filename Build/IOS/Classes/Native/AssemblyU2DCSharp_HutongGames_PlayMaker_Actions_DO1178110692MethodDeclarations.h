﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenImageFillAmount
struct DOTweenImageFillAmount_t1178110692;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFillAmount::.ctor()
extern "C"  void DOTweenImageFillAmount__ctor_m2392387780 (DOTweenImageFillAmount_t1178110692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFillAmount::Reset()
extern "C"  void DOTweenImageFillAmount_Reset_m428669677 (DOTweenImageFillAmount_t1178110692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFillAmount::OnEnter()
extern "C"  void DOTweenImageFillAmount_OnEnter_m3903492557 (DOTweenImageFillAmount_t1178110692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFillAmount::<OnEnter>m__3E()
extern "C"  void DOTweenImageFillAmount_U3COnEnterU3Em__3E_m2599734324 (DOTweenImageFillAmount_t1178110692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageFillAmount::<OnEnter>m__3F()
extern "C"  void DOTweenImageFillAmount_U3COnEnterU3Em__3F_m132972995 (DOTweenImageFillAmount_t1178110692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
