﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>
struct DefaultComparer_t3781558084;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2448008397_gshared (DefaultComparer_t3781558084 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2448008397(__this, method) ((  void (*) (DefaultComparer_t3781558084 *, const MethodInfo*))DefaultComparer__ctor_m2448008397_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4097966966_gshared (DefaultComparer_t3781558084 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4097966966(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3781558084 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m4097966966_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<HutongGames.PlayMaker.ParamDataType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m726360510_gshared (DefaultComparer_t3781558084 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m726360510(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3781558084 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m726360510_gshared)(__this, ___x0, ___y1, method)
