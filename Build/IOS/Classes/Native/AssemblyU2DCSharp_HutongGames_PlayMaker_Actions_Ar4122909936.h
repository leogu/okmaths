﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerArrayListProxy
struct PlayMakerArrayListProxy_t4012441185;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1918293222.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListActions
struct  ArrayListActions_t4122909936  : public CollectionsActions_t1918293222
{
public:
	// PlayMakerArrayListProxy HutongGames.PlayMaker.Actions.ArrayListActions::proxy
	PlayMakerArrayListProxy_t4012441185 * ___proxy_11;

public:
	inline static int32_t get_offset_of_proxy_11() { return static_cast<int32_t>(offsetof(ArrayListActions_t4122909936, ___proxy_11)); }
	inline PlayMakerArrayListProxy_t4012441185 * get_proxy_11() const { return ___proxy_11; }
	inline PlayMakerArrayListProxy_t4012441185 ** get_address_of_proxy_11() { return &___proxy_11; }
	inline void set_proxy_11(PlayMakerArrayListProxy_t4012441185 * value)
	{
		___proxy_11 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
