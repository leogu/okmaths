﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkIsClient
struct  NetworkIsClient_t536994873  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkIsClient::isClient
	FsmBool_t664485696 * ___isClient_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkIsClient::isClientEvent
	FsmEvent_t1258573736 * ___isClientEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkIsClient::isNotClientEvent
	FsmEvent_t1258573736 * ___isNotClientEvent_13;

public:
	inline static int32_t get_offset_of_isClient_11() { return static_cast<int32_t>(offsetof(NetworkIsClient_t536994873, ___isClient_11)); }
	inline FsmBool_t664485696 * get_isClient_11() const { return ___isClient_11; }
	inline FsmBool_t664485696 ** get_address_of_isClient_11() { return &___isClient_11; }
	inline void set_isClient_11(FsmBool_t664485696 * value)
	{
		___isClient_11 = value;
		Il2CppCodeGenWriteBarrier(&___isClient_11, value);
	}

	inline static int32_t get_offset_of_isClientEvent_12() { return static_cast<int32_t>(offsetof(NetworkIsClient_t536994873, ___isClientEvent_12)); }
	inline FsmEvent_t1258573736 * get_isClientEvent_12() const { return ___isClientEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_isClientEvent_12() { return &___isClientEvent_12; }
	inline void set_isClientEvent_12(FsmEvent_t1258573736 * value)
	{
		___isClientEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___isClientEvent_12, value);
	}

	inline static int32_t get_offset_of_isNotClientEvent_13() { return static_cast<int32_t>(offsetof(NetworkIsClient_t536994873, ___isNotClientEvent_13)); }
	inline FsmEvent_t1258573736 * get_isNotClientEvent_13() const { return ___isNotClientEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotClientEvent_13() { return &___isNotClientEvent_13; }
	inline void set_isNotClientEvent_13(FsmEvent_t1258573736 * value)
	{
		___isNotClientEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isNotClientEvent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
