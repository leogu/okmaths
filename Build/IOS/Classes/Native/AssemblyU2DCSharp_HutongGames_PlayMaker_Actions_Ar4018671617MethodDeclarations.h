﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListExists
struct ArrayListExists_t4018671617;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListExists::.ctor()
extern "C"  void ArrayListExists__ctor_m68558621 (ArrayListExists_t4018671617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListExists::Reset()
extern "C"  void ArrayListExists_Reset_m619389850 (ArrayListExists_t4018671617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListExists::OnEnter()
extern "C"  void ArrayListExists_OnEnter_m459591024 (ArrayListExists_t4018671617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
