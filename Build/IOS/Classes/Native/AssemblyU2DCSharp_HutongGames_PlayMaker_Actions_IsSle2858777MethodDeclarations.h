﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsSleeping2d
struct IsSleeping2d_t2858777;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsSleeping2d::.ctor()
extern "C"  void IsSleeping2d__ctor_m771786777 (IsSleeping2d_t2858777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping2d::Reset()
extern "C"  void IsSleeping2d_Reset_m2489729826 (IsSleeping2d_t2858777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping2d::OnEnter()
extern "C"  void IsSleeping2d_OnEnter_m1248717928 (IsSleeping2d_t2858777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping2d::OnUpdate()
extern "C"  void IsSleeping2d_OnUpdate_m791126309 (IsSleeping2d_t2858777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping2d::DoIsSleeping()
extern "C"  void IsSleeping2d_DoIsSleeping_m2550083519 (IsSleeping2d_t2858777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
