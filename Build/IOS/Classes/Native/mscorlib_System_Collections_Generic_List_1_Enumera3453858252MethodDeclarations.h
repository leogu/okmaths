﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3418776289(__this, ___l0, method) ((  void (*) (Enumerator_t3453858252 *, List_1_t3919128578 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3582169521(__this, method) ((  void (*) (Enumerator_t3453858252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3710923001(__this, method) ((  Il2CppObject * (*) (Enumerator_t3453858252 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::Dispose()
#define Enumerator_Dispose_m2617172766(__this, method) ((  void (*) (Enumerator_t3453858252 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::VerifyState()
#define Enumerator_VerifyState_m2296597639(__this, method) ((  void (*) (Enumerator_t3453858252 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::MoveNext()
#define Enumerator_MoveNext_m1206239585(__this, method) ((  bool (*) (Enumerator_t3453858252 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.FieldInfo>::get_Current()
#define Enumerator_get_Current_m3366568820(__this, method) ((  FieldInfo_t * (*) (Enumerator_t3453858252 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
