﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WheelJoint2D
struct WheelJoint2D_t570080601;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointSuspension2D1941285899.h"
#include "UnityEngine_UnityEngine_JointMotor2D2112906529.h"

// UnityEngine.JointSuspension2D UnityEngine.WheelJoint2D::get_suspension()
extern "C"  JointSuspension2D_t1941285899  WheelJoint2D_get_suspension_m1233405115 (WheelJoint2D_t570080601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::set_suspension(UnityEngine.JointSuspension2D)
extern "C"  void WheelJoint2D_set_suspension_m940632710 (WheelJoint2D_t570080601 * __this, JointSuspension2D_t1941285899  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_get_suspension(UnityEngine.JointSuspension2D&)
extern "C"  void WheelJoint2D_INTERNAL_get_suspension_m2554873286 (WheelJoint2D_t570080601 * __this, JointSuspension2D_t1941285899 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_set_suspension(UnityEngine.JointSuspension2D&)
extern "C"  void WheelJoint2D_INTERNAL_set_suspension_m1983363850 (WheelJoint2D_t570080601 * __this, JointSuspension2D_t1941285899 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)
extern "C"  void WheelJoint2D_set_useMotor_m2831031726 (WheelJoint2D_t570080601 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointMotor2D UnityEngine.WheelJoint2D::get_motor()
extern "C"  JointMotor2D_t2112906529  WheelJoint2D_get_motor_m4205090403 (WheelJoint2D_t570080601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void WheelJoint2D_set_motor_m287781130 (WheelJoint2D_t570080601 * __this, JointMotor2D_t2112906529  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void WheelJoint2D_INTERNAL_get_motor_m3405369598 (WheelJoint2D_t570080601 * __this, JointMotor2D_t2112906529 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void WheelJoint2D_INTERNAL_set_motor_m2691972290 (WheelJoint2D_t570080601 * __this, JointMotor2D_t2112906529 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
