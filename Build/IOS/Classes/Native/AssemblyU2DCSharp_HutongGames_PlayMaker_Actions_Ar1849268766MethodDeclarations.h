﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListIndexOf
struct ArrayListIndexOf_t1849268766;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListIndexOf::.ctor()
extern "C"  void ArrayListIndexOf__ctor_m3661331150 (ArrayListIndexOf_t1849268766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListIndexOf::Reset()
extern "C"  void ArrayListIndexOf_Reset_m160695183 (ArrayListIndexOf_t1849268766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListIndexOf::OnEnter()
extern "C"  void ArrayListIndexOf_OnEnter_m2272756823 (ArrayListIndexOf_t1849268766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListIndexOf::DoArrayListIndexOf()
extern "C"  void ArrayListIndexOf_DoArrayListIndexOf_m3365829889 (ArrayListIndexOf_t1849268766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
