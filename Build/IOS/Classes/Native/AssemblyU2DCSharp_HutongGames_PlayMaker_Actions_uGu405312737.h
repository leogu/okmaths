﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition
struct  uGuiScrollRectSetNormalizedPosition_t405312737  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::normalizedPosition
	FsmVector2_t2430450063 * ___normalizedPosition_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::horizontalPosition
	FsmFloat_t937133978 * ___horizontalPosition_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::verticalPosition
	FsmFloat_t937133978 * ___verticalPosition_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_15;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.UI.ScrollRect HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_17;
	// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::_originalValue
	Vector2_t2243707579  ____originalValue_18;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_normalizedPosition_12() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ___normalizedPosition_12)); }
	inline FsmVector2_t2430450063 * get_normalizedPosition_12() const { return ___normalizedPosition_12; }
	inline FsmVector2_t2430450063 ** get_address_of_normalizedPosition_12() { return &___normalizedPosition_12; }
	inline void set_normalizedPosition_12(FsmVector2_t2430450063 * value)
	{
		___normalizedPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedPosition_12, value);
	}

	inline static int32_t get_offset_of_horizontalPosition_13() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ___horizontalPosition_13)); }
	inline FsmFloat_t937133978 * get_horizontalPosition_13() const { return ___horizontalPosition_13; }
	inline FsmFloat_t937133978 ** get_address_of_horizontalPosition_13() { return &___horizontalPosition_13; }
	inline void set_horizontalPosition_13(FsmFloat_t937133978 * value)
	{
		___horizontalPosition_13 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalPosition_13, value);
	}

	inline static int32_t get_offset_of_verticalPosition_14() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ___verticalPosition_14)); }
	inline FsmFloat_t937133978 * get_verticalPosition_14() const { return ___verticalPosition_14; }
	inline FsmFloat_t937133978 ** get_address_of_verticalPosition_14() { return &___verticalPosition_14; }
	inline void set_verticalPosition_14(FsmFloat_t937133978 * value)
	{
		___verticalPosition_14 = value;
		Il2CppCodeGenWriteBarrier(&___verticalPosition_14, value);
	}

	inline static int32_t get_offset_of_resetOnExit_15() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ___resetOnExit_15)); }
	inline FsmBool_t664485696 * get_resetOnExit_15() const { return ___resetOnExit_15; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_15() { return &___resetOnExit_15; }
	inline void set_resetOnExit_15(FsmBool_t664485696 * value)
	{
		___resetOnExit_15 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__scrollRect_17() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ____scrollRect_17)); }
	inline ScrollRect_t1199013257 * get__scrollRect_17() const { return ____scrollRect_17; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_17() { return &____scrollRect_17; }
	inline void set__scrollRect_17(ScrollRect_t1199013257 * value)
	{
		____scrollRect_17 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRect_17, value);
	}

	inline static int32_t get_offset_of__originalValue_18() { return static_cast<int32_t>(offsetof(uGuiScrollRectSetNormalizedPosition_t405312737, ____originalValue_18)); }
	inline Vector2_t2243707579  get__originalValue_18() const { return ____originalValue_18; }
	inline Vector2_t2243707579 * get_address_of__originalValue_18() { return &____originalValue_18; }
	inline void set__originalValue_18(Vector2_t2243707579  value)
	{
		____originalValue_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
