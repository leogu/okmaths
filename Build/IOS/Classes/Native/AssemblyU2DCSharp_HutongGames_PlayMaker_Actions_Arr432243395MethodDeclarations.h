﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions
struct ArrayListSetVertexPositions_t432243395;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::.ctor()
extern "C"  void ArrayListSetVertexPositions__ctor_m1203753309 (ArrayListSetVertexPositions_t432243395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::Reset()
extern "C"  void ArrayListSetVertexPositions_Reset_m111238816 (ArrayListSetVertexPositions_t432243395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::OnEnter()
extern "C"  void ArrayListSetVertexPositions_OnEnter_m667744282 (ArrayListSetVertexPositions_t432243395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::OnUpdate()
extern "C"  void ArrayListSetVertexPositions_OnUpdate_m2336563833 (ArrayListSetVertexPositions_t432243395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::SetVertexPositions()
extern "C"  void ArrayListSetVertexPositions_SetVertexPositions_m2643635773 (ArrayListSetVertexPositions_t432243395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
