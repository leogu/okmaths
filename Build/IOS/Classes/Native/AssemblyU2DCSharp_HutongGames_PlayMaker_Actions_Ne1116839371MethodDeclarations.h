﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetIsMessageQueueRunning
struct NetworkSetIsMessageQueueRunning_t1116839371;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetIsMessageQueueRunning::.ctor()
extern "C"  void NetworkSetIsMessageQueueRunning__ctor_m3410999977 (NetworkSetIsMessageQueueRunning_t1116839371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetIsMessageQueueRunning::Reset()
extern "C"  void NetworkSetIsMessageQueueRunning_Reset_m2368463212 (NetworkSetIsMessageQueueRunning_t1116839371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetIsMessageQueueRunning::OnEnter()
extern "C"  void NetworkSetIsMessageQueueRunning_OnEnter_m1982684902 (NetworkSetIsMessageQueueRunning_t1116839371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
