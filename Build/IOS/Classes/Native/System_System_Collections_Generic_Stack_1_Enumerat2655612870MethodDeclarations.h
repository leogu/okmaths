﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerato132208513MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<HutongGames.PlayMaker.Fsm>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m2759563747(__this, ___t0, method) ((  void (*) (Enumerator_t2655612870 *, Stack_1_t2005614510 *, const MethodInfo*))Enumerator__ctor_m2816143215_gshared)(__this, ___t0, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<HutongGames.PlayMaker.Fsm>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1126723979(__this, method) ((  void (*) (Enumerator_t2655612870 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m456699159_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<HutongGames.PlayMaker.Fsm>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4059136467(__this, method) ((  Il2CppObject * (*) (Enumerator_t2655612870 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1270503615_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<HutongGames.PlayMaker.Fsm>::Dispose()
#define Enumerator_Dispose_m1841175270(__this, method) ((  void (*) (Enumerator_t2655612870 *, const MethodInfo*))Enumerator_Dispose_m1520016780_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<HutongGames.PlayMaker.Fsm>::MoveNext()
#define Enumerator_MoveNext_m1875261495(__this, method) ((  bool (*) (Enumerator_t2655612870 *, const MethodInfo*))Enumerator_MoveNext_m689054299_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<HutongGames.PlayMaker.Fsm>::get_Current()
#define Enumerator_get_Current_m2887750842(__this, method) ((  Fsm_t917886356 * (*) (Enumerator_t2655612870 *, const MethodInfo*))Enumerator_get_Current_m2076859656_gshared)(__this, method)
