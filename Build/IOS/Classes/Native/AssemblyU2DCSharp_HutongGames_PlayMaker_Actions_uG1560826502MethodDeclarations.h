﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues
struct uGuiLayoutElementGetValues_t1560826502;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::.ctor()
extern "C"  void uGuiLayoutElementGetValues__ctor_m472884454 (uGuiLayoutElementGetValues_t1560826502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::Reset()
extern "C"  void uGuiLayoutElementGetValues_Reset_m1267723223 (uGuiLayoutElementGetValues_t1560826502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::OnEnter()
extern "C"  void uGuiLayoutElementGetValues_OnEnter_m132150127 (uGuiLayoutElementGetValues_t1560826502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::OnUpdate()
extern "C"  void uGuiLayoutElementGetValues_OnUpdate_m635348568 (uGuiLayoutElementGetValues_t1560826502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementGetValues::DoGetValues()
extern "C"  void uGuiLayoutElementGetValues_DoGetValues_m336440475 (uGuiLayoutElementGetValues_t1560826502 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
