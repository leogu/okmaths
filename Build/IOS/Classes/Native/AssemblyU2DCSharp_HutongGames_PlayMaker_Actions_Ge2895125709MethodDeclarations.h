﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSine
struct GetSine_t2895125709;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSine::.ctor()
extern "C"  void GetSine__ctor_m3140993201 (GetSine_t2895125709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::Reset()
extern "C"  void GetSine_Reset_m1999316198 (GetSine_t2895125709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::OnEnter()
extern "C"  void GetSine_OnEnter_m3632656812 (GetSine_t2895125709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::OnUpdate()
extern "C"  void GetSine_OnUpdate_m105492053 (GetSine_t2895125709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSine::DoSine()
extern "C"  void GetSine_DoSine_m3756451971 (GetSine_t2895125709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
