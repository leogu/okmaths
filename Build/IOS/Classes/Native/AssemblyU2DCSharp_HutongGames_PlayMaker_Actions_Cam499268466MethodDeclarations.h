﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CameraFadeIn
struct CameraFadeIn_t499268466;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::.ctor()
extern "C"  void CameraFadeIn__ctor_m1114489562 (CameraFadeIn_t499268466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::Reset()
extern "C"  void CameraFadeIn_Reset_m3688568931 (CameraFadeIn_t499268466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::OnEnter()
extern "C"  void CameraFadeIn_OnEnter_m789293227 (CameraFadeIn_t499268466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::OnUpdate()
extern "C"  void CameraFadeIn_OnUpdate_m3091429132 (CameraFadeIn_t499268466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeIn::OnGUI()
extern "C"  void CameraFadeIn_OnGUI_m3806132006 (CameraFadeIn_t499268466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
