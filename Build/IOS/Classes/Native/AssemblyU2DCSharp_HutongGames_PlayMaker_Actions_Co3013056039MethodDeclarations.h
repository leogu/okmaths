﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co3291029034MethodDeclarations.h"

// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::.ctor()
#define ComponentAction_1__ctor_m1676205853(__this, method) ((  void (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1__ctor_m2818003542_gshared)(__this, method)
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_rigidbody()
#define ComponentAction_1_get_rigidbody_m3769161604(__this, method) ((  Rigidbody_t4233889191 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_rigidbody_m3236782951_gshared)(__this, method)
// UnityEngine.Rigidbody2D HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_rigidbody2d()
#define ComponentAction_1_get_rigidbody2d_m1424312996(__this, method) ((  Rigidbody2D_t502193897 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_rigidbody2d_m370912679_gshared)(__this, method)
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_renderer()
#define ComponentAction_1_get_renderer_m3673047904(__this, method) ((  Renderer_t257310565 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_renderer_m3415049211_gshared)(__this, method)
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_animation()
#define ComponentAction_1_get_animation_m2672354052(__this, method) ((  Animation_t2068071072 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_animation_m3473365767_gshared)(__this, method)
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_audio()
#define ComponentAction_1_get_audio_m2685573797(__this, method) ((  AudioSource_t1135106623 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_audio_m1942127190_gshared)(__this, method)
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_camera()
#define ComponentAction_1_get_camera_m251737072(__this, method) ((  Camera_t189460977 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_camera_m1130907379_gshared)(__this, method)
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_guiText()
#define ComponentAction_1_get_guiText_m3378047460(__this, method) ((  GUIText_t2411476300 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_guiText_m295773063_gshared)(__this, method)
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_guiTexture()
#define ComponentAction_1_get_guiTexture_m1335191318(__this, method) ((  GUITexture_t1909122990 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_guiTexture_m2570816111_gshared)(__this, method)
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_light()
#define ComponentAction_1_get_light_m3393666756(__this, method) ((  Light_t494725636 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_light_m1266840839_gshared)(__this, method)
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::get_networkView()
#define ComponentAction_1_get_networkView_m1407550788(__this, method) ((  NetworkView_t172525251 * (*) (ComponentAction_1_t3013056039 *, const MethodInfo*))ComponentAction_1_get_networkView_m117748871_gshared)(__this, method)
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<UnityEngine.GUIText>::UpdateCache(UnityEngine.GameObject)
#define ComponentAction_1_UpdateCache_m232079396(__this, ___go0, method) ((  bool (*) (ComponentAction_1_t3013056039 *, GameObject_t1756533147 *, const MethodInfo*))ComponentAction_1_UpdateCache_m929902755_gshared)(__this, ___go0, method)
