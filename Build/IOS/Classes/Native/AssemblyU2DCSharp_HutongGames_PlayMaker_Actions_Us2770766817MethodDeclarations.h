﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.UseGravity
struct UseGravity_t2770766817;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.UseGravity::.ctor()
extern "C"  void UseGravity__ctor_m1201102207 (UseGravity_t2770766817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGravity::Reset()
extern "C"  void UseGravity_Reset_m2273724302 (UseGravity_t2770766817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGravity::OnEnter()
extern "C"  void UseGravity_OnEnter_m2171279512 (UseGravity_t2770766817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.UseGravity::DoUseGravity()
extern "C"  void UseGravity_DoUseGravity_m804410305 (UseGravity_t2770766817 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
