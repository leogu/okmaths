﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPunchRotation
struct iTweenPunchRotation_t2124981882;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::.ctor()
extern "C"  void iTweenPunchRotation__ctor_m2560442018 (iTweenPunchRotation_t2124981882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::Reset()
extern "C"  void iTweenPunchRotation_Reset_m531596111 (iTweenPunchRotation_t2124981882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::OnEnter()
extern "C"  void iTweenPunchRotation_OnEnter_m1561138247 (iTweenPunchRotation_t2124981882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::OnExit()
extern "C"  void iTweenPunchRotation_OnExit_m1696390695 (iTweenPunchRotation_t2124981882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPunchRotation::DoiTween()
extern "C"  void iTweenPunchRotation_DoiTween_m3001996249 (iTweenPunchRotation_t2124981882 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
