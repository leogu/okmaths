﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmVector2
struct GetFsmVector2_t2206672881;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::.ctor()
extern "C"  void GetFsmVector2__ctor_m3329547299 (GetFsmVector2_t2206672881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::Reset()
extern "C"  void GetFsmVector2_Reset_m3905935950 (GetFsmVector2_t2206672881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::OnEnter()
extern "C"  void GetFsmVector2_OnEnter_m3562903864 (GetFsmVector2_t2206672881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::OnUpdate()
extern "C"  void GetFsmVector2_OnUpdate_m286772683 (GetFsmVector2_t2206672881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVector2::DoGetFsmVector2()
extern "C"  void GetFsmVector2_DoGetFsmVector2_m1237096797 (GetFsmVector2_t2206672881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
