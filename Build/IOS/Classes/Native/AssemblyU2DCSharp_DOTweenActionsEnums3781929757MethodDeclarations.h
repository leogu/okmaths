﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DOTweenActionsEnums
struct DOTweenActionsEnums_t3781929757;

#include "codegen/il2cpp-codegen.h"

// System.Void DOTweenActionsEnums::.ctor()
extern "C"  void DOTweenActionsEnums__ctor_m4233069230 (DOTweenActionsEnums_t3781929757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
