﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue
struct HashTableGetKeyFromValue_t3319063194;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::.ctor()
extern "C"  void HashTableGetKeyFromValue__ctor_m3690162160 (HashTableGetKeyFromValue_t3319063194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::Reset()
extern "C"  void HashTableGetKeyFromValue_Reset_m469178535 (HashTableGetKeyFromValue_t3319063194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::OnEnter()
extern "C"  void HashTableGetKeyFromValue_OnEnter_m1613919655 (HashTableGetKeyFromValue_t3319063194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::SortHashTableByValues()
extern "C"  void HashTableGetKeyFromValue_SortHashTableByValues_m825897375 (HashTableGetKeyFromValue_t3319063194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
