﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleTo
struct iTweenScaleTo_t3102985765;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::.ctor()
extern "C"  void iTweenScaleTo__ctor_m3202127825 (iTweenScaleTo_t3102985765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::Reset()
extern "C"  void iTweenScaleTo_Reset_m3945368102 (iTweenScaleTo_t3102985765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::OnEnter()
extern "C"  void iTweenScaleTo_OnEnter_m4132982292 (iTweenScaleTo_t3102985765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::OnExit()
extern "C"  void iTweenScaleTo_OnExit_m970752568 (iTweenScaleTo_t3102985765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleTo::DoiTween()
extern "C"  void iTweenScaleTo_DoiTween_m3619272494 (iTweenScaleTo_t3102985765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
