﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiComponentProxy
struct PlayMakerUGuiComponentProxy_t3753606439;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t2110469976;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData2110469976.h"

// System.Void PlayMakerUGuiComponentProxy::.ctor()
extern "C"  void PlayMakerUGuiComponentProxy__ctor_m2239865676 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::Start()
extern "C"  void PlayMakerUGuiComponentProxy_Start_m28514392 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::Update()
extern "C"  void PlayMakerUGuiComponentProxy_Update_m1021931887 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetupEventTarget()
extern "C"  void PlayMakerUGuiComponentProxy_SetupEventTarget_m1678303300 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetupVariableTarget()
extern "C"  void PlayMakerUGuiComponentProxy_SetupVariableTarget_m3720551472 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetupUiListeners()
extern "C"  void PlayMakerUGuiComponentProxy_SetupUiListeners_m1147191490 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::OnClick()
extern "C"  void PlayMakerUGuiComponentProxy_OnClick_m2794869353 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::OnValueChanged(System.Boolean)
extern "C"  void PlayMakerUGuiComponentProxy_OnValueChanged_m3269198205 (PlayMakerUGuiComponentProxy_t3753606439 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::OnValueChanged(System.Int32)
extern "C"  void PlayMakerUGuiComponentProxy_OnValueChanged_m104109259 (PlayMakerUGuiComponentProxy_t3753606439 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::OnValueChanged(System.Single)
extern "C"  void PlayMakerUGuiComponentProxy_OnValueChanged_m1056545031 (PlayMakerUGuiComponentProxy_t3753606439 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::OnValueChanged(UnityEngine.Vector2)
extern "C"  void PlayMakerUGuiComponentProxy_OnValueChanged_m1720808228 (PlayMakerUGuiComponentProxy_t3753606439 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::onEndEdit(System.String)
extern "C"  void PlayMakerUGuiComponentProxy_onEndEdit_m79276604 (PlayMakerUGuiComponentProxy_t3753606439 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetFsmVariable(UnityEngine.Vector2)
extern "C"  void PlayMakerUGuiComponentProxy_SetFsmVariable_m1147998224 (PlayMakerUGuiComponentProxy_t3753606439 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetFsmVariable(System.Boolean)
extern "C"  void PlayMakerUGuiComponentProxy_SetFsmVariable_m1782139917 (PlayMakerUGuiComponentProxy_t3753606439 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetFsmVariable(System.Single)
extern "C"  void PlayMakerUGuiComponentProxy_SetFsmVariable_m2444693987 (PlayMakerUGuiComponentProxy_t3753606439 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::SetFsmVariable(System.String)
extern "C"  void PlayMakerUGuiComponentProxy_SetFsmVariable_m4069882590 (PlayMakerUGuiComponentProxy_t3753606439 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiComponentProxy::FirePlayMakerEvent(HutongGames.PlayMaker.FsmEventData)
extern "C"  void PlayMakerUGuiComponentProxy_FirePlayMakerEvent_m2047814256 (PlayMakerUGuiComponentProxy_t3753606439 * __this, FsmEventData_t2110469976 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUGuiComponentProxy::DoesTargetImplementsEvent()
extern "C"  bool PlayMakerUGuiComponentProxy_DoesTargetImplementsEvent_m2706179548 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerUGuiComponentProxy::GetEventString()
extern "C"  String_t* PlayMakerUGuiComponentProxy_GetEventString_m3276951392 (PlayMakerUGuiComponentProxy_t3753606439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
