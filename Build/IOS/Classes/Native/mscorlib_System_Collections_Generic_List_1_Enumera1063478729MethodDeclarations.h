﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.ParamDataType>
struct List_1_t1528749055;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1063478729.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2113362854_gshared (Enumerator_t1063478729 * __this, List_1_t1528749055 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m2113362854(__this, ___l0, method) ((  void (*) (Enumerator_t1063478729 *, List_1_t1528749055 *, const MethodInfo*))Enumerator__ctor_m2113362854_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2802969604_gshared (Enumerator_t1063478729 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2802969604(__this, method) ((  void (*) (Enumerator_t1063478729 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2802969604_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m314139824_gshared (Enumerator_t1063478729 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m314139824(__this, method) ((  Il2CppObject * (*) (Enumerator_t1063478729 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m314139824_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::Dispose()
extern "C"  void Enumerator_Dispose_m3075243667_gshared (Enumerator_t1063478729 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3075243667(__this, method) ((  void (*) (Enumerator_t1063478729 *, const MethodInfo*))Enumerator_Dispose_m3075243667_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::VerifyState()
extern "C"  void Enumerator_VerifyState_m123484344_gshared (Enumerator_t1063478729 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m123484344(__this, method) ((  void (*) (Enumerator_t1063478729 *, const MethodInfo*))Enumerator_VerifyState_m123484344_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2035517724_gshared (Enumerator_t1063478729 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2035517724(__this, method) ((  bool (*) (Enumerator_t1063478729 *, const MethodInfo*))Enumerator_MoveNext_m2035517724_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ParamDataType>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2130541515_gshared (Enumerator_t1063478729 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2130541515(__this, method) ((  int32_t (*) (Enumerator_t1063478729 *, const MethodInfo*))Enumerator_get_Current_m2130541515_gshared)(__this, method)
