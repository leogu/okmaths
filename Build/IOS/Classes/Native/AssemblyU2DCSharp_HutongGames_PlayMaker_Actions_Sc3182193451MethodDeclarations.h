﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScreenPick
struct ScreenPick_t3182193451;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScreenPick::.ctor()
extern "C"  void ScreenPick__ctor_m2060448851 (ScreenPick_t3182193451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::Reset()
extern "C"  void ScreenPick_Reset_m990566516 (ScreenPick_t3182193451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::OnEnter()
extern "C"  void ScreenPick_OnEnter_m435834938 (ScreenPick_t3182193451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::OnUpdate()
extern "C"  void ScreenPick_OnUpdate_m3814250835 (ScreenPick_t3182193451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick::DoScreenPick()
extern "C"  void ScreenPick_DoScreenPick_m1980130241 (ScreenPick_t3182193451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
