﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetRelative
struct ArrayListGetRelative_t3219836369;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRelative::.ctor()
extern "C"  void ArrayListGetRelative__ctor_m3380999323 (ArrayListGetRelative_t3219836369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRelative::Reset()
extern "C"  void ArrayListGetRelative_Reset_m3566284930 (ArrayListGetRelative_t3219836369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRelative::OnEnter()
extern "C"  void ArrayListGetRelative_OnEnter_m278113276 (ArrayListGetRelative_t3219836369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRelative::GetItemAtIncrement()
extern "C"  void ArrayListGetRelative_GetItemAtIncrement_m1222230522 (ArrayListGetRelative_t3219836369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
