﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StartCoroutine
struct StartCoroutine_t732815040;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::.ctor()
extern "C"  void StartCoroutine__ctor_m4239236982 (StartCoroutine_t732815040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::Reset()
extern "C"  void StartCoroutine_Reset_m2518055181 (StartCoroutine_t732815040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::OnEnter()
extern "C"  void StartCoroutine_OnEnter_m2494970685 (StartCoroutine_t732815040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::DoStartCoroutine()
extern "C"  void StartCoroutine_DoStartCoroutine_m3417314465 (StartCoroutine_t732815040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartCoroutine::OnExit()
extern "C"  void StartCoroutine_OnExit_m1840080961 (StartCoroutine_t732815040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
