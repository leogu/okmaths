﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMove
struct DOTweenRigidbody2DMove_t3135211178;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMove::.ctor()
extern "C"  void DOTweenRigidbody2DMove__ctor_m110156736 (DOTweenRigidbody2DMove_t3135211178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMove::Reset()
extern "C"  void DOTweenRigidbody2DMove_Reset_m2957115127 (DOTweenRigidbody2DMove_t3135211178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMove::OnEnter()
extern "C"  void DOTweenRigidbody2DMove_OnEnter_m1222513399 (DOTweenRigidbody2DMove_t3135211178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMove::<OnEnter>m__7A()
extern "C"  void DOTweenRigidbody2DMove_U3COnEnterU3Em__7A_m1752857708 (DOTweenRigidbody2DMove_t3135211178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMove::<OnEnter>m__7B()
extern "C"  void DOTweenRigidbody2DMove_U3COnEnterU3Em__7B_m489301745 (DOTweenRigidbody2DMove_t3135211178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
