﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor
struct uGuiSetButtonNormalColor_t3921991874;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::.ctor()
extern "C"  void uGuiSetButtonNormalColor__ctor_m3610097628 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::Reset()
extern "C"  void uGuiSetButtonNormalColor_Reset_m149552779 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::OnEnter()
extern "C"  void uGuiSetButtonNormalColor_OnEnter_m3980735827 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::OnUpdate()
extern "C"  void uGuiSetButtonNormalColor_OnUpdate_m3999509978 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::OnExit()
extern "C"  void uGuiSetButtonNormalColor_OnExit_m1398218459 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::Initialize(UnityEngine.GameObject)
extern "C"  void uGuiSetButtonNormalColor_Initialize_m2936416046 (uGuiSetButtonNormalColor_t3921991874 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::DoSetButtonColor()
extern "C"  void uGuiSetButtonNormalColor_DoSetButtonColor_m1162348580 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::DoSetOldColorValue()
extern "C"  void uGuiSetButtonNormalColor_DoSetOldColorValue_m4210472490 (uGuiSetButtonNormalColor_t3921991874 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
