﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetRandom
struct ArrayListGetRandom_t957002120;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRandom::.ctor()
extern "C"  void ArrayListGetRandom__ctor_m1702555140 (ArrayListGetRandom_t957002120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRandom::Reset()
extern "C"  void ArrayListGetRandom_Reset_m1886518805 (ArrayListGetRandom_t957002120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRandom::OnEnter()
extern "C"  void ArrayListGetRandom_OnEnter_m1033945149 (ArrayListGetRandom_t957002120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetRandom::GetRandomItem()
extern "C"  void ArrayListGetRandom_GetRandomItem_m2046546764 (ArrayListGetRandom_t957002120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
