﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayById
struct DOTweenControlMethodsPlayById_t1950991127;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayById::.ctor()
extern "C"  void DOTweenControlMethodsPlayById__ctor_m1363168477 (DOTweenControlMethodsPlayById_t1950991127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayById::Reset()
extern "C"  void DOTweenControlMethodsPlayById_Reset_m2940976728 (DOTweenControlMethodsPlayById_t1950991127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayById::OnEnter()
extern "C"  void DOTweenControlMethodsPlayById_OnEnter_m2595908578 (DOTweenControlMethodsPlayById_t1950991127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
