﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MouseLook2
struct MouseLook2_t789373378;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"

// System.Void HutongGames.PlayMaker.Actions.MouseLook2::.ctor()
extern "C"  void MouseLook2__ctor_m3997248670 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::Reset()
extern "C"  void MouseLook2_Reset_m1382529199 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::OnEnter()
extern "C"  void MouseLook2_OnEnter_m1278982631 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::OnUpdate()
extern "C"  void MouseLook2_OnUpdate_m3502364800 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook2::DoMouseLook()
extern "C"  void MouseLook2_DoMouseLook_m1721564023 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::GetXRotation()
extern "C"  float MouseLook2_GetXRotation_m1127232796 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::GetYRotation()
extern "C"  float MouseLook2_GetYRotation_m2530853527 (MouseLook2_t789373378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook2::ClampAngle(System.Single,HutongGames.PlayMaker.FsmFloat,HutongGames.PlayMaker.FsmFloat)
extern "C"  float MouseLook2_ClampAngle_m2166638015 (Il2CppObject * __this /* static, unused */, float ___angle0, FsmFloat_t937133978 * ___min1, FsmFloat_t937133978 * ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
