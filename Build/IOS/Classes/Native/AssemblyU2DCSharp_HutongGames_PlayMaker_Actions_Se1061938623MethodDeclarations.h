﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTagsOnChildren
struct SetTagsOnChildren_t1061938623;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::.ctor()
extern "C"  void SetTagsOnChildren__ctor_m1566931203 (SetTagsOnChildren_t1061938623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::Reset()
extern "C"  void SetTagsOnChildren_Reset_m3855352672 (SetTagsOnChildren_t1061938623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::OnEnter()
extern "C"  void SetTagsOnChildren_OnEnter_m256350894 (SetTagsOnChildren_t1061938623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::SetTag(UnityEngine.GameObject)
extern "C"  void SetTagsOnChildren_SetTag_m1155671315 (SetTagsOnChildren_t1061938623 * __this, GameObject_t1756533147 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTagsOnChildren::UpdateComponentFilter()
extern "C"  void SetTagsOnChildren_UpdateComponentFilter_m2980256149 (SetTagsOnChildren_t1061938623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
