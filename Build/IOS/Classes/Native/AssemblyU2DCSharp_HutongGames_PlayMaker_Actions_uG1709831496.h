﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject
struct  uGuiGetSelectedGameObject_t1709831496  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::StoreGameObject
	FsmGameObject_t3097142863 * ___StoreGameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::ObjectChangedEvent
	FsmEvent_t1258573736 * ___ObjectChangedEvent_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.uGuiGetSelectedGameObject::lastGameObject
	GameObject_t1756533147 * ___lastGameObject_14;

public:
	inline static int32_t get_offset_of_StoreGameObject_11() { return static_cast<int32_t>(offsetof(uGuiGetSelectedGameObject_t1709831496, ___StoreGameObject_11)); }
	inline FsmGameObject_t3097142863 * get_StoreGameObject_11() const { return ___StoreGameObject_11; }
	inline FsmGameObject_t3097142863 ** get_address_of_StoreGameObject_11() { return &___StoreGameObject_11; }
	inline void set_StoreGameObject_11(FsmGameObject_t3097142863 * value)
	{
		___StoreGameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___StoreGameObject_11, value);
	}

	inline static int32_t get_offset_of_ObjectChangedEvent_12() { return static_cast<int32_t>(offsetof(uGuiGetSelectedGameObject_t1709831496, ___ObjectChangedEvent_12)); }
	inline FsmEvent_t1258573736 * get_ObjectChangedEvent_12() const { return ___ObjectChangedEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_ObjectChangedEvent_12() { return &___ObjectChangedEvent_12; }
	inline void set_ObjectChangedEvent_12(FsmEvent_t1258573736 * value)
	{
		___ObjectChangedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectChangedEvent_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(uGuiGetSelectedGameObject_t1709831496, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_lastGameObject_14() { return static_cast<int32_t>(offsetof(uGuiGetSelectedGameObject_t1709831496, ___lastGameObject_14)); }
	inline GameObject_t1756533147 * get_lastGameObject_14() const { return ___lastGameObject_14; }
	inline GameObject_t1756533147 ** get_address_of_lastGameObject_14() { return &___lastGameObject_14; }
	inline void set_lastGameObject_14(GameObject_t1756533147 * value)
	{
		___lastGameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___lastGameObject_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
