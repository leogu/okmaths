﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorIK
struct PlayMakerAnimatorIK_t1460122143;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerAnimatorIK::OnAnimatorIK(System.Int32)
extern "C"  void PlayMakerAnimatorIK_OnAnimatorIK_m2712347503 (PlayMakerAnimatorIK_t1460122143 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorIK::.ctor()
extern "C"  void PlayMakerAnimatorIK__ctor_m775137980 (PlayMakerAnimatorIK_t1460122143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
