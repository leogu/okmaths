﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTextColor
struct DOTweenTextColor_t2654933044;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTextColor::.ctor()
extern "C"  void DOTweenTextColor__ctor_m1488699062 (DOTweenTextColor_t2654933044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextColor::Reset()
extern "C"  void DOTweenTextColor_Reset_m659690789 (DOTweenTextColor_t2654933044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextColor::OnEnter()
extern "C"  void DOTweenTextColor_OnEnter_m3116795509 (DOTweenTextColor_t2654933044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextColor::<OnEnter>m__9C()
extern "C"  void DOTweenTextColor_U3COnEnterU3Em__9C_m3688542798 (DOTweenTextColor_t2654933044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextColor::<OnEnter>m__9D()
extern "C"  void DOTweenTextColor_U3COnEnterU3Em__9D_m3829705299 (DOTweenTextColor_t2654933044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
