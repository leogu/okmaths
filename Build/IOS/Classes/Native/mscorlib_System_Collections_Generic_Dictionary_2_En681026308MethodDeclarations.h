﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3655968902;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En681026308.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21413314124.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3007524706_gshared (Enumerator_t681026308 * __this, Dictionary_2_t3655968902 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3007524706(__this, ___dictionary0, method) ((  void (*) (Enumerator_t681026308 *, Dictionary_2_t3655968902 *, const MethodInfo*))Enumerator__ctor_m3007524706_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3594355539_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3594355539(__this, method) ((  Il2CppObject * (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3594355539_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3390753507_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3390753507(__this, method) ((  void (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3390753507_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1608269866_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1608269866(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1608269866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3989254625_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3989254625(__this, method) ((  Il2CppObject * (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3989254625_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m952755377_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m952755377(__this, method) ((  Il2CppObject * (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m952755377_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1920508743_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1920508743(__this, method) ((  bool (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_MoveNext_m1920508743_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  KeyValuePair_2_t1413314124  Enumerator_get_Current_m2195469575_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2195469575(__this, method) ((  KeyValuePair_2_t1413314124  (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_get_Current_m2195469575_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m387894914_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m387894914(__this, method) ((  Il2CppObject * (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_get_CurrentKey_m387894914_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_CurrentValue()
extern "C"  RaycastHit2D_t4063908774  Enumerator_get_CurrentValue_m3503556298_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3503556298(__this, method) ((  RaycastHit2D_t4063908774  (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_get_CurrentValue_m3503556298_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Reset()
extern "C"  void Enumerator_Reset_m2618179180_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2618179180(__this, method) ((  void (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_Reset_m2618179180_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2175141401_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2175141401(__this, method) ((  void (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_VerifyState_m2175141401_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m4267695059_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m4267695059(__this, method) ((  void (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_VerifyCurrent_m4267695059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m3229374318_gshared (Enumerator_t681026308 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3229374318(__this, method) ((  void (*) (Enumerator_t681026308 *, const MethodInfo*))Enumerator_Dispose_m3229374318_gshared)(__this, method)
