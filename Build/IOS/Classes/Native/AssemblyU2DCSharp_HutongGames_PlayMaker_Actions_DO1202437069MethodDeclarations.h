﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsValidate
struct DOTweenAdditionalMethodsValidate_t1202437069;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsValidate::.ctor()
extern "C"  void DOTweenAdditionalMethodsValidate__ctor_m3271522183 (DOTweenAdditionalMethodsValidate_t1202437069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsValidate::Reset()
extern "C"  void DOTweenAdditionalMethodsValidate_Reset_m418600886 (DOTweenAdditionalMethodsValidate_t1202437069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsValidate::OnEnter()
extern "C"  void DOTweenAdditionalMethodsValidate_OnEnter_m3734487992 (DOTweenAdditionalMethodsValidate_t1202437069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
