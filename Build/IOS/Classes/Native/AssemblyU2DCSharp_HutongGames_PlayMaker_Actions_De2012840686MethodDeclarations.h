﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DeviceShakeEvent
struct DeviceShakeEvent_t2012840686;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DeviceShakeEvent::.ctor()
extern "C"  void DeviceShakeEvent__ctor_m1981473832 (DeviceShakeEvent_t2012840686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceShakeEvent::Reset()
extern "C"  void DeviceShakeEvent_Reset_m255687871 (DeviceShakeEvent_t2012840686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceShakeEvent::OnUpdate()
extern "C"  void DeviceShakeEvent_OnUpdate_m1083881038 (DeviceShakeEvent_t2012840686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
