﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// admob.Admob
struct Admob_t546240967;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// vcAdmob
struct  vcAdmob_t785204872  : public MonoBehaviour_t1158329972
{
public:
	// System.String vcAdmob::bannerID
	String_t* ___bannerID_2;
	// System.String vcAdmob::fullID
	String_t* ___fullID_3;
	// System.Boolean vcAdmob::isTest
	bool ___isTest_4;
	// admob.Admob vcAdmob::ad
	Admob_t546240967 * ___ad_5;

public:
	inline static int32_t get_offset_of_bannerID_2() { return static_cast<int32_t>(offsetof(vcAdmob_t785204872, ___bannerID_2)); }
	inline String_t* get_bannerID_2() const { return ___bannerID_2; }
	inline String_t** get_address_of_bannerID_2() { return &___bannerID_2; }
	inline void set_bannerID_2(String_t* value)
	{
		___bannerID_2 = value;
		Il2CppCodeGenWriteBarrier(&___bannerID_2, value);
	}

	inline static int32_t get_offset_of_fullID_3() { return static_cast<int32_t>(offsetof(vcAdmob_t785204872, ___fullID_3)); }
	inline String_t* get_fullID_3() const { return ___fullID_3; }
	inline String_t** get_address_of_fullID_3() { return &___fullID_3; }
	inline void set_fullID_3(String_t* value)
	{
		___fullID_3 = value;
		Il2CppCodeGenWriteBarrier(&___fullID_3, value);
	}

	inline static int32_t get_offset_of_isTest_4() { return static_cast<int32_t>(offsetof(vcAdmob_t785204872, ___isTest_4)); }
	inline bool get_isTest_4() const { return ___isTest_4; }
	inline bool* get_address_of_isTest_4() { return &___isTest_4; }
	inline void set_isTest_4(bool value)
	{
		___isTest_4 = value;
	}

	inline static int32_t get_offset_of_ad_5() { return static_cast<int32_t>(offsetof(vcAdmob_t785204872, ___ad_5)); }
	inline Admob_t546240967 * get_ad_5() const { return ___ad_5; }
	inline Admob_t546240967 ** get_address_of_ad_5() { return &___ad_5; }
	inline void set_ad_5(Admob_t546240967 * value)
	{
		___ad_5 = value;
		Il2CppCodeGenWriteBarrier(&___ad_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
