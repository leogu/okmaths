﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeAllRigidBodies
struct WakeAllRigidBodies_t2746289884;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::.ctor()
extern "C"  void WakeAllRigidBodies__ctor_m826198894 (WakeAllRigidBodies_t2746289884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::Reset()
extern "C"  void WakeAllRigidBodies_Reset_m4285164653 (WakeAllRigidBodies_t2746289884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::OnEnter()
extern "C"  void WakeAllRigidBodies_OnEnter_m2509135725 (WakeAllRigidBodies_t2746289884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::OnUpdate()
extern "C"  void WakeAllRigidBodies_OnUpdate_m4038744224 (WakeAllRigidBodies_t2746289884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeAllRigidBodies::DoWakeAll()
extern "C"  void WakeAllRigidBodies_DoWakeAll_m1176343434 (WakeAllRigidBodies_t2746289884 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
