﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntSwitch
struct IntSwitch_t1839610745;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntSwitch::.ctor()
extern "C"  void IntSwitch__ctor_m2150362767 (IntSwitch_t1839610745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::Reset()
extern "C"  void IntSwitch_Reset_m2608140914 (IntSwitch_t1839610745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::OnEnter()
extern "C"  void IntSwitch_OnEnter_m3161679572 (IntSwitch_t1839610745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::OnUpdate()
extern "C"  void IntSwitch_OnUpdate_m2752124575 (IntSwitch_t1839610745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntSwitch::DoIntSwitch()
extern "C"  void IntSwitch_DoIntSwitch_m3044246957 (IntSwitch_t1839610745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
