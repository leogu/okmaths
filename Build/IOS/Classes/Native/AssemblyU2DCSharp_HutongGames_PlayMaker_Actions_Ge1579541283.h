﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetLastPointerDataInfo
struct  GetLastPointerDataInfo_t1579541283  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::clickCount
	FsmInt_t1273009179 * ___clickCount_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::clickTime
	FsmFloat_t937133978 * ___clickTime_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::delta
	FsmVector2_t2430450063 * ___delta_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::dragging
	FsmBool_t664485696 * ___dragging_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::eligibleForClick
	FsmBool_t664485696 * ___eligibleForClick_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::enterEventCamera
	FsmGameObject_t3097142863 * ___enterEventCamera_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::pressEventCamera
	FsmGameObject_t3097142863 * ___pressEventCamera_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::isPointerMoving
	FsmBool_t664485696 * ___isPointerMoving_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::isScrolling
	FsmBool_t664485696 * ___isScrolling_20;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::lastPress
	FsmGameObject_t3097142863 * ___lastPress_21;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::pointerDrag
	FsmGameObject_t3097142863 * ___pointerDrag_22;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::pointerEnter
	FsmGameObject_t3097142863 * ___pointerEnter_23;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::pointerId
	FsmInt_t1273009179 * ___pointerId_24;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::pointerPress
	FsmGameObject_t3097142863 * ___pointerPress_25;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::position
	FsmVector2_t2430450063 * ___position_26;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::pressPosition
	FsmVector2_t2430450063 * ___pressPosition_27;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::rawPointerPress
	FsmGameObject_t3097142863 * ___rawPointerPress_28;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::scrollDelta
	FsmVector2_t2430450063 * ___scrollDelta_29;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::used
	FsmBool_t664485696 * ___used_30;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::useDragThreshold
	FsmBool_t664485696 * ___useDragThreshold_31;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::worldNormal
	FsmVector3_t3996534004 * ___worldNormal_32;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::worldPosition
	FsmVector3_t3996534004 * ___worldPosition_33;

public:
	inline static int32_t get_offset_of_clickCount_12() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___clickCount_12)); }
	inline FsmInt_t1273009179 * get_clickCount_12() const { return ___clickCount_12; }
	inline FsmInt_t1273009179 ** get_address_of_clickCount_12() { return &___clickCount_12; }
	inline void set_clickCount_12(FsmInt_t1273009179 * value)
	{
		___clickCount_12 = value;
		Il2CppCodeGenWriteBarrier(&___clickCount_12, value);
	}

	inline static int32_t get_offset_of_clickTime_13() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___clickTime_13)); }
	inline FsmFloat_t937133978 * get_clickTime_13() const { return ___clickTime_13; }
	inline FsmFloat_t937133978 ** get_address_of_clickTime_13() { return &___clickTime_13; }
	inline void set_clickTime_13(FsmFloat_t937133978 * value)
	{
		___clickTime_13 = value;
		Il2CppCodeGenWriteBarrier(&___clickTime_13, value);
	}

	inline static int32_t get_offset_of_delta_14() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___delta_14)); }
	inline FsmVector2_t2430450063 * get_delta_14() const { return ___delta_14; }
	inline FsmVector2_t2430450063 ** get_address_of_delta_14() { return &___delta_14; }
	inline void set_delta_14(FsmVector2_t2430450063 * value)
	{
		___delta_14 = value;
		Il2CppCodeGenWriteBarrier(&___delta_14, value);
	}

	inline static int32_t get_offset_of_dragging_15() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___dragging_15)); }
	inline FsmBool_t664485696 * get_dragging_15() const { return ___dragging_15; }
	inline FsmBool_t664485696 ** get_address_of_dragging_15() { return &___dragging_15; }
	inline void set_dragging_15(FsmBool_t664485696 * value)
	{
		___dragging_15 = value;
		Il2CppCodeGenWriteBarrier(&___dragging_15, value);
	}

	inline static int32_t get_offset_of_eligibleForClick_16() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___eligibleForClick_16)); }
	inline FsmBool_t664485696 * get_eligibleForClick_16() const { return ___eligibleForClick_16; }
	inline FsmBool_t664485696 ** get_address_of_eligibleForClick_16() { return &___eligibleForClick_16; }
	inline void set_eligibleForClick_16(FsmBool_t664485696 * value)
	{
		___eligibleForClick_16 = value;
		Il2CppCodeGenWriteBarrier(&___eligibleForClick_16, value);
	}

	inline static int32_t get_offset_of_enterEventCamera_17() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___enterEventCamera_17)); }
	inline FsmGameObject_t3097142863 * get_enterEventCamera_17() const { return ___enterEventCamera_17; }
	inline FsmGameObject_t3097142863 ** get_address_of_enterEventCamera_17() { return &___enterEventCamera_17; }
	inline void set_enterEventCamera_17(FsmGameObject_t3097142863 * value)
	{
		___enterEventCamera_17 = value;
		Il2CppCodeGenWriteBarrier(&___enterEventCamera_17, value);
	}

	inline static int32_t get_offset_of_pressEventCamera_18() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___pressEventCamera_18)); }
	inline FsmGameObject_t3097142863 * get_pressEventCamera_18() const { return ___pressEventCamera_18; }
	inline FsmGameObject_t3097142863 ** get_address_of_pressEventCamera_18() { return &___pressEventCamera_18; }
	inline void set_pressEventCamera_18(FsmGameObject_t3097142863 * value)
	{
		___pressEventCamera_18 = value;
		Il2CppCodeGenWriteBarrier(&___pressEventCamera_18, value);
	}

	inline static int32_t get_offset_of_isPointerMoving_19() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___isPointerMoving_19)); }
	inline FsmBool_t664485696 * get_isPointerMoving_19() const { return ___isPointerMoving_19; }
	inline FsmBool_t664485696 ** get_address_of_isPointerMoving_19() { return &___isPointerMoving_19; }
	inline void set_isPointerMoving_19(FsmBool_t664485696 * value)
	{
		___isPointerMoving_19 = value;
		Il2CppCodeGenWriteBarrier(&___isPointerMoving_19, value);
	}

	inline static int32_t get_offset_of_isScrolling_20() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___isScrolling_20)); }
	inline FsmBool_t664485696 * get_isScrolling_20() const { return ___isScrolling_20; }
	inline FsmBool_t664485696 ** get_address_of_isScrolling_20() { return &___isScrolling_20; }
	inline void set_isScrolling_20(FsmBool_t664485696 * value)
	{
		___isScrolling_20 = value;
		Il2CppCodeGenWriteBarrier(&___isScrolling_20, value);
	}

	inline static int32_t get_offset_of_lastPress_21() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___lastPress_21)); }
	inline FsmGameObject_t3097142863 * get_lastPress_21() const { return ___lastPress_21; }
	inline FsmGameObject_t3097142863 ** get_address_of_lastPress_21() { return &___lastPress_21; }
	inline void set_lastPress_21(FsmGameObject_t3097142863 * value)
	{
		___lastPress_21 = value;
		Il2CppCodeGenWriteBarrier(&___lastPress_21, value);
	}

	inline static int32_t get_offset_of_pointerDrag_22() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___pointerDrag_22)); }
	inline FsmGameObject_t3097142863 * get_pointerDrag_22() const { return ___pointerDrag_22; }
	inline FsmGameObject_t3097142863 ** get_address_of_pointerDrag_22() { return &___pointerDrag_22; }
	inline void set_pointerDrag_22(FsmGameObject_t3097142863 * value)
	{
		___pointerDrag_22 = value;
		Il2CppCodeGenWriteBarrier(&___pointerDrag_22, value);
	}

	inline static int32_t get_offset_of_pointerEnter_23() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___pointerEnter_23)); }
	inline FsmGameObject_t3097142863 * get_pointerEnter_23() const { return ___pointerEnter_23; }
	inline FsmGameObject_t3097142863 ** get_address_of_pointerEnter_23() { return &___pointerEnter_23; }
	inline void set_pointerEnter_23(FsmGameObject_t3097142863 * value)
	{
		___pointerEnter_23 = value;
		Il2CppCodeGenWriteBarrier(&___pointerEnter_23, value);
	}

	inline static int32_t get_offset_of_pointerId_24() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___pointerId_24)); }
	inline FsmInt_t1273009179 * get_pointerId_24() const { return ___pointerId_24; }
	inline FsmInt_t1273009179 ** get_address_of_pointerId_24() { return &___pointerId_24; }
	inline void set_pointerId_24(FsmInt_t1273009179 * value)
	{
		___pointerId_24 = value;
		Il2CppCodeGenWriteBarrier(&___pointerId_24, value);
	}

	inline static int32_t get_offset_of_pointerPress_25() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___pointerPress_25)); }
	inline FsmGameObject_t3097142863 * get_pointerPress_25() const { return ___pointerPress_25; }
	inline FsmGameObject_t3097142863 ** get_address_of_pointerPress_25() { return &___pointerPress_25; }
	inline void set_pointerPress_25(FsmGameObject_t3097142863 * value)
	{
		___pointerPress_25 = value;
		Il2CppCodeGenWriteBarrier(&___pointerPress_25, value);
	}

	inline static int32_t get_offset_of_position_26() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___position_26)); }
	inline FsmVector2_t2430450063 * get_position_26() const { return ___position_26; }
	inline FsmVector2_t2430450063 ** get_address_of_position_26() { return &___position_26; }
	inline void set_position_26(FsmVector2_t2430450063 * value)
	{
		___position_26 = value;
		Il2CppCodeGenWriteBarrier(&___position_26, value);
	}

	inline static int32_t get_offset_of_pressPosition_27() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___pressPosition_27)); }
	inline FsmVector2_t2430450063 * get_pressPosition_27() const { return ___pressPosition_27; }
	inline FsmVector2_t2430450063 ** get_address_of_pressPosition_27() { return &___pressPosition_27; }
	inline void set_pressPosition_27(FsmVector2_t2430450063 * value)
	{
		___pressPosition_27 = value;
		Il2CppCodeGenWriteBarrier(&___pressPosition_27, value);
	}

	inline static int32_t get_offset_of_rawPointerPress_28() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___rawPointerPress_28)); }
	inline FsmGameObject_t3097142863 * get_rawPointerPress_28() const { return ___rawPointerPress_28; }
	inline FsmGameObject_t3097142863 ** get_address_of_rawPointerPress_28() { return &___rawPointerPress_28; }
	inline void set_rawPointerPress_28(FsmGameObject_t3097142863 * value)
	{
		___rawPointerPress_28 = value;
		Il2CppCodeGenWriteBarrier(&___rawPointerPress_28, value);
	}

	inline static int32_t get_offset_of_scrollDelta_29() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___scrollDelta_29)); }
	inline FsmVector2_t2430450063 * get_scrollDelta_29() const { return ___scrollDelta_29; }
	inline FsmVector2_t2430450063 ** get_address_of_scrollDelta_29() { return &___scrollDelta_29; }
	inline void set_scrollDelta_29(FsmVector2_t2430450063 * value)
	{
		___scrollDelta_29 = value;
		Il2CppCodeGenWriteBarrier(&___scrollDelta_29, value);
	}

	inline static int32_t get_offset_of_used_30() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___used_30)); }
	inline FsmBool_t664485696 * get_used_30() const { return ___used_30; }
	inline FsmBool_t664485696 ** get_address_of_used_30() { return &___used_30; }
	inline void set_used_30(FsmBool_t664485696 * value)
	{
		___used_30 = value;
		Il2CppCodeGenWriteBarrier(&___used_30, value);
	}

	inline static int32_t get_offset_of_useDragThreshold_31() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___useDragThreshold_31)); }
	inline FsmBool_t664485696 * get_useDragThreshold_31() const { return ___useDragThreshold_31; }
	inline FsmBool_t664485696 ** get_address_of_useDragThreshold_31() { return &___useDragThreshold_31; }
	inline void set_useDragThreshold_31(FsmBool_t664485696 * value)
	{
		___useDragThreshold_31 = value;
		Il2CppCodeGenWriteBarrier(&___useDragThreshold_31, value);
	}

	inline static int32_t get_offset_of_worldNormal_32() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___worldNormal_32)); }
	inline FsmVector3_t3996534004 * get_worldNormal_32() const { return ___worldNormal_32; }
	inline FsmVector3_t3996534004 ** get_address_of_worldNormal_32() { return &___worldNormal_32; }
	inline void set_worldNormal_32(FsmVector3_t3996534004 * value)
	{
		___worldNormal_32 = value;
		Il2CppCodeGenWriteBarrier(&___worldNormal_32, value);
	}

	inline static int32_t get_offset_of_worldPosition_33() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283, ___worldPosition_33)); }
	inline FsmVector3_t3996534004 * get_worldPosition_33() const { return ___worldPosition_33; }
	inline FsmVector3_t3996534004 ** get_address_of_worldPosition_33() { return &___worldPosition_33; }
	inline void set_worldPosition_33(FsmVector3_t3996534004 * value)
	{
		___worldPosition_33 = value;
		Il2CppCodeGenWriteBarrier(&___worldPosition_33, value);
	}
};

struct GetLastPointerDataInfo_t1579541283_StaticFields
{
public:
	// UnityEngine.EventSystems.PointerEventData HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::lastPointeEventData
	PointerEventData_t1599784723 * ___lastPointeEventData_11;

public:
	inline static int32_t get_offset_of_lastPointeEventData_11() { return static_cast<int32_t>(offsetof(GetLastPointerDataInfo_t1579541283_StaticFields, ___lastPointeEventData_11)); }
	inline PointerEventData_t1599784723 * get_lastPointeEventData_11() const { return ___lastPointeEventData_11; }
	inline PointerEventData_t1599784723 ** get_address_of_lastPointeEventData_11() { return &___lastPointeEventData_11; }
	inline void set_lastPointeEventData_11(PointerEventData_t1599784723 * value)
	{
		___lastPointeEventData_11 = value;
		Il2CppCodeGenWriteBarrier(&___lastPointeEventData_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
