﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformBlendableMoveBy
struct DOTweenTransformBlendableMoveBy_t3350554403;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableMoveBy::.ctor()
extern "C"  void DOTweenTransformBlendableMoveBy__ctor_m1970769509 (DOTweenTransformBlendableMoveBy_t3350554403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableMoveBy::Reset()
extern "C"  void DOTweenTransformBlendableMoveBy_Reset_m4275046776 (DOTweenTransformBlendableMoveBy_t3350554403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableMoveBy::OnEnter()
extern "C"  void DOTweenTransformBlendableMoveBy_OnEnter_m3016983466 (DOTweenTransformBlendableMoveBy_t3350554403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableMoveBy::<OnEnter>m__AA()
extern "C"  void DOTweenTransformBlendableMoveBy_U3COnEnterU3Em__AA_m2731291207 (DOTweenTransformBlendableMoveBy_t3350554403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableMoveBy::<OnEnter>m__AB()
extern "C"  void DOTweenTransformBlendableMoveBy_U3COnEnterU3Em__AB_m2731291046 (DOTweenTransformBlendableMoveBy_t3350554403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
