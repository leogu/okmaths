﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt
struct uGuiInputFieldGetTextAsInt_t62661610;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::.ctor()
extern "C"  void uGuiInputFieldGetTextAsInt__ctor_m4089369954 (uGuiInputFieldGetTextAsInt_t62661610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::Reset()
extern "C"  void uGuiInputFieldGetTextAsInt_Reset_m1757036187 (uGuiInputFieldGetTextAsInt_t62661610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::OnEnter()
extern "C"  void uGuiInputFieldGetTextAsInt_OnEnter_m219672355 (uGuiInputFieldGetTextAsInt_t62661610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::OnUpdate()
extern "C"  void uGuiInputFieldGetTextAsInt_OnUpdate_m4227566228 (uGuiInputFieldGetTextAsInt_t62661610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::DoGetTextValue()
extern "C"  void uGuiInputFieldGetTextAsInt_DoGetTextValue_m1928549517 (uGuiInputFieldGetTextAsInt_t62661610 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
