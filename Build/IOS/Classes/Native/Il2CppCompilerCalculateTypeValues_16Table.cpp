﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_U3CModuleU3E3783534214.h"
#include "DOTween_DG_Tweening_AutoPlay2503223703.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "DOTween_DG_Tweening_EaseFunction3306356708.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_DOTween_U3CU3Ec__DisplayClass53467700650.h"
#include "DOTween_DG_Tweening_DOVirtual1722510550.h"
#include "DOTween_DG_Tweening_DOVirtual_U3CU3Ec__DisplayClas3724530167.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_EaseFactory4203735666.h"
#include "DOTween_DG_Tweening_EaseFactory_U3CU3Ec__DisplayCl2066423765.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "DOTween_DG_Tweening_ShortcutExtensions3524050470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842963.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842866.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843029.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842932.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842831.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842734.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842897.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842800.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843227.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843130.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978348.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691431.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653346.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366429.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628352.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341435.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303350.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678340.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391423.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978441.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691524.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653439.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366522.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628445.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341528.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303443.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016526.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391516.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978538.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691621.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653536.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366619.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628542.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341625.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303540.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016623.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678530.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391613.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978383.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691466.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653381.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366464.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628387.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303385.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016468.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678375.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391458.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978224.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691307.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653222.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366305.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628228.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341311.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303226.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678216.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391299.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978317.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691400.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653315.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366398.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628321.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341404.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303319.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016402.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391392.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978414.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691497.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653412.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (U3CModuleU3E_t3783534219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (AutoPlay_t2503223703)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1605[5] = 
{
	AutoPlay_t2503223703::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (AxisConstraint_t1244566668)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1606[6] = 
{
	AxisConstraint_t1244566668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Color2_t232726623)+ sizeof (Il2CppObject), sizeof(Color2_t232726623_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1607[2] = 
{
	Color2_t232726623::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color2_t232726623::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (TweenCallback_t3697142134), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (EaseFunction_t3306356708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (DOTween_t2276353038), -1, sizeof(DOTween_t2276353038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1611[24] = 
{
	DOTween_t2276353038_StaticFields::get_offset_of_Version_0(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t2276353038_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t2276353038_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t2276353038_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t2276353038_StaticFields::get_offset_of__logBehaviour_5(),
	DOTween_t2276353038_StaticFields::get_offset_of_drawGizmos_6(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultUpdateType_7(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultTimeScaleIndependent_8(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoPlay_9(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultAutoKill_10(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultLoopType_11(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultRecyclable_12(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseType_13(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_14(),
	DOTween_t2276353038_StaticFields::get_offset_of_defaultEasePeriod_15(),
	DOTween_t2276353038_StaticFields::get_offset_of_instance_16(),
	DOTween_t2276353038_StaticFields::get_offset_of_isUnityEditor_17(),
	DOTween_t2276353038_StaticFields::get_offset_of_isDebugBuild_18(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveTweenersReached_19(),
	DOTween_t2276353038_StaticFields::get_offset_of_maxActiveSequencesReached_20(),
	DOTween_t2276353038_StaticFields::get_offset_of_GizmosDelegates_21(),
	DOTween_t2276353038_StaticFields::get_offset_of_initialized_22(),
	DOTween_t2276353038_StaticFields::get_offset_of_isQuitting_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (U3CU3Ec__DisplayClass52_0_t3467700650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1612[2] = 
{
	U3CU3Ec__DisplayClass52_0_t3467700650::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass52_0_t3467700650::get_offset_of_setter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (DOVirtual_t1722510550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (U3CU3Ec__DisplayClass0_0_t3724530167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_val_0(),
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_onVirtualUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (Ease_t2502520296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1615[39] = 
{
	Ease_t2502520296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (EaseFactory_t4203735666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (U3CU3Ec__DisplayClass2_0_t2066423765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[2] = 
{
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_motionDelay_0(),
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_customEase_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (PathMode_t1545785466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1619[5] = 
{
	PathMode_t1545785466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (PathType_t2815988833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1620[3] = 
{
	PathType_t2815988833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (RotateMode_t1177727514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1621[5] = 
{
	RotateMode_t1177727514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (ScrambleMode_t385206138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[7] = 
{
	ScrambleMode_t385206138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (TweenExtensions_t405253783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (LoopType_t2249218064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1624[4] = 
{
	LoopType_t2249218064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (Sequence_t110643099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1625[3] = 
{
	Sequence_t110643099::get_offset_of_sequencedTweens_51(),
	Sequence_t110643099::get_offset_of__sequencedObjs_52(),
	Sequence_t110643099::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (ShortcutExtensions_t3524050470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (U3CU3Ec__DisplayClass0_0_t964842963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[1] = 
{
	U3CU3Ec__DisplayClass0_0_t964842963::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (U3CU3Ec__DisplayClass1_0_t964842866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	U3CU3Ec__DisplayClass1_0_t964842866::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (U3CU3Ec__DisplayClass2_0_t964843029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[1] = 
{
	U3CU3Ec__DisplayClass2_0_t964843029::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (U3CU3Ec__DisplayClass3_0_t964842932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1630[1] = 
{
	U3CU3Ec__DisplayClass3_0_t964842932::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (U3CU3Ec__DisplayClass4_0_t964842831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[1] = 
{
	U3CU3Ec__DisplayClass4_0_t964842831::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (U3CU3Ec__DisplayClass5_0_t964842734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1632[1] = 
{
	U3CU3Ec__DisplayClass5_0_t964842734::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (U3CU3Ec__DisplayClass6_0_t964842897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1633[1] = 
{
	U3CU3Ec__DisplayClass6_0_t964842897::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (U3CU3Ec__DisplayClass7_0_t964842800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1634[1] = 
{
	U3CU3Ec__DisplayClass7_0_t964842800::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (U3CU3Ec__DisplayClass8_0_t964843227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1635[1] = 
{
	U3CU3Ec__DisplayClass8_0_t964843227::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (U3CU3Ec__DisplayClass9_0_t964843130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1636[1] = 
{
	U3CU3Ec__DisplayClass9_0_t964843130::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (U3CU3Ec__DisplayClass10_0_t3010978348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1637[1] = 
{
	U3CU3Ec__DisplayClass10_0_t3010978348::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (U3CU3Ec__DisplayClass11_0_t1424691431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1638[1] = 
{
	U3CU3Ec__DisplayClass11_0_t1424691431::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (U3CU3Ec__DisplayClass12_0_t2728653346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2728653346::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (U3CU3Ec__DisplayClass13_0_t1142366429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[1] = 
{
	U3CU3Ec__DisplayClass13_0_t1142366429::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (U3CU3Ec__DisplayClass14_0_t3575628352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[1] = 
{
	U3CU3Ec__DisplayClass14_0_t3575628352::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (U3CU3Ec__DisplayClass15_0_t1989341435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[1] = 
{
	U3CU3Ec__DisplayClass15_0_t1989341435::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (U3CU3Ec__DisplayClass16_0_t3293303350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3293303350::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (U3CU3Ec__DisplayClass17_0_t1707016433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[2] = 
{
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_startValue_0(),
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (U3CU3Ec__DisplayClass18_0_t1881678340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1645[1] = 
{
	U3CU3Ec__DisplayClass18_0_t1881678340::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (U3CU3Ec__DisplayClass19_0_t295391423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[2] = 
{
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (U3CU3Ec__DisplayClass20_0_t3010978441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1647[1] = 
{
	U3CU3Ec__DisplayClass20_0_t3010978441::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (U3CU3Ec__DisplayClass21_0_t1424691524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[2] = 
{
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (U3CU3Ec__DisplayClass22_0_t2728653439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1649[2] = 
{
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (U3CU3Ec__DisplayClass23_0_t1142366522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[1] = 
{
	U3CU3Ec__DisplayClass23_0_t1142366522::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (U3CU3Ec__DisplayClass24_0_t3575628445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[2] = 
{
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (U3CU3Ec__DisplayClass25_0_t1989341528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[1] = 
{
	U3CU3Ec__DisplayClass25_0_t1989341528::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (U3CU3Ec__DisplayClass26_0_t3293303443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[2] = 
{
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (U3CU3Ec__DisplayClass27_0_t1707016526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[2] = 
{
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (U3CU3Ec__DisplayClass28_0_t1881678433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1881678433::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (U3CU3Ec__DisplayClass29_0_t295391516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	U3CU3Ec__DisplayClass29_0_t295391516::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (U3CU3Ec__DisplayClass30_0_t3010978538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[1] = 
{
	U3CU3Ec__DisplayClass30_0_t3010978538::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (U3CU3Ec__DisplayClass31_0_t1424691621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1658[1] = 
{
	U3CU3Ec__DisplayClass31_0_t1424691621::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (U3CU3Ec__DisplayClass32_0_t2728653536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1659[1] = 
{
	U3CU3Ec__DisplayClass32_0_t2728653536::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (U3CU3Ec__DisplayClass33_0_t1142366619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[1] = 
{
	U3CU3Ec__DisplayClass33_0_t1142366619::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (U3CU3Ec__DisplayClass34_0_t3575628542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[6] = 
{
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (U3CU3Ec__DisplayClass35_0_t1989341625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1662[1] = 
{
	U3CU3Ec__DisplayClass35_0_t1989341625::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (U3CU3Ec__DisplayClass36_0_t3293303540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[1] = 
{
	U3CU3Ec__DisplayClass36_0_t3293303540::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (U3CU3Ec__DisplayClass37_0_t1707016623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[1] = 
{
	U3CU3Ec__DisplayClass37_0_t1707016623::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (U3CU3Ec__DisplayClass38_0_t1881678530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[1] = 
{
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (U3CU3Ec__DisplayClass39_0_t295391613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	U3CU3Ec__DisplayClass39_0_t295391613::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (U3CU3Ec__DisplayClass40_0_t3010978383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[1] = 
{
	U3CU3Ec__DisplayClass40_0_t3010978383::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (U3CU3Ec__DisplayClass41_0_t1424691466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[1] = 
{
	U3CU3Ec__DisplayClass41_0_t1424691466::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (U3CU3Ec__DisplayClass42_0_t2728653381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2728653381::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (U3CU3Ec__DisplayClass43_0_t1142366464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[1] = 
{
	U3CU3Ec__DisplayClass43_0_t1142366464::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (U3CU3Ec__DisplayClass44_0_t3575628387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[1] = 
{
	U3CU3Ec__DisplayClass44_0_t3575628387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (U3CU3Ec__DisplayClass45_0_t1989341470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[1] = 
{
	U3CU3Ec__DisplayClass45_0_t1989341470::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (U3CU3Ec__DisplayClass46_0_t3293303385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[1] = 
{
	U3CU3Ec__DisplayClass46_0_t3293303385::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (U3CU3Ec__DisplayClass47_0_t1707016468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[1] = 
{
	U3CU3Ec__DisplayClass47_0_t1707016468::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (U3CU3Ec__DisplayClass48_0_t1881678375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[1] = 
{
	U3CU3Ec__DisplayClass48_0_t1881678375::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (U3CU3Ec__DisplayClass49_0_t295391458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[1] = 
{
	U3CU3Ec__DisplayClass49_0_t295391458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (U3CU3Ec__DisplayClass50_0_t3010978224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[1] = 
{
	U3CU3Ec__DisplayClass50_0_t3010978224::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (U3CU3Ec__DisplayClass51_0_t1424691307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1678[1] = 
{
	U3CU3Ec__DisplayClass51_0_t1424691307::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (U3CU3Ec__DisplayClass52_0_t2728653222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[1] = 
{
	U3CU3Ec__DisplayClass52_0_t2728653222::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (U3CU3Ec__DisplayClass53_0_t1142366305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[1] = 
{
	U3CU3Ec__DisplayClass53_0_t1142366305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (U3CU3Ec__DisplayClass54_0_t3575628228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[1] = 
{
	U3CU3Ec__DisplayClass54_0_t3575628228::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (U3CU3Ec__DisplayClass55_0_t1989341311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[1] = 
{
	U3CU3Ec__DisplayClass55_0_t1989341311::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (U3CU3Ec__DisplayClass56_0_t3293303226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[1] = 
{
	U3CU3Ec__DisplayClass56_0_t3293303226::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (U3CU3Ec__DisplayClass57_0_t1707016309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[1] = 
{
	U3CU3Ec__DisplayClass57_0_t1707016309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (U3CU3Ec__DisplayClass58_0_t1881678216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1685[1] = 
{
	U3CU3Ec__DisplayClass58_0_t1881678216::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (U3CU3Ec__DisplayClass59_0_t295391299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[1] = 
{
	U3CU3Ec__DisplayClass59_0_t295391299::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (U3CU3Ec__DisplayClass60_0_t3010978317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[1] = 
{
	U3CU3Ec__DisplayClass60_0_t3010978317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (U3CU3Ec__DisplayClass61_0_t1424691400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1688[1] = 
{
	U3CU3Ec__DisplayClass61_0_t1424691400::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (U3CU3Ec__DisplayClass62_0_t2728653315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2728653315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (U3CU3Ec__DisplayClass63_0_t1142366398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[1] = 
{
	U3CU3Ec__DisplayClass63_0_t1142366398::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (U3CU3Ec__DisplayClass64_0_t3575628321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[6] = 
{
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (U3CU3Ec__DisplayClass65_0_t1989341404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[6] = 
{
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (U3CU3Ec__DisplayClass66_0_t3293303319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1693[1] = 
{
	U3CU3Ec__DisplayClass66_0_t3293303319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (U3CU3Ec__DisplayClass67_0_t1707016402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[1] = 
{
	U3CU3Ec__DisplayClass67_0_t1707016402::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (U3CU3Ec__DisplayClass68_0_t1881678309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[1] = 
{
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (U3CU3Ec__DisplayClass69_0_t295391392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[1] = 
{
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (U3CU3Ec__DisplayClass70_0_t3010978414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1697[2] = 
{
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (U3CU3Ec__DisplayClass71_0_t1424691497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1698[2] = 
{
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (U3CU3Ec__DisplayClass72_0_t2728653412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1699[3] = 
{
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_property_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
