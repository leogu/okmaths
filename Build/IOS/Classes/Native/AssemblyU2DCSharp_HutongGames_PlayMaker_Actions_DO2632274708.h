﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t3601875862;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_DOT206417114.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath
struct  DOTweenTransformLocalPath_t2632274708  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::path
	FsmGameObjectU5BU5D_t3601875862* ___path_12;
	// DG.Tweening.PathType HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::pathType
	int32_t ___pathType_13;
	// DG.Tweening.PathMode HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::pathMode
	int32_t ___pathMode_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::resolution
	FsmInt_t1273009179 * ___resolution_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::gizmoColor
	FsmColor_t118301965 * ___gizmoColor_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::duration
	FsmFloat_t937133978 * ___duration_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::startDelay
	FsmFloat_t937133978 * ___startDelay_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::closePath
	FsmBool_t664485696 * ___closePath_20;
	// DG.Tweening.AxisConstraint HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::lockPosition
	int32_t ___lockPosition_21;
	// DG.Tweening.AxisConstraint HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::lockRotation
	int32_t ___lockRotation_22;
	// HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath/LookAt HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::lookAt
	int32_t ___lookAt_23;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::lookAtPosition
	FsmVector3_t3996534004 * ___lookAtPosition_24;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::lookAtTarget
	FsmGameObject_t3097142863 * ___lookAtTarget_25;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::lookAhead
	FsmFloat_t937133978 * ___lookAhead_26;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::forwardDirection
	FsmVector3_t3996534004 * ___forwardDirection_27;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::up
	FsmVector3_t3996534004 * ___up_28;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::startEvent
	FsmEvent_t1258573736 * ___startEvent_29;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_30;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_31;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::tweenIdDescription
	String_t* ___tweenIdDescription_32;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::tweenIdType
	int32_t ___tweenIdType_33;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::stringAsId
	FsmString_t2414474701 * ___stringAsId_34;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::tagAsId
	FsmString_t2414474701 * ___tagAsId_35;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::selectedEase
	int32_t ___selectedEase_36;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::easeType
	int32_t ___easeType_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_38;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_39;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::loops
	FsmInt_t1273009179 * ___loops_40;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::loopType
	int32_t ___loopType_41;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_42;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::recyclable
	FsmBool_t664485696 * ___recyclable_43;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::updateType
	int32_t ___updateType_44;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_45;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::debugThis
	FsmBool_t664485696 * ___debugThis_46;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenTransformLocalPath::tweener
	Tweener_t760404022 * ___tweener_47;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_path_12() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___path_12)); }
	inline FsmGameObjectU5BU5D_t3601875862* get_path_12() const { return ___path_12; }
	inline FsmGameObjectU5BU5D_t3601875862** get_address_of_path_12() { return &___path_12; }
	inline void set_path_12(FsmGameObjectU5BU5D_t3601875862* value)
	{
		___path_12 = value;
		Il2CppCodeGenWriteBarrier(&___path_12, value);
	}

	inline static int32_t get_offset_of_pathType_13() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___pathType_13)); }
	inline int32_t get_pathType_13() const { return ___pathType_13; }
	inline int32_t* get_address_of_pathType_13() { return &___pathType_13; }
	inline void set_pathType_13(int32_t value)
	{
		___pathType_13 = value;
	}

	inline static int32_t get_offset_of_pathMode_14() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___pathMode_14)); }
	inline int32_t get_pathMode_14() const { return ___pathMode_14; }
	inline int32_t* get_address_of_pathMode_14() { return &___pathMode_14; }
	inline void set_pathMode_14(int32_t value)
	{
		___pathMode_14 = value;
	}

	inline static int32_t get_offset_of_resolution_15() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___resolution_15)); }
	inline FsmInt_t1273009179 * get_resolution_15() const { return ___resolution_15; }
	inline FsmInt_t1273009179 ** get_address_of_resolution_15() { return &___resolution_15; }
	inline void set_resolution_15(FsmInt_t1273009179 * value)
	{
		___resolution_15 = value;
		Il2CppCodeGenWriteBarrier(&___resolution_15, value);
	}

	inline static int32_t get_offset_of_gizmoColor_16() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___gizmoColor_16)); }
	inline FsmColor_t118301965 * get_gizmoColor_16() const { return ___gizmoColor_16; }
	inline FsmColor_t118301965 ** get_address_of_gizmoColor_16() { return &___gizmoColor_16; }
	inline void set_gizmoColor_16(FsmColor_t118301965 * value)
	{
		___gizmoColor_16 = value;
		Il2CppCodeGenWriteBarrier(&___gizmoColor_16, value);
	}

	inline static int32_t get_offset_of_duration_17() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___duration_17)); }
	inline FsmFloat_t937133978 * get_duration_17() const { return ___duration_17; }
	inline FsmFloat_t937133978 ** get_address_of_duration_17() { return &___duration_17; }
	inline void set_duration_17(FsmFloat_t937133978 * value)
	{
		___duration_17 = value;
		Il2CppCodeGenWriteBarrier(&___duration_17, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_18() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___setSpeedBased_18)); }
	inline FsmBool_t664485696 * get_setSpeedBased_18() const { return ___setSpeedBased_18; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_18() { return &___setSpeedBased_18; }
	inline void set_setSpeedBased_18(FsmBool_t664485696 * value)
	{
		___setSpeedBased_18 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_18, value);
	}

	inline static int32_t get_offset_of_startDelay_19() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___startDelay_19)); }
	inline FsmFloat_t937133978 * get_startDelay_19() const { return ___startDelay_19; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_19() { return &___startDelay_19; }
	inline void set_startDelay_19(FsmFloat_t937133978 * value)
	{
		___startDelay_19 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_19, value);
	}

	inline static int32_t get_offset_of_closePath_20() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___closePath_20)); }
	inline FsmBool_t664485696 * get_closePath_20() const { return ___closePath_20; }
	inline FsmBool_t664485696 ** get_address_of_closePath_20() { return &___closePath_20; }
	inline void set_closePath_20(FsmBool_t664485696 * value)
	{
		___closePath_20 = value;
		Il2CppCodeGenWriteBarrier(&___closePath_20, value);
	}

	inline static int32_t get_offset_of_lockPosition_21() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___lockPosition_21)); }
	inline int32_t get_lockPosition_21() const { return ___lockPosition_21; }
	inline int32_t* get_address_of_lockPosition_21() { return &___lockPosition_21; }
	inline void set_lockPosition_21(int32_t value)
	{
		___lockPosition_21 = value;
	}

	inline static int32_t get_offset_of_lockRotation_22() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___lockRotation_22)); }
	inline int32_t get_lockRotation_22() const { return ___lockRotation_22; }
	inline int32_t* get_address_of_lockRotation_22() { return &___lockRotation_22; }
	inline void set_lockRotation_22(int32_t value)
	{
		___lockRotation_22 = value;
	}

	inline static int32_t get_offset_of_lookAt_23() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___lookAt_23)); }
	inline int32_t get_lookAt_23() const { return ___lookAt_23; }
	inline int32_t* get_address_of_lookAt_23() { return &___lookAt_23; }
	inline void set_lookAt_23(int32_t value)
	{
		___lookAt_23 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_24() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___lookAtPosition_24)); }
	inline FsmVector3_t3996534004 * get_lookAtPosition_24() const { return ___lookAtPosition_24; }
	inline FsmVector3_t3996534004 ** get_address_of_lookAtPosition_24() { return &___lookAtPosition_24; }
	inline void set_lookAtPosition_24(FsmVector3_t3996534004 * value)
	{
		___lookAtPosition_24 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtPosition_24, value);
	}

	inline static int32_t get_offset_of_lookAtTarget_25() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___lookAtTarget_25)); }
	inline FsmGameObject_t3097142863 * get_lookAtTarget_25() const { return ___lookAtTarget_25; }
	inline FsmGameObject_t3097142863 ** get_address_of_lookAtTarget_25() { return &___lookAtTarget_25; }
	inline void set_lookAtTarget_25(FsmGameObject_t3097142863 * value)
	{
		___lookAtTarget_25 = value;
		Il2CppCodeGenWriteBarrier(&___lookAtTarget_25, value);
	}

	inline static int32_t get_offset_of_lookAhead_26() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___lookAhead_26)); }
	inline FsmFloat_t937133978 * get_lookAhead_26() const { return ___lookAhead_26; }
	inline FsmFloat_t937133978 ** get_address_of_lookAhead_26() { return &___lookAhead_26; }
	inline void set_lookAhead_26(FsmFloat_t937133978 * value)
	{
		___lookAhead_26 = value;
		Il2CppCodeGenWriteBarrier(&___lookAhead_26, value);
	}

	inline static int32_t get_offset_of_forwardDirection_27() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___forwardDirection_27)); }
	inline FsmVector3_t3996534004 * get_forwardDirection_27() const { return ___forwardDirection_27; }
	inline FsmVector3_t3996534004 ** get_address_of_forwardDirection_27() { return &___forwardDirection_27; }
	inline void set_forwardDirection_27(FsmVector3_t3996534004 * value)
	{
		___forwardDirection_27 = value;
		Il2CppCodeGenWriteBarrier(&___forwardDirection_27, value);
	}

	inline static int32_t get_offset_of_up_28() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___up_28)); }
	inline FsmVector3_t3996534004 * get_up_28() const { return ___up_28; }
	inline FsmVector3_t3996534004 ** get_address_of_up_28() { return &___up_28; }
	inline void set_up_28(FsmVector3_t3996534004 * value)
	{
		___up_28 = value;
		Il2CppCodeGenWriteBarrier(&___up_28, value);
	}

	inline static int32_t get_offset_of_startEvent_29() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___startEvent_29)); }
	inline FsmEvent_t1258573736 * get_startEvent_29() const { return ___startEvent_29; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_29() { return &___startEvent_29; }
	inline void set_startEvent_29(FsmEvent_t1258573736 * value)
	{
		___startEvent_29 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_29, value);
	}

	inline static int32_t get_offset_of_finishEvent_30() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___finishEvent_30)); }
	inline FsmEvent_t1258573736 * get_finishEvent_30() const { return ___finishEvent_30; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_30() { return &___finishEvent_30; }
	inline void set_finishEvent_30(FsmEvent_t1258573736 * value)
	{
		___finishEvent_30 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_30, value);
	}

	inline static int32_t get_offset_of_finishImmediately_31() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___finishImmediately_31)); }
	inline FsmBool_t664485696 * get_finishImmediately_31() const { return ___finishImmediately_31; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_31() { return &___finishImmediately_31; }
	inline void set_finishImmediately_31(FsmBool_t664485696 * value)
	{
		___finishImmediately_31 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_31, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_32() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___tweenIdDescription_32)); }
	inline String_t* get_tweenIdDescription_32() const { return ___tweenIdDescription_32; }
	inline String_t** get_address_of_tweenIdDescription_32() { return &___tweenIdDescription_32; }
	inline void set_tweenIdDescription_32(String_t* value)
	{
		___tweenIdDescription_32 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_32, value);
	}

	inline static int32_t get_offset_of_tweenIdType_33() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___tweenIdType_33)); }
	inline int32_t get_tweenIdType_33() const { return ___tweenIdType_33; }
	inline int32_t* get_address_of_tweenIdType_33() { return &___tweenIdType_33; }
	inline void set_tweenIdType_33(int32_t value)
	{
		___tweenIdType_33 = value;
	}

	inline static int32_t get_offset_of_stringAsId_34() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___stringAsId_34)); }
	inline FsmString_t2414474701 * get_stringAsId_34() const { return ___stringAsId_34; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_34() { return &___stringAsId_34; }
	inline void set_stringAsId_34(FsmString_t2414474701 * value)
	{
		___stringAsId_34 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_34, value);
	}

	inline static int32_t get_offset_of_tagAsId_35() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___tagAsId_35)); }
	inline FsmString_t2414474701 * get_tagAsId_35() const { return ___tagAsId_35; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_35() { return &___tagAsId_35; }
	inline void set_tagAsId_35(FsmString_t2414474701 * value)
	{
		___tagAsId_35 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_35, value);
	}

	inline static int32_t get_offset_of_selectedEase_36() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___selectedEase_36)); }
	inline int32_t get_selectedEase_36() const { return ___selectedEase_36; }
	inline int32_t* get_address_of_selectedEase_36() { return &___selectedEase_36; }
	inline void set_selectedEase_36(int32_t value)
	{
		___selectedEase_36 = value;
	}

	inline static int32_t get_offset_of_easeType_37() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___easeType_37)); }
	inline int32_t get_easeType_37() const { return ___easeType_37; }
	inline int32_t* get_address_of_easeType_37() { return &___easeType_37; }
	inline void set_easeType_37(int32_t value)
	{
		___easeType_37 = value;
	}

	inline static int32_t get_offset_of_animationCurve_38() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___animationCurve_38)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_38() const { return ___animationCurve_38; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_38() { return &___animationCurve_38; }
	inline void set_animationCurve_38(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_38 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_38, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_39() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___loopsDescriptionArea_39)); }
	inline String_t* get_loopsDescriptionArea_39() const { return ___loopsDescriptionArea_39; }
	inline String_t** get_address_of_loopsDescriptionArea_39() { return &___loopsDescriptionArea_39; }
	inline void set_loopsDescriptionArea_39(String_t* value)
	{
		___loopsDescriptionArea_39 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_39, value);
	}

	inline static int32_t get_offset_of_loops_40() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___loops_40)); }
	inline FsmInt_t1273009179 * get_loops_40() const { return ___loops_40; }
	inline FsmInt_t1273009179 ** get_address_of_loops_40() { return &___loops_40; }
	inline void set_loops_40(FsmInt_t1273009179 * value)
	{
		___loops_40 = value;
		Il2CppCodeGenWriteBarrier(&___loops_40, value);
	}

	inline static int32_t get_offset_of_loopType_41() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___loopType_41)); }
	inline int32_t get_loopType_41() const { return ___loopType_41; }
	inline int32_t* get_address_of_loopType_41() { return &___loopType_41; }
	inline void set_loopType_41(int32_t value)
	{
		___loopType_41 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_42() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___autoKillOnCompletion_42)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_42() const { return ___autoKillOnCompletion_42; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_42() { return &___autoKillOnCompletion_42; }
	inline void set_autoKillOnCompletion_42(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_42 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_42, value);
	}

	inline static int32_t get_offset_of_recyclable_43() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___recyclable_43)); }
	inline FsmBool_t664485696 * get_recyclable_43() const { return ___recyclable_43; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_43() { return &___recyclable_43; }
	inline void set_recyclable_43(FsmBool_t664485696 * value)
	{
		___recyclable_43 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_43, value);
	}

	inline static int32_t get_offset_of_updateType_44() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___updateType_44)); }
	inline int32_t get_updateType_44() const { return ___updateType_44; }
	inline int32_t* get_address_of_updateType_44() { return &___updateType_44; }
	inline void set_updateType_44(int32_t value)
	{
		___updateType_44 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_45() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___isIndependentUpdate_45)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_45() const { return ___isIndependentUpdate_45; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_45() { return &___isIndependentUpdate_45; }
	inline void set_isIndependentUpdate_45(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_45 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_45, value);
	}

	inline static int32_t get_offset_of_debugThis_46() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___debugThis_46)); }
	inline FsmBool_t664485696 * get_debugThis_46() const { return ___debugThis_46; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_46() { return &___debugThis_46; }
	inline void set_debugThis_46(FsmBool_t664485696 * value)
	{
		___debugThis_46 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_46, value);
	}

	inline static int32_t get_offset_of_tweener_47() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalPath_t2632274708, ___tweener_47)); }
	inline Tweener_t760404022 * get_tweener_47() const { return ___tweener_47; }
	inline Tweener_t760404022 ** get_address_of_tweener_47() { return &___tweener_47; }
	inline void set_tweener_47(Tweener_t760404022 * value)
	{
		___tweener_47 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
