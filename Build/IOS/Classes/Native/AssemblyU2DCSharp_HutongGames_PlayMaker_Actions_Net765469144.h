﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetLastPing
struct  NetworkGetLastPing_t765469144  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetLastPing::playerIndex
	FsmInt_t1273009179 * ___playerIndex_11;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetLastPing::cachePlayerReference
	bool ___cachePlayerReference_12;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetLastPing::everyFrame
	bool ___everyFrame_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetLastPing::lastPing
	FsmInt_t1273009179 * ___lastPing_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetLastPing::PlayerNotFoundEvent
	FsmEvent_t1258573736 * ___PlayerNotFoundEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetLastPing::PlayerFoundEvent
	FsmEvent_t1258573736 * ___PlayerFoundEvent_16;
	// UnityEngine.NetworkPlayer HutongGames.PlayMaker.Actions.NetworkGetLastPing::_player
	NetworkPlayer_t1243528291  ____player_17;

public:
	inline static int32_t get_offset_of_playerIndex_11() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ___playerIndex_11)); }
	inline FsmInt_t1273009179 * get_playerIndex_11() const { return ___playerIndex_11; }
	inline FsmInt_t1273009179 ** get_address_of_playerIndex_11() { return &___playerIndex_11; }
	inline void set_playerIndex_11(FsmInt_t1273009179 * value)
	{
		___playerIndex_11 = value;
		Il2CppCodeGenWriteBarrier(&___playerIndex_11, value);
	}

	inline static int32_t get_offset_of_cachePlayerReference_12() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ___cachePlayerReference_12)); }
	inline bool get_cachePlayerReference_12() const { return ___cachePlayerReference_12; }
	inline bool* get_address_of_cachePlayerReference_12() { return &___cachePlayerReference_12; }
	inline void set_cachePlayerReference_12(bool value)
	{
		___cachePlayerReference_12 = value;
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_lastPing_14() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ___lastPing_14)); }
	inline FsmInt_t1273009179 * get_lastPing_14() const { return ___lastPing_14; }
	inline FsmInt_t1273009179 ** get_address_of_lastPing_14() { return &___lastPing_14; }
	inline void set_lastPing_14(FsmInt_t1273009179 * value)
	{
		___lastPing_14 = value;
		Il2CppCodeGenWriteBarrier(&___lastPing_14, value);
	}

	inline static int32_t get_offset_of_PlayerNotFoundEvent_15() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ___PlayerNotFoundEvent_15)); }
	inline FsmEvent_t1258573736 * get_PlayerNotFoundEvent_15() const { return ___PlayerNotFoundEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_PlayerNotFoundEvent_15() { return &___PlayerNotFoundEvent_15; }
	inline void set_PlayerNotFoundEvent_15(FsmEvent_t1258573736 * value)
	{
		___PlayerNotFoundEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerNotFoundEvent_15, value);
	}

	inline static int32_t get_offset_of_PlayerFoundEvent_16() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ___PlayerFoundEvent_16)); }
	inline FsmEvent_t1258573736 * get_PlayerFoundEvent_16() const { return ___PlayerFoundEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_PlayerFoundEvent_16() { return &___PlayerFoundEvent_16; }
	inline void set_PlayerFoundEvent_16(FsmEvent_t1258573736 * value)
	{
		___PlayerFoundEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___PlayerFoundEvent_16, value);
	}

	inline static int32_t get_offset_of__player_17() { return static_cast<int32_t>(offsetof(NetworkGetLastPing_t765469144, ____player_17)); }
	inline NetworkPlayer_t1243528291  get__player_17() const { return ____player_17; }
	inline NetworkPlayer_t1243528291 * get_address_of__player_17() { return &____player_17; }
	inline void set__player_17(NetworkPlayer_t1243528291  value)
	{
		____player_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
