﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs
struct  NetworkSetMinimumAllocatableViewIDs_t2994535979  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::minimumViewIDs
	FsmInt_t1273009179 * ___minimumViewIDs_11;

public:
	inline static int32_t get_offset_of_minimumViewIDs_11() { return static_cast<int32_t>(offsetof(NetworkSetMinimumAllocatableViewIDs_t2994535979, ___minimumViewIDs_11)); }
	inline FsmInt_t1273009179 * get_minimumViewIDs_11() const { return ___minimumViewIDs_11; }
	inline FsmInt_t1273009179 ** get_address_of_minimumViewIDs_11() { return &___minimumViewIDs_11; }
	inline void set_minimumViewIDs_11(FsmInt_t1273009179 * value)
	{
		___minimumViewIDs_11 = value;
		Il2CppCodeGenWriteBarrier(&___minimumViewIDs_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
