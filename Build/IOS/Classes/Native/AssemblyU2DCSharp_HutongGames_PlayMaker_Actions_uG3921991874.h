﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Button
struct Button_t2872111280;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor
struct  uGuiSetButtonNormalColor_t3921991874  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::normalColor
	FsmColor_t118301965 * ___normalColor_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::resetOnExit
	bool ___resetOnExit_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::enabled
	FsmBool_t664485696 * ___enabled_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.UI.Button HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::_Button
	Button_t2872111280 * ____Button_16;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.uGuiSetButtonNormalColor::_OriginalNormalColor
	Color_t2020392075  ____OriginalNormalColor_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_normalColor_12() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ___normalColor_12)); }
	inline FsmColor_t118301965 * get_normalColor_12() const { return ___normalColor_12; }
	inline FsmColor_t118301965 ** get_address_of_normalColor_12() { return &___normalColor_12; }
	inline void set_normalColor_12(FsmColor_t118301965 * value)
	{
		___normalColor_12 = value;
		Il2CppCodeGenWriteBarrier(&___normalColor_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ___resetOnExit_13)); }
	inline bool get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline bool* get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(bool value)
	{
		___resetOnExit_13 = value;
	}

	inline static int32_t get_offset_of_enabled_14() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ___enabled_14)); }
	inline FsmBool_t664485696 * get_enabled_14() const { return ___enabled_14; }
	inline FsmBool_t664485696 ** get_address_of_enabled_14() { return &___enabled_14; }
	inline void set_enabled_14(FsmBool_t664485696 * value)
	{
		___enabled_14 = value;
		Il2CppCodeGenWriteBarrier(&___enabled_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of__Button_16() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ____Button_16)); }
	inline Button_t2872111280 * get__Button_16() const { return ____Button_16; }
	inline Button_t2872111280 ** get_address_of__Button_16() { return &____Button_16; }
	inline void set__Button_16(Button_t2872111280 * value)
	{
		____Button_16 = value;
		Il2CppCodeGenWriteBarrier(&____Button_16, value);
	}

	inline static int32_t get_offset_of__OriginalNormalColor_17() { return static_cast<int32_t>(offsetof(uGuiSetButtonNormalColor_t3921991874, ____OriginalNormalColor_17)); }
	inline Color_t2020392075  get__OriginalNormalColor_17() const { return ____OriginalNormalColor_17; }
	inline Color_t2020392075 * get_address_of__OriginalNormalColor_17() { return &____OriginalNormalColor_17; }
	inline void set__OriginalNormalColor_17(Color_t2020392075  value)
	{
		____OriginalNormalColor_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
