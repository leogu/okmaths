﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor118301965.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.Color HutongGames.PlayMaker.FsmColor::get_Value()
extern "C"  Color_t2020392075  FsmColor_get_Value_m687626399 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::set_Value(UnityEngine.Color)
extern "C"  void FsmColor_set_Value_m92994086 (FsmColor_t118301965 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmColor::get_RawValue()
extern "C"  Il2CppObject * FsmColor_get_RawValue_m3256255195 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::set_RawValue(System.Object)
extern "C"  void FsmColor_set_RawValue_m2306891508 (FsmColor_t118301965 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::.ctor()
extern "C"  void FsmColor__ctor_m1940543644 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::.ctor(System.String)
extern "C"  void FsmColor__ctor_m2534427510 (FsmColor_t118301965 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::.ctor(HutongGames.PlayMaker.FsmColor)
extern "C"  void FsmColor__ctor_m1773797051 (FsmColor_t118301965 * __this, FsmColor_t118301965 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmColor::Clone()
extern "C"  NamedVariable_t3026441313 * FsmColor_Clone_m305124211 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmColor::get_VariableType()
extern "C"  int32_t FsmColor_get_VariableType_m1082909086 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmColor::ToString()
extern "C"  String_t* FsmColor_ToString_m3649759539 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmColor::op_Implicit(UnityEngine.Color)
extern "C"  FsmColor_t118301965 * FsmColor_op_Implicit_m1499113597 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
