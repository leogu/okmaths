﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction
struct BaseFsmVariableIndexAction_t3235583395;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::.ctor()
extern "C"  void BaseFsmVariableIndexAction__ctor_m305525353 (BaseFsmVariableIndexAction_t3235583395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::Reset()
extern "C"  void BaseFsmVariableIndexAction_Reset_m2878245712 (BaseFsmVariableIndexAction_t3235583395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::UpdateCache(UnityEngine.GameObject,System.String)
extern "C"  bool BaseFsmVariableIndexAction_UpdateCache_m3540055838 (BaseFsmVariableIndexAction_t3235583395 * __this, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableIndexAction::DoVariableNotFound(System.String)
extern "C"  void BaseFsmVariableIndexAction_DoVariableNotFound_m1393183601 (BaseFsmVariableIndexAction_t3235583395 * __this, String_t* ___variableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
