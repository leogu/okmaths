﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectContains
struct RectContains_t3461006553;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectContains::.ctor()
extern "C"  void RectContains__ctor_m1575816537 (RectContains_t3461006553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::Reset()
extern "C"  void RectContains_Reset_m3298359522 (RectContains_t3461006553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::OnEnter()
extern "C"  void RectContains_OnEnter_m2163968680 (RectContains_t3461006553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::OnUpdate()
extern "C"  void RectContains_OnUpdate_m637108069 (RectContains_t3461006553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectContains::DoRectContains()
extern "C"  void RectContains_DoRectContains_m1800739905 (RectContains_t3461006553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
