﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.UpdateHelper
struct UpdateHelper_t2226607427;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"

// System.Void HutongGames.PlayMaker.UpdateHelper::.ctor()
extern "C"  void UpdateHelper__ctor_m100414094 (UpdateHelper_t2226607427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.UpdateHelper::SetDirty(HutongGames.PlayMaker.Fsm)
extern "C"  void UpdateHelper_SetDirty_m4083663442 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
