﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion
struct GetQuaternionMultipliedByQuaternion_t2590305934;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::.ctor()
extern "C"  void GetQuaternionMultipliedByQuaternion__ctor_m4197413498 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::Reset()
extern "C"  void GetQuaternionMultipliedByQuaternion_Reset_m4051731015 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnEnter()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnEnter_m2617732447 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnUpdate_m2408943988 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnLateUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnLateUpdate_m3881699936 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::OnFixedUpdate()
extern "C"  void GetQuaternionMultipliedByQuaternion_OnFixedUpdate_m4193110638 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionMultipliedByQuaternion::DoQuatMult()
extern "C"  void GetQuaternionMultipliedByQuaternion_DoQuatMult_m2246262444 (GetQuaternionMultipliedByQuaternion_t2590305934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
