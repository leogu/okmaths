﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers
struct  uGuiSliderGetWholeNumbers_t2820703578  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::wholeNumbers
	FsmBool_t664485696 * ___wholeNumbers_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::isShowingWholeNumbersEvent
	FsmEvent_t1258573736 * ___isShowingWholeNumbersEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::isNotShowingWholeNumbersEvent
	FsmEvent_t1258573736 * ___isNotShowingWholeNumbersEvent_14;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderGetWholeNumbers::_slider
	Slider_t297367283 * ____slider_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderGetWholeNumbers_t2820703578, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_wholeNumbers_12() { return static_cast<int32_t>(offsetof(uGuiSliderGetWholeNumbers_t2820703578, ___wholeNumbers_12)); }
	inline FsmBool_t664485696 * get_wholeNumbers_12() const { return ___wholeNumbers_12; }
	inline FsmBool_t664485696 ** get_address_of_wholeNumbers_12() { return &___wholeNumbers_12; }
	inline void set_wholeNumbers_12(FsmBool_t664485696 * value)
	{
		___wholeNumbers_12 = value;
		Il2CppCodeGenWriteBarrier(&___wholeNumbers_12, value);
	}

	inline static int32_t get_offset_of_isShowingWholeNumbersEvent_13() { return static_cast<int32_t>(offsetof(uGuiSliderGetWholeNumbers_t2820703578, ___isShowingWholeNumbersEvent_13)); }
	inline FsmEvent_t1258573736 * get_isShowingWholeNumbersEvent_13() const { return ___isShowingWholeNumbersEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isShowingWholeNumbersEvent_13() { return &___isShowingWholeNumbersEvent_13; }
	inline void set_isShowingWholeNumbersEvent_13(FsmEvent_t1258573736 * value)
	{
		___isShowingWholeNumbersEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isShowingWholeNumbersEvent_13, value);
	}

	inline static int32_t get_offset_of_isNotShowingWholeNumbersEvent_14() { return static_cast<int32_t>(offsetof(uGuiSliderGetWholeNumbers_t2820703578, ___isNotShowingWholeNumbersEvent_14)); }
	inline FsmEvent_t1258573736 * get_isNotShowingWholeNumbersEvent_14() const { return ___isNotShowingWholeNumbersEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotShowingWholeNumbersEvent_14() { return &___isNotShowingWholeNumbersEvent_14; }
	inline void set_isNotShowingWholeNumbersEvent_14(FsmEvent_t1258573736 * value)
	{
		___isNotShowingWholeNumbersEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isNotShowingWholeNumbersEvent_14, value);
	}

	inline static int32_t get_offset_of__slider_15() { return static_cast<int32_t>(offsetof(uGuiSliderGetWholeNumbers_t2820703578, ____slider_15)); }
	inline Slider_t297367283 * get__slider_15() const { return ____slider_15; }
	inline Slider_t297367283 ** get_address_of__slider_15() { return &____slider_15; }
	inline void set__slider_15(Slider_t297367283 * value)
	{
		____slider_15 = value;
		Il2CppCodeGenWriteBarrier(&____slider_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
