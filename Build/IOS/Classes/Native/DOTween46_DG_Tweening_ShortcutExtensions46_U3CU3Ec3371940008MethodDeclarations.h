﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t3371940008;

#include "codegen/il2cpp-codegen.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m2085635913 (U3CU3Ec__DisplayClass5_0_t3371940008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern "C"  float U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m1489628791 (U3CU3Ec__DisplayClass5_0_t3371940008 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m1563959817 (U3CU3Ec__DisplayClass5_0_t3371940008 * __this, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
