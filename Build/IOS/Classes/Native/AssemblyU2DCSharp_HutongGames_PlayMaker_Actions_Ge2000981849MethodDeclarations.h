﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorBool
struct GetAnimatorBool_t2000981849;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::.ctor()
extern "C"  void GetAnimatorBool__ctor_m1890007345 (GetAnimatorBool_t2000981849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::Reset()
extern "C"  void GetAnimatorBool_Reset_m1720250830 (GetAnimatorBool_t2000981849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnEnter()
extern "C"  void GetAnimatorBool_OnEnter_m2459043188 (GetAnimatorBool_t2000981849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::OnActionUpdate()
extern "C"  void GetAnimatorBool_OnActionUpdate_m1498548543 (GetAnimatorBool_t2000981849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBool::GetParameter()
extern "C"  void GetAnimatorBool_GetParameter_m3763214322 (GetAnimatorBool_t2000981849 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
