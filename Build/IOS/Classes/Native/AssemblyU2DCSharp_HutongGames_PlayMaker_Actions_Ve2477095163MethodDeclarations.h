﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3ClampMagnitude
struct Vector3ClampMagnitude_t2477095163;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::.ctor()
extern "C"  void Vector3ClampMagnitude__ctor_m2221948167 (Vector3ClampMagnitude_t2477095163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::Reset()
extern "C"  void Vector3ClampMagnitude_Reset_m2052187196 (Vector3ClampMagnitude_t2477095163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::OnEnter()
extern "C"  void Vector3ClampMagnitude_OnEnter_m528737498 (Vector3ClampMagnitude_t2477095163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::OnUpdate()
extern "C"  void Vector3ClampMagnitude_OnUpdate_m1726222599 (Vector3ClampMagnitude_t2477095163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3ClampMagnitude::DoVector3ClampMagnitude()
extern "C"  void Vector3ClampMagnitude_DoVector3ClampMagnitude_m1524409949 (Vector3ClampMagnitude_t2477095163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
