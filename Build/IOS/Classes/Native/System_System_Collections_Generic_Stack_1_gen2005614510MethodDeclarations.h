﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen3777177449MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::.ctor()
#define Stack_1__ctor_m3381430758(__this, method) ((  void (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::.ctor(System.Int32)
#define Stack_1__ctor_m103881454(__this, ___count0, method) ((  void (*) (Stack_1_t2005614510 *, int32_t, const MethodInfo*))Stack_1__ctor_m4173684663_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m2209809502(__this, method) ((  bool (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m2076161108_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m309721176(__this, method) ((  Il2CppObject * (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m3151629354_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m139334562(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t2005614510 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m2104527616_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3897517008(__this, method) ((  Il2CppObject* (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m680979874_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m1935158727(__this, method) ((  Il2CppObject * (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m3875192475_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Clear()
#define Stack_1_Clear_m4038960527(__this, method) ((  void (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_Clear_m3792598883_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Peek()
#define Stack_1_Peek_m1840985828(__this, method) ((  Fsm_t917886356 * (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_Peek_m1548778538_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Pop()
#define Stack_1_Pop_m1914303704(__this, method) ((  Fsm_t917886356 * (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_Pop_m535185982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::Push(T)
#define Stack_1_Push_m966230828(__this, ___t0, method) ((  void (*) (Stack_1_t2005614510 *, Fsm_t917886356 *, const MethodInfo*))Stack_1_Push_m2122392216_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::get_Count()
#define Stack_1_get_Count_m366203871(__this, method) ((  int32_t (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<HutongGames.PlayMaker.Fsm>::GetEnumerator()
#define Stack_1_GetEnumerator_m240728912(__this, method) ((  Enumerator_t2655612870  (*) (Stack_1_t2005614510 *, const MethodInfo*))Stack_1_GetEnumerator_m287848754_gshared)(__this, method)
