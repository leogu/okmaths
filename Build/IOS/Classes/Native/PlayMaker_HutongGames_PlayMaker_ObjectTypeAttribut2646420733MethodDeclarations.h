﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ObjectTypeAttribute
struct ObjectTypeAttribute_t2646420733;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Type HutongGames.PlayMaker.ObjectTypeAttribute::get_ObjectType()
extern "C"  Type_t * ObjectTypeAttribute_get_ObjectType_m565938262 (ObjectTypeAttribute_t2646420733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ObjectTypeAttribute::.ctor(System.Type)
extern "C"  void ObjectTypeAttribute__ctor_m3751498735 (ObjectTypeAttribute_t2646420733 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
