﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveY
struct DOTweenRigidbodyMoveY_t684693157;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveY::.ctor()
extern "C"  void DOTweenRigidbodyMoveY__ctor_m2121083993 (DOTweenRigidbodyMoveY_t684693157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveY::Reset()
extern "C"  void DOTweenRigidbodyMoveY_Reset_m127398782 (DOTweenRigidbodyMoveY_t684693157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveY::OnEnter()
extern "C"  void DOTweenRigidbodyMoveY_OnEnter_m1974923860 (DOTweenRigidbodyMoveY_t684693157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveY::<OnEnter>m__8C()
extern "C"  void DOTweenRigidbodyMoveY_U3COnEnterU3Em__8C_m4115973734 (DOTweenRigidbodyMoveY_t684693157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveY::<OnEnter>m__8D()
extern "C"  void DOTweenRigidbodyMoveY_U3COnEnterU3Em__8D_m4115973631 (DOTweenRigidbodyMoveY_t684693157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
