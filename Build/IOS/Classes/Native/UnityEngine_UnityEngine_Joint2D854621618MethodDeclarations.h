﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Joint2D
struct Joint2D_t854621618;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Joint2D854621618.h"

// UnityEngine.Vector2 UnityEngine.Joint2D::get_reactionForce()
extern "C"  Vector2_t2243707579  Joint2D_get_reactionForce_m827344471 (Joint2D_t854621618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::get_reactionTorque()
extern "C"  float Joint2D_get_reactionTorque_m471673395 (Joint2D_t854621618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Joint2D::GetReactionForce(System.Single)
extern "C"  Vector2_t2243707579  Joint2D_GetReactionForce_m459249225 (Joint2D_t854621618 * __this, float ___timeStep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::Joint2D_CUSTOM_INTERNAL_GetReactionForce(UnityEngine.Joint2D,System.Single,UnityEngine.Vector2&)
extern "C"  void Joint2D_Joint2D_CUSTOM_INTERNAL_GetReactionForce_m3430877202 (Il2CppObject * __this /* static, unused */, Joint2D_t854621618 * ___joint0, float ___timeStep1, Vector2_t2243707579 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::GetReactionTorque(System.Single)
extern "C"  float Joint2D_GetReactionTorque_m2168035725 (Joint2D_t854621618 * __this, float ___timeStep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::INTERNAL_CALL_GetReactionTorque(UnityEngine.Joint2D,System.Single)
extern "C"  float Joint2D_INTERNAL_CALL_GetReactionTorque_m1521667231 (Il2CppObject * __this /* static, unused */, Joint2D_t854621618 * ___self0, float ___timeStep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
