﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// UnityEngine.Material
struct Material_t193706927;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject2785794313.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_Type1303803226.h"

// UnityEngine.Material HutongGames.PlayMaker.FsmMaterial::get_Value()
extern "C"  Material_t193706927 * FsmMaterial_get_Value_m361239091 (FsmMaterial_t1421632035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::set_Value(UnityEngine.Material)
extern "C"  void FsmMaterial_set_Value_m4198295470 (FsmMaterial_t1421632035 * __this, Material_t193706927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor()
extern "C"  void FsmMaterial__ctor_m3208408158 (FsmMaterial_t1421632035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor(System.String)
extern "C"  void FsmMaterial__ctor_m2289599016 (FsmMaterial_t1421632035 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmMaterial::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmMaterial__ctor_m2380653685 (FsmMaterial_t1421632035 * __this, FsmObject_t2785794313 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmMaterial::Clone()
extern "C"  NamedVariable_t3026441313 * FsmMaterial_Clone_m4057668221 (FsmMaterial_t1421632035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmMaterial::get_VariableType()
extern "C"  int32_t FsmMaterial_get_VariableType_m439805202 (FsmMaterial_t1421632035 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmMaterial::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmMaterial_TestTypeConstraint_m1941663356 (FsmMaterial_t1421632035 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
