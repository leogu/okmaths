﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetEnumValue
struct SetEnumValue_t4139640040;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::.ctor()
extern "C"  void SetEnumValue__ctor_m1566203446 (SetEnumValue_t4139640040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::Reset()
extern "C"  void SetEnumValue_Reset_m496593229 (SetEnumValue_t4139640040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::OnEnter()
extern "C"  void SetEnumValue_OnEnter_m1310196917 (SetEnumValue_t4139640040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::OnUpdate()
extern "C"  void SetEnumValue_OnUpdate_m3216768856 (SetEnumValue_t4139640040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEnumValue::DoSetEnumValue()
extern "C"  void SetEnumValue_DoSetEnumValue_m4148798529 (SetEnumValue_t4139640040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
