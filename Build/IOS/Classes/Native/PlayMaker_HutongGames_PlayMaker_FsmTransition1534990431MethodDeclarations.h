﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust3741871820.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust2686433720.h"

// System.Void HutongGames.PlayMaker.FsmTransition::.ctor()
extern "C"  void FsmTransition__ctor_m656222936 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::.ctor(HutongGames.PlayMaker.FsmTransition)
extern "C"  void FsmTransition__ctor_m3679252663 (FsmTransition_t1534990431 * __this, FsmTransition_t1534990431 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmTransition::get_FsmEvent()
extern "C"  FsmEvent_t1258573736 * FsmTransition_get_FsmEvent_m3818892412 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_FsmEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmTransition_set_FsmEvent_m340477577 (FsmTransition_t1534990431 * __this, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmTransition::get_ToState()
extern "C"  String_t* FsmTransition_get_ToState_m3210519480 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_ToState(System.String)
extern "C"  void FsmTransition_set_ToState_m3220495481 (FsmTransition_t1534990431 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition/CustomLinkStyle HutongGames.PlayMaker.FsmTransition::get_LinkStyle()
extern "C"  uint8_t FsmTransition_get_LinkStyle_m3742091449 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_LinkStyle(HutongGames.PlayMaker.FsmTransition/CustomLinkStyle)
extern "C"  void FsmTransition_set_LinkStyle_m4168212980 (FsmTransition_t1534990431 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint HutongGames.PlayMaker.FsmTransition::get_LinkConstraint()
extern "C"  uint8_t FsmTransition_get_LinkConstraint_m1528446177 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_LinkConstraint(HutongGames.PlayMaker.FsmTransition/CustomLinkConstraint)
extern "C"  void FsmTransition_set_LinkConstraint_m1317153556 (FsmTransition_t1534990431 * __this, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmTransition::get_ColorIndex()
extern "C"  int32_t FsmTransition_get_ColorIndex_m2822953232 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTransition::set_ColorIndex(System.Int32)
extern "C"  void FsmTransition_set_ColorIndex_m4159467041 (FsmTransition_t1534990431 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmTransition::get_EventName()
extern "C"  String_t* FsmTransition_get_EventName_m334839757 (FsmTransition_t1534990431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmTransition::Equals(HutongGames.PlayMaker.FsmTransition)
extern "C"  bool FsmTransition_Equals_m4251063380 (FsmTransition_t1534990431 * __this, FsmTransition_t1534990431 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
