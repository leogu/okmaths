﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSort
struct ArrayListSort_t2392362357;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSort::.ctor()
extern "C"  void ArrayListSort__ctor_m1040760231 (ArrayListSort_t2392362357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSort::Reset()
extern "C"  void ArrayListSort_Reset_m2773859658 (ArrayListSort_t2392362357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSort::OnEnter()
extern "C"  void ArrayListSort_OnEnter_m613852604 (ArrayListSort_t2392362357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSort::DoArrayListSort()
extern "C"  void ArrayListSort_DoArrayListSort_m2364061933 (ArrayListSort_t2392362357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
