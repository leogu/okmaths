﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetShadowStrength
struct SetShadowStrength_t628635369;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::.ctor()
extern "C"  void SetShadowStrength__ctor_m414945687 (SetShadowStrength_t628635369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::Reset()
extern "C"  void SetShadowStrength_Reset_m1037721354 (SetShadowStrength_t628635369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::OnEnter()
extern "C"  void SetShadowStrength_OnEnter_m3150235652 (SetShadowStrength_t628635369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::OnUpdate()
extern "C"  void SetShadowStrength_OnUpdate_m3228613135 (SetShadowStrength_t628635369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetShadowStrength::DoSetShadowStrength()
extern "C"  void SetShadowStrength_DoSetShadowStrength_m1718224493 (SetShadowStrength_t628635369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
