﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Operator
struct Vector2Operator_t3983782915;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::.ctor()
extern "C"  void Vector2Operator__ctor_m3979245061 (Vector2Operator_t3983782915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::Reset()
extern "C"  void Vector2Operator_Reset_m2012632792 (Vector2Operator_t3983782915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::OnEnter()
extern "C"  void Vector2Operator_OnEnter_m1901647194 (Vector2Operator_t3983782915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::OnUpdate()
extern "C"  void Vector2Operator_OnUpdate_m943082105 (Vector2Operator_t3983782915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Operator::DoVector2Operator()
extern "C"  void Vector2Operator_DoVector2Operator_m3125197953 (Vector2Operator_t3983782915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
