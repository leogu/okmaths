﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers
struct  uGuiSetAnimationTriggers_t2640876623  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::normalTrigger
	FsmString_t2414474701 * ___normalTrigger_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::highlightedTrigger
	FsmString_t2414474701 * ___highlightedTrigger_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::pressedTrigger
	FsmString_t2414474701 * ___pressedTrigger_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::disabledTrigger
	FsmString_t2414474701 * ___disabledTrigger_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_16;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::_selectable
	Selectable_t1490392188 * ____selectable_17;
	// UnityEngine.UI.AnimationTriggers HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::_animationTriggers
	AnimationTriggers_t3244928895 * ____animationTriggers_18;
	// UnityEngine.UI.AnimationTriggers HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::_originalAnimationTriggers
	AnimationTriggers_t3244928895 * ____originalAnimationTriggers_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_normalTrigger_12() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ___normalTrigger_12)); }
	inline FsmString_t2414474701 * get_normalTrigger_12() const { return ___normalTrigger_12; }
	inline FsmString_t2414474701 ** get_address_of_normalTrigger_12() { return &___normalTrigger_12; }
	inline void set_normalTrigger_12(FsmString_t2414474701 * value)
	{
		___normalTrigger_12 = value;
		Il2CppCodeGenWriteBarrier(&___normalTrigger_12, value);
	}

	inline static int32_t get_offset_of_highlightedTrigger_13() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ___highlightedTrigger_13)); }
	inline FsmString_t2414474701 * get_highlightedTrigger_13() const { return ___highlightedTrigger_13; }
	inline FsmString_t2414474701 ** get_address_of_highlightedTrigger_13() { return &___highlightedTrigger_13; }
	inline void set_highlightedTrigger_13(FsmString_t2414474701 * value)
	{
		___highlightedTrigger_13 = value;
		Il2CppCodeGenWriteBarrier(&___highlightedTrigger_13, value);
	}

	inline static int32_t get_offset_of_pressedTrigger_14() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ___pressedTrigger_14)); }
	inline FsmString_t2414474701 * get_pressedTrigger_14() const { return ___pressedTrigger_14; }
	inline FsmString_t2414474701 ** get_address_of_pressedTrigger_14() { return &___pressedTrigger_14; }
	inline void set_pressedTrigger_14(FsmString_t2414474701 * value)
	{
		___pressedTrigger_14 = value;
		Il2CppCodeGenWriteBarrier(&___pressedTrigger_14, value);
	}

	inline static int32_t get_offset_of_disabledTrigger_15() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ___disabledTrigger_15)); }
	inline FsmString_t2414474701 * get_disabledTrigger_15() const { return ___disabledTrigger_15; }
	inline FsmString_t2414474701 ** get_address_of_disabledTrigger_15() { return &___disabledTrigger_15; }
	inline void set_disabledTrigger_15(FsmString_t2414474701 * value)
	{
		___disabledTrigger_15 = value;
		Il2CppCodeGenWriteBarrier(&___disabledTrigger_15, value);
	}

	inline static int32_t get_offset_of_resetOnExit_16() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ___resetOnExit_16)); }
	inline FsmBool_t664485696 * get_resetOnExit_16() const { return ___resetOnExit_16; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_16() { return &___resetOnExit_16; }
	inline void set_resetOnExit_16(FsmBool_t664485696 * value)
	{
		___resetOnExit_16 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_16, value);
	}

	inline static int32_t get_offset_of__selectable_17() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ____selectable_17)); }
	inline Selectable_t1490392188 * get__selectable_17() const { return ____selectable_17; }
	inline Selectable_t1490392188 ** get_address_of__selectable_17() { return &____selectable_17; }
	inline void set__selectable_17(Selectable_t1490392188 * value)
	{
		____selectable_17 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_17, value);
	}

	inline static int32_t get_offset_of__animationTriggers_18() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ____animationTriggers_18)); }
	inline AnimationTriggers_t3244928895 * get__animationTriggers_18() const { return ____animationTriggers_18; }
	inline AnimationTriggers_t3244928895 ** get_address_of__animationTriggers_18() { return &____animationTriggers_18; }
	inline void set__animationTriggers_18(AnimationTriggers_t3244928895 * value)
	{
		____animationTriggers_18 = value;
		Il2CppCodeGenWriteBarrier(&____animationTriggers_18, value);
	}

	inline static int32_t get_offset_of__originalAnimationTriggers_19() { return static_cast<int32_t>(offsetof(uGuiSetAnimationTriggers_t2640876623, ____originalAnimationTriggers_19)); }
	inline AnimationTriggers_t3244928895 * get__originalAnimationTriggers_19() const { return ____originalAnimationTriggers_19; }
	inline AnimationTriggers_t3244928895 ** get_address_of__originalAnimationTriggers_19() { return &____originalAnimationTriggers_19; }
	inline void set__originalAnimationTriggers_19(AnimationTriggers_t3244928895 * value)
	{
		____originalAnimationTriggers_19 = value;
		Il2CppCodeGenWriteBarrier(&____originalAnimationTriggers_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
