﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseButtonDown
struct GetMouseButtonDown_t3550553713;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::.ctor()
extern "C"  void GetMouseButtonDown__ctor_m2265996581 (GetMouseButtonDown_t3550553713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::Reset()
extern "C"  void GetMouseButtonDown_Reset_m2454290110 (GetMouseButtonDown_t3550553713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::OnEnter()
extern "C"  void GetMouseButtonDown_OnEnter_m1044682620 (GetMouseButtonDown_t3550553713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::OnUpdate()
extern "C"  void GetMouseButtonDown_OnUpdate_m4129528817 (GetMouseButtonDown_t3550553713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButtonDown::DoGetMouseButtonDown()
extern "C"  void GetMouseButtonDown_DoGetMouseButtonDown_m4162293185 (GetMouseButtonDown_t3550553713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
