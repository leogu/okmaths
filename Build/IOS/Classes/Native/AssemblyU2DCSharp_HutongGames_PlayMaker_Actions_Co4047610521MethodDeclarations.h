﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerSimpleMove
struct ControllerSimpleMove_t4047610521;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerSimpleMove::.ctor()
extern "C"  void ControllerSimpleMove__ctor_m4014115569 (ControllerSimpleMove_t4047610521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSimpleMove::Reset()
extern "C"  void ControllerSimpleMove_Reset_m3184027338 (ControllerSimpleMove_t4047610521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerSimpleMove::OnUpdate()
extern "C"  void ControllerSimpleMove_OnUpdate_m2918196581 (ControllerSimpleMove_t4047610521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
