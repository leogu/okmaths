﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Voxcat.SceneMan.Controller.LoadingSceneController
struct LoadingSceneController_t1917474958;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadRequestedScene
struct  LoadRequestedScene_t2989246692  : public MonoBehaviour_t1158329972
{
public:
	// Voxcat.SceneMan.Controller.LoadingSceneController LoadRequestedScene::loadingSC
	LoadingSceneController_t1917474958 * ___loadingSC_2;

public:
	inline static int32_t get_offset_of_loadingSC_2() { return static_cast<int32_t>(offsetof(LoadRequestedScene_t2989246692, ___loadingSC_2)); }
	inline LoadingSceneController_t1917474958 * get_loadingSC_2() const { return ___loadingSC_2; }
	inline LoadingSceneController_t1917474958 ** get_address_of_loadingSC_2() { return &___loadingSC_2; }
	inline void set_loadingSC_2(LoadingSceneController_t1917474958 * value)
	{
		___loadingSC_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadingSC_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
