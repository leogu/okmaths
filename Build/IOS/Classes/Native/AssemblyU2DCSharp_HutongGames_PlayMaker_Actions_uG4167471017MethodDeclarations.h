﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetPlaceHolder
struct uGuiInputFieldSetPlaceHolder_t4167471017;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetPlaceHolder::.ctor()
extern "C"  void uGuiInputFieldSetPlaceHolder__ctor_m905406229 (uGuiInputFieldSetPlaceHolder_t4167471017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetPlaceHolder::Reset()
extern "C"  void uGuiInputFieldSetPlaceHolder_Reset_m2346942350 (uGuiInputFieldSetPlaceHolder_t4167471017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetPlaceHolder::OnEnter()
extern "C"  void uGuiInputFieldSetPlaceHolder_OnEnter_m1874730676 (uGuiInputFieldSetPlaceHolder_t4167471017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetPlaceHolder::DoSetValue()
extern "C"  void uGuiInputFieldSetPlaceHolder_DoSetValue_m2648252947 (uGuiInputFieldSetPlaceHolder_t4167471017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetPlaceHolder::OnExit()
extern "C"  void uGuiInputFieldSetPlaceHolder_OnExit_m1885924904 (uGuiInputFieldSetPlaceHolder_t4167471017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
