﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkViewIsMine
struct NetworkViewIsMine_t2117354728;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::.ctor()
extern "C"  void NetworkViewIsMine__ctor_m2981709558 (NetworkViewIsMine_t2117354728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::_getNetworkView()
extern "C"  void NetworkViewIsMine__getNetworkView_m3943889984 (NetworkViewIsMine_t2117354728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::Reset()
extern "C"  void NetworkViewIsMine_Reset_m1138664161 (NetworkViewIsMine_t2117354728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::OnEnter()
extern "C"  void NetworkViewIsMine_OnEnter_m1176420177 (NetworkViewIsMine_t2117354728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewIsMine::checkIsMine()
extern "C"  void NetworkViewIsMine_checkIsMine_m2305165639 (NetworkViewIsMine_t2117354728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
