﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArraySet
struct ArraySet_t1522161601;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArraySet::.ctor()
extern "C"  void ArraySet__ctor_m3825625961 (ArraySet_t1522161601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::Reset()
extern "C"  void ArraySet_Reset_m2380565906 (ArraySet_t1522161601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::OnEnter()
extern "C"  void ArraySet_OnEnter_m3655910464 (ArraySet_t1522161601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::OnUpdate()
extern "C"  void ArraySet_OnUpdate_m2034262493 (ArraySet_t1522161601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySet::DoGetValue()
extern "C"  void ArraySet_DoGetValue_m3629601751 (ArraySet_t1522161601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
