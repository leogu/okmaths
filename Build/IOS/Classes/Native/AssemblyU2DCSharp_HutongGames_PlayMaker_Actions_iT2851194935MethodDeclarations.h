﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenLookFrom
struct iTweenLookFrom_t2851194935;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::.ctor()
extern "C"  void iTweenLookFrom__ctor_m921069107 (iTweenLookFrom_t2851194935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::Reset()
extern "C"  void iTweenLookFrom_Reset_m1995045988 (iTweenLookFrom_t2851194935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::OnEnter()
extern "C"  void iTweenLookFrom_OnEnter_m975142850 (iTweenLookFrom_t2851194935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::OnExit()
extern "C"  void iTweenLookFrom_OnExit_m3874746442 (iTweenLookFrom_t2851194935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookFrom::DoiTween()
extern "C"  void iTweenLookFrom_DoiTween_m559051416 (iTweenLookFrom_t2851194935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
