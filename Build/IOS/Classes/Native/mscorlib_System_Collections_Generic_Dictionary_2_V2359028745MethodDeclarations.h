﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>
struct ValueCollection_t2359028745;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3655968902;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit2D>
struct IEnumerator_1_t1539432601;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1047534370.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1875345419_gshared (ValueCollection_t2359028745 * __this, Dictionary_2_t3655968902 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1875345419(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2359028745 *, Dictionary_2_t3655968902 *, const MethodInfo*))ValueCollection__ctor_m1875345419_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m835338641_gshared (ValueCollection_t2359028745 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m835338641(__this, ___item0, method) ((  void (*) (ValueCollection_t2359028745 *, RaycastHit2D_t4063908774 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m835338641_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m560067856_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m560067856(__this, method) ((  void (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m560067856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875449127_gshared (ValueCollection_t2359028745 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875449127(__this, ___item0, method) ((  bool (*) (ValueCollection_t2359028745 *, RaycastHit2D_t4063908774 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875449127_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698227602_gshared (ValueCollection_t2359028745 * __this, RaycastHit2D_t4063908774  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698227602(__this, ___item0, method) ((  bool (*) (ValueCollection_t2359028745 *, RaycastHit2D_t4063908774 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698227602_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1327821650_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1327821650(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1327821650_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1087717004_gshared (ValueCollection_t2359028745 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1087717004(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2359028745 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1087717004_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2795042721_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2795042721(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2795042721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1278818006_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1278818006(__this, method) ((  bool (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1278818006_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2686570292_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2686570292(__this, method) ((  bool (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2686570292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1507176018_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1507176018(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1507176018_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m1572297112_gshared (ValueCollection_t2359028745 * __this, RaycastHit2DU5BU5D_t4176517891* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m1572297112(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2359028745 *, RaycastHit2DU5BU5D_t4176517891*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1572297112_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Enumerator_t1047534370  ValueCollection_GetEnumerator_m3932942483_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m3932942483(__this, method) ((  Enumerator_t1047534370  (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_GetEnumerator_m3932942483_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m2014429424_gshared (ValueCollection_t2359028745 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m2014429424(__this, method) ((  int32_t (*) (ValueCollection_t2359028745 *, const MethodInfo*))ValueCollection_get_Count_m2014429424_gshared)(__this, method)
