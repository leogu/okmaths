﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn
struct  uGuiToggleSetIsOn_t2261003551  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::isOn
	FsmBool_t664485696 * ___isOn_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::_toggle
	Toggle_t3976754468 * ____toggle_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiToggleSetIsOn::_originalValue
	bool ____originalValue_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiToggleSetIsOn_t2261003551, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isOn_12() { return static_cast<int32_t>(offsetof(uGuiToggleSetIsOn_t2261003551, ___isOn_12)); }
	inline FsmBool_t664485696 * get_isOn_12() const { return ___isOn_12; }
	inline FsmBool_t664485696 ** get_address_of_isOn_12() { return &___isOn_12; }
	inline void set_isOn_12(FsmBool_t664485696 * value)
	{
		___isOn_12 = value;
		Il2CppCodeGenWriteBarrier(&___isOn_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiToggleSetIsOn_t2261003551, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__toggle_14() { return static_cast<int32_t>(offsetof(uGuiToggleSetIsOn_t2261003551, ____toggle_14)); }
	inline Toggle_t3976754468 * get__toggle_14() const { return ____toggle_14; }
	inline Toggle_t3976754468 ** get_address_of__toggle_14() { return &____toggle_14; }
	inline void set__toggle_14(Toggle_t3976754468 * value)
	{
		____toggle_14 = value;
		Il2CppCodeGenWriteBarrier(&____toggle_14, value);
	}

	inline static int32_t get_offset_of__originalValue_15() { return static_cast<int32_t>(offsetof(uGuiToggleSetIsOn_t2261003551, ____originalValue_15)); }
	inline bool get__originalValue_15() const { return ____originalValue_15; }
	inline bool* get_address_of__originalValue_15() { return &____originalValue_15; }
	inline void set__originalValue_15(bool value)
	{
		____originalValue_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
