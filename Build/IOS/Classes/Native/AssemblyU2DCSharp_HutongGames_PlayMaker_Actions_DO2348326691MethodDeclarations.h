﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformScaleY
struct DOTweenTransformScaleY_t2348326691;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleY::.ctor()
extern "C"  void DOTweenTransformScaleY__ctor_m1151870313 (DOTweenTransformScaleY_t2348326691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleY::Reset()
extern "C"  void DOTweenTransformScaleY_Reset_m1951607504 (DOTweenTransformScaleY_t2348326691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleY::OnEnter()
extern "C"  void DOTweenTransformScaleY_OnEnter_m1599167706 (DOTweenTransformScaleY_t2348326691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleY::<OnEnter>m__DA()
extern "C"  void DOTweenTransformScaleY_U3COnEnterU3Em__DA_m4161330742 (DOTweenTransformScaleY_t2348326691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleY::<OnEnter>m__DB()
extern "C"  void DOTweenTransformScaleY_U3COnEnterU3Em__DB_m1129919409 (DOTweenTransformScaleY_t2348326691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
