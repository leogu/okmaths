﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetCurrentResolution
struct GetCurrentResolution_t386323333;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetCurrentResolution::.ctor()
extern "C"  void GetCurrentResolution__ctor_m1233652731 (GetCurrentResolution_t386323333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCurrentResolution::Reset()
extern "C"  void GetCurrentResolution_Reset_m404640498 (GetCurrentResolution_t386323333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCurrentResolution::OnEnter()
extern "C"  void GetCurrentResolution_OnEnter_m2837888172 (GetCurrentResolution_t386323333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
