﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetIntValue
struct SetIntValue_t929835840;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetIntValue::.ctor()
extern "C"  void SetIntValue__ctor_m620589916 (SetIntValue_t929835840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::Reset()
extern "C"  void SetIntValue_Reset_m1302631097 (SetIntValue_t929835840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::OnEnter()
extern "C"  void SetIntValue_OnEnter_m239039105 (SetIntValue_t929835840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIntValue::OnUpdate()
extern "C"  void SetIntValue_OnUpdate_m1070422594 (SetIntValue_t929835840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
