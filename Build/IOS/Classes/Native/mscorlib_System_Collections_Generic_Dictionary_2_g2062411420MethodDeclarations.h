﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor()
#define Dictionary_2__ctor_m2210880287(__this, method) ((  void (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m927282699(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m406310120_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m2789137964(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2602799901_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2489840973(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2062411420 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m206582704_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m253272349(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3143729840_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m506660494(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t2062411420 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2391180541_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m3252479419(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2062411420 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1206668798_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m725326332(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m853262843_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2243761932(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2954370043_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m431074090(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2062411420 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m237963271_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m261005059(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3775521570_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m4018758004(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m984276885_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m1615357468(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2062411420 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2868006769_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m2277900919(__this, ___key0, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2017099222_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m297654658(__this, method) ((  bool (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m960517203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1938821802(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1900166091_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1737261844(__this, method) ((  bool (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4094240197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m4242783619(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2062411420 *, KeyValuePair_2_t4114723938 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m990341268_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1942558529(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2062411420 *, KeyValuePair_2_t4114723938 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1058501024_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2940898575(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2062411420 *, KeyValuePair_2U5BU5D_t1125077975*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m976354816_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3597017126(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2062411420 *, KeyValuePair_2_t4114723938 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1705959559_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m290968970(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3578539931_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m478978021(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3100111910_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3435717228(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2925090477_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m962021223(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2684932776_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::get_Count()
#define Dictionary_2_get_Count_m3463639562(__this, method) ((  int32_t (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::get_Item(TKey)
#define Dictionary_2_get_Item_m2832075111(__this, ___key0, method) ((  FieldInfoU5BU5D_t125053523* (*) (Dictionary_2_t2062411420 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m2413909512_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m4231760978(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2062411420 *, Type_t *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_set_Item_m458653679_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m925807638(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2062411420 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1045257495_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20642037(__this, ___size0, method) ((  void (*) (Dictionary_2_t2062411420 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2270022740_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m2177438527(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2147716750_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m1920925501(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t4114723938  (*) (Il2CppObject * /* static, unused */, Type_t *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_make_pair_m2631942124_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m3638151645(__this /* static, unused */, ___key0, ___value1, method) ((  Type_t * (*) (Il2CppObject * /* static, unused */, Type_t *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_pick_key_m2840829442_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1836521813(__this /* static, unused */, ___key0, ___value1, method) ((  FieldInfoU5BU5D_t125053523* (*) (Il2CppObject * /* static, unused */, Type_t *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_pick_value_m1872663242_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m2559529396(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2062411420 *, KeyValuePair_2U5BU5D_t1125077975*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1495142643_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::Resize()
#define Dictionary_2_Resize_m3752028932(__this, method) ((  void (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_Resize_m2672264133_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::Add(TKey,TValue)
#define Dictionary_2_Add_m4144740721(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2062411420 *, Type_t *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_Add_m1708621268_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::Clear()
#define Dictionary_2_Clear_m3768322693(__this, method) ((  void (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m640343207(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2062411420 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m3553426152_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2263842639(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2062411420 *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_ContainsValue_m2375979648_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m736816254(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2062411420 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2864531407_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m499021080(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2062411420 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2160537783_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::Remove(TKey)
#define Dictionary_2_Remove_m2684051441(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2062411420 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m1366616528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m3414767590(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2062411420 *, Type_t *, FieldInfoU5BU5D_t125053523**, const MethodInfo*))Dictionary_2_TryGetValue_m1120370623_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::get_Keys()
#define Dictionary_2_get_Keys_m1032909307(__this, method) ((  KeyCollection_t250941895 * (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_get_Keys_m1635778172_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::get_Values()
#define Dictionary_2_get_Values_m3795807387(__this, method) ((  ValueCollection_t765471263 * (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_get_Values_m825860460_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m799280462(__this, ___key0, method) ((  Type_t * (*) (Dictionary_2_t2062411420 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4209561517_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m3136824910(__this, ___value0, method) ((  FieldInfoU5BU5D_t125053523* (*) (Dictionary_2_t2062411420 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1381983709_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m2177793444(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2062411420 *, KeyValuePair_2_t4114723938 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m663697471_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2329198865(__this, method) ((  Enumerator_t3382436122  (*) (Dictionary_2_t2062411420 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1752238884_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,System.Reflection.FieldInfo[]>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m560824310(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Type_t *, FieldInfoU5BU5D_t125053523*, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2061238213_gshared)(__this /* static, unused */, ___key0, ___value1, method)
