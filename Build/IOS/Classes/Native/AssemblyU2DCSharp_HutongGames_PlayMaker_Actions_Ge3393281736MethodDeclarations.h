﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIKGoal
struct GetAnimatorIKGoal_t3393281736;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::.ctor()
extern "C"  void GetAnimatorIKGoal__ctor_m2767949204 (GetAnimatorIKGoal_t3393281736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::Reset()
extern "C"  void GetAnimatorIKGoal_Reset_m3449981441 (GetAnimatorIKGoal_t3393281736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnEnter()
extern "C"  void GetAnimatorIKGoal_OnEnter_m2382963785 (GetAnimatorIKGoal_t3393281736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::OnActionUpdate()
extern "C"  void GetAnimatorIKGoal_OnActionUpdate_m145202688 (GetAnimatorIKGoal_t3393281736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIKGoal::DoGetIKGoal()
extern "C"  void GetAnimatorIKGoal_DoGetIKGoal_m718573748 (GetAnimatorIKGoal_t3393281736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
