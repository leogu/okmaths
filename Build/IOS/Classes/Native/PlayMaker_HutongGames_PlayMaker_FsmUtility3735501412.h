﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.UTF8Encoding
struct UTF8Encoding_t111055448;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmUtility
struct  FsmUtility_t3735501412  : public Il2CppObject
{
public:

public:
};

struct FsmUtility_t3735501412_StaticFields
{
public:
	// System.Text.UTF8Encoding HutongGames.PlayMaker.FsmUtility::encoding
	UTF8Encoding_t111055448 * ___encoding_0;

public:
	inline static int32_t get_offset_of_encoding_0() { return static_cast<int32_t>(offsetof(FsmUtility_t3735501412_StaticFields, ___encoding_0)); }
	inline UTF8Encoding_t111055448 * get_encoding_0() const { return ___encoding_0; }
	inline UTF8Encoding_t111055448 ** get_address_of_encoding_0() { return &___encoding_0; }
	inline void set_encoding_0(UTF8Encoding_t111055448 * value)
	{
		___encoding_0 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
