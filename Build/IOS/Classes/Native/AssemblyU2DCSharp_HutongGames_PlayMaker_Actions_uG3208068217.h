﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate1528800019.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild
struct  uGuiInputFieldRebuild_t3208068217  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// UnityEngine.UI.CanvasUpdate HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::canvasUpdate
	int32_t ___canvasUpdate_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::rebuildOnExit
	bool ___rebuildOnExit_13;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::_graphic
	Graphic_t2426225576 * ____graphic_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldRebuild_t3208068217, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_canvasUpdate_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldRebuild_t3208068217, ___canvasUpdate_12)); }
	inline int32_t get_canvasUpdate_12() const { return ___canvasUpdate_12; }
	inline int32_t* get_address_of_canvasUpdate_12() { return &___canvasUpdate_12; }
	inline void set_canvasUpdate_12(int32_t value)
	{
		___canvasUpdate_12 = value;
	}

	inline static int32_t get_offset_of_rebuildOnExit_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldRebuild_t3208068217, ___rebuildOnExit_13)); }
	inline bool get_rebuildOnExit_13() const { return ___rebuildOnExit_13; }
	inline bool* get_address_of_rebuildOnExit_13() { return &___rebuildOnExit_13; }
	inline void set_rebuildOnExit_13(bool value)
	{
		___rebuildOnExit_13 = value;
	}

	inline static int32_t get_offset_of__graphic_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldRebuild_t3208068217, ____graphic_14)); }
	inline Graphic_t2426225576 * get__graphic_14() const { return ____graphic_14; }
	inline Graphic_t2426225576 ** get_address_of__graphic_14() { return &____graphic_14; }
	inline void set__graphic_14(Graphic_t2426225576 * value)
	{
		____graphic_14 = value;
		Il2CppCodeGenWriteBarrier(&____graphic_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
