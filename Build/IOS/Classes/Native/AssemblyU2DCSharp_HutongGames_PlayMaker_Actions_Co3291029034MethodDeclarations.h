﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>
struct ComponentAction_1_t3291029034;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GUIText
struct GUIText_t2411476300;
// UnityEngine.GUITexture
struct GUITexture_t1909122990;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.NetworkView
struct NetworkView_t172525251;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::.ctor()
extern "C"  void ComponentAction_1__ctor_m2818003542_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1__ctor_m2818003542(__this, method) ((  void (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1__ctor_m2818003542_gshared)(__this, method)
// UnityEngine.Rigidbody HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_rigidbody()
extern "C"  Rigidbody_t4233889191 * ComponentAction_1_get_rigidbody_m3236782951_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_rigidbody_m3236782951(__this, method) ((  Rigidbody_t4233889191 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_rigidbody_m3236782951_gshared)(__this, method)
// UnityEngine.Rigidbody2D HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_rigidbody2d()
extern "C"  Rigidbody2D_t502193897 * ComponentAction_1_get_rigidbody2d_m370912679_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_rigidbody2d_m370912679(__this, method) ((  Rigidbody2D_t502193897 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_rigidbody2d_m370912679_gshared)(__this, method)
// UnityEngine.Renderer HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_renderer()
extern "C"  Renderer_t257310565 * ComponentAction_1_get_renderer_m3415049211_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_renderer_m3415049211(__this, method) ((  Renderer_t257310565 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_renderer_m3415049211_gshared)(__this, method)
// UnityEngine.Animation HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_animation()
extern "C"  Animation_t2068071072 * ComponentAction_1_get_animation_m3473365767_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_animation_m3473365767(__this, method) ((  Animation_t2068071072 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_animation_m3473365767_gshared)(__this, method)
// UnityEngine.AudioSource HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_audio()
extern "C"  AudioSource_t1135106623 * ComponentAction_1_get_audio_m1942127190_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_audio_m1942127190(__this, method) ((  AudioSource_t1135106623 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_audio_m1942127190_gshared)(__this, method)
// UnityEngine.Camera HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_camera()
extern "C"  Camera_t189460977 * ComponentAction_1_get_camera_m1130907379_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_camera_m1130907379(__this, method) ((  Camera_t189460977 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_camera_m1130907379_gshared)(__this, method)
// UnityEngine.GUIText HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_guiText()
extern "C"  GUIText_t2411476300 * ComponentAction_1_get_guiText_m295773063_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_guiText_m295773063(__this, method) ((  GUIText_t2411476300 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_guiText_m295773063_gshared)(__this, method)
// UnityEngine.GUITexture HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_guiTexture()
extern "C"  GUITexture_t1909122990 * ComponentAction_1_get_guiTexture_m2570816111_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_guiTexture_m2570816111(__this, method) ((  GUITexture_t1909122990 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_guiTexture_m2570816111_gshared)(__this, method)
// UnityEngine.Light HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_light()
extern "C"  Light_t494725636 * ComponentAction_1_get_light_m1266840839_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_light_m1266840839(__this, method) ((  Light_t494725636 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_light_m1266840839_gshared)(__this, method)
// UnityEngine.NetworkView HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::get_networkView()
extern "C"  NetworkView_t172525251 * ComponentAction_1_get_networkView_m117748871_gshared (ComponentAction_1_t3291029034 * __this, const MethodInfo* method);
#define ComponentAction_1_get_networkView_m117748871(__this, method) ((  NetworkView_t172525251 * (*) (ComponentAction_1_t3291029034 *, const MethodInfo*))ComponentAction_1_get_networkView_m117748871_gshared)(__this, method)
// System.Boolean HutongGames.PlayMaker.Actions.ComponentAction`1<System.Object>::UpdateCache(UnityEngine.GameObject)
extern "C"  bool ComponentAction_1_UpdateCache_m929902755_gshared (ComponentAction_1_t3291029034 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method);
#define ComponentAction_1_UpdateCache_m929902755(__this, ___go0, method) ((  bool (*) (ComponentAction_1_t3291029034 *, GameObject_t1756533147 *, const MethodInfo*))ComponentAction_1_UpdateCache_m929902755_gshared)(__this, ___go0, method)
