﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FormatString
struct FormatString_t3978075300;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FormatString::.ctor()
extern "C"  void FormatString__ctor_m1717388326 (FormatString_t3978075300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::Reset()
extern "C"  void FormatString_Reset_m3404403061 (FormatString_t3978075300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnEnter()
extern "C"  void FormatString_OnEnter_m2407425445 (FormatString_t3978075300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::OnUpdate()
extern "C"  void FormatString_OnUpdate_m281810712 (FormatString_t3978075300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FormatString::DoFormatString()
extern "C"  void FormatString_DoFormatString_m2248083521 (FormatString_t3978075300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
