﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection
struct uGuiScrollbarSetDirection_t4203356181;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::.ctor()
extern "C"  void uGuiScrollbarSetDirection__ctor_m2601471691 (uGuiScrollbarSetDirection_t4203356181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::Reset()
extern "C"  void uGuiScrollbarSetDirection_Reset_m736105622 (uGuiScrollbarSetDirection_t4203356181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::OnEnter()
extern "C"  void uGuiScrollbarSetDirection_OnEnter_m1874216480 (uGuiScrollbarSetDirection_t4203356181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::DoSetValue()
extern "C"  void uGuiScrollbarSetDirection_DoSetValue_m216234929 (uGuiScrollbarSetDirection_t4203356181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetDirection::OnExit()
extern "C"  void uGuiScrollbarSetDirection_OnExit_m3684201096 (uGuiScrollbarSetDirection_t4203356181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
