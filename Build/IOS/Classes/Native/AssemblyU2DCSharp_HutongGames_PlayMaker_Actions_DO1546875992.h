﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear
struct  DOTweenAdditionalMethodsClear_t1546875992  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear::destroy
	FsmBool_t664485696 * ___destroy_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear::debugThis
	FsmBool_t664485696 * ___debugThis_12;

public:
	inline static int32_t get_offset_of_destroy_11() { return static_cast<int32_t>(offsetof(DOTweenAdditionalMethodsClear_t1546875992, ___destroy_11)); }
	inline FsmBool_t664485696 * get_destroy_11() const { return ___destroy_11; }
	inline FsmBool_t664485696 ** get_address_of_destroy_11() { return &___destroy_11; }
	inline void set_destroy_11(FsmBool_t664485696 * value)
	{
		___destroy_11 = value;
		Il2CppCodeGenWriteBarrier(&___destroy_11, value);
	}

	inline static int32_t get_offset_of_debugThis_12() { return static_cast<int32_t>(offsetof(DOTweenAdditionalMethodsClear_t1546875992, ___debugThis_12)); }
	inline FsmBool_t664485696 * get_debugThis_12() const { return ___debugThis_12; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_12() { return &___debugThis_12; }
	inline void set_debugThis_12(FsmBool_t664485696 * value)
	{
		___debugThis_12 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
