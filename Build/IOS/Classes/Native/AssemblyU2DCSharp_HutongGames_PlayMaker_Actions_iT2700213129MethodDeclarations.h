﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleUpdate
struct iTweenScaleUpdate_t2700213129;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::.ctor()
extern "C"  void iTweenScaleUpdate__ctor_m2028145483 (iTweenScaleUpdate_t2700213129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::Reset()
extern "C"  void iTweenScaleUpdate_Reset_m1914741830 (iTweenScaleUpdate_t2700213129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnEnter()
extern "C"  void iTweenScaleUpdate_OnEnter_m2485088160 (iTweenScaleUpdate_t2700213129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnExit()
extern "C"  void iTweenScaleUpdate_OnExit_m1112464104 (iTweenScaleUpdate_t2700213129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::OnUpdate()
extern "C"  void iTweenScaleUpdate_OnUpdate_m643753491 (iTweenScaleUpdate_t2700213129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleUpdate::DoiTween()
extern "C"  void iTweenScaleUpdate_DoiTween_m1568045978 (iTweenScaleUpdate_t2700213129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
