﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Re2915049457.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis
struct  RectTransformFlipLayoutAxis_t2128522732  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis/RectTransformFlipOptions HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::axis
	int32_t ___axis_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::keepPositioning
	FsmBool_t664485696 * ___keepPositioning_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::recursive
	FsmBool_t664485696 * ___recursive_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2128522732, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_axis_12() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2128522732, ___axis_12)); }
	inline int32_t get_axis_12() const { return ___axis_12; }
	inline int32_t* get_address_of_axis_12() { return &___axis_12; }
	inline void set_axis_12(int32_t value)
	{
		___axis_12 = value;
	}

	inline static int32_t get_offset_of_keepPositioning_13() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2128522732, ___keepPositioning_13)); }
	inline FsmBool_t664485696 * get_keepPositioning_13() const { return ___keepPositioning_13; }
	inline FsmBool_t664485696 ** get_address_of_keepPositioning_13() { return &___keepPositioning_13; }
	inline void set_keepPositioning_13(FsmBool_t664485696 * value)
	{
		___keepPositioning_13 = value;
		Il2CppCodeGenWriteBarrier(&___keepPositioning_13, value);
	}

	inline static int32_t get_offset_of_recursive_14() { return static_cast<int32_t>(offsetof(RectTransformFlipLayoutAxis_t2128522732, ___recursive_14)); }
	inline FsmBool_t664485696 * get_recursive_14() const { return ___recursive_14; }
	inline FsmBool_t664485696 ** get_address_of_recursive_14() { return &___recursive_14; }
	inline void set_recursive_14(FsmBool_t664485696 * value)
	{
		___recursive_14 = value;
		Il2CppCodeGenWriteBarrier(&___recursive_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
