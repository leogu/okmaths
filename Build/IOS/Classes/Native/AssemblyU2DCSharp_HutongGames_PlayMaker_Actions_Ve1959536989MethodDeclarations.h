﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Interpolate
struct Vector3Interpolate_t1959536989;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::.ctor()
extern "C"  void Vector3Interpolate__ctor_m335764911 (Vector3Interpolate_t1959536989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::Reset()
extern "C"  void Vector3Interpolate_Reset_m2301347150 (Vector3Interpolate_t1959536989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::OnEnter()
extern "C"  void Vector3Interpolate_OnEnter_m3831713992 (Vector3Interpolate_t1959536989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Interpolate::OnUpdate()
extern "C"  void Vector3Interpolate_OnUpdate_m4149784959 (Vector3Interpolate_t1959536989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
