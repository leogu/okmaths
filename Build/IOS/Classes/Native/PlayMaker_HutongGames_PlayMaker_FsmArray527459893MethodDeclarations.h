﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Array
struct Il2CppArray;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Type1303803226.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray527459893.h"
#include "mscorlib_System_String2029220233.h"

// System.Object HutongGames.PlayMaker.FsmArray::get_RawValue()
extern "C"  Il2CppObject * FsmArray_get_RawValue_m837495043 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_RawValue(System.Object)
extern "C"  void FsmArray_set_RawValue_m1200891258 (FsmArray_t527459893 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmArray::get_ObjectType()
extern "C"  Type_t * FsmArray_get_ObjectType_m1780305676 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_ObjectType(System.Type)
extern "C"  void FsmArray_set_ObjectType_m310173343 (FsmArray_t527459893 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmArray::get_ObjectTypeName()
extern "C"  String_t* FsmArray_get_ObjectTypeName_m150755608 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] HutongGames.PlayMaker.FsmArray::get_Values()
extern "C"  ObjectU5BU5D_t3614634134* FsmArray_get_Values_m592566460 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_Values(System.Object[])
extern "C"  void FsmArray_set_Values_m2545008121 (FsmArray_t527459893 * __this, ObjectU5BU5D_t3614634134* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmArray::get_Length()
extern "C"  int32_t FsmArray_get_Length_m1940022255 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::get_TypeConstraint()
extern "C"  int32_t FsmArray_get_TypeConstraint_m4051893839 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::get_ElementType()
extern "C"  int32_t FsmArray_get_ElementType_m2542029030 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::set_ElementType(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmArray_set_ElementType_m4290463999 (FsmArray_t527459893 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::InitArray()
extern "C"  void FsmArray_InitArray_m2400636147 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmArray::Get(System.Int32)
extern "C"  Il2CppObject * FsmArray_Get_m688421582 (FsmArray_t527459893 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Set(System.Int32,System.Object)
extern "C"  void FsmArray_Set_m324686745 (FsmArray_t527459893 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmArray::Load(System.Int32)
extern "C"  Il2CppObject * FsmArray_Load_m1811887270 (FsmArray_t527459893 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Save(System.Int32,System.Object)
extern "C"  void FsmArray_Save_m2278080506 (FsmArray_t527459893 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::SetType(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmArray_SetType_m1360668000 (FsmArray_t527459893 * __this, int32_t ___newType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::SaveChanges()
extern "C"  void FsmArray_SaveChanges_m4224960568 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::CopyValues(HutongGames.PlayMaker.FsmArray)
extern "C"  void FsmArray_CopyValues_m3394065712 (FsmArray_t527459893 * __this, FsmArray_t527459893 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::ConformSourceArraySize()
extern "C"  void FsmArray_ConformSourceArraySize_m467793181 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array HutongGames.PlayMaker.FsmArray::GetSourceArray()
extern "C"  Il2CppArray * FsmArray_GetSourceArray_m487470259 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Resize(System.Int32)
extern "C"  void FsmArray_Resize_m247957487 (FsmArray_t527459893 * __this, int32_t ___newLength0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::Reset()
extern "C"  void FsmArray_Reset_m3923572225 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::.ctor()
extern "C"  void FsmArray__ctor_m3740456034 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::.ctor(System.String)
extern "C"  void FsmArray__ctor_m1515733752 (FsmArray_t527459893 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmArray::.ctor(HutongGames.PlayMaker.FsmArray)
extern "C"  void FsmArray__ctor_m1366238203 (FsmArray_t527459893 * __this, FsmArray_t527459893 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmArray::Clone()
extern "C"  NamedVariable_t3026441313 * FsmArray_Clone_m3681241155 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmArray::get_VariableType()
extern "C"  int32_t FsmArray_get_VariableType_m3991242088 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmArray::ToString()
extern "C"  String_t* FsmArray_ToString_m1700318875 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmArray::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmArray_TestTypeConstraint_m1689893872 (FsmArray_t527459893 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmArray::RealType()
extern "C"  Type_t * FsmArray_RealType_m1965969134 (FsmArray_t527459893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
