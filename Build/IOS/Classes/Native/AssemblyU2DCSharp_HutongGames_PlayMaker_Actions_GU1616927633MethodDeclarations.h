﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutToggle
struct GUILayoutToggle_t1616927633;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::.ctor()
extern "C"  void GUILayoutToggle__ctor_m1605436913 (GUILayoutToggle_t1616927633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::Reset()
extern "C"  void GUILayoutToggle_Reset_m3987925646 (GUILayoutToggle_t1616927633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutToggle::OnGUI()
extern "C"  void GUILayoutToggle_OnGUI_m3064658611 (GUILayoutToggle_t1616927633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
