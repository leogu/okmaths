﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformPunchPosition
struct DOTweenTransformPunchPosition_t1857486379;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchPosition::.ctor()
extern "C"  void DOTweenTransformPunchPosition__ctor_m3061419837 (DOTweenTransformPunchPosition_t1857486379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchPosition::Reset()
extern "C"  void DOTweenTransformPunchPosition_Reset_m1220136480 (DOTweenTransformPunchPosition_t1857486379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchPosition::OnEnter()
extern "C"  void DOTweenTransformPunchPosition_OnEnter_m1196276546 (DOTweenTransformPunchPosition_t1857486379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchPosition::<OnEnter>m__CE()
extern "C"  void DOTweenTransformPunchPosition_U3COnEnterU3Em__CE_m926052101 (DOTweenTransformPunchPosition_t1857486379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchPosition::<OnEnter>m__CF()
extern "C"  void DOTweenTransformPunchPosition_U3COnEnterU3Em__CF_m926051940 (DOTweenTransformPunchPosition_t1857486379 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
