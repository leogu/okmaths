﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolFlip
struct BoolFlip_t3541544621;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolFlip::.ctor()
extern "C"  void BoolFlip__ctor_m2252166463 (BoolFlip_t3541544621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolFlip::Reset()
extern "C"  void BoolFlip_Reset_m532645982 (BoolFlip_t3541544621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolFlip::OnEnter()
extern "C"  void BoolFlip_OnEnter_m3971806696 (BoolFlip_t3541544621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
