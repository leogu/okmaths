﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect19023354.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.Rect HutongGames.PlayMaker.FsmRect::get_Value()
extern "C"  Rect_t3681755626  FsmRect_get_Value_m3959922771 (FsmRect_t19023354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::set_Value(UnityEngine.Rect)
extern "C"  void FsmRect_set_Value_m829547054 (FsmRect_t19023354 * __this, Rect_t3681755626  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmRect::get_RawValue()
extern "C"  Il2CppObject * FsmRect_get_RawValue_m2936936508 (FsmRect_t19023354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::set_RawValue(System.Object)
extern "C"  void FsmRect_set_RawValue_m1509143501 (FsmRect_t19023354 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::.ctor()
extern "C"  void FsmRect__ctor_m2863765477 (FsmRect_t19023354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::.ctor(System.String)
extern "C"  void FsmRect__ctor_m3091639327 (FsmRect_t19023354 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmRect::.ctor(HutongGames.PlayMaker.FsmRect)
extern "C"  void FsmRect__ctor_m3953274339 (FsmRect_t19023354 * __this, FsmRect_t19023354 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmRect::Clone()
extern "C"  NamedVariable_t3026441313 * FsmRect_Clone_m4213736406 (FsmRect_t19023354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmRect::get_VariableType()
extern "C"  int32_t FsmRect_get_VariableType_m3265111193 (FsmRect_t19023354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmRect::ToString()
extern "C"  String_t* FsmRect_ToString_m2599305540 (FsmRect_t19023354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmRect::op_Implicit(UnityEngine.Rect)
extern "C"  FsmRect_t19023354 * FsmRect_op_Implicit_m1147236684 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
