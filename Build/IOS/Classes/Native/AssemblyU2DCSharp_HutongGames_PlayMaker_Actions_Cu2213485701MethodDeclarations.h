﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveRect
struct CurveRect_t2213485701;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveRect::.ctor()
extern "C"  void CurveRect__ctor_m3634154437 (CurveRect_t2213485701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::Reset()
extern "C"  void CurveRect_Reset_m1627612570 (CurveRect_t2213485701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::OnEnter()
extern "C"  void CurveRect_OnEnter_m4050474288 (CurveRect_t2213485701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::OnExit()
extern "C"  void CurveRect_OnExit_m1331852284 (CurveRect_t2213485701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveRect::OnUpdate()
extern "C"  void CurveRect_OnUpdate_m19890401 (CurveRect_t2213485701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
