﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseRect
struct EaseRect_t2446331988;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseRect::.ctor()
extern "C"  void EaseRect__ctor_m1582676556 (EaseRect_t2446331988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::Reset()
extern "C"  void EaseRect_Reset_m2655296869 (EaseRect_t2446331988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnEnter()
extern "C"  void EaseRect_OnEnter_m493196813 (EaseRect_t2446331988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnExit()
extern "C"  void EaseRect_OnExit_m1621140937 (EaseRect_t2446331988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseRect::OnUpdate()
extern "C"  void EaseRect_OnUpdate_m2837345674 (EaseRect_t2446331988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
