﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableCreate
struct HashTableCreate_t3789948198;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableCreate::.ctor()
extern "C"  void HashTableCreate__ctor_m1126098562 (HashTableCreate_t3789948198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCreate::Reset()
extern "C"  void HashTableCreate_Reset_m2503284287 (HashTableCreate_t3789948198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCreate::OnEnter()
extern "C"  void HashTableCreate_OnEnter_m3250568311 (HashTableCreate_t3789948198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCreate::OnExit()
extern "C"  void HashTableCreate_OnExit_m458227239 (HashTableCreate_t3789948198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCreate::DoAddPlayMakerHashTable()
extern "C"  void HashTableCreate_DoAddPlayMakerHashTable_m3502738572 (HashTableCreate_t3789948198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
