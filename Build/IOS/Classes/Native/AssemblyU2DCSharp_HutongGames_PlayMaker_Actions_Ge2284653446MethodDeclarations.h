﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight
struct GetAnimatorLeftFootBottomHeight_t2284653446;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::.ctor()
extern "C"  void GetAnimatorLeftFootBottomHeight__ctor_m3249923894 (GetAnimatorLeftFootBottomHeight_t2284653446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::Reset()
extern "C"  void GetAnimatorLeftFootBottomHeight_Reset_m2095175003 (GetAnimatorLeftFootBottomHeight_t2284653446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnEnter()
extern "C"  void GetAnimatorLeftFootBottomHeight_OnEnter_m977411283 (GetAnimatorLeftFootBottomHeight_t2284653446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorLeftFootBottomHeight_OnLateUpdate_m3674756140 (GetAnimatorLeftFootBottomHeight_t2284653446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLeftFootBottomHeight::_getLeftFootBottonHeight()
extern "C"  void GetAnimatorLeftFootBottomHeight__getLeftFootBottonHeight_m2784251333 (GetAnimatorLeftFootBottomHeight_t2284653446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
