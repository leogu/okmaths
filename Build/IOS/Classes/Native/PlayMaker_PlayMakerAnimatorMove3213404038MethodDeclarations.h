﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerAnimatorMove
struct PlayMakerAnimatorMove_t3213404038;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerAnimatorMove::OnAnimatorMove()
extern "C"  void PlayMakerAnimatorMove_OnAnimatorMove_m1252704242 (PlayMakerAnimatorMove_t3213404038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerAnimatorMove::.ctor()
extern "C"  void PlayMakerAnimatorMove__ctor_m3852680609 (PlayMakerAnimatorMove_t3213404038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
