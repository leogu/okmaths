﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmVariable
struct GetFsmVariable_t3000355774;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::.ctor()
extern "C"  void GetFsmVariable__ctor_m1925704170 (GetFsmVariable_t3000355774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::Reset()
extern "C"  void GetFsmVariable_Reset_m1127045027 (GetFsmVariable_t3000355774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::OnEnter()
extern "C"  void GetFsmVariable_OnEnter_m3625920035 (GetFsmVariable_t3000355774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::OnUpdate()
extern "C"  void GetFsmVariable_OnUpdate_m967110052 (GetFsmVariable_t3000355774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::InitFsmVar()
extern "C"  void GetFsmVariable_InitFsmVar_m3999800365 (GetFsmVariable_t3000355774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmVariable::DoGetFsmVariable()
extern "C"  void GetFsmVariable_DoGetFsmVariable_m2364517089 (GetFsmVariable_t3000355774 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
