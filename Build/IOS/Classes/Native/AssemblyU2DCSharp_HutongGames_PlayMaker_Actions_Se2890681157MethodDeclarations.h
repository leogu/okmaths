﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetIsKinematic2d
struct SetIsKinematic2d_t2890681157;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic2d::.ctor()
extern "C"  void SetIsKinematic2d__ctor_m2187663207 (SetIsKinematic2d_t2890681157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic2d::Reset()
extern "C"  void SetIsKinematic2d_Reset_m2371637366 (SetIsKinematic2d_t2890681157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic2d::OnEnter()
extern "C"  void SetIsKinematic2d_OnEnter_m4162200896 (SetIsKinematic2d_t2890681157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetIsKinematic2d::DoSetIsKinematic()
extern "C"  void SetIsKinematic2d_DoSetIsKinematic_m726304255 (SetIsKinematic2d_t2890681157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
