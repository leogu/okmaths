﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorTarget
struct SetAnimatorTarget_t762350858;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::.ctor()
extern "C"  void SetAnimatorTarget__ctor_m386479242 (SetAnimatorTarget_t762350858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::Reset()
extern "C"  void SetAnimatorTarget_Reset_m3681986119 (SetAnimatorTarget_t762350858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::OnPreprocess()
extern "C"  void SetAnimatorTarget_OnPreprocess_m4215102311 (SetAnimatorTarget_t762350858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::OnEnter()
extern "C"  void SetAnimatorTarget_OnEnter_m1039156567 (SetAnimatorTarget_t762350858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::DoAnimatorMove()
extern "C"  void SetAnimatorTarget_DoAnimatorMove_m4008729239 (SetAnimatorTarget_t762350858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTarget::SetTarget()
extern "C"  void SetAnimatorTarget_SetTarget_m2217284069 (SetAnimatorTarget_t762350858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
