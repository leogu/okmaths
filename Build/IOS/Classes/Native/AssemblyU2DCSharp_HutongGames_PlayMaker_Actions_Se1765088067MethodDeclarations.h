﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFloatValue
struct SetFloatValue_t1765088067;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::.ctor()
extern "C"  void SetFloatValue__ctor_m1894697243 (SetFloatValue_t1765088067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::Reset()
extern "C"  void SetFloatValue_Reset_m2613863224 (SetFloatValue_t1765088067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::OnEnter()
extern "C"  void SetFloatValue_OnEnter_m2613809758 (SetFloatValue_t1765088067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFloatValue::OnUpdate()
extern "C"  void SetFloatValue_OnUpdate_m3092239299 (SetFloatValue_t1765088067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
