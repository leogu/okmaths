﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralColor
struct SetProceduralColor_t2922530328;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::.ctor()
extern "C"  void SetProceduralColor__ctor_m562693662 (SetProceduralColor_t2922530328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::Reset()
extern "C"  void SetProceduralColor_Reset_m748253829 (SetProceduralColor_t2922530328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::OnEnter()
extern "C"  void SetProceduralColor_OnEnter_m596662549 (SetProceduralColor_t2922530328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::OnUpdate()
extern "C"  void SetProceduralColor_OnUpdate_m1679494536 (SetProceduralColor_t2922530328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralColor::DoSetProceduralFloat()
extern "C"  void SetProceduralColor_DoSetProceduralFloat_m327102478 (SetProceduralColor_t2922530328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
