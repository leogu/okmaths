﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetLocationInfo
struct GetLocationInfo_t2039944043;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::.ctor()
extern "C"  void GetLocationInfo__ctor_m2328700535 (GetLocationInfo_t2039944043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::Reset()
extern "C"  void GetLocationInfo_Reset_m1966514380 (GetLocationInfo_t2039944043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::OnEnter()
extern "C"  void GetLocationInfo_OnEnter_m1644307722 (GetLocationInfo_t2039944043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLocationInfo::DoGetLocationInfo()
extern "C"  void GetLocationInfo_DoGetLocationInfo_m1286865789 (GetLocationInfo_t2039944043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
