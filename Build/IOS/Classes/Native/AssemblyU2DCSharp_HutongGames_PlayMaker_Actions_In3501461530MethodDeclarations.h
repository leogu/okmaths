﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntClamp
struct IntClamp_t3501461530;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntClamp::.ctor()
extern "C"  void IntClamp__ctor_m2752867728 (IntClamp_t3501461530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::Reset()
extern "C"  void IntClamp_Reset_m142754983 (IntClamp_t3501461530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::OnEnter()
extern "C"  void IntClamp_OnEnter_m2149525399 (IntClamp_t3501461530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::OnUpdate()
extern "C"  void IntClamp_OnUpdate_m3361946086 (IntClamp_t3501461530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntClamp::DoClamp()
extern "C"  void IntClamp_DoClamp_m2465874404 (IntClamp_t3501461530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
