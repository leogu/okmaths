﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenSpriteRendererColor
struct DOTweenSpriteRendererColor_t3407535871;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererColor::.ctor()
extern "C"  void DOTweenSpriteRendererColor__ctor_m4169052617 (DOTweenSpriteRendererColor_t3407535871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererColor::Reset()
extern "C"  void DOTweenSpriteRendererColor_Reset_m2208884552 (DOTweenSpriteRendererColor_t3407535871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererColor::OnEnter()
extern "C"  void DOTweenSpriteRendererColor_OnEnter_m3861554618 (DOTweenSpriteRendererColor_t3407535871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererColor::<OnEnter>m__96()
extern "C"  void DOTweenSpriteRendererColor_U3COnEnterU3Em__96_m1480485746 (DOTweenSpriteRendererColor_t3407535871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererColor::<OnEnter>m__97()
extern "C"  void DOTweenSpriteRendererColor_U3COnEnterU3Em__97_m4229572077 (DOTweenSpriteRendererColor_t3407535871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
