﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraColor
struct DOTweenCameraColor_t2542907352;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraColor::.ctor()
extern "C"  void DOTweenCameraColor__ctor_m262064146 (DOTweenCameraColor_t2542907352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraColor::Reset()
extern "C"  void DOTweenCameraColor_Reset_m3726662249 (DOTweenCameraColor_t2542907352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraColor::OnEnter()
extern "C"  void DOTweenCameraColor_OnEnter_m1293639001 (DOTweenCameraColor_t2542907352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraColor::<OnEnter>m__24()
extern "C"  void DOTweenCameraColor_U3COnEnterU3Em__24_m1631669844 (DOTweenCameraColor_t2542907352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraColor::<OnEnter>m__25()
extern "C"  void DOTweenCameraColor_U3COnEnterU3Em__25_m3177550809 (DOTweenCameraColor_t2542907352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
