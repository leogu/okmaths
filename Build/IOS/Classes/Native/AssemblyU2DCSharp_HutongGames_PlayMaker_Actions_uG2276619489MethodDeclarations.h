﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiGraphicGetColor
struct uGuiGraphicGetColor_t2276619489;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::.ctor()
extern "C"  void uGuiGraphicGetColor__ctor_m3835496649 (uGuiGraphicGetColor_t2276619489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::Reset()
extern "C"  void uGuiGraphicGetColor_Reset_m2658435766 (uGuiGraphicGetColor_t2276619489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::OnEnter()
extern "C"  void uGuiGraphicGetColor_OnEnter_m647188556 (uGuiGraphicGetColor_t2276619489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::OnUpdate()
extern "C"  void uGuiGraphicGetColor_OnUpdate_m1953341957 (uGuiGraphicGetColor_t2276619489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::DoGetColorValue()
extern "C"  void uGuiGraphicGetColor_DoGetColorValue_m527402910 (uGuiGraphicGetColor_t2276619489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
