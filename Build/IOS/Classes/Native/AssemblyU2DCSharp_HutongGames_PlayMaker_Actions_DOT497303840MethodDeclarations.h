﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateInt
struct DOTweenAnimateInt_t497303840;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateInt::.ctor()
extern "C"  void DOTweenAnimateInt__ctor_m3348734242 (DOTweenAnimateInt_t497303840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateInt::Reset()
extern "C"  void DOTweenAnimateInt_Reset_m3782421725 (DOTweenAnimateInt_t497303840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateInt::OnEnter()
extern "C"  void DOTweenAnimateInt_OnEnter_m2729264645 (DOTweenAnimateInt_t497303840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Actions.DOTweenAnimateInt::<OnEnter>m__8()
extern "C"  int32_t DOTweenAnimateInt_U3COnEnterU3Em__8_m1040705158 (DOTweenAnimateInt_t497303840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateInt::<OnEnter>m__9(System.Int32)
extern "C"  void DOTweenAnimateInt_U3COnEnterU3Em__9_m101743606 (DOTweenAnimateInt_t497303840 * __this, int32_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateInt::<OnEnter>m__A()
extern "C"  void DOTweenAnimateInt_U3COnEnterU3Em__A_m10413887 (DOTweenAnimateInt_t497303840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateInt::<OnEnter>m__B()
extern "C"  void DOTweenAnimateInt_U3COnEnterU3Em__B_m1838619854 (DOTweenAnimateInt_t497303840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
