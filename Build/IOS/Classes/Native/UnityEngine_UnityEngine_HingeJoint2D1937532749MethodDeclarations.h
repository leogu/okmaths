﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HingeJoint2D
struct HingeJoint2D_t1937532749;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointMotor2D2112906529.h"
#include "UnityEngine_UnityEngine_JointAngleLimits2D2286990451.h"

// System.Void UnityEngine.HingeJoint2D::set_useMotor(System.Boolean)
extern "C"  void HingeJoint2D_set_useMotor_m1047536122 (HingeJoint2D_t1937532749 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)
extern "C"  void HingeJoint2D_set_useLimits_m3895841647 (HingeJoint2D_t1937532749 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointMotor2D UnityEngine.HingeJoint2D::get_motor()
extern "C"  JointMotor2D_t2112906529  HingeJoint2D_get_motor_m4190291289 (HingeJoint2D_t1937532749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void HingeJoint2D_set_motor_m3849925458 (HingeJoint2D_t1937532749 * __this, JointMotor2D_t2112906529  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void HingeJoint2D_INTERNAL_get_motor_m3867301698 (HingeJoint2D_t1937532749 * __this, JointMotor2D_t2112906529 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void HingeJoint2D_INTERNAL_set_motor_m3153904390 (HingeJoint2D_t1937532749 * __this, JointMotor2D_t2112906529 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointAngleLimits2D UnityEngine.HingeJoint2D::get_limits()
extern "C"  JointAngleLimits2D_t2286990451  HingeJoint2D_get_limits_m1026537344 (HingeJoint2D_t1937532749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::set_limits(UnityEngine.JointAngleLimits2D)
extern "C"  void HingeJoint2D_set_limits_m1757479223 (HingeJoint2D_t1937532749 * __this, JointAngleLimits2D_t2286990451  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_get_limits(UnityEngine.JointAngleLimits2D&)
extern "C"  void HingeJoint2D_INTERNAL_get_limits_m215910349 (HingeJoint2D_t1937532749 * __this, JointAngleLimits2D_t2286990451 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint2D::INTERNAL_set_limits(UnityEngine.JointAngleLimits2D&)
extern "C"  void HingeJoint2D_INTERNAL_set_limits_m1881633545 (HingeJoint2D_t1937532749 * __this, JointAngleLimits2D_t2286990451 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
