﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SmoothLookAtDirection
struct SmoothLookAtDirection_t3145253761;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::.ctor()
extern "C"  void SmoothLookAtDirection__ctor_m2678860339 (SmoothLookAtDirection_t3145253761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::Reset()
extern "C"  void SmoothLookAtDirection_Reset_m116984286 (SmoothLookAtDirection_t3145253761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnEnter()
extern "C"  void SmoothLookAtDirection_OnEnter_m3131278280 (SmoothLookAtDirection_t3145253761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnUpdate()
extern "C"  void SmoothLookAtDirection_OnUpdate_m3049727419 (SmoothLookAtDirection_t3145253761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::OnLateUpdate()
extern "C"  void SmoothLookAtDirection_OnLateUpdate_m1440224367 (SmoothLookAtDirection_t3145253761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothLookAtDirection::DoSmoothLookAtDirection()
extern "C"  void SmoothLookAtDirection_DoSmoothLookAtDirection_m4169672509 (SmoothLookAtDirection_t3145253761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
