﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t426334852;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass6_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass6_0__ctor_m766927388 (U3CU3Ec__DisplayClass6_0_t426334852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass6_0::<DOMoveX>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass6_0_U3CDOMoveXU3Eb__0_m2997082959 (U3CU3Ec__DisplayClass6_0_t426334852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
