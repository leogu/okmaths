﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HashTableSortKeysfromValues
struct HashTableSortKeysfromValues_t3434907158;

#include "codegen/il2cpp-codegen.h"

// System.Void HashTableSortKeysfromValues::.ctor()
extern "C"  void HashTableSortKeysfromValues__ctor_m2421630207 (HashTableSortKeysfromValues_t3434907158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HashTableSortKeysfromValues::Start()
extern "C"  void HashTableSortKeysfromValues_Start_m3633871771 (HashTableSortKeysfromValues_t3434907158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HashTableSortKeysfromValues::Update()
extern "C"  void HashTableSortKeysfromValues_Update_m3523305604 (HashTableSortKeysfromValues_t3434907158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
