﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject2785794313.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_Type1303803226.h"

// UnityEngine.Texture HutongGames.PlayMaker.FsmTexture::get_Value()
extern "C"  Texture_t2243626319 * FsmTexture_get_Value_m2325441495 (FsmTexture_t3372293163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::set_Value(UnityEngine.Texture)
extern "C"  void FsmTexture_set_Value_m1312269522 (FsmTexture_t3372293163 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::.ctor()
extern "C"  void FsmTexture__ctor_m1123765920 (FsmTexture_t3372293163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::.ctor(System.String)
extern "C"  void FsmTexture__ctor_m2567827482 (FsmTexture_t3372293163 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTexture::.ctor(HutongGames.PlayMaker.FsmObject)
extern "C"  void FsmTexture__ctor_m1201619313 (FsmTexture_t3372293163 * __this, FsmObject_t2785794313 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmTexture::Clone()
extern "C"  NamedVariable_t3026441313 * FsmTexture_Clone_m2535392569 (FsmTexture_t3372293163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmTexture::get_VariableType()
extern "C"  int32_t FsmTexture_get_VariableType_m1231939278 (FsmTexture_t3372293163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmTexture::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool FsmTexture_TestTypeConstraint_m1684580458 (FsmTexture_t3372293163 * __this, int32_t ___variableType0, Type_t * ____objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
