﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListRemove
struct ArrayListRemove_t1819720793;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListRemove::.ctor()
extern "C"  void ArrayListRemove__ctor_m2471980443 (ArrayListRemove_t1819720793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemove::Reset()
extern "C"  void ArrayListRemove_Reset_m3072438902 (ArrayListRemove_t1819720793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemove::OnEnter()
extern "C"  void ArrayListRemove_OnEnter_m2356633392 (ArrayListRemove_t1819720793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemove::DoRemoveFromArrayList()
extern "C"  void ArrayListRemove_DoRemoveFromArrayList_m1735355027 (ArrayListRemove_t1819720793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
