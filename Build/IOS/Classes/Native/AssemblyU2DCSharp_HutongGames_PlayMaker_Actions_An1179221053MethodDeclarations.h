﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorPlay
struct AnimatorPlay_t1179221053;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::.ctor()
extern "C"  void AnimatorPlay__ctor_m3273974069 (AnimatorPlay_t1179221053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::Reset()
extern "C"  void AnimatorPlay_Reset_m2479133126 (AnimatorPlay_t1179221053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::OnEnter()
extern "C"  void AnimatorPlay_OnEnter_m3758344140 (AnimatorPlay_t1179221053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::OnUpdate()
extern "C"  void AnimatorPlay_OnUpdate_m2032638145 (AnimatorPlay_t1179221053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorPlay::DoAnimatorPlay()
extern "C"  void AnimatorPlay_DoAnimatorPlay_m1971212481 (AnimatorPlay_t1179221053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
