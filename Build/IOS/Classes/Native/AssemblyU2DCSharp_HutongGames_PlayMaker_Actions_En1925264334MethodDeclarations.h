﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableBehaviour
struct EnableBehaviour_t1925264334;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::.ctor()
extern "C"  void EnableBehaviour__ctor_m2303112936 (EnableBehaviour_t1925264334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::Reset()
extern "C"  void EnableBehaviour_Reset_m484133387 (EnableBehaviour_t1925264334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::OnEnter()
extern "C"  void EnableBehaviour_OnEnter_m1581204291 (EnableBehaviour_t1925264334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::DoEnableBehaviour(UnityEngine.GameObject)
extern "C"  void EnableBehaviour_DoEnableBehaviour_m3917636869 (EnableBehaviour_t1925264334 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableBehaviour::OnExit()
extern "C"  void EnableBehaviour_OnExit_m471810211 (EnableBehaviour_t1925264334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.EnableBehaviour::ErrorCheck()
extern "C"  String_t* EnableBehaviour_ErrorCheck_m1732713001 (EnableBehaviour_t1925264334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
