﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartAll
struct DOTweenControlMethodsRestartAll_t2886931027;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartAll::.ctor()
extern "C"  void DOTweenControlMethodsRestartAll__ctor_m767666709 (DOTweenControlMethodsRestartAll_t2886931027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartAll::Reset()
extern "C"  void DOTweenControlMethodsRestartAll_Reset_m3134712840 (DOTweenControlMethodsRestartAll_t2886931027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartAll::OnEnter()
extern "C"  void DOTweenControlMethodsRestartAll_OnEnter_m558007578 (DOTweenControlMethodsRestartAll_t2886931027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
