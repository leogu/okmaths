﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionLerp
struct QuaternionLerp_t2507829947;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::.ctor()
extern "C"  void QuaternionLerp__ctor_m3331475277 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::Reset()
extern "C"  void QuaternionLerp_Reset_m2533097220 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnEnter()
extern "C"  void QuaternionLerp_OnEnter_m2780930342 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnUpdate()
extern "C"  void QuaternionLerp_OnUpdate_m3611121697 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnLateUpdate()
extern "C"  void QuaternionLerp_OnLateUpdate_m2169879117 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::OnFixedUpdate()
extern "C"  void QuaternionLerp_OnFixedUpdate_m2919769159 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLerp::DoQuatLerp()
extern "C"  void QuaternionLerp_DoQuatLerp_m2710910428 (QuaternionLerp_t2507829947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
