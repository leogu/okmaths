﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutFloatField
struct GUILayoutFloatField_t1191260867;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::.ctor()
extern "C"  void GUILayoutFloatField__ctor_m825401209 (GUILayoutFloatField_t1191260867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::Reset()
extern "C"  void GUILayoutFloatField_Reset_m843210972 (GUILayoutFloatField_t1191260867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatField::OnGUI()
extern "C"  void GUILayoutFloatField_OnGUI_m3561748263 (GUILayoutFloatField_t1191260867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
