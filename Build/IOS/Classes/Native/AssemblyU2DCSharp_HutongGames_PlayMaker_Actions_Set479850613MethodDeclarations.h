﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIContentColor
struct SetGUIContentColor_t479850613;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::.ctor()
extern "C"  void SetGUIContentColor__ctor_m40171839 (SetGUIContentColor_t479850613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::Reset()
extern "C"  void SetGUIContentColor_Reset_m871854398 (SetGUIContentColor_t479850613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIContentColor::OnGUI()
extern "C"  void SetGUIContentColor_OnGUI_m4117353073 (SetGUIContentColor_t479850613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
