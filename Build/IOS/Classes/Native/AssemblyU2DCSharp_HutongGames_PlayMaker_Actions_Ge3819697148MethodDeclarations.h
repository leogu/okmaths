﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmQuaternion
struct GetFsmQuaternion_t3819697148;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::.ctor()
extern "C"  void GetFsmQuaternion__ctor_m4164697282 (GetFsmQuaternion_t3819697148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::Reset()
extern "C"  void GetFsmQuaternion_Reset_m3089929825 (GetFsmQuaternion_t3819697148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::OnEnter()
extern "C"  void GetFsmQuaternion_OnEnter_m1009362633 (GetFsmQuaternion_t3819697148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::OnUpdate()
extern "C"  void GetFsmQuaternion_OnUpdate_m3510170052 (GetFsmQuaternion_t3819697148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmQuaternion::DoGetFsmVariable()
extern "C"  void GetFsmQuaternion_DoGetFsmVariable_m3311139947 (GetFsmQuaternion_t3819697148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
