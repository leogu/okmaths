﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction>
struct List_1_t2231499301;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// UnityEngine.Joint2D
struct Joint2D_t854621618;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1497896004;
// HutongGames.PlayMaker.ActionData
struct ActionData_t1467934444;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t1091630918;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Joint2D854621618.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// System.Single HutongGames.PlayMaker.FsmState::get_StateTime()
extern "C"  float FsmState_get_StateTime_m513157839 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_StateTime(System.Single)
extern "C"  void FsmState_set_StateTime_m1710524330 (FsmState_t1643911659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.FsmState::get_RealStartTime()
extern "C"  float FsmState_get_RealStartTime_m3721319750 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_RealStartTime(System.Single)
extern "C"  void FsmState_set_RealStartTime_m740534869 (FsmState_t1643911659 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_loopCount()
extern "C"  int32_t FsmState_get_loopCount_m205109870 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_loopCount(System.Int32)
extern "C"  void FsmState_set_loopCount_m1323904637 (FsmState_t1643911659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_maxLoopCount()
extern "C"  int32_t FsmState_get_maxLoopCount_m4247222280 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_maxLoopCount(System.Int32)
extern "C"  void FsmState_set_maxLoopCount_m377716181 (FsmState_t1643911659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::GetFullStateLabel(HutongGames.PlayMaker.FsmState)
extern "C"  String_t* FsmState_GetFullStateLabel_m755262026 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::.ctor(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmState__ctor_m239288040 (FsmState_t1643911659 * __this, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::.ctor(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmState__ctor_m2162395579 (FsmState_t1643911659 * __this, FsmState_t1643911659 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::CopyActionData(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmState_CopyActionData_m3279503264 (FsmState_t1643911659 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::LoadActions()
extern "C"  void FsmState_LoadActions_m803387101 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::SaveActions()
extern "C"  void FsmState_SaveActions_m3311443994 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::get_ActiveActions()
extern "C"  List_1_t2231499301 * FsmState_get_ActiveActions_m1835211778 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmStateAction> HutongGames.PlayMaker.FsmState::get_finishedActions()
extern "C"  List_1_t2231499301 * FsmState_get_finishedActions_m2060882870 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnEnter()
extern "C"  void FsmState_OnEnter_m2993875379 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::ActivateActions(System.Int32)
extern "C"  bool FsmState_ActivateActions_m1780114895 (FsmState_t1643911659 * __this, int32_t ___startIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmState_OnEvent_m2371896385 (FsmState_t1643911659 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnFixedUpdate()
extern "C"  void FsmState_OnFixedUpdate_m3843828618 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnUpdate()
extern "C"  void FsmState_OnUpdate_m1353252916 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnLateUpdate()
extern "C"  void FsmState_OnLateUpdate_m2558218176 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnAnimatorMove()
extern "C"  bool FsmState_OnAnimatorMove_m3159702129 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnAnimatorIK(System.Int32)
extern "C"  bool FsmState_OnAnimatorIK_m213850139 (FsmState_t1643911659 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionEnter(UnityEngine.Collision)
extern "C"  bool FsmState_OnCollisionEnter_m4234529378 (FsmState_t1643911659 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionStay(UnityEngine.Collision)
extern "C"  bool FsmState_OnCollisionStay_m3196134395 (FsmState_t1643911659 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionExit(UnityEngine.Collision)
extern "C"  bool FsmState_OnCollisionExit_m2170056496 (FsmState_t1643911659 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerEnter(UnityEngine.Collider)
extern "C"  bool FsmState_OnTriggerEnter_m3992825860 (FsmState_t1643911659 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerStay(UnityEngine.Collider)
extern "C"  bool FsmState_OnTriggerStay_m1506110353 (FsmState_t1643911659 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerExit(UnityEngine.Collider)
extern "C"  bool FsmState_OnTriggerExit_m4055614638 (FsmState_t1643911659 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnParticleCollision(UnityEngine.GameObject)
extern "C"  bool FsmState_OnParticleCollision_m1325327641 (FsmState_t1643911659 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  bool FsmState_OnCollisionEnter2D_m3091796802 (FsmState_t1643911659 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  bool FsmState_OnCollisionStay2D_m2457062363 (FsmState_t1643911659 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  bool FsmState_OnCollisionExit2D_m3459376528 (FsmState_t1643911659 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  bool FsmState_OnTriggerEnter2D_m2455106556 (FsmState_t1643911659 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  bool FsmState_OnTriggerStay2D_m2774172845 (FsmState_t1643911659 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  bool FsmState_OnTriggerExit2D_m357361718 (FsmState_t1643911659 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  bool FsmState_OnControllerColliderHit_m1320597400 (FsmState_t1643911659 * __this, ControllerColliderHit_t4070855101 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnJointBreak(System.Single)
extern "C"  bool FsmState_OnJointBreak_m2176628171 (FsmState_t1643911659 * __this, float ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  bool FsmState_OnJointBreak2D_m1949489011 (FsmState_t1643911659 * __this, Joint2D_t854621618 * ___joint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnGUI()
extern "C"  void FsmState_OnGUI_m1793473566 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::FinishAction(HutongGames.PlayMaker.FsmStateAction)
extern "C"  void FsmState_FinishAction_m3867341752 (FsmState_t1643911659 * __this, FsmStateAction_t2862378169 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::RemoveFinishedActions()
extern "C"  void FsmState_RemoveFinishedActions_m264076363 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::CheckAllActionsFinished()
extern "C"  void FsmState_CheckAllActionsFinished_m908708734 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::OnExit()
extern "C"  void FsmState_OnExit_m635432435 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::ResetLoopCount()
extern "C"  void FsmState_ResetLoopCount_m99531996 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.FsmState::GetTransition(System.Int32)
extern "C"  FsmTransition_t1534990431 * FsmState_GetTransition_m76329724 (FsmState_t1643911659 * __this, int32_t ___transitionIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::GetTransitionIndex(HutongGames.PlayMaker.FsmTransition)
extern "C"  int32_t FsmState_GetTransitionIndex_m152981430 (FsmState_t1643911659 * __this, FsmTransition_t1534990431 * ___transition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_Active()
extern "C"  bool FsmState_get_Active_m65240563 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmState::get_ActiveAction()
extern "C"  FsmStateAction_t2862378169 * FsmState_get_ActiveAction_m2403074917 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsInitialized()
extern "C"  bool FsmState_get_IsInitialized_m297158777 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmState::get_Fsm()
extern "C"  Fsm_t917886356 * FsmState_get_Fsm_m37672796 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Fsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmState_set_Fsm_m4013436413 (FsmState_t1643911659 * __this, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::get_Name()
extern "C"  String_t* FsmState_get_Name_m2457110913 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Name(System.String)
extern "C"  void FsmState_set_Name_m2972230504 (FsmState_t1643911659 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsSequence()
extern "C"  bool FsmState_get_IsSequence_m515848042 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_IsSequence(System.Boolean)
extern "C"  void FsmState_set_IsSequence_m3521377293 (FsmState_t1643911659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_ActiveActionIndex()
extern "C"  int32_t FsmState_get_ActiveActionIndex_m1865485221 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.FsmState::get_Position()
extern "C"  Rect_t3681755626  FsmState_get_Position_m2176831590 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Position(UnityEngine.Rect)
extern "C"  void FsmState_set_Position_m1846875389 (FsmState_t1643911659 * __this, Rect_t3681755626  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsBreakpoint()
extern "C"  bool FsmState_get_IsBreakpoint_m3341493716 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_IsBreakpoint(System.Boolean)
extern "C"  void FsmState_set_IsBreakpoint_m4145908845 (FsmState_t1643911659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_HideUnused()
extern "C"  bool FsmState_get_HideUnused_m561246459 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_HideUnused(System.Boolean)
extern "C"  void FsmState_set_HideUnused_m3268980962 (FsmState_t1643911659 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.FsmState::get_Actions()
extern "C"  FsmStateActionU5BU5D_t1497896004* FsmState_get_Actions_m2945510972 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Actions(HutongGames.PlayMaker.FsmStateAction[])
extern "C"  void FsmState_set_Actions_m3742700355 (FsmState_t1643911659 * __this, FsmStateActionU5BU5D_t1497896004* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_ActionsLoaded()
extern "C"  bool FsmState_get_ActionsLoaded_m3111890597 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.FsmState::get_ActionData()
extern "C"  ActionData_t1467934444 * FsmState_get_ActionData_m1992537252 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.FsmState::get_Transitions()
extern "C"  FsmTransitionU5BU5D_t1091630918* FsmState_get_Transitions_m726541619 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Transitions(HutongGames.PlayMaker.FsmTransition[])
extern "C"  void FsmState_set_Transitions_m713499416 (FsmState_t1643911659 * __this, FsmTransitionU5BU5D_t1091630918* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::get_Description()
extern "C"  String_t* FsmState_get_Description_m1844231100 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_Description(System.String)
extern "C"  void FsmState_set_Description_m2560583603 (FsmState_t1643911659 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_ColorIndex()
extern "C"  int32_t FsmState_get_ColorIndex_m4175238002 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmState::set_ColorIndex(System.Int32)
extern "C"  void FsmState_set_ColorIndex_m3157490225 (FsmState_t1643911659 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::GetStateIndex(HutongGames.PlayMaker.FsmState)
extern "C"  int32_t FsmState_GetStateIndex_m838748340 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
