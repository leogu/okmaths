﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue
struct  uGuiSliderGetNormalizedValue_t4120805239  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::value
	FsmFloat_t937133978 * ___value_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::_slider
	Slider_t297367283 * ____slider_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderGetNormalizedValue_t4120805239, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiSliderGetNormalizedValue_t4120805239, ___value_12)); }
	inline FsmFloat_t937133978 * get_value_12() const { return ___value_12; }
	inline FsmFloat_t937133978 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmFloat_t937133978 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(uGuiSliderGetNormalizedValue_t4120805239, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__slider_14() { return static_cast<int32_t>(offsetof(uGuiSliderGetNormalizedValue_t4120805239, ____slider_14)); }
	inline Slider_t297367283 * get__slider_14() const { return ____slider_14; }
	inline Slider_t297367283 ** get_address_of__slider_14() { return &____slider_14; }
	inline void set__slider_14(Slider_t297367283 * value)
	{
		____slider_14 = value;
		Il2CppCodeGenWriteBarrier(&____slider_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
