﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetMinValue
struct ArrayListGetMinValue_t1121161026;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMinValue::.ctor()
extern "C"  void ArrayListGetMinValue__ctor_m308925158 (ArrayListGetMinValue_t1121161026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMinValue::.cctor()
extern "C"  void ArrayListGetMinValue__cctor_m3403134217 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMinValue::Reset()
extern "C"  void ArrayListGetMinValue_Reset_m122008295 (ArrayListGetMinValue_t1121161026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMinValue::OnEnter()
extern "C"  void ArrayListGetMinValue_OnEnter_m4285359799 (ArrayListGetMinValue_t1121161026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMinValue::OnUpdate()
extern "C"  void ArrayListGetMinValue_OnUpdate_m311200288 (ArrayListGetMinValue_t1121161026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMinValue::DoFindMinimumValue()
extern "C"  void ArrayListGetMinValue_DoFindMinimumValue_m1400328297 (ArrayListGetMinValue_t1121161026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.ArrayListGetMinValue::ErrorCheck()
extern "C"  String_t* ArrayListGetMinValue_ErrorCheck_m1324153589 (ArrayListGetMinValue_t1121161026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
