﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightIntensity
struct SetLightIntensity_t972173073;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::.ctor()
extern "C"  void SetLightIntensity__ctor_m256410361 (SetLightIntensity_t972173073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::Reset()
extern "C"  void SetLightIntensity_Reset_m1633600806 (SetLightIntensity_t972173073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::OnEnter()
extern "C"  void SetLightIntensity_OnEnter_m320612732 (SetLightIntensity_t972173073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::OnUpdate()
extern "C"  void SetLightIntensity_OnUpdate_m372791189 (SetLightIntensity_t972173073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightIntensity::DoSetLightIntensity()
extern "C"  void SetLightIntensity_DoSetLightIntensity_m4246506785 (SetLightIntensity_t972173073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
