﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialTiling
struct DOTweenMaterialTiling_t3533985074;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTiling::.ctor()
extern "C"  void DOTweenMaterialTiling__ctor_m508389900 (DOTweenMaterialTiling_t3533985074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTiling::Reset()
extern "C"  void DOTweenMaterialTiling_Reset_m3139673543 (DOTweenMaterialTiling_t3533985074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTiling::OnEnter()
extern "C"  void DOTweenMaterialTiling_OnEnter_m2552097447 (DOTweenMaterialTiling_t3533985074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTiling::<OnEnter>m__5E()
extern "C"  void DOTweenMaterialTiling_U3COnEnterU3Em__5E_m688500750 (DOTweenMaterialTiling_t3533985074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialTiling::<OnEnter>m__5F()
extern "C"  void DOTweenMaterialTiling_U3COnEnterU3Em__5F_m688500651 (DOTweenMaterialTiling_t3533985074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
