﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "DOTween_DG_Tweening_LogBehaviour3505725029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenInit
struct  DOTweenInit_t580384762  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenInit::recycleAllByDefault
	FsmBool_t664485696 * ___recycleAllByDefault_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenInit::useSafeMode
	FsmBool_t664485696 * ___useSafeMode_12;
	// DG.Tweening.LogBehaviour HutongGames.PlayMaker.Actions.DOTweenInit::logBehaviour
	int32_t ___logBehaviour_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenInit::tweenersCapacity
	FsmInt_t1273009179 * ___tweenersCapacity_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenInit::sequencesCapacity
	FsmInt_t1273009179 * ___sequencesCapacity_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenInit::debugThis
	FsmBool_t664485696 * ___debugThis_16;

public:
	inline static int32_t get_offset_of_recycleAllByDefault_11() { return static_cast<int32_t>(offsetof(DOTweenInit_t580384762, ___recycleAllByDefault_11)); }
	inline FsmBool_t664485696 * get_recycleAllByDefault_11() const { return ___recycleAllByDefault_11; }
	inline FsmBool_t664485696 ** get_address_of_recycleAllByDefault_11() { return &___recycleAllByDefault_11; }
	inline void set_recycleAllByDefault_11(FsmBool_t664485696 * value)
	{
		___recycleAllByDefault_11 = value;
		Il2CppCodeGenWriteBarrier(&___recycleAllByDefault_11, value);
	}

	inline static int32_t get_offset_of_useSafeMode_12() { return static_cast<int32_t>(offsetof(DOTweenInit_t580384762, ___useSafeMode_12)); }
	inline FsmBool_t664485696 * get_useSafeMode_12() const { return ___useSafeMode_12; }
	inline FsmBool_t664485696 ** get_address_of_useSafeMode_12() { return &___useSafeMode_12; }
	inline void set_useSafeMode_12(FsmBool_t664485696 * value)
	{
		___useSafeMode_12 = value;
		Il2CppCodeGenWriteBarrier(&___useSafeMode_12, value);
	}

	inline static int32_t get_offset_of_logBehaviour_13() { return static_cast<int32_t>(offsetof(DOTweenInit_t580384762, ___logBehaviour_13)); }
	inline int32_t get_logBehaviour_13() const { return ___logBehaviour_13; }
	inline int32_t* get_address_of_logBehaviour_13() { return &___logBehaviour_13; }
	inline void set_logBehaviour_13(int32_t value)
	{
		___logBehaviour_13 = value;
	}

	inline static int32_t get_offset_of_tweenersCapacity_14() { return static_cast<int32_t>(offsetof(DOTweenInit_t580384762, ___tweenersCapacity_14)); }
	inline FsmInt_t1273009179 * get_tweenersCapacity_14() const { return ___tweenersCapacity_14; }
	inline FsmInt_t1273009179 ** get_address_of_tweenersCapacity_14() { return &___tweenersCapacity_14; }
	inline void set_tweenersCapacity_14(FsmInt_t1273009179 * value)
	{
		___tweenersCapacity_14 = value;
		Il2CppCodeGenWriteBarrier(&___tweenersCapacity_14, value);
	}

	inline static int32_t get_offset_of_sequencesCapacity_15() { return static_cast<int32_t>(offsetof(DOTweenInit_t580384762, ___sequencesCapacity_15)); }
	inline FsmInt_t1273009179 * get_sequencesCapacity_15() const { return ___sequencesCapacity_15; }
	inline FsmInt_t1273009179 ** get_address_of_sequencesCapacity_15() { return &___sequencesCapacity_15; }
	inline void set_sequencesCapacity_15(FsmInt_t1273009179 * value)
	{
		___sequencesCapacity_15 = value;
		Il2CppCodeGenWriteBarrier(&___sequencesCapacity_15, value);
	}

	inline static int32_t get_offset_of_debugThis_16() { return static_cast<int32_t>(offsetof(DOTweenInit_t580384762, ___debugThis_16)); }
	inline FsmBool_t664485696 * get_debugThis_16() const { return ___debugThis_16; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_16() { return &___debugThis_16; }
	inline void set_debugThis_16(FsmBool_t664485696 * value)
	{
		___debugThis_16 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
