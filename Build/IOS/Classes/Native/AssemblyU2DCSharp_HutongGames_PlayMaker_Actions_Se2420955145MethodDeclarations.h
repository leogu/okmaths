﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector3Value
struct SetVector3Value_t2420955145;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::.ctor()
extern "C"  void SetVector3Value__ctor_m1934546733 (SetVector3Value_t2420955145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::Reset()
extern "C"  void SetVector3Value_Reset_m755715626 (SetVector3Value_t2420955145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::OnEnter()
extern "C"  void SetVector3Value_OnEnter_m3490367016 (SetVector3Value_t2420955145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3Value::OnUpdate()
extern "C"  void SetVector3Value_OnUpdate_m2917615001 (SetVector3Value_t2420955145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
