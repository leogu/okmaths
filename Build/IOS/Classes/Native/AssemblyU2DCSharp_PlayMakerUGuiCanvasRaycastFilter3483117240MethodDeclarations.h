﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiCanvasRaycastFilterEventsProxy
struct PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void PlayMakerUGuiCanvasRaycastFilterEventsProxy::.ctor()
extern "C"  void PlayMakerUGuiCanvasRaycastFilterEventsProxy__ctor_m4272723781 (PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUGuiCanvasRaycastFilterEventsProxy::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool PlayMakerUGuiCanvasRaycastFilterEventsProxy_IsRaycastLocationValid_m1418835193 (PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240 * __this, Vector2_t2243707579  ___sp0, Camera_t189460977 * ___eventCamera1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
