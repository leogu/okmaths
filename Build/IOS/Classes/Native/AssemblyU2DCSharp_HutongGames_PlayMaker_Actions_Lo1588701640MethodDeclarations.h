﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LookAt
struct LookAt_t1588701640;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void HutongGames.PlayMaker.Actions.LookAt::.ctor()
extern "C"  void LookAt__ctor_m3230155946 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::Reset()
extern "C"  void LookAt_Reset_m2436682449 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::OnEnter()
extern "C"  void LookAt_OnEnter_m32586201 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::OnLateUpdate()
extern "C"  void LookAt_OnLateUpdate_m515878768 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LookAt::DoLookAt()
extern "C"  void LookAt_DoLookAt_m1685255713 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.LookAt::UpdateLookAtPosition()
extern "C"  bool LookAt_UpdateLookAtPosition_m4158269638 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::GetLookAtPosition()
extern "C"  Vector3_t2243707580  LookAt_GetLookAtPosition_m701845203 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.LookAt::GetLookAtPositionWithVertical()
extern "C"  Vector3_t2243707580  LookAt_GetLookAtPositionWithVertical_m3511632023 (LookAt_t1588701640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
