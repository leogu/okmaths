﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmFloat
struct SetFsmFloat_t2903169588;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::.ctor()
extern "C"  void SetFsmFloat__ctor_m841719374 (SetFsmFloat_t2903169588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::Reset()
extern "C"  void SetFsmFloat_Reset_m1682546897 (SetFsmFloat_t2903169588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::OnEnter()
extern "C"  void SetFsmFloat_OnEnter_m3004988137 (SetFsmFloat_t2903169588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::DoSetFsmFloat()
extern "C"  void SetFsmFloat_DoSetFsmFloat_m317043325 (SetFsmFloat_t2903169588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmFloat::OnUpdate()
extern "C"  void SetFsmFloat_OnUpdate_m772409416 (SetFsmFloat_t2903169588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
