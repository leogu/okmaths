﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderSetValue
struct uGuiSliderSetValue_t3755075152;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetValue::.ctor()
extern "C"  void uGuiSliderSetValue__ctor_m719708998 (uGuiSliderSetValue_t3755075152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetValue::Reset()
extern "C"  void uGuiSliderSetValue_Reset_m3291854397 (uGuiSliderSetValue_t3755075152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetValue::OnEnter()
extern "C"  void uGuiSliderSetValue_OnEnter_m2230779853 (uGuiSliderSetValue_t3755075152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetValue::OnUpdate()
extern "C"  void uGuiSliderSetValue_OnUpdate_m2863425040 (uGuiSliderSetValue_t3755075152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetValue::DoSetValue()
extern "C"  void uGuiSliderSetValue_DoSetValue_m2893232124 (uGuiSliderSetValue_t3755075152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetValue::OnExit()
extern "C"  void uGuiSliderSetValue_OnExit_m3594960657 (uGuiSliderSetValue_t3755075152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
