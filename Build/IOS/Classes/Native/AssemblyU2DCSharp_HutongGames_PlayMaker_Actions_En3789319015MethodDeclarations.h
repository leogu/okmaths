﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableFSM
struct EnableFSM_t3789319015;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnableFSM::.ctor()
extern "C"  void EnableFSM__ctor_m2530683611 (EnableFSM_t3789319015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::Reset()
extern "C"  void EnableFSM_Reset_m500059528 (EnableFSM_t3789319015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::OnEnter()
extern "C"  void EnableFSM_OnEnter_m671502726 (EnableFSM_t3789319015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::DoEnableFSM()
extern "C"  void EnableFSM_DoEnableFSM_m3415531725 (EnableFSM_t3789319015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFSM::OnExit()
extern "C"  void EnableFSM_OnExit_m25675174 (EnableFSM_t3789319015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
