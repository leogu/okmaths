﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVector2XY
struct GetVector2XY_t1745788062;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::.ctor()
extern "C"  void GetVector2XY__ctor_m865470700 (GetVector2XY_t1745788062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::Reset()
extern "C"  void GetVector2XY_Reset_m1935353163 (GetVector2XY_t1745788062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::OnEnter()
extern "C"  void GetVector2XY_OnEnter_m2726011675 (GetVector2XY_t1745788062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::OnUpdate()
extern "C"  void GetVector2XY_OnUpdate_m2769680322 (GetVector2XY_t1745788062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2XY::DoGetVector2XYZ()
extern "C"  void GetVector2XY_DoGetVector2XYZ_m1831172599 (GetVector2XY_t1745788062 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
