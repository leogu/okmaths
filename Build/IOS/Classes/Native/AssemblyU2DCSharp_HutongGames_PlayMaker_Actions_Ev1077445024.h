﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll
struct  EventSystemCurrentRayCastAll_t1077445024  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::screenPosition
	FsmVector3_t3996534004 * ___screenPosition_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::orScreenPosition2d
	FsmVector2_t2430450063 * ___orScreenPosition2d_12;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::gameObjectList
	FsmArray_t527459893 * ___gameObjectList_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::hitCount
	FsmInt_t1273009179 * ___hitCount_14;
	// System.Boolean HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.EventSystems.PointerEventData HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::pointer
	PointerEventData_t1599784723 * ___pointer_16;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::raycastResults
	List_1_t3685274804 * ___raycastResults_17;

public:
	inline static int32_t get_offset_of_screenPosition_11() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___screenPosition_11)); }
	inline FsmVector3_t3996534004 * get_screenPosition_11() const { return ___screenPosition_11; }
	inline FsmVector3_t3996534004 ** get_address_of_screenPosition_11() { return &___screenPosition_11; }
	inline void set_screenPosition_11(FsmVector3_t3996534004 * value)
	{
		___screenPosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___screenPosition_11, value);
	}

	inline static int32_t get_offset_of_orScreenPosition2d_12() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___orScreenPosition2d_12)); }
	inline FsmVector2_t2430450063 * get_orScreenPosition2d_12() const { return ___orScreenPosition2d_12; }
	inline FsmVector2_t2430450063 ** get_address_of_orScreenPosition2d_12() { return &___orScreenPosition2d_12; }
	inline void set_orScreenPosition2d_12(FsmVector2_t2430450063 * value)
	{
		___orScreenPosition2d_12 = value;
		Il2CppCodeGenWriteBarrier(&___orScreenPosition2d_12, value);
	}

	inline static int32_t get_offset_of_gameObjectList_13() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___gameObjectList_13)); }
	inline FsmArray_t527459893 * get_gameObjectList_13() const { return ___gameObjectList_13; }
	inline FsmArray_t527459893 ** get_address_of_gameObjectList_13() { return &___gameObjectList_13; }
	inline void set_gameObjectList_13(FsmArray_t527459893 * value)
	{
		___gameObjectList_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectList_13, value);
	}

	inline static int32_t get_offset_of_hitCount_14() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___hitCount_14)); }
	inline FsmInt_t1273009179 * get_hitCount_14() const { return ___hitCount_14; }
	inline FsmInt_t1273009179 ** get_address_of_hitCount_14() { return &___hitCount_14; }
	inline void set_hitCount_14(FsmInt_t1273009179 * value)
	{
		___hitCount_14 = value;
		Il2CppCodeGenWriteBarrier(&___hitCount_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of_pointer_16() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___pointer_16)); }
	inline PointerEventData_t1599784723 * get_pointer_16() const { return ___pointer_16; }
	inline PointerEventData_t1599784723 ** get_address_of_pointer_16() { return &___pointer_16; }
	inline void set_pointer_16(PointerEventData_t1599784723 * value)
	{
		___pointer_16 = value;
		Il2CppCodeGenWriteBarrier(&___pointer_16, value);
	}

	inline static int32_t get_offset_of_raycastResults_17() { return static_cast<int32_t>(offsetof(EventSystemCurrentRayCastAll_t1077445024, ___raycastResults_17)); }
	inline List_1_t3685274804 * get_raycastResults_17() const { return ___raycastResults_17; }
	inline List_1_t3685274804 ** get_address_of_raycastResults_17() { return &___raycastResults_17; }
	inline void set_raycastResults_17(List_1_t3685274804 * value)
	{
		___raycastResults_17 = value;
		Il2CppCodeGenWriteBarrier(&___raycastResults_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
