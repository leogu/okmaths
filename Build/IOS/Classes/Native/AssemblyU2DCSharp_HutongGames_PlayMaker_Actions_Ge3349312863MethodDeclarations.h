﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVector3XYZ
struct GetVector3XYZ_t3349312863;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::.ctor()
extern "C"  void GetVector3XYZ__ctor_m3031081973 (GetVector3XYZ_t3349312863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::Reset()
extern "C"  void GetVector3XYZ_Reset_m3476265888 (GetVector3XYZ_t3349312863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::OnEnter()
extern "C"  void GetVector3XYZ_OnEnter_m3559907770 (GetVector3XYZ_t3349312863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::OnUpdate()
extern "C"  void GetVector3XYZ_OnUpdate_m3414319353 (GetVector3XYZ_t3349312863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector3XYZ::DoGetVector3XYZ()
extern "C"  void GetVector3XYZ_DoGetVector3XYZ_m2292042385 (GetVector3XYZ_t3349312863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
