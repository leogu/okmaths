﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat
struct  uGuiInputFieldGetTextAsFloat_t2418060331  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::value
	FsmFloat_t937133978 * ___value_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::isFloat
	FsmBool_t664485696 * ___isFloat_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::isFloatEvent
	FsmEvent_t1258573736 * ___isFloatEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::isNotFloatEvent
	FsmEvent_t1258573736 * ___isNotFloatEvent_15;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::_inputField
	InputField_t1631627530 * ____inputField_17;
	// System.Single HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::_value
	float ____value_18;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::_success
	bool ____success_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ___value_12)); }
	inline FsmFloat_t937133978 * get_value_12() const { return ___value_12; }
	inline FsmFloat_t937133978 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmFloat_t937133978 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_isFloat_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ___isFloat_13)); }
	inline FsmBool_t664485696 * get_isFloat_13() const { return ___isFloat_13; }
	inline FsmBool_t664485696 ** get_address_of_isFloat_13() { return &___isFloat_13; }
	inline void set_isFloat_13(FsmBool_t664485696 * value)
	{
		___isFloat_13 = value;
		Il2CppCodeGenWriteBarrier(&___isFloat_13, value);
	}

	inline static int32_t get_offset_of_isFloatEvent_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ___isFloatEvent_14)); }
	inline FsmEvent_t1258573736 * get_isFloatEvent_14() const { return ___isFloatEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isFloatEvent_14() { return &___isFloatEvent_14; }
	inline void set_isFloatEvent_14(FsmEvent_t1258573736 * value)
	{
		___isFloatEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isFloatEvent_14, value);
	}

	inline static int32_t get_offset_of_isNotFloatEvent_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ___isNotFloatEvent_15)); }
	inline FsmEvent_t1258573736 * get_isNotFloatEvent_15() const { return ___isNotFloatEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotFloatEvent_15() { return &___isNotFloatEvent_15; }
	inline void set_isNotFloatEvent_15(FsmEvent_t1258573736 * value)
	{
		___isNotFloatEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___isNotFloatEvent_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__inputField_17() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ____inputField_17)); }
	inline InputField_t1631627530 * get__inputField_17() const { return ____inputField_17; }
	inline InputField_t1631627530 ** get_address_of__inputField_17() { return &____inputField_17; }
	inline void set__inputField_17(InputField_t1631627530 * value)
	{
		____inputField_17 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_17, value);
	}

	inline static int32_t get_offset_of__value_18() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ____value_18)); }
	inline float get__value_18() const { return ____value_18; }
	inline float* get_address_of__value_18() { return &____value_18; }
	inline void set__value_18(float value)
	{
		____value_18 = value;
	}

	inline static int32_t get_offset_of__success_19() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsFloat_t2418060331, ____success_19)); }
	inline bool get__success_19() const { return ____success_19; }
	inline bool* get_address_of__success_19() { return &____success_19; }
	inline void set__success_19(bool value)
	{
		____success_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
