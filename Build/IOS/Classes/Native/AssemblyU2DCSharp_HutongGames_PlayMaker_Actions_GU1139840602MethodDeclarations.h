﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace
struct GUILayoutFlexibleSpace_t1139840602;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace::.ctor()
extern "C"  void GUILayoutFlexibleSpace__ctor_m1294651952 (GUILayoutFlexibleSpace_t1139840602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace::Reset()
extern "C"  void GUILayoutFlexibleSpace_Reset_m2365913127 (GUILayoutFlexibleSpace_t1139840602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFlexibleSpace::OnGUI()
extern "C"  void GUILayoutFlexibleSpace_OnGUI_m3876326364 (GUILayoutFlexibleSpace_t1139840602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
