﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertFloatToString
struct ConvertFloatToString_t2174958833;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::.ctor()
extern "C"  void ConvertFloatToString__ctor_m3230376067 (ConvertFloatToString_t2174958833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::Reset()
extern "C"  void ConvertFloatToString_Reset_m4066658298 (ConvertFloatToString_t2174958833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::OnEnter()
extern "C"  void ConvertFloatToString_OnEnter_m737708956 (ConvertFloatToString_t2174958833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::OnUpdate()
extern "C"  void ConvertFloatToString_OnUpdate_m3675882283 (ConvertFloatToString_t2174958833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertFloatToString::DoConvertFloatToString()
extern "C"  void ConvertFloatToString_DoConvertFloatToString_m1797704385 (ConvertFloatToString_t2174958833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
