﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DestroyArrayList
struct  DestroyArrayList_t1040878829  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DestroyArrayList::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DestroyArrayList::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DestroyArrayList::successEvent
	FsmEvent_t1258573736 * ___successEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DestroyArrayList::notFoundEvent
	FsmEvent_t1258573736 * ___notFoundEvent_15;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(DestroyArrayList_t1040878829, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(DestroyArrayList_t1040878829, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_successEvent_14() { return static_cast<int32_t>(offsetof(DestroyArrayList_t1040878829, ___successEvent_14)); }
	inline FsmEvent_t1258573736 * get_successEvent_14() const { return ___successEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_successEvent_14() { return &___successEvent_14; }
	inline void set_successEvent_14(FsmEvent_t1258573736 * value)
	{
		___successEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_14, value);
	}

	inline static int32_t get_offset_of_notFoundEvent_15() { return static_cast<int32_t>(offsetof(DestroyArrayList_t1040878829, ___notFoundEvent_15)); }
	inline FsmEvent_t1258573736 * get_notFoundEvent_15() const { return ___notFoundEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_notFoundEvent_15() { return &___notFoundEvent_15; }
	inline void set_notFoundEvent_15(FsmEvent_t1258573736 * value)
	{
		___notFoundEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___notFoundEvent_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
