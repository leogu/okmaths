﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextEnd
struct uGuiInputFieldMoveCaretToTextEnd_t823827267;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextEnd::.ctor()
extern "C"  void uGuiInputFieldMoveCaretToTextEnd__ctor_m539420957 (uGuiInputFieldMoveCaretToTextEnd_t823827267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextEnd::Reset()
extern "C"  void uGuiInputFieldMoveCaretToTextEnd_Reset_m1609882868 (uGuiInputFieldMoveCaretToTextEnd_t823827267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextEnd::OnEnter()
extern "C"  void uGuiInputFieldMoveCaretToTextEnd_OnEnter_m1741365566 (uGuiInputFieldMoveCaretToTextEnd_t823827267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextEnd::DoAction()
extern "C"  void uGuiInputFieldMoveCaretToTextEnd_DoAction_m2799278424 (uGuiInputFieldMoveCaretToTextEnd_t823827267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
