﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Animator
struct Animator_t69676727;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t670468573;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal3089653344.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_AvatarTarget193923574.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask296470556.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1322940928.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode258421960.h"

// System.Boolean UnityEngine.Animator::get_isHuman()
extern "C"  bool Animator_get_isHuman_m1301163560 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_humanScale()
extern "C"  float Animator_get_humanScale_m658813206 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloat(System.Int32)
extern "C"  float Animator_GetFloat_m3714897923 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
extern "C"  void Animator_SetFloat_m2956422680 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m871614584 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBool(System.Int32)
extern "C"  bool Animator_GetBool_m919083767 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
extern "C"  void Animator_SetBool_m2272870950 (Animator_t69676727 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetInteger(System.Int32)
extern "C"  int32_t Animator_GetInteger_m1012099561 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetInteger(System.Int32,System.Int32)
extern "C"  void Animator_SetInteger_m1464706502 (Animator_t69676727 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m3418492570 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m865269317 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.String)
extern "C"  bool Animator_IsParameterControlledByCurve_m4289614957 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C"  Vector3_t2243707580  Animator_get_deltaPosition_m1710146426 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_deltaPosition_m3555533205 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_deltaRotation()
extern "C"  Quaternion_t4030073918  Animator_get_deltaRotation_m1211723535 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_deltaRotation_m1178344088 (Animator_t69676727 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
extern "C"  Vector3_t2243707580  Animator_get_rootPosition_m240413096 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_rootPosition_m1694908883 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C"  Quaternion_t4030073918  Animator_get_rootRotation_m1814890025 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_rootRotation_m4269311362 (Animator_t69676727 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_applyRootMotion()
extern "C"  bool Animator_get_applyRootMotion_m915232763 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C"  void Animator_set_applyRootMotion_m635228566 (Animator_t69676727 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_gravityWeight()
extern "C"  float Animator_get_gravityWeight_m449373735 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPosition()
extern "C"  Vector3_t2243707580  Animator_get_bodyPosition_m3886685512 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_bodyPosition(UnityEngine.Vector3)
extern "C"  void Animator_set_bodyPosition_m3816012379 (Animator_t69676727 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetBodyPositionInternal()
extern "C"  Vector3_t2243707580  Animator_GetBodyPositionInternal_m1960999654 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetBodyPositionInternal_m3696917400 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBodyPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetBodyPositionInternal_m2126057571 (Animator_t69676727 * __this, Vector3_t2243707580  ___bodyPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetBodyPositionInternal_m4293717132 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___bodyPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotation()
extern "C"  Quaternion_t4030073918  Animator_get_bodyRotation_m2990103407 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_bodyRotation(UnityEngine.Quaternion)
extern "C"  void Animator_set_bodyRotation_m3092584822 (Animator_t69676727 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetBodyRotationInternal()
extern "C"  Quaternion_t4030073918  Animator_GetBodyRotationInternal_m208861807 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetBodyRotationInternal_m2953975611 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Quaternion_t4030073918 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBodyRotationInternal(UnityEngine.Quaternion)
extern "C"  void Animator_SetBodyRotationInternal_m1205087780 (Animator_t69676727 * __this, Quaternion_t4030073918  ___bodyRotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetBodyRotationInternal_m1212579919 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Quaternion_t4030073918 * ___bodyRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPosition(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t2243707580  Animator_GetIKPosition_m184972236 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPositionInternal(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t2243707580  Animator_GetIKPositionInternal_m2286444847 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetIKPositionInternal_m2121966543 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Vector3_t2243707580 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPosition_m3754744543 (Animator_t69676727 * __this, int32_t ___goal0, Vector3_t2243707580  ___goalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPositionInternal_m627652436 (Animator_t69676727 * __this, int32_t ___goal0, Vector3_t2243707580  ___goalPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetIKPositionInternal_m2542413499 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Vector3_t2243707580 * ___goalPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotation(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t4030073918  Animator_GetIKRotation_m1400774255 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotationInternal(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t4030073918  Animator_GetIKRotationInternal_m879540238 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetIKRotationInternal_m3409638438 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Quaternion_t4030073918 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotation_m4119847878 (Animator_t69676727 * __this, int32_t ___goal0, Quaternion_t4030073918  ___goalRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationInternal(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotationInternal_m2182387227 (Animator_t69676727 * __this, int32_t ___goal0, Quaternion_t4030073918  ___goalRotation1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetIKRotationInternal_m4087004338 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Quaternion_t4030073918 * ___goalRotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKPositionWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeight_m2727777112 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeightInternal_m4211544419 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeight_m2782442723 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeightInternal_m3301773304 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKRotationWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeight_m3754472649 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeightInternal_m953924988 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeight_m3417634268 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeightInternal_m3149308873 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPosition_m2085986408 (Animator_t69676727 * __this, Vector3_t2243707580  ___lookAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPositionInternal_m1849644817 (Animator_t69676727 * __this, Vector3_t2243707580  ___lookAtPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m2150993438 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___lookAtPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m2799625122 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3239175203 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m1126855746 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single)
extern "C"  void Animator_SetLookAtWeight_m3938735683 (Animator_t69676727 * __this, float ___weight0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3872681091 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeightInternal_m1849982518 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
extern "C"  void Animator_set_stabilizeFeet_m224067765 (Animator_t69676727 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C"  int32_t Animator_get_layerCount_m2341760273 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Animator::GetLayerName(System.Int32)
extern "C"  String_t* Animator_GetLayerName_m1585786478 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C"  float Animator_GetLayerWeight_m2228986294 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C"  void Animator_SetLayerWeight_m2914961795 (Animator_t69676727 * __this, int32_t ___layerIndex0, float ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t2577870592  Animator_GetCurrentAnimatorStateInfo_m1931338898 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t2577870592  Animator_GetNextAnimatorStateInfo_m3085812524 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
extern "C"  AnimatorTransitionInfo_t2410896200  Animator_GetAnimatorTransitionInfo_m2734940813 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
extern "C"  bool Animator_IsInTransition_m3442920577 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_feetPivotActive()
extern "C"  float Animator_get_feetPivotActive_m2453426283 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
extern "C"  void Animator_set_feetPivotActive_m3070161808 (Animator_t69676727 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_pivotWeight()
extern "C"  float Animator_get_pivotWeight_m1286872529 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_pivotPosition()
extern "C"  Vector3_t2243707580  Animator_get_pivotPosition_m217711564 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_pivotPosition_m612074601 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C"  void Animator_MatchTarget_m1237878570 (Animator_t69676727 * __this, Vector3_t2243707580  ___matchPosition0, Quaternion_t4030073918  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t296470556  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C"  void Animator_INTERNAL_CALL_MatchTarget_m156824911 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___matchPosition1, Quaternion_t4030073918 * ___matchRotation2, int32_t ___targetBodyPart3, MatchTargetWeightMask_t296470556 * ___weightMask4, float ___startNormalizedTime5, float ___targetNormalizedTime6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
extern "C"  void Animator_InterruptMatchTarget_m3522396796 (Animator_t69676727 * __this, bool ___completeMatch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_isMatchingTarget()
extern "C"  bool Animator_get_isMatchingTarget_m2982519915 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_speed()
extern "C"  float Animator_get_speed_m3591305940 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C"  void Animator_set_speed_m3511108817 (Animator_t69676727 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m1296137205 (Animator_t69676727 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m3234175154 (Animator_t69676727 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m2181614708 (Animator_t69676727 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m3632052371 (Animator_t69676727 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
extern "C"  void Animator_SetTarget_m3396402303 (Animator_t69676727 * __this, int32_t ___targetIndex0, float ___targetNormalizedTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_targetPosition()
extern "C"  Vector3_t2243707580  Animator_get_targetPosition_m1133944793 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_targetPosition_m1320711800 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_targetRotation()
extern "C"  Quaternion_t4030073918  Animator_get_targetRotation_m2166002050 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_targetRotation_m1157869071 (Animator_t69676727 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C"  Transform_t3275118058 * Animator_GetBoneTransform_m1849768799 (Animator_t69676727 * __this, int32_t ___humanBoneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
extern "C"  int32_t Animator_get_cullingMode_m2883110248 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
extern "C"  void Animator_set_cullingMode_m2321274215 (Animator_t69676727 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StartPlayback()
extern "C"  void Animator_StartPlayback_m1725683593 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StopPlayback()
extern "C"  void Animator_StopPlayback_m419679945 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_playbackTime()
extern "C"  float Animator_get_playbackTime_m3236275835 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_playbackTime(System.Single)
extern "C"  void Animator_set_playbackTime_m2149309450 (Animator_t69676727 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StartRecording(System.Int32)
extern "C"  void Animator_StartRecording_m4246916954 (Animator_t69676727 * __this, int32_t ___frameCount0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::StopRecording()
extern "C"  void Animator_StopRecording_m3875895403 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_recorderStartTime()
extern "C"  float Animator_get_recorderStartTime_m4211465364 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_recorderStopTime()
extern "C"  float Animator_get_recorderStopTime_m1804781412 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C"  RuntimeAnimatorController_t670468573 * Animator_get_runtimeAnimatorController_m652575931 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m3313850714 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern "C"  void Animator_CheckIfInIKPass_m4048236447 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C"  bool Animator_CheckIfInIKPassInternal_m1624362606 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C"  void Animator_SetFloatID_m315788701 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C"  float Animator_GetFloatID_m2459156264 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C"  void Animator_SetBoolID_m1875705373 (Animator_t69676727 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C"  bool Animator_GetBoolID_m618935642 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C"  void Animator_SetIntegerID_m1279848667 (Animator_t69676727 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C"  int32_t Animator_GetIntegerID_m224950522 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m2002790359 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1445965342 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C"  bool Animator_IsParameterControlledByCurveString_m1958306560 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatIDDamp_m3704956665 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
extern "C"  bool Animator_get_layersAffectMassCenter_m635095639 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
extern "C"  void Animator_set_layersAffectMassCenter_m2611371262 (Animator_t69676727 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
extern "C"  float Animator_get_leftFeetBottomHeight_m3607592682 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
extern "C"  float Animator_get_rightFeetBottomHeight_m276028979 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C"  bool Animator_get_logWarnings_m485644130 (Animator_t69676727 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
