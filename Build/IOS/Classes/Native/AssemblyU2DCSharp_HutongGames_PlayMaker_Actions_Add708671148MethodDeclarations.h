﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddComponent
struct AddComponent_t708671148;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddComponent::.ctor()
extern "C"  void AddComponent__ctor_m793921790 (AddComponent_t708671148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::Reset()
extern "C"  void AddComponent_Reset_m3643031421 (AddComponent_t708671148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::OnEnter()
extern "C"  void AddComponent_OnEnter_m111710829 (AddComponent_t708671148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::OnExit()
extern "C"  void AddComponent_OnExit_m311605601 (AddComponent_t708671148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddComponent::DoAddComponent()
extern "C"  void AddComponent_DoAddComponent_m3216846017 (AddComponent_t708671148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
