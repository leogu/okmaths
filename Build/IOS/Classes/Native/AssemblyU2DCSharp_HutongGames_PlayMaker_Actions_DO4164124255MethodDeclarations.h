﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade
struct DOTweenCanvasGroupFade_t4164124255;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::.ctor()
extern "C"  void DOTweenCanvasGroupFade__ctor_m2890844193 (DOTweenCanvasGroupFade_t4164124255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::Reset()
extern "C"  void DOTweenCanvasGroupFade_Reset_m2054267696 (DOTweenCanvasGroupFade_t4164124255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::OnEnter()
extern "C"  void DOTweenCanvasGroupFade_OnEnter_m1470632650 (DOTweenCanvasGroupFade_t4164124255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::<OnEnter>m__36()
extern "C"  void DOTweenCanvasGroupFade_U3COnEnterU3Em__36_m831017272 (DOTweenCanvasGroupFade_t4164124255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::<OnEnter>m__37()
extern "C"  void DOTweenCanvasGroupFade_U3COnEnterU3Em__37_m689854771 (DOTweenCanvasGroupFade_t4164124255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
