﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmOwnerDefault[]
struct FsmOwnerDefaultU5BU5D_t4036794521;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListConcat
struct  ArrayListConcat_t163014501  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListConcat::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListConcat::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmOwnerDefault[] HutongGames.PlayMaker.Actions.ArrayListConcat::arrayListGameObjectTargets
	FsmOwnerDefaultU5BU5D_t4036794521* ___arrayListGameObjectTargets_14;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.ArrayListConcat::referenceTargets
	FsmStringU5BU5D_t2699231328* ___referenceTargets_15;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListConcat_t163014501, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListConcat_t163014501, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_arrayListGameObjectTargets_14() { return static_cast<int32_t>(offsetof(ArrayListConcat_t163014501, ___arrayListGameObjectTargets_14)); }
	inline FsmOwnerDefaultU5BU5D_t4036794521* get_arrayListGameObjectTargets_14() const { return ___arrayListGameObjectTargets_14; }
	inline FsmOwnerDefaultU5BU5D_t4036794521** get_address_of_arrayListGameObjectTargets_14() { return &___arrayListGameObjectTargets_14; }
	inline void set_arrayListGameObjectTargets_14(FsmOwnerDefaultU5BU5D_t4036794521* value)
	{
		___arrayListGameObjectTargets_14 = value;
		Il2CppCodeGenWriteBarrier(&___arrayListGameObjectTargets_14, value);
	}

	inline static int32_t get_offset_of_referenceTargets_15() { return static_cast<int32_t>(offsetof(ArrayListConcat_t163014501, ___referenceTargets_15)); }
	inline FsmStringU5BU5D_t2699231328* get_referenceTargets_15() const { return ___referenceTargets_15; }
	inline FsmStringU5BU5D_t2699231328** get_address_of_referenceTargets_15() { return &___referenceTargets_15; }
	inline void set_referenceTargets_15(FsmStringU5BU5D_t2699231328* value)
	{
		___referenceTargets_15 = value;
		Il2CppCodeGenWriteBarrier(&___referenceTargets_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
