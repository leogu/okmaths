﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1180903644.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ani322151382.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2712624701_gshared (InternalEnumerator_1_t1180903644 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2712624701(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1180903644 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2712624701_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4106838873_gshared (InternalEnumerator_1_t1180903644 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4106838873(__this, method) ((  void (*) (InternalEnumerator_1_t1180903644 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4106838873_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2398798705_gshared (InternalEnumerator_1_t1180903644 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2398798705(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1180903644 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2398798705_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3575074390_gshared (InternalEnumerator_1_t1180903644 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3575074390(__this, method) ((  void (*) (InternalEnumerator_1_t1180903644 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3575074390_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3487755673_gshared (InternalEnumerator_1_t1180903644 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3487755673(__this, method) ((  bool (*) (InternalEnumerator_1_t1180903644 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3487755673_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.AnimateFsmAction/Calculation>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3412432972_gshared (InternalEnumerator_1_t1180903644 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3412432972(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1180903644 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3412432972_gshared)(__this, method)
