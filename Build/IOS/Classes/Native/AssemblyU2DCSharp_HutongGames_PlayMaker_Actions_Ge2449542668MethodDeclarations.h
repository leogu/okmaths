﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d
struct GetNextOverlapCircle2d_t2449542668;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::.ctor()
extern "C"  void GetNextOverlapCircle2d__ctor_m1512036682 (GetNextOverlapCircle2d_t2449542668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::Reset()
extern "C"  void GetNextOverlapCircle2d_Reset_m2304434041 (GetNextOverlapCircle2d_t2449542668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::OnEnter()
extern "C"  void GetNextOverlapCircle2d_OnEnter_m3656665545 (GetNextOverlapCircle2d_t2449542668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::DoGetNextCollider()
extern "C"  void GetNextOverlapCircle2d_DoGetNextCollider_m1104377848 (GetNextOverlapCircle2d_t2449542668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapCircle2d::GetOverlapCircleAll()
extern "C"  Collider2DU5BU5D_t3535523695* GetNextOverlapCircle2d_GetOverlapCircleAll_m583757404 (GetNextOverlapCircle2d_t2449542668 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
