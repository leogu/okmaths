﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSpeed2d
struct GetSpeed2d_t3446440751;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::.ctor()
extern "C"  void GetSpeed2d__ctor_m657999173 (GetSpeed2d_t3446440751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::Reset()
extern "C"  void GetSpeed2d_Reset_m1488891284 (GetSpeed2d_t3446440751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::OnEnter()
extern "C"  void GetSpeed2d_OnEnter_m1986032102 (GetSpeed2d_t3446440751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::OnUpdate()
extern "C"  void GetSpeed2d_OnUpdate_m3478481937 (GetSpeed2d_t3446440751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSpeed2d::DoGetSpeed()
extern "C"  void GetSpeed2d_DoGetSpeed_m2789670143 (GetSpeed2d_t3446440751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
