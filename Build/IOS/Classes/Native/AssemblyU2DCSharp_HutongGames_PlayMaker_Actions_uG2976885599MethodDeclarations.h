﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax
struct uGuiSliderSetMinMax_t2976885599;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::.ctor()
extern "C"  void uGuiSliderSetMinMax__ctor_m1244367881 (uGuiSliderSetMinMax_t2976885599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::Reset()
extern "C"  void uGuiSliderSetMinMax_Reset_m1992492820 (uGuiSliderSetMinMax_t2976885599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::OnEnter()
extern "C"  void uGuiSliderSetMinMax_OnEnter_m4270540534 (uGuiSliderSetMinMax_t2976885599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::OnUpdate()
extern "C"  void uGuiSliderSetMinMax_OnUpdate_m1507966525 (uGuiSliderSetMinMax_t2976885599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::DoSetValue()
extern "C"  void uGuiSliderSetMinMax_DoSetValue_m2539257335 (uGuiSliderSetMinMax_t2976885599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::OnExit()
extern "C"  void uGuiSliderSetMinMax_OnExit_m2516449762 (uGuiSliderSetMinMax_t2976885599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
