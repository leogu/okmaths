﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan
struct GetAtan_t307145834;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan::.ctor()
extern "C"  void GetAtan__ctor_m2178842540 (GetAtan_t307145834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::Reset()
extern "C"  void GetAtan_Reset_m2841877447 (GetAtan_t307145834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::OnEnter()
extern "C"  void GetAtan_OnEnter_m1364492511 (GetAtan_t307145834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::OnUpdate()
extern "C"  void GetAtan_OnUpdate_m1322496146 (GetAtan_t307145834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan::DoATan()
extern "C"  void GetAtan_DoATan_m512415651 (GetAtan_t307145834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
