﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionBaseAction
struct QuaternionBaseAction_t3869353585;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionBaseAction::.ctor()
extern "C"  void QuaternionBaseAction__ctor_m2777676569 (QuaternionBaseAction_t3869353585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionBaseAction::Awake()
extern "C"  void QuaternionBaseAction_Awake_m367862070 (QuaternionBaseAction_t3869353585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
