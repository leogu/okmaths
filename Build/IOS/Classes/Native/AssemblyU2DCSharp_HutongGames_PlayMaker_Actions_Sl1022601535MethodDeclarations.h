﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Sleep
struct Sleep_t1022601535;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Sleep::.ctor()
extern "C"  void Sleep__ctor_m3883859613 (Sleep_t1022601535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep::Reset()
extern "C"  void Sleep_Reset_m2196110872 (Sleep_t1022601535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep::OnEnter()
extern "C"  void Sleep_OnEnter_m949070730 (Sleep_t1022601535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep::DoSleep()
extern "C"  void Sleep_DoSleep_m4166806385 (Sleep_t1022601535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
