﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent
struct  uGuiToggleOnClickEvent_t1450544853  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::sendEvent
	FsmEvent_t1258573736 * ___sendEvent_12;
	// UnityEngine.UI.Toggle HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::_toggle
	Toggle_t3976754468 * ____toggle_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiToggleOnClickEvent_t1450544853, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_sendEvent_12() { return static_cast<int32_t>(offsetof(uGuiToggleOnClickEvent_t1450544853, ___sendEvent_12)); }
	inline FsmEvent_t1258573736 * get_sendEvent_12() const { return ___sendEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_sendEvent_12() { return &___sendEvent_12; }
	inline void set_sendEvent_12(FsmEvent_t1258573736 * value)
	{
		___sendEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_12, value);
	}

	inline static int32_t get_offset_of__toggle_13() { return static_cast<int32_t>(offsetof(uGuiToggleOnClickEvent_t1450544853, ____toggle_13)); }
	inline Toggle_t3976754468 * get__toggle_13() const { return ____toggle_13; }
	inline Toggle_t3976754468 ** get_address_of__toggle_13() { return &____toggle_13; }
	inline void set__toggle_13(Toggle_t3976754468 * value)
	{
		____toggle_13 = value;
		Il2CppCodeGenWriteBarrier(&____toggle_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
