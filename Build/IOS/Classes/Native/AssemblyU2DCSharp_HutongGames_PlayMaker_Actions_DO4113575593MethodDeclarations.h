﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTextBlendableColor
struct DOTweenTextBlendableColor_t4113575593;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTextBlendableColor::.ctor()
extern "C"  void DOTweenTextBlendableColor__ctor_m3764459947 (DOTweenTextBlendableColor_t4113575593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextBlendableColor::Reset()
extern "C"  void DOTweenTextBlendableColor_Reset_m1071374438 (DOTweenTextBlendableColor_t4113575593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextBlendableColor::OnEnter()
extern "C"  void DOTweenTextBlendableColor_OnEnter_m1558321216 (DOTweenTextBlendableColor_t4113575593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextBlendableColor::<OnEnter>m__9A()
extern "C"  void DOTweenTextBlendableColor_U3COnEnterU3Em__9A_m904451605 (DOTweenTextBlendableColor_t4113575593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextBlendableColor::<OnEnter>m__9B()
extern "C"  void DOTweenTextBlendableColor_U3COnEnterU3Em__9B_m904451444 (DOTweenTextBlendableColor_t4113575593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
