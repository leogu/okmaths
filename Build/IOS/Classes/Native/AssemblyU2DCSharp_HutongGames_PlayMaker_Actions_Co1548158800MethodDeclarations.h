﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ControllerIsGrounded
struct ControllerIsGrounded_t1548158800;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::.ctor()
extern "C"  void ControllerIsGrounded__ctor_m540604614 (ControllerIsGrounded_t1548158800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::Reset()
extern "C"  void ControllerIsGrounded_Reset_m3114390109 (ControllerIsGrounded_t1548158800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::OnEnter()
extern "C"  void ControllerIsGrounded_OnEnter_m699186829 (ControllerIsGrounded_t1548158800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::OnUpdate()
extern "C"  void ControllerIsGrounded_OnUpdate_m950964784 (ControllerIsGrounded_t1548158800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ControllerIsGrounded::DoControllerIsGrounded()
extern "C"  void ControllerIsGrounded_DoControllerIsGrounded_m872646529 (ControllerIsGrounded_t1548158800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
