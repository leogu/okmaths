﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLookAtPosition
struct DOTweenTransformLookAtPosition_t4190778859;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtPosition::.ctor()
extern "C"  void DOTweenTransformLookAtPosition__ctor_m1571861703 (DOTweenTransformLookAtPosition_t4190778859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtPosition::Reset()
extern "C"  void DOTweenTransformLookAtPosition_Reset_m1387083664 (DOTweenTransformLookAtPosition_t4190778859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtPosition::OnEnter()
extern "C"  void DOTweenTransformLookAtPosition_OnEnter_m450514086 (DOTweenTransformLookAtPosition_t4190778859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtPosition::<OnEnter>m__C2()
extern "C"  void DOTweenTransformLookAtPosition_U3COnEnterU3Em__C2_m4152523220 (DOTweenTransformLookAtPosition_t4190778859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtPosition::<OnEnter>m__C3()
extern "C"  void DOTweenTransformLookAtPosition_U3COnEnterU3Em__C3_m4293685721 (DOTweenTransformLookAtPosition_t4190778859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
