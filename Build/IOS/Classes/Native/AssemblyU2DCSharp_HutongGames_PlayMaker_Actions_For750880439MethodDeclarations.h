﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ForwardAllEvents
struct ForwardAllEvents_t750880439;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"

// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::.ctor()
extern "C"  void ForwardAllEvents__ctor_m3437611923 (ForwardAllEvents_t750880439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ForwardAllEvents::Reset()
extern "C"  void ForwardAllEvents_Reset_m828574788 (ForwardAllEvents_t750880439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ForwardAllEvents::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardAllEvents_Event_m55993001 (ForwardAllEvents_t750880439 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
