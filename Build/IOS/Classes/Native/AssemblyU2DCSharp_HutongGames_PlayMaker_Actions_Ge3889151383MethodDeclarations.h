﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo
struct GetAnimatorCurrentStateInfo_t3889151383;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::.ctor()
extern "C"  void GetAnimatorCurrentStateInfo__ctor_m199377055 (GetAnimatorCurrentStateInfo_t3889151383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::Reset()
extern "C"  void GetAnimatorCurrentStateInfo_Reset_m2596690772 (GetAnimatorCurrentStateInfo_t3889151383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnEnter()
extern "C"  void GetAnimatorCurrentStateInfo_OnEnter_m3993607986 (GetAnimatorCurrentStateInfo_t3889151383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::OnActionUpdate()
extern "C"  void GetAnimatorCurrentStateInfo_OnActionUpdate_m3411985101 (GetAnimatorCurrentStateInfo_t3889151383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfo::GetLayerInfo()
extern "C"  void GetAnimatorCurrentStateInfo_GetLayerInfo_m914053794 (GetAnimatorCurrentStateInfo_t3889151383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
