﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StopAnimation
struct StopAnimation_t3771465676;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StopAnimation::.ctor()
extern "C"  void StopAnimation__ctor_m2610979176 (StopAnimation_t3771465676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::Reset()
extern "C"  void StopAnimation_Reset_m558042605 (StopAnimation_t3771465676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::OnEnter()
extern "C"  void StopAnimation_OnEnter_m1904408845 (StopAnimation_t3771465676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopAnimation::DoStopAnimation()
extern "C"  void StopAnimation_DoStopAnimation_m1430861869 (StopAnimation_t3771465676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
