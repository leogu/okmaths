﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToColor
struct ConvertBoolToColor_t37650447;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::.ctor()
extern "C"  void ConvertBoolToColor__ctor_m3419990621 (ConvertBoolToColor_t37650447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::Reset()
extern "C"  void ConvertBoolToColor_Reset_m4214045212 (ConvertBoolToColor_t37650447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::OnEnter()
extern "C"  void ConvertBoolToColor_OnEnter_m2893853958 (ConvertBoolToColor_t37650447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::OnUpdate()
extern "C"  void ConvertBoolToColor_OnUpdate_m4187542641 (ConvertBoolToColor_t37650447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToColor::DoConvertBoolToColor()
extern "C"  void ConvertBoolToColor_DoConvertBoolToColor_m2529240833 (ConvertBoolToColor_t37650447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
