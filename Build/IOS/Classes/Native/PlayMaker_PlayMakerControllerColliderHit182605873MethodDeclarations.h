﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerControllerColliderHit
struct PlayMakerControllerColliderHit_t182605873;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"

// System.Void PlayMakerControllerColliderHit::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void PlayMakerControllerColliderHit_OnControllerColliderHit_m1972322520 (PlayMakerControllerColliderHit_t182605873 * __this, ControllerColliderHit_t4070855101 * ___hitCollider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerControllerColliderHit::.ctor()
extern "C"  void PlayMakerControllerColliderHit__ctor_m101360368 (PlayMakerControllerColliderHit_t182605873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
