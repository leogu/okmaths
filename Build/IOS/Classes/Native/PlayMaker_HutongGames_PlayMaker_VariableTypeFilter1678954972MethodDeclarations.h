﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.VariableTypeFilter
struct VariableTypeFilter_t1678954972;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.VariableTypeFilter::.ctor()
extern "C"  void VariableTypeFilter__ctor_m3633182603 (VariableTypeFilter_t1678954972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
