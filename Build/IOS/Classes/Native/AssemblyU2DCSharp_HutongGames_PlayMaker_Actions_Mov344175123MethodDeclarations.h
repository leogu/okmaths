﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MoveTowards
struct MoveTowards_t344175123;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void HutongGames.PlayMaker.Actions.MoveTowards::.ctor()
extern "C"  void MoveTowards__ctor_m3812437571 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::Reset()
extern "C"  void MoveTowards_Reset_m233294800 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::OnUpdate()
extern "C"  void MoveTowards_OnUpdate_m1068160627 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MoveTowards::DoMoveTowards()
extern "C"  void MoveTowards_DoMoveTowards_m4207370829 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.MoveTowards::UpdateTargetPos()
extern "C"  bool MoveTowards_UpdateTargetPos_m946280343 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::GetTargetPos()
extern "C"  Vector3_t2243707580  MoveTowards_GetTargetPos_m2444034500 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.MoveTowards::GetTargetPosWithVertical()
extern "C"  Vector3_t2243707580  MoveTowards_GetTargetPosWithVertical_m719630694 (MoveTowards_t344175123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
