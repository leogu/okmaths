﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AxisEvent
struct AxisEvent_t3650138295;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AxisEvent::.ctor()
extern "C"  void AxisEvent__ctor_m1283236625 (AxisEvent_t3650138295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AxisEvent::Reset()
extern "C"  void AxisEvent_Reset_m2031361068 (AxisEvent_t3650138295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AxisEvent::OnUpdate()
extern "C"  void AxisEvent_OnUpdate_m789029157 (AxisEvent_t3650138295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
