﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyJump
struct DOTweenRigidbodyJump_t1232043301;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyJump::.ctor()
extern "C"  void DOTweenRigidbodyJump__ctor_m203892967 (DOTweenRigidbodyJump_t1232043301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyJump::Reset()
extern "C"  void DOTweenRigidbodyJump_Reset_m2779838646 (DOTweenRigidbodyJump_t1232043301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyJump::OnEnter()
extern "C"  void DOTweenRigidbodyJump_OnEnter_m882571600 (DOTweenRigidbodyJump_t1232043301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyJump::<OnEnter>m__82()
extern "C"  void DOTweenRigidbodyJump_U3COnEnterU3Em__82_m3372841427 (DOTweenRigidbodyJump_t1232043301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyJump::<OnEnter>m__83()
extern "C"  void DOTweenRigidbodyJump_U3COnEnterU3Em__83_m1826960462 (DOTweenRigidbodyJump_t1232043301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
