﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.LinkerData
struct LinkerData_t3223412941;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_3223412941.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::.ctor()
extern "C"  void LinkerData__ctor_m813679469 (LinkerData_t3223412941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::get_DebugAll()
extern "C"  bool LinkerData_get_DebugAll_m1561454348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Ecosystem.Utils.LinkerData HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::get_instance()
extern "C"  LinkerData_t3223412941 * LinkerData_get_instance_m1199558942 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::set_instance(HutongGames.PlayMaker.Ecosystem.Utils.LinkerData)
extern "C"  void LinkerData_set_instance_m2650389275 (Il2CppObject * __this /* static, unused */, LinkerData_t3223412941 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::RegisterClassDependancy(System.String,System.String)
extern "C"  void LinkerData_RegisterClassDependancy_m2790984705 (Il2CppObject * __this /* static, unused */, String_t* ___assemblyName0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::RegisterLinkerEntry(System.String,System.String)
extern "C"  void LinkerData_RegisterLinkerEntry_m2284796591 (LinkerData_t3223412941 * __this, String_t* ___assemblyName0, String_t* ___typeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::OnEnable()
extern "C"  void LinkerData_OnEnable_m1867139869 (LinkerData_t3223412941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
