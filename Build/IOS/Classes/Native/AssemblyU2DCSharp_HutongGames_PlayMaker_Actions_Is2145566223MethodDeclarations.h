﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsKinematic2d
struct IsKinematic2d_t2145566223;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsKinematic2d::.ctor()
extern "C"  void IsKinematic2d__ctor_m3009924005 (IsKinematic2d_t2145566223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic2d::Reset()
extern "C"  void IsKinematic2d_Reset_m472130928 (IsKinematic2d_t2145566223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic2d::OnEnter()
extern "C"  void IsKinematic2d_OnEnter_m2494839018 (IsKinematic2d_t2145566223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic2d::OnUpdate()
extern "C"  void IsKinematic2d_OnUpdate_m957838761 (IsKinematic2d_t2145566223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic2d::DoIsKinematic()
extern "C"  void IsKinematic2d_DoIsKinematic_m3183142967 (IsKinematic2d_t2145566223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
