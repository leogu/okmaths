﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraFarClipPlane
struct DOTweenCameraFarClipPlane_t3646922036;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFarClipPlane::.ctor()
extern "C"  void DOTweenCameraFarClipPlane__ctor_m2379551246 (DOTweenCameraFarClipPlane_t3646922036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFarClipPlane::Reset()
extern "C"  void DOTweenCameraFarClipPlane_Reset_m3258869009 (DOTweenCameraFarClipPlane_t3646922036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFarClipPlane::OnEnter()
extern "C"  void DOTweenCameraFarClipPlane_OnEnter_m1819580121 (DOTweenCameraFarClipPlane_t3646922036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFarClipPlane::<OnEnter>m__26()
extern "C"  void DOTweenCameraFarClipPlane_U3COnEnterU3Em__26_m2825133974 (DOTweenCameraFarClipPlane_t3646922036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFarClipPlane::<OnEnter>m__27()
extern "C"  void DOTweenCameraFarClipPlane_U3COnEnterU3Em__27_m2825133879 (DOTweenCameraFarClipPlane_t3646922036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
