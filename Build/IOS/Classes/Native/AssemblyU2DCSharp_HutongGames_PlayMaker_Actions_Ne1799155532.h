﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount
struct  NetworkGetConnectionsCount_t1799155532  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::connectionsCount
	FsmInt_t1273009179 * ___connectionsCount_11;
	// System.Boolean HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::everyFrame
	bool ___everyFrame_12;

public:
	inline static int32_t get_offset_of_connectionsCount_11() { return static_cast<int32_t>(offsetof(NetworkGetConnectionsCount_t1799155532, ___connectionsCount_11)); }
	inline FsmInt_t1273009179 * get_connectionsCount_11() const { return ___connectionsCount_11; }
	inline FsmInt_t1273009179 ** get_address_of_connectionsCount_11() { return &___connectionsCount_11; }
	inline void set_connectionsCount_11(FsmInt_t1273009179 * value)
	{
		___connectionsCount_11 = value;
		Il2CppCodeGenWriteBarrier(&___connectionsCount_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(NetworkGetConnectionsCount_t1799155532, ___everyFrame_12)); }
	inline bool get_everyFrame_12() const { return ___everyFrame_12; }
	inline bool* get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(bool value)
	{
		___everyFrame_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
