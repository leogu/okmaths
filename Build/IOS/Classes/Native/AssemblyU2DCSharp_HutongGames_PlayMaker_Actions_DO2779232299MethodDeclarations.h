﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialFade
struct DOTweenMaterialFade_t2779232299;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFade::.ctor()
extern "C"  void DOTweenMaterialFade__ctor_m3579230193 (DOTweenMaterialFade_t2779232299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFade::Reset()
extern "C"  void DOTweenMaterialFade_Reset_m4024421796 (DOTweenMaterialFade_t2779232299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFade::OnEnter()
extern "C"  void DOTweenMaterialFade_OnEnter_m174773574 (DOTweenMaterialFade_t2779232299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFade::<OnEnter>m__54()
extern "C"  void DOTweenMaterialFade_U3COnEnterU3Em__54_m731613112 (DOTweenMaterialFade_t2779232299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFade::<OnEnter>m__55()
extern "C"  void DOTweenMaterialFade_U3COnEnterU3Em__55_m731613079 (DOTweenMaterialFade_t2779232299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
