﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTextureScale
struct SetTextureScale_t1431691931;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::.ctor()
extern "C"  void SetTextureScale__ctor_m3007525767 (SetTextureScale_t1431691931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::Reset()
extern "C"  void SetTextureScale_Reset_m2669418364 (SetTextureScale_t1431691931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::OnEnter()
extern "C"  void SetTextureScale_OnEnter_m3893786826 (SetTextureScale_t1431691931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::OnUpdate()
extern "C"  void SetTextureScale_OnUpdate_m3821658183 (SetTextureScale_t1431691931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTextureScale::DoSetTextureScale()
extern "C"  void SetTextureScale_DoSetTextureScale_m2879635453 (SetTextureScale_t1431691931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
