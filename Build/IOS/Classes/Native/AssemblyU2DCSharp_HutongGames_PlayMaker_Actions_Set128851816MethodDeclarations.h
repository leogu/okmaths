﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmQuaternion
struct SetFsmQuaternion_t128851816;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::.ctor()
extern "C"  void SetFsmQuaternion__ctor_m3069395126 (SetFsmQuaternion_t128851816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::Reset()
extern "C"  void SetFsmQuaternion_Reset_m1995988589 (SetFsmQuaternion_t128851816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::OnEnter()
extern "C"  void SetFsmQuaternion_OnEnter_m3518007093 (SetFsmQuaternion_t128851816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::DoSetFsmQuaternion()
extern "C"  void SetFsmQuaternion_DoSetFsmQuaternion_m4057498497 (SetFsmQuaternion_t128851816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmQuaternion::OnUpdate()
extern "C"  void SetFsmQuaternion_OnUpdate_m1724962616 (SetFsmQuaternion_t128851816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
