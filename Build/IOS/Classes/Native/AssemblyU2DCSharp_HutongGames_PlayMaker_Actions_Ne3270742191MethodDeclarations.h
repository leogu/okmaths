﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections
struct NetworkSetMaximumConnections_t3270742191;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::.ctor()
extern "C"  void NetworkSetMaximumConnections__ctor_m714073679 (NetworkSetMaximumConnections_t3270742191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::Reset()
extern "C"  void NetworkSetMaximumConnections_Reset_m2155039064 (NetworkSetMaximumConnections_t3270742191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::OnEnter()
extern "C"  void NetworkSetMaximumConnections_OnEnter_m4150691630 (NetworkSetMaximumConnections_t3270742191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.NetworkSetMaximumConnections::ErrorCheck()
extern "C"  String_t* NetworkSetMaximumConnections_ErrorCheck_m1227218536 (NetworkSetMaximumConnections_t3270742191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
