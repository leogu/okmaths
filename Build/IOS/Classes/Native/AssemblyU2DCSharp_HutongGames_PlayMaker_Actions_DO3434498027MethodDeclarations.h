﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAudioSourcePitch
struct DOTweenAudioSourcePitch_t3434498027;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourcePitch::.ctor()
extern "C"  void DOTweenAudioSourcePitch__ctor_m2304935025 (DOTweenAudioSourcePitch_t3434498027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourcePitch::Reset()
extern "C"  void DOTweenAudioSourcePitch_Reset_m2967982340 (DOTweenAudioSourcePitch_t3434498027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourcePitch::OnEnter()
extern "C"  void DOTweenAudioSourcePitch_OnEnter_m1230638598 (DOTweenAudioSourcePitch_t3434498027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourcePitch::<OnEnter>m__20()
extern "C"  void DOTweenAudioSourcePitch_U3COnEnterU3Em__20_m3630366785 (DOTweenAudioSourcePitch_t3434498027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourcePitch::<OnEnter>m__21()
extern "C"  void DOTweenAudioSourcePitch_U3COnEnterU3Em__21_m3630366752 (DOTweenAudioSourcePitch_t3434498027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
