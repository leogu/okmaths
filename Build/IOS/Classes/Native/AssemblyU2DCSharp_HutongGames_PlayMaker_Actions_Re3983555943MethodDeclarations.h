﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RebuildTextures
struct RebuildTextures_t3983555943;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RebuildTextures::.ctor()
extern "C"  void RebuildTextures__ctor_m3580276951 (RebuildTextures_t3983555943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RebuildTextures::Reset()
extern "C"  void RebuildTextures_Reset_m2580824220 (RebuildTextures_t3983555943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RebuildTextures::OnEnter()
extern "C"  void RebuildTextures_OnEnter_m3356007122 (RebuildTextures_t3983555943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RebuildTextures::OnUpdate()
extern "C"  void RebuildTextures_OnUpdate_m1265206751 (RebuildTextures_t3983555943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RebuildTextures::DoRebuildTextures()
extern "C"  void RebuildTextures_DoRebuildTextures_m517064349 (RebuildTextures_t3983555943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
