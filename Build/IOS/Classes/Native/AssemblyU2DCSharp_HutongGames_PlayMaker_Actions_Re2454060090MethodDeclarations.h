﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectOverlaps
struct RectOverlaps_t2454060090;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::.ctor()
extern "C"  void RectOverlaps__ctor_m2709176882 (RectOverlaps_t2454060090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::Reset()
extern "C"  void RectOverlaps_Reset_m375760843 (RectOverlaps_t2454060090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::OnEnter()
extern "C"  void RectOverlaps_OnEnter_m1847869363 (RectOverlaps_t2454060090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::OnUpdate()
extern "C"  void RectOverlaps_OnUpdate_m3481321252 (RectOverlaps_t2454060090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::DoRectOverlap()
extern "C"  void RectOverlaps_DoRectOverlap_m1093712508 (RectOverlaps_t2454060090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.RectOverlaps::Intersect(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectOverlaps_Intersect_m718150195 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___a0, Rect_t3681755626  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectOverlaps::FlipNegative(UnityEngine.Rect&)
extern "C"  void RectOverlaps_FlipNegative_m343564847 (Il2CppObject * __this /* static, unused */, Rect_t3681755626 * ___r0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
