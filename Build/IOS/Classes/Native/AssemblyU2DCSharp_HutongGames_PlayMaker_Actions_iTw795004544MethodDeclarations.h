﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenPause
struct iTweenPause_t795004544;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenPause::.ctor()
extern "C"  void iTweenPause__ctor_m4046024758 (iTweenPause_t795004544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::Reset()
extern "C"  void iTweenPause_Reset_m1484161377 (iTweenPause_t795004544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::OnEnter()
extern "C"  void iTweenPause_OnEnter_m1003547369 (iTweenPause_t795004544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenPause::DoiTween()
extern "C"  void iTweenPause_DoiTween_m1656160803 (iTweenPause_t795004544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
