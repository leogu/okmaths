﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialOffsetProperty
struct DOTweenMaterialOffsetProperty_t4123362719;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffsetProperty::.ctor()
extern "C"  void DOTweenMaterialOffsetProperty__ctor_m2124347381 (DOTweenMaterialOffsetProperty_t4123362719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffsetProperty::Reset()
extern "C"  void DOTweenMaterialOffsetProperty_Reset_m2632099296 (DOTweenMaterialOffsetProperty_t4123362719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffsetProperty::OnEnter()
extern "C"  void DOTweenMaterialOffsetProperty_OnEnter_m3793416378 (DOTweenMaterialOffsetProperty_t4123362719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffsetProperty::<OnEnter>m__5C()
extern "C"  void DOTweenMaterialOffsetProperty_U3COnEnterU3Em__5C_m1127277681 (DOTweenMaterialOffsetProperty_t4123362719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffsetProperty::<OnEnter>m__5D()
extern "C"  void DOTweenMaterialOffsetProperty_U3COnEnterU3Em__5D_m1127277780 (DOTweenMaterialOffsetProperty_t4123362719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
