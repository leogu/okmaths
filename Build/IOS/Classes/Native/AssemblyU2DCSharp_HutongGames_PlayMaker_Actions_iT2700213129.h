﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenScaleUpdate
struct  iTweenScaleUpdate_t2700213129  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenScaleUpdate::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenScaleUpdate::transformScale
	FsmGameObject_t3097142863 * ___transformScale_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenScaleUpdate::vectorScale
	FsmVector3_t3996534004 * ___vectorScale_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenScaleUpdate::time
	FsmFloat_t937133978 * ___time_14;
	// System.Collections.Hashtable HutongGames.PlayMaker.Actions.iTweenScaleUpdate::hash
	Hashtable_t909839986 * ___hash_15;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.iTweenScaleUpdate::go
	GameObject_t1756533147 * ___go_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t2700213129, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_transformScale_12() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t2700213129, ___transformScale_12)); }
	inline FsmGameObject_t3097142863 * get_transformScale_12() const { return ___transformScale_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_transformScale_12() { return &___transformScale_12; }
	inline void set_transformScale_12(FsmGameObject_t3097142863 * value)
	{
		___transformScale_12 = value;
		Il2CppCodeGenWriteBarrier(&___transformScale_12, value);
	}

	inline static int32_t get_offset_of_vectorScale_13() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t2700213129, ___vectorScale_13)); }
	inline FsmVector3_t3996534004 * get_vectorScale_13() const { return ___vectorScale_13; }
	inline FsmVector3_t3996534004 ** get_address_of_vectorScale_13() { return &___vectorScale_13; }
	inline void set_vectorScale_13(FsmVector3_t3996534004 * value)
	{
		___vectorScale_13 = value;
		Il2CppCodeGenWriteBarrier(&___vectorScale_13, value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t2700213129, ___time_14)); }
	inline FsmFloat_t937133978 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t937133978 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t937133978 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier(&___time_14, value);
	}

	inline static int32_t get_offset_of_hash_15() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t2700213129, ___hash_15)); }
	inline Hashtable_t909839986 * get_hash_15() const { return ___hash_15; }
	inline Hashtable_t909839986 ** get_address_of_hash_15() { return &___hash_15; }
	inline void set_hash_15(Hashtable_t909839986 * value)
	{
		___hash_15 = value;
		Il2CppCodeGenWriteBarrier(&___hash_15, value);
	}

	inline static int32_t get_offset_of_go_16() { return static_cast<int32_t>(offsetof(iTweenScaleUpdate_t2700213129, ___go_16)); }
	inline GameObject_t1756533147 * get_go_16() const { return ___go_16; }
	inline GameObject_t1756533147 ** get_address_of_go_16() { return &___go_16; }
	inline void set_go_16(GameObject_t1756533147 * value)
	{
		___go_16 = value;
		Il2CppCodeGenWriteBarrier(&___go_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
