﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart
struct  uGuiInputFieldMoveCaretToTextStart_t370720346  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::shift
	FsmBool_t664485696 * ___shift_12;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::_inputField
	InputField_t1631627530 * ____inputField_13;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldMoveCaretToTextStart_t370720346, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_shift_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldMoveCaretToTextStart_t370720346, ___shift_12)); }
	inline FsmBool_t664485696 * get_shift_12() const { return ___shift_12; }
	inline FsmBool_t664485696 ** get_address_of_shift_12() { return &___shift_12; }
	inline void set_shift_12(FsmBool_t664485696 * value)
	{
		___shift_12 = value;
		Il2CppCodeGenWriteBarrier(&___shift_12, value);
	}

	inline static int32_t get_offset_of__inputField_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldMoveCaretToTextStart_t370720346, ____inputField_13)); }
	inline InputField_t1631627530 * get__inputField_13() const { return ____inputField_13; }
	inline InputField_t1631627530 ** get_address_of__inputField_13() { return &____inputField_13; }
	inline void set__inputField_13(InputField_t1631627530 * value)
	{
		____inputField_13 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
