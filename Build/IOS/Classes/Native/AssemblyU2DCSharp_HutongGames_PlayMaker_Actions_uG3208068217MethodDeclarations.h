﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild
struct uGuiInputFieldRebuild_t3208068217;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::.ctor()
extern "C"  void uGuiInputFieldRebuild__ctor_m238856721 (uGuiInputFieldRebuild_t3208068217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::Reset()
extern "C"  void uGuiInputFieldRebuild_Reset_m1747256622 (uGuiInputFieldRebuild_t3208068217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::OnEnter()
extern "C"  void uGuiInputFieldRebuild_OnEnter_m2779348372 (uGuiInputFieldRebuild_t3208068217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::DoAction()
extern "C"  void uGuiInputFieldRebuild_DoAction_m2672876538 (uGuiInputFieldRebuild_t3208068217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldRebuild::OnExit()
extern "C"  void uGuiInputFieldRebuild_OnExit_m384884056 (uGuiInputFieldRebuild_t3208068217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
