﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformPath
struct DOTweenTransformPath_t1051647389;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPath::.ctor()
extern "C"  void DOTweenTransformPath__ctor_m2903604501 (DOTweenTransformPath_t1051647389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPath::Reset()
extern "C"  void DOTweenTransformPath_Reset_m2108763558 (DOTweenTransformPath_t1051647389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPath::OnEnter()
extern "C"  void DOTweenTransformPath_OnEnter_m2142376300 (DOTweenTransformPath_t1051647389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPath::<OnEnter>m__CC()
extern "C"  void DOTweenTransformPath_U3COnEnterU3Em__CC_m866990611 (DOTweenTransformPath_t1051647389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPath::<OnEnter>m__CD()
extern "C"  void DOTweenTransformPath_U3COnEnterU3Em__CD_m4173820400 (DOTweenTransformPath_t1051647389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
