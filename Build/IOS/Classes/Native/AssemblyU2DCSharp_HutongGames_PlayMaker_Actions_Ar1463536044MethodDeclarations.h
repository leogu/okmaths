﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListAdd
struct ArrayListAdd_t1463536044;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListAdd::.ctor()
extern "C"  void ArrayListAdd__ctor_m1488491740 (ArrayListAdd_t1463536044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListAdd::Reset()
extern "C"  void ArrayListAdd_Reset_m1303210485 (ArrayListAdd_t1463536044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListAdd::OnEnter()
extern "C"  void ArrayListAdd_OnEnter_m345538597 (ArrayListAdd_t1463536044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListAdd::AddToArrayList()
extern "C"  void ArrayListAdd_AddToArrayList_m384052525 (ArrayListAdd_t1463536044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
