﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin
struct RectTransformGetAnchorMin_t768771649;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::.ctor()
extern "C"  void RectTransformGetAnchorMin__ctor_m1496591879 (RectTransformGetAnchorMin_t768771649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::Reset()
extern "C"  void RectTransformGetAnchorMin_Reset_m3669654970 (RectTransformGetAnchorMin_t768771649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::OnEnter()
extern "C"  void RectTransformGetAnchorMin_OnEnter_m1511525724 (RectTransformGetAnchorMin_t768771649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::OnActionUpdate()
extern "C"  void RectTransformGetAnchorMin_OnActionUpdate_m2614084345 (RectTransformGetAnchorMin_t768771649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMin::DoGetValues()
extern "C"  void RectTransformGetAnchorMin_DoGetValues_m1575691944 (RectTransformGetAnchorMin_t768771649 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
