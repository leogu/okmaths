﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertEnumToString
struct ConvertEnumToString_t2542243456;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::.ctor()
extern "C"  void ConvertEnumToString__ctor_m1809761780 (ConvertEnumToString_t2542243456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::Reset()
extern "C"  void ConvertEnumToString_Reset_m2525598977 (ConvertEnumToString_t2542243456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::OnEnter()
extern "C"  void ConvertEnumToString_OnEnter_m1237899921 (ConvertEnumToString_t2542243456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::OnUpdate()
extern "C"  void ConvertEnumToString_OnUpdate_m832684226 (ConvertEnumToString_t2542243456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertEnumToString::DoConvertEnumToString()
extern "C"  void ConvertEnumToString_DoConvertEnumToString_m2515204573 (ConvertEnumToString_t2542243456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
