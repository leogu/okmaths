﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CollectionsActions
struct CollectionsActions_t1918293222;
// PlayMakerHashTableProxy
struct PlayMakerHashTableProxy_t3073922234;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PlayMakerArrayListProxy
struct PlayMakerArrayListProxy_t4012441185;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.CollectionsActions::.ctor()
extern "C"  void CollectionsActions__ctor_m2761223024 (CollectionsActions_t1918293222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerHashTableProxy HutongGames.PlayMaker.Actions.CollectionsActions::GetHashTableProxyPointer(UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  PlayMakerHashTableProxy_t3073922234 * CollectionsActions_GetHashTableProxyPointer_m552597517 (CollectionsActions_t1918293222 * __this, GameObject_t1756533147 * ___aProxy0, String_t* ___nameReference1, bool ___silent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerArrayListProxy HutongGames.PlayMaker.Actions.CollectionsActions::GetArrayListProxyPointer(UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  PlayMakerArrayListProxy_t4012441185 * CollectionsActions_GetArrayListProxyPointer_m3930594317 (CollectionsActions_t1918293222 * __this, GameObject_t1756533147 * ___aProxy0, String_t* ___nameReference1, bool ___silent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
