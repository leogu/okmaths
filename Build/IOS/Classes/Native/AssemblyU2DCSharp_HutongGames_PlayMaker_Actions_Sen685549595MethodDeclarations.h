﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendRandomEvent
struct SendRandomEvent_t685549595;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendRandomEvent::.ctor()
extern "C"  void SendRandomEvent__ctor_m3025564409 (SendRandomEvent_t685549595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendRandomEvent::Reset()
extern "C"  void SendRandomEvent_Reset_m1827739932 (SendRandomEvent_t685549595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendRandomEvent::OnEnter()
extern "C"  void SendRandomEvent_OnEnter_m2081268022 (SendRandomEvent_t685549595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendRandomEvent::OnUpdate()
extern "C"  void SendRandomEvent_OnUpdate_m1966395069 (SendRandomEvent_t685549595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
