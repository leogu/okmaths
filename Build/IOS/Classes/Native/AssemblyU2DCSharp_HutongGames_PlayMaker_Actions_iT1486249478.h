﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_Space4278750806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.iTweenRotateUpdate
struct  iTweenRotateUpdate_t1486249478  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.iTweenRotateUpdate::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.iTweenRotateUpdate::transformRotation
	FsmGameObject_t3097142863 * ___transformRotation_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.iTweenRotateUpdate::vectorRotation
	FsmVector3_t3996534004 * ___vectorRotation_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.iTweenRotateUpdate::time
	FsmFloat_t937133978 * ___time_14;
	// UnityEngine.Space HutongGames.PlayMaker.Actions.iTweenRotateUpdate::space
	int32_t ___space_15;
	// System.Collections.Hashtable HutongGames.PlayMaker.Actions.iTweenRotateUpdate::hash
	Hashtable_t909839986 * ___hash_16;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.iTweenRotateUpdate::go
	GameObject_t1756533147 * ___go_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_transformRotation_12() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___transformRotation_12)); }
	inline FsmGameObject_t3097142863 * get_transformRotation_12() const { return ___transformRotation_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_transformRotation_12() { return &___transformRotation_12; }
	inline void set_transformRotation_12(FsmGameObject_t3097142863 * value)
	{
		___transformRotation_12 = value;
		Il2CppCodeGenWriteBarrier(&___transformRotation_12, value);
	}

	inline static int32_t get_offset_of_vectorRotation_13() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___vectorRotation_13)); }
	inline FsmVector3_t3996534004 * get_vectorRotation_13() const { return ___vectorRotation_13; }
	inline FsmVector3_t3996534004 ** get_address_of_vectorRotation_13() { return &___vectorRotation_13; }
	inline void set_vectorRotation_13(FsmVector3_t3996534004 * value)
	{
		___vectorRotation_13 = value;
		Il2CppCodeGenWriteBarrier(&___vectorRotation_13, value);
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___time_14)); }
	inline FsmFloat_t937133978 * get_time_14() const { return ___time_14; }
	inline FsmFloat_t937133978 ** get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(FsmFloat_t937133978 * value)
	{
		___time_14 = value;
		Il2CppCodeGenWriteBarrier(&___time_14, value);
	}

	inline static int32_t get_offset_of_space_15() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___space_15)); }
	inline int32_t get_space_15() const { return ___space_15; }
	inline int32_t* get_address_of_space_15() { return &___space_15; }
	inline void set_space_15(int32_t value)
	{
		___space_15 = value;
	}

	inline static int32_t get_offset_of_hash_16() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___hash_16)); }
	inline Hashtable_t909839986 * get_hash_16() const { return ___hash_16; }
	inline Hashtable_t909839986 ** get_address_of_hash_16() { return &___hash_16; }
	inline void set_hash_16(Hashtable_t909839986 * value)
	{
		___hash_16 = value;
		Il2CppCodeGenWriteBarrier(&___hash_16, value);
	}

	inline static int32_t get_offset_of_go_17() { return static_cast<int32_t>(offsetof(iTweenRotateUpdate_t1486249478, ___go_17)); }
	inline GameObject_t1756533147 * get_go_17() const { return ___go_17; }
	inline GameObject_t1756533147 ** get_address_of_go_17() { return &___go_17; }
	inline void set_go_17(GameObject_t1756533147 * value)
	{
		___go_17 = value;
		Il2CppCodeGenWriteBarrier(&___go_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
