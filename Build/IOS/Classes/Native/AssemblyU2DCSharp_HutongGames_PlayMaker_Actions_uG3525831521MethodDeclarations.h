﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused
struct uGuiInputFieldGetIsFocused_t3525831521;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::.ctor()
extern "C"  void uGuiInputFieldGetIsFocused__ctor_m317193417 (uGuiInputFieldGetIsFocused_t3525831521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::Reset()
extern "C"  void uGuiInputFieldGetIsFocused_Reset_m3775603794 (uGuiInputFieldGetIsFocused_t3525831521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::OnEnter()
extern "C"  void uGuiInputFieldGetIsFocused_OnEnter_m2882628848 (uGuiInputFieldGetIsFocused_t3525831521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::DoGetValue()
extern "C"  void uGuiInputFieldGetIsFocused_DoGetValue_m1158493591 (uGuiInputFieldGetIsFocused_t3525831521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
