﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs
struct NetworkSetMinimumAllocatableViewIDs_t2994535979;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::.ctor()
extern "C"  void NetworkSetMinimumAllocatableViewIDs__ctor_m2572600305 (NetworkSetMinimumAllocatableViewIDs_t2994535979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::Reset()
extern "C"  void NetworkSetMinimumAllocatableViewIDs_Reset_m3017791908 (NetworkSetMinimumAllocatableViewIDs_t2994535979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetMinimumAllocatableViewIDs::OnEnter()
extern "C"  void NetworkSetMinimumAllocatableViewIDs_OnEnter_m1315725638 (NetworkSetMinimumAllocatableViewIDs_t2994535979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
