﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolAllTrue
struct BoolAllTrue_t3427760439;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::.ctor()
extern "C"  void BoolAllTrue__ctor_m2956156499 (BoolAllTrue_t3427760439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::Reset()
extern "C"  void BoolAllTrue_Reset_m2749258288 (BoolAllTrue_t3427760439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::OnEnter()
extern "C"  void BoolAllTrue_OnEnter_m941914006 (BoolAllTrue_t3427760439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::OnUpdate()
extern "C"  void BoolAllTrue_OnUpdate_m3372071691 (BoolAllTrue_t3427760439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolAllTrue::DoAllTrue()
extern "C"  void BoolAllTrue_DoAllTrue_m4252652683 (BoolAllTrue_t3427760439 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
