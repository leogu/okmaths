﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize
struct DOTweenTrailRendererResize_t3030774513;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::.ctor()
extern "C"  void DOTweenTrailRendererResize__ctor_m599737945 (DOTweenTrailRendererResize_t3030774513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::Reset()
extern "C"  void DOTweenTrailRendererResize_Reset_m4059230722 (DOTweenTrailRendererResize_t3030774513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::OnEnter()
extern "C"  void DOTweenTrailRendererResize_OnEnter_m1599289024 (DOTweenTrailRendererResize_t3030774513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::<OnEnter>m__A2()
extern "C"  void DOTweenTrailRendererResize_U3COnEnterU3Em__A2_m649841700 (DOTweenTrailRendererResize_t3030774513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::<OnEnter>m__A3()
extern "C"  void DOTweenTrailRendererResize_U3COnEnterU3Em__A3_m791004201 (DOTweenTrailRendererResize_t3030774513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
