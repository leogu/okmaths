﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.HostData[]
struct HostDataU5BU5D_t1011608759;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String UnityEngine.MasterServer::get_ipAddress()
extern "C"  String_t* MasterServer_get_ipAddress_m1552175907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_ipAddress(System.String)
extern "C"  void MasterServer_set_ipAddress_m518203496 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.MasterServer::get_port()
extern "C"  int32_t MasterServer_get_port_m3416376690 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_port(System.Int32)
extern "C"  void MasterServer_set_port_m3293017953 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::RequestHostList(System.String)
extern "C"  void MasterServer_RequestHostList_m660473625 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.HostData[] UnityEngine.MasterServer::PollHostList()
extern "C"  HostDataU5BU5D_t1011608759* MasterServer_PollHostList_m2376545823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::RegisterHost(System.String,System.String,System.String)
extern "C"  void MasterServer_RegisterHost_m2321457831 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, String_t* ___gameName1, String_t* ___comment2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::UnregisterHost()
extern "C"  void MasterServer_UnregisterHost_m4253193212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::ClearHostList()
extern "C"  void MasterServer_ClearHostList_m1356680311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.MasterServer::get_updateRate()
extern "C"  int32_t MasterServer_get_updateRate_m2849397026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_updateRate(System.Int32)
extern "C"  void MasterServer_set_updateRate_m3353854091 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.MasterServer::get_dedicatedServer()
extern "C"  bool MasterServer_get_dedicatedServer_m1786223459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MasterServer::set_dedicatedServer(System.Boolean)
extern "C"  void MasterServer_set_dedicatedServer_m3331804062 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
