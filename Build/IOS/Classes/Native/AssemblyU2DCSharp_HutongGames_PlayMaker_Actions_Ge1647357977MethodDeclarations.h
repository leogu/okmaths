﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseButton
struct GetMouseButton_t1647357977;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::.ctor()
extern "C"  void GetMouseButton__ctor_m3856156979 (GetMouseButton_t1647357977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::Reset()
extern "C"  void GetMouseButton_Reset_m4040935146 (GetMouseButton_t1647357977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::OnEnter()
extern "C"  void GetMouseButton_OnEnter_m3074494868 (GetMouseButton_t1647357977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseButton::OnUpdate()
extern "C"  void GetMouseButton_OnUpdate_m3479608867 (GetMouseButton_t1647357977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
