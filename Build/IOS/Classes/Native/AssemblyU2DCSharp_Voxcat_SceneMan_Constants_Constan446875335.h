﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Voxcat.SceneMan.Constants.Constants
struct  Constants_t446875335  : public Il2CppObject
{
public:

public:
};

struct Constants_t446875335_StaticFields
{
public:
	// System.Single Voxcat.SceneMan.Constants.Constants::LOADING_SCENE_WAIT_TIME
	float ___LOADING_SCENE_WAIT_TIME_1;

public:
	inline static int32_t get_offset_of_LOADING_SCENE_WAIT_TIME_1() { return static_cast<int32_t>(offsetof(Constants_t446875335_StaticFields, ___LOADING_SCENE_WAIT_TIME_1)); }
	inline float get_LOADING_SCENE_WAIT_TIME_1() const { return ___LOADING_SCENE_WAIT_TIME_1; }
	inline float* get_address_of_LOADING_SCENE_WAIT_TIME_1() { return &___LOADING_SCENE_WAIT_TIME_1; }
	inline void set_LOADING_SCENE_WAIT_TIME_1(float value)
	{
		___LOADING_SCENE_WAIT_TIME_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
