﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider
struct GUILayoutHorizontalSlider_t2346953116;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::.ctor()
extern "C"  void GUILayoutHorizontalSlider__ctor_m2167097506 (GUILayoutHorizontalSlider_t2346953116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::Reset()
extern "C"  void GUILayoutHorizontalSlider_Reset_m2830136373 (GUILayoutHorizontalSlider_t2346953116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutHorizontalSlider::OnGUI()
extern "C"  void GUILayoutHorizontalSlider_OnGUI_m3135287614 (GUILayoutHorizontalSlider_t2346953116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
