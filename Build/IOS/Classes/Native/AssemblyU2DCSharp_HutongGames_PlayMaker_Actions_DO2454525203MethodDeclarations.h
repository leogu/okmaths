﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLightColor
struct DOTweenLightColor_t2454525203;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLightColor::.ctor()
extern "C"  void DOTweenLightColor__ctor_m697099479 (DOTweenLightColor_t2454525203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightColor::Reset()
extern "C"  void DOTweenLightColor_Reset_m524014412 (DOTweenLightColor_t2454525203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightColor::OnEnter()
extern "C"  void DOTweenLightColor_OnEnter_m657587138 (DOTweenLightColor_t2454525203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightColor::<OnEnter>m__48()
extern "C"  void DOTweenLightColor_U3COnEnterU3Em__48_m3599518693 (DOTweenLightColor_t2454525203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightColor::<OnEnter>m__49()
extern "C"  void DOTweenLightColor_U3COnEnterU3Em__49_m3599518726 (DOTweenLightColor_t2454525203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
