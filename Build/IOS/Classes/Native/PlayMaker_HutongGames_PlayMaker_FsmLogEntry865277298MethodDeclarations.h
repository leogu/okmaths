﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmLogEntry
struct FsmLogEntry_t865277298;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t3672513366;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Texture
struct Texture_t2243626319;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog3672513366.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType1161667734.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"

// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.FsmLogEntry::get_Log()
extern "C"  FsmLog_t3672513366 * FsmLogEntry_get_Log_m1930058449 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Log(HutongGames.PlayMaker.FsmLog)
extern "C"  void FsmLogEntry_set_Log_m1042615272 (FsmLogEntry_t865277298 * __this, FsmLog_t3672513366 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmLogType HutongGames.PlayMaker.FsmLogEntry::get_LogType()
extern "C"  int32_t FsmLogEntry_get_LogType_m2810500785 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_LogType(HutongGames.PlayMaker.FsmLogType)
extern "C"  void FsmLogEntry_set_LogType_m3641539640 (FsmLogEntry_t865277298 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmLogEntry::get_Fsm()
extern "C"  Fsm_t917886356 * FsmLogEntry_get_Fsm_m827616125 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::get_State()
extern "C"  FsmState_t1643911659 * FsmLogEntry_get_State_m1283019649 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_State(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLogEntry_set_State_m4048138202 (FsmLogEntry_t865277298 * __this, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmLogEntry::get_SentByState()
extern "C"  FsmState_t1643911659 * FsmLogEntry_get_SentByState_m3712318892 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_SentByState(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLogEntry_set_SentByState_m4266419317 (FsmLogEntry_t865277298 * __this, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmLogEntry::get_Action()
extern "C"  FsmStateAction_t2862378169 * FsmLogEntry_get_Action_m3867059302 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Action(HutongGames.PlayMaker.FsmStateAction)
extern "C"  void FsmLogEntry_set_Action_m1294242537 (FsmLogEntry_t865277298 * __this, FsmStateAction_t2862378169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmLogEntry::get_Event()
extern "C"  FsmEvent_t1258573736 * FsmLogEntry_get_Event_m280860593 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmLogEntry_set_Event_m1998190360 (FsmLogEntry_t865277298 * __this, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.FsmLogEntry::get_Transition()
extern "C"  FsmTransition_t1534990431 * FsmLogEntry_get_Transition_m4004608385 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Transition(HutongGames.PlayMaker.FsmTransition)
extern "C"  void FsmLogEntry_set_Transition_m2780889920 (FsmLogEntry_t865277298 * __this, FsmTransition_t1534990431 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmLogEntry::get_EventTarget()
extern "C"  FsmEventTarget_t172293745 * FsmLogEntry_get_EventTarget_m2907392565 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_EventTarget(HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void FsmLogEntry_set_EventTarget_m680367794 (FsmLogEntry_t865277298 * __this, FsmEventTarget_t172293745 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.FsmLogEntry::get_Time()
extern "C"  float FsmLogEntry_get_Time_m1576594795 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Time(System.Single)
extern "C"  void FsmLogEntry_set_Time_m1039533484 (FsmLogEntry_t865277298 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_Text()
extern "C"  String_t* FsmLogEntry_get_Text_m2386098886 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Text(System.String)
extern "C"  void FsmLogEntry_set_Text_m940150019 (FsmLogEntry_t865277298 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_Text2()
extern "C"  String_t* FsmLogEntry_get_Text2_m852786598 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_Text2(System.String)
extern "C"  void FsmLogEntry_set_Text2_m1941432509 (FsmLogEntry_t865277298 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmLogEntry::get_FrameCount()
extern "C"  int32_t FsmLogEntry_get_FrameCount_m2339003680 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_FrameCount(System.Int32)
extern "C"  void FsmLogEntry_set_FrameCount_m3586606679 (FsmLogEntry_t865277298 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::get_FsmVariablesCopy()
extern "C"  FsmVariables_t630687169 * FsmLogEntry_get_FsmVariablesCopy_m3193459780 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_FsmVariablesCopy(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmLogEntry_set_FsmVariablesCopy_m4109622963 (FsmLogEntry_t865277298 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmLogEntry::get_GlobalVariablesCopy()
extern "C"  FsmVariables_t630687169 * FsmLogEntry_get_GlobalVariablesCopy_m1863077847 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GlobalVariablesCopy(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmLogEntry_set_GlobalVariablesCopy_m2704767938 (FsmLogEntry_t865277298 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.FsmLogEntry::get_GameObject()
extern "C"  GameObject_t1756533147 * FsmLogEntry_get_GameObject_m765156388 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GameObject(UnityEngine.GameObject)
extern "C"  void FsmLogEntry_set_GameObject_m3043649499 (FsmLogEntry_t865277298 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_GameObjectName()
extern "C"  String_t* FsmLogEntry_get_GameObjectName_m1651454293 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GameObjectName(System.String)
extern "C"  void FsmLogEntry_set_GameObjectName_m1441470182 (FsmLogEntry_t865277298 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture HutongGames.PlayMaker.FsmLogEntry::get_GameObjectIcon()
extern "C"  Texture_t2243626319 * FsmLogEntry_get_GameObjectIcon_m3238577227 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::set_GameObjectIcon(UnityEngine.Texture)
extern "C"  void FsmLogEntry_set_GameObjectIcon_m516890814 (FsmLogEntry_t865277298 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLogEntry::get_TextWithTimecode()
extern "C"  String_t* FsmLogEntry_get_TextWithTimecode_m3940291942 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmLogEntry::GetIndex()
extern "C"  int32_t FsmLogEntry_GetIndex_m822913019 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::DebugLog()
extern "C"  void FsmLogEntry_DebugLog_m1915972160 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLogEntry::.ctor()
extern "C"  void FsmLogEntry__ctor_m801171321 (FsmLogEntry_t865277298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
