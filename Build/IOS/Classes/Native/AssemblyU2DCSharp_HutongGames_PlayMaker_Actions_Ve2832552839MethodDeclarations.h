﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Subtract
struct Vector2Subtract_t2832552839;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Subtract::.ctor()
extern "C"  void Vector2Subtract__ctor_m3841557281 (Vector2Subtract_t2832552839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Subtract::Reset()
extern "C"  void Vector2Subtract_Reset_m450002140 (Vector2Subtract_t2832552839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Subtract::OnEnter()
extern "C"  void Vector2Subtract_OnEnter_m4145930590 (Vector2Subtract_t2832552839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Subtract::OnUpdate()
extern "C"  void Vector2Subtract_OnUpdate_m2560994357 (Vector2Subtract_t2832552839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
