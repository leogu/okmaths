﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugDrawShape
struct DebugDrawShape_t2189308108;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugDrawShape::.ctor()
extern "C"  void DebugDrawShape__ctor_m2560445906 (DebugDrawShape_t2189308108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugDrawShape::Reset()
extern "C"  void DebugDrawShape_Reset_m871492977 (DebugDrawShape_t2189308108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugDrawShape::OnDrawActionGizmos()
extern "C"  void DebugDrawShape_OnDrawActionGizmos_m2623861714 (DebugDrawShape_t2189308108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
