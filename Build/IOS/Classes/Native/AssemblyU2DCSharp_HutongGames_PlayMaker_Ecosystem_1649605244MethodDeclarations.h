﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute
struct ButtonAttribute_t1649605244;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String,System.String,System.Boolean,System.Reflection.BindingFlags)
extern "C"  void ButtonAttribute__ctor_m2023637095 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, String_t* ___buttonName1, bool ___useValue2, int32_t ___flags3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String,System.Boolean,System.Reflection.BindingFlags)
extern "C"  void ButtonAttribute__ctor_m3731002365 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, bool ___useValue1, int32_t ___flags2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String,System.Boolean)
extern "C"  void ButtonAttribute__ctor_m241567441 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, bool ___useValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String,System.String,System.Reflection.BindingFlags)
extern "C"  void ButtonAttribute__ctor_m2136639336 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, String_t* ___buttonName1, int32_t ___flags2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String,System.String)
extern "C"  void ButtonAttribute__ctor_m4098840220 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, String_t* ___buttonName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String,System.Reflection.BindingFlags)
extern "C"  void ButtonAttribute__ctor_m3454367164 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, int32_t ___flags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::.ctor(System.String)
extern "C"  void ButtonAttribute__ctor_m2981981780 (ButtonAttribute_t1649605244 * __this, String_t* ___methodName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
