﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m673103941(__this, ___l0, method) ((  void (*) (Enumerator_t3005263720 *, List_1_t3470534046 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3853805197(__this, method) ((  void (*) (Enumerator_t3005263720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1548273297(__this, method) ((  Il2CppObject * (*) (Enumerator_t3005263720 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::Dispose()
#define Enumerator_Dispose_m1813083214(__this, method) ((  void (*) (Enumerator_t3005263720 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::VerifyState()
#define Enumerator_VerifyState_m1300678731(__this, method) ((  void (*) (Enumerator_t3005263720 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::MoveNext()
#define Enumerator_MoveNext_m2780624042(__this, method) ((  bool (*) (Enumerator_t3005263720 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.ActionReport>::get_Current()
#define Enumerator_get_Current_m1681060776(__this, method) ((  ActionReport_t4101412914 * (*) (Enumerator_t3005263720 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
