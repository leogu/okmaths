﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformScaleX
struct DOTweenTransformScaleX_t3914410632;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleX::.ctor()
extern "C"  void DOTweenTransformScaleX__ctor_m2960992142 (DOTweenTransformScaleX_t3914410632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleX::Reset()
extern "C"  void DOTweenTransformScaleX_Reset_m1243623925 (DOTweenTransformScaleX_t3914410632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleX::OnEnter()
extern "C"  void DOTweenTransformScaleX_OnEnter_m3330472725 (DOTweenTransformScaleX_t3914410632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleX::<OnEnter>m__D8()
extern "C"  void DOTweenTransformScaleX_U3COnEnterU3Em__D8_m978345454 (DOTweenTransformScaleX_t3914410632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleX::<OnEnter>m__D9()
extern "C"  void DOTweenTransformScaleX_U3COnEnterU3Em__D9_m1119507955 (DOTweenTransformScaleX_t3914410632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
