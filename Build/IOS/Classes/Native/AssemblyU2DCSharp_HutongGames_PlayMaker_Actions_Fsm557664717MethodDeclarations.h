﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmStateSwitch
struct FsmStateSwitch_t557664717;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::.ctor()
extern "C"  void FsmStateSwitch__ctor_m529826527 (FsmStateSwitch_t557664717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::Reset()
extern "C"  void FsmStateSwitch_Reset_m3102026014 (FsmStateSwitch_t557664717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnEnter()
extern "C"  void FsmStateSwitch_OnEnter_m2896641864 (FsmStateSwitch_t557664717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::OnUpdate()
extern "C"  void FsmStateSwitch_OnUpdate_m1244245679 (FsmStateSwitch_t557664717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateSwitch::DoFsmStateSwitch()
extern "C"  void FsmStateSwitch_DoFsmStateSwitch_m2239951937 (FsmStateSwitch_t557664717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
