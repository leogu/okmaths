﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal
struct GUILayoutBeginHorizontal_t1766684562;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::.ctor()
extern "C"  void GUILayoutBeginHorizontal__ctor_m2184813434 (GUILayoutBeginHorizontal_t1766684562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::Reset()
extern "C"  void GUILayoutBeginHorizontal_Reset_m459325571 (GUILayoutBeginHorizontal_t1766684562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginHorizontal::OnGUI()
extern "C"  void GUILayoutBeginHorizontal_OnGUI_m3911602950 (GUILayoutBeginHorizontal_t1766684562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
