﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector22430450063.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVector2::get_Value()
extern "C"  Vector2_t2243707579  FsmVector2_get_Value_m2589490767 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::set_Value(UnityEngine.Vector2)
extern "C"  void FsmVector2_set_Value_m1294693978 (FsmVector2_t2430450063 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmVector2::get_RawValue()
extern "C"  Il2CppObject * FsmVector2_get_RawValue_m12320189 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::set_RawValue(System.Object)
extern "C"  void FsmVector2_set_RawValue_m2366638868 (FsmVector2_t2430450063 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::.ctor()
extern "C"  void FsmVector2__ctor_m39699932 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::.ctor(System.String)
extern "C"  void FsmVector2__ctor_m1458268414 (FsmVector2_t2430450063 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::.ctor(HutongGames.PlayMaker.FsmVector2)
extern "C"  void FsmVector2__ctor_m2743074395 (FsmVector2_t2430450063 * __this, FsmVector2_t2430450063 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVector2::Clone()
extern "C"  NamedVariable_t3026441313 * FsmVector2_Clone_m2076120957 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVector2::get_VariableType()
extern "C"  int32_t FsmVector2_get_VariableType_m3495522746 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVector2::ToString()
extern "C"  String_t* FsmVector2_ToString_m1268970357 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmVector2::op_Implicit(UnityEngine.Vector2)
extern "C"  FsmVector2_t2430450063 * FsmVector2_op_Implicit_m3143727231 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
