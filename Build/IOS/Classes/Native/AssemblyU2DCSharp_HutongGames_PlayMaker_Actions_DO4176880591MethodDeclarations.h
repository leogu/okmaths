﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformSizeDelta
struct DOTweenRectTransformSizeDelta_t4176880591;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformSizeDelta::.ctor()
extern "C"  void DOTweenRectTransformSizeDelta__ctor_m348466853 (DOTweenRectTransformSizeDelta_t4176880591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformSizeDelta::Reset()
extern "C"  void DOTweenRectTransformSizeDelta_Reset_m856210320 (DOTweenRectTransformSizeDelta_t4176880591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformSizeDelta::OnEnter()
extern "C"  void DOTweenRectTransformSizeDelta_OnEnter_m1023938026 (DOTweenRectTransformSizeDelta_t4176880591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformSizeDelta::<OnEnter>m__76()
extern "C"  void DOTweenRectTransformSizeDelta_U3COnEnterU3Em__76_m2110811756 (DOTweenRectTransformSizeDelta_t4176880591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformSizeDelta::<OnEnter>m__77()
extern "C"  void DOTweenRectTransformSizeDelta_U3COnEnterU3Em__77_m2110811851 (DOTweenRectTransformSizeDelta_t4176880591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
