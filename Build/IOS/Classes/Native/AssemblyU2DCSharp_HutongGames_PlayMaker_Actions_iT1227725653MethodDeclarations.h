﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleBy
struct iTweenScaleBy_t1227725653;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::.ctor()
extern "C"  void iTweenScaleBy__ctor_m3552748949 (iTweenScaleBy_t1227725653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::Reset()
extern "C"  void iTweenScaleBy_Reset_m1546215530 (iTweenScaleBy_t1227725653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::OnEnter()
extern "C"  void iTweenScaleBy_OnEnter_m1364097408 (iTweenScaleBy_t1227725653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::OnExit()
extern "C"  void iTweenScaleBy_OnExit_m4051870540 (iTweenScaleBy_t1227725653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleBy::DoiTween()
extern "C"  void iTweenScaleBy_DoiTween_m2144273830 (iTweenScaleBy_t1227725653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
