﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableClear
struct HashTableClear_t3919206191;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableClear::.ctor()
extern "C"  void HashTableClear__ctor_m941890723 (HashTableClear_t3919206191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableClear::Reset()
extern "C"  void HashTableClear_Reset_m143528020 (HashTableClear_t3919206191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableClear::OnEnter()
extern "C"  void HashTableClear_OnEnter_m3955851258 (HashTableClear_t3919206191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableClear::ClearHashTable()
extern "C"  void HashTableClear_ClearHashTable_m2802657924 (HashTableClear_t3919206191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
