﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve
struct GetAnimatorIsParameterControlledByCurve_t1350628870;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::.ctor()
extern "C"  void GetAnimatorIsParameterControlledByCurve__ctor_m2924744728 (GetAnimatorIsParameterControlledByCurve_t1350628870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::Reset()
extern "C"  void GetAnimatorIsParameterControlledByCurve_Reset_m3525211867 (GetAnimatorIsParameterControlledByCurve_t1350628870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::OnEnter()
extern "C"  void GetAnimatorIsParameterControlledByCurve_OnEnter_m117919995 (GetAnimatorIsParameterControlledByCurve_t1350628870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsParameterControlledByCurve::DoCheckIsParameterControlledByCurve()
extern "C"  void GetAnimatorIsParameterControlledByCurve_DoCheckIsParameterControlledByCurve_m2652154612 (GetAnimatorIsParameterControlledByCurve_t1350628870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
