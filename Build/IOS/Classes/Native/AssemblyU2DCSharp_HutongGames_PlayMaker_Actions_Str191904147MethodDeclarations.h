﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringReplace
struct StringReplace_t191904147;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringReplace::.ctor()
extern "C"  void StringReplace__ctor_m2140485187 (StringReplace_t191904147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::Reset()
extern "C"  void StringReplace_Reset_m146779120 (StringReplace_t191904147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::OnEnter()
extern "C"  void StringReplace_OnEnter_m2614614142 (StringReplace_t191904147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::OnUpdate()
extern "C"  void StringReplace_OnUpdate_m3575805011 (StringReplace_t191904147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringReplace::DoReplace()
extern "C"  void StringReplace_DoReplace_m3230215000 (StringReplace_t191904147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
