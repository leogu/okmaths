﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddForce2d
struct AddForce2d_t1214204412;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddForce2d::.ctor()
extern "C"  void AddForce2d__ctor_m1720349740 (AddForce2d_t1214204412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::Reset()
extern "C"  void AddForce2d_Reset_m923047685 (AddForce2d_t1214204412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnPreprocess()
extern "C"  void AddForce2d_OnPreprocess_m91921765 (AddForce2d_t1214204412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnEnter()
extern "C"  void AddForce2d_OnEnter_m1228354405 (AddForce2d_t1214204412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::OnFixedUpdate()
extern "C"  void AddForce2d_OnFixedUpdate_m3608356072 (AddForce2d_t1214204412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce2d::DoAddForce()
extern "C"  void AddForce2d_DoAddForce_m1096358239 (AddForce2d_t1214204412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
