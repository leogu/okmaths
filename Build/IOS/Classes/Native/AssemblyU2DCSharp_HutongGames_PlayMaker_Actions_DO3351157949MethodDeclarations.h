﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat
struct DOTweenAudioMixerSetFloat_t3351157949;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::.ctor()
extern "C"  void DOTweenAudioMixerSetFloat__ctor_m167446167 (DOTweenAudioMixerSetFloat_t3351157949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::Reset()
extern "C"  void DOTweenAudioMixerSetFloat_Reset_m63760506 (DOTweenAudioMixerSetFloat_t3351157949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::OnEnter()
extern "C"  void DOTweenAudioMixerSetFloat_OnEnter_m1728145364 (DOTweenAudioMixerSetFloat_t3351157949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::<OnEnter>m__1C()
extern "C"  void DOTweenAudioMixerSetFloat_U3COnEnterU3Em__1C_m3624577255 (DOTweenAudioMixerSetFloat_t3351157949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::<OnEnter>m__1D()
extern "C"  void DOTweenAudioMixerSetFloat_U3COnEnterU3Em__1D_m3624577358 (DOTweenAudioMixerSetFloat_t3351157949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
