﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendEventByName
struct SendEventByName_t239596870;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendEventByName::.ctor()
extern "C"  void SendEventByName__ctor_m1607059190 (SendEventByName_t239596870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventByName::Reset()
extern "C"  void SendEventByName_Reset_m465370011 (SendEventByName_t239596870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventByName::OnEnter()
extern "C"  void SendEventByName_OnEnter_m257003715 (SendEventByName_t239596870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventByName::OnUpdate()
extern "C"  void SendEventByName_OnUpdate_m3823099424 (SendEventByName_t239596870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
