﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LoadLevelNum
struct LoadLevelNum_t1446155692;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::.ctor()
extern "C"  void LoadLevelNum__ctor_m4170201574 (LoadLevelNum_t1446155692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::Reset()
extern "C"  void LoadLevelNum_Reset_m2209458549 (LoadLevelNum_t1446155692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LoadLevelNum::OnEnter()
extern "C"  void LoadLevelNum_OnEnter_m3279811437 (LoadLevelNum_t1446155692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
