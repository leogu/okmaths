﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateColor
struct DOTweenAnimateColor_t47803158;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateColor::.ctor()
extern "C"  void DOTweenAnimateColor__ctor_m1661907904 (DOTweenAnimateColor_t47803158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateColor::Reset()
extern "C"  void DOTweenAnimateColor_Reset_m464079699 (DOTweenAnimateColor_t47803158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateColor::OnEnter()
extern "C"  void DOTweenAnimateColor_OnEnter_m29944587 (DOTweenAnimateColor_t47803158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.Actions.DOTweenAnimateColor::<OnEnter>m__0()
extern "C"  Color_t2020392075  DOTweenAnimateColor_U3COnEnterU3Em__0_m1986788701 (DOTweenAnimateColor_t47803158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateColor::<OnEnter>m__1(UnityEngine.Color)
extern "C"  void DOTweenAnimateColor_U3COnEnterU3Em__1_m568478727 (DOTweenAnimateColor_t47803158 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateColor::<OnEnter>m__2()
extern "C"  void DOTweenAnimateColor_U3COnEnterU3Em__2_m4024706184 (DOTweenAnimateColor_t47803158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateColor::<OnEnter>m__3()
extern "C"  void DOTweenAnimateColor_U3COnEnterU3Em__3_m3883543683 (DOTweenAnimateColor_t47803158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
