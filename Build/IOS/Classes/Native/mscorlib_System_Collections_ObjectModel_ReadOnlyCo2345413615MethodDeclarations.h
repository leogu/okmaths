﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>
struct ReadOnlyCollection_1_t2345413615;
// System.Collections.Generic.IList`1<HutongGames.PlayMaker.ParamDataType>
struct IList_1_t2700568524;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t835534274;
// System.Collections.Generic.IEnumerator`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerator_1_t3930119046;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2371230945_gshared (ReadOnlyCollection_1_t2345413615 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2371230945(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2371230945_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2086212559_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2086212559(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2086212559_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3365770779_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3365770779(__this, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3365770779_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m464486036_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m464486036(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m464486036_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3982330858_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3982330858(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3982330858_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1549865160_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1549865160(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1549865160_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1882796798_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1882796798(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1882796798_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m973808211_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m973808211(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m973808211_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3910375231_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3910375231(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3910375231_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m560356282_gshared (ReadOnlyCollection_1_t2345413615 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m560356282(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m560356282_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m394273915_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m394273915(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m394273915_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m665493086_gshared (ReadOnlyCollection_1_t2345413615 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m665493086(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2345413615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m665493086_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m857004868_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m857004868(__this, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m857004868_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m1160377912_gshared (ReadOnlyCollection_1_t2345413615 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m1160377912(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m1160377912_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3565573072_gshared (ReadOnlyCollection_1_t2345413615 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3565573072(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2345413615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3565573072_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2754244155_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2754244155(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2754244155_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m4230392251_gshared (ReadOnlyCollection_1_t2345413615 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m4230392251(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m4230392251_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1918754289_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1918754289(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1918754289_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1965817086_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1965817086(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1965817086_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2735631098_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2735631098(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2735631098_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1310750387_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1310750387(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m1310750387_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2124293914_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2124293914(__this, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2124293914_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2200133923_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2200133923(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2200133923_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2175965052_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2175965052(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2175965052_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m927654109_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m927654109(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m927654109_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m528524987_gshared (ReadOnlyCollection_1_t2345413615 * __this, ParamDataTypeU5BU5D_t835534274* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m528524987(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t2345413615 *, ParamDataTypeU5BU5D_t835534274*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m528524987_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m4186362122_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m4186362122(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m4186362122_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2378092479_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2378092479(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2378092479_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3098505566_gshared (ReadOnlyCollection_1_t2345413615 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m3098505566(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t2345413615 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3098505566_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<HutongGames.PlayMaker.ParamDataType>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m591617530_gshared (ReadOnlyCollection_1_t2345413615 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m591617530(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t2345413615 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m591617530_gshared)(__this, ___index0, method)
