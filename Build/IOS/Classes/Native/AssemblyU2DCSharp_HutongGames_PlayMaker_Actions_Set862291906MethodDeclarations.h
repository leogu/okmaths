﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVisibility
struct SetVisibility_t862291906;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.SetVisibility::.ctor()
extern "C"  void SetVisibility__ctor_m196390054 (SetVisibility_t862291906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::Reset()
extern "C"  void SetVisibility_Reset_m2423603483 (SetVisibility_t862291906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::OnEnter()
extern "C"  void SetVisibility_OnEnter_m2904483075 (SetVisibility_t862291906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::DoSetVisibility(UnityEngine.GameObject)
extern "C"  void SetVisibility_DoSetVisibility_m3809863981 (SetVisibility_t862291906 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::OnExit()
extern "C"  void SetVisibility_OnExit_m1744172571 (SetVisibility_t862291906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVisibility::ResetVisibility()
extern "C"  void SetVisibility_ResetVisibility_m528379017 (SetVisibility_t862291906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
