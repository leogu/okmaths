﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiDropEventsProxy
struct PlayMakerUGuiDropEventsProxy_t1929450712;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void PlayMakerUGuiDropEventsProxy::.ctor()
extern "C"  void PlayMakerUGuiDropEventsProxy__ctor_m3913428283 (PlayMakerUGuiDropEventsProxy_t1929450712 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiDropEventsProxy::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiDropEventsProxy_OnDrop_m1645661223 (PlayMakerUGuiDropEventsProxy_t1929450712 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
