﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct PlayMakerEvent_t1571596806;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy
struct  PlayMakerEventProxy_t3497046822  : public MonoBehaviour_t1158329972
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy::eventTarget
	PlayMakerEventTarget_t2104288969 * ___eventTarget_2;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy::fsmEvent
	PlayMakerEvent_t1571596806 * ___fsmEvent_3;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy::debug
	bool ___debug_4;

public:
	inline static int32_t get_offset_of_eventTarget_2() { return static_cast<int32_t>(offsetof(PlayMakerEventProxy_t3497046822, ___eventTarget_2)); }
	inline PlayMakerEventTarget_t2104288969 * get_eventTarget_2() const { return ___eventTarget_2; }
	inline PlayMakerEventTarget_t2104288969 ** get_address_of_eventTarget_2() { return &___eventTarget_2; }
	inline void set_eventTarget_2(PlayMakerEventTarget_t2104288969 * value)
	{
		___eventTarget_2 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_2, value);
	}

	inline static int32_t get_offset_of_fsmEvent_3() { return static_cast<int32_t>(offsetof(PlayMakerEventProxy_t3497046822, ___fsmEvent_3)); }
	inline PlayMakerEvent_t1571596806 * get_fsmEvent_3() const { return ___fsmEvent_3; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_fsmEvent_3() { return &___fsmEvent_3; }
	inline void set_fsmEvent_3(PlayMakerEvent_t1571596806 * value)
	{
		___fsmEvent_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsmEvent_3, value);
	}

	inline static int32_t get_offset_of_debug_4() { return static_cast<int32_t>(offsetof(PlayMakerEventProxy_t3497046822, ___debug_4)); }
	inline bool get_debug_4() const { return ___debug_4; }
	inline bool* get_address_of_debug_4() { return &___debug_4; }
	inline void set_debug_4(bool value)
	{
		___debug_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
