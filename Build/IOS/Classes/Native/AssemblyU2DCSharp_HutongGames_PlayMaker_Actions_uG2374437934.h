﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar
struct  uGuiInputFieldSetAsterixChar_t2374437934  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::asterixChar
	FsmString_t2414474701 * ___asterixChar_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::_inputField
	InputField_t1631627530 * ____inputField_14;
	// System.Char HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::_originalValue
	Il2CppChar ____originalValue_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetAsterixChar_t2374437934, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_asterixChar_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetAsterixChar_t2374437934, ___asterixChar_12)); }
	inline FsmString_t2414474701 * get_asterixChar_12() const { return ___asterixChar_12; }
	inline FsmString_t2414474701 ** get_address_of_asterixChar_12() { return &___asterixChar_12; }
	inline void set_asterixChar_12(FsmString_t2414474701 * value)
	{
		___asterixChar_12 = value;
		Il2CppCodeGenWriteBarrier(&___asterixChar_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetAsterixChar_t2374437934, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__inputField_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetAsterixChar_t2374437934, ____inputField_14)); }
	inline InputField_t1631627530 * get__inputField_14() const { return ____inputField_14; }
	inline InputField_t1631627530 ** get_address_of__inputField_14() { return &____inputField_14; }
	inline void set__inputField_14(InputField_t1631627530 * value)
	{
		____inputField_14 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_14, value);
	}

	inline static int32_t get_offset_of__originalValue_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetAsterixChar_t2374437934, ____originalValue_15)); }
	inline Il2CppChar get__originalValue_15() const { return ____originalValue_15; }
	inline Il2CppChar* get_address_of__originalValue_15() { return &____originalValue_15; }
	inline void set__originalValue_15(Il2CppChar value)
	{
		____originalValue_15 = value;
	}
};

struct uGuiInputFieldSetAsterixChar_t2374437934_StaticFields
{
public:
	// System.Char HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::__char__
	Il2CppChar _____char___16;

public:
	inline static int32_t get_offset_of___char___16() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetAsterixChar_t2374437934_StaticFields, _____char___16)); }
	inline Il2CppChar get___char___16() const { return _____char___16; }
	inline Il2CppChar* get_address_of___char___16() { return &_____char___16; }
	inline void set___char___16(Il2CppChar value)
	{
		_____char___16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
