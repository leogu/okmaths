﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListRevertToSnapShot
struct ArrayListRevertToSnapShot_t3323918276;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListRevertToSnapShot::.ctor()
extern "C"  void ArrayListRevertToSnapShot__ctor_m3204512824 (ArrayListRevertToSnapShot_t3323918276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRevertToSnapShot::Reset()
extern "C"  void ArrayListRevertToSnapShot_Reset_m2205064317 (ArrayListRevertToSnapShot_t3323918276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRevertToSnapShot::OnEnter()
extern "C"  void ArrayListRevertToSnapShot_OnEnter_m3840766805 (ArrayListRevertToSnapShot_t3323918276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRevertToSnapShot::DoArrayListRevertToSnapShot()
extern "C"  void ArrayListRevertToSnapShot_DoArrayListRevertToSnapShot_m291882477 (ArrayListRevertToSnapShot_t3323918276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
