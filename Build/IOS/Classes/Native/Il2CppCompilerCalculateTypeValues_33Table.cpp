﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1486249478.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iTw510332153.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1227725653.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1258603452.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3102985765.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT2700213129.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3056226867.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3594807706.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT1378538546.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_iT3748785574.h"
#include "AssemblyU2DCSharp_iTweenFSMEvents3874706169.h"
#include "AssemblyU2DCSharp_iTweenFSMType369394580.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_UpdateHelp2226607427.h"
#include "AssemblyU2DCSharp_RadialLayout3190219395.h"
#include "AssemblyU2DCSharp_InputNavigator3938593723.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co2931552185.h"
#include "AssemblyU2DCSharp_RewardAd_menu3222548784.h"
#include "AssemblyU2DCSharp_vcAdmob785204872.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In1135994075.h"
#include "AssemblyU2DCSharp_Voxcat_SceneMan_Constants_Constan446875335.h"
#include "AssemblyU2DCSharp_LoadRequestedScene2989246692.h"
#include "AssemblyU2DCSharp_Voxcat_SceneMan_Controller_Loadi1917474958.h"
#include "AssemblyU2DCSharp_Voxcat_SceneMan_Controller_Scene2773221316.h"
#include "AssemblyU2DCSharp_vcSceneAdmin405751812.h"
#include "AssemblyU2DCSharp_admobdemo4253778382.h"
#include "AssemblyU2DCSharp_iTween488923914.h"
#include "AssemblyU2DCSharp_iTween_EaseType818674011.h"
#include "AssemblyU2DCSharp_iTween_LoopType1490651981.h"
#include "AssemblyU2DCSharp_iTween_NamedValueColor2874784184.h"
#include "AssemblyU2DCSharp_iTween_Defaults4204852305.h"
#include "AssemblyU2DCSharp_iTween_CRSpline4177960625.h"
#include "AssemblyU2DCSharp_iTween_EasingFunction3676968174.h"
#include "AssemblyU2DCSharp_iTween_ApplyTween747394300.h"
#include "AssemblyU2DCSharp_iTween_U3CTweenDelayU3Ec__Iterat1889752800.h"
#include "AssemblyU2DCSharp_iTween_U3CTweenRestartU3Ec__Iter3903815285.h"
#include "AssemblyU2DCSharp_iTween_U3CStartU3Ec__Iterator21700299370.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3116038558.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (iTweenRotateUpdate_t1486249478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3300[7] = 
{
	iTweenRotateUpdate_t1486249478::get_offset_of_gameObject_11(),
	iTweenRotateUpdate_t1486249478::get_offset_of_transformRotation_12(),
	iTweenRotateUpdate_t1486249478::get_offset_of_vectorRotation_13(),
	iTweenRotateUpdate_t1486249478::get_offset_of_time_14(),
	iTweenRotateUpdate_t1486249478::get_offset_of_space_15(),
	iTweenRotateUpdate_t1486249478::get_offset_of_hash_16(),
	iTweenRotateUpdate_t1486249478::get_offset_of_go_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (iTweenScaleAdd_t510332153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3301[8] = 
{
	iTweenScaleAdd_t510332153::get_offset_of_gameObject_19(),
	iTweenScaleAdd_t510332153::get_offset_of_id_20(),
	iTweenScaleAdd_t510332153::get_offset_of_vector_21(),
	iTweenScaleAdd_t510332153::get_offset_of_time_22(),
	iTweenScaleAdd_t510332153::get_offset_of_delay_23(),
	iTweenScaleAdd_t510332153::get_offset_of_speed_24(),
	iTweenScaleAdd_t510332153::get_offset_of_easeType_25(),
	iTweenScaleAdd_t510332153::get_offset_of_loopType_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (iTweenScaleBy_t1227725653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3302[8] = 
{
	iTweenScaleBy_t1227725653::get_offset_of_gameObject_19(),
	iTweenScaleBy_t1227725653::get_offset_of_id_20(),
	iTweenScaleBy_t1227725653::get_offset_of_vector_21(),
	iTweenScaleBy_t1227725653::get_offset_of_time_22(),
	iTweenScaleBy_t1227725653::get_offset_of_delay_23(),
	iTweenScaleBy_t1227725653::get_offset_of_speed_24(),
	iTweenScaleBy_t1227725653::get_offset_of_easeType_25(),
	iTweenScaleBy_t1227725653::get_offset_of_loopType_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (iTweenScaleFrom_t1258603452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3303[9] = 
{
	iTweenScaleFrom_t1258603452::get_offset_of_gameObject_19(),
	iTweenScaleFrom_t1258603452::get_offset_of_id_20(),
	iTweenScaleFrom_t1258603452::get_offset_of_transformScale_21(),
	iTweenScaleFrom_t1258603452::get_offset_of_vectorScale_22(),
	iTweenScaleFrom_t1258603452::get_offset_of_time_23(),
	iTweenScaleFrom_t1258603452::get_offset_of_delay_24(),
	iTweenScaleFrom_t1258603452::get_offset_of_speed_25(),
	iTweenScaleFrom_t1258603452::get_offset_of_easeType_26(),
	iTweenScaleFrom_t1258603452::get_offset_of_loopType_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (iTweenScaleTo_t3102985765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[9] = 
{
	iTweenScaleTo_t3102985765::get_offset_of_gameObject_19(),
	iTweenScaleTo_t3102985765::get_offset_of_id_20(),
	iTweenScaleTo_t3102985765::get_offset_of_transformScale_21(),
	iTweenScaleTo_t3102985765::get_offset_of_vectorScale_22(),
	iTweenScaleTo_t3102985765::get_offset_of_time_23(),
	iTweenScaleTo_t3102985765::get_offset_of_delay_24(),
	iTweenScaleTo_t3102985765::get_offset_of_speed_25(),
	iTweenScaleTo_t3102985765::get_offset_of_easeType_26(),
	iTweenScaleTo_t3102985765::get_offset_of_loopType_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (iTweenScaleUpdate_t2700213129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3305[6] = 
{
	iTweenScaleUpdate_t2700213129::get_offset_of_gameObject_11(),
	iTweenScaleUpdate_t2700213129::get_offset_of_transformScale_12(),
	iTweenScaleUpdate_t2700213129::get_offset_of_vectorScale_13(),
	iTweenScaleUpdate_t2700213129::get_offset_of_time_14(),
	iTweenScaleUpdate_t2700213129::get_offset_of_hash_15(),
	iTweenScaleUpdate_t2700213129::get_offset_of_go_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (iTweenShakePosition_t3056226867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3306[8] = 
{
	iTweenShakePosition_t3056226867::get_offset_of_gameObject_19(),
	iTweenShakePosition_t3056226867::get_offset_of_id_20(),
	iTweenShakePosition_t3056226867::get_offset_of_vector_21(),
	iTweenShakePosition_t3056226867::get_offset_of_time_22(),
	iTweenShakePosition_t3056226867::get_offset_of_delay_23(),
	iTweenShakePosition_t3056226867::get_offset_of_loopType_24(),
	iTweenShakePosition_t3056226867::get_offset_of_space_25(),
	iTweenShakePosition_t3056226867::get_offset_of_axis_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (iTweenShakeRotation_t3594807706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[7] = 
{
	iTweenShakeRotation_t3594807706::get_offset_of_gameObject_19(),
	iTweenShakeRotation_t3594807706::get_offset_of_id_20(),
	iTweenShakeRotation_t3594807706::get_offset_of_vector_21(),
	iTweenShakeRotation_t3594807706::get_offset_of_time_22(),
	iTweenShakeRotation_t3594807706::get_offset_of_delay_23(),
	iTweenShakeRotation_t3594807706::get_offset_of_loopType_24(),
	iTweenShakeRotation_t3594807706::get_offset_of_space_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (iTweenShakeScale_t1378538546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[6] = 
{
	iTweenShakeScale_t1378538546::get_offset_of_gameObject_19(),
	iTweenShakeScale_t1378538546::get_offset_of_id_20(),
	iTweenShakeScale_t1378538546::get_offset_of_vector_21(),
	iTweenShakeScale_t1378538546::get_offset_of_time_22(),
	iTweenShakeScale_t1378538546::get_offset_of_delay_23(),
	iTweenShakeScale_t1378538546::get_offset_of_loopType_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (iTweenStop_t3748785574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3309[5] = 
{
	iTweenStop_t3748785574::get_offset_of_gameObject_11(),
	iTweenStop_t3748785574::get_offset_of_id_12(),
	iTweenStop_t3748785574::get_offset_of_iTweenType_13(),
	iTweenStop_t3748785574::get_offset_of_includeChildren_14(),
	iTweenStop_t3748785574::get_offset_of_inScene_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (iTweenFSMEvents_t3874706169), -1, sizeof(iTweenFSMEvents_t3874706169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3310[5] = 
{
	iTweenFSMEvents_t3874706169_StaticFields::get_offset_of_itweenIDCount_2(),
	iTweenFSMEvents_t3874706169::get_offset_of_itweenID_3(),
	iTweenFSMEvents_t3874706169::get_offset_of_itweenFSMAction_4(),
	iTweenFSMEvents_t3874706169::get_offset_of_donotfinish_5(),
	iTweenFSMEvents_t3874706169::get_offset_of_islooping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (iTweenFSMType_t369394580)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3311[9] = 
{
	iTweenFSMType_t369394580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (UpdateHelper_t2226607427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (RadialLayout_t3190219395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[4] = 
{
	RadialLayout_t3190219395::get_offset_of_fDistance_10(),
	RadialLayout_t3190219395::get_offset_of_MinAngle_11(),
	RadialLayout_t3190219395::get_offset_of_MaxAngle_12(),
	RadialLayout_t3190219395::get_offset_of_StartAngle_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (InputNavigator_t3938593723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[3] = 
{
	InputNavigator_t3938593723::get_offset_of_system_2(),
	InputNavigator_t3938593723::get_offset_of_NavigationKeys_3(),
	InputNavigator_t3938593723::get_offset_of__keyDown_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (CommonDivisorMultiple_t2931552185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[5] = 
{
	CommonDivisorMultiple_t2931552185::get_offset_of_integer1_11(),
	CommonDivisorMultiple_t2931552185::get_offset_of_integer2_12(),
	CommonDivisorMultiple_t2931552185::get_offset_of_storeLC_Divisor_13(),
	CommonDivisorMultiple_t2931552185::get_offset_of_storeGC_Multiple_14(),
	CommonDivisorMultiple_t2931552185::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (RewardAd_menu_t3222548784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[1] = 
{
	RewardAd_menu_t3222548784::get_offset_of_zoneId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (vcAdmob_t785204872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[4] = 
{
	vcAdmob_t785204872::get_offset_of_bannerID_2(),
	vcAdmob_t785204872::get_offset_of_fullID_3(),
	vcAdmob_t785204872::get_offset_of_isTest_4(),
	vcAdmob_t785204872::get_offset_of_ad_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (IntModulo_t1135994075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[5] = 
{
	IntModulo_t1135994075::get_offset_of_dividend_11(),
	IntModulo_t1135994075::get_offset_of_diviser_12(),
	IntModulo_t1135994075::get_offset_of_result_13(),
	IntModulo_t1135994075::get_offset_of_resultAsInt_14(),
	IntModulo_t1135994075::get_offset_of_everyFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (Constants_t446875335), -1, sizeof(Constants_t446875335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3319[2] = 
{
	0,
	Constants_t446875335_StaticFields::get_offset_of_LOADING_SCENE_WAIT_TIME_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (LoadRequestedScene_t2989246692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[1] = 
{
	LoadRequestedScene_t2989246692::get_offset_of_loadingSC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (LoadingSceneController_t1917474958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (SceneController_t2773221316), -1, sizeof(SceneController_t2773221316_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3322[2] = 
{
	SceneController_t2773221316_StaticFields::get_offset_of_scene_0(),
	SceneController_t2773221316_StaticFields::get_offset_of_previousScene_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (vcSceneAdmin_t405751812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (admobdemo_t4253778382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[1] = 
{
	admobdemo_t4253778382::get_offset_of_ad_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (iTween_t488923914), -1, sizeof(iTween_t488923914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3325[50] = 
{
	iTween_t488923914_StaticFields::get_offset_of_tweens_2(),
	iTween_t488923914_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t488923914::get_offset_of_id_4(),
	iTween_t488923914::get_offset_of_type_5(),
	iTween_t488923914::get_offset_of_method_6(),
	iTween_t488923914::get_offset_of_easeType_7(),
	iTween_t488923914::get_offset_of_time_8(),
	iTween_t488923914::get_offset_of_delay_9(),
	iTween_t488923914::get_offset_of_loopType_10(),
	iTween_t488923914::get_offset_of_isRunning_11(),
	iTween_t488923914::get_offset_of_isPaused_12(),
	iTween_t488923914::get_offset_of__name_13(),
	iTween_t488923914::get_offset_of_runningTime_14(),
	iTween_t488923914::get_offset_of_percentage_15(),
	iTween_t488923914::get_offset_of_delayStarted_16(),
	iTween_t488923914::get_offset_of_kinematic_17(),
	iTween_t488923914::get_offset_of_isLocal_18(),
	iTween_t488923914::get_offset_of_loop_19(),
	iTween_t488923914::get_offset_of_reverse_20(),
	iTween_t488923914::get_offset_of_wasPaused_21(),
	iTween_t488923914::get_offset_of_physics_22(),
	iTween_t488923914::get_offset_of_tweenArguments_23(),
	iTween_t488923914::get_offset_of_space_24(),
	iTween_t488923914::get_offset_of_ease_25(),
	iTween_t488923914::get_offset_of_apply_26(),
	iTween_t488923914::get_offset_of_audioSource_27(),
	iTween_t488923914::get_offset_of_vector3s_28(),
	iTween_t488923914::get_offset_of_vector2s_29(),
	iTween_t488923914::get_offset_of_colors_30(),
	iTween_t488923914::get_offset_of_floats_31(),
	iTween_t488923914::get_offset_of_rects_32(),
	iTween_t488923914::get_offset_of_path_33(),
	iTween_t488923914::get_offset_of_preUpdate_34(),
	iTween_t488923914::get_offset_of_postUpdate_35(),
	iTween_t488923914::get_offset_of_namedcolorvalue_36(),
	iTween_t488923914::get_offset_of_lastRealTime_37(),
	iTween_t488923914::get_offset_of_useRealTime_38(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_39(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_40(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_41(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_42(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_43(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_44(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map9_45(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24mapA_46(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24mapB_47(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_48(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24mapD_49(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24mapE_50(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (EaseType_t818674011)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3326[34] = 
{
	EaseType_t818674011::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (LoopType_t1490651981)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3327[4] = 
{
	LoopType_t1490651981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (NamedValueColor_t2874784184)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3328[5] = 
{
	NamedValueColor_t2874784184::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (Defaults_t4204852305), -1, sizeof(Defaults_t4204852305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3329[16] = 
{
	Defaults_t4204852305_StaticFields::get_offset_of_time_0(),
	Defaults_t4204852305_StaticFields::get_offset_of_delay_1(),
	Defaults_t4204852305_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4204852305_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4204852305_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4204852305_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4204852305_StaticFields::get_offset_of_space_7(),
	Defaults_t4204852305_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4204852305_StaticFields::get_offset_of_color_9(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4204852305_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4204852305_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4204852305_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (CRSpline_t4177960625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[1] = 
{
	CRSpline_t4177960625::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (EasingFunction_t3676968174), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (ApplyTween_t747394300), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t1889752800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[3] = 
{
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24PC_0(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t3903815285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[3] = 
{
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24PC_0(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (U3CStartU3Ec__Iterator2_t1700299370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3335[3] = 
{
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24PC_0(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U3CU3Ef__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3336[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (U24ArrayTypeU24124_t116038559)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24124_t116038559_marshaled_pinvoke), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
