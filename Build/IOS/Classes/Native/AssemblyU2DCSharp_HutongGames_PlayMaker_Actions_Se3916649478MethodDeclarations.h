﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmVector3
struct SetFsmVector3_t3916649478;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::.ctor()
extern "C"  void SetFsmVector3__ctor_m758088696 (SetFsmVector3_t3916649478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::Reset()
extern "C"  void SetFsmVector3_Reset_m1334477083 (SetFsmVector3_t3916649478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::OnEnter()
extern "C"  void SetFsmVector3_OnEnter_m309349099 (SetFsmVector3_t3916649478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::DoSetFsmVector3()
extern "C"  void SetFsmVector3_DoSetFsmVector3_m1726989865 (SetFsmVector3_t3916649478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmVector3::OnUpdate()
extern "C"  void SetFsmVector3_OnUpdate_m292780662 (SetFsmVector3_t3916649478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
