﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmVarOverride>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m3554808526(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1497935131 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmVarOverride>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3023743050(__this, method) ((  void (*) (InternalEnumerator_1_t1497935131 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmVarOverride>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3283933858(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1497935131 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmVarOverride>::Dispose()
#define InternalEnumerator_1_Dispose_m1569511173(__this, method) ((  void (*) (InternalEnumerator_1_t1497935131 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmVarOverride>::MoveNext()
#define InternalEnumerator_1_MoveNext_m1437196302(__this, method) ((  bool (*) (InternalEnumerator_1_t1497935131 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmVarOverride>::get_Current()
#define InternalEnumerator_1_get_Current_m2591697853(__this, method) ((  FsmVarOverride_t639182869 * (*) (InternalEnumerator_1_t1497935131 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
