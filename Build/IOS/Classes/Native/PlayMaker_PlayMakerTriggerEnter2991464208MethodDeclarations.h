﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerEnter
struct PlayMakerTriggerEnter_t2991464208;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void PlayMakerTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerEnter_OnTriggerEnter_m3165567559 (PlayMakerTriggerEnter_t2991464208 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerEnter::.ctor()
extern "C"  void PlayMakerTriggerEnter__ctor_m2499662619 (PlayMakerTriggerEnter_t2991464208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
