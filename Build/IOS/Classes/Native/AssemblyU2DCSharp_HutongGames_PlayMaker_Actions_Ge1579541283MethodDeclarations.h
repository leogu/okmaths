﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetLastPointerDataInfo
struct GetLastPointerDataInfo_t1579541283;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::.ctor()
extern "C"  void GetLastPointerDataInfo__ctor_m680352327 (GetLastPointerDataInfo_t1579541283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::Reset()
extern "C"  void GetLastPointerDataInfo_Reset_m1755919440 (GetLastPointerDataInfo_t1579541283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLastPointerDataInfo::OnEnter()
extern "C"  void GetLastPointerDataInfo_OnEnter_m1704236062 (GetLastPointerDataInfo_t1579541283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
