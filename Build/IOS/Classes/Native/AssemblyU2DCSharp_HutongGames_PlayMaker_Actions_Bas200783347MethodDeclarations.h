﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BaseAnimationAction
struct BaseAnimationAction_t200783347;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void HutongGames.PlayMaker.Actions.BaseAnimationAction::.ctor()
extern "C"  void BaseAnimationAction__ctor_m2023490959 (BaseAnimationAction_t200783347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseAnimationAction::OnActionTargetInvoked(System.Object)
extern "C"  void BaseAnimationAction_OnActionTargetInvoked_m3391941707 (BaseAnimationAction_t200783347 * __this, Il2CppObject * ___targetObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
