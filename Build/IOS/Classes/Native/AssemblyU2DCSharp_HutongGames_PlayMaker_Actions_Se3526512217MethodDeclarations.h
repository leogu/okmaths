﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimationTime
struct SetAnimationTime_t3526512217;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::.ctor()
extern "C"  void SetAnimationTime__ctor_m471130227 (SetAnimationTime_t3526512217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::Reset()
extern "C"  void SetAnimationTime_Reset_m2432687754 (SetAnimationTime_t3526512217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::OnEnter()
extern "C"  void SetAnimationTime_OnEnter_m3852008212 (SetAnimationTime_t3526512217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::OnUpdate()
extern "C"  void SetAnimationTime_OnUpdate_m2546068163 (SetAnimationTime_t3526512217 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationTime::DoSetAnimationTime(UnityEngine.GameObject)
extern "C"  void SetAnimationTime_DoSetAnimationTime_m3951448813 (SetAnimationTime_t3526512217 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
