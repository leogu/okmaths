﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveUpdate
struct iTweenMoveUpdate_t784312922;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::.ctor()
extern "C"  void iTweenMoveUpdate__ctor_m2791377102 (iTweenMoveUpdate_t784312922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::Reset()
extern "C"  void iTweenMoveUpdate_Reset_m215152511 (iTweenMoveUpdate_t784312922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnEnter()
extern "C"  void iTweenMoveUpdate_OnEnter_m4089275855 (iTweenMoveUpdate_t784312922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnUpdate()
extern "C"  void iTweenMoveUpdate_OnUpdate_m1248848904 (iTweenMoveUpdate_t784312922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::OnExit()
extern "C"  void iTweenMoveUpdate_OnExit_m1410193767 (iTweenMoveUpdate_t784312922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveUpdate::DoiTween()
extern "C"  void iTweenMoveUpdate_DoiTween_m2799024893 (iTweenMoveUpdate_t784312922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
