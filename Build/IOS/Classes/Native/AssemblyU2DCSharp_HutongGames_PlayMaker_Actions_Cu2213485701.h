﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu1654913835.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2535927234.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CurveRect
struct  CurveRect_t2213485701  : public CurveFsmAction_t1654913835
{
public:
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::rectVariable
	FsmRect_t19023354 * ___rectVariable_35;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::fromValue
	FsmRect_t19023354 * ___fromValue_36;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.CurveRect::toValue
	FsmRect_t19023354 * ___toValue_37;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveX
	FsmAnimationCurve_t326747561 * ___curveX_38;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationX
	int32_t ___calculationX_39;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveY
	FsmAnimationCurve_t326747561 * ___curveY_40;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationY
	int32_t ___calculationY_41;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveW
	FsmAnimationCurve_t326747561 * ___curveW_42;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationW
	int32_t ___calculationW_43;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.CurveRect::curveH
	FsmAnimationCurve_t326747561 * ___curveH_44;
	// HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation HutongGames.PlayMaker.Actions.CurveRect::calculationH
	int32_t ___calculationH_45;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.CurveRect::rct
	Rect_t3681755626  ___rct_46;
	// System.Boolean HutongGames.PlayMaker.Actions.CurveRect::finishInNextStep
	bool ___finishInNextStep_47;

public:
	inline static int32_t get_offset_of_rectVariable_35() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___rectVariable_35)); }
	inline FsmRect_t19023354 * get_rectVariable_35() const { return ___rectVariable_35; }
	inline FsmRect_t19023354 ** get_address_of_rectVariable_35() { return &___rectVariable_35; }
	inline void set_rectVariable_35(FsmRect_t19023354 * value)
	{
		___rectVariable_35 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariable_35, value);
	}

	inline static int32_t get_offset_of_fromValue_36() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___fromValue_36)); }
	inline FsmRect_t19023354 * get_fromValue_36() const { return ___fromValue_36; }
	inline FsmRect_t19023354 ** get_address_of_fromValue_36() { return &___fromValue_36; }
	inline void set_fromValue_36(FsmRect_t19023354 * value)
	{
		___fromValue_36 = value;
		Il2CppCodeGenWriteBarrier(&___fromValue_36, value);
	}

	inline static int32_t get_offset_of_toValue_37() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___toValue_37)); }
	inline FsmRect_t19023354 * get_toValue_37() const { return ___toValue_37; }
	inline FsmRect_t19023354 ** get_address_of_toValue_37() { return &___toValue_37; }
	inline void set_toValue_37(FsmRect_t19023354 * value)
	{
		___toValue_37 = value;
		Il2CppCodeGenWriteBarrier(&___toValue_37, value);
	}

	inline static int32_t get_offset_of_curveX_38() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___curveX_38)); }
	inline FsmAnimationCurve_t326747561 * get_curveX_38() const { return ___curveX_38; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_curveX_38() { return &___curveX_38; }
	inline void set_curveX_38(FsmAnimationCurve_t326747561 * value)
	{
		___curveX_38 = value;
		Il2CppCodeGenWriteBarrier(&___curveX_38, value);
	}

	inline static int32_t get_offset_of_calculationX_39() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___calculationX_39)); }
	inline int32_t get_calculationX_39() const { return ___calculationX_39; }
	inline int32_t* get_address_of_calculationX_39() { return &___calculationX_39; }
	inline void set_calculationX_39(int32_t value)
	{
		___calculationX_39 = value;
	}

	inline static int32_t get_offset_of_curveY_40() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___curveY_40)); }
	inline FsmAnimationCurve_t326747561 * get_curveY_40() const { return ___curveY_40; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_curveY_40() { return &___curveY_40; }
	inline void set_curveY_40(FsmAnimationCurve_t326747561 * value)
	{
		___curveY_40 = value;
		Il2CppCodeGenWriteBarrier(&___curveY_40, value);
	}

	inline static int32_t get_offset_of_calculationY_41() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___calculationY_41)); }
	inline int32_t get_calculationY_41() const { return ___calculationY_41; }
	inline int32_t* get_address_of_calculationY_41() { return &___calculationY_41; }
	inline void set_calculationY_41(int32_t value)
	{
		___calculationY_41 = value;
	}

	inline static int32_t get_offset_of_curveW_42() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___curveW_42)); }
	inline FsmAnimationCurve_t326747561 * get_curveW_42() const { return ___curveW_42; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_curveW_42() { return &___curveW_42; }
	inline void set_curveW_42(FsmAnimationCurve_t326747561 * value)
	{
		___curveW_42 = value;
		Il2CppCodeGenWriteBarrier(&___curveW_42, value);
	}

	inline static int32_t get_offset_of_calculationW_43() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___calculationW_43)); }
	inline int32_t get_calculationW_43() const { return ___calculationW_43; }
	inline int32_t* get_address_of_calculationW_43() { return &___calculationW_43; }
	inline void set_calculationW_43(int32_t value)
	{
		___calculationW_43 = value;
	}

	inline static int32_t get_offset_of_curveH_44() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___curveH_44)); }
	inline FsmAnimationCurve_t326747561 * get_curveH_44() const { return ___curveH_44; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_curveH_44() { return &___curveH_44; }
	inline void set_curveH_44(FsmAnimationCurve_t326747561 * value)
	{
		___curveH_44 = value;
		Il2CppCodeGenWriteBarrier(&___curveH_44, value);
	}

	inline static int32_t get_offset_of_calculationH_45() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___calculationH_45)); }
	inline int32_t get_calculationH_45() const { return ___calculationH_45; }
	inline int32_t* get_address_of_calculationH_45() { return &___calculationH_45; }
	inline void set_calculationH_45(int32_t value)
	{
		___calculationH_45 = value;
	}

	inline static int32_t get_offset_of_rct_46() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___rct_46)); }
	inline Rect_t3681755626  get_rct_46() const { return ___rct_46; }
	inline Rect_t3681755626 * get_address_of_rct_46() { return &___rct_46; }
	inline void set_rct_46(Rect_t3681755626  value)
	{
		___rct_46 = value;
	}

	inline static int32_t get_offset_of_finishInNextStep_47() { return static_cast<int32_t>(offsetof(CurveRect_t2213485701, ___finishInNextStep_47)); }
	inline bool get_finishInNextStep_47() const { return ___finishInNextStep_47; }
	inline bool* get_address_of_finishInNextStep_47() { return &___finishInNextStep_47; }
	inline void set_finishInNextStep_47(bool value)
	{
		___finishInNextStep_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
