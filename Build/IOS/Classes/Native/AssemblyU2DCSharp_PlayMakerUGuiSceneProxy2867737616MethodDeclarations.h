﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiSceneProxy
struct PlayMakerUGuiSceneProxy_t2867737616;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerUGuiSceneProxy::.ctor()
extern "C"  void PlayMakerUGuiSceneProxy__ctor_m127462991 (PlayMakerUGuiSceneProxy_t2867737616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiSceneProxy::Start()
extern "C"  void PlayMakerUGuiSceneProxy_Start_m2519770219 (PlayMakerUGuiSceneProxy_t2867737616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiSceneProxy::Update()
extern "C"  void PlayMakerUGuiSceneProxy_Update_m4284502610 (PlayMakerUGuiSceneProxy_t2867737616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
