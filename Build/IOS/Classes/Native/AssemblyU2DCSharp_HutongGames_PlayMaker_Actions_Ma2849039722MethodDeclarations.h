﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerRegisterHost
struct MasterServerRegisterHost_t2849039722;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::.ctor()
extern "C"  void MasterServerRegisterHost__ctor_m2000828642 (MasterServerRegisterHost_t2849039722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::Reset()
extern "C"  void MasterServerRegisterHost_Reset_m2184518011 (MasterServerRegisterHost_t2849039722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::OnEnter()
extern "C"  void MasterServerRegisterHost_OnEnter_m1908125859 (MasterServerRegisterHost_t2849039722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerRegisterHost::DoMasterServerRegisterHost()
extern "C"  void MasterServerRegisterHost_DoMasterServerRegisterHost_m74230913 (MasterServerRegisterHost_t2849039722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
