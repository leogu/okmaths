﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerExit2D
struct PlayMakerTriggerExit2D_t1071223868;
// UnityEngine.Collider2D
struct Collider2D_t646061738;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void PlayMakerTriggerExit2D::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerExit2D_OnTriggerExit2D_m1603338421 (PlayMakerTriggerExit2D_t1071223868 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerExit2D::.ctor()
extern "C"  void PlayMakerTriggerExit2D__ctor_m2601655989 (PlayMakerTriggerExit2D_t1071223868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
