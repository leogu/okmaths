﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolNoneTrue
struct BoolNoneTrue_t577732384;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::.ctor()
extern "C"  void BoolNoneTrue__ctor_m3578539350 (BoolNoneTrue_t577732384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::Reset()
extern "C"  void BoolNoneTrue_Reset_m77855565 (BoolNoneTrue_t577732384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::OnEnter()
extern "C"  void BoolNoneTrue_OnEnter_m2654069853 (BoolNoneTrue_t577732384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::OnUpdate()
extern "C"  void BoolNoneTrue_OnUpdate_m1780353920 (BoolNoneTrue_t577732384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolNoneTrue::DoNoneTrue()
extern "C"  void BoolNoneTrue_DoNoneTrue_m1928855091 (BoolNoneTrue_t577732384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
