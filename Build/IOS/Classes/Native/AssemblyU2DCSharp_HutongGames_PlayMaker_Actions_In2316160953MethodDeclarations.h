﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntChanged
struct IntChanged_t2316160953;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntChanged::.ctor()
extern "C"  void IntChanged__ctor_m451606899 (IntChanged_t2316160953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntChanged::Reset()
extern "C"  void IntChanged_Reset_m3025117834 (IntChanged_t2316160953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntChanged::OnEnter()
extern "C"  void IntChanged_OnEnter_m389006500 (IntChanged_t2316160953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntChanged::OnUpdate()
extern "C"  void IntChanged_OnUpdate_m44880643 (IntChanged_t2316160953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
