﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmFloat
struct GetFsmFloat_t2460865480;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::.ctor()
extern "C"  void GetFsmFloat__ctor_m2807007202 (GetFsmFloat_t2460865480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::Reset()
extern "C"  void GetFsmFloat_Reset_m3647842909 (GetFsmFloat_t2460865480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::OnEnter()
extern "C"  void GetFsmFloat_OnEnter_m3435247357 (GetFsmFloat_t2460865480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::OnUpdate()
extern "C"  void GetFsmFloat_OnUpdate_m2085190324 (GetFsmFloat_t2460865480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmFloat::DoGetFsmFloat()
extern "C"  void GetFsmFloat_DoGetFsmFloat_m2449648077 (GetFsmFloat_t2460865480 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
