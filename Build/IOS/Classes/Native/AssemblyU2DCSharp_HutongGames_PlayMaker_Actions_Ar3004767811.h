﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight
struct  ArrayListGetFarthestGameObjectInSight_t3004767811  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::distanceFrom
	FsmGameObject_t3097142863 * ___distanceFrom_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::orDistanceFromVector3
	FsmVector3_t3996534004 * ___orDistanceFromVector3_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::everyframe
	bool ___everyframe_16;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::fromGameObject
	FsmOwnerDefault_t2023674184 * ___fromGameObject_17;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::layerMask
	FsmIntU5BU5D_t2637547802* ___layerMask_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::invertMask
	FsmBool_t664485696 * ___invertMask_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::farthestGameObject
	FsmGameObject_t3097142863 * ___farthestGameObject_20;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::farthestIndex
	FsmInt_t1273009179 * ___farthestIndex_21;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_distanceFrom_14() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___distanceFrom_14)); }
	inline FsmGameObject_t3097142863 * get_distanceFrom_14() const { return ___distanceFrom_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_distanceFrom_14() { return &___distanceFrom_14; }
	inline void set_distanceFrom_14(FsmGameObject_t3097142863 * value)
	{
		___distanceFrom_14 = value;
		Il2CppCodeGenWriteBarrier(&___distanceFrom_14, value);
	}

	inline static int32_t get_offset_of_orDistanceFromVector3_15() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___orDistanceFromVector3_15)); }
	inline FsmVector3_t3996534004 * get_orDistanceFromVector3_15() const { return ___orDistanceFromVector3_15; }
	inline FsmVector3_t3996534004 ** get_address_of_orDistanceFromVector3_15() { return &___orDistanceFromVector3_15; }
	inline void set_orDistanceFromVector3_15(FsmVector3_t3996534004 * value)
	{
		___orDistanceFromVector3_15 = value;
		Il2CppCodeGenWriteBarrier(&___orDistanceFromVector3_15, value);
	}

	inline static int32_t get_offset_of_everyframe_16() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___everyframe_16)); }
	inline bool get_everyframe_16() const { return ___everyframe_16; }
	inline bool* get_address_of_everyframe_16() { return &___everyframe_16; }
	inline void set_everyframe_16(bool value)
	{
		___everyframe_16 = value;
	}

	inline static int32_t get_offset_of_fromGameObject_17() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___fromGameObject_17)); }
	inline FsmOwnerDefault_t2023674184 * get_fromGameObject_17() const { return ___fromGameObject_17; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_fromGameObject_17() { return &___fromGameObject_17; }
	inline void set_fromGameObject_17(FsmOwnerDefault_t2023674184 * value)
	{
		___fromGameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___fromGameObject_17, value);
	}

	inline static int32_t get_offset_of_layerMask_18() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___layerMask_18)); }
	inline FsmIntU5BU5D_t2637547802* get_layerMask_18() const { return ___layerMask_18; }
	inline FsmIntU5BU5D_t2637547802** get_address_of_layerMask_18() { return &___layerMask_18; }
	inline void set_layerMask_18(FsmIntU5BU5D_t2637547802* value)
	{
		___layerMask_18 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_18, value);
	}

	inline static int32_t get_offset_of_invertMask_19() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___invertMask_19)); }
	inline FsmBool_t664485696 * get_invertMask_19() const { return ___invertMask_19; }
	inline FsmBool_t664485696 ** get_address_of_invertMask_19() { return &___invertMask_19; }
	inline void set_invertMask_19(FsmBool_t664485696 * value)
	{
		___invertMask_19 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_19, value);
	}

	inline static int32_t get_offset_of_farthestGameObject_20() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___farthestGameObject_20)); }
	inline FsmGameObject_t3097142863 * get_farthestGameObject_20() const { return ___farthestGameObject_20; }
	inline FsmGameObject_t3097142863 ** get_address_of_farthestGameObject_20() { return &___farthestGameObject_20; }
	inline void set_farthestGameObject_20(FsmGameObject_t3097142863 * value)
	{
		___farthestGameObject_20 = value;
		Il2CppCodeGenWriteBarrier(&___farthestGameObject_20, value);
	}

	inline static int32_t get_offset_of_farthestIndex_21() { return static_cast<int32_t>(offsetof(ArrayListGetFarthestGameObjectInSight_t3004767811, ___farthestIndex_21)); }
	inline FsmInt_t1273009179 * get_farthestIndex_21() const { return ___farthestIndex_21; }
	inline FsmInt_t1273009179 ** get_address_of_farthestIndex_21() { return &___farthestIndex_21; }
	inline void set_farthestIndex_21(FsmInt_t1273009179 * value)
	{
		___farthestIndex_21 = value;
		Il2CppCodeGenWriteBarrier(&___farthestIndex_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
