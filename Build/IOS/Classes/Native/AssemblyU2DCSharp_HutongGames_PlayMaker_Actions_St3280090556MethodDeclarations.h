﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StartLocationServiceUpdates
struct StartLocationServiceUpdates_t3280090556;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::.ctor()
extern "C"  void StartLocationServiceUpdates__ctor_m1061948870 (StartLocationServiceUpdates_t3280090556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::Reset()
extern "C"  void StartLocationServiceUpdates_Reset_m4240314969 (StartLocationServiceUpdates_t3280090556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::OnEnter()
extern "C"  void StartLocationServiceUpdates_OnEnter_m3254151841 (StartLocationServiceUpdates_t3280090556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartLocationServiceUpdates::OnUpdate()
extern "C"  void StartLocationServiceUpdates_OnUpdate_m3191523328 (StartLocationServiceUpdates_t3280090556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
