﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayReverse
struct ArrayReverse_t806552339;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::.ctor()
extern "C"  void ArrayReverse__ctor_m3848951103 (ArrayReverse_t806552339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::Reset()
extern "C"  void ArrayReverse_Reset_m3049209560 (ArrayReverse_t806552339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayReverse::OnEnter()
extern "C"  void ArrayReverse_OnEnter_m3121962446 (ArrayReverse_t806552339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
