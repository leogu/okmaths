﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformShakePosition
struct DOTweenTransformShakePosition_t3444484117;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakePosition::.ctor()
extern "C"  void DOTweenTransformShakePosition__ctor_m3625839893 (DOTweenTransformShakePosition_t3444484117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakePosition::Reset()
extern "C"  void DOTweenTransformShakePosition_Reset_m1737448266 (DOTweenTransformShakePosition_t3444484117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakePosition::OnEnter()
extern "C"  void DOTweenTransformShakePosition_OnEnter_m1502726464 (DOTweenTransformShakePosition_t3444484117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakePosition::<OnEnter>m__DE()
extern "C"  void DOTweenTransformShakePosition_U3COnEnterU3Em__DE_m1333554540 (DOTweenTransformShakePosition_t3444484117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakePosition::<OnEnter>m__DF()
extern "C"  void DOTweenTransformShakePosition_U3COnEnterU3Em__DF_m1333554441 (DOTweenTransformShakePosition_t3444484117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
