﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime
struct  DOTweenTrailRendererTime_t3352072962  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::to
	FsmFloat_t937133978 * ___to_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::duration
	FsmFloat_t937133978 * ___duration_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::startDelay
	FsmFloat_t937133978 * ___startDelay_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::startEvent
	FsmEvent_t1258573736 * ___startEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_18;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::tweenIdDescription
	String_t* ___tweenIdDescription_19;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::tweenIdType
	int32_t ___tweenIdType_20;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::stringAsId
	FsmString_t2414474701 * ___stringAsId_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::tagAsId
	FsmString_t2414474701 * ___tagAsId_22;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::selectedEase
	int32_t ___selectedEase_23;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::easeType
	int32_t ___easeType_24;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_25;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_26;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::loops
	FsmInt_t1273009179 * ___loops_27;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::loopType
	int32_t ___loopType_28;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_29;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::recyclable
	FsmBool_t664485696 * ___recyclable_30;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::updateType
	int32_t ___updateType_31;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_32;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::debugThis
	FsmBool_t664485696 * ___debugThis_33;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::tweener
	Tweener_t760404022 * ___tweener_34;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_to_12() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___to_12)); }
	inline FsmFloat_t937133978 * get_to_12() const { return ___to_12; }
	inline FsmFloat_t937133978 ** get_address_of_to_12() { return &___to_12; }
	inline void set_to_12(FsmFloat_t937133978 * value)
	{
		___to_12 = value;
		Il2CppCodeGenWriteBarrier(&___to_12, value);
	}

	inline static int32_t get_offset_of_duration_13() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___duration_13)); }
	inline FsmFloat_t937133978 * get_duration_13() const { return ___duration_13; }
	inline FsmFloat_t937133978 ** get_address_of_duration_13() { return &___duration_13; }
	inline void set_duration_13(FsmFloat_t937133978 * value)
	{
		___duration_13 = value;
		Il2CppCodeGenWriteBarrier(&___duration_13, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_14() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___setSpeedBased_14)); }
	inline FsmBool_t664485696 * get_setSpeedBased_14() const { return ___setSpeedBased_14; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_14() { return &___setSpeedBased_14; }
	inline void set_setSpeedBased_14(FsmBool_t664485696 * value)
	{
		___setSpeedBased_14 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_14, value);
	}

	inline static int32_t get_offset_of_startDelay_15() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___startDelay_15)); }
	inline FsmFloat_t937133978 * get_startDelay_15() const { return ___startDelay_15; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_15() { return &___startDelay_15; }
	inline void set_startDelay_15(FsmFloat_t937133978 * value)
	{
		___startDelay_15 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_15, value);
	}

	inline static int32_t get_offset_of_startEvent_16() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___startEvent_16)); }
	inline FsmEvent_t1258573736 * get_startEvent_16() const { return ___startEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_16() { return &___startEvent_16; }
	inline void set_startEvent_16(FsmEvent_t1258573736 * value)
	{
		___startEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_16, value);
	}

	inline static int32_t get_offset_of_finishEvent_17() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___finishEvent_17)); }
	inline FsmEvent_t1258573736 * get_finishEvent_17() const { return ___finishEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_17() { return &___finishEvent_17; }
	inline void set_finishEvent_17(FsmEvent_t1258573736 * value)
	{
		___finishEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_17, value);
	}

	inline static int32_t get_offset_of_finishImmediately_18() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___finishImmediately_18)); }
	inline FsmBool_t664485696 * get_finishImmediately_18() const { return ___finishImmediately_18; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_18() { return &___finishImmediately_18; }
	inline void set_finishImmediately_18(FsmBool_t664485696 * value)
	{
		___finishImmediately_18 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_18, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_19() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___tweenIdDescription_19)); }
	inline String_t* get_tweenIdDescription_19() const { return ___tweenIdDescription_19; }
	inline String_t** get_address_of_tweenIdDescription_19() { return &___tweenIdDescription_19; }
	inline void set_tweenIdDescription_19(String_t* value)
	{
		___tweenIdDescription_19 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_19, value);
	}

	inline static int32_t get_offset_of_tweenIdType_20() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___tweenIdType_20)); }
	inline int32_t get_tweenIdType_20() const { return ___tweenIdType_20; }
	inline int32_t* get_address_of_tweenIdType_20() { return &___tweenIdType_20; }
	inline void set_tweenIdType_20(int32_t value)
	{
		___tweenIdType_20 = value;
	}

	inline static int32_t get_offset_of_stringAsId_21() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___stringAsId_21)); }
	inline FsmString_t2414474701 * get_stringAsId_21() const { return ___stringAsId_21; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_21() { return &___stringAsId_21; }
	inline void set_stringAsId_21(FsmString_t2414474701 * value)
	{
		___stringAsId_21 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_21, value);
	}

	inline static int32_t get_offset_of_tagAsId_22() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___tagAsId_22)); }
	inline FsmString_t2414474701 * get_tagAsId_22() const { return ___tagAsId_22; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_22() { return &___tagAsId_22; }
	inline void set_tagAsId_22(FsmString_t2414474701 * value)
	{
		___tagAsId_22 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_22, value);
	}

	inline static int32_t get_offset_of_selectedEase_23() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___selectedEase_23)); }
	inline int32_t get_selectedEase_23() const { return ___selectedEase_23; }
	inline int32_t* get_address_of_selectedEase_23() { return &___selectedEase_23; }
	inline void set_selectedEase_23(int32_t value)
	{
		___selectedEase_23 = value;
	}

	inline static int32_t get_offset_of_easeType_24() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___easeType_24)); }
	inline int32_t get_easeType_24() const { return ___easeType_24; }
	inline int32_t* get_address_of_easeType_24() { return &___easeType_24; }
	inline void set_easeType_24(int32_t value)
	{
		___easeType_24 = value;
	}

	inline static int32_t get_offset_of_animationCurve_25() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___animationCurve_25)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_25() const { return ___animationCurve_25; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_25() { return &___animationCurve_25; }
	inline void set_animationCurve_25(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_25 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_25, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_26() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___loopsDescriptionArea_26)); }
	inline String_t* get_loopsDescriptionArea_26() const { return ___loopsDescriptionArea_26; }
	inline String_t** get_address_of_loopsDescriptionArea_26() { return &___loopsDescriptionArea_26; }
	inline void set_loopsDescriptionArea_26(String_t* value)
	{
		___loopsDescriptionArea_26 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_26, value);
	}

	inline static int32_t get_offset_of_loops_27() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___loops_27)); }
	inline FsmInt_t1273009179 * get_loops_27() const { return ___loops_27; }
	inline FsmInt_t1273009179 ** get_address_of_loops_27() { return &___loops_27; }
	inline void set_loops_27(FsmInt_t1273009179 * value)
	{
		___loops_27 = value;
		Il2CppCodeGenWriteBarrier(&___loops_27, value);
	}

	inline static int32_t get_offset_of_loopType_28() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___loopType_28)); }
	inline int32_t get_loopType_28() const { return ___loopType_28; }
	inline int32_t* get_address_of_loopType_28() { return &___loopType_28; }
	inline void set_loopType_28(int32_t value)
	{
		___loopType_28 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_29() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___autoKillOnCompletion_29)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_29() const { return ___autoKillOnCompletion_29; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_29() { return &___autoKillOnCompletion_29; }
	inline void set_autoKillOnCompletion_29(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_29 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_29, value);
	}

	inline static int32_t get_offset_of_recyclable_30() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___recyclable_30)); }
	inline FsmBool_t664485696 * get_recyclable_30() const { return ___recyclable_30; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_30() { return &___recyclable_30; }
	inline void set_recyclable_30(FsmBool_t664485696 * value)
	{
		___recyclable_30 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_30, value);
	}

	inline static int32_t get_offset_of_updateType_31() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___updateType_31)); }
	inline int32_t get_updateType_31() const { return ___updateType_31; }
	inline int32_t* get_address_of_updateType_31() { return &___updateType_31; }
	inline void set_updateType_31(int32_t value)
	{
		___updateType_31 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_32() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___isIndependentUpdate_32)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_32() const { return ___isIndependentUpdate_32; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_32() { return &___isIndependentUpdate_32; }
	inline void set_isIndependentUpdate_32(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_32 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_32, value);
	}

	inline static int32_t get_offset_of_debugThis_33() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___debugThis_33)); }
	inline FsmBool_t664485696 * get_debugThis_33() const { return ___debugThis_33; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_33() { return &___debugThis_33; }
	inline void set_debugThis_33(FsmBool_t664485696 * value)
	{
		___debugThis_33 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_33, value);
	}

	inline static int32_t get_offset_of_tweener_34() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererTime_t3352072962, ___tweener_34)); }
	inline Tweener_t760404022 * get_tweener_34() const { return ___tweener_34; }
	inline Tweener_t760404022 ** get_address_of_tweener_34() { return &___tweener_34; }
	inline void set_tweener_34(Tweener_t760404022 * value)
	{
		___tweener_34 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
