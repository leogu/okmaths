﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableAddMany
struct HashTableAddMany_t4129373816;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableAddMany::.ctor()
extern "C"  void HashTableAddMany__ctor_m2216779920 (HashTableAddMany_t4129373816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableAddMany::Reset()
extern "C"  void HashTableAddMany_Reset_m2031727585 (HashTableAddMany_t4129373816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableAddMany::OnEnter()
extern "C"  void HashTableAddMany_OnEnter_m2903139937 (HashTableAddMany_t4129373816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableAddMany::AddToHashTable()
extern "C"  void HashTableAddMany_AddToHashTable_m1412934004 (HashTableAddMany_t4129373816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
