﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerUnregisterHost
struct MasterServerUnregisterHost_t1508119803;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerUnregisterHost::.ctor()
extern "C"  void MasterServerUnregisterHost__ctor_m4034402573 (MasterServerUnregisterHost_t1508119803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerUnregisterHost::OnEnter()
extern "C"  void MasterServerUnregisterHost_OnEnter_m2903411126 (MasterServerUnregisterHost_t1508119803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
