﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorFloat
struct GetAnimatorFloat_t2698660235;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::.ctor()
extern "C"  void GetAnimatorFloat__ctor_m1137293739 (GetAnimatorFloat_t2698660235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::Reset()
extern "C"  void GetAnimatorFloat_Reset_m1933214780 (GetAnimatorFloat_t2698660235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnEnter()
extern "C"  void GetAnimatorFloat_OnEnter_m3772006474 (GetAnimatorFloat_t2698660235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::OnActionUpdate()
extern "C"  void GetAnimatorFloat_OnActionUpdate_m522843833 (GetAnimatorFloat_t2698660235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFloat::GetParameter()
extern "C"  void GetAnimatorFloat_GetParameter_m3235255100 (GetAnimatorFloat_t2698660235 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
