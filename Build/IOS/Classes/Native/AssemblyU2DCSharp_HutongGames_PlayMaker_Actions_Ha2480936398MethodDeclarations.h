﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableValues
struct HashTableValues_t2480936398;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableValues::.ctor()
extern "C"  void HashTableValues__ctor_m3038876316 (HashTableValues_t2480936398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableValues::Reset()
extern "C"  void HashTableValues_Reset_m3482293703 (HashTableValues_t2480936398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableValues::OnEnter()
extern "C"  void HashTableValues_OnEnter_m2450494391 (HashTableValues_t2480936398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableValues::doHashTableValues()
extern "C"  void HashTableValues_doHashTableValues_m698603993 (HashTableValues_t2480936398 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
