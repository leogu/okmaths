﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomEvent
struct RandomEvent_t1725515053;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomEvent::.ctor()
extern "C"  void RandomEvent__ctor_m4270943857 (RandomEvent_t1725515053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomEvent::Reset()
extern "C"  void RandomEvent_Reset_m536526278 (RandomEvent_t1725515053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomEvent::OnEnter()
extern "C"  void RandomEvent_OnEnter_m785253148 (RandomEvent_t1725515053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomEvent::OnUpdate()
extern "C"  void RandomEvent_OnUpdate_m2952673333 (RandomEvent_t1725515053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RandomEvent::GetRandomEvent()
extern "C"  FsmEvent_t1258573736 * RandomEvent_GetRandomEvent_m2298737225 (RandomEvent_t1725515053 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
