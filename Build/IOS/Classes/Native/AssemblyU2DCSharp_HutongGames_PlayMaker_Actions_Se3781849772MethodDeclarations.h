﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAudioClip
struct SetAudioClip_t3781849772;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAudioClip::.ctor()
extern "C"  void SetAudioClip__ctor_m2342809448 (SetAudioClip_t3781849772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioClip::Reset()
extern "C"  void SetAudioClip_Reset_m657147185 (SetAudioClip_t3781849772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioClip::OnEnter()
extern "C"  void SetAudioClip_OnEnter_m3691524257 (SetAudioClip_t3781849772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
