﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Voxcat.SceneMan.Controller.SceneController
struct  SceneController_t2773221316  : public Il2CppObject
{
public:

public:
};

struct SceneController_t2773221316_StaticFields
{
public:
	// System.String Voxcat.SceneMan.Controller.SceneController::scene
	String_t* ___scene_0;
	// System.String Voxcat.SceneMan.Controller.SceneController::previousScene
	String_t* ___previousScene_1;

public:
	inline static int32_t get_offset_of_scene_0() { return static_cast<int32_t>(offsetof(SceneController_t2773221316_StaticFields, ___scene_0)); }
	inline String_t* get_scene_0() const { return ___scene_0; }
	inline String_t** get_address_of_scene_0() { return &___scene_0; }
	inline void set_scene_0(String_t* value)
	{
		___scene_0 = value;
		Il2CppCodeGenWriteBarrier(&___scene_0, value);
	}

	inline static int32_t get_offset_of_previousScene_1() { return static_cast<int32_t>(offsetof(SceneController_t2773221316_StaticFields, ___previousScene_1)); }
	inline String_t* get_previousScene_1() const { return ___previousScene_1; }
	inline String_t** get_address_of_previousScene_1() { return &___previousScene_1; }
	inline void set_previousScene_1(String_t* value)
	{
		___previousScene_1 = value;
		Il2CppCodeGenWriteBarrier(&___previousScene_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
