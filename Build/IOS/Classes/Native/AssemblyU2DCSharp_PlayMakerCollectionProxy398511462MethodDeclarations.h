﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollectionProxy
struct PlayMakerCollectionProxy_t398511462;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void PlayMakerCollectionProxy::.ctor()
extern "C"  void PlayMakerCollectionProxy__ctor_m1172878553 (PlayMakerCollectionProxy_t398511462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerCollectionProxy::getFsmVariableType(HutongGames.PlayMaker.VariableType)
extern "C"  String_t* PlayMakerCollectionProxy_getFsmVariableType_m2821990036 (PlayMakerCollectionProxy_t398511462 * __this, int32_t ____type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollectionProxy::dispatchEvent(System.String,System.Object,System.String)
extern "C"  void PlayMakerCollectionProxy_dispatchEvent_m72092547 (PlayMakerCollectionProxy_t398511462 * __this, String_t* ___anEvent0, Il2CppObject * ___value1, String_t* ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollectionProxy::cleanPrefilledLists()
extern "C"  void PlayMakerCollectionProxy_cleanPrefilledLists_m3176425474 (PlayMakerCollectionProxy_t398511462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
