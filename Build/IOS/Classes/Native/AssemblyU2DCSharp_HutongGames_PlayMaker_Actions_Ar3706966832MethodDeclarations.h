﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects
struct ArrayListActivateGameObjects_t3706966832;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::.ctor()
extern "C"  void ArrayListActivateGameObjects__ctor_m214738298 (ArrayListActivateGameObjects_t3706966832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::Reset()
extern "C"  void ArrayListActivateGameObjects_Reset_m3068972961 (ArrayListActivateGameObjects_t3706966832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::OnEnter()
extern "C"  void ArrayListActivateGameObjects_OnEnter_m607928241 (ArrayListActivateGameObjects_t3706966832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::DoAction()
extern "C"  void ArrayListActivateGameObjects_DoAction_m773371253 (ArrayListActivateGameObjects_t3706966832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::OnExit()
extern "C"  void ArrayListActivateGameObjects_OnExit_m529858845 (ArrayListActivateGameObjects_t3706966832 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
