﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextChild
struct GetNextChild_t1059056315;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextChild::.ctor()
extern "C"  void GetNextChild__ctor_m2126786693 (GetNextChild_t1059056315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::Reset()
extern "C"  void GetNextChild_Reset_m3815180268 (GetNextChild_t1059056315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::OnEnter()
extern "C"  void GetNextChild_OnEnter_m2817706230 (GetNextChild_t1059056315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextChild::DoGetNextChild(UnityEngine.GameObject)
extern "C"  void GetNextChild_DoGetNextChild_m1173271661 (GetNextChild_t1059056315 * __this, GameObject_t1756533147 * ___parent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
