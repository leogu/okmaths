﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmInt
struct SetFsmInt_t1716896067;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::.ctor()
extern "C"  void SetFsmInt__ctor_m1425760627 (SetFsmInt_t1716896067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::Reset()
extern "C"  void SetFsmInt_Reset_m3882309568 (SetFsmInt_t1716896067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::OnEnter()
extern "C"  void SetFsmInt_OnEnter_m570485166 (SetFsmInt_t1716896067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::DoSetFsmInt()
extern "C"  void SetFsmInt_DoSetFsmInt_m3294848813 (SetFsmInt_t1716896067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmInt::OnUpdate()
extern "C"  void SetFsmInt_OnUpdate_m4123470211 (SetFsmInt_t1716896067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
