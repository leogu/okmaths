﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime
struct SetAnimatorPlayBackTime_t170527177;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::.ctor()
extern "C"  void SetAnimatorPlayBackTime__ctor_m1760046841 (SetAnimatorPlayBackTime_t170527177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::Reset()
extern "C"  void SetAnimatorPlayBackTime_Reset_m1562871110 (SetAnimatorPlayBackTime_t170527177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::OnEnter()
extern "C"  void SetAnimatorPlayBackTime_OnEnter_m50939044 (SetAnimatorPlayBackTime_t170527177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::OnUpdate()
extern "C"  void SetAnimatorPlayBackTime_OnUpdate_m2906049533 (SetAnimatorPlayBackTime_t170527177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackTime::DoPlaybackTime()
extern "C"  void SetAnimatorPlayBackTime_DoPlaybackTime_m4030669916 (SetAnimatorPlayBackTime_t170527177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
