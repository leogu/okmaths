﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation
struct RectTransformGetLocalRotation_t1390810947;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::.ctor()
extern "C"  void RectTransformGetLocalRotation__ctor_m550849873 (RectTransformGetLocalRotation_t1390810947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::Reset()
extern "C"  void RectTransformGetLocalRotation_Reset_m3561354884 (RectTransformGetLocalRotation_t1390810947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::OnEnter()
extern "C"  void RectTransformGetLocalRotation_OnEnter_m605447310 (RectTransformGetLocalRotation_t1390810947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::OnActionUpdate()
extern "C"  void RectTransformGetLocalRotation_OnActionUpdate_m444283795 (RectTransformGetLocalRotation_t1390810947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalRotation::DoGetValues()
extern "C"  void RectTransformGetLocalRotation_DoGetValues_m3209268866 (RectTransformGetLocalRotation_t1390810947 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
