﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmColor
struct GetFsmColor_t316829825;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::.ctor()
extern "C"  void GetFsmColor__ctor_m2251737289 (GetFsmColor_t316829825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::Reset()
extern "C"  void GetFsmColor_Reset_m2081980278 (GetFsmColor_t316829825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::OnEnter()
extern "C"  void GetFsmColor_OnEnter_m1512053084 (GetFsmColor_t316829825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::OnUpdate()
extern "C"  void GetFsmColor_OnUpdate_m2851038789 (GetFsmColor_t316829825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmColor::DoGetFsmColor()
extern "C"  void GetFsmColor_DoGetFsmColor_m2630284577 (GetFsmColor_t316829825 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
