﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAudioLoop
struct SetAudioLoop_t4164931234;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAudioLoop::.ctor()
extern "C"  void SetAudioLoop__ctor_m430751728 (SetAudioLoop_t4164931234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioLoop::Reset()
extern "C"  void SetAudioLoop_Reset_m2148113671 (SetAudioLoop_t4164931234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioLoop::OnEnter()
extern "C"  void SetAudioLoop_OnEnter_m2879467871 (SetAudioLoop_t4164931234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
