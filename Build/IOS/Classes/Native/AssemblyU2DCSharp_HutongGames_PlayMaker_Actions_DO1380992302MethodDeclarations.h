﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbody2DRotate
struct DOTweenRigidbody2DRotate_t1380992302;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DRotate::.ctor()
extern "C"  void DOTweenRigidbody2DRotate__ctor_m35320146 (DOTweenRigidbody2DRotate_t1380992302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DRotate::Reset()
extern "C"  void DOTweenRigidbody2DRotate_Reset_m1110887259 (DOTweenRigidbody2DRotate_t1380992302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DRotate::OnEnter()
extern "C"  void DOTweenRigidbody2DRotate_OnEnter_m1122926803 (DOTweenRigidbody2DRotate_t1380992302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DRotate::<OnEnter>m__80()
extern "C"  void DOTweenRigidbody2DRotate_U3COnEnterU3Em__80_m3572877622 (DOTweenRigidbody2DRotate_t1380992302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DRotate::<OnEnter>m__81()
extern "C"  void DOTweenRigidbody2DRotate_U3COnEnterU3Em__81_m3431715121 (DOTweenRigidbody2DRotate_t1380992302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
