﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableContainsValue
struct HashTableContainsValue_t3852786858;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableContainsValue::.ctor()
extern "C"  void HashTableContainsValue__ctor_m3039451198 (HashTableContainsValue_t3852786858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContainsValue::Reset()
extern "C"  void HashTableContainsValue_Reset_m2853312239 (HashTableContainsValue_t3852786858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContainsValue::OnEnter()
extern "C"  void HashTableContainsValue_OnEnter_m3201388815 (HashTableContainsValue_t3852786858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContainsValue::doContainsValue()
extern "C"  void HashTableContainsValue_doContainsValue_m1165665089 (HashTableContainsValue_t3852786858 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
