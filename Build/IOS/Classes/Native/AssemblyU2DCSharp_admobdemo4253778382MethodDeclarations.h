﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// admobdemo
struct admobdemo_t4253778382;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void admobdemo::.ctor()
extern "C"  void admobdemo__ctor_m2338502621 (admobdemo_t4253778382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::Start()
extern "C"  void admobdemo_Start_m1355362069 (admobdemo_t4253778382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::Update()
extern "C"  void admobdemo_Update_m435938768 (admobdemo_t4253778382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::initAdmob()
extern "C"  void admobdemo_initAdmob_m4197987538 (admobdemo_t4253778382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::OnGUI()
extern "C"  void admobdemo_OnGUI_m3236244459 (admobdemo_t4253778382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::onInterstitialEvent(System.String,System.String)
extern "C"  void admobdemo_onInterstitialEvent_m100663414 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::onBannerEvent(System.String,System.String)
extern "C"  void admobdemo_onBannerEvent_m3861622086 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::onRewardedVideoEvent(System.String,System.String)
extern "C"  void admobdemo_onRewardedVideoEvent_m3014261907 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void admobdemo::onNativeBannerEvent(System.String,System.String)
extern "C"  void admobdemo_onNativeBannerEvent_m2650432589 (admobdemo_t4253778382 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
