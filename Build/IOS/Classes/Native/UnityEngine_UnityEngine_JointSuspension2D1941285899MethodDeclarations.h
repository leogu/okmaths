﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointSuspension2D
struct JointSuspension2D_t1941285899;
struct JointSuspension2D_t1941285899_marshaled_pinvoke;
struct JointSuspension2D_t1941285899_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointSuspension2D1941285899.h"

// System.Void UnityEngine.JointSuspension2D::set_dampingRatio(System.Single)
extern "C"  void JointSuspension2D_set_dampingRatio_m3368877289 (JointSuspension2D_t1941285899 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointSuspension2D::set_frequency(System.Single)
extern "C"  void JointSuspension2D_set_frequency_m2889659074 (JointSuspension2D_t1941285899 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointSuspension2D::set_angle(System.Single)
extern "C"  void JointSuspension2D_set_angle_m3265933329 (JointSuspension2D_t1941285899 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointSuspension2D_t1941285899;
struct JointSuspension2D_t1941285899_marshaled_pinvoke;

extern "C" void JointSuspension2D_t1941285899_marshal_pinvoke(const JointSuspension2D_t1941285899& unmarshaled, JointSuspension2D_t1941285899_marshaled_pinvoke& marshaled);
extern "C" void JointSuspension2D_t1941285899_marshal_pinvoke_back(const JointSuspension2D_t1941285899_marshaled_pinvoke& marshaled, JointSuspension2D_t1941285899& unmarshaled);
extern "C" void JointSuspension2D_t1941285899_marshal_pinvoke_cleanup(JointSuspension2D_t1941285899_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointSuspension2D_t1941285899;
struct JointSuspension2D_t1941285899_marshaled_com;

extern "C" void JointSuspension2D_t1941285899_marshal_com(const JointSuspension2D_t1941285899& unmarshaled, JointSuspension2D_t1941285899_marshaled_com& marshaled);
extern "C" void JointSuspension2D_t1941285899_marshal_com_back(const JointSuspension2D_t1941285899_marshaled_com& marshaled, JointSuspension2D_t1941285899& unmarshaled);
extern "C" void JointSuspension2D_t1941285899_marshal_com_cleanup(JointSuspension2D_t1941285899_marshaled_com& marshaled);
