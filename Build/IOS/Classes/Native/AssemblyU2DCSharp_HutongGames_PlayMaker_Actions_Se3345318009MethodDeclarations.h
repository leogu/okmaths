﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmMaterial
struct SetFsmMaterial_t3345318009;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::.ctor()
extern "C"  void SetFsmMaterial__ctor_m2892592307 (SetFsmMaterial_t3345318009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::Reset()
extern "C"  void SetFsmMaterial_Reset_m1166806346 (SetFsmMaterial_t3345318009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::OnEnter()
extern "C"  void SetFsmMaterial_OnEnter_m3489241060 (SetFsmMaterial_t3345318009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::DoSetFsmBool()
extern "C"  void SetFsmMaterial_DoSetFsmBool_m2270217842 (SetFsmMaterial_t3345318009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmMaterial::OnUpdate()
extern "C"  void SetFsmMaterial_OnUpdate_m1883567683 (SetFsmMaterial_t3345318009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
