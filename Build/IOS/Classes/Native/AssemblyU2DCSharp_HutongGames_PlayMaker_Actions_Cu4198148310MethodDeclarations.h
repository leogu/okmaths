﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CutToCamera
struct CutToCamera_t4198148310;
// UnityEngine.Camera
struct Camera_t189460977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"

// System.Void HutongGames.PlayMaker.Actions.CutToCamera::.ctor()
extern "C"  void CutToCamera__ctor_m2521703176 (CutToCamera_t4198148310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::Reset()
extern "C"  void CutToCamera_Reset_m2942812299 (CutToCamera_t4198148310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::OnEnter()
extern "C"  void CutToCamera_OnEnter_m2607390075 (CutToCamera_t4198148310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::OnExit()
extern "C"  void CutToCamera_OnExit_m2613271819 (CutToCamera_t4198148310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CutToCamera::SwitchCamera(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void CutToCamera_SwitchCamera_m1598636345 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___camera10, Camera_t189460977 * ___camera21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
