﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation
struct RectTransformSetLocalRotation_t2073568479;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::.ctor()
extern "C"  void RectTransformSetLocalRotation__ctor_m2141130901 (RectTransformSetLocalRotation_t2073568479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::Reset()
extern "C"  void RectTransformSetLocalRotation_Reset_m2717523776 (RectTransformSetLocalRotation_t2073568479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::OnEnter()
extern "C"  void RectTransformSetLocalRotation_OnEnter_m2999864106 (RectTransformSetLocalRotation_t2073568479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::OnActionUpdate()
extern "C"  void RectTransformSetLocalRotation_OnActionUpdate_m2739324535 (RectTransformSetLocalRotation_t2073568479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetLocalRotation::DoSetValues()
extern "C"  void RectTransformSetLocalRotation_DoSetValues_m900957570 (RectTransformSetLocalRotation_t2073568479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
