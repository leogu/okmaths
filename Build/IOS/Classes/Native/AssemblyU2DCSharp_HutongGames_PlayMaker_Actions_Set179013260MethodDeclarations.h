﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetSkybox
struct SetSkybox_t179013260;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetSkybox::.ctor()
extern "C"  void SetSkybox__ctor_m758871440 (SetSkybox_t179013260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::Reset()
extern "C"  void SetSkybox_Reset_m4054382277 (SetSkybox_t179013260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::OnEnter()
extern "C"  void SetSkybox_OnEnter_m443188765 (SetSkybox_t179013260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetSkybox::OnUpdate()
extern "C"  void SetSkybox_OnUpdate_m35543542 (SetSkybox_t179013260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
