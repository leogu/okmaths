﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Basics
struct Basics_t2766259351;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Basics::.ctor()
extern "C"  void Basics__ctor_m1744303750 (Basics_t2766259351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::Start()
extern "C"  void Basics_Start_m2474433602 (Basics_t2766259351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Basics::<Start>m__E4()
extern "C"  Vector3_t2243707580  Basics_U3CStartU3Em__E4_m2145003164 (Basics_t2766259351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::<Start>m__E5(UnityEngine.Vector3)
extern "C"  void Basics_U3CStartU3Em__E5_m3138066958 (Basics_t2766259351 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
