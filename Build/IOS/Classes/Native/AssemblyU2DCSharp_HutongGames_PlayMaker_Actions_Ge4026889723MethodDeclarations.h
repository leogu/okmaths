﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorDelta
struct GetAnimatorDelta_t4026889723;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::.ctor()
extern "C"  void GetAnimatorDelta__ctor_m3897989475 (GetAnimatorDelta_t4026889723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::Reset()
extern "C"  void GetAnimatorDelta_Reset_m1051327652 (GetAnimatorDelta_t4026889723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnEnter()
extern "C"  void GetAnimatorDelta_OnEnter_m2071414410 (GetAnimatorDelta_t4026889723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::OnActionUpdate()
extern "C"  void GetAnimatorDelta_OnActionUpdate_m3656498081 (GetAnimatorDelta_t4026889723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorDelta::DoGetDeltaPosition()
extern "C"  void GetAnimatorDelta_DoGetDeltaPosition_m1479320623 (GetAnimatorDelta_t4026889723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
