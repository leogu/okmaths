﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalRotate
struct DOTweenTransformLocalRotate_t3848429816;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalRotate::.ctor()
extern "C"  void DOTweenTransformLocalRotate__ctor_m1846981118 (DOTweenTransformLocalRotate_t3848429816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalRotate::Reset()
extern "C"  void DOTweenTransformLocalRotate_Reset_m3604154841 (DOTweenTransformLocalRotate_t3848429816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalRotate::OnEnter()
extern "C"  void DOTweenTransformLocalRotate_OnEnter_m2773179121 (DOTweenTransformLocalRotate_t3848429816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalRotate::<OnEnter>m__BE()
extern "C"  void DOTweenTransformLocalRotate_U3COnEnterU3Em__BE_m258973777 (DOTweenTransformLocalRotate_t3848429816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalRotate::<OnEnter>m__BF()
extern "C"  void DOTweenTransformLocalRotate_U3COnEnterU3Em__BF_m258973810 (DOTweenTransformLocalRotate_t3848429816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
