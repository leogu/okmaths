﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetPivot
struct RectTransformGetPivot_t1187052588;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetPivot::.ctor()
extern "C"  void RectTransformGetPivot__ctor_m2919487432 (RectTransformGetPivot_t1187052588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetPivot::Reset()
extern "C"  void RectTransformGetPivot_Reset_m3638649453 (RectTransformGetPivot_t1187052588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetPivot::OnEnter()
extern "C"  void RectTransformGetPivot_OnEnter_m2071668333 (RectTransformGetPivot_t1187052588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetPivot::OnActionUpdate()
extern "C"  void RectTransformGetPivot_OnActionUpdate_m3917478356 (RectTransformGetPivot_t1187052588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetPivot::DoGetValues()
extern "C"  void RectTransformGetPivot_DoGetValues_m2111004041 (RectTransformGetPivot_t1187052588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
