﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindById
struct DOTweenControlMethodsSmoothRewindById_t1729880820;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindById::.ctor()
extern "C"  void DOTweenControlMethodsSmoothRewindById__ctor_m1112952566 (DOTweenControlMethodsSmoothRewindById_t1729880820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindById::Reset()
extern "C"  void DOTweenControlMethodsSmoothRewindById_Reset_m1701927497 (DOTweenControlMethodsSmoothRewindById_t1729880820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindById::OnEnter()
extern "C"  void DOTweenControlMethodsSmoothRewindById_OnEnter_m2029841561 (DOTweenControlMethodsSmoothRewindById_t1729880820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
