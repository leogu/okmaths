﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal
struct  uGuiInputFieldScreenToLocal_t2650285434  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::screen
	FsmVector2_t2430450063 * ___screen_12;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::local
	FsmVector2_t2430450063 * ___local_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::_inputField
	InputField_t1631627530 * ____inputField_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldScreenToLocal_t2650285434, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_screen_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldScreenToLocal_t2650285434, ___screen_12)); }
	inline FsmVector2_t2430450063 * get_screen_12() const { return ___screen_12; }
	inline FsmVector2_t2430450063 ** get_address_of_screen_12() { return &___screen_12; }
	inline void set_screen_12(FsmVector2_t2430450063 * value)
	{
		___screen_12 = value;
		Il2CppCodeGenWriteBarrier(&___screen_12, value);
	}

	inline static int32_t get_offset_of_local_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldScreenToLocal_t2650285434, ___local_13)); }
	inline FsmVector2_t2430450063 * get_local_13() const { return ___local_13; }
	inline FsmVector2_t2430450063 ** get_address_of_local_13() { return &___local_13; }
	inline void set_local_13(FsmVector2_t2430450063 * value)
	{
		___local_13 = value;
		Il2CppCodeGenWriteBarrier(&___local_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldScreenToLocal_t2650285434, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__inputField_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldScreenToLocal_t2650285434, ____inputField_15)); }
	inline InputField_t1631627530 * get__inputField_15() const { return ____inputField_15; }
	inline InputField_t1631627530 ** get_address_of__inputField_15() { return &____inputField_15; }
	inline void set__inputField_15(InputField_t1631627530 * value)
	{
		____inputField_15 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
