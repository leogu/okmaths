﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRe3397257107.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect1199013257.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTy905360158.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_Scrollbar3834843475.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRec3529018992.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition605142169.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection3187567897.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility4019374597.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis375128448.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3672778802.h"
#include "DOTween43_U3CModuleU3E3783534214.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43301896125.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334720.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334751.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334817.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334852.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334883.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334918.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334949.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec4023650049.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940008.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940074.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940109.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940140.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3299805082.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799701.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124703.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2735155078.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039116993.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480301.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3299805272.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117183.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480270.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (U3CClickRepeatU3Ec__Iterator5_t3397257107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[7] = 
{
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_U3CaxisCoordinateU3E__1_2(),
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_U24PC_3(),
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_U24current_4(),
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_U3CU24U3EeventData_5(),
	U3CClickRepeatU3Ec__Iterator5_t3397257107::get_offset_of_U3CU3Ef__this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (ScrollRect_t1199013257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[36] = 
{
	ScrollRect_t1199013257::get_offset_of_m_Content_2(),
	ScrollRect_t1199013257::get_offset_of_m_Horizontal_3(),
	ScrollRect_t1199013257::get_offset_of_m_Vertical_4(),
	ScrollRect_t1199013257::get_offset_of_m_MovementType_5(),
	ScrollRect_t1199013257::get_offset_of_m_Elasticity_6(),
	ScrollRect_t1199013257::get_offset_of_m_Inertia_7(),
	ScrollRect_t1199013257::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t1199013257::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t1199013257::get_offset_of_m_Viewport_10(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t1199013257::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t1199013257::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t1199013257::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t1199013257::get_offset_of_m_ViewRect_20(),
	ScrollRect_t1199013257::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t1199013257::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t1199013257::get_offset_of_m_Velocity_23(),
	ScrollRect_t1199013257::get_offset_of_m_Dragging_24(),
	ScrollRect_t1199013257::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t1199013257::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t1199013257::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t1199013257::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t1199013257::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t1199013257::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t1199013257::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t1199013257::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t1199013257::get_offset_of_m_Rect_33(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t1199013257::get_offset_of_m_Tracker_36(),
	ScrollRect_t1199013257::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (MovementType_t905360158)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[4] = 
{
	MovementType_t905360158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (ScrollbarVisibility_t3834843475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[4] = 
{
	ScrollbarVisibility_t3834843475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (ScrollRectEvent_t3529018992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (Selectable_t1490392188), -1, sizeof(Selectable_t1490392188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1905[14] = 
{
	Selectable_t1490392188_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1490392188::get_offset_of_m_Navigation_3(),
	Selectable_t1490392188::get_offset_of_m_Transition_4(),
	Selectable_t1490392188::get_offset_of_m_Colors_5(),
	Selectable_t1490392188::get_offset_of_m_SpriteState_6(),
	Selectable_t1490392188::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1490392188::get_offset_of_m_Interactable_8(),
	Selectable_t1490392188::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1490392188::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1490392188::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1490392188::get_offset_of_m_CanvasGroupCache_12(),
	Selectable_t1490392188::get_offset_of_U3CisPointerInsideU3Ek__BackingField_13(),
	Selectable_t1490392188::get_offset_of_U3CisPointerDownU3Ek__BackingField_14(),
	Selectable_t1490392188::get_offset_of_U3ChasSelectionU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (Transition_t605142169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[5] = 
{
	Transition_t605142169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (SelectionState_t3187567897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1907[5] = 
{
	SelectionState_t3187567897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (SetPropertyUtility_t4019374597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (Slider_t297367283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[15] = 
{
	Slider_t297367283::get_offset_of_m_FillRect_16(),
	Slider_t297367283::get_offset_of_m_HandleRect_17(),
	Slider_t297367283::get_offset_of_m_Direction_18(),
	Slider_t297367283::get_offset_of_m_MinValue_19(),
	Slider_t297367283::get_offset_of_m_MaxValue_20(),
	Slider_t297367283::get_offset_of_m_WholeNumbers_21(),
	Slider_t297367283::get_offset_of_m_Value_22(),
	Slider_t297367283::get_offset_of_m_OnValueChanged_23(),
	Slider_t297367283::get_offset_of_m_FillImage_24(),
	Slider_t297367283::get_offset_of_m_FillTransform_25(),
	Slider_t297367283::get_offset_of_m_FillContainerRect_26(),
	Slider_t297367283::get_offset_of_m_HandleTransform_27(),
	Slider_t297367283::get_offset_of_m_HandleContainerRect_28(),
	Slider_t297367283::get_offset_of_m_Offset_29(),
	Slider_t297367283::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Direction_t1525323322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[5] = 
{
	Direction_t1525323322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (Axis_t375128448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1912[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1914[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1916[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1918[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1920[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1921[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1927[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[14] = 
{
	0,
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_6(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_7(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_8(),
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1929[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1930[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1933[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1935[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1936[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1937[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[3] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1946[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1947[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1948[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1954[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[4] = 
{
	0,
	Shadow_t4269599528::get_offset_of_m_EffectColor_4(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_5(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1962[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (U24ArrayTypeU2412_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778807_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (ShortcutExtensions43_t301896125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (U3CU3Ec__DisplayClass2_0_t426334720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[1] = 
{
	U3CU3Ec__DisplayClass2_0_t426334720::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (U3CU3Ec__DisplayClass3_0_t426334751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[1] = 
{
	U3CU3Ec__DisplayClass3_0_t426334751::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (U3CU3Ec__DisplayClass5_0_t426334817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[1] = 
{
	U3CU3Ec__DisplayClass5_0_t426334817::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (U3CU3Ec__DisplayClass6_0_t426334852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[1] = 
{
	U3CU3Ec__DisplayClass6_0_t426334852::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (U3CU3Ec__DisplayClass7_0_t426334883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[1] = 
{
	U3CU3Ec__DisplayClass7_0_t426334883::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (U3CU3Ec__DisplayClass8_0_t426334918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[1] = 
{
	U3CU3Ec__DisplayClass8_0_t426334918::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (U3CU3Ec__DisplayClass9_0_t426334949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[6] = 
{
	U3CU3Ec__DisplayClass9_0_t426334949::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass9_0_t426334949::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass9_0_t426334949::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass9_0_t426334949::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass9_0_t426334949::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass9_0_t426334949::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (U3CU3Ec__DisplayClass10_0_t4023650049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[2] = 
{
	U3CU3Ec__DisplayClass10_0_t4023650049::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass10_0_t4023650049::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (DOTweenUtils46_t1550156519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (ShortcutExtensions46_t705180652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (U3CU3Ec__DisplayClass0_0_t3371939845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1977[1] = 
{
	U3CU3Ec__DisplayClass0_0_t3371939845::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (U3CU3Ec__DisplayClass3_0_t3371939942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3371939942::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (U3CU3Ec__DisplayClass4_0_t3371939977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1979[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3371939977::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (U3CU3Ec__DisplayClass5_0_t3371940008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[1] = 
{
	U3CU3Ec__DisplayClass5_0_t3371940008::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (U3CU3Ec__DisplayClass7_0_t3371940074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1981[1] = 
{
	U3CU3Ec__DisplayClass7_0_t3371940074::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (U3CU3Ec__DisplayClass8_0_t3371940109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[1] = 
{
	U3CU3Ec__DisplayClass8_0_t3371940109::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (U3CU3Ec__DisplayClass9_0_t3371940140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1983[1] = 
{
	U3CU3Ec__DisplayClass9_0_t3371940140::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (U3CU3Ec__DisplayClass10_0_t3299805082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[1] = 
{
	U3CU3Ec__DisplayClass10_0_t3299805082::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (U3CU3Ec__DisplayClass11_0_t308799701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[1] = 
{
	U3CU3Ec__DisplayClass11_0_t308799701::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (U3CU3Ec__DisplayClass13_0_t591124703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1986[1] = 
{
	U3CU3Ec__DisplayClass13_0_t591124703::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (U3CU3Ec__DisplayClass14_0_t2735155078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[1] = 
{
	U3CU3Ec__DisplayClass14_0_t2735155078::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (U3CU3Ec__DisplayClass15_0_t4039116993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1988[1] = 
{
	U3CU3Ec__DisplayClass15_0_t4039116993::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (U3CU3Ec__DisplayClass16_0_t3017480080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3017480080::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (U3CU3Ec__DisplayClass22_0_t3582130305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[1] = 
{
	U3CU3Ec__DisplayClass22_0_t3582130305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (U3CU3Ec__DisplayClass23_0_t591124924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[1] = 
{
	U3CU3Ec__DisplayClass23_0_t591124924::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (U3CU3Ec__DisplayClass25_0_t4039117214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[1] = 
{
	U3CU3Ec__DisplayClass25_0_t4039117214::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (U3CU3Ec__DisplayClass26_0_t3017480301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[6] = 
{
	U3CU3Ec__DisplayClass26_0_t3017480301::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass26_0_t3017480301::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass26_0_t3017480301::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass26_0_t3017480301::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass26_0_t3017480301::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass26_0_t3017480301::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (U3CU3Ec__DisplayClass30_0_t3299805272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[1] = 
{
	U3CU3Ec__DisplayClass30_0_t3299805272::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (U3CU3Ec__DisplayClass31_0_t308799891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[1] = 
{
	U3CU3Ec__DisplayClass31_0_t308799891::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (U3CU3Ec__DisplayClass32_0_t3582130274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[1] = 
{
	U3CU3Ec__DisplayClass32_0_t3582130274::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (U3CU3Ec__DisplayClass33_0_t591124893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1997[1] = 
{
	U3CU3Ec__DisplayClass33_0_t591124893::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (U3CU3Ec__DisplayClass35_0_t4039117183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[2] = 
{
	U3CU3Ec__DisplayClass35_0_t4039117183::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass35_0_t4039117183::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (U3CU3Ec__DisplayClass36_0_t3017480270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[2] = 
{
	U3CU3Ec__DisplayClass36_0_t3017480270::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass36_0_t3017480270::get_offset_of_target_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
