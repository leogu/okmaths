﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DeviceVibrate
struct DeviceVibrate_t2899533373;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::.ctor()
extern "C"  void DeviceVibrate__ctor_m272672805 (DeviceVibrate_t2899533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::Reset()
extern "C"  void DeviceVibrate_Reset_m126977914 (DeviceVibrate_t2899533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DeviceVibrate::OnEnter()
extern "C"  void DeviceVibrate_OnEnter_m1950247080 (DeviceVibrate_t2899533373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
