﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAngleToTarget
struct GetAngleToTarget_t2458755781;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::.ctor()
extern "C"  void GetAngleToTarget__ctor_m3699921615 (GetAngleToTarget_t2458755781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::Reset()
extern "C"  void GetAngleToTarget_Reset_m2628684494 (GetAngleToTarget_t2458755781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::OnLateUpdate()
extern "C"  void GetAngleToTarget_OnLateUpdate_m630020403 (GetAngleToTarget_t2458755781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAngleToTarget::DoGetAngleToTarget()
extern "C"  void GetAngleToTarget_DoGetAngleToTarget_m2847357441 (GetAngleToTarget_t2458755781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
