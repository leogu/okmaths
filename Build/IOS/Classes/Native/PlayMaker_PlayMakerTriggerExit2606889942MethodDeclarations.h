﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerTriggerExit
struct PlayMakerTriggerExit_t2606889942;
// UnityEngine.Collider
struct Collider_t3497673348;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void PlayMakerTriggerExit::OnTriggerExit(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerExit_OnTriggerExit_m732680115 (PlayMakerTriggerExit_t2606889942 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerTriggerExit::.ctor()
extern "C"  void PlayMakerTriggerExit__ctor_m3002147931 (PlayMakerTriggerExit_t2606889942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
