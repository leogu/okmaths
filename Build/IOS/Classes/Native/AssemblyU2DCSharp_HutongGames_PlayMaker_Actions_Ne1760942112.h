﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties
struct  NetworkGetNextConnectedPlayerProperties_t1760942112  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::loopEvent
	FsmEvent_t1258573736 * ___loopEvent_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::finishedEvent
	FsmEvent_t1258573736 * ___finishedEvent_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::index
	FsmInt_t1273009179 * ___index_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::IpAddress
	FsmString_t2414474701 * ___IpAddress_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::port
	FsmInt_t1273009179 * ___port_15;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::guid
	FsmString_t2414474701 * ___guid_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::externalIPAddress
	FsmString_t2414474701 * ___externalIPAddress_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::externalPort
	FsmInt_t1273009179 * ___externalPort_18;
	// System.Int32 HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::nextItemIndex
	int32_t ___nextItemIndex_19;

public:
	inline static int32_t get_offset_of_loopEvent_11() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___loopEvent_11)); }
	inline FsmEvent_t1258573736 * get_loopEvent_11() const { return ___loopEvent_11; }
	inline FsmEvent_t1258573736 ** get_address_of_loopEvent_11() { return &___loopEvent_11; }
	inline void set_loopEvent_11(FsmEvent_t1258573736 * value)
	{
		___loopEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_11, value);
	}

	inline static int32_t get_offset_of_finishedEvent_12() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___finishedEvent_12)); }
	inline FsmEvent_t1258573736 * get_finishedEvent_12() const { return ___finishedEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_finishedEvent_12() { return &___finishedEvent_12; }
	inline void set_finishedEvent_12(FsmEvent_t1258573736 * value)
	{
		___finishedEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_12, value);
	}

	inline static int32_t get_offset_of_index_13() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___index_13)); }
	inline FsmInt_t1273009179 * get_index_13() const { return ___index_13; }
	inline FsmInt_t1273009179 ** get_address_of_index_13() { return &___index_13; }
	inline void set_index_13(FsmInt_t1273009179 * value)
	{
		___index_13 = value;
		Il2CppCodeGenWriteBarrier(&___index_13, value);
	}

	inline static int32_t get_offset_of_IpAddress_14() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___IpAddress_14)); }
	inline FsmString_t2414474701 * get_IpAddress_14() const { return ___IpAddress_14; }
	inline FsmString_t2414474701 ** get_address_of_IpAddress_14() { return &___IpAddress_14; }
	inline void set_IpAddress_14(FsmString_t2414474701 * value)
	{
		___IpAddress_14 = value;
		Il2CppCodeGenWriteBarrier(&___IpAddress_14, value);
	}

	inline static int32_t get_offset_of_port_15() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___port_15)); }
	inline FsmInt_t1273009179 * get_port_15() const { return ___port_15; }
	inline FsmInt_t1273009179 ** get_address_of_port_15() { return &___port_15; }
	inline void set_port_15(FsmInt_t1273009179 * value)
	{
		___port_15 = value;
		Il2CppCodeGenWriteBarrier(&___port_15, value);
	}

	inline static int32_t get_offset_of_guid_16() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___guid_16)); }
	inline FsmString_t2414474701 * get_guid_16() const { return ___guid_16; }
	inline FsmString_t2414474701 ** get_address_of_guid_16() { return &___guid_16; }
	inline void set_guid_16(FsmString_t2414474701 * value)
	{
		___guid_16 = value;
		Il2CppCodeGenWriteBarrier(&___guid_16, value);
	}

	inline static int32_t get_offset_of_externalIPAddress_17() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___externalIPAddress_17)); }
	inline FsmString_t2414474701 * get_externalIPAddress_17() const { return ___externalIPAddress_17; }
	inline FsmString_t2414474701 ** get_address_of_externalIPAddress_17() { return &___externalIPAddress_17; }
	inline void set_externalIPAddress_17(FsmString_t2414474701 * value)
	{
		___externalIPAddress_17 = value;
		Il2CppCodeGenWriteBarrier(&___externalIPAddress_17, value);
	}

	inline static int32_t get_offset_of_externalPort_18() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___externalPort_18)); }
	inline FsmInt_t1273009179 * get_externalPort_18() const { return ___externalPort_18; }
	inline FsmInt_t1273009179 ** get_address_of_externalPort_18() { return &___externalPort_18; }
	inline void set_externalPort_18(FsmInt_t1273009179 * value)
	{
		___externalPort_18 = value;
		Il2CppCodeGenWriteBarrier(&___externalPort_18, value);
	}

	inline static int32_t get_offset_of_nextItemIndex_19() { return static_cast<int32_t>(offsetof(NetworkGetNextConnectedPlayerProperties_t1760942112, ___nextItemIndex_19)); }
	inline int32_t get_nextItemIndex_19() const { return ___nextItemIndex_19; }
	inline int32_t* get_address_of_nextItemIndex_19() { return &___nextItemIndex_19; }
	inline void set_nextItemIndex_19(int32_t value)
	{
		___nextItemIndex_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
