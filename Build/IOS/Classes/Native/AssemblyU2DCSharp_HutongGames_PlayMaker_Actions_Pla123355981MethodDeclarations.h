﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayRandomAnimation
struct PlayRandomAnimation_t123355981;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::.ctor()
extern "C"  void PlayRandomAnimation__ctor_m1203701903 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::Reset()
extern "C"  void PlayRandomAnimation_Reset_m2960888034 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::OnEnter()
extern "C"  void PlayRandomAnimation_OnEnter_m3020141604 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::DoPlayRandomAnimation()
extern "C"  void PlayRandomAnimation_DoPlayRandomAnimation_m1013861581 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::DoPlayAnimation(System.String)
extern "C"  void PlayRandomAnimation_DoPlayAnimation_m3103233088 (PlayRandomAnimation_t123355981 * __this, String_t* ___animName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::OnUpdate()
extern "C"  void PlayRandomAnimation_OnUpdate_m570485039 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::OnExit()
extern "C"  void PlayRandomAnimation_OnExit_m3554668836 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayRandomAnimation::StopAnimation()
extern "C"  void PlayRandomAnimation_StopAnimation_m1456347901 (PlayRandomAnimation_t123355981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
