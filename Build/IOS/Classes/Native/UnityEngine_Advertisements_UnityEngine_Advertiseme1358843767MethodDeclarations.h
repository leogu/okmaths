﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t1358843767;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t258469394;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern "C"  void ShowOptions__ctor_m3915763896 (ShowOptions_t1358843767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern "C"  Action_1_t258469394 * ShowOptions_get_resultCallback_m3731275459 (ShowOptions_t1358843767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C"  void ShowOptions_set_resultCallback_m3136348778 (ShowOptions_t1358843767 * __this, Action_1_t258469394 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern "C"  String_t* ShowOptions_get_gamerSid_m3683233454 (ShowOptions_t1358843767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern "C"  void ShowOptions_set_gamerSid_m3133060487 (ShowOptions_t1358843767 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
