﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetScreenResolutions
struct ArrayListGetScreenResolutions_t4177542830;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetScreenResolutions::.ctor()
extern "C"  void ArrayListGetScreenResolutions__ctor_m1751342388 (ArrayListGetScreenResolutions_t4177542830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetScreenResolutions::Reset()
extern "C"  void ArrayListGetScreenResolutions_Reset_m41272975 (ArrayListGetScreenResolutions_t4177542830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetScreenResolutions::OnEnter()
extern "C"  void ArrayListGetScreenResolutions_OnEnter_m3951329687 (ArrayListGetScreenResolutions_t4177542830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetScreenResolutions::getResolutions()
extern "C"  void ArrayListGetScreenResolutions_getResolutions_m604221305 (ArrayListGetScreenResolutions_t4177542830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
