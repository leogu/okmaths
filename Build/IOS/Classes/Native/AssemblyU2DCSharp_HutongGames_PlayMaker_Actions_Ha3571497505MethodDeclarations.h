﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableAdd
struct HashTableAdd_t3571497505;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableAdd::.ctor()
extern "C"  void HashTableAdd__ctor_m2291233767 (HashTableAdd_t3571497505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableAdd::Reset()
extern "C"  void HashTableAdd_Reset_m4011564262 (HashTableAdd_t3571497505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableAdd::OnEnter()
extern "C"  void HashTableAdd_OnEnter_m3453154376 (HashTableAdd_t3571497505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableAdd::AddToHashTable()
extern "C"  void HashTableAdd_AddToHashTable_m4076459245 (HashTableAdd_t3571497505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
