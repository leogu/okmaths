﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenSpriteRendererBlendableColor
struct DOTweenSpriteRendererBlendableColor_t1006197182;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererBlendableColor::.ctor()
extern "C"  void DOTweenSpriteRendererBlendableColor__ctor_m3607330686 (DOTweenSpriteRendererBlendableColor_t1006197182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererBlendableColor::Reset()
extern "C"  void DOTweenSpriteRendererBlendableColor_Reset_m1613633299 (DOTweenSpriteRendererBlendableColor_t1006197182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererBlendableColor::OnEnter()
extern "C"  void DOTweenSpriteRendererBlendableColor_OnEnter_m3775670539 (DOTweenSpriteRendererBlendableColor_t1006197182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererBlendableColor::<OnEnter>m__94()
extern "C"  void DOTweenSpriteRendererBlendableColor_U3COnEnterU3Em__94_m2557832565 (DOTweenSpriteRendererBlendableColor_t1006197182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererBlendableColor::<OnEnter>m__95()
extern "C"  void DOTweenSpriteRendererBlendableColor_U3COnEnterU3Em__95_m2557832660 (DOTweenSpriteRendererBlendableColor_t1006197182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
