﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize
struct uGuiScrollbarSetSize_t576963639;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize::.ctor()
extern "C"  void uGuiScrollbarSetSize__ctor_m3065420467 (uGuiScrollbarSetSize_t576963639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize::Reset()
extern "C"  void uGuiScrollbarSetSize_Reset_m1624735652 (uGuiScrollbarSetSize_t576963639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize::OnEnter()
extern "C"  void uGuiScrollbarSetSize_OnEnter_m196839874 (uGuiScrollbarSetSize_t576963639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize::OnUpdate()
extern "C"  void uGuiScrollbarSetSize_OnUpdate_m417366923 (uGuiScrollbarSetSize_t576963639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize::DoSetValue()
extern "C"  void uGuiScrollbarSetSize_DoSetValue_m1769352993 (uGuiScrollbarSetSize_t576963639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetSize::OnExit()
extern "C"  void uGuiScrollbarSetSize_OnExit_m3561721098 (uGuiScrollbarSetSize_t576963639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
