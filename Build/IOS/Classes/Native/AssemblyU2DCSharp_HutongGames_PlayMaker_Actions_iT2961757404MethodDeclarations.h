﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenLookUpdate
struct iTweenLookUpdate_t2961757404;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::.ctor()
extern "C"  void iTweenLookUpdate__ctor_m3215081776 (iTweenLookUpdate_t2961757404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::Reset()
extern "C"  void iTweenLookUpdate_Reset_m4008555401 (iTweenLookUpdate_t2961757404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnEnter()
extern "C"  void iTweenLookUpdate_OnEnter_m422238721 (iTweenLookUpdate_t2961757404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnExit()
extern "C"  void iTweenLookUpdate_OnExit_m1499576997 (iTweenLookUpdate_t2961757404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::OnUpdate()
extern "C"  void iTweenLookUpdate_OnUpdate_m3767490662 (iTweenLookUpdate_t2961757404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookUpdate::DoiTween()
extern "C"  void iTweenLookUpdate_DoiTween_m1049765943 (iTweenLookUpdate_t2961757404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
