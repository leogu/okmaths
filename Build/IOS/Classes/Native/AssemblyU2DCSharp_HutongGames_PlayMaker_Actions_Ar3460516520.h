﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetNext
struct  ArrayListGetNext_t3460516520  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetNext::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetNext::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListGetNext::reset
	FsmBool_t664485696 * ___reset_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetNext::startIndex
	FsmInt_t1273009179 * ___startIndex_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetNext::endIndex
	FsmInt_t1273009179 * ___endIndex_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListGetNext::loopEvent
	FsmEvent_t1258573736 * ___loopEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListGetNext::finishedEvent
	FsmEvent_t1258573736 * ___finishedEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListGetNext::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_19;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetNext::currentIndex
	FsmInt_t1273009179 * ___currentIndex_20;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayListGetNext::result
	FsmVar_t2872592513 * ___result_21;
	// System.Int32 HutongGames.PlayMaker.Actions.ArrayListGetNext::nextItemIndex
	int32_t ___nextItemIndex_22;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_reset_14() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___reset_14)); }
	inline FsmBool_t664485696 * get_reset_14() const { return ___reset_14; }
	inline FsmBool_t664485696 ** get_address_of_reset_14() { return &___reset_14; }
	inline void set_reset_14(FsmBool_t664485696 * value)
	{
		___reset_14 = value;
		Il2CppCodeGenWriteBarrier(&___reset_14, value);
	}

	inline static int32_t get_offset_of_startIndex_15() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___startIndex_15)); }
	inline FsmInt_t1273009179 * get_startIndex_15() const { return ___startIndex_15; }
	inline FsmInt_t1273009179 ** get_address_of_startIndex_15() { return &___startIndex_15; }
	inline void set_startIndex_15(FsmInt_t1273009179 * value)
	{
		___startIndex_15 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_15, value);
	}

	inline static int32_t get_offset_of_endIndex_16() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___endIndex_16)); }
	inline FsmInt_t1273009179 * get_endIndex_16() const { return ___endIndex_16; }
	inline FsmInt_t1273009179 ** get_address_of_endIndex_16() { return &___endIndex_16; }
	inline void set_endIndex_16(FsmInt_t1273009179 * value)
	{
		___endIndex_16 = value;
		Il2CppCodeGenWriteBarrier(&___endIndex_16, value);
	}

	inline static int32_t get_offset_of_loopEvent_17() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___loopEvent_17)); }
	inline FsmEvent_t1258573736 * get_loopEvent_17() const { return ___loopEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_loopEvent_17() { return &___loopEvent_17; }
	inline void set_loopEvent_17(FsmEvent_t1258573736 * value)
	{
		___loopEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_17, value);
	}

	inline static int32_t get_offset_of_finishedEvent_18() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___finishedEvent_18)); }
	inline FsmEvent_t1258573736 * get_finishedEvent_18() const { return ___finishedEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_finishedEvent_18() { return &___finishedEvent_18; }
	inline void set_finishedEvent_18(FsmEvent_t1258573736 * value)
	{
		___finishedEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_18, value);
	}

	inline static int32_t get_offset_of_failureEvent_19() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___failureEvent_19)); }
	inline FsmEvent_t1258573736 * get_failureEvent_19() const { return ___failureEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_19() { return &___failureEvent_19; }
	inline void set_failureEvent_19(FsmEvent_t1258573736 * value)
	{
		___failureEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_19, value);
	}

	inline static int32_t get_offset_of_currentIndex_20() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___currentIndex_20)); }
	inline FsmInt_t1273009179 * get_currentIndex_20() const { return ___currentIndex_20; }
	inline FsmInt_t1273009179 ** get_address_of_currentIndex_20() { return &___currentIndex_20; }
	inline void set_currentIndex_20(FsmInt_t1273009179 * value)
	{
		___currentIndex_20 = value;
		Il2CppCodeGenWriteBarrier(&___currentIndex_20, value);
	}

	inline static int32_t get_offset_of_result_21() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___result_21)); }
	inline FsmVar_t2872592513 * get_result_21() const { return ___result_21; }
	inline FsmVar_t2872592513 ** get_address_of_result_21() { return &___result_21; }
	inline void set_result_21(FsmVar_t2872592513 * value)
	{
		___result_21 = value;
		Il2CppCodeGenWriteBarrier(&___result_21, value);
	}

	inline static int32_t get_offset_of_nextItemIndex_22() { return static_cast<int32_t>(offsetof(ArrayListGetNext_t3460516520, ___nextItemIndex_22)); }
	inline int32_t get_nextItemIndex_22() const { return ___nextItemIndex_22; }
	inline int32_t* get_address_of_nextItemIndex_22() { return &___nextItemIndex_22; }
	inline void set_nextItemIndex_22(int32_t value)
	{
		___nextItemIndex_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
