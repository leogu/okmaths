﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t851691520;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C"  void Collider2D_set_isTrigger_m1243156937 (Collider2D_t646061738 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t502193897 * Collider2D_get_attachedRigidbody_m1321121400 (Collider2D_t646061738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Collider2D::get_shapeCount()
extern "C"  int32_t Collider2D_get_shapeCount_m95259526 (Collider2D_t646061738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
extern "C"  PhysicsMaterial2D_t851691520 * Collider2D_get_sharedMaterial_m1412882174 (Collider2D_t646061738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
