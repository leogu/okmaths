﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayAll
struct DOTweenControlMethodsPlayAll_t2261592914;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayAll::.ctor()
extern "C"  void DOTweenControlMethodsPlayAll__ctor_m4050309442 (DOTweenControlMethodsPlayAll_t2261592914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayAll::Reset()
extern "C"  void DOTweenControlMethodsPlayAll_Reset_m2978261915 (DOTweenControlMethodsPlayAll_t2261592914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayAll::OnEnter()
extern "C"  void DOTweenControlMethodsPlayAll_OnEnter_m4239065611 (DOTweenControlMethodsPlayAll_t2261592914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
