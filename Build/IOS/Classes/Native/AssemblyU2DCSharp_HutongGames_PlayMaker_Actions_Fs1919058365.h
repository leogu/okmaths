﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1112354236.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FsmStateActionAdvanced
struct  FsmStateActionAdvanced_t1919058365  : public FsmStateAction_t2862378169
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::everyFrame
	bool ___everyFrame_11;
	// HutongGames.PlayMaker.Actions.FsmStateActionAdvanced/FrameUpdateSelector HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::updateType
	int32_t ___updateType_12;

public:
	inline static int32_t get_offset_of_everyFrame_11() { return static_cast<int32_t>(offsetof(FsmStateActionAdvanced_t1919058365, ___everyFrame_11)); }
	inline bool get_everyFrame_11() const { return ___everyFrame_11; }
	inline bool* get_address_of_everyFrame_11() { return &___everyFrame_11; }
	inline void set_everyFrame_11(bool value)
	{
		___everyFrame_11 = value;
	}

	inline static int32_t get_offset_of_updateType_12() { return static_cast<int32_t>(offsetof(FsmStateActionAdvanced_t1919058365, ___updateType_12)); }
	inline int32_t get_updateType_12() const { return ___updateType_12; }
	inline int32_t* get_address_of_updateType_12() { return &___updateType_12; }
	inline void set_updateType_12(int32_t value)
	{
		___updateType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
