﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalMoveBy
struct DOTweenTransformBlendableLocalMoveBy_t3221235228;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalMoveBy::.ctor()
extern "C"  void DOTweenTransformBlendableLocalMoveBy__ctor_m39184506 (DOTweenTransformBlendableLocalMoveBy_t3221235228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalMoveBy::Reset()
extern "C"  void DOTweenTransformBlendableLocalMoveBy_Reset_m220710985 (DOTweenTransformBlendableLocalMoveBy_t3221235228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalMoveBy::OnEnter()
extern "C"  void DOTweenTransformBlendableLocalMoveBy_OnEnter_m895633065 (DOTweenTransformBlendableLocalMoveBy_t3221235228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalMoveBy::<OnEnter>m__A6()
extern "C"  void DOTweenTransformBlendableLocalMoveBy_U3COnEnterU3Em__A6_m1583782169 (DOTweenTransformBlendableLocalMoveBy_t3221235228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalMoveBy::<OnEnter>m__A7()
extern "C"  void DOTweenTransformBlendableLocalMoveBy_U3COnEnterU3Em__A7_m3129663134 (DOTweenTransformBlendableLocalMoveBy_t3221235228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
