﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.utils.Comment
struct  Comment_t4162749105  : public MonoBehaviour_t1158329972
{
public:
	// System.String HutongGames.PlayMaker.Ecosystem.utils.Comment::Text
	String_t* ___Text_2;

public:
	inline static int32_t get_offset_of_Text_2() { return static_cast<int32_t>(offsetof(Comment_t4162749105, ___Text_2)); }
	inline String_t* get_Text_2() const { return ___Text_2; }
	inline String_t** get_address_of_Text_2() { return &___Text_2; }
	inline void set_Text_2(String_t* value)
	{
		___Text_2 = value;
		Il2CppCodeGenWriteBarrier(&___Text_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
