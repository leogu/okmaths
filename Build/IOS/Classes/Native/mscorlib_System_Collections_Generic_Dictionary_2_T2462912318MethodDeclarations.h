﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>
struct Transform_1_t2462912318;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2941950544_gshared (Transform_1_t2462912318 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2941950544(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2462912318 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2941950544_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::Invoke(TKey,TValue)
extern "C"  Il2CppObject * Transform_1_Invoke_m180315616_gshared (Transform_1_t2462912318 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m180315616(__this, ___key0, ___value1, method) ((  Il2CppObject * (*) (Transform_1_t2462912318 *, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Transform_1_Invoke_m180315616_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2900276025_gshared (Transform_1_t2462912318 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2900276025(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2462912318 *, Il2CppObject *, RaycastHit2D_t4063908774 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2900276025_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,UnityEngine.RaycastHit2D,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Transform_1_EndInvoke_m2694061694_gshared (Transform_1_t2462912318 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2694061694(__this, ___result0, method) ((  Il2CppObject * (*) (Transform_1_t2462912318 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2694061694_gshared)(__this, ___result0, method)
