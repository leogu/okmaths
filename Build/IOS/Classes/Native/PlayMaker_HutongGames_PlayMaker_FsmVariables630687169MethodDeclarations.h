﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGlobals
struct PlayMakerGlobals_t2120229426;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// HutongGames.PlayMaker.NamedVariable[]
struct NamedVariableU5BU5D_t2156269820;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// System.Type
struct Type_t;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t4177556671;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3830815681;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t381696854;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t643261629;
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t1455296735;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t3489263757;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t4100123680;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t3601875862;
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t2711026008;
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t2440162814;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t2051300532;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1567627762;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t3720629962;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "mscorlib_System_Type1303803226.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"

// PlayMakerGlobals HutongGames.PlayMaker.FsmVariables::get_GlobalsComponent()
extern "C"  PlayMakerGlobals_t2120229426 * FsmVariables_get_GlobalsComponent_m3161560113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmVariables::get_GlobalVariables()
extern "C"  FsmVariables_t630687169 * FsmVariables_get_GlobalVariables_m583989627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVariables::get_GlobalVariablesSynced()
extern "C"  bool FsmVariables_get_GlobalVariablesSynced_m2538480389 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_GlobalVariablesSynced(System.Boolean)
extern "C"  void FsmVariables_set_GlobalVariablesSynced_m493809710 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] HutongGames.PlayMaker.FsmVariables::get_Categories()
extern "C"  StringU5BU5D_t1642385972* FsmVariables_get_Categories_m759441052 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_Categories(System.String[])
extern "C"  void FsmVariables_set_Categories_m1588039007 (FsmVariables_t630687169 * __this, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] HutongGames.PlayMaker.FsmVariables::get_CategoryIDs()
extern "C"  Int32U5BU5D_t3030399641* FsmVariables_get_CategoryIDs_m3550387943 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_CategoryIDs(System.Int32[])
extern "C"  void FsmVariables_set_CategoryIDs_m1516139338 (FsmVariables_t630687169 * __this, Int32U5BU5D_t3030399641* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.FsmVariables::GetAllNamedVariables()
extern "C"  NamedVariableU5BU5D_t2156269820* FsmVariables_GetAllNamedVariables_m3174356475 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.FsmVariables::GetNamedVariables(HutongGames.PlayMaker.VariableType)
extern "C"  NamedVariableU5BU5D_t2156269820* FsmVariables_GetNamedVariables_m1222016766 (FsmVariables_t630687169 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVariables::Contains(System.String)
extern "C"  bool FsmVariables_Contains_m3049442345 (FsmVariables_t630687169 * __this, String_t* ___variableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVariables::Contains(HutongGames.PlayMaker.NamedVariable)
extern "C"  bool FsmVariables_Contains_m1685443226 (FsmVariables_t630687169 * __this, NamedVariable_t3026441313 * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable[] HutongGames.PlayMaker.FsmVariables::GetNames(System.Type)
extern "C"  NamedVariableU5BU5D_t2156269820* FsmVariables_GetNames_m1428546007 (FsmVariables_t630687169 * __this, Type_t * ___ofType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor()
extern "C"  void FsmVariables__ctor_m4065964934 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables__ctor_m899263547 (FsmVariables_t630687169 * __this, FsmVariables_t630687169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::OverrideVariableValues(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables_OverrideVariableValues_m4861249 (FsmVariables_t630687169 * __this, FsmVariables_t630687169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::ApplyVariableValues(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables_ApplyVariableValues_m3209954337 (FsmVariables_t630687169 * __this, FsmVariables_t630687169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::ApplyVariableValuesCareful(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables_ApplyVariableValuesCareful_m1567920955 (FsmVariables_t630687169 * __this, FsmVariables_t630687169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::get_FloatVariables()
extern "C"  FsmFloatU5BU5D_t4177556671* FsmVariables_get_FloatVariables_m3630743291 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_FloatVariables(HutongGames.PlayMaker.FsmFloat[])
extern "C"  void FsmVariables_set_FloatVariables_m2438069600 (FsmVariables_t630687169 * __this, FsmFloatU5BU5D_t4177556671* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::get_IntVariables()
extern "C"  FsmIntU5BU5D_t2637547802* FsmVariables_get_IntVariables_m2179857115 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_IntVariables(HutongGames.PlayMaker.FsmInt[])
extern "C"  void FsmVariables_set_IntVariables_m3686705184 (FsmVariables_t630687169 * __this, FsmIntU5BU5D_t2637547802* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::get_BoolVariables()
extern "C"  FsmBoolU5BU5D_t3830815681* FsmVariables_get_BoolVariables_m1639951187 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_BoolVariables(HutongGames.PlayMaker.FsmBool[])
extern "C"  void FsmVariables_set_BoolVariables_m776154598 (FsmVariables_t630687169 * __this, FsmBoolU5BU5D_t3830815681* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::get_StringVariables()
extern "C"  FsmStringU5BU5D_t2699231328* FsmVariables_get_StringVariables_m3984963863 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_StringVariables(HutongGames.PlayMaker.FsmString[])
extern "C"  void FsmVariables_set_StringVariables_m41677636 (FsmVariables_t630687169 * __this, FsmStringU5BU5D_t2699231328* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::get_Vector2Variables()
extern "C"  FsmVector2U5BU5D_t381696854* FsmVariables_get_Vector2Variables_m3290377243 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_Vector2Variables(HutongGames.PlayMaker.FsmVector2[])
extern "C"  void FsmVariables_set_Vector2Variables_m3993223968 (FsmVariables_t630687169 * __this, FsmVector2U5BU5D_t381696854* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::get_Vector3Variables()
extern "C"  FsmVector3U5BU5D_t643261629* FsmVariables_get_Vector3Variables_m1350627003 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_Vector3Variables(HutongGames.PlayMaker.FsmVector3[])
extern "C"  void FsmVariables_set_Vector3Variables_m2768117376 (FsmVariables_t630687169 * __this, FsmVector3U5BU5D_t643261629* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect[] HutongGames.PlayMaker.FsmVariables::get_RectVariables()
extern "C"  FsmRectU5BU5D_t1455296735* FsmVariables_get_RectVariables_m536779595 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_RectVariables(HutongGames.PlayMaker.FsmRect[])
extern "C"  void FsmVariables_set_RectVariables_m3312250810 (FsmVariables_t630687169 * __this, FsmRectU5BU5D_t1455296735* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::get_QuaternionVariables()
extern "C"  FsmQuaternionU5BU5D_t3489263757* FsmVariables_get_QuaternionVariables_m3575404011 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_QuaternionVariables(HutongGames.PlayMaker.FsmQuaternion[])
extern "C"  void FsmVariables_set_QuaternionVariables_m4102714490 (FsmVariables_t630687169 * __this, FsmQuaternionU5BU5D_t3489263757* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::get_ColorVariables()
extern "C"  FsmColorU5BU5D_t4100123680* FsmVariables_get_ColorVariables_m3164034491 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_ColorVariables(HutongGames.PlayMaker.FsmColor[])
extern "C"  void FsmVariables_set_ColorVariables_m3690923360 (FsmVariables_t630687169 * __this, FsmColorU5BU5D_t4100123680* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.FsmVariables::get_GameObjectVariables()
extern "C"  FsmGameObjectU5BU5D_t3601875862* FsmVariables_get_GameObjectVariables_m1899560415 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_GameObjectVariables(HutongGames.PlayMaker.FsmGameObject[])
extern "C"  void FsmVariables_set_GameObjectVariables_m2965260144 (FsmVariables_t630687169 * __this, FsmGameObjectU5BU5D_t3601875862* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray[] HutongGames.PlayMaker.FsmVariables::get_ArrayVariables()
extern "C"  FsmArrayU5BU5D_t2711026008* FsmVariables_get_ArrayVariables_m1760922875 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_ArrayVariables(HutongGames.PlayMaker.FsmArray[])
extern "C"  void FsmVariables_set_ArrayVariables_m530350496 (FsmVariables_t630687169 * __this, FsmArrayU5BU5D_t2711026008* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum[] HutongGames.PlayMaker.FsmVariables::get_EnumVariables()
extern "C"  FsmEnumU5BU5D_t2440162814* FsmVariables_get_EnumVariables_m993801311 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_EnumVariables(HutongGames.PlayMaker.FsmEnum[])
extern "C"  void FsmVariables_set_EnumVariables_m3112608992 (FsmVariables_t630687169 * __this, FsmEnumU5BU5D_t2440162814* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.FsmVariables::get_ObjectVariables()
extern "C"  FsmObjectU5BU5D_t2051300532* FsmVariables_get_ObjectVariables_m3470799223 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_ObjectVariables(HutongGames.PlayMaker.FsmObject[])
extern "C"  void FsmVariables_set_ObjectVariables_m2589837284 (FsmVariables_t630687169 * __this, FsmObjectU5BU5D_t2051300532* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.FsmVariables::get_MaterialVariables()
extern "C"  FsmMaterialU5BU5D_t1567627762* FsmVariables_get_MaterialVariables_m468505119 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_MaterialVariables(HutongGames.PlayMaker.FsmMaterial[])
extern "C"  void FsmVariables_set_MaterialVariables_m1994018000 (FsmVariables_t630687169 * __this, FsmMaterialU5BU5D_t1567627762* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.FsmVariables::get_TextureVariables()
extern "C"  FsmTextureU5BU5D_t3720629962* FsmVariables_get_TextureVariables_m3940249115 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_TextureVariables(HutongGames.PlayMaker.FsmTexture[])
extern "C"  void FsmVariables_set_TextureVariables_m1379545888 (FsmVariables_t630687169 * __this, FsmTextureU5BU5D_t3720629962* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVariables::GetVariable(System.String)
extern "C"  NamedVariable_t3026441313 * FsmVariables_GetVariable_m792017948 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmVariables::GetFsmFloat(System.String)
extern "C"  FsmFloat_t937133978 * FsmVariables_GetFsmFloat_m1215451093 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmVariables::GetFsmObject(System.String)
extern "C"  FsmObject_t2785794313 * FsmVariables_GetFsmObject_m703724597 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.FsmVariables::GetFsmMaterial(System.String)
extern "C"  FsmMaterial_t1421632035 * FsmVariables_GetFsmMaterial_m1839896713 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.FsmVariables::GetFsmTexture(System.String)
extern "C"  FsmTexture_t3372293163 * FsmVariables_GetFsmTexture_m4035782613 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmVariables::GetFsmInt(System.String)
extern "C"  FsmInt_t1273009179 * FsmVariables_GetFsmInt_m4075723285 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmVariables::GetFsmBool(System.String)
extern "C"  FsmBool_t664485696 * FsmVariables_GetFsmBool_m2288958261 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmVariables::GetFsmString(System.String)
extern "C"  FsmString_t2414474701 * FsmVariables_GetFsmString_m619955285 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmVariables::GetFsmVector2(System.String)
extern "C"  FsmVector2_t2430450063 * FsmVariables_GetFsmVector2_m1741883477 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmVariables::GetFsmVector3(System.String)
extern "C"  FsmVector3_t3996534004 * FsmVariables_GetFsmVector3_m1981542197 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmVariables::GetFsmRect(System.String)
extern "C"  FsmRect_t19023354 * FsmVariables_GetFsmRect_m3728049889 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmVariables::GetFsmQuaternion(System.String)
extern "C"  FsmQuaternion_t878438756 * FsmVariables_GetFsmQuaternion_m1196207457 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmVariables::GetFsmColor(System.String)
extern "C"  FsmColor_t118301965 * FsmVariables_GetFsmColor_m198518549 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmVariables::GetFsmGameObject(System.String)
extern "C"  FsmGameObject_t3097142863 * FsmVariables_GetFsmGameObject_m3373890729 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.FsmVariables::GetFsmArray(System.String)
extern "C"  FsmArray_t527459893 * FsmVariables_GetFsmArray_m4229145173 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.FsmVariables::GetFsmEnum(System.String)
extern "C"  FsmEnum_t2808516103 * FsmVariables_GetFsmEnum_m221487545 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::LogMissingVariable(System.String)
extern "C"  void FsmVariables_LogMissingVariable_m2821689436 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVariables::FindVariable(System.String)
extern "C"  NamedVariable_t3026441313 * FsmVariables_FindVariable_m888037189 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVariables::FindVariable(HutongGames.PlayMaker.VariableType,System.String)
extern "C"  NamedVariable_t3026441313 * FsmVariables_FindVariable_m1190425545 (FsmVariables_t630687169 * __this, int32_t ___type0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmVariables::FindFsmFloat(System.String)
extern "C"  FsmFloat_t937133978 * FsmVariables_FindFsmFloat_m3554272580 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.FsmVariables::FindFsmObject(System.String)
extern "C"  FsmObject_t2785794313 * FsmVariables_FindFsmObject_m1576212012 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.FsmVariables::FindFsmMaterial(System.String)
extern "C"  FsmMaterial_t1421632035 * FsmVariables_FindFsmMaterial_m1577713996 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.FsmVariables::FindFsmTexture(System.String)
extern "C"  FsmTexture_t3372293163 * FsmVariables_FindFsmTexture_m2285545046 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmVariables::FindFsmInt(System.String)
extern "C"  FsmInt_t1273009179 * FsmVariables_FindFsmInt_m2727677110 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmVariables::FindFsmBool(System.String)
extern "C"  FsmBool_t664485696 * FsmVariables_FindFsmBool_m2600024012 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.FsmVariables::FindFsmString(System.String)
extern "C"  FsmString_t2414474701 * FsmVariables_FindFsmString_m1937500908 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmVariables::FindFsmVector2(System.String)
extern "C"  FsmVector2_t2430450063 * FsmVariables_FindFsmVector2_m3194631334 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmVariables::FindFsmVector3(System.String)
extern "C"  FsmVector3_t3996534004 * FsmVariables_FindFsmVector3_m663739468 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmVariables::FindFsmRect(System.String)
extern "C"  FsmRect_t19023354 * FsmVariables_FindFsmRect_m2425596108 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmVariables::FindFsmQuaternion(System.String)
extern "C"  FsmQuaternion_t878438756 * FsmVariables_FindFsmQuaternion_m1212649036 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmVariables::FindFsmColor(System.String)
extern "C"  FsmColor_t118301965 * FsmVariables_FindFsmColor_m3563511390 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmVariables::FindFsmGameObject(System.String)
extern "C"  FsmGameObject_t3097142863 * FsmVariables_FindFsmGameObject_m1797685292 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.FsmVariables::FindFsmEnum(System.String)
extern "C"  FsmEnum_t2808516103 * FsmVariables_FindFsmEnum_m3583034188 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.FsmVariables::FindFsmArray(System.String)
extern "C"  FsmArray_t527459893 * FsmVariables_FindFsmArray_m568163214 (FsmVariables_t630687169 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
