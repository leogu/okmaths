﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture
struct uGuiRawImageSetTexture_t1546979978;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::.ctor()
extern "C"  void uGuiRawImageSetTexture__ctor_m980590250 (uGuiRawImageSetTexture_t1546979978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::Reset()
extern "C"  void uGuiRawImageSetTexture_Reset_m4204017459 (uGuiRawImageSetTexture_t1546979978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::OnEnter()
extern "C"  void uGuiRawImageSetTexture_OnEnter_m1721499251 (uGuiRawImageSetTexture_t1546979978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::DoSetValue()
extern "C"  void uGuiRawImageSetTexture_DoSetValue_m587620756 (uGuiRawImageSetTexture_t1546979978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRawImageSetTexture::OnExit()
extern "C"  void uGuiRawImageSetTexture_OnExit_m2045095667 (uGuiRawImageSetTexture_t1546979978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
