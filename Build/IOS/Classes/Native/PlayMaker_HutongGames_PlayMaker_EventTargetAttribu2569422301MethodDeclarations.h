﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.EventTargetAttribute
struct EventTargetAttribute_t2569422301;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3694600655.h"

// HutongGames.PlayMaker.FsmEventTarget/EventTarget HutongGames.PlayMaker.EventTargetAttribute::get_Target()
extern "C"  int32_t EventTargetAttribute_get_Target_m2988398414 (EventTargetAttribute_t2569422301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.EventTargetAttribute::.ctor(HutongGames.PlayMaker.FsmEventTarget/EventTarget)
extern "C"  void EventTargetAttribute__ctor_m2482427037 (EventTargetAttribute_t2569422301 * __this, int32_t ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
