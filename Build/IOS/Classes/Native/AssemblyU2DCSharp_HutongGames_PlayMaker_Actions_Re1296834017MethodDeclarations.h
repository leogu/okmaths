﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax
struct RectTransformGetOffsetMax_t1296834017;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::.ctor()
extern "C"  void RectTransformGetOffsetMax__ctor_m943269761 (RectTransformGetOffsetMax_t1296834017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::Reset()
extern "C"  void RectTransformGetOffsetMax_Reset_m2486817950 (RectTransformGetOffsetMax_t1296834017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::OnEnter()
extern "C"  void RectTransformGetOffsetMax_OnEnter_m1343739644 (RectTransformGetOffsetMax_t1296834017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::OnActionUpdate()
extern "C"  void RectTransformGetOffsetMax_OnActionUpdate_m1251733551 (RectTransformGetOffsetMax_t1296834017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMax::DoGetValues()
extern "C"  void RectTransformGetOffsetMax_DoGetValues_m2594778928 (RectTransformGetOffsetMax_t1296834017 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
