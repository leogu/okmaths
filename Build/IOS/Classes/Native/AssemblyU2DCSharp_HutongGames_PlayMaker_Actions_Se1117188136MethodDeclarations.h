﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmArrayItem
struct SetFsmArrayItem_t1117188136;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::.ctor()
extern "C"  void SetFsmArrayItem__ctor_m418602646 (SetFsmArrayItem_t1117188136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::Reset()
extern "C"  void SetFsmArrayItem_Reset_m314938337 (SetFsmArrayItem_t1117188136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::OnEnter()
extern "C"  void SetFsmArrayItem_OnEnter_m1426587745 (SetFsmArrayItem_t1117188136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::DoSetFsmArray()
extern "C"  void SetFsmArrayItem_DoSetFsmArray_m3822892670 (SetFsmArrayItem_t1117188136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmArrayItem::OnUpdate()
extern "C"  void SetFsmArrayItem_OnUpdate_m3094329136 (SetFsmArrayItem_t1117188136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
