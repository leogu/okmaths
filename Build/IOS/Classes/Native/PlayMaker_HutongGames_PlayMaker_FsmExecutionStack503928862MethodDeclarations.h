﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"

// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingFsm()
extern "C"  Fsm_t917886356 * FsmExecutionStack_get_ExecutingFsm_m3131172619 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingState()
extern "C"  FsmState_t1643911659 * FsmExecutionStack_get_ExecutingState_m1300602151 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingStateName()
extern "C"  String_t* FsmExecutionStack_get_ExecutingStateName_m547985155 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.FsmExecutionStack::get_ExecutingAction()
extern "C"  FsmStateAction_t2862378169 * FsmExecutionStack_get_ExecutingAction_m1743200508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmExecutionStack::get_StackCount()
extern "C"  int32_t FsmExecutionStack_get_StackCount_m106067031 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmExecutionStack::get_MaxStackCount()
extern "C"  int32_t FsmExecutionStack_get_MaxStackCount_m2846102551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::set_MaxStackCount(System.Int32)
extern "C"  void FsmExecutionStack_set_MaxStackCount_m1944052764 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PushFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmExecutionStack_PushFsm_m1873564309 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___executingFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PopFsm()
extern "C"  void FsmExecutionStack_PopFsm_m3490719728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmExecutionStack::GetDebugString()
extern "C"  String_t* FsmExecutionStack_GetDebugString_m4265536710 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::.cctor()
extern "C"  void FsmExecutionStack__cctor_m868302778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
