﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget
struct PlayMakerFsmTarget_t1869733351;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_U378386173.h"
#include "PlayMaker_PlayMakerFSM437737208.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::.ctor()
extern "C"  void PlayMakerFsmTarget__ctor_m3186032627 (PlayMakerFsmTarget_t1869733351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::.ctor(HutongGames.PlayMaker.Ecosystem.Utils.ProxyFsmTarget)
extern "C"  void PlayMakerFsmTarget__ctor_m3615685495 (PlayMakerFsmTarget_t1869733351 * __this, int32_t ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::get_fsmComponent()
extern "C"  PlayMakerFSM_t437737208 * PlayMakerFsmTarget_get_fsmComponent_m2966784228 (PlayMakerFsmTarget_t1869733351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::set_fsmComponent(PlayMakerFSM)
extern "C"  void PlayMakerFsmTarget_set_fsmComponent_m3212097193 (PlayMakerFsmTarget_t1869733351 * __this, PlayMakerFSM_t437737208 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::Initialize()
extern "C"  void PlayMakerFsmTarget_Initialize_m175222895 (PlayMakerFsmTarget_t1869733351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
