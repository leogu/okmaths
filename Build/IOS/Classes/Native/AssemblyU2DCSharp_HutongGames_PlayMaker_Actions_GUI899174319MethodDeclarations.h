﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutAction
struct GUILayoutAction_t899174319;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2108882777;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutAction::.ctor()
extern "C"  void GUILayoutAction__ctor_m1416712753 (GUILayoutAction_t899174319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption[] HutongGames.PlayMaker.Actions.GUILayoutAction::get_LayoutOptions()
extern "C"  GUILayoutOptionU5BU5D_t2108882777* GUILayoutAction_get_LayoutOptions_m2523172238 (GUILayoutAction_t899174319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutAction::Reset()
extern "C"  void GUILayoutAction_Reset_m4001606412 (GUILayoutAction_t899174319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
