﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionAngleAxis
struct QuaternionAngleAxis_t50996912;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::.ctor()
extern "C"  void QuaternionAngleAxis__ctor_m726042104 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::Reset()
extern "C"  void QuaternionAngleAxis_Reset_m3819878213 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnEnter()
extern "C"  void QuaternionAngleAxis_OnEnter_m4086079085 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnUpdate()
extern "C"  void QuaternionAngleAxis_OnUpdate_m2341981238 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnLateUpdate()
extern "C"  void QuaternionAngleAxis_OnLateUpdate_m2766922514 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::OnFixedUpdate()
extern "C"  void QuaternionAngleAxis_OnFixedUpdate_m3492695632 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionAngleAxis::DoQuatAngleAxis()
extern "C"  void QuaternionAngleAxis_DoQuatAngleAxis_m2735952860 (QuaternionAngleAxis_t50996912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
