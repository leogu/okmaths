﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveX
struct DOTweenRigidbody2DMoveX_t3580673314;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveX::.ctor()
extern "C"  void DOTweenRigidbody2DMoveX__ctor_m1561604538 (DOTweenRigidbody2DMoveX_t3580673314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveX::Reset()
extern "C"  void DOTweenRigidbody2DMoveX_Reset_m3827716983 (DOTweenRigidbody2DMoveX_t3580673314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveX::OnEnter()
extern "C"  void DOTweenRigidbody2DMoveX_OnEnter_m2959505967 (DOTweenRigidbody2DMoveX_t3580673314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveX::<OnEnter>m__7C()
extern "C"  void DOTweenRigidbody2DMoveX_U3COnEnterU3Em__7C_m947097804 (DOTweenRigidbody2DMoveX_t3580673314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbody2DMoveX::<OnEnter>m__7D()
extern "C"  void DOTweenRigidbody2DMoveX_U3COnEnterU3Em__7D_m947097643 (DOTweenRigidbody2DMoveX_t3580673314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
