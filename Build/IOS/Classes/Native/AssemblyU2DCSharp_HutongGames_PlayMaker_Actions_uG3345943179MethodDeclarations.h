﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetSelectionColor
struct uGuiInputFieldGetSelectionColor_t3345943179;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetSelectionColor::.ctor()
extern "C"  void uGuiInputFieldGetSelectionColor__ctor_m891051731 (uGuiInputFieldGetSelectionColor_t3345943179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetSelectionColor::Reset()
extern "C"  void uGuiInputFieldGetSelectionColor_Reset_m734130752 (uGuiInputFieldGetSelectionColor_t3345943179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetSelectionColor::OnEnter()
extern "C"  void uGuiInputFieldGetSelectionColor_OnEnter_m169125798 (uGuiInputFieldGetSelectionColor_t3345943179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetSelectionColor::OnUpdate()
extern "C"  void uGuiInputFieldGetSelectionColor_OnUpdate_m3796390459 (uGuiInputFieldGetSelectionColor_t3345943179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetSelectionColor::DoGetValue()
extern "C"  void uGuiInputFieldGetSelectionColor_DoGetValue_m2098147401 (uGuiInputFieldGetSelectionColor_t3345943179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
