﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.FsmVariableType
struct FsmVariableType_t767666332;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.FsmVariableType::.ctor(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmVariableType__ctor_m265739510 (FsmVariableType_t767666332 * __this, int32_t ___variableType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
