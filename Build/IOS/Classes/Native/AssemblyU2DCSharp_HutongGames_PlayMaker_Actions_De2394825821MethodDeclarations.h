﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyObject
struct DestroyObject_t2394825821;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyObject::.ctor()
extern "C"  void DestroyObject__ctor_m2661792193 (DestroyObject_t2394825821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::Reset()
extern "C"  void DestroyObject_Reset_m1482977718 (DestroyObject_t2394825821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::OnEnter()
extern "C"  void DestroyObject_OnEnter_m1719925372 (DestroyObject_t2394825821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObject::OnUpdate()
extern "C"  void DestroyObject_OnUpdate_m3561984325 (DestroyObject_t2394825821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
