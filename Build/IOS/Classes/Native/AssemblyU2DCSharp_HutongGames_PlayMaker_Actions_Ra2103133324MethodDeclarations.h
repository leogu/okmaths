﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RaycastAll
struct RaycastAll_t2103133324;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RaycastAll::.ctor()
extern "C"  void RaycastAll__ctor_m3545728606 (RaycastAll_t2103133324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::Reset()
extern "C"  void RaycastAll_Reset_m2711891741 (RaycastAll_t2103133324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::OnEnter()
extern "C"  void RaycastAll_OnEnter_m2128572445 (RaycastAll_t2103133324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::OnUpdate()
extern "C"  void RaycastAll_OnUpdate_m1422396080 (RaycastAll_t2103133324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RaycastAll::DoRaycast()
extern "C"  void RaycastAll_DoRaycast_m2883002290 (RaycastAll_t2103133324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
