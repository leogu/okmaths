﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TouchGUIEvent
struct TouchGUIEvent_t3988768176;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::.ctor()
extern "C"  void TouchGUIEvent__ctor_m339890768 (TouchGUIEvent_t3988768176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::Reset()
extern "C"  void TouchGUIEvent_Reset_m2604229261 (TouchGUIEvent_t3988768176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::OnEnter()
extern "C"  void TouchGUIEvent_OnEnter_m1390389101 (TouchGUIEvent_t3988768176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::OnUpdate()
extern "C"  void TouchGUIEvent_OnUpdate_m3700913014 (TouchGUIEvent_t3988768176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::DoTouchGUIEvent()
extern "C"  void TouchGUIEvent_DoTouchGUIEvent_m3442223677 (TouchGUIEvent_t3988768176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::DoTouch(UnityEngine.Touch)
extern "C"  void TouchGUIEvent_DoTouch_m122922796 (TouchGUIEvent_t3988768176 * __this, Touch_t407273883  ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchGUIEvent::DoTouchOffset(UnityEngine.Vector3)
extern "C"  void TouchGUIEvent_DoTouchOffset_m654612272 (TouchGUIEvent_t3988768176 * __this, Vector3_t2243707580  ___touchPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
