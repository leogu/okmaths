﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled
struct  uGuiInputFieldGetWasCanceled_t3459342752  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::wasCanceled
	FsmBool_t664485696 * ___wasCanceled_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::wasCanceledEvent
	FsmEvent_t1258573736 * ___wasCanceledEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::wasNotCanceledEvent
	FsmEvent_t1258573736 * ___wasNotCanceledEvent_14;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::_inputField
	InputField_t1631627530 * ____inputField_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetWasCanceled_t3459342752, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_wasCanceled_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetWasCanceled_t3459342752, ___wasCanceled_12)); }
	inline FsmBool_t664485696 * get_wasCanceled_12() const { return ___wasCanceled_12; }
	inline FsmBool_t664485696 ** get_address_of_wasCanceled_12() { return &___wasCanceled_12; }
	inline void set_wasCanceled_12(FsmBool_t664485696 * value)
	{
		___wasCanceled_12 = value;
		Il2CppCodeGenWriteBarrier(&___wasCanceled_12, value);
	}

	inline static int32_t get_offset_of_wasCanceledEvent_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetWasCanceled_t3459342752, ___wasCanceledEvent_13)); }
	inline FsmEvent_t1258573736 * get_wasCanceledEvent_13() const { return ___wasCanceledEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_wasCanceledEvent_13() { return &___wasCanceledEvent_13; }
	inline void set_wasCanceledEvent_13(FsmEvent_t1258573736 * value)
	{
		___wasCanceledEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___wasCanceledEvent_13, value);
	}

	inline static int32_t get_offset_of_wasNotCanceledEvent_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetWasCanceled_t3459342752, ___wasNotCanceledEvent_14)); }
	inline FsmEvent_t1258573736 * get_wasNotCanceledEvent_14() const { return ___wasNotCanceledEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_wasNotCanceledEvent_14() { return &___wasNotCanceledEvent_14; }
	inline void set_wasNotCanceledEvent_14(FsmEvent_t1258573736 * value)
	{
		___wasNotCanceledEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___wasNotCanceledEvent_14, value);
	}

	inline static int32_t get_offset_of__inputField_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetWasCanceled_t3459342752, ____inputField_15)); }
	inline InputField_t1631627530 * get__inputField_15() const { return ____inputField_15; }
	inline InputField_t1631627530 ** get_address_of__inputField_15() { return &____inputField_15; }
	inline void set__inputField_15(InputField_t1631627530 * value)
	{
		____inputField_15 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
