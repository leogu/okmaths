﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmGameObject
struct GetFsmGameObject_t735093401;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::.ctor()
extern "C"  void GetFsmGameObject__ctor_m56946255 (GetFsmGameObject_t735093401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::Reset()
extern "C"  void GetFsmGameObject_Reset_m4164419806 (GetFsmGameObject_t735093401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::OnEnter()
extern "C"  void GetFsmGameObject_OnEnter_m265027968 (GetFsmGameObject_t735093401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::OnUpdate()
extern "C"  void GetFsmGameObject_OnUpdate_m1533769287 (GetFsmGameObject_t735093401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmGameObject::DoGetFsmGameObject()
extern "C"  void GetFsmGameObject_DoGetFsmGameObject_m1494967553 (GetFsmGameObject_t735093401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
