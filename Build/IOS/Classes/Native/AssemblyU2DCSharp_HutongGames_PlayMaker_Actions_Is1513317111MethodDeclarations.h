﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsSleeping
struct IsSleeping_t1513317111;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsSleeping::.ctor()
extern "C"  void IsSleeping__ctor_m1382417651 (IsSleeping_t1513317111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::Reset()
extern "C"  void IsSleeping_Reset_m4230738884 (IsSleeping_t1513317111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::OnEnter()
extern "C"  void IsSleeping_OnEnter_m2272649730 (IsSleeping_t1513317111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::OnUpdate()
extern "C"  void IsSleeping_OnUpdate_m2985131627 (IsSleeping_t1513317111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsSleeping::DoIsSleeping()
extern "C"  void IsSleeping_DoIsSleeping_m1763261825 (IsSleeping_t1513317111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
