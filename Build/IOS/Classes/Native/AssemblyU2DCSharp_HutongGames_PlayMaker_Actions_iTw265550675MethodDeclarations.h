﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateFrom
struct iTweenRotateFrom_t265550675;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::.ctor()
extern "C"  void iTweenRotateFrom__ctor_m1356410785 (iTweenRotateFrom_t265550675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::Reset()
extern "C"  void iTweenRotateFrom_Reset_m285726744 (iTweenRotateFrom_t265550675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::OnEnter()
extern "C"  void iTweenRotateFrom_OnEnter_m2693011402 (iTweenRotateFrom_t265550675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::OnExit()
extern "C"  void iTweenRotateFrom_OnExit_m4128789854 (iTweenRotateFrom_t265550675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateFrom::DoiTween()
extern "C"  void iTweenRotateFrom_DoiTween_m2351002680 (iTweenRotateFrom_t265550675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
