﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName
struct GetAnimatorCurrentStateInfoIsName_t1453407382;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentStateInfoIsName__ctor_m2489864370 (GetAnimatorCurrentStateInfoIsName_t1453407382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentStateInfoIsName_Reset_m2344189839 (GetAnimatorCurrentStateInfoIsName_t1453407382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnEnter()
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnEnter_m2104009767 (GetAnimatorCurrentStateInfoIsName_t1453407382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::OnActionUpdate()
extern "C"  void GetAnimatorCurrentStateInfoIsName_OnActionUpdate_m1735784142 (GetAnimatorCurrentStateInfoIsName_t1453407382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentStateInfoIsName::IsName()
extern "C"  void GetAnimatorCurrentStateInfoIsName_IsName_m2657394583 (GetAnimatorCurrentStateInfoIsName_t1453407382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
