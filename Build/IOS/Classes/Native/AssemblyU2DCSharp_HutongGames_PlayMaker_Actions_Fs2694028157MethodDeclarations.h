﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmStateTest
struct FsmStateTest_t2694028157;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::.ctor()
extern "C"  void FsmStateTest__ctor_m2103260803 (FsmStateTest_t2694028157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::Reset()
extern "C"  void FsmStateTest_Reset_m3172864746 (FsmStateTest_t2694028157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnEnter()
extern "C"  void FsmStateTest_OnEnter_m409530532 (FsmStateTest_t2694028157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::OnUpdate()
extern "C"  void FsmStateTest_OnUpdate_m604770467 (FsmStateTest_t2694028157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateTest::DoFsmStateTest()
extern "C"  void FsmStateTest_DoFsmStateTest_m1795198529 (FsmStateTest_t2694028157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
