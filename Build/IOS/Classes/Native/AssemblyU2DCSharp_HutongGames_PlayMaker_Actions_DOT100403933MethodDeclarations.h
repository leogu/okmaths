﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLightIntensity
struct DOTweenLightIntensity_t100403933;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLightIntensity::.ctor()
extern "C"  void DOTweenLightIntensity__ctor_m3321361381 (DOTweenLightIntensity_t100403933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightIntensity::Reset()
extern "C"  void DOTweenLightIntensity_Reset_m534785306 (DOTweenLightIntensity_t100403933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightIntensity::OnEnter()
extern "C"  void DOTweenLightIntensity_OnEnter_m1422774456 (DOTweenLightIntensity_t100403933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightIntensity::<OnEnter>m__4A()
extern "C"  void DOTweenLightIntensity_U3COnEnterU3Em__4A_m3142446856 (DOTweenLightIntensity_t100403933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightIntensity::<OnEnter>m__4B()
extern "C"  void DOTweenLightIntensity_U3COnEnterU3Em__4B_m3142446757 (DOTweenLightIntensity_t100403933 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
