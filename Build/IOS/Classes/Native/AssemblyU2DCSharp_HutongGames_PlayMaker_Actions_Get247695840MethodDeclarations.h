﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetParticleCollisionInfo
struct GetParticleCollisionInfo_t247695840;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::.ctor()
extern "C"  void GetParticleCollisionInfo__ctor_m983541054 (GetParticleCollisionInfo_t247695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::Reset()
extern "C"  void GetParticleCollisionInfo_Reset_m1812547013 (GetParticleCollisionInfo_t247695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::StoreCollisionInfo()
extern "C"  void GetParticleCollisionInfo_StoreCollisionInfo_m1195962043 (GetParticleCollisionInfo_t247695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetParticleCollisionInfo::OnEnter()
extern "C"  void GetParticleCollisionInfo_OnEnter_m2496521517 (GetParticleCollisionInfo_t247695840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
