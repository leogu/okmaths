﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTimeInfo
struct GetTimeInfo_t1355101333;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::.ctor()
extern "C"  void GetTimeInfo__ctor_m1183144563 (GetTimeInfo_t1355101333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::Reset()
extern "C"  void GetTimeInfo_Reset_m1650640206 (GetTimeInfo_t1355101333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::OnEnter()
extern "C"  void GetTimeInfo_OnEnter_m2664860784 (GetTimeInfo_t1355101333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::OnUpdate()
extern "C"  void GetTimeInfo_OnUpdate_m3824162243 (GetTimeInfo_t1355101333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTimeInfo::DoGetTimeInfo()
extern "C"  void GetTimeInfo_DoGetTimeInfo_m1153563773 (GetTimeInfo_t1355101333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
