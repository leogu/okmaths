﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenInit
struct DOTweenInit_t580384762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenInit::.ctor()
extern "C"  void DOTweenInit__ctor_m1618695292 (DOTweenInit_t580384762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenInit::Reset()
extern "C"  void DOTweenInit_Reset_m2102356279 (DOTweenInit_t580384762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenInit::OnEnter()
extern "C"  void DOTweenInit_OnEnter_m2962790367 (DOTweenInit_t580384762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
