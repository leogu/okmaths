﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugFsmVariable
struct DebugFsmVariable_t1864498445;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::.ctor()
extern "C"  void DebugFsmVariable__ctor_m261725949 (DebugFsmVariable_t1864498445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::Reset()
extern "C"  void DebugFsmVariable_Reset_m1330532158 (DebugFsmVariable_t1864498445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugFsmVariable::OnEnter()
extern "C"  void DebugFsmVariable_OnEnter_m539574732 (DebugFsmVariable_t1864498445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
