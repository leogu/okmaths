﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTagCount
struct GetTagCount_t3277611095;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTagCount::.ctor()
extern "C"  void GetTagCount__ctor_m735755689 (GetTagCount_t3277611095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTagCount::Reset()
extern "C"  void GetTagCount_Reset_m3189439892 (GetTagCount_t3277611095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTagCount::OnEnter()
extern "C"  void GetTagCount_OnEnter_m1758775150 (GetTagCount_t3277611095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
