﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMass
struct SetMass_t2451840088;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMass::.ctor()
extern "C"  void SetMass__ctor_m2738296178 (SetMass_t2451840088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass::Reset()
extern "C"  void SetMass_Reset_m3462334413 (SetMass_t2451840088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass::OnEnter()
extern "C"  void SetMass_OnEnter_m706586301 (SetMass_t2451840088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass::DoSetMass()
extern "C"  void SetMass_DoSetMass_m2871455341 (SetMass_t2451840088 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
