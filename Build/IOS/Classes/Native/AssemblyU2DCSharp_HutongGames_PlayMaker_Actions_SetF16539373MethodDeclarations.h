﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmEnum
struct SetFsmEnum_t16539373;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::.ctor()
extern "C"  void SetFsmEnum__ctor_m2822235635 (SetFsmEnum_t16539373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::Reset()
extern "C"  void SetFsmEnum_Reset_m213780250 (SetFsmEnum_t16539373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::OnEnter()
extern "C"  void SetFsmEnum_OnEnter_m1318469924 (SetFsmEnum_t16539373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::DoSetFsmEnum()
extern "C"  void SetFsmEnum_DoSetFsmEnum_m1383548289 (SetFsmEnum_t16539373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmEnum::OnUpdate()
extern "C"  void SetFsmEnum_OnUpdate_m3393983155 (SetFsmEnum_t16539373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
