﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertIntToString
struct ConvertIntToString_t2484598324;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::.ctor()
extern "C"  void ConvertIntToString__ctor_m3400001504 (ConvertIntToString_t2484598324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::Reset()
extern "C"  void ConvertIntToString_Reset_m1712411417 (ConvertIntToString_t2484598324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::OnEnter()
extern "C"  void ConvertIntToString_OnEnter_m4144160857 (ConvertIntToString_t2484598324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::OnUpdate()
extern "C"  void ConvertIntToString_OnUpdate_m3616733358 (ConvertIntToString_t2484598324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToString::DoConvertIntToString()
extern "C"  void ConvertIntToString_DoConvertIntToString_m3362435681 (ConvertIntToString_t2484598324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
