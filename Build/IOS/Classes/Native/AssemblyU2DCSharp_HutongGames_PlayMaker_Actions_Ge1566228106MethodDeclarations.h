﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetScreenWidth
struct GetScreenWidth_t1566228106;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetScreenWidth::.ctor()
extern "C"  void GetScreenWidth__ctor_m408644884 (GetScreenWidth_t1566228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScreenWidth::Reset()
extern "C"  void GetScreenWidth_Reset_m1850183571 (GetScreenWidth_t1566228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScreenWidth::OnEnter()
extern "C"  void GetScreenWidth_OnEnter_m2114494747 (GetScreenWidth_t1566228106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
