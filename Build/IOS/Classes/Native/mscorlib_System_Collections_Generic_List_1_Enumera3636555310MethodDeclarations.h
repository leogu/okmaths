﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2277852255(__this, ___l0, method) ((  void (*) (Enumerator_t3636555310 *, List_1_t4101825636 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1040759011(__this, method) ((  void (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2032790235(__this, method) ((  Il2CppObject * (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::Dispose()
#define Enumerator_Dispose_m3206985816(__this, method) ((  void (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::VerifyState()
#define Enumerator_VerifyState_m3054409537(__this, method) ((  void (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::MoveNext()
#define Enumerator_MoveNext_m3092022352(__this, method) ((  bool (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::get_Current()
#define Enumerator_get_Current_m4187226974(__this, method) ((  PlayMakerFSM_t437737208 * (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
