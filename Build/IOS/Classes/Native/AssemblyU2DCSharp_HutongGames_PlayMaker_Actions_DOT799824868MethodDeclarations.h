﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformShakeRotation
struct DOTweenTransformShakeRotation_t799824868;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeRotation::.ctor()
extern "C"  void DOTweenTransformShakeRotation__ctor_m1707465318 (DOTweenTransformShakeRotation_t799824868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeRotation::Reset()
extern "C"  void DOTweenTransformShakeRotation_Reset_m4253868473 (DOTweenTransformShakeRotation_t799824868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeRotation::OnEnter()
extern "C"  void DOTweenTransformShakeRotation_OnEnter_m2897908953 (DOTweenTransformShakeRotation_t799824868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeRotation::<OnEnter>m__E0()
extern "C"  void DOTweenTransformShakeRotation_U3COnEnterU3Em__E0_m4172529731 (DOTweenTransformShakeRotation_t799824868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeRotation::<OnEnter>m__E1()
extern "C"  void DOTweenTransformShakeRotation_U3COnEnterU3Em__E1_m4172529636 (DOTweenTransformShakeRotation_t799824868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
