﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject
struct GUILayoutBeginAreaFollowObject_t2353368243;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::.ctor()
extern "C"  void GUILayoutBeginAreaFollowObject__ctor_m2589160139 (GUILayoutBeginAreaFollowObject_t2353368243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::Reset()
extern "C"  void GUILayoutBeginAreaFollowObject_Reset_m1518702844 (GUILayoutBeginAreaFollowObject_t2353368243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::OnGUI()
extern "C"  void GUILayoutBeginAreaFollowObject_OnGUI_m1282986357 (GUILayoutBeginAreaFollowObject_t2353368243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::DummyBeginArea()
extern "C"  void GUILayoutBeginAreaFollowObject_DummyBeginArea_m4215257035 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
