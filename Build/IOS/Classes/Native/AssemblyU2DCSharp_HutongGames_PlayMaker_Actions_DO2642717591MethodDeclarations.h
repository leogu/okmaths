﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateRect
struct DOTweenAnimateRect_t2642717591;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateRect::.ctor()
extern "C"  void DOTweenAnimateRect__ctor_m367130811 (DOTweenAnimateRect_t2642717591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateRect::Reset()
extern "C"  void DOTweenAnimateRect_Reset_m2086651420 (DOTweenAnimateRect_t2642717591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateRect::OnEnter()
extern "C"  void DOTweenAnimateRect_OnEnter_m297670738 (DOTweenAnimateRect_t2642717591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.Actions.DOTweenAnimateRect::<OnEnter>m__C()
extern "C"  Rect_t3681755626  DOTweenAnimateRect_U3COnEnterU3Em__C_m1847445756 (DOTweenAnimateRect_t2642717591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateRect::<OnEnter>m__D(UnityEngine.Rect)
extern "C"  void DOTweenAnimateRect_U3COnEnterU3Em__D_m1127789878 (DOTweenAnimateRect_t2642717591 * __this, Rect_t3681755626  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateRect::<OnEnter>m__E()
extern "C"  void DOTweenAnimateRect_U3COnEnterU3Em__E_m3863420182 (DOTweenAnimateRect_t2642717591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateRect::<OnEnter>m__F()
extern "C"  void DOTweenAnimateRect_U3COnEnterU3Em__F_m3863420215 (DOTweenAnimateRect_t2642717591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
