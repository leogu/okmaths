﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveX
struct DOTweenTransformLocalMoveX_t1815969772;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveX::.ctor()
extern "C"  void DOTweenTransformLocalMoveX__ctor_m807830974 (DOTweenTransformLocalMoveX_t1815969772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveX::Reset()
extern "C"  void DOTweenTransformLocalMoveX_Reset_m3659105533 (DOTweenTransformLocalMoveX_t1815969772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveX::OnEnter()
extern "C"  void DOTweenTransformLocalMoveX_OnEnter_m2922991021 (DOTweenTransformLocalMoveX_t1815969772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveX::<OnEnter>m__B6()
extern "C"  void DOTweenTransformLocalMoveX_U3COnEnterU3Em__B6_m678949210 (DOTweenTransformLocalMoveX_t1815969772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveX::<OnEnter>m__B7()
extern "C"  void DOTweenTransformLocalMoveX_U3COnEnterU3Em__B7_m820111711 (DOTweenTransformLocalMoveX_t1815969772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
