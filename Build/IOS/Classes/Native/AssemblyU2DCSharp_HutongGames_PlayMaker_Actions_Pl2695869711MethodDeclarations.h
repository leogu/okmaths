﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat
struct PlayerPrefsGetFloat_t2695869711;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::.ctor()
extern "C"  void PlayerPrefsGetFloat__ctor_m3541263493 (PlayerPrefsGetFloat_t2695869711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::Reset()
extern "C"  void PlayerPrefsGetFloat_Reset_m4117656368 (PlayerPrefsGetFloat_t2695869711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetFloat::OnEnter()
extern "C"  void PlayerPrefsGetFloat_OnEnter_m1959599386 (PlayerPrefsGetFloat_t2695869711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
