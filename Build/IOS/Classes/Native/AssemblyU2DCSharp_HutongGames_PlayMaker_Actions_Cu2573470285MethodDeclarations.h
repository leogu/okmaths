﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveFloat
struct CurveFloat_t2573470285;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveFloat::.ctor()
extern "C"  void CurveFloat__ctor_m533881705 (CurveFloat_t2573470285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::Reset()
extern "C"  void CurveFloat_Reset_m1329798522 (CurveFloat_t2573470285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::OnEnter()
extern "C"  void CurveFloat_OnEnter_m1039286984 (CurveFloat_t2573470285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::OnExit()
extern "C"  void CurveFloat_OnExit_m473118324 (CurveFloat_t2573470285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveFloat::OnUpdate()
extern "C"  void CurveFloat_OnUpdate_m770905845 (CurveFloat_t2573470285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
