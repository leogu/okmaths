﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool664485696.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Boolean HutongGames.PlayMaker.FsmBool::get_Value()
extern "C"  bool FsmBool_get_Value_m3738134001 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::set_Value(System.Boolean)
extern "C"  void FsmBool_set_Value_m2522230142 (FsmBool_t664485696 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmBool::get_RawValue()
extern "C"  Il2CppObject * FsmBool_get_RawValue_m2784760110 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::set_RawValue(System.Object)
extern "C"  void FsmBool_set_RawValue_m4292948159 (FsmBool_t664485696 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::.ctor()
extern "C"  void FsmBool__ctor_m1176758167 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::.ctor(System.String)
extern "C"  void FsmBool__ctor_m2789090213 (FsmBool_t664485696 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::.ctor(HutongGames.PlayMaker.FsmBool)
extern "C"  void FsmBool__ctor_m2009683 (FsmBool_t664485696 * __this, FsmBool_t664485696 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmBool::Clone()
extern "C"  NamedVariable_t3026441313 * FsmBool_Clone_m1786839576 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmBool::get_VariableType()
extern "C"  int32_t FsmBool_get_VariableType_m1208184791 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmBool::ToString()
extern "C"  String_t* FsmBool_ToString_m2952914806 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmBool::op_Implicit(System.Boolean)
extern "C"  FsmBool_t664485696 * FsmBool_op_Implicit_m2342972798 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
