﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVelocity
struct GetVelocity_t3528056651;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVelocity::.ctor()
extern "C"  void GetVelocity__ctor_m175644971 (GetVelocity_t3528056651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::Reset()
extern "C"  void GetVelocity_Reset_m867391240 (GetVelocity_t3528056651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::OnEnter()
extern "C"  void GetVelocity_OnEnter_m1822647974 (GetVelocity_t3528056651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::OnUpdate()
extern "C"  void GetVelocity_OnUpdate_m3347273339 (GetVelocity_t3528056651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVelocity::DoGetVelocity()
extern "C"  void GetVelocity_DoGetVelocity_m3613318797 (GetVelocity_t3528056651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
