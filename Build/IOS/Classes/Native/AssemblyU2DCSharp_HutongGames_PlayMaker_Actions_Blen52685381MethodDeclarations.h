﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BlendAnimation
struct BlendAnimation_t52685381;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::.ctor()
extern "C"  void BlendAnimation__ctor_m2702317029 (BlendAnimation_t52685381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::Reset()
extern "C"  void BlendAnimation_Reset_m94645430 (BlendAnimation_t52685381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::OnEnter()
extern "C"  void BlendAnimation_OnEnter_m1101927876 (BlendAnimation_t52685381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::OnUpdate()
extern "C"  void BlendAnimation_OnUpdate_m446080121 (BlendAnimation_t52685381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BlendAnimation::DoBlendAnimation(UnityEngine.GameObject)
extern "C"  void BlendAnimation_DoBlendAnimation_m3795381229 (BlendAnimation_t52685381 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
