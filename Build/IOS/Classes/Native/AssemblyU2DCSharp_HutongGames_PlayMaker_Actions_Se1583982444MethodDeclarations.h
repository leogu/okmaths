﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUITexture
struct SetGUITexture_t1583982444;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::.ctor()
extern "C"  void SetGUITexture__ctor_m3677229904 (SetGUITexture_t1583982444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::Reset()
extern "C"  void SetGUITexture_Reset_m2690832165 (SetGUITexture_t1583982444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITexture::OnEnter()
extern "C"  void SetGUITexture_OnEnter_m4215617773 (SetGUITexture_t1583982444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
