﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListCopyTo
struct ArrayListCopyTo_t3839887165;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListCopyTo::.ctor()
extern "C"  void ArrayListCopyTo__ctor_m1775273675 (ArrayListCopyTo_t3839887165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCopyTo::Reset()
extern "C"  void ArrayListCopyTo_Reset_m2373978774 (ArrayListCopyTo_t3839887165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCopyTo::OnEnter()
extern "C"  void ArrayListCopyTo_OnEnter_m3768012040 (ArrayListCopyTo_t3839887165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListCopyTo::DoArrayListCopyTo(System.Collections.ArrayList)
extern "C"  void ArrayListCopyTo_DoArrayListCopyTo_m743327772 (ArrayListCopyTo_t3839887165 * __this, ArrayList_t4252133567 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
