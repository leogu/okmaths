﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan2
struct GetAtan2_t1152720016;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan2::.ctor()
extern "C"  void GetAtan2__ctor_m362881148 (GetAtan2_t1152720016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::Reset()
extern "C"  void GetAtan2_Reset_m543834589 (GetAtan2_t1152720016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::OnEnter()
extern "C"  void GetAtan2_OnEnter_m3607343285 (GetAtan2_t1152720016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::OnUpdate()
extern "C"  void GetAtan2_OnUpdate_m279683794 (GetAtan2_t1152720016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2::DoATan()
extern "C"  void GetAtan2_DoATan_m1477126121 (GetAtan2_t1152720016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
