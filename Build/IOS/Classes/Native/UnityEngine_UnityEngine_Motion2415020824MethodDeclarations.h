﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Motion
struct Motion_t2415020824;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Motion::.ctor()
extern "C"  void Motion__ctor_m784531835 (Motion_t2415020824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
