﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetScale
struct GetScale_t179239356;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetScale::.ctor()
extern "C"  void GetScale__ctor_m1685198274 (GetScale_t179239356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::Reset()
extern "C"  void GetScale_Reset_m4293377313 (GetScale_t179239356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::OnEnter()
extern "C"  void GetScale_OnEnter_m1405017401 (GetScale_t179239356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::OnUpdate()
extern "C"  void GetScale_OnUpdate_m1947562692 (GetScale_t179239356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScale::DoGetScale()
extern "C"  void GetScale_DoGetScale_m929225793 (GetScale_t179239356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
