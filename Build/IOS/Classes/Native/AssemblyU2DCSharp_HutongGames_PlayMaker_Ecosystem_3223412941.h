﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.String
struct String_t;
// HutongGames.PlayMaker.Ecosystem.Utils.LinkerData
struct LinkerData_t3223412941;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>>
struct Dictionary_2_t3313120627;

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.LinkerData
struct  LinkerData_t3223412941  : public ScriptableObject_t1975622470
{
public:
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::debug
	bool ___debug_2;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::LinkContentUpdateDone
	bool ___LinkContentUpdateDone_3;
	// UnityEngine.TextAsset HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::Asset
	TextAsset_t3973159845 * ___Asset_4;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::AssetPath
	String_t* ___AssetPath_5;
	// HutongGames.PlayMaker.Ecosystem.Utils.LinkerData HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::self
	LinkerData_t3223412941 * ___self_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.String>> HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::linkerEntries
	Dictionary_2_t3313120627 * ___linkerEntries_7;

public:
	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_LinkContentUpdateDone_3() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941, ___LinkContentUpdateDone_3)); }
	inline bool get_LinkContentUpdateDone_3() const { return ___LinkContentUpdateDone_3; }
	inline bool* get_address_of_LinkContentUpdateDone_3() { return &___LinkContentUpdateDone_3; }
	inline void set_LinkContentUpdateDone_3(bool value)
	{
		___LinkContentUpdateDone_3 = value;
	}

	inline static int32_t get_offset_of_Asset_4() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941, ___Asset_4)); }
	inline TextAsset_t3973159845 * get_Asset_4() const { return ___Asset_4; }
	inline TextAsset_t3973159845 ** get_address_of_Asset_4() { return &___Asset_4; }
	inline void set_Asset_4(TextAsset_t3973159845 * value)
	{
		___Asset_4 = value;
		Il2CppCodeGenWriteBarrier(&___Asset_4, value);
	}

	inline static int32_t get_offset_of_AssetPath_5() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941, ___AssetPath_5)); }
	inline String_t* get_AssetPath_5() const { return ___AssetPath_5; }
	inline String_t** get_address_of_AssetPath_5() { return &___AssetPath_5; }
	inline void set_AssetPath_5(String_t* value)
	{
		___AssetPath_5 = value;
		Il2CppCodeGenWriteBarrier(&___AssetPath_5, value);
	}

	inline static int32_t get_offset_of_self_6() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941, ___self_6)); }
	inline LinkerData_t3223412941 * get_self_6() const { return ___self_6; }
	inline LinkerData_t3223412941 ** get_address_of_self_6() { return &___self_6; }
	inline void set_self_6(LinkerData_t3223412941 * value)
	{
		___self_6 = value;
		Il2CppCodeGenWriteBarrier(&___self_6, value);
	}

	inline static int32_t get_offset_of_linkerEntries_7() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941, ___linkerEntries_7)); }
	inline Dictionary_2_t3313120627 * get_linkerEntries_7() const { return ___linkerEntries_7; }
	inline Dictionary_2_t3313120627 ** get_address_of_linkerEntries_7() { return &___linkerEntries_7; }
	inline void set_linkerEntries_7(Dictionary_2_t3313120627 * value)
	{
		___linkerEntries_7 = value;
		Il2CppCodeGenWriteBarrier(&___linkerEntries_7, value);
	}
};

struct LinkerData_t3223412941_StaticFields
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.LinkerData HutongGames.PlayMaker.Ecosystem.Utils.LinkerData::_instance_
	LinkerData_t3223412941 * ____instance__8;

public:
	inline static int32_t get_offset_of__instance__8() { return static_cast<int32_t>(offsetof(LinkerData_t3223412941_StaticFields, ____instance__8)); }
	inline LinkerData_t3223412941 * get__instance__8() const { return ____instance__8; }
	inline LinkerData_t3223412941 ** get_address_of__instance__8() { return &____instance__8; }
	inline void set__instance__8(LinkerData_t3223412941 * value)
	{
		____instance__8 = value;
		Il2CppCodeGenWriteBarrier(&____instance__8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
