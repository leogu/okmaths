﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SmoothFollowAction
struct SmoothFollowAction_t1446383579;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SmoothFollowAction::.ctor()
extern "C"  void SmoothFollowAction__ctor_m2819254083 (SmoothFollowAction_t1446383579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothFollowAction::Reset()
extern "C"  void SmoothFollowAction_Reset_m1742328100 (SmoothFollowAction_t1446383579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SmoothFollowAction::OnLateUpdate()
extern "C"  void SmoothFollowAction_OnLateUpdate_m1351577311 (SmoothFollowAction_t1446383579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
