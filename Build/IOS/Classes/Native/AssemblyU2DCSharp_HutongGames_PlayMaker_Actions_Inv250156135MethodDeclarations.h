﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.InvokeMethod
struct InvokeMethod_t250156135;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::.ctor()
extern "C"  void InvokeMethod__ctor_m1370647929 (InvokeMethod_t250156135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::Reset()
extern "C"  void InvokeMethod_Reset_m540058392 (InvokeMethod_t250156135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::OnEnter()
extern "C"  void InvokeMethod_OnEnter_m4057535202 (InvokeMethod_t250156135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::DoInvokeMethod(UnityEngine.GameObject)
extern "C"  void InvokeMethod_DoInvokeMethod_m1865940397 (InvokeMethod_t250156135 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InvokeMethod::OnExit()
extern "C"  void InvokeMethod_OnExit_m1761252294 (InvokeMethod_t250156135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
