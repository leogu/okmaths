﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin
struct RectTransformGetOffsetMin_t1653064395;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::.ctor()
extern "C"  void RectTransformGetOffsetMin__ctor_m3026089791 (RectTransformGetOffsetMin_t1653064395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::Reset()
extern "C"  void RectTransformGetOffsetMin_Reset_m298752900 (RectTransformGetOffsetMin_t1653064395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::OnEnter()
extern "C"  void RectTransformGetOffsetMin_OnEnter_m2681222010 (RectTransformGetOffsetMin_t1653064395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::OnActionUpdate()
extern "C"  void RectTransformGetOffsetMin_OnActionUpdate_m1005386041 (RectTransformGetOffsetMin_t1653064395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetOffsetMin::DoGetValues()
extern "C"  void RectTransformGetOffsetMin_DoGetValues_m1820416190 (RectTransformGetOffsetMin_t1653064395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
