﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectIsChildOf
struct GameObjectIsChildOf_t3059270092;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::.ctor()
extern "C"  void GameObjectIsChildOf__ctor_m3015610110 (GameObjectIsChildOf_t3059270092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::Reset()
extern "C"  void GameObjectIsChildOf_Reset_m1305544161 (GameObjectIsChildOf_t3059270092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::OnEnter()
extern "C"  void GameObjectIsChildOf_OnEnter_m2723589745 (GameObjectIsChildOf_t3059270092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsChildOf::DoIsChildOf(UnityEngine.GameObject)
extern "C"  void GameObjectIsChildOf_DoIsChildOf_m2633538304 (GameObjectIsChildOf_t3059270092 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
