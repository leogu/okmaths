﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetStringRight
struct GetStringRight_t1778127835;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetStringRight::.ctor()
extern "C"  void GetStringRight__ctor_m534874329 (GetStringRight_t1778127835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::Reset()
extern "C"  void GetStringRight_Reset_m1980720736 (GetStringRight_t1778127835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::OnEnter()
extern "C"  void GetStringRight_OnEnter_m3701143426 (GetStringRight_t1778127835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::OnUpdate()
extern "C"  void GetStringRight_OnUpdate_m3877238789 (GetStringRight_t1778127835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringRight::DoGetStringRight()
extern "C"  void GetStringRight_DoGetStringRight_m1638278209 (GetStringRight_t1778127835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
