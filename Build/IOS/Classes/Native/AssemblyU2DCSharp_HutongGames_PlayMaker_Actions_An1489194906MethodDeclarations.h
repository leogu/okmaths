﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorStopPlayback
struct AnimatorStopPlayback_t1489194906;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::.ctor()
extern "C"  void AnimatorStopPlayback__ctor_m3629034588 (AnimatorStopPlayback_t1489194906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::Reset()
extern "C"  void AnimatorStopPlayback_Reset_m1908939147 (AnimatorStopPlayback_t1489194906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorStopPlayback::OnEnter()
extern "C"  void AnimatorStopPlayback_OnEnter_m1544155803 (AnimatorStopPlayback_t1489194906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
