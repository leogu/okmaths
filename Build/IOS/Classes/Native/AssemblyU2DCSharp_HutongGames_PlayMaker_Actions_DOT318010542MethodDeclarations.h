﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenOutlineFade
struct DOTweenOutlineFade_t318010542;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineFade::.ctor()
extern "C"  void DOTweenOutlineFade__ctor_m2900589480 (DOTweenOutlineFade_t318010542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineFade::Reset()
extern "C"  void DOTweenOutlineFade_Reset_m3692991327 (DOTweenOutlineFade_t318010542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineFade::OnEnter()
extern "C"  void DOTweenOutlineFade_OnEnter_m1687282735 (DOTweenOutlineFade_t318010542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineFade::<OnEnter>m__66()
extern "C"  void DOTweenOutlineFade_U3COnEnterU3Em__66_m3480240632 (DOTweenOutlineFade_t318010542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineFade::<OnEnter>m__67()
extern "C"  void DOTweenOutlineFade_U3COnEnterU3Em__67_m3621403133 (DOTweenOutlineFade_t318010542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
