﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseById
struct DOTweenControlMethodsPauseById_t283039607;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseById::.ctor()
extern "C"  void DOTweenControlMethodsPauseById__ctor_m2505508519 (DOTweenControlMethodsPauseById_t283039607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseById::Reset()
extern "C"  void DOTweenControlMethodsPauseById_Reset_m3948401728 (DOTweenControlMethodsPauseById_t283039607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPauseById::OnEnter()
extern "C"  void DOTweenControlMethodsPauseById_OnEnter_m2666468486 (DOTweenControlMethodsPauseById_t283039607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
