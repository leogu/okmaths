﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t4177556671;
// UnityEngine.AnimationState
struct AnimationState_t1303741697;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t4287019078;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject3097142863.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector33996534004.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault2023674184.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel3809977218.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"

// UnityEngine.Texture2D HutongGames.PlayMaker.ActionHelpers::get_WhiteTexture()
extern "C"  Texture2D_t3542995729 * ActionHelpers_get_WhiteTexture_m1805562354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::IsVisible(UnityEngine.GameObject)
extern "C"  bool ActionHelpers_IsVisible_m2570058896 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::RuntimeError(HutongGames.PlayMaker.FsmStateAction,System.String)
extern "C"  void ActionHelpers_RuntimeError_m2759970799 (Il2CppObject * __this /* static, unused */, FsmStateAction_t2862378169 * ___action0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM HutongGames.PlayMaker.ActionHelpers::GetGameObjectFsm(UnityEngine.GameObject,System.String)
extern "C"  PlayMakerFSM_t437737208 * ActionHelpers_GetGameObjectFsm_m2570111466 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionHelpers::GetRandomWeightedIndex(HutongGames.PlayMaker.FsmFloat[])
extern "C"  int32_t ActionHelpers_GetRandomWeightedIndex_m3581206222 (Il2CppObject * __this /* static, unused */, FsmFloatU5BU5D_t4177556671* ___weights0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::HasAnimationFinished(UnityEngine.AnimationState,System.Single,System.Single)
extern "C"  bool ActionHelpers_HasAnimationFinished_m1275399874 (Il2CppObject * __this /* static, unused */, AnimationState_t1303741697 * ___anim0, float ___prevTime1, float ___currentTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.ActionHelpers::GetPosition(HutongGames.PlayMaker.FsmGameObject,HutongGames.PlayMaker.FsmVector3)
extern "C"  Vector3_t2243707580  ActionHelpers_GetPosition_m679032034 (Il2CppObject * __this /* static, unused */, FsmGameObject_t3097142863 * ___fsmGameObject0, FsmVector3_t3996534004 * ___fsmVector31, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::IsMouseOver(UnityEngine.GameObject,System.Single,System.Int32)
extern "C"  bool ActionHelpers_IsMouseOver_m375012187 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit HutongGames.PlayMaker.ActionHelpers::MousePick(System.Single,System.Int32)
extern "C"  RaycastHit_t87180320  ActionHelpers_MousePick_m2692860946 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.ActionHelpers::MouseOver(System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * ActionHelpers_MouseOver_m1354246946 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::DoMousePick(System.Single,System.Int32)
extern "C"  void ActionHelpers_DoMousePick_m1416612573 (Il2CppObject * __this /* static, unused */, float ___distance0, int32_t ___layerMask1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionHelpers::LayerArrayToLayerMask(HutongGames.PlayMaker.FsmInt[],System.Boolean)
extern "C"  int32_t ActionHelpers_LayerArrayToLayerMask_m4230506674 (Il2CppObject * __this /* static, unused */, FsmIntU5BU5D_t2637547802* ___layers0, bool ___invert1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionHelpers::IsLoopingWrapMode(UnityEngine.WrapMode)
extern "C"  bool ActionHelpers_IsLoopingWrapMode_m3570497147 (Il2CppObject * __this /* static, unused */, int32_t ___wrapMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckRayDistance(System.Single)
extern "C"  String_t* ActionHelpers_CheckRayDistance_m3560862441 (Il2CppObject * __this /* static, unused */, float ___rayDistance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckForValidEvent(HutongGames.PlayMaker.FsmState,System.String)
extern "C"  String_t* ActionHelpers_CheckForValidEvent_m194286151 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___state0, String_t* ___eventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckPhysicsSetup(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  String_t* ActionHelpers_CheckPhysicsSetup_m944483889 (Il2CppObject * __this /* static, unused */, FsmOwnerDefault_t2023674184 * ___ownerDefault0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckOwnerPhysicsSetup(UnityEngine.GameObject)
extern "C"  String_t* ActionHelpers_CheckOwnerPhysicsSetup_m2139107500 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckOwnerPhysics2dSetup(UnityEngine.GameObject)
extern "C"  String_t* ActionHelpers_CheckOwnerPhysics2dSetup_m3088015818 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::CheckPhysicsSetup(UnityEngine.GameObject)
extern "C"  String_t* ActionHelpers_CheckPhysicsSetup_m1000692381 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___gameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::DebugLog(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.LogLevel,System.String,System.Boolean)
extern "C"  void ActionHelpers_DebugLog_m2600111668 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, int32_t ___logLevel1, String_t* ___text2, bool ___sendToUnityLog3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::LogError(System.String)
extern "C"  void ActionHelpers_LogError_m3297803786 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::LogWarning(System.String)
extern "C"  void ActionHelpers_LogWarning_m2329513168 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::GetValueLabel(HutongGames.PlayMaker.INamedVariable)
extern "C"  String_t* ActionHelpers_GetValueLabel_m1103973512 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionHelpers::GetValueLabel(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  String_t* ActionHelpers_GetValueLabel_m3561079964 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, FsmOwnerDefault_t2023674184 * ___ownerDefault1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionHelpers::AddAnimationClip(UnityEngine.GameObject,UnityEngine.AnimationClip)
extern "C"  void ActionHelpers_AddAnimationClip_m3003172656 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, AnimationClip_t3510324950 * ___animClip1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
