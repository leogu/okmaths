﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetEventProperties
struct SetEventProperties_t666002199;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetEventProperties::.ctor()
extern "C"  void SetEventProperties__ctor_m1440318877 (SetEventProperties_t666002199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventProperties::Reset()
extern "C"  void SetEventProperties_Reset_m2880492156 (SetEventProperties_t666002199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventProperties::OnEnter()
extern "C"  void SetEventProperties_OnEnter_m2967732366 (SetEventProperties_t666002199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
