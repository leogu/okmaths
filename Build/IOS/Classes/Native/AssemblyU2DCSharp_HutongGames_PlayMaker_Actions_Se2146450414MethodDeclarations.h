﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTag
struct SetTag_t2146450414;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTag::.ctor()
extern "C"  void SetTag__ctor_m2894854984 (SetTag_t2146450414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTag::Reset()
extern "C"  void SetTag_Reset_m559280543 (SetTag_t2146450414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTag::OnEnter()
extern "C"  void SetTag_OnEnter_m1541617855 (SetTag_t2146450414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
