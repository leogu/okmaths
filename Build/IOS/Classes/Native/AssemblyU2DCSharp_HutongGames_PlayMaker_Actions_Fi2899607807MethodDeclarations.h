﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindChild
struct FindChild_t2899607807;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindChild::.ctor()
extern "C"  void FindChild__ctor_m4095404061 (FindChild_t2899607807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindChild::Reset()
extern "C"  void FindChild_Reset_m2966229720 (FindChild_t2899607807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindChild::OnEnter()
extern "C"  void FindChild_OnEnter_m1678018698 (FindChild_t2899607807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindChild::DoFindChild()
extern "C"  void FindChild_DoFindChild_m4186064945 (FindChild_t2899607807 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
