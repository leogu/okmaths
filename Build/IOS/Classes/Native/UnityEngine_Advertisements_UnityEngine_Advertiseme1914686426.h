﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_Advertisements_UnityEngine_Advertisemen714383836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnityAdsUnsupported
struct  UnityAdsUnsupported_t1914686426  : public UnityAdsPlatform_t714383836
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
