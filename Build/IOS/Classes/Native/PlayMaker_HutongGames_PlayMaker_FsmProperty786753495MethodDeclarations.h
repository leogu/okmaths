﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t786753495;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty786753495.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.FsmProperty::.ctor()
extern "C"  void FsmProperty__ctor_m3379579666 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::.ctor(HutongGames.PlayMaker.FsmProperty)
extern "C"  void FsmProperty__ctor_m3292484055 (FsmProperty_t786753495 * __this, FsmProperty_t786753495 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::SetVariable(HutongGames.PlayMaker.NamedVariable)
extern "C"  void FsmProperty_SetVariable_m3985467369 (FsmProperty_t786753495 * __this, NamedVariable_t3026441313 * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmProperty::GetVariable()
extern "C"  NamedVariable_t3026441313 * FsmProperty_GetVariable_m366107146 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::SetPropertyName(System.String)
extern "C"  void FsmProperty_SetPropertyName_m2983506722 (FsmProperty_t786753495 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::SetValue()
extern "C"  void FsmProperty_SetValue_m4077847927 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::GetValue()
extern "C"  void FsmProperty_GetValue_m1612573019 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::Init()
extern "C"  void FsmProperty_Init_m3997690210 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::CheckForReinitialize()
extern "C"  void FsmProperty_CheckForReinitialize_m1136842896 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmProperty::ResetParameters()
extern "C"  void FsmProperty_ResetParameters_m2295946509 (FsmProperty_t786753495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
