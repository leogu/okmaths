﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers
struct uGuiSetAnimationTriggers_t2640876623;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::.ctor()
extern "C"  void uGuiSetAnimationTriggers__ctor_m1401448839 (uGuiSetAnimationTriggers_t2640876623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::Reset()
extern "C"  void uGuiSetAnimationTriggers_Reset_m3361917696 (uGuiSetAnimationTriggers_t2640876623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::OnEnter()
extern "C"  void uGuiSetAnimationTriggers_OnEnter_m3873781854 (uGuiSetAnimationTriggers_t2640876623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::DoSetValue()
extern "C"  void uGuiSetAnimationTriggers_DoSetValue_m2800725177 (uGuiSetAnimationTriggers_t2640876623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetAnimationTriggers::OnExit()
extern "C"  void uGuiSetAnimationTriggers_OnExit_m3984774382 (uGuiSetAnimationTriggers_t2640876623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
