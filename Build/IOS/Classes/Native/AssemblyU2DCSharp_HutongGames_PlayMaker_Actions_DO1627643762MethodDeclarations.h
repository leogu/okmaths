﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindById
struct DOTweenControlMethodsRewindById_t1627643762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindById::.ctor()
extern "C"  void DOTweenControlMethodsRewindById__ctor_m3689576888 (DOTweenControlMethodsRewindById_t1627643762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindById::Reset()
extern "C"  void DOTweenControlMethodsRewindById_Reset_m1941016971 (DOTweenControlMethodsRewindById_t1627643762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRewindById::OnEnter()
extern "C"  void DOTweenControlMethodsRewindById_OnEnter_m3883085547 (DOTweenControlMethodsRewindById_t1627643762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
