﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.InverseTransformDirection
struct InverseTransformDirection_t3308449633;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::.ctor()
extern "C"  void InverseTransformDirection__ctor_m869338687 (InverseTransformDirection_t3308449633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::Reset()
extern "C"  void InverseTransformDirection_Reset_m1686092674 (InverseTransformDirection_t3308449633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::OnEnter()
extern "C"  void InverseTransformDirection_OnEnter_m2592052140 (InverseTransformDirection_t3308449633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::OnUpdate()
extern "C"  void InverseTransformDirection_OnUpdate_m1905642391 (InverseTransformDirection_t3308449633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.InverseTransformDirection::DoInverseTransformDirection()
extern "C"  void InverseTransformDirection_DoInverseTransformDirection_m2472280237 (InverseTransformDirection_t3308449633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
