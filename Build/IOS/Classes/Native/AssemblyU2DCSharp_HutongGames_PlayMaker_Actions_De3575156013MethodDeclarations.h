﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugLog
struct DebugLog_t3575156013;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugLog::.ctor()
extern "C"  void DebugLog__ctor_m2035210791 (DebugLog_t3575156013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugLog::Reset()
extern "C"  void DebugLog_Reset_m3476174518 (DebugLog_t3575156013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugLog::OnEnter()
extern "C"  void DebugLog_OnEnter_m1520566936 (DebugLog_t3575156013 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
