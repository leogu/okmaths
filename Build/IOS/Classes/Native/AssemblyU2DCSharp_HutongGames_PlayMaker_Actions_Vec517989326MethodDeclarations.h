﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2PerSecond
struct Vector2PerSecond_t517989326;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::.ctor()
extern "C"  void Vector2PerSecond__ctor_m3643247742 (Vector2PerSecond_t517989326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::Reset()
extern "C"  void Vector2PerSecond_Reset_m142611775 (Vector2PerSecond_t517989326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::OnEnter()
extern "C"  void Vector2PerSecond_OnEnter_m1676004359 (Vector2PerSecond_t517989326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2PerSecond::OnUpdate()
extern "C"  void Vector2PerSecond_OnUpdate_m2009619184 (Vector2PerSecond_t517989326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
