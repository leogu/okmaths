﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1624700828;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1624700828.h"

// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.DelayedEvent::get_FsmEvent()
extern "C"  FsmEvent_t1258573736 * DelayedEvent_get_FsmEvent_m1628537977 (DelayedEvent_t1624700828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.DelayedEvent::get_Timer()
extern "C"  float DelayedEvent_get_Timer_m2908372965 (DelayedEvent_t1624700828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  void DelayedEvent__ctor_m2544356758 (DelayedEvent_t1624700828 * __this, Fsm_t917886356 * ___fsm0, FsmEvent_t1258573736 * ___fsmEvent1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,System.String,System.Single)
extern "C"  void DelayedEvent__ctor_m3300578730 (DelayedEvent_t1624700828 * __this, Fsm_t917886356 * ___fsm0, String_t* ___fsmEventName1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmEventTarget,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  void DelayedEvent__ctor_m219312015 (DelayedEvent_t1624700828 * __this, Fsm_t917886356 * ___fsm0, FsmEventTarget_t172293745 * ___eventTarget1, FsmEvent_t1258573736 * ___fsmEvent2, float ___delay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmEventTarget,System.String,System.Single)
extern "C"  void DelayedEvent__ctor_m4088405755 (DelayedEvent_t1624700828 * __this, Fsm_t917886356 * ___fsm0, FsmEventTarget_t172293745 * ___eventTarget1, String_t* ___fsmEvent2, float ___delay3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(PlayMakerFSM,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  void DelayedEvent__ctor_m868433610 (DelayedEvent_t1624700828 * __this, PlayMakerFSM_t437737208 * ___fsm0, FsmEvent_t1258573736 * ___fsmEvent1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::.ctor(PlayMakerFSM,System.String,System.Single)
extern "C"  void DelayedEvent__ctor_m2152531726 (DelayedEvent_t1624700828 * __this, PlayMakerFSM_t437737208 * ___fsm0, String_t* ___fsmEventName1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.DelayedEvent::Update()
extern "C"  void DelayedEvent_Update_m831470360 (DelayedEvent_t1624700828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.DelayedEvent::WasSent(HutongGames.PlayMaker.DelayedEvent)
extern "C"  bool DelayedEvent_WasSent_m1980822908 (Il2CppObject * __this /* static, unused */, DelayedEvent_t1624700828 * ___delayedEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.DelayedEvent::get_Finished()
extern "C"  bool DelayedEvent_get_Finished_m1620759544 (DelayedEvent_t1624700828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
