﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition
struct RectTransformGetLocalPosition_t852213708;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::.ctor()
extern "C"  void RectTransformGetLocalPosition__ctor_m205561256 (RectTransformGetLocalPosition_t852213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::Reset()
extern "C"  void RectTransformGetLocalPosition_Reset_m732306541 (RectTransformGetLocalPosition_t852213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::OnEnter()
extern "C"  void RectTransformGetLocalPosition_OnEnter_m3655747149 (RectTransformGetLocalPosition_t852213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::OnActionUpdate()
extern "C"  void RectTransformGetLocalPosition_OnActionUpdate_m2152985716 (RectTransformGetLocalPosition_t852213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::DoGetValues()
extern "C"  void RectTransformGetLocalPosition_DoGetValues_m3008350953 (RectTransformGetLocalPosition_t852213708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
