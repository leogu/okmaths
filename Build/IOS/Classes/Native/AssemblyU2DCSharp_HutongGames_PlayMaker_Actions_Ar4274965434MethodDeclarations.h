﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListIsEmpty
struct ArrayListIsEmpty_t4274965434;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListIsEmpty::.ctor()
extern "C"  void ArrayListIsEmpty__ctor_m3282947918 (ArrayListIsEmpty_t4274965434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListIsEmpty::Reset()
extern "C"  void ArrayListIsEmpty_Reset_m1321111743 (ArrayListIsEmpty_t4274965434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListIsEmpty::OnEnter()
extern "C"  void ArrayListIsEmpty_OnEnter_m3296213727 (ArrayListIsEmpty_t4274965434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
