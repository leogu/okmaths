﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle
struct RectTransformScreenPointToWorldPointInRectangle_t3466560733;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::.ctor()
extern "C"  void RectTransformScreenPointToWorldPointInRectangle__ctor_m3082493113 (RectTransformScreenPointToWorldPointInRectangle_t3466560733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::Reset()
extern "C"  void RectTransformScreenPointToWorldPointInRectangle_Reset_m3764526110 (RectTransformScreenPointToWorldPointInRectangle_t3466560733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::OnEnter()
extern "C"  void RectTransformScreenPointToWorldPointInRectangle_OnEnter_m4172376188 (RectTransformScreenPointToWorldPointInRectangle_t3466560733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::OnUpdate()
extern "C"  void RectTransformScreenPointToWorldPointInRectangle_OnUpdate_m1931222949 (RectTransformScreenPointToWorldPointInRectangle_t3466560733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::DoCheck()
extern "C"  void RectTransformScreenPointToWorldPointInRectangle_DoCheck_m1954091126 (RectTransformScreenPointToWorldPointInRectangle_t3466560733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
