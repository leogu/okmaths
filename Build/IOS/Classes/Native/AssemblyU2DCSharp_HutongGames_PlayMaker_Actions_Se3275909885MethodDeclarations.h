﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIAlpha
struct SetGUIAlpha_t3275909885;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::.ctor()
extern "C"  void SetGUIAlpha__ctor_m735290051 (SetGUIAlpha_t3275909885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::Reset()
extern "C"  void SetGUIAlpha_Reset_m3039563358 (SetGUIAlpha_t3275909885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIAlpha::OnGUI()
extern "C"  void SetGUIAlpha_OnGUI_m1182120921 (SetGUIAlpha_t3275909885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
