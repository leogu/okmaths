﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat
struct uGuiInputFieldGetTextAsFloat_t2418060331;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::.ctor()
extern "C"  void uGuiInputFieldGetTextAsFloat__ctor_m3316141377 (uGuiInputFieldGetTextAsFloat_t2418060331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::Reset()
extern "C"  void uGuiInputFieldGetTextAsFloat_Reset_m3496306936 (uGuiInputFieldGetTextAsFloat_t2418060331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::OnEnter()
extern "C"  void uGuiInputFieldGetTextAsFloat_OnEnter_m2669895586 (uGuiInputFieldGetTextAsFloat_t2418060331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::OnUpdate()
extern "C"  void uGuiInputFieldGetTextAsFloat_OnUpdate_m2493546773 (uGuiInputFieldGetTextAsFloat_t2418060331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsFloat::DoGetTextValue()
extern "C"  void uGuiInputFieldGetTextAsFloat_DoGetTextValue_m2117952750 (uGuiInputFieldGetTextAsFloat_t2418060331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
