﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"

// System.Void UnityEngine.AnimationClip::.ctor()
extern "C"  void AnimationClip__ctor_m3495408353 (AnimationClip_t3510324950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m2274684340 (Il2CppObject * __this /* static, unused */, AnimationClip_t3510324950 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
extern "C"  void AnimationClip_SetCurve_m2504807125 (AnimationClip_t3510324950 * __this, String_t* ___relativePath0, Type_t * ___type1, String_t* ___propertyName2, AnimationCurve_t3306541151 * ___curve3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
