﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialFadeProperty
struct DOTweenMaterialFadeProperty_t3441056982;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFadeProperty::.ctor()
extern "C"  void DOTweenMaterialFadeProperty__ctor_m2925960756 (DOTweenMaterialFadeProperty_t3441056982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFadeProperty::Reset()
extern "C"  void DOTweenMaterialFadeProperty_Reset_m3369378639 (DOTweenMaterialFadeProperty_t3441056982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFadeProperty::OnEnter()
extern "C"  void DOTweenMaterialFadeProperty_OnEnter_m3018707519 (DOTweenMaterialFadeProperty_t3441056982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFadeProperty::<OnEnter>m__56()
extern "C"  void DOTweenMaterialFadeProperty_U3COnEnterU3Em__56_m332697347 (DOTweenMaterialFadeProperty_t3441056982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialFadeProperty::<OnEnter>m__57()
extern "C"  void DOTweenMaterialFadeProperty_U3COnEnterU3Em__57_m332697380 (DOTweenMaterialFadeProperty_t3441056982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
