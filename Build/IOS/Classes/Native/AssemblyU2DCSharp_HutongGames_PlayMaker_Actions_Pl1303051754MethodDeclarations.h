﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsGetInt
struct PlayerPrefsGetInt_t1303051754;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::.ctor()
extern "C"  void PlayerPrefsGetInt__ctor_m916214134 (PlayerPrefsGetInt_t1303051754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::Reset()
extern "C"  void PlayerPrefsGetInt_Reset_m709307243 (PlayerPrefsGetInt_t1303051754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsGetInt::OnEnter()
extern "C"  void PlayerPrefsGetInt_OnEnter_m2343632747 (PlayerPrefsGetInt_t1303051754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
