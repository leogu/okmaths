﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint
struct RectTransformContainsScreenPoint_t1785956669;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::.ctor()
extern "C"  void RectTransformContainsScreenPoint__ctor_m467764769 (RectTransformContainsScreenPoint_t1785956669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::Reset()
extern "C"  void RectTransformContainsScreenPoint_Reset_m1302962818 (RectTransformContainsScreenPoint_t1785956669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::OnEnter()
extern "C"  void RectTransformContainsScreenPoint_OnEnter_m3446855864 (RectTransformContainsScreenPoint_t1785956669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::OnUpdate()
extern "C"  void RectTransformContainsScreenPoint_OnUpdate_m753446341 (RectTransformContainsScreenPoint_t1785956669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformContainsScreenPoint::DoCheck()
extern "C"  void RectTransformContainsScreenPoint_DoCheck_m2876349338 (RectTransformContainsScreenPoint_t1785956669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
