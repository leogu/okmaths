﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FsmTemplate
struct FsmTemplate_t1285897084;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String FsmTemplate::get_Category()
extern "C"  String_t* FsmTemplate_get_Category_m4148816523 (FsmTemplate_t1285897084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FsmTemplate::set_Category(System.String)
extern "C"  void FsmTemplate_set_Category_m1152343194 (FsmTemplate_t1285897084 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FsmTemplate::OnEnable()
extern "C"  void FsmTemplate_OnEnable_m1658985275 (FsmTemplate_t1285897084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FsmTemplate::.ctor()
extern "C"  void FsmTemplate__ctor_m1961479239 (FsmTemplate_t1285897084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
