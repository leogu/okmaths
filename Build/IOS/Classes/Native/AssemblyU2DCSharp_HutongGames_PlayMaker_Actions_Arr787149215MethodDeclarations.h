﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObject
struct ArrayListGetFarthestGameObject_t787149215;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObject::.ctor()
extern "C"  void ArrayListGetFarthestGameObject__ctor_m1535186443 (ArrayListGetFarthestGameObject_t787149215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObject::Reset()
extern "C"  void ArrayListGetFarthestGameObject_Reset_m3218444492 (ArrayListGetFarthestGameObject_t787149215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObject::OnEnter()
extern "C"  void ArrayListGetFarthestGameObject_OnEnter_m533336554 (ArrayListGetFarthestGameObject_t787149215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObject::OnUpdate()
extern "C"  void ArrayListGetFarthestGameObject_OnUpdate_m1213481955 (ArrayListGetFarthestGameObject_t787149215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObject::DoFindFarthestGo()
extern "C"  void ArrayListGetFarthestGameObject_DoFindFarthestGo_m921940692 (ArrayListGetFarthestGameObject_t787149215 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
