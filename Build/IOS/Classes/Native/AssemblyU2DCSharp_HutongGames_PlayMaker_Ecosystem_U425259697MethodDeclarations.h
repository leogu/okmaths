﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.EventTargetVariable
struct EventTargetVariable_t425259697;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.EventTargetVariable::.ctor(System.String)
extern "C"  void EventTargetVariable__ctor_m733267087 (EventTargetVariable_t425259697 * __this, String_t* ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
