﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// DG.Tweening.Sequence
struct Sequence_t110643099;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump
struct  DOTweenRigidbody2DJump_t9691787  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::to
	FsmVector2_t2430450063 * ___to_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::snapping
	FsmBool_t664485696 * ___snapping_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::jumpPower
	FsmFloat_t937133978 * ___jumpPower_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::numJumps
	FsmInt_t1273009179 * ___numJumps_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::duration
	FsmFloat_t937133978 * ___duration_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::startDelay
	FsmFloat_t937133978 * ___startDelay_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::startEvent
	FsmEvent_t1258573736 * ___startEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_20;
	// System.String HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::tweenIdDescription
	String_t* ___tweenIdDescription_21;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::tweenIdType
	int32_t ___tweenIdType_22;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::stringAsId
	FsmString_t2414474701 * ___stringAsId_23;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::tagAsId
	FsmString_t2414474701 * ___tagAsId_24;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::selectedEase
	int32_t ___selectedEase_25;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::easeType
	int32_t ___easeType_26;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_27;
	// System.String HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_28;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::loops
	FsmInt_t1273009179 * ___loops_29;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::loopType
	int32_t ___loopType_30;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_31;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::recyclable
	FsmBool_t664485696 * ___recyclable_32;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::updateType
	int32_t ___updateType_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::debugThis
	FsmBool_t664485696 * ___debugThis_35;
	// DG.Tweening.Sequence HutongGames.PlayMaker.Actions.DOTweenRigidbody2DJump::sequence
	Sequence_t110643099 * ___sequence_36;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_to_12() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___to_12)); }
	inline FsmVector2_t2430450063 * get_to_12() const { return ___to_12; }
	inline FsmVector2_t2430450063 ** get_address_of_to_12() { return &___to_12; }
	inline void set_to_12(FsmVector2_t2430450063 * value)
	{
		___to_12 = value;
		Il2CppCodeGenWriteBarrier(&___to_12, value);
	}

	inline static int32_t get_offset_of_snapping_13() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___snapping_13)); }
	inline FsmBool_t664485696 * get_snapping_13() const { return ___snapping_13; }
	inline FsmBool_t664485696 ** get_address_of_snapping_13() { return &___snapping_13; }
	inline void set_snapping_13(FsmBool_t664485696 * value)
	{
		___snapping_13 = value;
		Il2CppCodeGenWriteBarrier(&___snapping_13, value);
	}

	inline static int32_t get_offset_of_jumpPower_14() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___jumpPower_14)); }
	inline FsmFloat_t937133978 * get_jumpPower_14() const { return ___jumpPower_14; }
	inline FsmFloat_t937133978 ** get_address_of_jumpPower_14() { return &___jumpPower_14; }
	inline void set_jumpPower_14(FsmFloat_t937133978 * value)
	{
		___jumpPower_14 = value;
		Il2CppCodeGenWriteBarrier(&___jumpPower_14, value);
	}

	inline static int32_t get_offset_of_numJumps_15() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___numJumps_15)); }
	inline FsmInt_t1273009179 * get_numJumps_15() const { return ___numJumps_15; }
	inline FsmInt_t1273009179 ** get_address_of_numJumps_15() { return &___numJumps_15; }
	inline void set_numJumps_15(FsmInt_t1273009179 * value)
	{
		___numJumps_15 = value;
		Il2CppCodeGenWriteBarrier(&___numJumps_15, value);
	}

	inline static int32_t get_offset_of_duration_16() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___duration_16)); }
	inline FsmFloat_t937133978 * get_duration_16() const { return ___duration_16; }
	inline FsmFloat_t937133978 ** get_address_of_duration_16() { return &___duration_16; }
	inline void set_duration_16(FsmFloat_t937133978 * value)
	{
		___duration_16 = value;
		Il2CppCodeGenWriteBarrier(&___duration_16, value);
	}

	inline static int32_t get_offset_of_startDelay_17() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___startDelay_17)); }
	inline FsmFloat_t937133978 * get_startDelay_17() const { return ___startDelay_17; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_17() { return &___startDelay_17; }
	inline void set_startDelay_17(FsmFloat_t937133978 * value)
	{
		___startDelay_17 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_17, value);
	}

	inline static int32_t get_offset_of_startEvent_18() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___startEvent_18)); }
	inline FsmEvent_t1258573736 * get_startEvent_18() const { return ___startEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_18() { return &___startEvent_18; }
	inline void set_startEvent_18(FsmEvent_t1258573736 * value)
	{
		___startEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_18, value);
	}

	inline static int32_t get_offset_of_finishEvent_19() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___finishEvent_19)); }
	inline FsmEvent_t1258573736 * get_finishEvent_19() const { return ___finishEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_19() { return &___finishEvent_19; }
	inline void set_finishEvent_19(FsmEvent_t1258573736 * value)
	{
		___finishEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_19, value);
	}

	inline static int32_t get_offset_of_finishImmediately_20() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___finishImmediately_20)); }
	inline FsmBool_t664485696 * get_finishImmediately_20() const { return ___finishImmediately_20; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_20() { return &___finishImmediately_20; }
	inline void set_finishImmediately_20(FsmBool_t664485696 * value)
	{
		___finishImmediately_20 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_20, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_21() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___tweenIdDescription_21)); }
	inline String_t* get_tweenIdDescription_21() const { return ___tweenIdDescription_21; }
	inline String_t** get_address_of_tweenIdDescription_21() { return &___tweenIdDescription_21; }
	inline void set_tweenIdDescription_21(String_t* value)
	{
		___tweenIdDescription_21 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_21, value);
	}

	inline static int32_t get_offset_of_tweenIdType_22() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___tweenIdType_22)); }
	inline int32_t get_tweenIdType_22() const { return ___tweenIdType_22; }
	inline int32_t* get_address_of_tweenIdType_22() { return &___tweenIdType_22; }
	inline void set_tweenIdType_22(int32_t value)
	{
		___tweenIdType_22 = value;
	}

	inline static int32_t get_offset_of_stringAsId_23() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___stringAsId_23)); }
	inline FsmString_t2414474701 * get_stringAsId_23() const { return ___stringAsId_23; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_23() { return &___stringAsId_23; }
	inline void set_stringAsId_23(FsmString_t2414474701 * value)
	{
		___stringAsId_23 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_23, value);
	}

	inline static int32_t get_offset_of_tagAsId_24() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___tagAsId_24)); }
	inline FsmString_t2414474701 * get_tagAsId_24() const { return ___tagAsId_24; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_24() { return &___tagAsId_24; }
	inline void set_tagAsId_24(FsmString_t2414474701 * value)
	{
		___tagAsId_24 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_24, value);
	}

	inline static int32_t get_offset_of_selectedEase_25() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___selectedEase_25)); }
	inline int32_t get_selectedEase_25() const { return ___selectedEase_25; }
	inline int32_t* get_address_of_selectedEase_25() { return &___selectedEase_25; }
	inline void set_selectedEase_25(int32_t value)
	{
		___selectedEase_25 = value;
	}

	inline static int32_t get_offset_of_easeType_26() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___easeType_26)); }
	inline int32_t get_easeType_26() const { return ___easeType_26; }
	inline int32_t* get_address_of_easeType_26() { return &___easeType_26; }
	inline void set_easeType_26(int32_t value)
	{
		___easeType_26 = value;
	}

	inline static int32_t get_offset_of_animationCurve_27() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___animationCurve_27)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_27() const { return ___animationCurve_27; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_27() { return &___animationCurve_27; }
	inline void set_animationCurve_27(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_27 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_27, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_28() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___loopsDescriptionArea_28)); }
	inline String_t* get_loopsDescriptionArea_28() const { return ___loopsDescriptionArea_28; }
	inline String_t** get_address_of_loopsDescriptionArea_28() { return &___loopsDescriptionArea_28; }
	inline void set_loopsDescriptionArea_28(String_t* value)
	{
		___loopsDescriptionArea_28 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_28, value);
	}

	inline static int32_t get_offset_of_loops_29() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___loops_29)); }
	inline FsmInt_t1273009179 * get_loops_29() const { return ___loops_29; }
	inline FsmInt_t1273009179 ** get_address_of_loops_29() { return &___loops_29; }
	inline void set_loops_29(FsmInt_t1273009179 * value)
	{
		___loops_29 = value;
		Il2CppCodeGenWriteBarrier(&___loops_29, value);
	}

	inline static int32_t get_offset_of_loopType_30() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___loopType_30)); }
	inline int32_t get_loopType_30() const { return ___loopType_30; }
	inline int32_t* get_address_of_loopType_30() { return &___loopType_30; }
	inline void set_loopType_30(int32_t value)
	{
		___loopType_30 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_31() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___autoKillOnCompletion_31)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_31() const { return ___autoKillOnCompletion_31; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_31() { return &___autoKillOnCompletion_31; }
	inline void set_autoKillOnCompletion_31(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_31 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_31, value);
	}

	inline static int32_t get_offset_of_recyclable_32() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___recyclable_32)); }
	inline FsmBool_t664485696 * get_recyclable_32() const { return ___recyclable_32; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_32() { return &___recyclable_32; }
	inline void set_recyclable_32(FsmBool_t664485696 * value)
	{
		___recyclable_32 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_32, value);
	}

	inline static int32_t get_offset_of_updateType_33() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___updateType_33)); }
	inline int32_t get_updateType_33() const { return ___updateType_33; }
	inline int32_t* get_address_of_updateType_33() { return &___updateType_33; }
	inline void set_updateType_33(int32_t value)
	{
		___updateType_33 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_34() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___isIndependentUpdate_34)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_34() const { return ___isIndependentUpdate_34; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_34() { return &___isIndependentUpdate_34; }
	inline void set_isIndependentUpdate_34(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_34 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_34, value);
	}

	inline static int32_t get_offset_of_debugThis_35() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___debugThis_35)); }
	inline FsmBool_t664485696 * get_debugThis_35() const { return ___debugThis_35; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_35() { return &___debugThis_35; }
	inline void set_debugThis_35(FsmBool_t664485696 * value)
	{
		___debugThis_35 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_35, value);
	}

	inline static int32_t get_offset_of_sequence_36() { return static_cast<int32_t>(offsetof(DOTweenRigidbody2DJump_t9691787, ___sequence_36)); }
	inline Sequence_t110643099 * get_sequence_36() const { return ___sequence_36; }
	inline Sequence_t110643099 ** get_address_of_sequence_36() { return &___sequence_36; }
	inline void set_sequence_36(Sequence_t110643099 * value)
	{
		___sequence_36 = value;
		Il2CppCodeGenWriteBarrier(&___sequence_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
