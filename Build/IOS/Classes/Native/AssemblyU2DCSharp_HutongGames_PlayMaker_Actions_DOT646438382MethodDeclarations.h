﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenSliderValue
struct DOTweenSliderValue_t646438382;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenSliderValue::.ctor()
extern "C"  void DOTweenSliderValue__ctor_m83785298 (DOTweenSliderValue_t646438382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSliderValue::Reset()
extern "C"  void DOTweenSliderValue_Reset_m3546791387 (DOTweenSliderValue_t646438382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSliderValue::OnEnter()
extern "C"  void DOTweenSliderValue_OnEnter_m3469854691 (DOTweenSliderValue_t646438382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSliderValue::<OnEnter>m__92()
extern "C"  void DOTweenSliderValue_U3COnEnterU3Em__92_m141326991 (DOTweenSliderValue_t646438382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSliderValue::<OnEnter>m__93()
extern "C"  void DOTweenSliderValue_U3COnEnterU3Em__93_m164490 (DOTweenSliderValue_t646438382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
