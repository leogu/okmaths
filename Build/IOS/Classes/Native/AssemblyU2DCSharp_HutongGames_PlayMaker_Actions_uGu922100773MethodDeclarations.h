﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties
struct uGuiNavigationExplicitSetProperties_t922100773;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::.ctor()
extern "C"  void uGuiNavigationExplicitSetProperties__ctor_m1833398615 (uGuiNavigationExplicitSetProperties_t922100773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::Reset()
extern "C"  void uGuiNavigationExplicitSetProperties_Reset_m1851212602 (uGuiNavigationExplicitSetProperties_t922100773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::OnEnter()
extern "C"  void uGuiNavigationExplicitSetProperties_OnEnter_m3391684908 (uGuiNavigationExplicitSetProperties_t922100773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::DoSetValue()
extern "C"  void uGuiNavigationExplicitSetProperties_DoSetValue_m1975543145 (uGuiNavigationExplicitSetProperties_t922100773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::OnExit()
extern "C"  void uGuiNavigationExplicitSetProperties_OnExit_m1007888828 (uGuiNavigationExplicitSetProperties_t922100773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
