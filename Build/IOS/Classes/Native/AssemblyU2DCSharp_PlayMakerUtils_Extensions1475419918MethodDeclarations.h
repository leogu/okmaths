﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Component
struct Component_t3819376471;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"

// System.Int32 PlayMakerUtils_Extensions::IndexOf(System.Collections.ArrayList,System.Object)
extern "C"  int32_t PlayMakerUtils_Extensions_IndexOf_m2144362061 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___target0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerUtils_Extensions::IndexOf(System.Collections.ArrayList,System.Object,System.Int32)
extern "C"  int32_t PlayMakerUtils_Extensions_IndexOf_m2215573282 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___target0, Il2CppObject * ___value1, int32_t ___startIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerUtils_Extensions::IndexOf(System.Collections.ArrayList,System.Object,System.Int32,System.Int32)
extern "C"  int32_t PlayMakerUtils_Extensions_IndexOf_m1311641473 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___target0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerUtils_Extensions::LastIndexOf(System.Collections.ArrayList,System.Object)
extern "C"  int32_t PlayMakerUtils_Extensions_LastIndexOf_m3281676427 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___target0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerUtils_Extensions::LastIndexOf(System.Collections.ArrayList,System.Object,System.Int32)
extern "C"  int32_t PlayMakerUtils_Extensions_LastIndexOf_m3555349402 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___target0, Il2CppObject * ___value1, int32_t ___startIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerUtils_Extensions::LastIndexOf(System.Collections.ArrayList,System.Object,System.Int32,System.Int32)
extern "C"  int32_t PlayMakerUtils_Extensions_LastIndexOf_m124747223 (Il2CppObject * __this /* static, unused */, ArrayList_t4252133567 * ___target0, Il2CppObject * ___value1, int32_t ___startIndex2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerUtils_Extensions::GetPath(UnityEngine.Transform)
extern "C"  String_t* PlayMakerUtils_Extensions_GetPath_m1307321432 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerUtils_Extensions::GetPath(UnityEngine.Component)
extern "C"  String_t* PlayMakerUtils_Extensions_GetPath_m738914439 (Il2CppObject * __this /* static, unused */, Component_t3819376471 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
