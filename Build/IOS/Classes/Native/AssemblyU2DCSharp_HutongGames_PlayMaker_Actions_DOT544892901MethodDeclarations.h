﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateVector3
struct DOTweenAnimateVector3_t544892901;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::.ctor()
extern "C"  void DOTweenAnimateVector3__ctor_m2597199141 (DOTweenAnimateVector3_t544892901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::Reset()
extern "C"  void DOTweenAnimateVector3_Reset_m590665722 (DOTweenAnimateVector3_t544892901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::OnEnter()
extern "C"  void DOTweenAnimateVector3_OnEnter_m4190975056 (DOTweenAnimateVector3_t544892901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::<OnEnter>m__18()
extern "C"  Vector3_t2243707580  DOTweenAnimateVector3_U3COnEnterU3Em__18_m2280945236 (DOTweenAnimateVector3_t544892901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::<OnEnter>m__19(UnityEngine.Vector3)
extern "C"  void DOTweenAnimateVector3_U3COnEnterU3Em__19_m2962999218 (DOTweenAnimateVector3_t544892901 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::<OnEnter>m__1A()
extern "C"  void DOTweenAnimateVector3_U3COnEnterU3Em__1A_m2350538231 (DOTweenAnimateVector3_t544892901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector3::<OnEnter>m__1B()
extern "C"  void DOTweenAnimateVector3_U3COnEnterU3Em__1B_m2350538260 (DOTweenAnimateVector3_t544892901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
