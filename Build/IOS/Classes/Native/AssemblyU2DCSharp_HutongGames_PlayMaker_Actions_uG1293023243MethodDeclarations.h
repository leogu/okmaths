﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiNavigationGetMode
struct uGuiNavigationGetMode_t1293023243;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::.ctor()
extern "C"  void uGuiNavigationGetMode__ctor_m466648659 (uGuiNavigationGetMode_t1293023243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::Reset()
extern "C"  void uGuiNavigationGetMode_Reset_m2863970560 (uGuiNavigationGetMode_t1293023243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::OnEnter()
extern "C"  void uGuiNavigationGetMode_OnEnter_m4223910822 (uGuiNavigationGetMode_t1293023243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::DoGetValue()
extern "C"  void uGuiNavigationGetMode_DoGetValue_m3733796169 (uGuiNavigationGetMode_t1293023243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
