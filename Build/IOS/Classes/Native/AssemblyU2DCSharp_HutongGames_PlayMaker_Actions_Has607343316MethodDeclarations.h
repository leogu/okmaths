﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableGet
struct HashTableGet_t607343316;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableGet::.ctor()
extern "C"  void HashTableGet__ctor_m2487699576 (HashTableGet_t607343316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGet::Reset()
extern "C"  void HashTableGet_Reset_m2670809761 (HashTableGet_t607343316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGet::OnEnter()
extern "C"  void HashTableGet_OnEnter_m1173916985 (HashTableGet_t607343316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGet::Get()
extern "C"  void HashTableGet_Get_m2596364944 (HashTableGet_t607343316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
