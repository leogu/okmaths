﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat
struct PlayerPrefsSetFloat_t1786127731;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::.ctor()
extern "C"  void PlayerPrefsSetFloat__ctor_m1288856385 (PlayerPrefsSetFloat_t1786127731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::Reset()
extern "C"  void PlayerPrefsSetFloat_Reset_m4394100 (PlayerPrefsSetFloat_t1786127731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetFloat::OnEnter()
extern "C"  void PlayerPrefsSetFloat_OnEnter_m3484825726 (PlayerPrefsSetFloat_t1786127731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
