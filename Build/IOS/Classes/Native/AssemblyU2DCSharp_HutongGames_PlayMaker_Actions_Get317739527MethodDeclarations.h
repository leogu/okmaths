﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetPosition
struct GetPosition_t317739527;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetPosition::.ctor()
extern "C"  void GetPosition__ctor_m2537635597 (GetPosition_t317739527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::Reset()
extern "C"  void GetPosition_Reset_m4115452296 (GetPosition_t317739527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::OnEnter()
extern "C"  void GetPosition_OnEnter_m2698622162 (GetPosition_t317739527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::OnUpdate()
extern "C"  void GetPosition_OnUpdate_m2023072785 (GetPosition_t317739527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPosition::DoGetPosition()
extern "C"  void GetPosition_DoGetPosition_m1898803121 (GetPosition_t317739527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
