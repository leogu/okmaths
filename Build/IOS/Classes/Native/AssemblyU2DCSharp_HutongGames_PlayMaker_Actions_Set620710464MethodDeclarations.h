﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorInt
struct SetAnimatorInt_t620710464;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::.ctor()
extern "C"  void SetAnimatorInt__ctor_m3964287030 (SetAnimatorInt_t620710464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::Reset()
extern "C"  void SetAnimatorInt_Reset_m463603245 (SetAnimatorInt_t620710464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::OnEnter()
extern "C"  void SetAnimatorInt_OnEnter_m3055034877 (SetAnimatorInt_t620710464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::OnActionUpdate()
extern "C"  void SetAnimatorInt_OnActionUpdate_m3051244578 (SetAnimatorInt_t620710464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorInt::SetParameter()
extern "C"  void SetAnimatorInt_SetParameter_m2100579519 (SetAnimatorInt_t620710464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
