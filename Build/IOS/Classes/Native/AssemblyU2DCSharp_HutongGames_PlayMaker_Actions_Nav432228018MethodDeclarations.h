﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer
struct NavMeshAgentAnimatorSynchronizer_t432228018;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::.ctor()
extern "C"  void NavMeshAgentAnimatorSynchronizer__ctor_m1547657016 (NavMeshAgentAnimatorSynchronizer_t432228018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::Reset()
extern "C"  void NavMeshAgentAnimatorSynchronizer_Reset_m107198943 (NavMeshAgentAnimatorSynchronizer_t432228018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::OnPreprocess()
extern "C"  void NavMeshAgentAnimatorSynchronizer_OnPreprocess_m684957327 (NavMeshAgentAnimatorSynchronizer_t432228018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::OnEnter()
extern "C"  void NavMeshAgentAnimatorSynchronizer_OnEnter_m1644572975 (NavMeshAgentAnimatorSynchronizer_t432228018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NavMeshAgentAnimatorSynchronizer::DoAnimatorMove()
extern "C"  void NavMeshAgentAnimatorSynchronizer_DoAnimatorMove_m3959863727 (NavMeshAgentAnimatorSynchronizer_t432228018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
