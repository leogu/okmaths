﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>
struct KeyCollection_t1844499377;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3655968902;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2050505044.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m331708235_gshared (KeyCollection_t1844499377 * __this, Dictionary_2_t3655968902 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m331708235(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1844499377 *, Dictionary_2_t3655968902 *, const MethodInfo*))KeyCollection__ctor_m331708235_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m453544545_gshared (KeyCollection_t1844499377 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m453544545(__this, ___item0, method) ((  void (*) (KeyCollection_t1844499377 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m453544545_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1228884376_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1228884376(__this, method) ((  void (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1228884376_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m877625383_gshared (KeyCollection_t1844499377 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m877625383(__this, ___item0, method) ((  bool (*) (KeyCollection_t1844499377 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m877625383_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2202014108_gshared (KeyCollection_t1844499377 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2202014108(__this, ___item0, method) ((  bool (*) (KeyCollection_t1844499377 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2202014108_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3038075768_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3038075768(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3038075768_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1903150270_gshared (KeyCollection_t1844499377 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1903150270(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1844499377 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1903150270_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2155406921_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2155406921(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2155406921_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m353679046_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m353679046(__this, method) ((  bool (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m353679046_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3434268198_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3434268198(__this, method) ((  bool (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3434268198_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m915026728_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m915026728(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m915026728_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4264182144_gshared (KeyCollection_t1844499377 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4264182144(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1844499377 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4264182144_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Enumerator_t2050505044  KeyCollection_GetEnumerator_m3783561107_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3783561107(__this, method) ((  Enumerator_t2050505044  (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_GetEnumerator_m3783561107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1331222946_gshared (KeyCollection_t1844499377 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1331222946(__this, method) ((  int32_t (*) (KeyCollection_t1844499377 *, const MethodInfo*))KeyCollection_get_Count_m1331222946_gshared)(__this, method)
