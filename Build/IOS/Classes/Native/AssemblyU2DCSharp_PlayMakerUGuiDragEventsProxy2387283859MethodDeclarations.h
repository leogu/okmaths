﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiDragEventsProxy
struct PlayMakerUGuiDragEventsProxy_t2387283859;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void PlayMakerUGuiDragEventsProxy::.ctor()
extern "C"  void PlayMakerUGuiDragEventsProxy__ctor_m3723293036 (PlayMakerUGuiDragEventsProxy_t2387283859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiDragEventsProxy::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiDragEventsProxy_OnBeginDrag_m2115622608 (PlayMakerUGuiDragEventsProxy_t2387283859 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiDragEventsProxy::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiDragEventsProxy_OnDrag_m2630371483 (PlayMakerUGuiDragEventsProxy_t2387283859 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiDragEventsProxy::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiDragEventsProxy_OnEndDrag_m869808372 (PlayMakerUGuiDragEventsProxy_t2387283859 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
