﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkIsClient
struct NetworkIsClient_t536994873;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::.ctor()
extern "C"  void NetworkIsClient__ctor_m361162811 (NetworkIsClient_t536994873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::Reset()
extern "C"  void NetworkIsClient_Reset_m2094254550 (NetworkIsClient_t536994873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::OnEnter()
extern "C"  void NetworkIsClient_OnEnter_m3956130496 (NetworkIsClient_t536994873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsClient::DoCheckIsClient()
extern "C"  void NetworkIsClient_DoCheckIsClient_m1671472421 (NetworkIsClient_t536994873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
