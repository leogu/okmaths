﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.HelpUrlAttribute
struct HelpUrlAttribute_t504102850;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String HutongGames.PlayMaker.HelpUrlAttribute::get_Url()
extern "C"  String_t* HelpUrlAttribute_get_Url_m4142580574 (HelpUrlAttribute_t504102850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.HelpUrlAttribute::.ctor(System.String)
extern "C"  void HelpUrlAttribute__ctor_m2233512137 (HelpUrlAttribute_t504102850 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
