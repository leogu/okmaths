﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate
struct uGuiInputfieldSetCaretBlinkRate_t4166640759;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate::.ctor()
extern "C"  void uGuiInputfieldSetCaretBlinkRate__ctor_m3437463507 (uGuiInputfieldSetCaretBlinkRate_t4166640759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate::Reset()
extern "C"  void uGuiInputfieldSetCaretBlinkRate_Reset_m2260414800 (uGuiInputfieldSetCaretBlinkRate_t4166640759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate::OnEnter()
extern "C"  void uGuiInputfieldSetCaretBlinkRate_OnEnter_m397820390 (uGuiInputfieldSetCaretBlinkRate_t4166640759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate::OnUpdate()
extern "C"  void uGuiInputfieldSetCaretBlinkRate_OnUpdate_m429798571 (uGuiInputfieldSetCaretBlinkRate_t4166640759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate::DoSetValue()
extern "C"  void uGuiInputfieldSetCaretBlinkRate_DoSetValue_m625541641 (uGuiInputfieldSetCaretBlinkRate_t4166640759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputfieldSetCaretBlinkRate::OnExit()
extern "C"  void uGuiInputfieldSetCaretBlinkRate_OnExit_m2211518614 (uGuiInputfieldSetCaretBlinkRate_t4166640759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
