﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetVertexColors
struct ArrayListGetVertexColors_t400366347;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexColors::.ctor()
extern "C"  void ArrayListGetVertexColors__ctor_m2213149909 (ArrayListGetVertexColors_t400366347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexColors::Reset()
extern "C"  void ArrayListGetVertexColors_Reset_m3287366460 (ArrayListGetVertexColors_t400366347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexColors::OnEnter()
extern "C"  void ArrayListGetVertexColors_OnEnter_m1282719222 (ArrayListGetVertexColors_t400366347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexColors::getVertexColors()
extern "C"  void ArrayListGetVertexColors_getVertexColors_m1473070461 (ArrayListGetVertexColors_t400366347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
