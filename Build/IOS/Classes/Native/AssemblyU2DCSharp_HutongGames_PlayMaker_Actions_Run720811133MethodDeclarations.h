﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RunFSMAction
struct RunFSMAction_t720811133;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::.ctor()
extern "C"  void RunFSMAction__ctor_m1919854561 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::Reset()
extern "C"  void RunFSMAction_Reset_m842924354 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.RunFSMAction::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool RunFSMAction_Event_m677726639 (RunFSMAction_t720811133 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnEnter()
extern "C"  void RunFSMAction_OnEnter_m1881808264 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnUpdate()
extern "C"  void RunFSMAction_OnUpdate_m2982491589 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnFixedUpdate()
extern "C"  void RunFSMAction_OnFixedUpdate_m2088576487 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnLateUpdate()
extern "C"  void RunFSMAction_OnLateUpdate_m3103257409 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void RunFSMAction_DoTriggerEnter_m493813307 (RunFSMAction_t720811133 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerStay(UnityEngine.Collider)
extern "C"  void RunFSMAction_DoTriggerStay_m1562750898 (RunFSMAction_t720811133 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerExit(UnityEngine.Collider)
extern "C"  void RunFSMAction_DoTriggerExit_m976147767 (RunFSMAction_t720811133 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void RunFSMAction_DoCollisionEnter_m2285247269 (RunFSMAction_t720811133 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionStay(UnityEngine.Collision)
extern "C"  void RunFSMAction_DoCollisionStay_m1535880480 (RunFSMAction_t720811133 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionExit(UnityEngine.Collision)
extern "C"  void RunFSMAction_DoCollisionExit_m781549773 (RunFSMAction_t720811133 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoParticleCollision(UnityEngine.GameObject)
extern "C"  void RunFSMAction_DoParticleCollision_m3648415010 (RunFSMAction_t720811133 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void RunFSMAction_DoControllerColliderHit_m1283032413 (RunFSMAction_t720811133 * __this, ControllerColliderHit_t4070855101 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void RunFSMAction_DoTriggerEnter2D_m2462700219 (RunFSMAction_t720811133 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void RunFSMAction_DoTriggerStay2D_m1816540878 (RunFSMAction_t720811133 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void RunFSMAction_DoTriggerExit2D_m1937701879 (RunFSMAction_t720811133 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void RunFSMAction_DoCollisionEnter2D_m2096631557 (RunFSMAction_t720811133 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void RunFSMAction_DoCollisionStay2D_m2230195840 (RunFSMAction_t720811133 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::DoCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void RunFSMAction_DoCollisionExit2D_m4143467181 (RunFSMAction_t720811133 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnGUI()
extern "C"  void RunFSMAction_OnGUI_m455929691 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::OnExit()
extern "C"  void RunFSMAction_OnExit_m2915330164 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RunFSMAction::CheckIfFinished()
extern "C"  void RunFSMAction_CheckIfFinished_m1247002192 (RunFSMAction_t720811133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
