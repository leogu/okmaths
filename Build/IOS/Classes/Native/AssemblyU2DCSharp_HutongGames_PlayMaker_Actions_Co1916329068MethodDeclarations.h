﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CollisionEvent
struct CollisionEvent_t1916329068;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::.ctor()
extern "C"  void CollisionEvent__ctor_m1724138760 (CollisionEvent_t1916329068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::Reset()
extern "C"  void CollisionEvent_Reset_m3171061169 (CollisionEvent_t1916329068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::OnPreprocess()
extern "C"  void CollisionEvent_OnPreprocess_m2884469489 (CollisionEvent_t1916329068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::StoreCollisionInfo(UnityEngine.Collision)
extern "C"  void CollisionEvent_StoreCollisionInfo_m3041322362 (CollisionEvent_t1916329068 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void CollisionEvent_DoCollisionEnter_m3852971526 (CollisionEvent_t1916329068 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoCollisionStay(UnityEngine.Collision)
extern "C"  void CollisionEvent_DoCollisionStay_m3490862799 (CollisionEvent_t1916329068 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoCollisionExit(UnityEngine.Collision)
extern "C"  void CollisionEvent_DoCollisionExit_m676216348 (CollisionEvent_t1916329068 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void CollisionEvent_DoControllerColliderHit_m3779854308 (CollisionEvent_t1916329068 * __this, ControllerColliderHit_t4070855101 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CollisionEvent::DoParticleCollision(UnityEngine.GameObject)
extern "C"  void CollisionEvent_DoParticleCollision_m3025748297 (CollisionEvent_t1916329068 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.CollisionEvent::ErrorCheck()
extern "C"  String_t* CollisionEvent_ErrorCheck_m180659983 (CollisionEvent_t1916329068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
