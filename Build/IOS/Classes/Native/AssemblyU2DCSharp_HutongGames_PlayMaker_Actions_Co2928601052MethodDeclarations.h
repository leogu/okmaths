﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertMaterialToObject
struct ConvertMaterialToObject_t2928601052;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::.ctor()
extern "C"  void ConvertMaterialToObject__ctor_m4176739270 (ConvertMaterialToObject_t2928601052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::Reset()
extern "C"  void ConvertMaterialToObject_Reset_m2928929113 (ConvertMaterialToObject_t2928601052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::OnEnter()
extern "C"  void ConvertMaterialToObject_OnEnter_m4219440785 (ConvertMaterialToObject_t2928601052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::OnUpdate()
extern "C"  void ConvertMaterialToObject_OnUpdate_m2417136384 (ConvertMaterialToObject_t2928601052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertMaterialToObject::DoConvertMaterialToObject()
extern "C"  void ConvertMaterialToObject_DoConvertMaterialToObject_m681352445 (ConvertMaterialToObject_t2928601052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
