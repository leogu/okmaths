﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatCompare
struct FloatCompare_t2851023783;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatCompare::.ctor()
extern "C"  void FloatCompare__ctor_m2583968389 (FloatCompare_t2851023783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::Reset()
extern "C"  void FloatCompare_Reset_m254350452 (FloatCompare_t2851023783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnEnter()
extern "C"  void FloatCompare_OnEnter_m714098270 (FloatCompare_t2851023783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::OnUpdate()
extern "C"  void FloatCompare_OnUpdate_m1329533849 (FloatCompare_t2851023783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatCompare::DoCompare()
extern "C"  void FloatCompare_DoCompare_m2829935609 (FloatCompare_t2851023783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.FloatCompare::ErrorCheck()
extern "C"  String_t* FloatCompare_ErrorCheck_m3497971908 (FloatCompare_t2851023783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
