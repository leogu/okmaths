﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MatchTargetWeightMask
struct MatchTargetWeightMask_t296470556;
struct MatchTargetWeightMask_t296470556_marshaled_pinvoke;
struct MatchTargetWeightMask_t296470556_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask296470556.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UnityEngine.MatchTargetWeightMask::.ctor(UnityEngine.Vector3,System.Single)
extern "C"  void MatchTargetWeightMask__ctor_m3489090191 (MatchTargetWeightMask_t296470556 * __this, Vector3_t2243707580  ___positionXYZWeight0, float ___rotationWeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct MatchTargetWeightMask_t296470556;
struct MatchTargetWeightMask_t296470556_marshaled_pinvoke;

extern "C" void MatchTargetWeightMask_t296470556_marshal_pinvoke(const MatchTargetWeightMask_t296470556& unmarshaled, MatchTargetWeightMask_t296470556_marshaled_pinvoke& marshaled);
extern "C" void MatchTargetWeightMask_t296470556_marshal_pinvoke_back(const MatchTargetWeightMask_t296470556_marshaled_pinvoke& marshaled, MatchTargetWeightMask_t296470556& unmarshaled);
extern "C" void MatchTargetWeightMask_t296470556_marshal_pinvoke_cleanup(MatchTargetWeightMask_t296470556_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct MatchTargetWeightMask_t296470556;
struct MatchTargetWeightMask_t296470556_marshaled_com;

extern "C" void MatchTargetWeightMask_t296470556_marshal_com(const MatchTargetWeightMask_t296470556& unmarshaled, MatchTargetWeightMask_t296470556_marshaled_com& marshaled);
extern "C" void MatchTargetWeightMask_t296470556_marshal_com_back(const MatchTargetWeightMask_t296470556_marshaled_com& marshaled, MatchTargetWeightMask_t296470556& unmarshaled);
extern "C" void MatchTargetWeightMask_t296470556_marshal_com_cleanup(MatchTargetWeightMask_t296470556_marshaled_com& marshaled);
