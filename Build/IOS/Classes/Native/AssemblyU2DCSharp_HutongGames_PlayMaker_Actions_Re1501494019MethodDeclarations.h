﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition
struct RectTransformGetAnchoredPosition_t1501494019;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::.ctor()
extern "C"  void RectTransformGetAnchoredPosition__ctor_m2137166545 (RectTransformGetAnchoredPosition_t1501494019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::Reset()
extern "C"  void RectTransformGetAnchoredPosition_Reset_m1061595336 (RectTransformGetAnchoredPosition_t1501494019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::OnEnter()
extern "C"  void RectTransformGetAnchoredPosition_OnEnter_m1081742522 (RectTransformGetAnchoredPosition_t1501494019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::OnActionUpdate()
extern "C"  void RectTransformGetAnchoredPosition_OnActionUpdate_m762279063 (RectTransformGetAnchoredPosition_t1501494019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchoredPosition::DoGetValues()
extern "C"  void RectTransformGetAnchoredPosition_DoGetValues_m1064686774 (RectTransformGetAnchoredPosition_t1501494019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
