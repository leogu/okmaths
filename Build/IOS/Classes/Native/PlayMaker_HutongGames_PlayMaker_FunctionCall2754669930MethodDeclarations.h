﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t2754669930;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall2754669930.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.FunctionCall::.ctor()
extern "C"  void FunctionCall__ctor_m196414445 (FunctionCall_t2754669930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FunctionCall::.ctor(HutongGames.PlayMaker.FunctionCall)
extern "C"  void FunctionCall__ctor_m1739832155 (FunctionCall_t2754669930 * __this, FunctionCall_t2754669930 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FunctionCall::ResetParameters()
extern "C"  void FunctionCall_ResetParameters_m4159129772 (FunctionCall_t2754669930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FunctionCall::get_ParameterType()
extern "C"  String_t* FunctionCall_get_ParameterType_m2201785492 (FunctionCall_t2754669930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FunctionCall::set_ParameterType(System.String)
extern "C"  void FunctionCall_set_ParameterType_m3080740811 (FunctionCall_t2754669930 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
