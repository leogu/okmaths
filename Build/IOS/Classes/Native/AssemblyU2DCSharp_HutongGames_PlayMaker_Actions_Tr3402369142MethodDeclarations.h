﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TriggerEvent
struct TriggerEvent_t3402369142;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"

// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::.ctor()
extern "C"  void TriggerEvent__ctor_m32969684 (TriggerEvent_t3402369142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::Reset()
extern "C"  void TriggerEvent_Reset_m2880206595 (TriggerEvent_t3402369142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::OnPreprocess()
extern "C"  void TriggerEvent_OnPreprocess_m466436683 (TriggerEvent_t3402369142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::StoreCollisionInfo(UnityEngine.Collider)
extern "C"  void TriggerEvent_StoreCollisionInfo_m2531173636 (TriggerEvent_t3402369142 * __this, Collider_t3497673348 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void TriggerEvent_DoTriggerEnter_m288985460 (TriggerEvent_t3402369142 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::DoTriggerStay(UnityEngine.Collider)
extern "C"  void TriggerEvent_DoTriggerStay_m2569168883 (TriggerEvent_t3402369142 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TriggerEvent::DoTriggerExit(UnityEngine.Collider)
extern "C"  void TriggerEvent_DoTriggerExit_m1216983150 (TriggerEvent_t3402369142 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.TriggerEvent::ErrorCheck()
extern "C"  String_t* TriggerEvent_ErrorCheck_m3675857065 (TriggerEvent_t3402369142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
