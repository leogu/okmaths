﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d
struct GetNextOverlapPoint2d_t137316342;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::.ctor()
extern "C"  void GetNextOverlapPoint2d__ctor_m1148889362 (GetNextOverlapPoint2d_t137316342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::Reset()
extern "C"  void GetNextOverlapPoint2d_Reset_m2526075087 (GetNextOverlapPoint2d_t137316342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::OnEnter()
extern "C"  void GetNextOverlapPoint2d_OnEnter_m4002943431 (GetNextOverlapPoint2d_t137316342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::DoGetNextCollider()
extern "C"  void GetNextOverlapPoint2d_DoGetNextCollider_m425263928 (GetNextOverlapPoint2d_t137316342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D[] HutongGames.PlayMaker.Actions.GetNextOverlapPoint2d::GetOverlapPointAll()
extern "C"  Collider2DU5BU5D_t3535523695* GetNextOverlapPoint2d_GetOverlapPointAll_m750318590 (GetNextOverlapPoint2d_t137316342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
