﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues
struct  uGuiLayoutElementSetValues_t2474063466  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::minWidth
	FsmFloat_t937133978 * ___minWidth_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::minHeight
	FsmFloat_t937133978 * ___minHeight_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::preferredWidth
	FsmFloat_t937133978 * ___preferredWidth_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::preferredHeight
	FsmFloat_t937133978 * ___preferredHeight_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::flexibleWidth
	FsmFloat_t937133978 * ___flexibleWidth_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::flexibleHeight
	FsmFloat_t937133978 * ___flexibleHeight_17;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.LayoutElement HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::_layoutElement
	LayoutElement_t2808691390 * ____layoutElement_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_minWidth_12() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___minWidth_12)); }
	inline FsmFloat_t937133978 * get_minWidth_12() const { return ___minWidth_12; }
	inline FsmFloat_t937133978 ** get_address_of_minWidth_12() { return &___minWidth_12; }
	inline void set_minWidth_12(FsmFloat_t937133978 * value)
	{
		___minWidth_12 = value;
		Il2CppCodeGenWriteBarrier(&___minWidth_12, value);
	}

	inline static int32_t get_offset_of_minHeight_13() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___minHeight_13)); }
	inline FsmFloat_t937133978 * get_minHeight_13() const { return ___minHeight_13; }
	inline FsmFloat_t937133978 ** get_address_of_minHeight_13() { return &___minHeight_13; }
	inline void set_minHeight_13(FsmFloat_t937133978 * value)
	{
		___minHeight_13 = value;
		Il2CppCodeGenWriteBarrier(&___minHeight_13, value);
	}

	inline static int32_t get_offset_of_preferredWidth_14() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___preferredWidth_14)); }
	inline FsmFloat_t937133978 * get_preferredWidth_14() const { return ___preferredWidth_14; }
	inline FsmFloat_t937133978 ** get_address_of_preferredWidth_14() { return &___preferredWidth_14; }
	inline void set_preferredWidth_14(FsmFloat_t937133978 * value)
	{
		___preferredWidth_14 = value;
		Il2CppCodeGenWriteBarrier(&___preferredWidth_14, value);
	}

	inline static int32_t get_offset_of_preferredHeight_15() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___preferredHeight_15)); }
	inline FsmFloat_t937133978 * get_preferredHeight_15() const { return ___preferredHeight_15; }
	inline FsmFloat_t937133978 ** get_address_of_preferredHeight_15() { return &___preferredHeight_15; }
	inline void set_preferredHeight_15(FsmFloat_t937133978 * value)
	{
		___preferredHeight_15 = value;
		Il2CppCodeGenWriteBarrier(&___preferredHeight_15, value);
	}

	inline static int32_t get_offset_of_flexibleWidth_16() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___flexibleWidth_16)); }
	inline FsmFloat_t937133978 * get_flexibleWidth_16() const { return ___flexibleWidth_16; }
	inline FsmFloat_t937133978 ** get_address_of_flexibleWidth_16() { return &___flexibleWidth_16; }
	inline void set_flexibleWidth_16(FsmFloat_t937133978 * value)
	{
		___flexibleWidth_16 = value;
		Il2CppCodeGenWriteBarrier(&___flexibleWidth_16, value);
	}

	inline static int32_t get_offset_of_flexibleHeight_17() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___flexibleHeight_17)); }
	inline FsmFloat_t937133978 * get_flexibleHeight_17() const { return ___flexibleHeight_17; }
	inline FsmFloat_t937133978 ** get_address_of_flexibleHeight_17() { return &___flexibleHeight_17; }
	inline void set_flexibleHeight_17(FsmFloat_t937133978 * value)
	{
		___flexibleHeight_17 = value;
		Il2CppCodeGenWriteBarrier(&___flexibleHeight_17, value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of__layoutElement_19() { return static_cast<int32_t>(offsetof(uGuiLayoutElementSetValues_t2474063466, ____layoutElement_19)); }
	inline LayoutElement_t2808691390 * get__layoutElement_19() const { return ____layoutElement_19; }
	inline LayoutElement_t2808691390 ** get_address_of__layoutElement_19() { return &____layoutElement_19; }
	inline void set__layoutElement_19(LayoutElement_t2808691390 * value)
	{
		____layoutElement_19 = value;
		Il2CppCodeGenWriteBarrier(&____layoutElement_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
