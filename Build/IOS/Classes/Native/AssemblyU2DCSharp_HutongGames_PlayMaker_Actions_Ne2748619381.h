﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkIsServer
struct  NetworkIsServer_t2748619381  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.NetworkIsServer::isServer
	FsmBool_t664485696 * ___isServer_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkIsServer::isServerEvent
	FsmEvent_t1258573736 * ___isServerEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.NetworkIsServer::isNotServerEvent
	FsmEvent_t1258573736 * ___isNotServerEvent_13;

public:
	inline static int32_t get_offset_of_isServer_11() { return static_cast<int32_t>(offsetof(NetworkIsServer_t2748619381, ___isServer_11)); }
	inline FsmBool_t664485696 * get_isServer_11() const { return ___isServer_11; }
	inline FsmBool_t664485696 ** get_address_of_isServer_11() { return &___isServer_11; }
	inline void set_isServer_11(FsmBool_t664485696 * value)
	{
		___isServer_11 = value;
		Il2CppCodeGenWriteBarrier(&___isServer_11, value);
	}

	inline static int32_t get_offset_of_isServerEvent_12() { return static_cast<int32_t>(offsetof(NetworkIsServer_t2748619381, ___isServerEvent_12)); }
	inline FsmEvent_t1258573736 * get_isServerEvent_12() const { return ___isServerEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_isServerEvent_12() { return &___isServerEvent_12; }
	inline void set_isServerEvent_12(FsmEvent_t1258573736 * value)
	{
		___isServerEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___isServerEvent_12, value);
	}

	inline static int32_t get_offset_of_isNotServerEvent_13() { return static_cast<int32_t>(offsetof(NetworkIsServer_t2748619381, ___isNotServerEvent_13)); }
	inline FsmEvent_t1258573736 * get_isNotServerEvent_13() const { return ___isNotServerEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotServerEvent_13() { return &___isNotServerEvent_13; }
	inline void set_isNotServerEvent_13(FsmEvent_t1258573736 * value)
	{
		___isNotServerEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isNotServerEvent_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
