﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectChanged
struct GameObjectChanged_t3258813155;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::.ctor()
extern "C"  void GameObjectChanged__ctor_m4006693819 (GameObjectChanged_t3258813155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::Reset()
extern "C"  void GameObjectChanged_Reset_m3718563384 (GameObjectChanged_t3258813155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnEnter()
extern "C"  void GameObjectChanged_OnEnter_m1163061134 (GameObjectChanged_t3258813155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectChanged::OnUpdate()
extern "C"  void GameObjectChanged_OnUpdate_m985921603 (GameObjectChanged_t3258813155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
