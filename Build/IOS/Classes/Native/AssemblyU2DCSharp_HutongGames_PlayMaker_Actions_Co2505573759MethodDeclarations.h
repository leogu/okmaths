﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertBoolToString
struct ConvertBoolToString_t2505573759;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::.ctor()
extern "C"  void ConvertBoolToString__ctor_m3970937845 (ConvertBoolToString_t2505573759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::Reset()
extern "C"  void ConvertBoolToString_Reset_m1253786752 (ConvertBoolToString_t2505573759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::OnEnter()
extern "C"  void ConvertBoolToString_OnEnter_m2753496778 (ConvertBoolToString_t2505573759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::OnUpdate()
extern "C"  void ConvertBoolToString_OnUpdate_m17790233 (ConvertBoolToString_t2505573759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertBoolToString::DoConvertBoolToString()
extern "C"  void ConvertBoolToString_DoConvertBoolToString_m419773169 (ConvertBoolToString_t2505573759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
