﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionLookRotation
struct QuaternionLookRotation_t1032259027;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::.ctor()
extern "C"  void QuaternionLookRotation__ctor_m2722178133 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::Reset()
extern "C"  void QuaternionLookRotation_Reset_m759548700 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnEnter()
extern "C"  void QuaternionLookRotation_OnEnter_m1895982526 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnUpdate()
extern "C"  void QuaternionLookRotation_OnUpdate_m566509641 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnLateUpdate()
extern "C"  void QuaternionLookRotation_OnLateUpdate_m449339957 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::OnFixedUpdate()
extern "C"  void QuaternionLookRotation_OnFixedUpdate_m2853733407 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLookRotation::DoQuatLookRotation()
extern "C"  void QuaternionLookRotation_DoQuatLookRotation_m2005680284 (QuaternionLookRotation_t1032259027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
