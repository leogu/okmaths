﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween50_U3CModuleU3E3783534214.h"
#include "DOTween50_DG_Tweening_ShortcutExtensions501867980067.h"
#include "DOTween50_DG_Tweening_ShortcutExtensions50_U3CU3Ec3395645806.h"
#include "DOTweenPro_U3CModuleU3E3783534214.h"
#include "DOTweenPro_DG_Tweening_DOTweenVisualManager2945673405.h"
#include "DOTweenPro_DG_Tweening_HandlesDrawMode3273484032.h"
#include "DOTweenPro_DG_Tweening_HandlesType3201532857.h"
#include "DOTweenPro_DG_Tweening_DOTweenInspectorMode2739551672.h"
#include "DOTweenPro_DG_Tweening_DOTweenPath1397145371.h"
#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "DOTweenPro_DG_Tweening_Core_DOTweenAnimationType119935370.h"
#include "DOTweenPro_DG_Tweening_Core_OnDisableBehaviour125315118.h"
#include "DOTweenPro_DG_Tweening_Core_OnEnableBehaviour285142911.h"
#include "DOTweenPro_DG_Tweening_Core_TargetType2706200073.h"
#include "DOTweenPro_DG_Tweening_Core_VisualManagerPreset4087939440.h"
#include "PlayMaker_U3CModuleU3E3783534214.h"
#include "PlayMaker_HutongGames_Extensions_RectExtensions388407962.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions2165146299.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions_627548518.h"
#include "PlayMaker_HutongGames_Utility_ColorUtils3046161770.h"
#include "PlayMaker_HutongGames_Utility_StringUtils3308261572.h"
#include "PlayMaker_HutongGames_PlayMaker_CollisionType2558904546.h"
#include "PlayMaker_HutongGames_PlayMaker_TriggerType3618183354.h"
#include "PlayMaker_HutongGames_PlayMaker_Collision2DType280604644.h"
#include "PlayMaker_HutongGames_PlayMaker_Trigger2DType4037242640.h"
#include "PlayMaker_HutongGames_PlayMaker_InterpolationType1288321096.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseEventType2419436389.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategory928321528.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHint658807571.h"
#include "PlayMaker_HutongGames_PlayMaker_MouseButton1622948991.h"
#include "PlayMaker_HutongGames_PlayMaker_LogLevel3809977218.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionHelpers246470375.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport4101412914.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport_U3CU33039022016.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionTarget967701655.h"
#include "PlayMaker_HutongGames_PlayMaker_NoActionTargetsAtt1534437589.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategoryAttr2497102516.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionSection2860151895.h"
#include "PlayMaker_HutongGames_PlayMaker_ArrayEditorAttribut966672038.h"
#include "PlayMaker_HutongGames_PlayMaker_CheckForComponentA1147058572.h"
#include "PlayMaker_HutongGames_PlayMaker_CompoundArrayAttri3340422024.h"
#include "PlayMaker_HutongGames_PlayMaker_EventTargetAttribu2569422301.h"
#include "PlayMaker_HutongGames_PlayMaker_HasFloatSliderAttr1179041011.h"
#include "PlayMaker_HutongGames_PlayMaker_HelpUrlAttribute504102850.h"
#include "PlayMaker_HutongGames_PlayMaker_HideTypeFilter1036096420.h"
#include "PlayMaker_HutongGames_PlayMaker_MatchElementTypeAt1512058421.h"
#include "PlayMaker_HutongGames_PlayMaker_MatchFieldTypeAttr2336588201.h"
#include "PlayMaker_HutongGames_PlayMaker_NoteAttribute3148576718.h"
#include "PlayMaker_HutongGames_PlayMaker_ObjectTypeAttribut2646420733.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeAttrib3909644206.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeFilter1678954972.h"
#include "PlayMaker_HutongGames_PlayMaker_RequiredFieldAttri4283157755.h"
#include "PlayMaker_HutongGames_PlayMaker_TitleAttribute1952452400.h"
#include "PlayMaker_HutongGames_PlayMaker_TooltipAttribute714119257.h"
#include "PlayMaker_HutongGames_PlayMaker_UIHintAttribute1878072735.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl924276959.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl_841294727.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget_Eve3694600655.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride639182869.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall2754669930.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption3699592477.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption_Layou4193526299.h"
#include "PlayMaker_HutongGames_PlayMaker_DebugUtils810358730.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1624700828.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmDebugUtility882664281.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData2110469976.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmExecutionStack503928862.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty786753495.h"
#include "PlayMaker_FsmTemplate1285897084.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTime334847885.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmAnimationCurve326747561.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray527459893.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool664485696.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor118301965.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum2808516103.h"
#include "PlayMaker_HutongGames_PlayMaker_None2771209004.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject3097142863.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1273009179.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject2785794313.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial1421632035.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault2023674184.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption3848444473.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion878438756.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect19023354.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString2414474701.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3372293163.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar2872592513.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector22430450063.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector33996534004.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData1467934444.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData_Context2765413152.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (ShortcutExtensions50_t1867980067), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CU3Ec__DisplayClass0_0_t3395645806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3395645806::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass0_0_t3395645806::get_offset_of_floatName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (DOTweenVisualManager_t2945673405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[4] = 
{
	DOTweenVisualManager_t2945673405::get_offset_of_preset_2(),
	DOTweenVisualManager_t2945673405::get_offset_of_onEnableBehaviour_3(),
	DOTweenVisualManager_t2945673405::get_offset_of_onDisableBehaviour_4(),
	DOTweenVisualManager_t2945673405::get_offset_of__requiresRestartFromSpawnPoint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (HandlesDrawMode_t3273484032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2005[3] = 
{
	HandlesDrawMode_t3273484032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (HandlesType_t3201532857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2006[3] = 
{
	HandlesType_t3201532857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (DOTweenInspectorMode_t2739551672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2007[3] = 
{
	DOTweenInspectorMode_t2739551672::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (DOTweenPath_t1397145371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[36] = 
{
	DOTweenPath_t1397145371::get_offset_of_delay_17(),
	DOTweenPath_t1397145371::get_offset_of_duration_18(),
	DOTweenPath_t1397145371::get_offset_of_easeType_19(),
	DOTweenPath_t1397145371::get_offset_of_easeCurve_20(),
	DOTweenPath_t1397145371::get_offset_of_loops_21(),
	DOTweenPath_t1397145371::get_offset_of_id_22(),
	DOTweenPath_t1397145371::get_offset_of_loopType_23(),
	DOTweenPath_t1397145371::get_offset_of_orientType_24(),
	DOTweenPath_t1397145371::get_offset_of_lookAtTransform_25(),
	DOTweenPath_t1397145371::get_offset_of_lookAtPosition_26(),
	DOTweenPath_t1397145371::get_offset_of_lookAhead_27(),
	DOTweenPath_t1397145371::get_offset_of_autoPlay_28(),
	DOTweenPath_t1397145371::get_offset_of_autoKill_29(),
	DOTweenPath_t1397145371::get_offset_of_relative_30(),
	DOTweenPath_t1397145371::get_offset_of_isLocal_31(),
	DOTweenPath_t1397145371::get_offset_of_isClosedPath_32(),
	DOTweenPath_t1397145371::get_offset_of_pathResolution_33(),
	DOTweenPath_t1397145371::get_offset_of_pathMode_34(),
	DOTweenPath_t1397145371::get_offset_of_lockRotation_35(),
	DOTweenPath_t1397145371::get_offset_of_assignForwardAndUp_36(),
	DOTweenPath_t1397145371::get_offset_of_forwardDirection_37(),
	DOTweenPath_t1397145371::get_offset_of_upDirection_38(),
	DOTweenPath_t1397145371::get_offset_of_wps_39(),
	DOTweenPath_t1397145371::get_offset_of_fullWps_40(),
	DOTweenPath_t1397145371::get_offset_of_path_41(),
	DOTweenPath_t1397145371::get_offset_of_inspectorMode_42(),
	DOTweenPath_t1397145371::get_offset_of_pathType_43(),
	DOTweenPath_t1397145371::get_offset_of_handlesType_44(),
	DOTweenPath_t1397145371::get_offset_of_livePreview_45(),
	DOTweenPath_t1397145371::get_offset_of_handlesDrawMode_46(),
	DOTweenPath_t1397145371::get_offset_of_perspectiveHandleSize_47(),
	DOTweenPath_t1397145371::get_offset_of_showIndexes_48(),
	DOTweenPath_t1397145371::get_offset_of_showWpLength_49(),
	DOTweenPath_t1397145371::get_offset_of_pathColor_50(),
	DOTweenPath_t1397145371::get_offset_of_lastSrcPosition_51(),
	DOTweenPath_t1397145371::get_offset_of_wpsDropdown_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (ABSAnimationComponent_t2205594551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[15] = 
{
	ABSAnimationComponent_t2205594551::get_offset_of_updateType_2(),
	ABSAnimationComponent_t2205594551::get_offset_of_isSpeedBased_3(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStart_4(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnPlay_5(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnUpdate_6(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStepComplete_7(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnComplete_8(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnTweenCreated_9(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStart_10(),
	ABSAnimationComponent_t2205594551::get_offset_of_onPlay_11(),
	ABSAnimationComponent_t2205594551::get_offset_of_onUpdate_12(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStepComplete_13(),
	ABSAnimationComponent_t2205594551::get_offset_of_onComplete_14(),
	ABSAnimationComponent_t2205594551::get_offset_of_onTweenCreated_15(),
	ABSAnimationComponent_t2205594551::get_offset_of_tween_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (DOTweenAnimationType_t119935370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[23] = 
{
	DOTweenAnimationType_t119935370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (OnDisableBehaviour_t125315118)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2011[7] = 
{
	OnDisableBehaviour_t125315118::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (OnEnableBehaviour_t285142911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[5] = 
{
	OnEnableBehaviour_t285142911::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (TargetType_t2706200073)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2013[17] = 
{
	TargetType_t2706200073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (VisualManagerPreset_t4087939440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[3] = 
{
	VisualManagerPreset_t4087939440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (RectExtensions_t388407962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (TextureExtensions_t2165146299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Point_t627548518)+ sizeof (Il2CppObject), sizeof(Point_t627548518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[2] = 
{
	Point_t627548518::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Point_t627548518::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (ColorUtils_t3046161770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (StringUtils_t3308261572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (CollisionType_t2558904546)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2023[6] = 
{
	CollisionType_t2558904546::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (TriggerType_t3618183354)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2024[4] = 
{
	TriggerType_t3618183354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (Collision2DType_t280604644)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2025[5] = 
{
	Collision2DType_t280604644::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (Trigger2DType_t4037242640)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[4] = 
{
	Trigger2DType_t4037242640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (InterpolationType_t1288321096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2027[3] = 
{
	InterpolationType_t1288321096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (MouseEventType_t2419436389)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2028[7] = 
{
	MouseEventType_t2419436389::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (ActionCategory_t928321528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2029[54] = 
{
	ActionCategory_t928321528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (UIHint_t658807571)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2030[38] = 
{
	UIHint_t658807571::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (MouseButton_t1622948991)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2031[5] = 
{
	MouseButton_t1622948991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (LogLevel_t3809977218)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2032[4] = 
{
	LogLevel_t3809977218::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (ActionHelpers_t246470375), -1, sizeof(ActionHelpers_t246470375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2033[5] = 
{
	ActionHelpers_t246470375_StaticFields::get_offset_of_whiteTexture_0(),
	ActionHelpers_t246470375_StaticFields::get_offset_of_mousePickInfo_1(),
	ActionHelpers_t246470375_StaticFields::get_offset_of_mousePickRaycastTime_2(),
	ActionHelpers_t246470375_StaticFields::get_offset_of_mousePickDistanceUsed_3(),
	ActionHelpers_t246470375_StaticFields::get_offset_of_mousePickLayerMaskUsed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (ActionReport_t4101412914), -1, sizeof(ActionReport_t4101412914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2034[10] = 
{
	ActionReport_t4101412914_StaticFields::get_offset_of_ActionReportList_0(),
	ActionReport_t4101412914_StaticFields::get_offset_of_InfoCount_1(),
	ActionReport_t4101412914_StaticFields::get_offset_of_ErrorCount_2(),
	ActionReport_t4101412914::get_offset_of_fsm_3(),
	ActionReport_t4101412914::get_offset_of_state_4(),
	ActionReport_t4101412914::get_offset_of_action_5(),
	ActionReport_t4101412914::get_offset_of_actionIndex_6(),
	ActionReport_t4101412914::get_offset_of_logText_7(),
	ActionReport_t4101412914::get_offset_of_isError_8(),
	ActionReport_t4101412914::get_offset_of_parameter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (U3CU3Ec__DisplayClass2_t3039022016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[1] = 
{
	U3CU3Ec__DisplayClass2_t3039022016::get_offset_of_fsm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (ActionTarget_t967701655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[3] = 
{
	ActionTarget_t967701655::get_offset_of_objectType_0(),
	ActionTarget_t967701655::get_offset_of_fieldName_1(),
	ActionTarget_t967701655::get_offset_of_allowPrefabs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (NoActionTargetsAttribute_t1534437589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (ActionCategoryAttribute_t2497102516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[1] = 
{
	ActionCategoryAttribute_t2497102516::get_offset_of_category_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (ActionSection_t2860151895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2039[1] = 
{
	ActionSection_t2860151895::get_offset_of_section_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (ArrayEditorAttribute_t966672038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[6] = 
{
	ArrayEditorAttribute_t966672038::get_offset_of_variableType_0(),
	ArrayEditorAttribute_t966672038::get_offset_of_objectType_1(),
	ArrayEditorAttribute_t966672038::get_offset_of_elementName_2(),
	ArrayEditorAttribute_t966672038::get_offset_of_fixedSize_3(),
	ArrayEditorAttribute_t966672038::get_offset_of_maxSize_4(),
	ArrayEditorAttribute_t966672038::get_offset_of_minSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (CheckForComponentAttribute_t1147058572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[3] = 
{
	CheckForComponentAttribute_t1147058572::get_offset_of_type0_0(),
	CheckForComponentAttribute_t1147058572::get_offset_of_type1_1(),
	CheckForComponentAttribute_t1147058572::get_offset_of_type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (CompoundArrayAttribute_t3340422024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[3] = 
{
	CompoundArrayAttribute_t3340422024::get_offset_of_name_0(),
	CompoundArrayAttribute_t3340422024::get_offset_of_firstArrayName_1(),
	CompoundArrayAttribute_t3340422024::get_offset_of_secondArrayName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (EventTargetAttribute_t2569422301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	EventTargetAttribute_t2569422301::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (HasFloatSliderAttribute_t1179041011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[2] = 
{
	HasFloatSliderAttribute_t1179041011::get_offset_of_minValue_0(),
	HasFloatSliderAttribute_t1179041011::get_offset_of_maxValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (HelpUrlAttribute_t504102850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[1] = 
{
	HelpUrlAttribute_t504102850::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (HideTypeFilter_t1036096420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (MatchElementTypeAttribute_t1512058421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	MatchElementTypeAttribute_t1512058421::get_offset_of_fieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (MatchFieldTypeAttribute_t2336588201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	MatchFieldTypeAttribute_t2336588201::get_offset_of_fieldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (NoteAttribute_t3148576718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[1] = 
{
	NoteAttribute_t3148576718::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (ObjectTypeAttribute_t2646420733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[1] = 
{
	ObjectTypeAttribute_t2646420733::get_offset_of_objectType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (VariableTypeAttribute_t3909644206), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[1] = 
{
	VariableTypeAttribute_t3909644206::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (VariableTypeFilter_t1678954972), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (RequiredFieldAttribute_t4283157755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (TitleAttribute_t1952452400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[1] = 
{
	TitleAttribute_t1952452400::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (TooltipAttribute_t714119257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[1] = 
{
	TooltipAttribute_t714119257::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (UIHintAttribute_t1878072735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[1] = 
{
	UIHintAttribute_t1878072735::get_offset_of_hint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (FsmTemplateControl_t924276959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[4] = 
{
	FsmTemplateControl_t924276959::get_offset_of_fsmTemplate_0(),
	FsmTemplateControl_t924276959::get_offset_of_fsmVarOverrides_1(),
	FsmTemplateControl_t924276959::get_offset_of_runFsm_2(),
	FsmTemplateControl_t924276959::get_offset_of_U3CIDU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (U3CU3Ec__DisplayClass2_t841294727), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[1] = 
{
	U3CU3Ec__DisplayClass2_t841294727::get_offset_of_namedVariable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (FsmEventTarget_t172293745), -1, sizeof(FsmEventTarget_t172293745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2059[7] = 
{
	FsmEventTarget_t172293745_StaticFields::get_offset_of_self_0(),
	FsmEventTarget_t172293745::get_offset_of_target_1(),
	FsmEventTarget_t172293745::get_offset_of_excludeSelf_2(),
	FsmEventTarget_t172293745::get_offset_of_gameObject_3(),
	FsmEventTarget_t172293745::get_offset_of_fsmName_4(),
	FsmEventTarget_t172293745::get_offset_of_sendToChildren_5(),
	FsmEventTarget_t172293745::get_offset_of_fsmComponent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (EventTarget_t3694600655)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2060[8] = 
{
	EventTarget_t3694600655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (FsmVarOverride_t639182869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[3] = 
{
	FsmVarOverride_t639182869::get_offset_of_variable_0(),
	FsmVarOverride_t639182869::get_offset_of_fsmVar_1(),
	FsmVarOverride_t639182869::get_offset_of_isEdited_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (FunctionCall_t2754669930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[17] = 
{
	FunctionCall_t2754669930::get_offset_of_FunctionName_0(),
	FunctionCall_t2754669930::get_offset_of_parameterType_1(),
	FunctionCall_t2754669930::get_offset_of_BoolParameter_2(),
	FunctionCall_t2754669930::get_offset_of_FloatParameter_3(),
	FunctionCall_t2754669930::get_offset_of_IntParameter_4(),
	FunctionCall_t2754669930::get_offset_of_GameObjectParameter_5(),
	FunctionCall_t2754669930::get_offset_of_ObjectParameter_6(),
	FunctionCall_t2754669930::get_offset_of_StringParameter_7(),
	FunctionCall_t2754669930::get_offset_of_Vector2Parameter_8(),
	FunctionCall_t2754669930::get_offset_of_Vector3Parameter_9(),
	FunctionCall_t2754669930::get_offset_of_RectParamater_10(),
	FunctionCall_t2754669930::get_offset_of_QuaternionParameter_11(),
	FunctionCall_t2754669930::get_offset_of_MaterialParameter_12(),
	FunctionCall_t2754669930::get_offset_of_TextureParameter_13(),
	FunctionCall_t2754669930::get_offset_of_ColorParameter_14(),
	FunctionCall_t2754669930::get_offset_of_EnumParameter_15(),
	FunctionCall_t2754669930::get_offset_of_ArrayParameter_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (LayoutOption_t3699592477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[3] = 
{
	LayoutOption_t3699592477::get_offset_of_option_0(),
	LayoutOption_t3699592477::get_offset_of_floatParam_1(),
	LayoutOption_t3699592477::get_offset_of_boolParam_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (LayoutOptionType_t4193526299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2064[9] = 
{
	LayoutOptionType_t4193526299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (DebugUtils_t810358730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (DelayedEvent_t1624700828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[6] = 
{
	DelayedEvent_t1624700828::get_offset_of_fsm_0(),
	DelayedEvent_t1624700828::get_offset_of_fsmEvent_1(),
	DelayedEvent_t1624700828::get_offset_of_eventTarget_2(),
	DelayedEvent_t1624700828::get_offset_of_eventData_3(),
	DelayedEvent_t1624700828::get_offset_of_timer_4(),
	DelayedEvent_t1624700828::get_offset_of_eventFired_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (FsmDebugUtility_t882664281), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (FsmEventData_t2110469976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[21] = 
{
	FsmEventData_t2110469976::get_offset_of_SentByFsm_0(),
	FsmEventData_t2110469976::get_offset_of_SentByState_1(),
	FsmEventData_t2110469976::get_offset_of_SentByAction_2(),
	FsmEventData_t2110469976::get_offset_of_BoolData_3(),
	FsmEventData_t2110469976::get_offset_of_IntData_4(),
	FsmEventData_t2110469976::get_offset_of_FloatData_5(),
	FsmEventData_t2110469976::get_offset_of_Vector2Data_6(),
	FsmEventData_t2110469976::get_offset_of_Vector3Data_7(),
	FsmEventData_t2110469976::get_offset_of_StringData_8(),
	FsmEventData_t2110469976::get_offset_of_QuaternionData_9(),
	FsmEventData_t2110469976::get_offset_of_RectData_10(),
	FsmEventData_t2110469976::get_offset_of_ColorData_11(),
	FsmEventData_t2110469976::get_offset_of_ObjectData_12(),
	FsmEventData_t2110469976::get_offset_of_GameObjectData_13(),
	FsmEventData_t2110469976::get_offset_of_MaterialData_14(),
	FsmEventData_t2110469976::get_offset_of_TextureData_15(),
	FsmEventData_t2110469976::get_offset_of_Player_16(),
	FsmEventData_t2110469976::get_offset_of_DisconnectionInfo_17(),
	FsmEventData_t2110469976::get_offset_of_ConnectionError_18(),
	FsmEventData_t2110469976::get_offset_of_NetworkMessageInfo_19(),
	FsmEventData_t2110469976::get_offset_of_MasterServerEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (FsmExecutionStack_t503928862), -1, sizeof(FsmExecutionStack_t503928862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2069[2] = 
{
	FsmExecutionStack_t503928862_StaticFields::get_offset_of_fsmExecutionStack_0(),
	FsmExecutionStack_t503928862_StaticFields::get_offset_of_U3CMaxStackCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (FsmProperty_t786753495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[24] = 
{
	FsmProperty_t786753495::get_offset_of_TargetObject_0(),
	FsmProperty_t786753495::get_offset_of_TargetTypeName_1(),
	FsmProperty_t786753495::get_offset_of_TargetType_2(),
	FsmProperty_t786753495::get_offset_of_PropertyName_3(),
	FsmProperty_t786753495::get_offset_of_PropertyType_4(),
	FsmProperty_t786753495::get_offset_of_BoolParameter_5(),
	FsmProperty_t786753495::get_offset_of_FloatParameter_6(),
	FsmProperty_t786753495::get_offset_of_IntParameter_7(),
	FsmProperty_t786753495::get_offset_of_GameObjectParameter_8(),
	FsmProperty_t786753495::get_offset_of_StringParameter_9(),
	FsmProperty_t786753495::get_offset_of_Vector2Parameter_10(),
	FsmProperty_t786753495::get_offset_of_Vector3Parameter_11(),
	FsmProperty_t786753495::get_offset_of_RectParamater_12(),
	FsmProperty_t786753495::get_offset_of_QuaternionParameter_13(),
	FsmProperty_t786753495::get_offset_of_ObjectParameter_14(),
	FsmProperty_t786753495::get_offset_of_MaterialParameter_15(),
	FsmProperty_t786753495::get_offset_of_TextureParameter_16(),
	FsmProperty_t786753495::get_offset_of_ColorParameter_17(),
	FsmProperty_t786753495::get_offset_of_EnumParameter_18(),
	FsmProperty_t786753495::get_offset_of_ArrayParameter_19(),
	FsmProperty_t786753495::get_offset_of_setProperty_20(),
	FsmProperty_t786753495::get_offset_of_initialized_21(),
	FsmProperty_t786753495::get_offset_of_targetObjectCached_22(),
	FsmProperty_t786753495::get_offset_of_memberInfo_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (FsmTemplate_t1285897084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[2] = 
{
	FsmTemplate_t1285897084::get_offset_of_category_2(),
	FsmTemplate_t1285897084::get_offset_of_fsm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (FsmTime_t334847885), -1, sizeof(FsmTime_t334847885_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2072[4] = 
{
	FsmTime_t334847885_StaticFields::get_offset_of_firstUpdateHasHappened_0(),
	FsmTime_t334847885_StaticFields::get_offset_of_totalEditorPlayerPausedTime_1(),
	FsmTime_t334847885_StaticFields::get_offset_of_realtimeLastUpdate_2(),
	FsmTime_t334847885_StaticFields::get_offset_of_frameCountLastUpdate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (FsmAnimationCurve_t326747561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[1] = 
{
	FsmAnimationCurve_t326747561::get_offset_of_curve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (NamedVariable_t3026441313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[5] = 
{
	NamedVariable_t3026441313::get_offset_of_useVariable_0(),
	NamedVariable_t3026441313::get_offset_of_name_1(),
	NamedVariable_t3026441313::get_offset_of_tooltip_2(),
	NamedVariable_t3026441313::get_offset_of_showInInspector_3(),
	NamedVariable_t3026441313::get_offset_of_networkSync_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (FsmArray_t527459893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[11] = 
{
	FsmArray_t527459893::get_offset_of_type_5(),
	FsmArray_t527459893::get_offset_of_objectTypeName_6(),
	FsmArray_t527459893::get_offset_of_objectType_7(),
	FsmArray_t527459893::get_offset_of_floatValues_8(),
	FsmArray_t527459893::get_offset_of_intValues_9(),
	FsmArray_t527459893::get_offset_of_boolValues_10(),
	FsmArray_t527459893::get_offset_of_stringValues_11(),
	FsmArray_t527459893::get_offset_of_vector4Values_12(),
	FsmArray_t527459893::get_offset_of_objectReferences_13(),
	FsmArray_t527459893::get_offset_of_sourceArray_14(),
	FsmArray_t527459893::get_offset_of_values_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (FsmBool_t664485696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[1] = 
{
	FsmBool_t664485696::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (FsmColor_t118301965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	FsmColor_t118301965::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (FsmEnum_t2808516103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2080[4] = 
{
	FsmEnum_t2808516103::get_offset_of_enumName_5(),
	FsmEnum_t2808516103::get_offset_of_intValue_6(),
	FsmEnum_t2808516103::get_offset_of_value_7(),
	FsmEnum_t2808516103::get_offset_of_enumType_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (None_t2771209004)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2081[2] = 
{
	None_t2771209004::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (FsmFloat_t937133978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[1] = 
{
	FsmFloat_t937133978::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (FsmGameObject_t3097142863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[1] = 
{
	FsmGameObject_t3097142863::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (FsmInt_t1273009179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[1] = 
{
	FsmInt_t1273009179::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (FsmObject_t2785794313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[3] = 
{
	FsmObject_t2785794313::get_offset_of_typeName_5(),
	FsmObject_t2785794313::get_offset_of_value_6(),
	FsmObject_t2785794313::get_offset_of_objectType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (FsmMaterial_t1421632035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (FsmOwnerDefault_t2023674184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[2] = 
{
	FsmOwnerDefault_t2023674184::get_offset_of_ownerOption_0(),
	FsmOwnerDefault_t2023674184::get_offset_of_gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (OwnerDefaultOption_t3848444473)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2088[3] = 
{
	OwnerDefaultOption_t3848444473::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (FsmQuaternion_t878438756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[1] = 
{
	FsmQuaternion_t878438756::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (FsmRect_t19023354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[1] = 
{
	FsmRect_t19023354::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (FsmString_t2414474701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[1] = 
{
	FsmString_t2414474701::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (FsmTexture_t3372293163), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (FsmVar_t2872592513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[19] = 
{
	FsmVar_t2872592513::get_offset_of_variableName_0(),
	FsmVar_t2872592513::get_offset_of_objectType_1(),
	FsmVar_t2872592513::get_offset_of_useVariable_2(),
	FsmVar_t2872592513::get_offset_of_namedVar_3(),
	FsmVar_t2872592513::get_offset_of_namedVarType_4(),
	FsmVar_t2872592513::get_offset_of_enumType_5(),
	FsmVar_t2872592513::get_offset_of_enumValue_6(),
	FsmVar_t2872592513::get_offset_of__objectType_7(),
	FsmVar_t2872592513::get_offset_of_type_8(),
	FsmVar_t2872592513::get_offset_of_floatValue_9(),
	FsmVar_t2872592513::get_offset_of_intValue_10(),
	FsmVar_t2872592513::get_offset_of_boolValue_11(),
	FsmVar_t2872592513::get_offset_of_stringValue_12(),
	FsmVar_t2872592513::get_offset_of_vector4Value_13(),
	FsmVar_t2872592513::get_offset_of_objectReference_14(),
	FsmVar_t2872592513::get_offset_of_arrayValue_15(),
	FsmVar_t2872592513::get_offset_of_vector2_16(),
	FsmVar_t2872592513::get_offset_of_vector3_17(),
	FsmVar_t2872592513::get_offset_of_rect_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (FsmVector2_t2430450063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[1] = 
{
	FsmVector2_t2430450063::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (FsmVector3_t3996534004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[1] = 
{
	FsmVector3_t3996534004::get_offset_of_value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (ActionData_t1467934444), -1, sizeof(ActionData_t1467934444_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2096[48] = 
{
	0,
	0,
	ActionData_t1467934444_StaticFields::get_offset_of_ActionTypeLookup_2(),
	ActionData_t1467934444_StaticFields::get_offset_of_ActionFieldsLookup_3(),
	ActionData_t1467934444_StaticFields::get_offset_of_ActionHashCodeLookup_4(),
	ActionData_t1467934444_StaticFields::get_offset_of_resaveActionData_5(),
	ActionData_t1467934444_StaticFields::get_offset_of_UsedIndices_6(),
	ActionData_t1467934444_StaticFields::get_offset_of_InitFields_7(),
	ActionData_t1467934444::get_offset_of_actionNames_8(),
	ActionData_t1467934444::get_offset_of_customNames_9(),
	ActionData_t1467934444::get_offset_of_actionEnabled_10(),
	ActionData_t1467934444::get_offset_of_actionIsOpen_11(),
	ActionData_t1467934444::get_offset_of_actionStartIndex_12(),
	ActionData_t1467934444::get_offset_of_actionHashCodes_13(),
	ActionData_t1467934444::get_offset_of_unityObjectParams_14(),
	ActionData_t1467934444::get_offset_of_fsmGameObjectParams_15(),
	ActionData_t1467934444::get_offset_of_fsmOwnerDefaultParams_16(),
	ActionData_t1467934444::get_offset_of_animationCurveParams_17(),
	ActionData_t1467934444::get_offset_of_functionCallParams_18(),
	ActionData_t1467934444::get_offset_of_fsmTemplateControlParams_19(),
	ActionData_t1467934444::get_offset_of_fsmEventTargetParams_20(),
	ActionData_t1467934444::get_offset_of_fsmPropertyParams_21(),
	ActionData_t1467934444::get_offset_of_layoutOptionParams_22(),
	ActionData_t1467934444::get_offset_of_fsmStringParams_23(),
	ActionData_t1467934444::get_offset_of_fsmObjectParams_24(),
	ActionData_t1467934444::get_offset_of_fsmVarParams_25(),
	ActionData_t1467934444::get_offset_of_fsmArrayParams_26(),
	ActionData_t1467934444::get_offset_of_fsmEnumParams_27(),
	ActionData_t1467934444::get_offset_of_fsmFloatParams_28(),
	ActionData_t1467934444::get_offset_of_fsmIntParams_29(),
	ActionData_t1467934444::get_offset_of_fsmBoolParams_30(),
	ActionData_t1467934444::get_offset_of_fsmVector2Params_31(),
	ActionData_t1467934444::get_offset_of_fsmVector3Params_32(),
	ActionData_t1467934444::get_offset_of_fsmColorParams_33(),
	ActionData_t1467934444::get_offset_of_fsmRectParams_34(),
	ActionData_t1467934444::get_offset_of_fsmQuaternionParams_35(),
	ActionData_t1467934444::get_offset_of_stringParams_36(),
	ActionData_t1467934444::get_offset_of_byteData_37(),
	ActionData_t1467934444::get_offset_of_byteDataAsArray_38(),
	ActionData_t1467934444::get_offset_of_arrayParamSizes_39(),
	ActionData_t1467934444::get_offset_of_arrayParamTypes_40(),
	ActionData_t1467934444::get_offset_of_customTypeSizes_41(),
	ActionData_t1467934444::get_offset_of_customTypeNames_42(),
	ActionData_t1467934444::get_offset_of_paramDataType_43(),
	ActionData_t1467934444::get_offset_of_paramName_44(),
	ActionData_t1467934444::get_offset_of_paramDataPos_45(),
	ActionData_t1467934444::get_offset_of_paramByteDataSize_46(),
	ActionData_t1467934444::get_offset_of_nextParamIndex_47(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (Context_t2765413152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[5] = 
{
	Context_t2765413152::get_offset_of_currentFsm_0(),
	Context_t2765413152::get_offset_of_currentState_1(),
	Context_t2765413152::get_offset_of_currentAction_2(),
	Context_t2765413152::get_offset_of_currentActionIndex_3(),
	Context_t2765413152::get_offset_of_currentParameter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (ParamDataType_t2159627923)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2098[44] = 
{
	ParamDataType_t2159627923::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (Fsm_t917886356), -1, sizeof(Fsm_t917886356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2099[102] = 
{
	0,
	0,
	0,
	Fsm_t917886356::get_offset_of_updateHelperSetDirty_3(),
	Fsm_t917886356_StaticFields::get_offset_of_EventData_4(),
	Fsm_t917886356_StaticFields::get_offset_of_debugLookAtColor_5(),
	Fsm_t917886356_StaticFields::get_offset_of_debugRaycastColor_6(),
	Fsm_t917886356::get_offset_of_dataVersion_7(),
	Fsm_t917886356::get_offset_of_owner_8(),
	Fsm_t917886356::get_offset_of_usedInTemplate_9(),
	Fsm_t917886356::get_offset_of_name_10(),
	Fsm_t917886356::get_offset_of_startState_11(),
	Fsm_t917886356::get_offset_of_states_12(),
	Fsm_t917886356::get_offset_of_events_13(),
	Fsm_t917886356::get_offset_of_globalTransitions_14(),
	Fsm_t917886356::get_offset_of_variables_15(),
	Fsm_t917886356::get_offset_of_description_16(),
	Fsm_t917886356::get_offset_of_docUrl_17(),
	Fsm_t917886356::get_offset_of_showStateLabel_18(),
	Fsm_t917886356::get_offset_of_maxLoopCount_19(),
	Fsm_t917886356::get_offset_of_watermark_20(),
	Fsm_t917886356::get_offset_of_password_21(),
	Fsm_t917886356::get_offset_of_locked_22(),
	Fsm_t917886356::get_offset_of_manualUpdate_23(),
	Fsm_t917886356::get_offset_of_keepDelayedEventsOnStateExit_24(),
	Fsm_t917886356::get_offset_of_preprocessed_25(),
	Fsm_t917886356::get_offset_of_host_26(),
	Fsm_t917886356::get_offset_of_rootFsm_27(),
	Fsm_t917886356::get_offset_of_subFsmList_28(),
	Fsm_t917886356::get_offset_of_setDirty_29(),
	Fsm_t917886356::get_offset_of_activeStateEntered_30(),
	Fsm_t917886356::get_offset_of_ExposedEvents_31(),
	Fsm_t917886356::get_offset_of_myLog_32(),
	Fsm_t917886356::get_offset_of_RestartOnEnable_33(),
	Fsm_t917886356::get_offset_of_EnableDebugFlow_34(),
	Fsm_t917886356::get_offset_of_EnableBreakpoints_35(),
	Fsm_t917886356::get_offset_of_StepFrame_36(),
	Fsm_t917886356::get_offset_of_delayedEvents_37(),
	Fsm_t917886356::get_offset_of_updateEvents_38(),
	Fsm_t917886356::get_offset_of_removeEvents_39(),
	Fsm_t917886356::get_offset_of_editorFlags_40(),
	Fsm_t917886356::get_offset_of_initialized_41(),
	Fsm_t917886356::get_offset_of_activeStateName_42(),
	Fsm_t917886356::get_offset_of_activeState_43(),
	Fsm_t917886356::get_offset_of_switchToState_44(),
	Fsm_t917886356::get_offset_of_previousActiveState_45(),
	Fsm_t917886356_StaticFields::get_offset_of_StateColors_46(),
	Fsm_t917886356::get_offset_of_editState_47(),
	Fsm_t917886356::get_offset_of_mouseEvents_48(),
	Fsm_t917886356::get_offset_of_handleLevelLoaded_49(),
	Fsm_t917886356::get_offset_of_handleTriggerEnter2D_50(),
	Fsm_t917886356::get_offset_of_handleTriggerExit2D_51(),
	Fsm_t917886356::get_offset_of_handleTriggerStay2D_52(),
	Fsm_t917886356::get_offset_of_handleCollisionEnter2D_53(),
	Fsm_t917886356::get_offset_of_handleCollisionExit2D_54(),
	Fsm_t917886356::get_offset_of_handleCollisionStay2D_55(),
	Fsm_t917886356::get_offset_of_handleTriggerEnter_56(),
	Fsm_t917886356::get_offset_of_handleTriggerExit_57(),
	Fsm_t917886356::get_offset_of_handleTriggerStay_58(),
	Fsm_t917886356::get_offset_of_handleCollisionEnter_59(),
	Fsm_t917886356::get_offset_of_handleCollisionExit_60(),
	Fsm_t917886356::get_offset_of_handleCollisionStay_61(),
	Fsm_t917886356::get_offset_of_handleParticleCollision_62(),
	Fsm_t917886356::get_offset_of_handleControllerColliderHit_63(),
	Fsm_t917886356::get_offset_of_handleJointBreak_64(),
	Fsm_t917886356::get_offset_of_handleJointBreak2D_65(),
	Fsm_t917886356::get_offset_of_handleOnGUI_66(),
	Fsm_t917886356::get_offset_of_handleFixedUpdate_67(),
	Fsm_t917886356::get_offset_of_handleApplicationEvents_68(),
	Fsm_t917886356_StaticFields::get_offset_of_lastRaycastHit2DInfoLUT_69(),
	Fsm_t917886356::get_offset_of_handleAnimatorMove_70(),
	Fsm_t917886356::get_offset_of_handleAnimatorIK_71(),
	Fsm_t917886356_StaticFields::get_offset_of_targetSelf_72(),
	Fsm_t917886356::get_offset_of_U3CStartedU3Ek__BackingField_73(),
	Fsm_t917886356::get_offset_of_U3CEventTargetU3Ek__BackingField_74(),
	Fsm_t917886356::get_offset_of_U3CFinishedU3Ek__BackingField_75(),
	Fsm_t917886356::get_offset_of_U3CLastTransitionU3Ek__BackingField_76(),
	Fsm_t917886356::get_offset_of_U3CIsModifiedPrefabInstanceU3Ek__BackingField_77(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CLastClickedObjectU3Ek__BackingField_78(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CBreakpointsEnabledU3Ek__BackingField_79(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CHitBreakpointU3Ek__BackingField_80(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CBreakAtFsmU3Ek__BackingField_81(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CBreakAtStateU3Ek__BackingField_82(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CIsBreakU3Ek__BackingField_83(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CIsErrorBreakU3Ek__BackingField_84(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CLastErrorU3Ek__BackingField_85(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CStepToStateChangeU3Ek__BackingField_86(),
	Fsm_t917886356_StaticFields::get_offset_of_U3CStepFsmU3Ek__BackingField_87(),
	Fsm_t917886356::get_offset_of_U3CSwitchedStateU3Ek__BackingField_88(),
	Fsm_t917886356::get_offset_of_U3CCollisionInfoU3Ek__BackingField_89(),
	Fsm_t917886356::get_offset_of_U3CTriggerColliderU3Ek__BackingField_90(),
	Fsm_t917886356::get_offset_of_U3CCollision2DInfoU3Ek__BackingField_91(),
	Fsm_t917886356::get_offset_of_U3CTriggerCollider2DU3Ek__BackingField_92(),
	Fsm_t917886356::get_offset_of_U3CJointBreakForceU3Ek__BackingField_93(),
	Fsm_t917886356::get_offset_of_U3CBrokenJoint2DU3Ek__BackingField_94(),
	Fsm_t917886356::get_offset_of_U3CParticleCollisionGOU3Ek__BackingField_95(),
	Fsm_t917886356::get_offset_of_U3CTriggerNameU3Ek__BackingField_96(),
	Fsm_t917886356::get_offset_of_U3CCollisionNameU3Ek__BackingField_97(),
	Fsm_t917886356::get_offset_of_U3CTrigger2dNameU3Ek__BackingField_98(),
	Fsm_t917886356::get_offset_of_U3CCollision2dNameU3Ek__BackingField_99(),
	Fsm_t917886356::get_offset_of_U3CControllerColliderU3Ek__BackingField_100(),
	Fsm_t917886356::get_offset_of_U3CRaycastHitInfoU3Ek__BackingField_101(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
