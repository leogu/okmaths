﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AudioPlay
struct AudioPlay_t2372341796;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AudioPlay::.ctor()
extern "C"  void AudioPlay__ctor_m469236826 (AudioPlay_t2372341796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::Reset()
extern "C"  void AudioPlay_Reset_m487046589 (AudioPlay_t2372341796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::OnEnter()
extern "C"  void AudioPlay_OnEnter_m1323953229 (AudioPlay_t2372341796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AudioPlay::OnUpdate()
extern "C"  void AudioPlay_OnUpdate_m396568980 (AudioPlay_t2372341796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
