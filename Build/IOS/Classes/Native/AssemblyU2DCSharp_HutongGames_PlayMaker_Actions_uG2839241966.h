﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// PlayMakerUGuiCanvasRaycastFilterEventsProxy
struct PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter
struct  uGuiCanvasEnableRaycastFilter_t2839241966  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::enableRaycasting
	FsmBool_t664485696 * ___enableRaycasting_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::everyFrame
	bool ___everyFrame_14;
	// PlayMakerUGuiCanvasRaycastFilterEventsProxy HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::_comp
	PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240 * ____comp_15;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasEnableRaycastFilter::_originalValue
	bool ____originalValue_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiCanvasEnableRaycastFilter_t2839241966, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_enableRaycasting_12() { return static_cast<int32_t>(offsetof(uGuiCanvasEnableRaycastFilter_t2839241966, ___enableRaycasting_12)); }
	inline FsmBool_t664485696 * get_enableRaycasting_12() const { return ___enableRaycasting_12; }
	inline FsmBool_t664485696 ** get_address_of_enableRaycasting_12() { return &___enableRaycasting_12; }
	inline void set_enableRaycasting_12(FsmBool_t664485696 * value)
	{
		___enableRaycasting_12 = value;
		Il2CppCodeGenWriteBarrier(&___enableRaycasting_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiCanvasEnableRaycastFilter_t2839241966, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiCanvasEnableRaycastFilter_t2839241966, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__comp_15() { return static_cast<int32_t>(offsetof(uGuiCanvasEnableRaycastFilter_t2839241966, ____comp_15)); }
	inline PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240 * get__comp_15() const { return ____comp_15; }
	inline PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240 ** get_address_of__comp_15() { return &____comp_15; }
	inline void set__comp_15(PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240 * value)
	{
		____comp_15 = value;
		Il2CppCodeGenWriteBarrier(&____comp_15, value);
	}

	inline static int32_t get_offset_of__originalValue_16() { return static_cast<int32_t>(offsetof(uGuiCanvasEnableRaycastFilter_t2839241966, ____originalValue_16)); }
	inline bool get__originalValue_16() const { return ____originalValue_16; }
	inline bool* get_address_of__originalValue_16() { return &____originalValue_16; }
	inline void set__originalValue_16(bool value)
	{
		____originalValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
