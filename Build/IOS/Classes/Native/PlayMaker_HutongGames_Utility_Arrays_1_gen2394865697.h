﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t3601875862;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.Arrays`1<HutongGames.PlayMaker.FsmGameObject>
struct  Arrays_1_t2394865697  : public Il2CppObject
{
public:

public:
};

struct Arrays_1_t2394865697_StaticFields
{
public:
	// T[] HutongGames.Utility.Arrays`1::Empty
	FsmGameObjectU5BU5D_t3601875862* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Arrays_1_t2394865697_StaticFields, ___Empty_0)); }
	inline FsmGameObjectU5BU5D_t3601875862* get_Empty_0() const { return ___Empty_0; }
	inline FsmGameObjectU5BU5D_t3601875862** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(FsmGameObjectU5BU5D_t3601875862* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
