﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetChildCount
struct GetChildCount_t4111048973;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetChildCount::.ctor()
extern "C"  void GetChildCount__ctor_m795941629 (GetChildCount_t4111048973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::Reset()
extern "C"  void GetChildCount_Reset_m3215576514 (GetChildCount_t4111048973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::OnEnter()
extern "C"  void GetChildCount_OnEnter_m3621774760 (GetChildCount_t4111048973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildCount::DoGetChildCount()
extern "C"  void GetChildCount_DoGetChildCount_m4253553073 (GetChildCount_t4111048973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
