﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent
struct uGuiInputFieldOnSubmitEvent_t2122334953;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::.ctor()
extern "C"  void uGuiInputFieldOnSubmitEvent__ctor_m885814605 (uGuiInputFieldOnSubmitEvent_t2122334953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::Reset()
extern "C"  void uGuiInputFieldOnSubmitEvent_Reset_m1577565098 (uGuiInputFieldOnSubmitEvent_t2122334953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::OnEnter()
extern "C"  void uGuiInputFieldOnSubmitEvent_OnEnter_m3783409480 (uGuiInputFieldOnSubmitEvent_t2122334953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::OnExit()
extern "C"  void uGuiInputFieldOnSubmitEvent_OnExit_m1977037460 (uGuiInputFieldOnSubmitEvent_t2122334953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnSubmitEvent::DoOnEndEdit(System.String)
extern "C"  void uGuiInputFieldOnSubmitEvent_DoOnEndEdit_m136452978 (uGuiInputFieldOnSubmitEvent_t2122334953 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
