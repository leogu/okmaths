﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraFieldOfView
struct DOTweenCameraFieldOfView_t369541943;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFieldOfView::.ctor()
extern "C"  void DOTweenCameraFieldOfView__ctor_m1402175775 (DOTweenCameraFieldOfView_t369541943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFieldOfView::Reset()
extern "C"  void DOTweenCameraFieldOfView_Reset_m3975172872 (DOTweenCameraFieldOfView_t369541943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFieldOfView::OnEnter()
extern "C"  void DOTweenCameraFieldOfView_OnEnter_m1003272518 (DOTweenCameraFieldOfView_t369541943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFieldOfView::<OnEnter>m__28()
extern "C"  void DOTweenCameraFieldOfView_U3COnEnterU3Em__28_m2406508043 (DOTweenCameraFieldOfView_t369541943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraFieldOfView::<OnEnter>m__29()
extern "C"  void DOTweenCameraFieldOfView_U3COnEnterU3Em__29_m3952389008 (DOTweenCameraFieldOfView_t369541943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
