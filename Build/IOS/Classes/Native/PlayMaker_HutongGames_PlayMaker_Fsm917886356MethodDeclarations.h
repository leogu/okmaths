﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct List_1_t287007488;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// FsmTemplate
struct FsmTemplate_t1285897084;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent>
struct List_1_t993821960;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t1586422282;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t287863993;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t1091630918;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t3672513366;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Joint2D
struct Joint2D_t854621618;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t924276959;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t2110469976;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1624700828;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "PlayMaker_FsmTemplate1285897084.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_Joint2D854621618.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl924276959.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData2110469976.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault2023674184.h"
#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_FsmList()
extern "C"  List_1_t287007488 * Fsm_get_FsmList_m3228152621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_SortedFsmList()
extern "C"  List_1_t287007488 * Fsm_get_SortedFsmList_m2137420164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo HutongGames.PlayMaker.Fsm::get_UpdateHelperSetDirty()
extern "C"  MethodInfo_t * Fsm_get_UpdateHelperSetDirty_m2148272451 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ManualUpdate()
extern "C"  bool Fsm_get_ManualUpdate_m3049553031 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ManualUpdate(System.Boolean)
extern "C"  void Fsm_set_ManualUpdate_m688297342 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_KeepDelayedEventsOnStateExit()
extern "C"  bool Fsm_get_KeepDelayedEventsOnStateExit_m3586705596 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_KeepDelayedEventsOnStateExit(System.Boolean)
extern "C"  void Fsm_set_KeepDelayedEventsOnStateExit_m2240822517 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Preprocessed()
extern "C"  bool Fsm_get_Preprocessed_m4033536981 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Preprocessed(System.Boolean)
extern "C"  void Fsm_set_Preprocessed_m1718706670 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_Host()
extern "C"  Fsm_t917886356 * Fsm_get_Host_m2166998137 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Host(HutongGames.PlayMaker.Fsm)
extern "C"  void Fsm_set_Host_m48577128 (Fsm_t917886356 * __this, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Password()
extern "C"  String_t* Fsm_get_Password_m4207765166 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Locked()
extern "C"  bool Fsm_get_Locked_m3176350432 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Lock(System.String)
extern "C"  void Fsm_Lock_m1702046960 (Fsm_t917886356 * __this, String_t* ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Unlock(System.String)
extern "C"  void Fsm_Unlock_m3615336251 (Fsm_t917886356 * __this, String_t* ___pass0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FsmTemplate HutongGames.PlayMaker.Fsm::get_Template()
extern "C"  FsmTemplate_t1285897084 * Fsm_get_Template_m3887363727 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsSubFsm()
extern "C"  bool Fsm_get_IsSubFsm_m2680904342 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_RootFsm()
extern "C"  Fsm_t917886356 * Fsm_get_RootFsm_m2408457475 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_SubFsmList()
extern "C"  List_1_t287007488 * Fsm_get_SubFsmList_m106111173 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Started()
extern "C"  bool Fsm_get_Started_m2972087901 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Started(System.Boolean)
extern "C"  void Fsm_set_Started_m2496592890 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.DelayedEvent> HutongGames.PlayMaker.Fsm::get_DelayedEvents()
extern "C"  List_1_t993821960 * Fsm_get_DelayedEvents_m434720076 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::KillDelayedEvents()
extern "C"  void Fsm_KillDelayedEvents_m2385667250 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::get_DataVersion()
extern "C"  int32_t Fsm_get_DataVersion_m2551881936 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DataVersion(System.Int32)
extern "C"  void Fsm_set_DataVersion_m88737183 (Fsm_t917886356 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MonoBehaviour HutongGames.PlayMaker.Fsm::get_Owner()
extern "C"  MonoBehaviour_t1158329972 * Fsm_get_Owner_m3436607703 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Owner(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_set_Owner_m3780371132 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_NameIsExpanded()
extern "C"  bool Fsm_get_NameIsExpanded_m912001200 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_NameIsExpanded(System.Boolean)
extern "C"  void Fsm_set_NameIsExpanded_m1396458869 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ControlsIsExpanded()
extern "C"  bool Fsm_get_ControlsIsExpanded_m2742197541 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ControlsIsExpanded(System.Boolean)
extern "C"  void Fsm_set_ControlsIsExpanded_m1413128354 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_DebugIsExpanded()
extern "C"  bool Fsm_get_DebugIsExpanded_m1188324106 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DebugIsExpanded(System.Boolean)
extern "C"  void Fsm_set_DebugIsExpanded_m800184331 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ExperimentalIsExpanded()
extern "C"  bool Fsm_get_ExperimentalIsExpanded_m3641033257 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ExperimentalIsExpanded(System.Boolean)
extern "C"  void Fsm_set_ExperimentalIsExpanded_m1888546058 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Name()
extern "C"  String_t* Fsm_get_Name_m2639317356 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Name(System.String)
extern "C"  void Fsm_set_Name_m629262121 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FsmTemplate HutongGames.PlayMaker.Fsm::get_UsedInTemplate()
extern "C"  FsmTemplate_t1285897084 * Fsm_get_UsedInTemplate_m3209877907 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_UsedInTemplate(FsmTemplate)
extern "C"  void Fsm_set_UsedInTemplate_m2342808710 (Fsm_t917886356 * __this, FsmTemplate_t1285897084 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_StartState()
extern "C"  String_t* Fsm_get_StartState_m2509917862 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_StartState(System.String)
extern "C"  void Fsm_set_StartState_m1915714789 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::get_States()
extern "C"  FsmStateU5BU5D_t1586422282* Fsm_get_States_m1003125214 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_States(HutongGames.PlayMaker.FsmState[])
extern "C"  void Fsm_set_States_m93428543 (Fsm_t917886356 * __this, FsmStateU5BU5D_t1586422282* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::get_Events()
extern "C"  FsmEventU5BU5D_t287863993* Fsm_get_Events_m465635212 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Events(HutongGames.PlayMaker.FsmEvent[])
extern "C"  void Fsm_set_Events_m2765528895 (Fsm_t917886356 * __this, FsmEventU5BU5D_t287863993* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::get_GlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t1091630918* Fsm_get_GlobalTransitions_m387686343 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_GlobalTransitions(HutongGames.PlayMaker.FsmTransition[])
extern "C"  void Fsm_set_GlobalTransitions_m2956556498 (Fsm_t917886356 * __this, FsmTransitionU5BU5D_t1091630918* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::get_Variables()
extern "C"  FsmVariables_t630687169 * Fsm_get_Variables_m738201045 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void Fsm_set_Variables_m694063032 (Fsm_t917886356 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Fsm::get_EventTarget()
extern "C"  FsmEventTarget_t172293745 * Fsm_get_EventTarget_m3613072437 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_EventTarget(HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void Fsm_set_EventTarget_m972259304 (Fsm_t917886356 * __this, FsmEventTarget_t172293745 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Initialized()
extern "C"  bool Fsm_get_Initialized_m1401615690 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Active()
extern "C"  bool Fsm_get_Active_m1413219028 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Finished()
extern "C"  bool Fsm_get_Finished_m3585713968 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Finished(System.Boolean)
extern "C"  void Fsm_set_Finished_m3045382849 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsSwitchingState()
extern "C"  bool Fsm_get_IsSwitchingState_m2285414671 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_ActiveState()
extern "C"  FsmState_t1643911659 * Fsm_get_ActiveState_m1540121605 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ActiveState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_ActiveState_m423822318 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_ActiveStateName()
extern "C"  String_t* Fsm_get_ActiveStateName_m2290391609 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_PreviousActiveState()
extern "C"  FsmState_t1643911659 * Fsm_get_PreviousActiveState_m2041953030 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_PreviousActiveState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_PreviousActiveState_m1441110627 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition HutongGames.PlayMaker.Fsm::get_LastTransition()
extern "C"  FsmTransition_t1534990431 * Fsm_get_LastTransition_m647665351 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastTransition(HutongGames.PlayMaker.FsmTransition)
extern "C"  void Fsm_set_LastTransition_m4223571782 (Fsm_t917886356 * __this, FsmTransition_t1534990431 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::get_MaxLoopCount()
extern "C"  int32_t Fsm_get_MaxLoopCount_m1527442817 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::get_MaxLoopCountOverride()
extern "C"  int32_t Fsm_get_MaxLoopCountOverride_m2057761213 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_MaxLoopCountOverride(System.Int32)
extern "C"  void Fsm_set_MaxLoopCountOverride_m767369370 (Fsm_t917886356 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_OwnerName()
extern "C"  String_t* Fsm_get_OwnerName_m3249653591 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_OwnerDebugName()
extern "C"  String_t* Fsm_get_OwnerDebugName_m906735342 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_GameObject()
extern "C"  GameObject_t1756533147 * Fsm_get_GameObject_m315529030 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_GameObjectName()
extern "C"  String_t* Fsm_get_GameObjectName_m1607528485 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object HutongGames.PlayMaker.Fsm::get_OwnerObject()
extern "C"  Object_t1021602117 * Fsm_get_OwnerObject_m1758222649 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM HutongGames.PlayMaker.Fsm::get_FsmComponent()
extern "C"  PlayMakerFSM_t437737208 * Fsm_get_FsmComponent_m1380827196 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.Fsm::get_MyLog()
extern "C"  FsmLog_t3672513366 * Fsm_get_MyLog_m3263952233 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsModifiedPrefabInstance()
extern "C"  bool Fsm_get_IsModifiedPrefabInstance_m1741502688 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_IsModifiedPrefabInstance(System.Boolean)
extern "C"  void Fsm_set_IsModifiedPrefabInstance_m899426119 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Description()
extern "C"  String_t* Fsm_get_Description_m204017195 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Description(System.String)
extern "C"  void Fsm_set_Description_m2175047222 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Watermark()
extern "C"  String_t* Fsm_get_Watermark_m3150968781 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Watermark(System.String)
extern "C"  void Fsm_set_Watermark_m298835098 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ShowStateLabel()
extern "C"  bool Fsm_get_ShowStateLabel_m3886429386 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ShowStateLabel(System.Boolean)
extern "C"  void Fsm_set_ShowStateLabel_m2790468581 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.Fsm::get_DebugLookAtColor()
extern "C"  Color_t2020392075  Fsm_get_DebugLookAtColor_m3472556307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DebugLookAtColor(UnityEngine.Color)
extern "C"  void Fsm_set_DebugLookAtColor_m2057914672 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.Fsm::get_DebugRaycastColor()
extern "C"  Color_t2020392075  Fsm_get_DebugRaycastColor_m1379651988 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DebugRaycastColor(UnityEngine.Color)
extern "C"  void Fsm_set_DebugRaycastColor_m2345282013 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_GuiLabel()
extern "C"  String_t* Fsm_get_GuiLabel_m3141446448 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_DocUrl()
extern "C"  String_t* Fsm_get_DocUrl_m3734718192 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_DocUrl(System.String)
extern "C"  void Fsm_set_DocUrl_m599062369 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_EditState()
extern "C"  FsmState_t1643911659 * Fsm_get_EditState_m1392825861 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_EditState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_EditState_m1738404636 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_LastClickedObject()
extern "C"  GameObject_t1756533147 * Fsm_get_LastClickedObject_m2901359403 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastClickedObject(UnityEngine.GameObject)
extern "C"  void Fsm_set_LastClickedObject_m3675016442 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_BreakpointsEnabled()
extern "C"  bool Fsm_get_BreakpointsEnabled_m4290079393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BreakpointsEnabled(System.Boolean)
extern "C"  void Fsm_set_BreakpointsEnabled_m169131116 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HitBreakpoint()
extern "C"  bool Fsm_get_HitBreakpoint_m1473971126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HitBreakpoint(System.Boolean)
extern "C"  void Fsm_set_HitBreakpoint_m3133137153 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_BreakAtFsm()
extern "C"  Fsm_t917886356 * Fsm_get_BreakAtFsm_m4036522439 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BreakAtFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void Fsm_set_BreakAtFsm_m1256911702 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_BreakAtState()
extern "C"  FsmState_t1643911659 * Fsm_get_BreakAtState_m146847755 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BreakAtState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_set_BreakAtState_m3868374240 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsBreak()
extern "C"  bool Fsm_get_IsBreak_m1009229833 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_IsBreak(System.Boolean)
extern "C"  void Fsm_set_IsBreak_m764217324 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_IsErrorBreak()
extern "C"  bool Fsm_get_IsErrorBreak_m1608424393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_IsErrorBreak(System.Boolean)
extern "C"  void Fsm_set_IsErrorBreak_m4015658996 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_LastError()
extern "C"  String_t* Fsm_get_LastError_m626024767 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastError(System.String)
extern "C"  void Fsm_set_LastError_m3697235538 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_StepToStateChange()
extern "C"  bool Fsm_get_StepToStateChange_m3751165768 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_StepToStateChange(System.Boolean)
extern "C"  void Fsm_set_StepToStateChange_m1180323979 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::get_StepFsm()
extern "C"  Fsm_t917886356 * Fsm_get_StepFsm_m155395483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_StepFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void Fsm_set_StepFsm_m938251080 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_SwitchedState()
extern "C"  bool Fsm_get_SwitchedState_m2510596306 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_SwitchedState(System.Boolean)
extern "C"  void Fsm_set_SwitchedState_m3464988531 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_MouseEvents()
extern "C"  bool Fsm_get_MouseEvents_m3020071934 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_MouseEvents(System.Boolean)
extern "C"  void Fsm_set_MouseEvents_m786106569 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleLevelLoaded()
extern "C"  bool Fsm_get_HandleLevelLoaded_m1742270875 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleLevelLoaded(System.Boolean)
extern "C"  void Fsm_set_HandleLevelLoaded_m922634522 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter2D()
extern "C"  bool Fsm_get_HandleTriggerEnter2D_m172380676 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerEnter2D(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerEnter2D_m87970099 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit2D()
extern "C"  bool Fsm_get_HandleTriggerExit2D_m766085676 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerExit2D(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerExit2D_m725180559 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay2D()
extern "C"  bool Fsm_get_HandleTriggerStay2D_m2610463627 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerStay2D(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerStay2D_m3829302426 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter2D()
extern "C"  bool Fsm_get_HandleCollisionEnter2D_m1401630576 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionEnter2D(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionEnter2D_m1797382929 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit2D()
extern "C"  bool Fsm_get_HandleCollisionExit2D_m3119743400 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionExit2D(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionExit2D_m2084988865 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay2D()
extern "C"  bool Fsm_get_HandleCollisionStay2D_m226828777 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionStay2D(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionStay2D_m923928150 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter()
extern "C"  bool Fsm_get_HandleTriggerEnter_m790156034 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerEnter(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerEnter_m3865443025 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit()
extern "C"  bool Fsm_get_HandleTriggerExit_m362350026 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerExit(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerExit_m4131739509 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay()
extern "C"  bool Fsm_get_HandleTriggerStay_m3688276885 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleTriggerStay(System.Boolean)
extern "C"  void Fsm_set_HandleTriggerStay_m2440691004 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter()
extern "C"  bool Fsm_get_HandleCollisionEnter_m1456631574 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionEnter(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionEnter_m955167463 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit()
extern "C"  bool Fsm_get_HandleCollisionExit_m3450142898 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionExit(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionExit_m3543547363 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay()
extern "C"  bool Fsm_get_HandleCollisionStay_m1380478791 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleCollisionStay(System.Boolean)
extern "C"  void Fsm_set_HandleCollisionStay_m2249027452 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleParticleCollision()
extern "C"  bool Fsm_get_HandleParticleCollision_m2116271836 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleParticleCollision(System.Boolean)
extern "C"  void Fsm_set_HandleParticleCollision_m827726651 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleControllerColliderHit()
extern "C"  bool Fsm_get_HandleControllerColliderHit_m3942844253 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleControllerColliderHit(System.Boolean)
extern "C"  void Fsm_set_HandleControllerColliderHit_m4065529716 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak()
extern "C"  bool Fsm_get_HandleJointBreak_m3453435899 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleJointBreak(System.Boolean)
extern "C"  void Fsm_set_HandleJointBreak_m2784839312 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak2D()
extern "C"  bool Fsm_get_HandleJointBreak2D_m466287097 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleJointBreak2D(System.Boolean)
extern "C"  void Fsm_set_HandleJointBreak2D_m4167859246 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleOnGUI()
extern "C"  bool Fsm_get_HandleOnGUI_m1667477422 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleOnGUI(System.Boolean)
extern "C"  void Fsm_set_HandleOnGUI_m3808117791 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleFixedUpdate()
extern "C"  bool Fsm_get_HandleFixedUpdate_m2387851797 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleFixedUpdate(System.Boolean)
extern "C"  void Fsm_set_HandleFixedUpdate_m1286405006 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleApplicationEvents()
extern "C"  bool Fsm_get_HandleApplicationEvents_m1537621535 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleApplicationEvents(System.Boolean)
extern "C"  void Fsm_set_HandleApplicationEvents_m1386547774 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ResetEventHandlerFlags()
extern "C"  void Fsm_ResetEventHandlerFlags_m2062434531 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collision HutongGames.PlayMaker.Fsm::get_CollisionInfo()
extern "C"  Collision_t2876846408 * Fsm_get_CollisionInfo_m671677372 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_CollisionInfo(UnityEngine.Collision)
extern "C"  void Fsm_set_CollisionInfo_m2846927561 (Fsm_t917886356 * __this, Collision_t2876846408 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider HutongGames.PlayMaker.Fsm::get_TriggerCollider()
extern "C"  Collider_t3497673348 * Fsm_get_TriggerCollider_m3225889836 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_TriggerCollider(UnityEngine.Collider)
extern "C"  void Fsm_set_TriggerCollider_m3429466907 (Fsm_t917886356 * __this, Collider_t3497673348 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collision2D HutongGames.PlayMaker.Fsm::get_Collision2DInfo()
extern "C"  Collision2D_t1539500754 * Fsm_get_Collision2DInfo_m2865428404 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Collision2DInfo(UnityEngine.Collision2D)
extern "C"  void Fsm_set_Collision2DInfo_m3749404201 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D HutongGames.PlayMaker.Fsm::get_TriggerCollider2D()
extern "C"  Collider2D_t646061738 * Fsm_get_TriggerCollider2D_m2473407896 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_TriggerCollider2D(UnityEngine.Collider2D)
extern "C"  void Fsm_set_TriggerCollider2D_m1959796955 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Fsm::get_JointBreakForce()
extern "C"  float Fsm_get_JointBreakForce_m3272180562 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_JointBreakForce(System.Single)
extern "C"  void Fsm_set_JointBreakForce_m1432342517 (Fsm_t917886356 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Joint2D HutongGames.PlayMaker.Fsm::get_BrokenJoint2D()
extern "C"  Joint2D_t854621618 * Fsm_get_BrokenJoint2D_m3185766303 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_BrokenJoint2D(UnityEngine.Joint2D)
extern "C"  void Fsm_set_BrokenJoint2D_m2371351870 (Fsm_t917886356 * __this, Joint2D_t854621618 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_ParticleCollisionGO()
extern "C"  GameObject_t1756533147 * Fsm_get_ParticleCollisionGO_m178382449 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ParticleCollisionGO(UnityEngine.GameObject)
extern "C"  void Fsm_set_ParticleCollisionGO_m901173032 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_CollisionGO()
extern "C"  GameObject_t1756533147 * Fsm_get_CollisionGO_m4129314389 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_Collision2dGO()
extern "C"  GameObject_t1756533147 * Fsm_get_Collision2dGO_m3864790327 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_TriggerGO()
extern "C"  GameObject_t1756533147 * Fsm_get_TriggerGO_m3648412683 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::get_Trigger2dGO()
extern "C"  GameObject_t1756533147 * Fsm_get_Trigger2dGO_m990714561 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_TriggerName()
extern "C"  String_t* Fsm_get_TriggerName_m2843419100 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_TriggerName(System.String)
extern "C"  void Fsm_set_TriggerName_m3981039185 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_CollisionName()
extern "C"  String_t* Fsm_get_CollisionName_m2425162816 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_CollisionName(System.String)
extern "C"  void Fsm_set_CollisionName_m3184175347 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Trigger2dName()
extern "C"  String_t* Fsm_get_Trigger2dName_m2993977086 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Trigger2dName(System.String)
extern "C"  void Fsm_set_Trigger2dName_m4134485071 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Collision2dName()
extern "C"  String_t* Fsm_get_Collision2dName_m4009909990 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Collision2dName(System.String)
extern "C"  void Fsm_set_Collision2dName_m401456173 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ControllerColliderHit HutongGames.PlayMaker.Fsm::get_ControllerCollider()
extern "C"  ControllerColliderHit_t4070855101 * Fsm_get_ControllerCollider_m3850599901 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ControllerCollider(UnityEngine.ControllerColliderHit)
extern "C"  void Fsm_set_ControllerCollider_m2810757166 (Fsm_t917886356 * __this, ControllerColliderHit_t4070855101 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit HutongGames.PlayMaker.Fsm::get_RaycastHitInfo()
extern "C"  RaycastHit_t87180320  Fsm_get_RaycastHitInfo_m2871237254 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_RaycastHitInfo(UnityEngine.RaycastHit)
extern "C"  void Fsm_set_RaycastHitInfo_m3536322337 (Fsm_t917886356 * __this, RaycastHit_t87180320  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::RecordLastRaycastHit2DInfo(HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D)
extern "C"  void Fsm_RecordLastRaycastHit2DInfo_m756800341 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, RaycastHit2D_t4063908774  ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D HutongGames.PlayMaker.Fsm::GetLastRaycastHit2DInfo(HutongGames.PlayMaker.Fsm)
extern "C"  RaycastHit2D_t4063908774  Fsm_GetLastRaycastHit2DInfo_m331081057 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorMove()
extern "C"  bool Fsm_get_HandleAnimatorMove_m1712909480 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleAnimatorMove(System.Boolean)
extern "C"  void Fsm_set_HandleAnimatorMove_m2112503791 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorIK()
extern "C"  bool Fsm_get_HandleAnimatorIK_m2492200159 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleAnimatorIK(System.Boolean)
extern "C"  void Fsm_set_HandleAnimatorIK_m3869023658 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::NewTempFsm()
extern "C"  Fsm_t917886356 * Fsm_NewTempFsm_m2225216928 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.ctor()
extern "C"  void Fsm__ctor_m1745524161 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVariables)
extern "C"  void Fsm__ctor_m2872900866 (Fsm_t917886356 * __this, Fsm_t917886356 * ___source0, FsmVariables_t630687169 * ___overrideVariables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::CreateSubFsm(HutongGames.PlayMaker.FsmTemplateControl)
extern "C"  Fsm_t917886356 * Fsm_CreateSubFsm_m2497459229 (Fsm_t917886356 * __this, FsmTemplateControl_t924276959 * ___templateControl0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::GetRootFsm()
extern "C"  Fsm_t917886356 * Fsm_GetRootFsm_m408959144 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::CheckIfDirty()
extern "C"  void Fsm_CheckIfDirty_m1860917440 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Reset(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Reset_m1808960153 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateDataVersion()
extern "C"  void Fsm_UpdateDataVersion_m143079898 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SaveActions()
extern "C"  void Fsm_SaveActions_m454533049 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Clear(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Clear_m341686841 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixDataVersion()
extern "C"  void Fsm_FixDataVersion_m3368674364 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::DeduceDataVersion()
extern "C"  int32_t Fsm_DeduceDataVersion_m1252541111 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Preprocess(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Preprocess_m297661774 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Preprocess()
extern "C"  void Fsm_Preprocess_m1693129101 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Awake()
extern "C"  void Fsm_Awake_m3126270154 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Init(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Init_m1264563958 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Reinitialize()
extern "C"  void Fsm_Reinitialize_m4236433894 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::InitData()
extern "C"  void Fsm_InitData_m3521367119 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::CheckFsmEventsForEventHandlers()
extern "C"  void Fsm_CheckFsmEventsForEventHandlers_m787971298 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnEnable()
extern "C"  void Fsm_OnEnable_m1500513457 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Start()
extern "C"  void Fsm_Start_m2000538737 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Update()
extern "C"  void Fsm_Update_m1801256204 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateDelayedEvents()
extern "C"  void Fsm_UpdateDelayedEvents_m2694693175 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ClearDelayedEvents()
extern "C"  void Fsm_ClearDelayedEvents_m917774659 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixedUpdate()
extern "C"  void Fsm_FixedUpdate_m4175912806 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::LateUpdate()
extern "C"  void Fsm_LateUpdate_m459470616 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDisable()
extern "C"  void Fsm_OnDisable_m2956643674 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Stop()
extern "C"  void Fsm_Stop_m3650953385 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::StopAndReset()
extern "C"  void Fsm_StopAndReset_m1713820127 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::HasEvent(System.String)
extern "C"  bool Fsm_HasEvent_m1208441265 (Fsm_t917886356 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ProcessEvent(HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventData)
extern "C"  void Fsm_ProcessEvent_m3610580600 (Fsm_t917886356 * __this, FsmEvent_t1258573736 * ___fsmEvent0, FsmEventData_t2110469976 * ___eventData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetEventDataSentByInfo()
extern "C"  void Fsm_SetEventDataSentByInfo_m323261414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetEventDataSentByInfo(HutongGames.PlayMaker.FsmEventData)
extern "C"  void Fsm_SetEventDataSentByInfo_m3425162710 (Il2CppObject * __this /* static, unused */, FsmEventData_t2110469976 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventData HutongGames.PlayMaker.Fsm::GetEventDataSentByInfo()
extern "C"  FsmEventData_t2110469976 * Fsm_GetEventDataSentByInfo_m868970127 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEventTarget,System.String)
extern "C"  void Fsm_Event_m1156864510 (Fsm_t917886356 * __this, FsmEventTarget_t172293745 * ___eventTarget0, String_t* ___fsmEventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEventTarget,HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_Event_m135555812 (Fsm_t917886356 * __this, FsmEventTarget_t172293745 * ___eventTarget0, FsmEvent_t1258573736 * ___fsmEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(System.String)
extern "C"  void Fsm_Event_m603178779 (Fsm_t917886356 * __this, String_t* ___fsmEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_Event_m4079224475 (Fsm_t917886356 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Fsm::DelayedEvent(HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  DelayedEvent_t1624700828 * Fsm_DelayedEvent_m1521897107 (Fsm_t917886356 * __this, FsmEvent_t1258573736 * ___fsmEvent0, float ___delay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.DelayedEvent HutongGames.PlayMaker.Fsm::DelayedEvent(HutongGames.PlayMaker.FsmEventTarget,HutongGames.PlayMaker.FsmEvent,System.Single)
extern "C"  DelayedEvent_t1624700828 * Fsm_DelayedEvent_m1247884586 (Fsm_t917886356 * __this, FsmEventTarget_t172293745 * ___eventTarget0, FsmEvent_t1258573736 * ___fsmEvent1, float ___delay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEvent(System.String,System.Boolean)
extern "C"  void Fsm_BroadcastEvent_m1761250535 (Fsm_t917886356 * __this, String_t* ___fsmEventName0, bool ___excludeSelf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEvent(HutongGames.PlayMaker.FsmEvent,System.Boolean)
extern "C"  void Fsm_BroadcastEvent_m3906091451 (Fsm_t917886356 * __this, FsmEvent_t1258573736 * ___fsmEvent0, bool ___excludeSelf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEventToGameObject(UnityEngine.GameObject,System.String,System.Boolean,System.Boolean)
extern "C"  void Fsm_BroadcastEventToGameObject_m3377334640 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___go0, String_t* ___fsmEventName1, bool ___sendToChildren2, bool ___excludeSelf3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::BroadcastEventToGameObject(UnityEngine.GameObject,HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventData,System.Boolean,System.Boolean)
extern "C"  void Fsm_BroadcastEventToGameObject_m3709301772 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___go0, FsmEvent_t1258573736 * ___fsmEvent1, FsmEventData_t2110469976 * ___eventData2, bool ___sendToChildren3, bool ___excludeSelf4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SendEventToFsmOnGameObject(UnityEngine.GameObject,System.String,System.String)
extern "C"  void Fsm_SendEventToFsmOnGameObject_m2418098184 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___gameObject0, String_t* ___fsmName1, String_t* ___fsmEventName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SendEventToFsmOnGameObject(UnityEngine.GameObject,System.String,HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_SendEventToFsmOnGameObject_m4026988562 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___gameObject0, String_t* ___fsmName1, FsmEvent_t1258573736 * ___fsmEvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetState(System.String)
extern "C"  void Fsm_SetState_m2915332810 (Fsm_t917886356 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateStateChanges()
extern "C"  void Fsm_UpdateStateChanges_m302729058 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::DoTransition(HutongGames.PlayMaker.FsmTransition,System.Boolean)
extern "C"  bool Fsm_DoTransition_m3549100543 (Fsm_t917886356 * __this, FsmTransition_t1534990431 * ___transition0, bool ___isGlobal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SwitchState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_SwitchState_m3688218433 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___toState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::GotoPreviousState()
extern "C"  void Fsm_GotoPreviousState_m3737212102 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::EnterState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_EnterState_m1495630231 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixedUpdateState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_FixedUpdateState_m3209370006 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::UpdateState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_UpdateState_m992309812 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::LateUpdateState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_LateUpdateState_m1022022488 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::ExitState(HutongGames.PlayMaker.FsmState)
extern "C"  void Fsm_ExitState_m2872762335 (Fsm_t917886356 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.Fsm::GetSubFsm(System.String)
extern "C"  Fsm_t917886356 * Fsm_GetSubFsm_m3332232070 (Fsm_t917886356 * __this, String_t* ___subFsmName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::GetFullFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* Fsm_GetFullFsmLabel_m3600599001 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Fsm::GetOwnerDefaultTarget(HutongGames.PlayMaker.FsmOwnerDefault)
extern "C"  GameObject_t1756533147 * Fsm_GetOwnerDefaultTarget_m1045900479 (Fsm_t917886356 * __this, FsmOwnerDefault_t2023674184 * ___ownerDefault0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::GetState(System.String)
extern "C"  FsmState_t1643911659 * Fsm_GetState_m3976212846 (Fsm_t917886356 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Fsm::GetEvent(System.String)
extern "C"  FsmEvent_t1258573736 * Fsm_GetEvent_m847933550 (Fsm_t917886356 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Fsm::CompareTo(System.Object)
extern "C"  int32_t Fsm_CompareTo_m980341957 (Fsm_t917886356 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Fsm::GetFsmObject(System.String)
extern "C"  FsmObject_t2785794313 * Fsm_GetFsmObject_m257472990 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Fsm::GetFsmMaterial(System.String)
extern "C"  FsmMaterial_t1421632035 * Fsm_GetFsmMaterial_m3052414910 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Fsm::GetFsmTexture(System.String)
extern "C"  FsmTexture_t3372293163 * Fsm_GetFsmTexture_m229123132 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Fsm::GetFsmFloat(System.String)
extern "C"  FsmFloat_t937133978 * Fsm_GetFsmFloat_m123979582 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Fsm::GetFsmInt(System.String)
extern "C"  FsmInt_t1273009179 * Fsm_GetFsmInt_m3249108964 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Fsm::GetFsmBool(System.String)
extern "C"  FsmBool_t664485696 * Fsm_GetFsmBool_m2314359390 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Fsm::GetFsmString(System.String)
extern "C"  FsmString_t2414474701 * Fsm_GetFsmString_m2643590558 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Fsm::GetFsmVector2(System.String)
extern "C"  FsmVector2_t2430450063 * Fsm_GetFsmVector2_m1347740308 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Fsm::GetFsmVector3(System.String)
extern "C"  FsmVector3_t3996534004 * Fsm_GetFsmVector3_m27292378 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Fsm::GetFsmRect(System.String)
extern "C"  FsmRect_t19023354 * Fsm_GetFsmRect_m4285421086 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Fsm::GetFsmQuaternion(System.String)
extern "C"  FsmQuaternion_t878438756 * Fsm_GetFsmQuaternion_m3448925598 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Fsm::GetFsmColor(System.String)
extern "C"  FsmColor_t118301965 * Fsm_GetFsmColor_m4082708848 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Fsm::GetFsmGameObject(System.String)
extern "C"  FsmGameObject_t3097142863 * Fsm_GetFsmGameObject_m3476008094 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Fsm::GetFsmArray(System.String)
extern "C"  FsmArray_t527459893 * Fsm_GetFsmArray_m1088320936 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Fsm::GetFsmEnum(System.String)
extern "C"  FsmEnum_t2808516103 * Fsm_GetFsmEnum_m824582078 (Fsm_t917886356 * __this, String_t* ___varName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDrawGizmos()
extern "C"  void Fsm_OnDrawGizmos_m1898914053 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDrawGizmosSelected()
extern "C"  void Fsm_OnDrawGizmosSelected_m1028153160 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionEnter_m809557319 (Fsm_t917886356 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionStay_m3817365356 (Fsm_t917886356 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionExit_m2954176683 (Fsm_t917886356 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerEnter_m2846356605 (Fsm_t917886356 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerStay_m3815556686 (Fsm_t917886356 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerExit_m3786251381 (Fsm_t917886356 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void Fsm_OnParticleCollision_m784661054 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionEnter2D_m1740000423 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionStay2D_m1881561292 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionExit2D_m1642132107 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerEnter2D_m774284857 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerStay2D_m1827537318 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerExit2D_m616971321 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void Fsm_OnControllerColliderHit_m450115771 (Fsm_t917886356 * __this, ControllerColliderHit_t4070855101 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak(System.Single)
extern "C"  void Fsm_OnJointBreak_m3216038832 (Fsm_t917886356 * __this, float ___breakForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  void Fsm_OnJointBreak2D_m2409422232 (Fsm_t917886356 * __this, Joint2D_t854621618 * ___brokenJoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorMove()
extern "C"  void Fsm_OnAnimatorMove_m169525216 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorIK(System.Int32)
extern "C"  void Fsm_OnAnimatorIK_m791829240 (Fsm_t917886356 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnGUI()
extern "C"  void Fsm_OnGUI_m3260270399 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::DoBreakpoint()
extern "C"  void Fsm_DoBreakpoint_m4154047037 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::DoBreakError(System.String)
extern "C"  void Fsm_DoBreakError_m3817016507 (Fsm_t917886356 * __this, String_t* ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::DoBreak()
extern "C"  void Fsm_DoBreak_m4051632113 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Continue()
extern "C"  void Fsm_Continue_m1944178068 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDestroy()
extern "C"  void Fsm_OnDestroy_m940690992 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.cctor()
extern "C"  void Fsm__cctor_m2444360684 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
