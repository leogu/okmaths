﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRotation
struct SetRotation_t886184892;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRotation::.ctor()
extern "C"  void SetRotation__ctor_m721680672 (SetRotation_t886184892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::Reset()
extern "C"  void SetRotation_Reset_m2998884501 (SetRotation_t886184892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnEnter()
extern "C"  void SetRotation_OnEnter_m1715143229 (SetRotation_t886184892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnUpdate()
extern "C"  void SetRotation_OnUpdate_m2115295142 (SetRotation_t886184892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::OnLateUpdate()
extern "C"  void SetRotation_OnLateUpdate_m356508802 (SetRotation_t886184892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRotation::DoSetRotation()
extern "C"  void SetRotation_DoSetRotation_m3013141453 (SetRotation_t886184892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
