﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3709210170.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2680906638.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput2561058385.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3236822917.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc249603239.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1328781136.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3293839588.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2986189219.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2824271922.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers3244928895.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSu2828018738.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate1528800019.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry1780385998.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2652774230.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls1409660779.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Reso2975512894.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownIte4139978805.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataL2653737080.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedD2299200057.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec_167183231.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData2614388407.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker2633059652.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Blo2548930813.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry377833367.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type3352948571.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod1640962579.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizont1880137149.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical3595376133.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin902486598028.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin1803744816572.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin3603462491556.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1028629049.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType1274231802.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_Character3437478890.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType2931319356.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEven907918422.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeE2863344003.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState1111987863.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidat1946318473.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretBl503613599.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseD1840153439.h"
#include "UnityEngine_UI_UnityEngine_UI_Mask2977958238.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic540192618.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_Cull3778758259.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities1936577068.h"
#include "UnityEngine_UI_UnityEngine_UI_Misc2977957982.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1571958496.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1081683921.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage2749640213.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D1156185964.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar3248359358.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction3696775921.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEven1794825321.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis2427050347.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1806[10] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_4(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_5(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_6(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_7(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_8(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_9(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_10(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[20] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1813[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[21] = 
{
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_2(),
	PointerEventData_t1599784723::get_offset_of_hovered_3(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1821[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_10(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (MouseButtonEventData_t3709210170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[2] = 
{
	MouseButtonEventData_t3709210170::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3709210170::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (StandaloneInputModule_t70867863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[12] = 
{
	StandaloneInputModule_t70867863::get_offset_of_m_PrevActionTime_12(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMoveVector_13(),
	StandaloneInputModule_t70867863::get_offset_of_m_ConsecutiveMoveCount_14(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMousePosition_15(),
	StandaloneInputModule_t70867863::get_offset_of_m_MousePosition_16(),
	StandaloneInputModule_t70867863::get_offset_of_m_HorizontalAxis_17(),
	StandaloneInputModule_t70867863::get_offset_of_m_VerticalAxis_18(),
	StandaloneInputModule_t70867863::get_offset_of_m_SubmitButton_19(),
	StandaloneInputModule_t70867863::get_offset_of_m_CancelButton_20(),
	StandaloneInputModule_t70867863::get_offset_of_m_InputActionsPerSecond_21(),
	StandaloneInputModule_t70867863::get_offset_of_m_RepeatDelay_22(),
	StandaloneInputModule_t70867863::get_offset_of_m_ForceModuleActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (InputMode_t2680906638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[3] = 
{
	InputMode_t2680906638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (TouchInputModule_t2561058385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[3] = 
{
	TouchInputModule_t2561058385::get_offset_of_m_LastMousePosition_12(),
	TouchInputModule_t2561058385::get_offset_of_m_MousePosition_13(),
	TouchInputModule_t2561058385::get_offset_of_m_ForceModuleActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (BaseRaycaster_t2336171397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (Physics2DRaycaster_t3236822917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (PhysicsRaycaster_t249603239), -1, sizeof(PhysicsRaycaster_t249603239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[4] = 
{
	0,
	PhysicsRaycaster_t249603239::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t249603239::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t249603239_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (ColorTween_t3438117476)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[6] = 
{
	ColorTween_t3438117476::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (ColorTweenMode_t1328781136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	ColorTweenMode_t1328781136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (ColorTweenCallback_t3293839588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (FloatTween_t2986189219)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[5] = 
{
	FloatTween_t2986189219::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (FloatTweenCallback_t2824271922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (AnimationTriggers_t3244928895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t3244928895::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t3244928895::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t3244928895::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t3244928895::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (Button_t2872111280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[1] = 
{
	Button_t2872111280::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (ButtonClickedEvent_t2455055323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (U3COnFinishSubmitU3Ec__Iterator1_t2828018738), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[5] = 
{
	U3COnFinishSubmitU3Ec__Iterator1_t2828018738::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator1_t2828018738::get_offset_of_U3CelapsedTimeU3E__1_1(),
	U3COnFinishSubmitU3Ec__Iterator1_t2828018738::get_offset_of_U24PC_2(),
	U3COnFinishSubmitU3Ec__Iterator1_t2828018738::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator1_t2828018738::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (CanvasUpdate_t1528800019)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1846[7] = 
{
	CanvasUpdate_t1528800019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (CanvasUpdateRegistry_t1780385998), -1, sizeof(CanvasUpdateRegistry_t1780385998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1848[6] = 
{
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ColorBlock_t2652774230)+ sizeof (Il2CppObject), sizeof(ColorBlock_t2652774230_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1849[6] = 
{
	ColorBlock_t2652774230::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (DefaultControls_t1409660779), -1, sizeof(DefaultControls_t1409660779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1850[9] = 
{
	0,
	0,
	0,
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_TextColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Resources_t2975512894)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[7] = 
{
	Resources_t2975512894::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (Dropdown_t1985816271), -1, sizeof(Dropdown_t1985816271_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1852[14] = 
{
	Dropdown_t1985816271::get_offset_of_m_Template_16(),
	Dropdown_t1985816271::get_offset_of_m_CaptionText_17(),
	Dropdown_t1985816271::get_offset_of_m_CaptionImage_18(),
	Dropdown_t1985816271::get_offset_of_m_ItemText_19(),
	Dropdown_t1985816271::get_offset_of_m_ItemImage_20(),
	Dropdown_t1985816271::get_offset_of_m_Value_21(),
	Dropdown_t1985816271::get_offset_of_m_Options_22(),
	Dropdown_t1985816271::get_offset_of_m_OnValueChanged_23(),
	Dropdown_t1985816271::get_offset_of_m_Dropdown_24(),
	Dropdown_t1985816271::get_offset_of_m_Blocker_25(),
	Dropdown_t1985816271::get_offset_of_m_Items_26(),
	Dropdown_t1985816271::get_offset_of_m_AlphaTweenRunner_27(),
	Dropdown_t1985816271::get_offset_of_validTemplate_28(),
	Dropdown_t1985816271_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (DropdownItem_t4139978805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[4] = 
{
	DropdownItem_t4139978805::get_offset_of_m_Text_2(),
	DropdownItem_t4139978805::get_offset_of_m_Image_3(),
	DropdownItem_t4139978805::get_offset_of_m_RectTransform_4(),
	DropdownItem_t4139978805::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (OptionData_t2420267500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[2] = 
{
	OptionData_t2420267500::get_offset_of_m_Text_0(),
	OptionData_t2420267500::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (OptionDataList_t2653737080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[1] = 
{
	OptionDataList_t2653737080::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (DropdownEvent_t2203087800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[6] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057::get_offset_of_U3CiU3E__0_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057::get_offset_of_U24PC_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057::get_offset_of_U24current_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057::get_offset_of_U3CU24U3Edelay_4(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_t2299200057::get_offset_of_U3CU3Ef__this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (U3CShowU3Ec__AnonStorey6_t167183231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[2] = 
{
	U3CShowU3Ec__AnonStorey6_t167183231::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey6_t167183231::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (FontData_t2614388407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1859[12] = 
{
	FontData_t2614388407::get_offset_of_m_Font_0(),
	FontData_t2614388407::get_offset_of_m_FontSize_1(),
	FontData_t2614388407::get_offset_of_m_FontStyle_2(),
	FontData_t2614388407::get_offset_of_m_BestFit_3(),
	FontData_t2614388407::get_offset_of_m_MinSize_4(),
	FontData_t2614388407::get_offset_of_m_MaxSize_5(),
	FontData_t2614388407::get_offset_of_m_Alignment_6(),
	FontData_t2614388407::get_offset_of_m_AlignByGeometry_7(),
	FontData_t2614388407::get_offset_of_m_RichText_8(),
	FontData_t2614388407::get_offset_of_m_HorizontalOverflow_9(),
	FontData_t2614388407::get_offset_of_m_VerticalOverflow_10(),
	FontData_t2614388407::get_offset_of_m_LineSpacing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (FontUpdateTracker_t2633059652), -1, sizeof(FontUpdateTracker_t2633059652_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1860[1] = 
{
	FontUpdateTracker_t2633059652_StaticFields::get_offset_of_m_Tracked_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Graphic_t2426225576), -1, sizeof(Graphic_t2426225576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1861[17] = 
{
	Graphic_t2426225576_StaticFields::get_offset_of_s_DefaultUI_2(),
	Graphic_t2426225576_StaticFields::get_offset_of_s_WhiteTexture_3(),
	Graphic_t2426225576::get_offset_of_m_Material_4(),
	Graphic_t2426225576::get_offset_of_m_Color_5(),
	Graphic_t2426225576::get_offset_of_m_RaycastTarget_6(),
	Graphic_t2426225576::get_offset_of_m_RectTransform_7(),
	Graphic_t2426225576::get_offset_of_m_CanvasRender_8(),
	Graphic_t2426225576::get_offset_of_m_Canvas_9(),
	Graphic_t2426225576::get_offset_of_m_VertsDirty_10(),
	Graphic_t2426225576::get_offset_of_m_MaterialDirty_11(),
	Graphic_t2426225576::get_offset_of_m_OnDirtyLayoutCallback_12(),
	Graphic_t2426225576::get_offset_of_m_OnDirtyVertsCallback_13(),
	Graphic_t2426225576::get_offset_of_m_OnDirtyMaterialCallback_14(),
	Graphic_t2426225576_StaticFields::get_offset_of_s_Mesh_15(),
	Graphic_t2426225576_StaticFields::get_offset_of_s_VertexHelper_16(),
	Graphic_t2426225576::get_offset_of_m_ColorTweenRunner_17(),
	Graphic_t2426225576::get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (GraphicRaycaster_t410733016), -1, sizeof(GraphicRaycaster_t410733016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[8] = 
{
	0,
	GraphicRaycaster_t410733016::get_offset_of_m_IgnoreReversedGraphics_3(),
	GraphicRaycaster_t410733016::get_offset_of_m_BlockingObjects_4(),
	GraphicRaycaster_t410733016::get_offset_of_m_BlockingMask_5(),
	GraphicRaycaster_t410733016::get_offset_of_m_Canvas_6(),
	GraphicRaycaster_t410733016::get_offset_of_m_RaycastResults_7(),
	GraphicRaycaster_t410733016_StaticFields::get_offset_of_s_SortedGraphics_8(),
	GraphicRaycaster_t410733016_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (BlockingObjects_t2548930813)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1863[5] = 
{
	BlockingObjects_t2548930813::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (GraphicRegistry_t377833367), -1, sizeof(GraphicRegistry_t377833367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1864[3] = 
{
	GraphicRegistry_t377833367_StaticFields::get_offset_of_s_Instance_0(),
	GraphicRegistry_t377833367::get_offset_of_m_Graphics_1(),
	GraphicRegistry_t377833367_StaticFields::get_offset_of_s_EmptyList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (Image_t2042527209), -1, sizeof(Image_t2042527209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1866[15] = 
{
	Image_t2042527209_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	Image_t2042527209::get_offset_of_m_Sprite_29(),
	Image_t2042527209::get_offset_of_m_OverrideSprite_30(),
	Image_t2042527209::get_offset_of_m_Type_31(),
	Image_t2042527209::get_offset_of_m_PreserveAspect_32(),
	Image_t2042527209::get_offset_of_m_FillCenter_33(),
	Image_t2042527209::get_offset_of_m_FillMethod_34(),
	Image_t2042527209::get_offset_of_m_FillAmount_35(),
	Image_t2042527209::get_offset_of_m_FillClockwise_36(),
	Image_t2042527209::get_offset_of_m_FillOrigin_37(),
	Image_t2042527209::get_offset_of_m_AlphaHitTestMinimumThreshold_38(),
	Image_t2042527209_StaticFields::get_offset_of_s_VertScratch_39(),
	Image_t2042527209_StaticFields::get_offset_of_s_UVScratch_40(),
	Image_t2042527209_StaticFields::get_offset_of_s_Xy_41(),
	Image_t2042527209_StaticFields::get_offset_of_s_Uv_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (Type_t3352948571)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[5] = 
{
	Type_t3352948571::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (FillMethod_t1640962579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1868[6] = 
{
	FillMethod_t1640962579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (OriginHorizontal_t1880137149)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[3] = 
{
	OriginHorizontal_t1880137149::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (OriginVertical_t3595376133)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1870[3] = 
{
	OriginVertical_t3595376133::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (Origin90_t2486598028)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1871[5] = 
{
	Origin90_t2486598028::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (Origin180_t3744816572)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1872[5] = 
{
	Origin180_t3744816572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (Origin360_t3462491556)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1873[5] = 
{
	Origin360_t3462491556::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (InputField_t1631627530), -1, sizeof(InputField_t1631627530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1876[48] = 
{
	0,
	0,
	0,
	InputField_t1631627530::get_offset_of_m_Keyboard_19(),
	InputField_t1631627530_StaticFields::get_offset_of_kSeparators_20(),
	InputField_t1631627530::get_offset_of_m_TextComponent_21(),
	InputField_t1631627530::get_offset_of_m_Placeholder_22(),
	InputField_t1631627530::get_offset_of_m_ContentType_23(),
	InputField_t1631627530::get_offset_of_m_InputType_24(),
	InputField_t1631627530::get_offset_of_m_AsteriskChar_25(),
	InputField_t1631627530::get_offset_of_m_KeyboardType_26(),
	InputField_t1631627530::get_offset_of_m_LineType_27(),
	InputField_t1631627530::get_offset_of_m_HideMobileInput_28(),
	InputField_t1631627530::get_offset_of_m_CharacterValidation_29(),
	InputField_t1631627530::get_offset_of_m_CharacterLimit_30(),
	InputField_t1631627530::get_offset_of_m_OnEndEdit_31(),
	InputField_t1631627530::get_offset_of_m_OnValueChanged_32(),
	InputField_t1631627530::get_offset_of_m_OnValidateInput_33(),
	InputField_t1631627530::get_offset_of_m_CaretColor_34(),
	InputField_t1631627530::get_offset_of_m_CustomCaretColor_35(),
	InputField_t1631627530::get_offset_of_m_SelectionColor_36(),
	InputField_t1631627530::get_offset_of_m_Text_37(),
	InputField_t1631627530::get_offset_of_m_CaretBlinkRate_38(),
	InputField_t1631627530::get_offset_of_m_CaretWidth_39(),
	InputField_t1631627530::get_offset_of_m_ReadOnly_40(),
	InputField_t1631627530::get_offset_of_m_CaretPosition_41(),
	InputField_t1631627530::get_offset_of_m_CaretSelectPosition_42(),
	InputField_t1631627530::get_offset_of_caretRectTrans_43(),
	InputField_t1631627530::get_offset_of_m_CursorVerts_44(),
	InputField_t1631627530::get_offset_of_m_InputTextCache_45(),
	InputField_t1631627530::get_offset_of_m_CachedInputRenderer_46(),
	InputField_t1631627530::get_offset_of_m_PreventFontCallback_47(),
	InputField_t1631627530::get_offset_of_m_Mesh_48(),
	InputField_t1631627530::get_offset_of_m_AllowInput_49(),
	InputField_t1631627530::get_offset_of_m_ShouldActivateNextUpdate_50(),
	InputField_t1631627530::get_offset_of_m_UpdateDrag_51(),
	InputField_t1631627530::get_offset_of_m_DragPositionOutOfBounds_52(),
	InputField_t1631627530::get_offset_of_m_CaretVisible_53(),
	InputField_t1631627530::get_offset_of_m_BlinkCoroutine_54(),
	InputField_t1631627530::get_offset_of_m_BlinkStartTime_55(),
	InputField_t1631627530::get_offset_of_m_DrawStart_56(),
	InputField_t1631627530::get_offset_of_m_DrawEnd_57(),
	InputField_t1631627530::get_offset_of_m_DragCoroutine_58(),
	InputField_t1631627530::get_offset_of_m_OriginalText_59(),
	InputField_t1631627530::get_offset_of_m_WasCanceled_60(),
	InputField_t1631627530::get_offset_of_m_HasDoneFocusTransition_61(),
	InputField_t1631627530::get_offset_of_m_ProcessingEvent_62(),
	InputField_t1631627530_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_63(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (ContentType_t1028629049)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1877[11] = 
{
	ContentType_t1028629049::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (InputType_t1274231802)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1878[4] = 
{
	InputType_t1274231802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (CharacterValidation_t3437478890)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1879[7] = 
{
	CharacterValidation_t3437478890::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (LineType_t2931319356)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[4] = 
{
	LineType_t2931319356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (SubmitEvent_t907918422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (OnChangeEvent_t2863344003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (EditState_t1111987863)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[3] = 
{
	EditState_t1111987863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (OnValidateInput_t1946318473), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (U3CCaretBlinkU3Ec__Iterator3_t503613599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[5] = 
{
	U3CCaretBlinkU3Ec__Iterator3_t503613599::get_offset_of_U3CblinkPeriodU3E__0_0(),
	U3CCaretBlinkU3Ec__Iterator3_t503613599::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator3_t503613599::get_offset_of_U24PC_2(),
	U3CCaretBlinkU3Ec__Iterator3_t503613599::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator3_t503613599::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U3ClocalMousePosU3E__0_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U3CdelayU3E__2_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U24PC_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U3CU24U3EeventData_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator4_t1840153439::get_offset_of_U3CU3Ef__this_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (Mask_t2977958238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[5] = 
{
	Mask_t2977958238::get_offset_of_m_RectTransform_2(),
	Mask_t2977958238::get_offset_of_m_ShowMaskGraphic_3(),
	Mask_t2977958238::get_offset_of_m_Graphic_4(),
	Mask_t2977958238::get_offset_of_m_MaskMaterial_5(),
	Mask_t2977958238::get_offset_of_m_UnmaskMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (MaskableGraphic_t540192618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[9] = 
{
	MaskableGraphic_t540192618::get_offset_of_m_ShouldRecalculateStencil_19(),
	MaskableGraphic_t540192618::get_offset_of_m_MaskMaterial_20(),
	MaskableGraphic_t540192618::get_offset_of_m_ParentMask_21(),
	MaskableGraphic_t540192618::get_offset_of_m_Maskable_22(),
	MaskableGraphic_t540192618::get_offset_of_m_IncludeForMasking_23(),
	MaskableGraphic_t540192618::get_offset_of_m_OnCullStateChanged_24(),
	MaskableGraphic_t540192618::get_offset_of_m_ShouldRecalculate_25(),
	MaskableGraphic_t540192618::get_offset_of_m_StencilValue_26(),
	MaskableGraphic_t540192618::get_offset_of_m_Corners_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (CullStateChangedEvent_t3778758259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (MaskUtilities_t1936577068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (Misc_t2977957982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (Navigation_t1571958496)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[5] = 
{
	Navigation_t1571958496::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (Mode_t1081683921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1893[6] = 
{
	Mode_t1081683921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (RawImage_t2749640213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[2] = 
{
	RawImage_t2749640213::get_offset_of_m_Texture_28(),
	RawImage_t2749640213::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (RectMask2D_t1156185964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[8] = 
{
	RectMask2D_t1156185964::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t1156185964::get_offset_of_m_RectTransform_3(),
	RectMask2D_t1156185964::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t1156185964::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t1156185964::get_offset_of_m_Clippers_6(),
	RectMask2D_t1156185964::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t1156185964::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t1156185964::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (Scrollbar_t3248359358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[11] = 
{
	Scrollbar_t3248359358::get_offset_of_m_HandleRect_16(),
	Scrollbar_t3248359358::get_offset_of_m_Direction_17(),
	Scrollbar_t3248359358::get_offset_of_m_Value_18(),
	Scrollbar_t3248359358::get_offset_of_m_Size_19(),
	Scrollbar_t3248359358::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t3248359358::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t3248359358::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t3248359358::get_offset_of_m_Offset_23(),
	Scrollbar_t3248359358::get_offset_of_m_Tracker_24(),
	Scrollbar_t3248359358::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t3248359358::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (Direction_t3696775921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1897[5] = 
{
	Direction_t3696775921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ScrollEvent_t1794825321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (Axis_t2427050347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1899[3] = 
{
	Axis_t2427050347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
