﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionRotateTowards
struct QuaternionRotateTowards_t3565674165;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::.ctor()
extern "C"  void QuaternionRotateTowards__ctor_m3466382535 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::Reset()
extern "C"  void QuaternionRotateTowards_Reset_m3352987562 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnEnter()
extern "C"  void QuaternionRotateTowards_OnEnter_m2702000268 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnUpdate()
extern "C"  void QuaternionRotateTowards_OnUpdate_m1824230567 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnLateUpdate()
extern "C"  void QuaternionRotateTowards_OnLateUpdate_m2462791091 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::OnFixedUpdate()
extern "C"  void QuaternionRotateTowards_OnFixedUpdate_m1927350325 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionRotateTowards::DoQuatRotateTowards()
extern "C"  void QuaternionRotateTowards_DoQuatRotateTowards_m2646640622 (QuaternionRotateTowards_t3565674165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
