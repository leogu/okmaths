﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetComponent
struct GetComponent_t1894953309;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetComponent::.ctor()
extern "C"  void GetComponent__ctor_m2567467043 (GetComponent_t1894953309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::Reset()
extern "C"  void GetComponent_Reset_m1127008970 (GetComponent_t1894953309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::OnEnter()
extern "C"  void GetComponent_OnEnter_m2502537092 (GetComponent_t1894953309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::OnUpdate()
extern "C"  void GetComponent_OnUpdate_m3025543171 (GetComponent_t1894953309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetComponent::DoGetComponent()
extern "C"  void GetComponent_DoGetComponent_m3920002241 (GetComponent_t1894953309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
