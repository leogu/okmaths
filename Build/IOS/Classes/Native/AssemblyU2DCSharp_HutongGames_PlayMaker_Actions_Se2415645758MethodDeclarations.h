﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIColor
struct SetGUIColor_t2415645758;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::.ctor()
extern "C"  void SetGUIColor__ctor_m3287948236 (SetGUIColor_t2415645758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::Reset()
extern "C"  void SetGUIColor_Reset_m2171367895 (SetGUIColor_t2415645758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIColor::OnGUI()
extern "C"  void SetGUIColor_OnGUI_m2300497880 (SetGUIColor_t2415645758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
