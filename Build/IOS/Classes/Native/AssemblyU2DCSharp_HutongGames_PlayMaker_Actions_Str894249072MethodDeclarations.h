﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringContains
struct StringContains_t894249072;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringContains::.ctor()
extern "C"  void StringContains__ctor_m3608839300 (StringContains_t894249072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::Reset()
extern "C"  void StringContains_Reset_m2539460181 (StringContains_t894249072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::OnEnter()
extern "C"  void StringContains_OnEnter_m2637192277 (StringContains_t894249072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::OnUpdate()
extern "C"  void StringContains_OnUpdate_m2805757938 (StringContains_t894249072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringContains::DoStringContains()
extern "C"  void StringContains_DoStringContains_m4199563041 (StringContains_t894249072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
