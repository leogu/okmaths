﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector3XYZ
struct SetVector3XYZ_t719726163;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::.ctor()
extern "C"  void SetVector3XYZ__ctor_m3854437865 (SetVector3XYZ_t719726163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::Reset()
extern "C"  void SetVector3XYZ_Reset_m4662668 (SetVector3XYZ_t719726163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::OnEnter()
extern "C"  void SetVector3XYZ_OnEnter_m666151470 (SetVector3XYZ_t719726163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::OnUpdate()
extern "C"  void SetVector3XYZ_OnUpdate_m2411070469 (SetVector3XYZ_t719726163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector3XYZ::DoSetVector3XYZ()
extern "C"  void SetVector3XYZ_DoSetVector3XYZ_m42872705 (SetVector3XYZ_t719726163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
