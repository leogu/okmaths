﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtGameObject
struct DOTweenRigidbodyLookAtGameObject_t2583017160;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtGameObject::.ctor()
extern "C"  void DOTweenRigidbodyLookAtGameObject__ctor_m3674579512 (DOTweenRigidbodyLookAtGameObject_t2583017160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtGameObject::Reset()
extern "C"  void DOTweenRigidbodyLookAtGameObject_Reset_m2842901049 (DOTweenRigidbodyLookAtGameObject_t2583017160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtGameObject::OnEnter()
extern "C"  void DOTweenRigidbodyLookAtGameObject_OnEnter_m2778943297 (DOTweenRigidbodyLookAtGameObject_t2583017160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtGameObject::<OnEnter>m__84()
extern "C"  void DOTweenRigidbodyLookAtGameObject_U3COnEnterU3Em__84_m506696544 (DOTweenRigidbodyLookAtGameObject_t2583017160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtGameObject::<OnEnter>m__85()
extern "C"  void DOTweenRigidbodyLookAtGameObject_U3COnEnterU3Em__85_m3255782875 (DOTweenRigidbodyLookAtGameObject_t2583017160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
