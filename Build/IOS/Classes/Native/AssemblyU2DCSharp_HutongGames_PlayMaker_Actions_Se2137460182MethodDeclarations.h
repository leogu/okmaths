﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendEvent
struct SendEvent_t2137460182;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendEvent::.ctor()
extern "C"  void SendEvent__ctor_m2446685344 (SendEvent_t2137460182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEvent::Reset()
extern "C"  void SendEvent_Reset_m2318929875 (SendEvent_t2137460182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEvent::OnEnter()
extern "C"  void SendEvent_OnEnter_m2605175355 (SendEvent_t2137460182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEvent::OnUpdate()
extern "C"  void SendEvent_OnUpdate_m395627974 (SendEvent_t2137460182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
