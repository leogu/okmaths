﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// UnityEngine.NetworkPlayer[]
struct NetworkPlayerU5BU5D_t2821705394;
// UnityEngine.Object
struct Object_t1021602117;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1607866742.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel1480423150.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "UnityEngine_UnityEngine_NetworkPeerType2458303118.h"

// UnityEngine.NetworkConnectionError UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t Network_InitializeServer_m492610505 (Il2CppObject * __this /* static, unused */, int32_t ___connections0, int32_t ___listenPort1, bool ___useNat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_incomingPassword(System.String)
extern "C"  void Network_set_incomingPassword_m3038067325 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)
extern "C"  void Network_set_logLevel_m786315563 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::InitializeSecurity()
extern "C"  void Network_InitializeSecurity_m1229341969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkConnectionError UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)
extern "C"  int32_t Network_Internal_ConnectToSingleIP_m1433145057 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, int32_t ___localPort2, String_t* ___password3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkConnectionError UnityEngine.Network::Connect(System.String,System.Int32,System.String)
extern "C"  int32_t Network_Connect_m294459100 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, String_t* ___password2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Disconnect(System.Int32)
extern "C"  void Network_Disconnect_m2376044840 (Il2CppObject * __this /* static, unused */, int32_t ___timeout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Disconnect()
extern "C"  void Network_Disconnect_m607059163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)
extern "C"  void Network_CloseConnection_m2751000432 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___target0, bool ___sendDisconnectionNotification1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer[] UnityEngine.Network::get_connections()
extern "C"  NetworkPlayerU5BU5D_t2821705394* Network_get_connections_m538736002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::Internal_GetPlayer()
extern "C"  int32_t Network_Internal_GetPlayer_m2085539076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPlayer UnityEngine.Network::get_player()
extern "C"  NetworkPlayer_t1243528291  Network_get_player_m2218319246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Network::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  Object_t1021602117 * Network_Instantiate_m1019383205 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___prefab0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, int32_t ___group3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C"  Object_t1021602117 * Network_INTERNAL_CALL_Instantiate_m1722130588 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___prefab0, Vector3_t2243707580 * ___position1, Quaternion_t4030073918 * ___rotation2, int32_t ___group3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)
extern "C"  void Network_DestroyPlayerObjects_m2401047830 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID,System.UInt32)
extern "C"  void Network_Internal_RemoveRPCs_m708688402 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, NetworkViewID_t3942988548  ___viewID1, uint32_t ___channelMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)
extern "C"  void Network_INTERNAL_CALL_Internal_RemoveRPCs_m2146465587 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, NetworkViewID_t3942988548 * ___viewID1, uint32_t ___channelMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkPlayer)
extern "C"  void Network_RemoveRPCs_m636089305 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkViewID)
extern "C"  void Network_RemoveRPCs_m2643009806 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___viewID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::get_isClient()
extern "C"  bool Network_get_isClient_m3471532805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::get_isServer()
extern "C"  bool Network_get_isServer_m4174143497 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NetworkPeerType UnityEngine.Network::get_peerType()
extern "C"  int32_t Network_get_peerType_m1422231822 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::SetLevelPrefix(System.Int32)
extern "C"  void Network_SetLevelPrefix_m387506644 (Il2CppObject * __this /* static, unused */, int32_t ___prefix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetLastPing_m350006847 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetAveragePing_m2710622608 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Network::get_sendRate()
extern "C"  float Network_get_sendRate_m2240556848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_sendRate(System.Single)
extern "C"  void Network_set_sendRate_m2532595313 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::get_isMessageQueueRunning()
extern "C"  bool Network_get_isMessageQueueRunning_m3067227829 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)
extern "C"  void Network_set_isMessageQueueRunning_m298779940 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::Internal_GetTime(System.Double&)
extern "C"  void Network_Internal_GetTime_m2548642614 (Il2CppObject * __this /* static, unused */, double* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UnityEngine.Network::get_time()
extern "C"  double Network_get_time_m955449524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::get_minimumAllocatableViewIDs()
extern "C"  int32_t Network_get_minimumAllocatableViewIDs_m1995281259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)
extern "C"  void Network_set_minimumAllocatableViewIDs_m832310412 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Network::HavePublicAddress()
extern "C"  bool Network_HavePublicAddress_m2110731574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Network::get_maxConnections()
extern "C"  int32_t Network_get_maxConnections_m2063963077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Network::set_maxConnections(System.Int32)
extern "C"  void Network_set_maxConnections_m3373629084 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
