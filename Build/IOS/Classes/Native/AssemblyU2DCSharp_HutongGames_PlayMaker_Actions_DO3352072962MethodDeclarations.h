﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime
struct DOTweenTrailRendererTime_t3352072962;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::.ctor()
extern "C"  void DOTweenTrailRendererTime__ctor_m2536693670 (DOTweenTrailRendererTime_t3352072962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::Reset()
extern "C"  void DOTweenTrailRendererTime_Reset_m571914919 (DOTweenTrailRendererTime_t3352072962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::OnEnter()
extern "C"  void DOTweenTrailRendererTime_OnEnter_m1381240311 (DOTweenTrailRendererTime_t3352072962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::<OnEnter>m__A4()
extern "C"  void DOTweenTrailRendererTime_U3COnEnterU3Em__A4_m2471597181 (DOTweenTrailRendererTime_t3352072962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTrailRendererTime::<OnEnter>m__A5()
extern "C"  void DOTweenTrailRendererTime_U3COnEnterU3Em__A5_m925716216 (DOTweenTrailRendererTime_t3352072962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
