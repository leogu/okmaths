﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t3371940140  : public Il2CppObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::target
	LayoutElement_t2808691390 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t3371940140, ___target_0)); }
	inline LayoutElement_t2808691390 * get_target_0() const { return ___target_0; }
	inline LayoutElement_t2808691390 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_t2808691390 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
