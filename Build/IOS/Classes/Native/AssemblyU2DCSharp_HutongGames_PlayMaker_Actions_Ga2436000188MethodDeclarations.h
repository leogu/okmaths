﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectIsNull
struct GameObjectIsNull_t2436000188;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::.ctor()
extern "C"  void GameObjectIsNull__ctor_m1296956388 (GameObjectIsNull_t2436000188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::Reset()
extern "C"  void GameObjectIsNull_Reset_m463917133 (GameObjectIsNull_t2436000188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::OnEnter()
extern "C"  void GameObjectIsNull_OnEnter_m484747381 (GameObjectIsNull_t2436000188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::OnUpdate()
extern "C"  void GameObjectIsNull_OnUpdate_m204692258 (GameObjectIsNull_t2436000188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectIsNull::DoIsGameObjectNull()
extern "C"  void GameObjectIsNull_DoIsGameObjectNull_m2426098145 (GameObjectIsNull_t2436000188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
