﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendEventToFsm
struct SendEventToFsm_t102492465;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::.ctor()
extern "C"  void SendEventToFsm__ctor_m1773923173 (SendEventToFsm_t102492465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::Reset()
extern "C"  void SendEventToFsm_Reset_m3734387806 (SendEventToFsm_t102492465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::OnEnter()
extern "C"  void SendEventToFsm_OnEnter_m1255039804 (SendEventToFsm_t102492465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendEventToFsm::OnUpdate()
extern "C"  void SendEventToFsm_OnUpdate_m178561745 (SendEventToFsm_t102492465 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
