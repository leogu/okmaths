﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenImageColor
struct DOTweenImageColor_t793182230;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenImageColor::.ctor()
extern "C"  void DOTweenImageColor__ctor_m1046827462 (DOTweenImageColor_t793182230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageColor::Reset()
extern "C"  void DOTweenImageColor_Reset_m1607372459 (DOTweenImageColor_t793182230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageColor::OnEnter()
extern "C"  void DOTweenImageColor_OnEnter_m1764196515 (DOTweenImageColor_t793182230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageColor::<OnEnter>m__3A()
extern "C"  void DOTweenImageColor_U3COnEnterU3Em__3A_m2246395502 (DOTweenImageColor_t793182230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenImageColor::<OnEnter>m__3B()
extern "C"  void DOTweenImageColor_U3COnEnterU3Em__3B_m2246395469 (DOTweenImageColor_t793182230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
