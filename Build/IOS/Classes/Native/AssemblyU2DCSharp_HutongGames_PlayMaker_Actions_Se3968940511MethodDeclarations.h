﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVelocity
struct SetVelocity_t3968940511;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVelocity::.ctor()
extern "C"  void SetVelocity__ctor_m3182373311 (SetVelocity_t3968940511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::Reset()
extern "C"  void SetVelocity_Reset_m3874127764 (SetVelocity_t3968940511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnPreprocess()
extern "C"  void SetVelocity_OnPreprocess_m4004545836 (SetVelocity_t3968940511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnEnter()
extern "C"  void SetVelocity_OnEnter_m2260705722 (SetVelocity_t3968940511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::OnFixedUpdate()
extern "C"  void SetVelocity_OnFixedUpdate_m318030101 (SetVelocity_t3968940511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVelocity::DoSetVelocity()
extern "C"  void SetVelocity_DoSetVelocity_m1014653661 (SetVelocity_t3968940511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
