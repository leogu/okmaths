﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseColor
struct EaseColor_t3804070093;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseColor::.ctor()
extern "C"  void EaseColor__ctor_m2724950889 (EaseColor_t3804070093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::Reset()
extern "C"  void EaseColor_Reset_m3468190670 (EaseColor_t3804070093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnEnter()
extern "C"  void EaseColor_OnEnter_m1271590140 (EaseColor_t3804070093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnExit()
extern "C"  void EaseColor_OnExit_m1026400672 (EaseColor_t3804070093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseColor::OnUpdate()
extern "C"  void EaseColor_OnUpdate_m3207568053 (EaseColor_t3804070093 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
