﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMouseY
struct GetMouseY_t2179746180;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMouseY::.ctor()
extern "C"  void GetMouseY__ctor_m2576691768 (GetMouseY_t2179746180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::Reset()
extern "C"  void GetMouseY_Reset_m1397869341 (GetMouseY_t2179746180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::OnEnter()
extern "C"  void GetMouseY_OnEnter_m3205833157 (GetMouseY_t2179746180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::OnUpdate()
extern "C"  void GetMouseY_OnUpdate_m1895571870 (GetMouseY_t2179746180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMouseY::DoGetMouseY()
extern "C"  void GetMouseY_DoGetMouseY_m3624976109 (GetMouseY_t2179746180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
