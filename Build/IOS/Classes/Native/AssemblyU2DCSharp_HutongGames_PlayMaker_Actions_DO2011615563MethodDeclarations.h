﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformShakeAnchorPos
struct DOTweenRectTransformShakeAnchorPos_t2011615563;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformShakeAnchorPos::.ctor()
extern "C"  void DOTweenRectTransformShakeAnchorPos__ctor_m2806601493 (DOTweenRectTransformShakeAnchorPos_t2011615563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformShakeAnchorPos::Reset()
extern "C"  void DOTweenRectTransformShakeAnchorPos_Reset_m3878653116 (DOTweenRectTransformShakeAnchorPos_t2011615563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformShakeAnchorPos::OnEnter()
extern "C"  void DOTweenRectTransformShakeAnchorPos_OnEnter_m2223406134 (DOTweenRectTransformShakeAnchorPos_t2011615563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformShakeAnchorPos::<OnEnter>m__74()
extern "C"  void DOTweenRectTransformShakeAnchorPos_U3COnEnterU3Em__74_m375053926 (DOTweenRectTransformShakeAnchorPos_t2011615563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformShakeAnchorPos::<OnEnter>m__75()
extern "C"  void DOTweenRectTransformShakeAnchorPos_U3COnEnterU3Em__75_m233891425 (DOTweenRectTransformShakeAnchorPos_t2011615563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
