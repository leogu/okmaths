﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LocationInfo
struct LocationInfo_t1364725149;
struct LocationInfo_t1364725149_marshaled_pinvoke;
struct LocationInfo_t1364725149_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LocationInfo1364725149.h"

// System.Single UnityEngine.LocationInfo::get_latitude()
extern "C"  float LocationInfo_get_latitude_m2482205269 (LocationInfo_t1364725149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LocationInfo::get_longitude()
extern "C"  float LocationInfo_get_longitude_m306881672 (LocationInfo_t1364725149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LocationInfo::get_altitude()
extern "C"  float LocationInfo_get_altitude_m523209073 (LocationInfo_t1364725149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LocationInfo::get_horizontalAccuracy()
extern "C"  float LocationInfo_get_horizontalAccuracy_m753214408 (LocationInfo_t1364725149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.LocationInfo::get_verticalAccuracy()
extern "C"  float LocationInfo_get_verticalAccuracy_m2584578518 (LocationInfo_t1364725149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct LocationInfo_t1364725149;
struct LocationInfo_t1364725149_marshaled_pinvoke;

extern "C" void LocationInfo_t1364725149_marshal_pinvoke(const LocationInfo_t1364725149& unmarshaled, LocationInfo_t1364725149_marshaled_pinvoke& marshaled);
extern "C" void LocationInfo_t1364725149_marshal_pinvoke_back(const LocationInfo_t1364725149_marshaled_pinvoke& marshaled, LocationInfo_t1364725149& unmarshaled);
extern "C" void LocationInfo_t1364725149_marshal_pinvoke_cleanup(LocationInfo_t1364725149_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct LocationInfo_t1364725149;
struct LocationInfo_t1364725149_marshaled_com;

extern "C" void LocationInfo_t1364725149_marshal_com(const LocationInfo_t1364725149& unmarshaled, LocationInfo_t1364725149_marshaled_com& marshaled);
extern "C" void LocationInfo_t1364725149_marshal_com_back(const LocationInfo_t1364725149_marshaled_com& marshaled, LocationInfo_t1364725149& unmarshaled);
extern "C" void LocationInfo_t1364725149_marshal_com_cleanup(LocationInfo_t1364725149_marshaled_com& marshaled);
