﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PerSecond
struct PerSecond_t231785839;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PerSecond::.ctor()
extern "C"  void PerSecond__ctor_m1027176633 (PerSecond_t231785839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::Reset()
extern "C"  void PerSecond_Reset_m1688646372 (PerSecond_t231785839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::OnEnter()
extern "C"  void PerSecond_OnEnter_m3439598838 (PerSecond_t231785839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::OnUpdate()
extern "C"  void PerSecond_OnUpdate_m4267532973 (PerSecond_t231785839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PerSecond::DoPerSecond()
extern "C"  void PerSecond_DoPerSecond_m2063976753 (PerSecond_t231785839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
