﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate
struct  uGuiInputFieldGetCaretBlinkRate_t1105302691  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::caretBlinkRate
	FsmFloat_t937133978 * ___caretBlinkRate_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::_inputField
	InputField_t1631627530 * ____inputField_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCaretBlinkRate_t1105302691, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_caretBlinkRate_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCaretBlinkRate_t1105302691, ___caretBlinkRate_12)); }
	inline FsmFloat_t937133978 * get_caretBlinkRate_12() const { return ___caretBlinkRate_12; }
	inline FsmFloat_t937133978 ** get_address_of_caretBlinkRate_12() { return &___caretBlinkRate_12; }
	inline void set_caretBlinkRate_12(FsmFloat_t937133978 * value)
	{
		___caretBlinkRate_12 = value;
		Il2CppCodeGenWriteBarrier(&___caretBlinkRate_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCaretBlinkRate_t1105302691, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__inputField_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCaretBlinkRate_t1105302691, ____inputField_14)); }
	inline InputField_t1631627530 * get__inputField_14() const { return ____inputField_14; }
	inline InputField_t1631627530 ** get_address_of__inputField_14() { return &____inputField_14; }
	inline void set__inputField_14(InputField_t1631627530 * value)
	{
		____inputField_14 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
