﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::.ctor()
#define List_1__ctor_m246584498(__this, method) ((  void (*) (List_1_t293398091 *, const MethodInfo*))List_1__ctor_m365405030_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2185267882(__this, ___collection0, method) ((  void (*) (List_1_t293398091 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1612406893_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::.ctor(System.Int32)
#define List_1__ctor_m1790644364(__this, ___capacity0, method) ((  void (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::.cctor()
#define List_1__cctor_m3810213960(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3089647991(__this, method) ((  Il2CppObject* (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1782903547(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t293398091 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1033061458(__this, method) ((  Il2CppObject * (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m4100700707(__this, ___item0, method) ((  int32_t (*) (List_1_t293398091 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2444461315(__this, ___item0, method) ((  bool (*) (List_1_t293398091 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3390924465(__this, ___item0, method) ((  int32_t (*) (List_1_t293398091 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3750931200(__this, ___index0, ___item1, method) ((  void (*) (List_1_t293398091 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2534263502(__this, ___item0, method) ((  void (*) (List_1_t293398091 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3423775270(__this, method) ((  bool (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1713364619(__this, method) ((  bool (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1298453567(__this, method) ((  Il2CppObject * (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1884753100(__this, method) ((  bool (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m232114863(__this, method) ((  bool (*) (List_1_t293398091 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m3487051586(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2902192397(__this, ___index0, ___value1, method) ((  void (*) (List_1_t293398091 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Add(T)
#define List_1_Add_m3710844302(__this, ___item0, method) ((  void (*) (List_1_t293398091 *, FsmTemplateControl_t924276959 *, const MethodInfo*))List_1_Add_m567051994_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2710588263(__this, ___newCount0, method) ((  void (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m730601428(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t293398091 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m642843871(__this, ___collection0, method) ((  void (*) (List_1_t293398091 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2105720031(__this, ___enumerable0, method) ((  void (*) (List_1_t293398091 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3098536362(__this, ___collection0, method) ((  void (*) (List_1_t293398091 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::AsReadOnly()
#define List_1_AsReadOnly_m199256143(__this, method) ((  ReadOnlyCollection_1_t1110062651 * (*) (List_1_t293398091 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Clear()
#define List_1_Clear_m1821902072(__this, method) ((  void (*) (List_1_t293398091 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Contains(T)
#define List_1_Contains_m888050254(__this, ___item0, method) ((  bool (*) (List_1_t293398091 *, FsmTemplateControl_t924276959 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2777192224(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t293398091 *, FsmTemplateControlU5BU5D_t352068806*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Find(System.Predicate`1<T>)
#define List_1_Find_m3207509826(__this, ___match0, method) ((  FsmTemplateControl_t924276959 * (*) (List_1_t293398091 *, Predicate_1_t3662214370 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3362801771(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3662214370 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3055933628(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t293398091 *, int32_t, int32_t, Predicate_1_t3662214370 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::GetEnumerator()
#define List_1_GetEnumerator_m800297409(__this, method) ((  Enumerator_t4123095061  (*) (List_1_t293398091 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::IndexOf(T)
#define List_1_IndexOf_m667887518(__this, ___item0, method) ((  int32_t (*) (List_1_t293398091 *, FsmTemplateControl_t924276959 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3050421463(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t293398091 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m156174008(__this, ___index0, method) ((  void (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Insert(System.Int32,T)
#define List_1_Insert_m114647961(__this, ___index0, ___item1, method) ((  void (*) (List_1_t293398091 *, int32_t, FsmTemplateControl_t924276959 *, const MethodInfo*))List_1_Insert_m283311118_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m2672672474(__this, ___collection0, method) ((  void (*) (List_1_t293398091 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Remove(T)
#define List_1_Remove_m3655665345(__this, ___item0, method) ((  bool (*) (List_1_t293398091 *, FsmTemplateControl_t924276959 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2657387579(__this, ___match0, method) ((  int32_t (*) (List_1_t293398091 *, Predicate_1_t3662214370 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1892976517(__this, ___index0, method) ((  void (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m3551427468(__this, ___index0, ___count1, method) ((  void (*) (List_1_t293398091 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Reverse()
#define List_1_Reverse_m1045752079(__this, method) ((  void (*) (List_1_t293398091 *, const MethodInfo*))List_1_Reverse_m3280715839_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Sort()
#define List_1_Sort_m1914744657(__this, method) ((  void (*) (List_1_t293398091 *, const MethodInfo*))List_1_Sort_m1888745365_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m4037671440(__this, ___comparison0, method) ((  void (*) (List_1_t293398091 *, Comparison_1_t2186015810 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::ToArray()
#define List_1_ToArray_m1314321366(__this, method) ((  FsmTemplateControlU5BU5D_t352068806* (*) (List_1_t293398091 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::TrimExcess()
#define List_1_TrimExcess_m4210649418(__this, method) ((  void (*) (List_1_t293398091 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::get_Capacity()
#define List_1_get_Capacity_m1990342136(__this, method) ((  int32_t (*) (List_1_t293398091 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3786809887(__this, ___value0, method) ((  void (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::get_Count()
#define List_1_get_Count_m3711848548(__this, method) ((  int32_t (*) (List_1_t293398091 *, const MethodInfo*))List_1_get_Count_m1012630527_gshared)(__this, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::get_Item(System.Int32)
#define List_1_get_Item_m1802494499(__this, ___index0, method) ((  FsmTemplateControl_t924276959 * (*) (List_1_t293398091 *, int32_t, const MethodInfo*))List_1_get_Item_m1650084162_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>::set_Item(System.Int32,T)
#define List_1_set_Item_m2750354378(__this, ___index0, ___value1, method) ((  void (*) (List_1_t293398091 *, int32_t, FsmTemplateControl_t924276959 *, const MethodInfo*))List_1_set_Item_m1410262499_gshared)(__this, ___index0, ___value1, method)
