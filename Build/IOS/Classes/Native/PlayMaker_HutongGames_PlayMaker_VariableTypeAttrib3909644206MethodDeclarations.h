﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.VariableTypeAttribute
struct VariableTypeAttribute_t3909644206;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.VariableTypeAttribute::get_Type()
extern "C"  int32_t VariableTypeAttribute_get_Type_m551433485 (VariableTypeAttribute_t3909644206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.VariableTypeAttribute::.ctor(HutongGames.PlayMaker.VariableType)
extern "C"  void VariableTypeAttribute__ctor_m2357064027 (VariableTypeAttribute_t3909644206 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
