﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRoot
struct GetRoot_t4279641242;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRoot::.ctor()
extern "C"  void GetRoot__ctor_m2599496698 (GetRoot_t4279641242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRoot::Reset()
extern "C"  void GetRoot_Reset_m581720823 (GetRoot_t4279641242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRoot::OnEnter()
extern "C"  void GetRoot_OnEnter_m3553255319 (GetRoot_t4279641242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRoot::DoGetRoot()
extern "C"  void GetRoot_DoGetRoot_m2218555753 (GetRoot_t4279641242 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
