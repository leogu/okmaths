﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGUI
struct PlayMakerGUI_t2662579489;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.Texture
struct Texture_t2243626319;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"

// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern "C"  bool PlayMakerGUI_get_EnableStateLabels_m3131360880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_EnableStateLabels(System.Boolean)
extern "C"  void PlayMakerGUI_set_EnableStateLabels_m1834678665 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerGUI PlayMakerGUI::get_Instance()
extern "C"  PlayMakerGUI_t2662579489 * PlayMakerGUI_get_Instance_m3854846106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_Enabled()
extern "C"  bool PlayMakerGUI_get_Enabled_m402578142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern "C"  GUISkin_t1436893342 * PlayMakerGUI_get_GUISkin_m2441732861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUISkin(UnityEngine.GUISkin)
extern "C"  void PlayMakerGUI_set_GUISkin_m4222880562 (Il2CppObject * __this /* static, unused */, GUISkin_t1436893342 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIColor_m3278185336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIColor_m3151620121 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIBackgroundColor_m2538762796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIBackgroundColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIBackgroundColor_m3152511687 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIContentColor_m1794729359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIContentColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIContentColor_m3644451446 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 PlayMakerGUI::get_GUIMatrix()
extern "C"  Matrix4x4_t2933234003  PlayMakerGUI_get_GUIMatrix_m1271908730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern "C"  void PlayMakerGUI_set_GUIMatrix_m1989428149 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern "C"  Texture_t2243626319 * PlayMakerGUI_get_MouseCursor_m803897773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_MouseCursor(UnityEngine.Texture)
extern "C"  void PlayMakerGUI_set_MouseCursor_m3353084044 (Il2CppObject * __this /* static, unused */, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_LockCursor()
extern "C"  bool PlayMakerGUI_get_LockCursor_m2699961842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_LockCursor(System.Boolean)
extern "C"  void PlayMakerGUI_set_LockCursor_m800390487 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_HideCursor()
extern "C"  bool PlayMakerGUI_get_HideCursor_m1761535307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_HideCursor(System.Boolean)
extern "C"  void PlayMakerGUI_set_HideCursor_m2707817742 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::InitLabelStyle()
extern "C"  void PlayMakerGUI_InitLabelStyle_m2898018055 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DrawStateLabels()
extern "C"  void PlayMakerGUI_DrawStateLabels_m1092711642 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern "C"  void PlayMakerGUI_DrawStateLabel_m1328883389 (PlayMakerGUI_t2662579489 * __this, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern "C"  String_t* PlayMakerGUI_GenerateStateLabel_m2774892031 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::Awake()
extern "C"  void PlayMakerGUI_Awake_m3750694929 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnEnable()
extern "C"  void PlayMakerGUI_OnEnable_m857055774 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnGUI()
extern "C"  void PlayMakerGUI_OnGUI_m507334058 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C"  void PlayMakerGUI_CallOnGUI_m3101561124 (PlayMakerGUI_t2662579489 * __this, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnDisable()
extern "C"  void PlayMakerGUI_OnDisable_m2748493449 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DoEditGUI()
extern "C"  void PlayMakerGUI_DoEditGUI_m2014605058 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::OnApplicationQuit()
extern "C"  void PlayMakerGUI_OnApplicationQuit_m1056069416 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::.ctor()
extern "C"  void PlayMakerGUI__ctor_m83179638 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerGUI::<DrawStateLabels>b__1(PlayMakerFSM,PlayMakerFSM)
extern "C"  int32_t PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m2467571249 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___x0, PlayMakerFSM_t437737208 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::.cctor()
extern "C"  void PlayMakerGUI__cctor_m2174270851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
