﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiGetColorBlock
struct  uGuiGetColorBlock_t3491763842  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiGetColorBlock::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiGetColorBlock::fadeDuration
	FsmFloat_t937133978 * ___fadeDuration_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiGetColorBlock::colorMultiplier
	FsmFloat_t937133978 * ___colorMultiplier_13;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiGetColorBlock::normalColor
	FsmColor_t118301965 * ___normalColor_14;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiGetColorBlock::pressedColor
	FsmColor_t118301965 * ___pressedColor_15;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiGetColorBlock::highlightedColor
	FsmColor_t118301965 * ___highlightedColor_16;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiGetColorBlock::disabledColor
	FsmColor_t118301965 * ___disabledColor_17;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiGetColorBlock::everyFrame
	bool ___everyFrame_18;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiGetColorBlock::_selectable
	Selectable_t1490392188 * ____selectable_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_fadeDuration_12() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___fadeDuration_12)); }
	inline FsmFloat_t937133978 * get_fadeDuration_12() const { return ___fadeDuration_12; }
	inline FsmFloat_t937133978 ** get_address_of_fadeDuration_12() { return &___fadeDuration_12; }
	inline void set_fadeDuration_12(FsmFloat_t937133978 * value)
	{
		___fadeDuration_12 = value;
		Il2CppCodeGenWriteBarrier(&___fadeDuration_12, value);
	}

	inline static int32_t get_offset_of_colorMultiplier_13() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___colorMultiplier_13)); }
	inline FsmFloat_t937133978 * get_colorMultiplier_13() const { return ___colorMultiplier_13; }
	inline FsmFloat_t937133978 ** get_address_of_colorMultiplier_13() { return &___colorMultiplier_13; }
	inline void set_colorMultiplier_13(FsmFloat_t937133978 * value)
	{
		___colorMultiplier_13 = value;
		Il2CppCodeGenWriteBarrier(&___colorMultiplier_13, value);
	}

	inline static int32_t get_offset_of_normalColor_14() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___normalColor_14)); }
	inline FsmColor_t118301965 * get_normalColor_14() const { return ___normalColor_14; }
	inline FsmColor_t118301965 ** get_address_of_normalColor_14() { return &___normalColor_14; }
	inline void set_normalColor_14(FsmColor_t118301965 * value)
	{
		___normalColor_14 = value;
		Il2CppCodeGenWriteBarrier(&___normalColor_14, value);
	}

	inline static int32_t get_offset_of_pressedColor_15() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___pressedColor_15)); }
	inline FsmColor_t118301965 * get_pressedColor_15() const { return ___pressedColor_15; }
	inline FsmColor_t118301965 ** get_address_of_pressedColor_15() { return &___pressedColor_15; }
	inline void set_pressedColor_15(FsmColor_t118301965 * value)
	{
		___pressedColor_15 = value;
		Il2CppCodeGenWriteBarrier(&___pressedColor_15, value);
	}

	inline static int32_t get_offset_of_highlightedColor_16() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___highlightedColor_16)); }
	inline FsmColor_t118301965 * get_highlightedColor_16() const { return ___highlightedColor_16; }
	inline FsmColor_t118301965 ** get_address_of_highlightedColor_16() { return &___highlightedColor_16; }
	inline void set_highlightedColor_16(FsmColor_t118301965 * value)
	{
		___highlightedColor_16 = value;
		Il2CppCodeGenWriteBarrier(&___highlightedColor_16, value);
	}

	inline static int32_t get_offset_of_disabledColor_17() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___disabledColor_17)); }
	inline FsmColor_t118301965 * get_disabledColor_17() const { return ___disabledColor_17; }
	inline FsmColor_t118301965 ** get_address_of_disabledColor_17() { return &___disabledColor_17; }
	inline void set_disabledColor_17(FsmColor_t118301965 * value)
	{
		___disabledColor_17 = value;
		Il2CppCodeGenWriteBarrier(&___disabledColor_17, value);
	}

	inline static int32_t get_offset_of_everyFrame_18() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ___everyFrame_18)); }
	inline bool get_everyFrame_18() const { return ___everyFrame_18; }
	inline bool* get_address_of_everyFrame_18() { return &___everyFrame_18; }
	inline void set_everyFrame_18(bool value)
	{
		___everyFrame_18 = value;
	}

	inline static int32_t get_offset_of__selectable_19() { return static_cast<int32_t>(offsetof(uGuiGetColorBlock_t3491763842, ____selectable_19)); }
	inline Selectable_t1490392188 * get__selectable_19() const { return ____selectable_19; }
	inline Selectable_t1490392188 ** get_address_of__selectable_19() { return &____selectable_19; }
	inline void set__selectable_19(Selectable_t1490392188 * value)
	{
		____selectable_19 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
