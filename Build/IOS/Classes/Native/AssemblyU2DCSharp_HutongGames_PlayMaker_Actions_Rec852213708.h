﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Rec627647977.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition
struct  RectTransformGetLocalPosition_t852213708  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition/LocalPositionReference HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::reference
	int32_t ___reference_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::position
	FsmVector3_t3996534004 * ___position_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::position2d
	FsmVector2_t2430450063 * ___position2d_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::x
	FsmFloat_t937133978 * ___x_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::y
	FsmFloat_t937133978 * ___y_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::z
	FsmFloat_t937133978 * ___z_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetLocalPosition::_rt
	RectTransform_t3349966182 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_reference_14() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___reference_14)); }
	inline int32_t get_reference_14() const { return ___reference_14; }
	inline int32_t* get_address_of_reference_14() { return &___reference_14; }
	inline void set_reference_14(int32_t value)
	{
		___reference_14 = value;
	}

	inline static int32_t get_offset_of_position_15() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___position_15)); }
	inline FsmVector3_t3996534004 * get_position_15() const { return ___position_15; }
	inline FsmVector3_t3996534004 ** get_address_of_position_15() { return &___position_15; }
	inline void set_position_15(FsmVector3_t3996534004 * value)
	{
		___position_15 = value;
		Il2CppCodeGenWriteBarrier(&___position_15, value);
	}

	inline static int32_t get_offset_of_position2d_16() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___position2d_16)); }
	inline FsmVector2_t2430450063 * get_position2d_16() const { return ___position2d_16; }
	inline FsmVector2_t2430450063 ** get_address_of_position2d_16() { return &___position2d_16; }
	inline void set_position2d_16(FsmVector2_t2430450063 * value)
	{
		___position2d_16 = value;
		Il2CppCodeGenWriteBarrier(&___position2d_16, value);
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___x_17)); }
	inline FsmFloat_t937133978 * get_x_17() const { return ___x_17; }
	inline FsmFloat_t937133978 ** get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(FsmFloat_t937133978 * value)
	{
		___x_17 = value;
		Il2CppCodeGenWriteBarrier(&___x_17, value);
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___y_18)); }
	inline FsmFloat_t937133978 * get_y_18() const { return ___y_18; }
	inline FsmFloat_t937133978 ** get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(FsmFloat_t937133978 * value)
	{
		___y_18 = value;
		Il2CppCodeGenWriteBarrier(&___y_18, value);
	}

	inline static int32_t get_offset_of_z_19() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ___z_19)); }
	inline FsmFloat_t937133978 * get_z_19() const { return ___z_19; }
	inline FsmFloat_t937133978 ** get_address_of_z_19() { return &___z_19; }
	inline void set_z_19(FsmFloat_t937133978 * value)
	{
		___z_19 = value;
		Il2CppCodeGenWriteBarrier(&___z_19, value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformGetLocalPosition_t852213708, ____rt_20)); }
	inline RectTransform_t3349966182 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3349966182 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3349966182 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier(&____rt_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
