﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1081683921.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1571958496.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiNavigationSetMode
struct  uGuiNavigationSetMode_t1293698679  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// UnityEngine.UI.Navigation/Mode HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::navigationMode
	int32_t ___navigationMode_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::_selectable
	Selectable_t1490392188 * ____selectable_14;
	// UnityEngine.UI.Navigation HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::_navigation
	Navigation_t1571958496  ____navigation_15;
	// UnityEngine.UI.Navigation/Mode HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::_originalValue
	int32_t ____originalValue_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiNavigationSetMode_t1293698679, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_navigationMode_12() { return static_cast<int32_t>(offsetof(uGuiNavigationSetMode_t1293698679, ___navigationMode_12)); }
	inline int32_t get_navigationMode_12() const { return ___navigationMode_12; }
	inline int32_t* get_address_of_navigationMode_12() { return &___navigationMode_12; }
	inline void set_navigationMode_12(int32_t value)
	{
		___navigationMode_12 = value;
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiNavigationSetMode_t1293698679, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__selectable_14() { return static_cast<int32_t>(offsetof(uGuiNavigationSetMode_t1293698679, ____selectable_14)); }
	inline Selectable_t1490392188 * get__selectable_14() const { return ____selectable_14; }
	inline Selectable_t1490392188 ** get_address_of__selectable_14() { return &____selectable_14; }
	inline void set__selectable_14(Selectable_t1490392188 * value)
	{
		____selectable_14 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_14, value);
	}

	inline static int32_t get_offset_of__navigation_15() { return static_cast<int32_t>(offsetof(uGuiNavigationSetMode_t1293698679, ____navigation_15)); }
	inline Navigation_t1571958496  get__navigation_15() const { return ____navigation_15; }
	inline Navigation_t1571958496 * get_address_of__navigation_15() { return &____navigation_15; }
	inline void set__navigation_15(Navigation_t1571958496  value)
	{
		____navigation_15 = value;
	}

	inline static int32_t get_offset_of__originalValue_16() { return static_cast<int32_t>(offsetof(uGuiNavigationSetMode_t1293698679, ____originalValue_16)); }
	inline int32_t get__originalValue_16() const { return ____originalValue_16; }
	inline int32_t* get_address_of__originalValue_16() { return &____originalValue_16; }
	inline void set__originalValue_16(int32_t value)
	{
		____originalValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
