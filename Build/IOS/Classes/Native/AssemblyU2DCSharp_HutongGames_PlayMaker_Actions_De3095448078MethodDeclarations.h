﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DetachChildren
struct DetachChildren_t3095448078;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.DetachChildren::.ctor()
extern "C"  void DetachChildren__ctor_m327157404 (DetachChildren_t3095448078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::Reset()
extern "C"  void DetachChildren_Reset_m3173819355 (DetachChildren_t3095448078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::OnEnter()
extern "C"  void DetachChildren_OnEnter_m1809885963 (DetachChildren_t3095448078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DetachChildren::DoDetachChildren(UnityEngine.GameObject)
extern "C"  void DetachChildren_DoDetachChildren_m2797982093 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
