﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLineRendererColor
struct DOTweenLineRendererColor_t1286902182;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLineRendererColor::.ctor()
extern "C"  void DOTweenLineRendererColor__ctor_m726815984 (DOTweenLineRendererColor_t1286902182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLineRendererColor::Reset()
extern "C"  void DOTweenLineRendererColor_Reset_m1518710391 (DOTweenLineRendererColor_t1286902182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLineRendererColor::OnEnter()
extern "C"  void DOTweenLineRendererColor_OnEnter_m3524475703 (DOTweenLineRendererColor_t1286902182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLineRendererColor::<OnEnter>m__4E()
extern "C"  void DOTweenLineRendererColor_U3COnEnterU3Em__4E_m383918881 (DOTweenLineRendererColor_t1286902182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLineRendererColor::<OnEnter>m__4F()
extern "C"  void DOTweenLineRendererColor_U3COnEnterU3Em__4F_m4255398674 (DOTweenLineRendererColor_t1286902182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
