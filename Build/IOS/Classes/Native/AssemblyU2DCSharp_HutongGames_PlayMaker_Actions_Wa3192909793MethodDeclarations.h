﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeUp2d
struct WakeUp2d_t3192909793;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeUp2d::.ctor()
extern "C"  void WakeUp2d__ctor_m539076049 (WakeUp2d_t3192909793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp2d::Reset()
extern "C"  void WakeUp2d_Reset_m4034306026 (WakeUp2d_t3192909793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp2d::OnEnter()
extern "C"  void WakeUp2d_OnEnter_m1270268064 (WakeUp2d_t3192909793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp2d::DoWakeUp()
extern "C"  void WakeUp2d_DoWakeUp_m1192560511 (WakeUp2d_t3192909793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
