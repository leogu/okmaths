﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight
struct SetAnimatorLayerWeight_t1821312336;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::.ctor()
extern "C"  void SetAnimatorLayerWeight__ctor_m3031111662 (SetAnimatorLayerWeight_t1821312336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::Reset()
extern "C"  void SetAnimatorLayerWeight_Reset_m3860117621 (SetAnimatorLayerWeight_t1821312336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::OnEnter()
extern "C"  void SetAnimatorLayerWeight_OnEnter_m1903068317 (SetAnimatorLayerWeight_t1821312336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::OnUpdate()
extern "C"  void SetAnimatorLayerWeight_OnUpdate_m2631887536 (SetAnimatorLayerWeight_t1821312336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayerWeight::DoLayerWeight()
extern "C"  void SetAnimatorLayerWeight_DoLayerWeight_m2594134044 (SetAnimatorLayerWeight_t1821312336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
