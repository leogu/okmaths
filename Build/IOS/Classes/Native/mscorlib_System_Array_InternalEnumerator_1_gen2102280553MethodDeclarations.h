﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2102280553.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m446705113_gshared (InternalEnumerator_1_t2102280553 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m446705113(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2102280553 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m446705113_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m967006629_gshared (InternalEnumerator_1_t2102280553 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m967006629(__this, method) ((  void (*) (InternalEnumerator_1_t2102280553 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m967006629_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2277530457_gshared (InternalEnumerator_1_t2102280553 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2277530457(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2102280553 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2277530457_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3632231598_gshared (InternalEnumerator_1_t2102280553 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3632231598(__this, method) ((  void (*) (InternalEnumerator_1_t2102280553 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3632231598_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2063766749_gshared (InternalEnumerator_1_t2102280553 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2063766749(__this, method) ((  bool (*) (InternalEnumerator_1_t2102280553 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2063766749_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.NetworkPlayer>::get_Current()
extern "C"  NetworkPlayer_t1243528291  InternalEnumerator_1_get_Current_m1265704222_gshared (InternalEnumerator_1_t2102280553 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1265704222(__this, method) ((  NetworkPlayer_t1243528291  (*) (InternalEnumerator_1_t2102280553 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1265704222_gshared)(__this, method)
