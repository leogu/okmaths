﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.Event
struct Event_t3028476042;
// UnityEngine.SliderState
struct SliderState_t1595681032;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
struct GameCenterPlatform_t2156144444;
// UnityEngine.SocialPlatforms.ILocalUser
struct ILocalUser_t2210666073;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t2283071720;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t4052399267;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
struct Action_1_t3885079697;
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t2511354027;
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t3039104018;
// UnityEngine.SocialPlatforms.ILeaderboard
struct ILeaderboard_t77027648;
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t3263047812;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t2930725895;
// UnityEngine.SocialPlatforms.IAchievement
struct IAchievement_t1752291260;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1333316625;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t3110978151;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
struct GcLeaderboard_t453887929;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
struct Leaderboard_t4160680639;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t2307748940;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t3365630962;
// UnityEngine.SocialPlatforms.IScore
struct IScore_t513966369;
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t3237304636;
// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t3019851150;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t3461248430;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t952253354;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// System.Object
struct Il2CppObject;
// System.Diagnostics.StackTrace
struct StackTrace_t2500644597;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t2151245329;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t2454598508;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// UnityEngine.TextEditor
struct TextEditor_t3975561390;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t2425757932;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t2990399006;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t1745199419;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t3597577401;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t4162218475;
// UnityEngine.Font
struct Font_t4239498691;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.ThreadAndSerializationSafe
struct ThreadAndSerializationSafe_t2122816804;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t4278647215;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t601950206;
// UnityEngine.TrackedReference
struct TrackedReference_t1045890189;
// UnityEngine.TrailRenderer
struct TrailRenderer_t2490637367;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Transform/Enumerator
struct Enumerator_t1251553160;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t3067050131;
// System.Exception
struct Exception_t1927440687;
// UnityEngine.UnityAPICompatibilityVersionAttribute
struct UnityAPICompatibilityVersionAttribute_t2508627033;
// UnityEngine.UnityException
struct UnityException_t2687879050;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1785723201;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t3968615785;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t2105307154;
// UnityEngine.WheelJoint2D
struct WheelJoint2D_t570080601;
// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t2585285711;
// UnityEngine.WritableAttribute
struct WritableAttribute_t3715198420;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Text.Encoding
struct Encoding_t663144255;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981;
// UnityEngineInternal.GenericStack
struct GenericStack_t3718539591;
// System.Delegate
struct Delegate_t3022476291;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1390152093;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_SliderHandler3550500579.h"
#include "UnityEngine_UnityEngine_SliderHandler3550500579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_Event3028476042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIUtility3275770671MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SystemClock104337557MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SliderState1595681032.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_SliderState1595681032MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2156144444.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2156144444MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4117976357MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3110978151.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3365630962.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4117976357.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope2583939667.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_960725851.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_960725851MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie3110978151MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3885079697MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3885079697.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3198293052.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3198293052MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local3019851150MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Local3019851150.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149.h"
#include "mscorlib_System_Action_1_gen2511354027MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie1333316625.h"
#include "mscorlib_System_Action_1_gen2511354027.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1754866149MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238.h"
#include "mscorlib_System_Action_1_gen3039104018MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score2307748940.h"
#include "mscorlib_System_Action_1_gen3039104018.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter3676783238MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserP3365630962MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_453887929MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade4160680639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leade4160680639.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_453887929.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope3775842435.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3652706031.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3652706031MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3263047812MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3263047812.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achie1333316625MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score2307748940MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState455716270.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range3455291607MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope2583939667MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope3775842435MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState455716270MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SortingLayer221838959.h"
#include "UnityEngine_UnityEngine_SortingLayer221838959MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4278750806.h"
#include "UnityEngine_UnityEngine_Space4278750806MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpaceAttribute952253354.h"
#include "UnityEngine_UnityEngine_SpaceAttribute952253354MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_Sprite309593783MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility4181890362.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility4181890362MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StackTraceUtility1881293839.h"
#include "UnityEngine_UnityEngine_StackTraceUtility1881293839MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace2500644597MethodDeclarations.h"
#include "mscorlib_System_Diagnostics_StackTrace2500644597.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Diagnostics_StackFrame2050294881.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075.h"
#include "mscorlib_System_Diagnostics_StackFrame2050294881MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour2151245329MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_SystemClock104337557.h"
#include "mscorlib_System_DateTimeKind2186819611.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895.h"
#include "UnityEngine_UnityEngine_SystemInfo2353426895MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute2454598508.h"
#include "UnityEngine_UnityEngine_TextAreaAttribute2454598508MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845.h"
#include "UnityEngine_UnityEngine_TextAsset3973159845MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextClipping2573530411.h"
#include "UnityEngine_UnityEngine_TextClipping2573530411MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor3975561390.h"
#include "UnityEngine_UnityEngine_TextEditor3975561390MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1747193563MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1747193563.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1119726228.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType593718391.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_TextEditor_CharacterType593718391MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin1119726228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3138797698MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings2543476768.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings2543476768MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontStyle2764949590.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2027154177.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode3668245347.h"
#include "UnityEngine_UnityEngine_Font4239498691.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerator647235000.h"
#include "UnityEngine_UnityEngine_TextGenerator647235000MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425757932MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2990399006MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2425757932.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2990399006.h"
#include "UnityEngine_UnityEngine_Font4239498691MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Texture2243626319MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_TextureFormat1386130234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe2122816804.h"
#include "UnityEngine_UnityEngine_ThreadAndSerializationSafe2122816804MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TooltipAttribute4278647215.h"
#include "UnityEngine_UnityEngine_TooltipAttribute4278647215MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch407273883.h"
#include "UnityEngine_UnityEngine_Touch407273883MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "UnityEngine_UnityEngine_TouchType2732027771.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard601950206.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard601950206MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType875112366.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Intern1040270188.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_Intern1040270188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType875112366MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchType2732027771MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189.h"
#include "UnityEngine_UnityEngine_TrackedReference1045890189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TrailRenderer2490637367.h"
#include "UnityEngine_UnityEngine_TrailRenderer2490637367MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1903422412.h"
#include "UnityEngine_UnityEngine_UnhandledExceptionHandler1903422412MethodDeclarations.h"
#include "mscorlib_System_AppDomain2719102437MethodDeclarations.h"
#include "mscorlib_System_UnhandledExceptionEventHandler1916531888MethodDeclarations.h"
#include "mscorlib_System_AppDomain2719102437.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3067050131.h"
#include "mscorlib_System_UnhandledExceptionEventHandler1916531888.h"
#include "mscorlib_System_UnhandledExceptionEventArgs3067050131MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2508627033.h"
#include "UnityEngine_UnityEngine_UnityAPICompatibilityVersi2508627033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityException2687879050.h"
#include "UnityEngine_UnityEngine_UnityException2687879050MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "UnityEngine_UnityEngine_UnityString276356480.h"
#include "UnityEngine_UnityEngine_UnityString276356480MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode3668245347MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate3968615785MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154.h"
#include "UnityEngine_UnityEngine_WaitForSecondsRealtime2105307154MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1786092740MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WheelJoint2D570080601.h"
#include "UnityEngine_UnityEngine_WheelJoint2D570080601MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JointSuspension2D1941285899.h"
#include "UnityEngine_UnityEngine_JointMotor2D2112906529.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_UnityEngine_WrapMode255797857MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2585285711.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall2585285711MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WritableAttribute3715198420.h"
#include "UnityEngine_UnityEngine_WritableAttribute3715198420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "UnityEngine_UnityEngine_WWWTranscoder1124214756MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream743994179MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2766455145MethodDeclarations.h"
#include "mscorlib_System_IO_MemoryStream743994179.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2766455145.h"
#include "UnityEngine_UnityEngine_WWWTranscoder1124214756.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591.h"
#include "UnityEngine_UnityEngineInternal_GenericStack3718539591MethodDeclarations.h"
#include "mscorlib_System_Collections_Stack1043988394MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleA1390152093MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules1810425448MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.SliderHandler::.ctor(UnityEngine.Rect,System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUIStyle,System.Boolean,System.Int32)
extern "C"  void SliderHandler__ctor_m1547880569 (SliderHandler_t3550500579 * __this, Rect_t3681755626  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t1799908754 * ___slider5, GUIStyle_t1799908754 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0 = ___position0;
		__this->set_position_0(L_0);
		float L_1 = ___currentValue1;
		__this->set_currentValue_1(L_1);
		float L_2 = ___size2;
		__this->set_size_2(L_2);
		float L_3 = ___start3;
		__this->set_start_3(L_3);
		float L_4 = ___end4;
		__this->set_end_4(L_4);
		GUIStyle_t1799908754 * L_5 = ___slider5;
		__this->set_slider_5(L_5);
		GUIStyle_t1799908754 * L_6 = ___thumb6;
		__this->set_thumb_6(L_6);
		bool L_7 = ___horiz7;
		__this->set_horiz_7(L_7);
		int32_t L_8 = ___id8;
		__this->set_id_8(L_8);
		return;
	}
}
extern "C"  void SliderHandler__ctor_m1547880569_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___position0, float ___currentValue1, float ___size2, float ___start3, float ___end4, GUIStyle_t1799908754 * ___slider5, GUIStyle_t1799908754 * ___thumb6, bool ___horiz7, int32_t ___id8, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	SliderHandler__ctor_m1547880569(_thisAdjusted, ___position0, ___currentValue1, ___size2, ___start3, ___end4, ___slider5, ___thumb6, ___horiz7, ___id8, method);
}
// System.Single UnityEngine.SliderHandler::Handle()
extern "C"  float SliderHandler_Handle_m504867634 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GUIStyle_t1799908754 * L_0 = __this->get_slider_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t1799908754 * L_1 = __this->get_thumb_6();
		if (L_1)
		{
			goto IL_001d;
		}
	}

IL_0016:
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_001d:
	{
		int32_t L_3 = SliderHandler_CurrentEventType_m2472981589(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4 == 0)
		{
			goto IL_004f;
		}
		if (L_4 == 1)
		{
			goto IL_005d;
		}
		if (L_4 == 2)
		{
			goto IL_006b;
		}
		if (L_4 == 3)
		{
			goto IL_0056;
		}
		if (L_4 == 4)
		{
			goto IL_006b;
		}
		if (L_4 == 5)
		{
			goto IL_006b;
		}
		if (L_4 == 6)
		{
			goto IL_006b;
		}
		if (L_4 == 7)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_006b;
	}

IL_004f:
	{
		float L_5 = SliderHandler_OnMouseDown_m2819993578(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0056:
	{
		float L_6 = SliderHandler_OnMouseDrag_m1069947484(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_005d:
	{
		float L_7 = SliderHandler_OnMouseUp_m3083734299(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0064:
	{
		float L_8 = SliderHandler_OnRepaint_m4171175698(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_006b:
	{
		float L_9 = __this->get_currentValue_1();
		return L_9;
	}
}
extern "C"  float SliderHandler_Handle_m504867634_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_Handle_m504867634(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDown()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t104337557_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDown_m2819993578_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDown_m2819993578 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDown_m2819993578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t693205669  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Rect_t3681755626  L_0 = __this->get_position_0();
		V_1 = L_0;
		Event_t3028476042 * L_1 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Event_get_mousePosition_m3789571399(L_1, /*hidden argument*/NULL);
		bool L_3 = Rect_Contains_m1334685290((&V_1), L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = SliderHandler_IsEmptySlider_m2679659864(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}

IL_0029:
	{
		float L_5 = __this->get_currentValue_1();
		return L_5;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_scrollTroughSide_m1337099359(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m1071873884(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Event_t3028476042 * L_7 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m3575594482(L_7, /*hidden argument*/NULL);
		Rect_t3681755626  L_8 = SliderHandler_ThumbSelectionRect_m1949915148(__this, /*hidden argument*/NULL);
		V_2 = L_8;
		Event_t3028476042 * L_9 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = Event_get_mousePosition_m3789571399(L_9, /*hidden argument*/NULL);
		bool L_11 = Rect_Contains_m1334685290((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_007d;
		}
	}
	{
		float L_12 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		SliderHandler_StartDraggingWithValue_m1407392347(__this, L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_currentValue_1();
		return L_13;
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		bool L_14 = SliderHandler_SupportsPageMovements_m983193435(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00c7;
		}
	}
	{
		SliderState_t1595681032 * L_15 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		L_15->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t104337557_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_16 = SystemClock_get_now_m4108727544(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_16;
		DateTime_t693205669  L_17 = DateTime_AddMilliseconds_m1813199744((&V_3), (250.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m2724006954(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = SliderHandler_CurrentScrollTroughSide_m2283829530(__this, /*hidden argument*/NULL);
		GUI_set_scrollTroughSide_m1337099359(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		float L_19 = SliderHandler_PageMovementValue_m1651578409(__this, /*hidden argument*/NULL);
		return L_19;
	}

IL_00c7:
	{
		float L_20 = SliderHandler_ValueForCurrentMousePosition_m1752598323(__this, /*hidden argument*/NULL);
		V_0 = L_20;
		float L_21 = V_0;
		SliderHandler_StartDraggingWithValue_m1407392347(__this, L_21, /*hidden argument*/NULL);
		float L_22 = V_0;
		float L_23 = SliderHandler_Clamp_m291298090(__this, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
extern "C"  float SliderHandler_OnMouseDown_m2819993578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnMouseDown_m2819993578(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseDrag()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseDrag_m1069947484_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseDrag_m1069947484 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseDrag_m1069947484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SliderState_t1595681032 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m466901769(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		float L_2 = __this->get_currentValue_1();
		return L_2;
	}

IL_0017:
	{
		SliderState_t1595681032 * L_3 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		SliderState_t1595681032 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = L_4->get_isDragging_2();
		if (L_5)
		{
			goto IL_0030;
		}
	}
	{
		float L_6 = __this->get_currentValue_1();
		return L_6;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Event_t3028476042 * L_7 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Event_Use_m3575594482(L_7, /*hidden argument*/NULL);
		float L_8 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		SliderState_t1595681032 * L_9 = V_0;
		NullCheck(L_9);
		float L_10 = L_9->get_dragStartPos_0();
		V_1 = ((float)((float)L_8-(float)L_10));
		SliderState_t1595681032 * L_11 = V_0;
		NullCheck(L_11);
		float L_12 = L_11->get_dragStartValue_1();
		float L_13 = V_1;
		float L_14 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_12+(float)((float)((float)L_13/(float)L_14))));
		float L_15 = V_2;
		float L_16 = SliderHandler_Clamp_m291298090(__this, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  float SliderHandler_OnMouseDrag_m1069947484_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnMouseDrag_m1069947484(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnMouseUp()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnMouseUp_m3083734299_MetadataUsageId;
extern "C"  float SliderHandler_OnMouseUp_m3083734299 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnMouseUp_m3083734299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_get_hotControl_m466901769(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0021;
		}
	}
	{
		Event_t3028476042 * L_2 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Event_Use_m3575594482(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m1071873884(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		float L_3 = __this->get_currentValue_1();
		return L_3;
	}
}
extern "C"  float SliderHandler_OnMouseUp_m3083734299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnMouseUp_m3083734299(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::OnRepaint()
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* SystemClock_t104337557_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_OnRepaint_m4171175698_MetadataUsageId;
extern "C"  float SliderHandler_OnRepaint_m4171175698 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_OnRepaint_m4171175698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DateTime_t693205669  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GUIStyle_t1799908754 * L_0 = __this->get_slider_5();
		Rect_t3681755626  L_1 = __this->get_position_0();
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_2 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_3 = __this->get_id_8();
		NullCheck(L_0);
		GUIStyle_Draw_m2055025106(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		bool L_4 = SliderHandler_IsEmptySlider_m2679659864(__this, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		GUIStyle_t1799908754 * L_5 = __this->get_thumb_6();
		Rect_t3681755626  L_6 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_7 = ((GUIContent_t4210063000_StaticFields*)GUIContent_t4210063000_il2cpp_TypeInfo_var->static_fields)->get_none_6();
		int32_t L_8 = __this->get_id_8();
		NullCheck(L_5);
		GUIStyle_Draw_m2055025106(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_9 = GUIUtility_get_hotControl_m466901769(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_id_8();
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_007c;
		}
	}
	{
		Rect_t3681755626  L_11 = __this->get_position_0();
		V_0 = L_11;
		Event_t3028476042 * L_12 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector2_t2243707579  L_13 = Event_get_mousePosition_m3789571399(L_12, /*hidden argument*/NULL);
		bool L_14 = Rect_Contains_m1334685290((&V_0), L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		bool L_15 = SliderHandler_IsEmptySlider_m2679659864(__this, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0083;
		}
	}

IL_007c:
	{
		float L_16 = __this->get_currentValue_1();
		return L_16;
	}

IL_0083:
	{
		Rect_t3681755626  L_17 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		Event_t3028476042 * L_18 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = Event_get_mousePosition_m3789571399(L_18, /*hidden argument*/NULL);
		bool L_20 = Rect_Contains_m1334685290((&V_1), L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		int32_t L_21 = GUI_get_scrollTroughSide_m237006560(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_hotControl_m1071873884(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		float L_22 = __this->get_currentValue_1();
		return L_22;
	}

IL_00b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_InternalRepaintEditorWindow_m219194149(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t104337557_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_23 = SystemClock_get_now_m4108727544(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t693205669  L_24 = GUI_get_nextScrollStepTime_m4045060331(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		bool L_25 = DateTime_op_LessThan_m3944619870(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		float L_26 = __this->get_currentValue_1();
		return L_26;
	}

IL_00d8:
	{
		int32_t L_27 = SliderHandler_CurrentScrollTroughSide_m2283829530(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		int32_t L_28 = GUI_get_scrollTroughSide_m237006560(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_27) == ((int32_t)L_28)))
		{
			goto IL_00ef;
		}
	}
	{
		float L_29 = __this->get_currentValue_1();
		return L_29;
	}

IL_00ef:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SystemClock_t104337557_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_30 = SystemClock_get_now_m4108727544(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_30;
		DateTime_t693205669  L_31 = DateTime_AddMilliseconds_m1813199744((&V_2), (30.0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_nextScrollStepTime_m2724006954(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		bool L_32 = SliderHandler_SupportsPageMovements_m983193435(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_012e;
		}
	}
	{
		SliderState_t1595681032 * L_33 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set_isDragging_2((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_changed_m470833806(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		float L_34 = SliderHandler_PageMovementValue_m1651578409(__this, /*hidden argument*/NULL);
		return L_34;
	}

IL_012e:
	{
		float L_35 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		return L_35;
	}
}
extern "C"  float SliderHandler_OnRepaint_m4171175698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_OnRepaint_m4171175698(_thisAdjusted, method);
}
// UnityEngine.EventType UnityEngine.SliderHandler::CurrentEventType()
extern "C"  int32_t SliderHandler_CurrentEventType_m2472981589 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	{
		Event_t3028476042 * L_0 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		NullCheck(L_0);
		int32_t L_2 = Event_GetTypeForControl_m3906355766(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t SliderHandler_CurrentEventType_m2472981589_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_CurrentEventType_m2472981589(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SliderHandler::CurrentScrollTroughSide()
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m2283829530 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Event_t3028476042 * L_1 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Event_get_mousePosition_m3789571399(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_x_1();
		G_B3_0 = L_3;
		goto IL_0036;
	}

IL_0023:
	{
		Event_t3028476042 * L_4 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector2_t2243707579  L_5 = Event_get_mousePosition_m3789571399(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = (&V_3)->get_y_2();
		G_B3_0 = L_6;
	}

IL_0036:
	{
		V_0 = G_B3_0;
		bool L_7 = __this->get_horiz_7();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		Rect_t3681755626  L_8 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_x_m1393582490((&V_4), /*hidden argument*/NULL);
		G_B6_0 = L_9;
		goto IL_0065;
	}

IL_0056:
	{
		Rect_t3681755626  L_10 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = Rect_get_y_m1393582395((&V_5), /*hidden argument*/NULL);
		G_B6_0 = L_11;
	}

IL_0065:
	{
		V_1 = G_B6_0;
		float L_12 = V_0;
		float L_13 = V_1;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0073;
		}
	}
	{
		G_B9_0 = 1;
		goto IL_0074;
	}

IL_0073:
	{
		G_B9_0 = (-1);
	}

IL_0074:
	{
		return G_B9_0;
	}
}
extern "C"  int32_t SliderHandler_CurrentScrollTroughSide_m2283829530_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_CurrentScrollTroughSide_m2283829530(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::IsEmptySlider()
extern "C"  bool SliderHandler_IsEmptySlider_m2679659864 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		return (bool)((((float)L_0) == ((float)L_1))? 1 : 0);
	}
}
extern "C"  bool SliderHandler_IsEmptySlider_m2679659864_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_IsEmptySlider_m2679659864(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SliderHandler::SupportsPageMovements()
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SupportsPageMovements_m983193435_MetadataUsageId;
extern "C"  bool SliderHandler_SupportsPageMovements_m983193435 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SupportsPageMovements_m983193435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_size_2();
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_usePageScrollbars_m1086009624(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool SliderHandler_SupportsPageMovements_m983193435_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_SupportsPageMovements_m983193435(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageMovementValue()
extern "C"  float SliderHandler_PageMovementValue_m1651578409 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		float L_0 = __this->get_currentValue_1();
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		V_1 = G_B3_0;
		float L_3 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		float L_4 = SliderHandler_PageUpMovementBound_m2929319993(__this, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0048;
		}
	}
	{
		float L_5 = V_0;
		float L_6 = __this->get_size_2();
		int32_t L_7 = V_1;
		V_0 = ((float)((float)L_5+(float)((float)((float)((float)((float)L_6*(float)(((float)((float)L_7)))))*(float)(0.9f)))));
		goto IL_005a;
	}

IL_0048:
	{
		float L_8 = V_0;
		float L_9 = __this->get_size_2();
		int32_t L_10 = V_1;
		V_0 = ((float)((float)L_8-(float)((float)((float)((float)((float)L_9*(float)(((float)((float)L_10)))))*(float)(0.9f)))));
	}

IL_005a:
	{
		float L_11 = V_0;
		float L_12 = SliderHandler_Clamp_m291298090(__this, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  float SliderHandler_PageMovementValue_m1651578409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_PageMovementValue_m1651578409(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::PageUpMovementBound()
extern "C"  float SliderHandler_PageUpMovementBound_m2929319993 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		Rect_t3681755626  L_1 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_2-(float)L_4));
	}

IL_0029:
	{
		Rect_t3681755626  L_5 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_yMax_m2915146103((&V_2), /*hidden argument*/NULL);
		Rect_t3681755626  L_7 = __this->get_position_0();
		V_3 = L_7;
		float L_8 = Rect_get_y_m1393582395((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_6-(float)L_8));
	}
}
extern "C"  float SliderHandler_PageUpMovementBound_m2929319993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_PageUpMovementBound_m2929319993(_thisAdjusted, method);
}
// UnityEngine.Event UnityEngine.SliderHandler::CurrentEvent()
extern "C"  Event_t3028476042 * SliderHandler_CurrentEvent_m2481129493 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	{
		Event_t3028476042 * L_0 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Event_t3028476042 * SliderHandler_CurrentEvent_m2481129493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_CurrentEvent_m2481129493(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValueForCurrentMousePosition()
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m1752598323 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		float L_1 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		float L_4 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		float L_5 = __this->get_start_3();
		float L_6 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_1-(float)((float)((float)L_3*(float)(0.5f)))))/(float)L_4))+(float)L_5))-(float)((float)((float)L_6*(float)(0.5f)))));
	}

IL_0042:
	{
		float L_7 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_8 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		float L_10 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		float L_11 = __this->get_start_3();
		float L_12 = __this->get_size_2();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_7-(float)((float)((float)L_9*(float)(0.5f)))))/(float)L_10))+(float)L_11))-(float)((float)((float)L_12*(float)(0.5f)))));
	}
}
extern "C"  float SliderHandler_ValueForCurrentMousePosition_m1752598323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ValueForCurrentMousePosition_m1752598323(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::Clamp(System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_Clamp_m291298090_MetadataUsageId;
extern "C"  float SliderHandler_Clamp_m291298090 (SliderHandler_t3550500579 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_Clamp_m291298090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		float L_1 = SliderHandler_MinValue_m229001767(__this, /*hidden argument*/NULL);
		float L_2 = SliderHandler_MaxValue_m781424109(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  float SliderHandler_Clamp_m291298090_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_Clamp_m291298090(_thisAdjusted, ___value0, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbSelectionRect()
extern "C"  Rect_t3681755626  SliderHandler_ThumbSelectionRect_m1949915148 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Rect_t3681755626  L_0 = SliderHandler_ThumbRect_m4193953892(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = ((int32_t)12);
		float L_1 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = V_1;
		if ((!(((float)L_1) < ((float)(((float)((float)L_2)))))))
		{
			goto IL_003f;
		}
	}
	{
		Rect_t3681755626 * L_3 = (&V_0);
		float L_4 = Rect_get_x_m1393582490(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		float L_6 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m3783700513(L_3, ((float)((float)L_4-(float)((float)((float)((float)((float)(((float)((float)L_5)))-(float)L_6))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		Rect_set_width_m1921257731((&V_0), (((float)((float)L_7))), /*hidden argument*/NULL);
	}

IL_003f:
	{
		float L_8 = Rect_get_height_m3128694305((&V_0), /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		if ((!(((float)L_8) < ((float)(((float)((float)L_9)))))))
		{
			goto IL_0074;
		}
	}
	{
		Rect_t3681755626 * L_10 = (&V_0);
		float L_11 = Rect_get_y_m1393582395(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		float L_13 = Rect_get_height_m3128694305((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m4294916608(L_10, ((float)((float)L_11-(float)((float)((float)((float)((float)(((float)((float)L_12)))-(float)L_13))/(float)(2.0f))))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		Rect_set_height_m2019122814((&V_0), (((float)((float)L_14))), /*hidden argument*/NULL);
	}

IL_0074:
	{
		Rect_t3681755626  L_15 = V_0;
		return L_15;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_ThumbSelectionRect_m1949915148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ThumbSelectionRect_m1949915148(_thisAdjusted, method);
}
// System.Void UnityEngine.SliderHandler::StartDraggingWithValue(System.Single)
extern "C"  void SliderHandler_StartDraggingWithValue_m1407392347 (SliderHandler_t3550500579 * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderState_t1595681032 * V_0 = NULL;
	{
		SliderState_t1595681032 * L_0 = SliderHandler_SliderState_m3520725942(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		SliderState_t1595681032 * L_1 = V_0;
		float L_2 = SliderHandler_MousePosition_m4110511062(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_dragStartPos_0(L_2);
		SliderState_t1595681032 * L_3 = V_0;
		float L_4 = ___dragStartValue0;
		NullCheck(L_3);
		L_3->set_dragStartValue_1(L_4);
		SliderState_t1595681032 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_isDragging_2((bool)1);
		return;
	}
}
extern "C"  void SliderHandler_StartDraggingWithValue_m1407392347_AdjustorThunk (Il2CppObject * __this, float ___dragStartValue0, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	SliderHandler_StartDraggingWithValue_m1407392347(_thisAdjusted, ___dragStartValue0, method);
}
// UnityEngine.SliderState UnityEngine.SliderHandler::SliderState()
extern const Il2CppType* SliderState_t1595681032_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* SliderState_t1595681032_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_SliderState_m3520725942_MetadataUsageId;
extern "C"  SliderState_t1595681032 * SliderHandler_SliderState_m3520725942 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_SliderState_m3520725942_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(SliderState_t1595681032_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_1 = __this->get_id_8();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = GUIUtility_GetStateObject_m3509738425(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((SliderState_t1595681032 *)CastclassClass(L_2, SliderState_t1595681032_il2cpp_TypeInfo_var));
	}
}
extern "C"  SliderState_t1595681032 * SliderHandler_SliderState_m3520725942_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_SliderState_m3520725942(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::ThumbRect()
extern "C"  Rect_t3681755626  SliderHandler_ThumbRect_m4193953892 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Rect_t3681755626  L_1 = SliderHandler_HorizontalThumbRect_m1760436800(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Rect_t3681755626  L_2 = SliderHandler_VerticalThumbRect_m1555251118(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_ThumbRect_m4193953892_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ThumbRect_m4193953892(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::VerticalThumbRect()
extern "C"  Rect_t3681755626  SliderHandler_VerticalThumbRect_m1555251118 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		Rect_t3681755626  L_3 = __this->get_position_0();
		V_1 = L_3;
		float L_4 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_5 = __this->get_slider_5();
		NullCheck(L_5);
		RectOffset_t3387826427 * L_6 = GUIStyle_get_padding_m4076916754(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_left_m439065308(L_6, /*hidden argument*/NULL);
		float L_8 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_start_3();
		float L_10 = V_0;
		Rect_t3681755626  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m1393582395((&V_2), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3387826427 * L_14 = GUIStyle_get_padding_m4076916754(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m3629049358(L_14, /*hidden argument*/NULL);
		Rect_t3681755626  L_16 = __this->get_position_0();
		V_3 = L_16;
		float L_17 = Rect_get_width_m1138015702((&V_3), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_18 = __this->get_slider_5();
		NullCheck(L_18);
		RectOffset_t3387826427 * L_19 = GUIStyle_get_padding_m4076916754(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_horizontal_m3818523637(L_19, /*hidden argument*/NULL);
		float L_21 = __this->get_size_2();
		float L_22 = V_0;
		float L_23 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)L_4+(float)(((float)((float)L_7))))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8-(float)L_9))*(float)L_10))+(float)L_12))+(float)(((float)((float)L_15))))), ((float)((float)L_17-(float)(((float)((float)L_20))))), ((float)((float)((float)((float)L_21*(float)L_22))+(float)L_23)), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		Rect_t3681755626  L_25 = __this->get_position_0();
		V_4 = L_25;
		float L_26 = Rect_get_x_m1393582490((&V_4), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_27 = __this->get_slider_5();
		NullCheck(L_27);
		RectOffset_t3387826427 * L_28 = GUIStyle_get_padding_m4076916754(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_29 = RectOffset_get_left_m439065308(L_28, /*hidden argument*/NULL);
		float L_30 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_31 = __this->get_size_2();
		float L_32 = __this->get_start_3();
		float L_33 = V_0;
		Rect_t3681755626  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m1393582395((&V_5), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_36 = __this->get_slider_5();
		NullCheck(L_36);
		RectOffset_t3387826427 * L_37 = GUIStyle_get_padding_m4076916754(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = RectOffset_get_top_m3629049358(L_37, /*hidden argument*/NULL);
		Rect_t3681755626  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_width_m1138015702((&V_6), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_41 = __this->get_slider_5();
		NullCheck(L_41);
		RectOffset_t3387826427 * L_42 = GUIStyle_get_padding_m4076916754(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_horizontal_m3818523637(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_size_2();
		float L_45 = V_0;
		float L_46 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_47;
		memset(&L_47, 0, sizeof(L_47));
		Rect__ctor_m1220545469(&L_47, ((float)((float)L_26+(float)(((float)((float)L_29))))), ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))-(float)L_32))*(float)L_33))+(float)L_35))+(float)(((float)((float)L_38))))), ((float)((float)L_40-(float)(((float)((float)L_43))))), ((float)((float)((float)((float)L_44*(float)((-L_45))))+(float)L_46)), /*hidden argument*/NULL);
		return L_47;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_VerticalThumbRect_m1555251118_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_VerticalThumbRect_m1555251118(_thisAdjusted, method);
}
// UnityEngine.Rect UnityEngine.SliderHandler::HorizontalThumbRect()
extern "C"  Rect_t3681755626  SliderHandler_HorizontalThumbRect_m1760436800 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t3681755626  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = SliderHandler_ValuesPerPixel_m834671253(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = __this->get_start_3();
		float L_2 = __this->get_end_4();
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_009d;
		}
	}
	{
		float L_3 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_4 = __this->get_start_3();
		float L_5 = V_0;
		Rect_t3681755626  L_6 = __this->get_position_0();
		V_1 = L_6;
		float L_7 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_8 = __this->get_slider_5();
		NullCheck(L_8);
		RectOffset_t3387826427 * L_9 = GUIStyle_get_padding_m4076916754(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_left_m439065308(L_9, /*hidden argument*/NULL);
		Rect_t3681755626  L_11 = __this->get_position_0();
		V_2 = L_11;
		float L_12 = Rect_get_y_m1393582395((&V_2), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_13 = __this->get_slider_5();
		NullCheck(L_13);
		RectOffset_t3387826427 * L_14 = GUIStyle_get_padding_m4076916754(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m3629049358(L_14, /*hidden argument*/NULL);
		float L_16 = __this->get_size_2();
		float L_17 = V_0;
		float L_18 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_19 = __this->get_position_0();
		V_3 = L_19;
		float L_20 = Rect_get_height_m3128694305((&V_3), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_21 = __this->get_slider_5();
		NullCheck(L_21);
		RectOffset_t3387826427 * L_22 = GUIStyle_get_padding_m4076916754(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = RectOffset_get_vertical_m3856345169(L_22, /*hidden argument*/NULL);
		Rect_t3681755626  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Rect__ctor_m1220545469(&L_24, ((float)((float)((float)((float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5))+(float)L_7))+(float)(((float)((float)L_10))))), ((float)((float)L_12+(float)(((float)((float)L_15))))), ((float)((float)((float)((float)L_16*(float)L_17))+(float)L_18)), ((float)((float)L_20-(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		return L_24;
	}

IL_009d:
	{
		float L_25 = SliderHandler_ClampedCurrentValue_m1479539118(__this, /*hidden argument*/NULL);
		float L_26 = __this->get_size_2();
		float L_27 = __this->get_start_3();
		float L_28 = V_0;
		Rect_t3681755626  L_29 = __this->get_position_0();
		V_4 = L_29;
		float L_30 = Rect_get_x_m1393582490((&V_4), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_31 = __this->get_slider_5();
		NullCheck(L_31);
		RectOffset_t3387826427 * L_32 = GUIStyle_get_padding_m4076916754(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = RectOffset_get_left_m439065308(L_32, /*hidden argument*/NULL);
		Rect_t3681755626  L_34 = __this->get_position_0();
		V_5 = L_34;
		float L_35 = Rect_get_y_m1393582395((&V_5), /*hidden argument*/NULL);
		float L_36 = __this->get_size_2();
		float L_37 = V_0;
		float L_38 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		Rect_t3681755626  L_39 = __this->get_position_0();
		V_6 = L_39;
		float L_40 = Rect_get_height_m3128694305((&V_6), /*hidden argument*/NULL);
		Rect_t3681755626  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Rect__ctor_m1220545469(&L_41, ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_25+(float)L_26))-(float)L_27))*(float)L_28))+(float)L_30))+(float)(((float)((float)L_33))))), L_35, ((float)((float)((float)((float)L_36*(float)((-L_37))))+(float)L_38)), L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
extern "C"  Rect_t3681755626  SliderHandler_HorizontalThumbRect_m1760436800_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_HorizontalThumbRect_m1760436800(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ClampedCurrentValue()
extern "C"  float SliderHandler_ClampedCurrentValue_m1479539118 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_currentValue_1();
		float L_1 = SliderHandler_Clamp_m291298090(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  float SliderHandler_ClampedCurrentValue_m1479539118_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ClampedCurrentValue_m1479539118(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MousePosition()
extern "C"  float SliderHandler_MousePosition_m4110511062 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		Event_t3028476042 * L_1 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_2 = Event_get_mousePosition_m3789571399(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		Rect_t3681755626  L_4 = __this->get_position_0();
		V_1 = L_4;
		float L_5 = Rect_get_x_m1393582490((&V_1), /*hidden argument*/NULL);
		return ((float)((float)L_3-(float)L_5));
	}

IL_002e:
	{
		Event_t3028476042 * L_6 = SliderHandler_CurrentEvent_m2481129493(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t2243707579  L_7 = Event_get_mousePosition_m3789571399(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_y_2();
		Rect_t3681755626  L_9 = __this->get_position_0();
		V_3 = L_9;
		float L_10 = Rect_get_y_m1393582395((&V_3), /*hidden argument*/NULL);
		return ((float)((float)L_8-(float)L_10));
	}
}
extern "C"  float SliderHandler_MousePosition_m4110511062_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_MousePosition_m4110511062(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ValuesPerPixel()
extern "C"  float SliderHandler_ValuesPerPixel_m834671253 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		Rect_t3681755626  L_1 = __this->get_position_0();
		V_0 = L_1;
		float L_2 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_3 = __this->get_slider_5();
		NullCheck(L_3);
		RectOffset_t3387826427 * L_4 = GUIStyle_get_padding_m4076916754(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_horizontal_m3818523637(L_4, /*hidden argument*/NULL);
		float L_6 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		float L_7 = __this->get_end_4();
		float L_8 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_2-(float)(((float)((float)L_5)))))-(float)L_6))/(float)((float)((float)L_7-(float)L_8))));
	}

IL_0041:
	{
		Rect_t3681755626  L_9 = __this->get_position_0();
		V_1 = L_9;
		float L_10 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_11 = __this->get_slider_5();
		NullCheck(L_11);
		RectOffset_t3387826427 * L_12 = GUIStyle_get_padding_m4076916754(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_vertical_m3856345169(L_12, /*hidden argument*/NULL);
		float L_14 = SliderHandler_ThumbSize_m3714327193(__this, /*hidden argument*/NULL);
		float L_15 = __this->get_end_4();
		float L_16 = __this->get_start_3();
		return ((float)((float)((float)((float)((float)((float)L_10-(float)(((float)((float)L_13)))))-(float)L_14))/(float)((float)((float)L_15-(float)L_16))));
	}
}
extern "C"  float SliderHandler_ValuesPerPixel_m834671253_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ValuesPerPixel_m834671253(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::ThumbSize()
extern "C"  float SliderHandler_ThumbSize_m3714327193 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	{
		bool L_0 = __this->get_horiz_7();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t1799908754 * L_1 = __this->get_thumb_6();
		NullCheck(L_1);
		float L_2 = GUIStyle_get_fixedWidth_m97997484(L_1, /*hidden argument*/NULL);
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0030;
		}
	}
	{
		GUIStyle_t1799908754 * L_3 = __this->get_thumb_6();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedWidth_m97997484(L_3, /*hidden argument*/NULL);
		G_B4_0 = L_4;
		goto IL_0041;
	}

IL_0030:
	{
		GUIStyle_t1799908754 * L_5 = __this->get_thumb_6();
		NullCheck(L_5);
		RectOffset_t3387826427 * L_6 = GUIStyle_get_padding_m4076916754(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_horizontal_m3818523637(L_6, /*hidden argument*/NULL);
		G_B4_0 = (((float)((float)L_7)));
	}

IL_0041:
	{
		return G_B4_0;
	}

IL_0042:
	{
		GUIStyle_t1799908754 * L_8 = __this->get_thumb_6();
		NullCheck(L_8);
		float L_9 = GUIStyle_get_fixedHeight_m414733479(L_8, /*hidden argument*/NULL);
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		GUIStyle_t1799908754 * L_10 = __this->get_thumb_6();
		NullCheck(L_10);
		float L_11 = GUIStyle_get_fixedHeight_m414733479(L_10, /*hidden argument*/NULL);
		G_B8_0 = L_11;
		goto IL_0078;
	}

IL_0067:
	{
		GUIStyle_t1799908754 * L_12 = __this->get_thumb_6();
		NullCheck(L_12);
		RectOffset_t3387826427 * L_13 = GUIStyle_get_padding_m4076916754(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_vertical_m3856345169(L_13, /*hidden argument*/NULL);
		G_B8_0 = (((float)((float)L_14)));
	}

IL_0078:
	{
		return G_B8_0;
	}
}
extern "C"  float SliderHandler_ThumbSize_m3714327193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_ThumbSize_m3714327193(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MaxValue()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MaxValue_m781424109_MetadataUsageId;
extern "C"  float SliderHandler_MaxValue_m781424109 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MaxValue_m781424109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_size_2();
		return ((float)((float)L_2-(float)L_3));
	}
}
extern "C"  float SliderHandler_MaxValue_m781424109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_MaxValue_m781424109(_thisAdjusted, method);
}
// System.Single UnityEngine.SliderHandler::MinValue()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t SliderHandler_MinValue_m229001767_MetadataUsageId;
extern "C"  float SliderHandler_MinValue_m229001767 (SliderHandler_t3550500579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SliderHandler_MinValue_m229001767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_start_3();
		float L_1 = __this->get_end_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float SliderHandler_MinValue_m229001767_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	SliderHandler_t3550500579 * _thisAdjusted = reinterpret_cast<SliderHandler_t3550500579 *>(__this + 1);
	return SliderHandler_MinValue_m229001767(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_pinvoke(const SliderHandler_t3550500579& unmarshaled, SliderHandler_t3550500579_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t3550500579_marshal_pinvoke_back(const SliderHandler_t3550500579_marshaled_pinvoke& marshaled, SliderHandler_t3550500579& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_pinvoke_cleanup(SliderHandler_t3550500579_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_com(const SliderHandler_t3550500579& unmarshaled, SliderHandler_t3550500579_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
extern "C" void SliderHandler_t3550500579_marshal_com_back(const SliderHandler_t3550500579_marshaled_com& marshaled, SliderHandler_t3550500579& unmarshaled)
{
	Il2CppCodeGenException* ___slider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'slider' of type 'SliderHandler': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___slider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SliderHandler
extern "C" void SliderHandler_t3550500579_marshal_com_cleanup(SliderHandler_t3550500579_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SliderState::.ctor()
extern "C"  void SliderState__ctor_m1096533539 (SliderState_t1595681032 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C"  void GameCenterPlatform__ctor_m644203297 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern Il2CppClass* AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4117976357_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2974994212_MethodInfo_var;
extern const uint32_t GameCenterPlatform__cctor_m2403939600_MetadataUsageId;
extern "C"  void GameCenterPlatform__cctor_m2403939600 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform__cctor_m2403939600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t847281182*)SZArrayNew(AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_friends_10(((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)));
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_users_11(((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)));
		List_1_t4117976357 * L_0 = (List_1_t4117976357 *)il2cpp_codegen_object_new(List_1_t4117976357_il2cpp_TypeInfo_var);
		List_1__ctor_m2974994212(L_0, /*hidden argument*/List_1__ctor_m2974994212_MethodInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_m_GcBoards_14(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m692395677_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m692395677 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___user0, Action_1_t3627374100 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m692395677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_FriendsCallback_1(L_0);
		GameCenterPlatform_Internal_LoadFriends_m3035019738(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1019748987_MetadataUsageId;
extern "C"  void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1019748987 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___user0, Action_1_t3627374100 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m1019748987_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0(L_0);
		GameCenterPlatform_Internal_Authenticate_m3797365482(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C"  void GameCenterPlatform_Internal_Authenticate_m3797365482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m3797365482_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m3797365482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m3797365482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C"  bool GameCenterPlatform_Internal_Authenticated_m4294501884 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m4294501884_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m4294501884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m4294501884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C"  String_t* GameCenterPlatform_Internal_UserName_m3048265218 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m3048265218_ftn) ();
	static GameCenterPlatform_Internal_UserName_m3048265218_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m3048265218_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C"  String_t* GameCenterPlatform_Internal_UserID_m1103178632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m1103178632_ftn) ();
	static GameCenterPlatform_Internal_UserID_m1103178632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m1103178632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C"  bool GameCenterPlatform_Internal_Underage_m2690511558 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m2690511558_ftn) ();
	static GameCenterPlatform_Internal_Underage_m2690511558_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m2690511558_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C"  Texture2D_t3542995729 * GameCenterPlatform_Internal_UserImage_m915316496 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3542995729 * (*GameCenterPlatform_Internal_UserImage_m915316496_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m915316496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m915316496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C"  void GameCenterPlatform_Internal_LoadFriends_m3035019738 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m3035019738_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m3035019738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m3035019738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C"  void GameCenterPlatform_Internal_LoadAchievementDescriptions_m631344173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m631344173_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m631344173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m631344173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C"  void GameCenterPlatform_Internal_LoadAchievements_m2701153839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m2701153839_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m2701153839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m2701153839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C"  void GameCenterPlatform_Internal_ReportProgress_m1293200984 (Il2CppObject * __this /* static, unused */, String_t* ___id0, double ___progress1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m1293200984_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m1293200984_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m1293200984_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id0, ___progress1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C"  void GameCenterPlatform_Internal_ReportScore_m2380287015 (Il2CppObject * __this /* static, unused */, int64_t ___score0, String_t* ___category1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m2380287015_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m2380287015_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m2380287015_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score0, ___category1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C"  void GameCenterPlatform_Internal_LoadScores_m4195262886 (Il2CppObject * __this /* static, unused */, String_t* ___category0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m4195262886_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m4195262886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m4195262886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C"  void GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C"  void GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C"  void GameCenterPlatform_Internal_LoadUsers_m3870253977 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___userIds0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m3870253977_ftn) (StringU5BU5D_t1642385972*);
	static GameCenterPlatform_Internal_LoadUsers_m3870253977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m3870253977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C"  void GameCenterPlatform_Internal_ResetAllAchievements_m3489790181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m3489790181_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m3489790181_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m3489790181_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C"  void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ResetAllAchievements_m4114806314_MetadataUsageId;
extern "C"  void GameCenterPlatform_ResetAllAchievements_m4114806314 (Il2CppObject * __this /* static, unused */, Action_1_t3627374100 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ResetAllAchievements_m4114806314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t3627374100 * L_0 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_ResetAchievements_12(L_0);
		GameCenterPlatform_Internal_ResetAllAchievements_m3489790181(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m534321293_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m534321293 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m534321293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m4005094965(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m2527518460_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m2527518460 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m2527518460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int32_t L_1 = ___timeScope1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C"  void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780 (Il2CppObject * __this /* static, unused */, String_t* ___leaderboardID0, int32_t ___timeScope1, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m915894780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID0, ___timeScope1);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearAchievementDescriptions_m4063396811_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearAchievementDescriptions_m4063396811 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearAchievementDescriptions_m4063396811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_1);
		int32_t L_2 = ___size0;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_adCache_9(((AchievementDescriptionU5BU5D_t847281182*)SZArrayNew(AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var, (uint32_t)L_3)));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetAchievementDescription_m1025952251_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescription_m1025952251 (Il2CppObject * __this /* static, unused */, GcAchievementDescriptionData_t960725851  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescription_m1025952251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_1 = ___number1;
		AchievementDescription_t3110978151 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m1135716620((&___data0), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (AchievementDescription_t3110978151 *)L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2731130621;
extern const uint32_t GameCenterPlatform_SetAchievementDescriptionImage_m2184571696_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetAchievementDescriptionImage_m2184571696 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetAchievementDescriptionImage_m2184571696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_0);
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2731130621, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_3 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		int32_t L_4 = ___number1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		AchievementDescription_t3110978151 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Texture2D_t3542995729 * L_7 = ___texture0;
		NullCheck(L_6);
		AchievementDescription_SetImage_m1395221782(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m943750401_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2035814694;
extern const uint32_t GameCenterPlatform_TriggerAchievementDescriptionCallback_m382485689_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerAchievementDescriptionCallback_m382485689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerAchievementDescriptionCallback_m382485689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3885079697 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t847281182* L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2035814694, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3885079697 * L_3 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementDescriptionLoaderCallback_2();
		AchievementDescriptionU5BU5D_t847281182* L_4 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_adCache_9();
		NullCheck(L_3);
		Action_1_Invoke_m943750401(L_3, (IAchievementDescriptionU5BU5D_t4083280315*)(IAchievementDescriptionU5BU5D_t4083280315*)L_4, /*hidden argument*/Action_1_Invoke_m943750401_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_AuthenticateCallbackWrapper_m1619241663_MetadataUsageId;
extern "C"  void GameCenterPlatform_AuthenticateCallbackWrapper_m1619241663 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AuthenticateCallbackWrapper_m1619241663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t3627374100 * G_B3_0 = NULL;
	Action_1_t3627374100 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Action_1_t3627374100 * G_B4_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2282436159(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t3627374100 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AuthenticateCallback_0();
		int32_t L_2 = ___result0;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m3662000152(G_B4_1, (bool)G_B4_0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_AuthenticateCallback_0((Action_1_t3627374100 *)NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearFriends_m1742022050_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearFriends_m1742022050 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearFriends_m1742022050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2690967919(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriends_m676763082_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriends_m676763082 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t3198293052  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriends_m676763082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m2451723029((&___data0), (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetFriendImage_m1119516317_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetFriendImage_m1119516317 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetFriendImage_m1119516317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m4283674749(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_friends_10()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerFriendsCallbackWrapper_m809905571_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerFriendsCallbackWrapper_m809905571 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerFriendsCallbackWrapper_m809905571_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t3627374100 * G_B5_0 = NULL;
	Action_1_t3627374100 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	Action_1_t3627374100 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t2930725895* L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		UserProfileU5BU5D_t2930725895* L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_friends_10();
		NullCheck(L_1);
		LocalUser_SetFriends_m3706685636(L_1, (IUserProfileU5BU5D_t3461248430*)(IUserProfileU5BU5D_t3461248430*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_3 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_4 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_FriendsCallback_1();
		int32_t L_5 = ___result0;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m3662000152(G_B6_1, (bool)G_B6_0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3760172603_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2309484702;
extern const uint32_t GameCenterPlatform_AchievementCallbackWrapper_m1306048684_MetadataUsageId;
extern "C"  void GameCenterPlatform_AchievementCallbackWrapper_m1306048684 (Il2CppObject * __this /* static, unused */, GcAchievementDataU5BU5D_t2283071720* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_AchievementCallbackWrapper_m1306048684_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t2450740364* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t2511354027 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t2283071720* L_1 = ___result0;
		NullCheck(L_1);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2309484702, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t2283071720* L_2 = ___result0;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t2450740364*)SZArrayNew(AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t2450740364* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t2283071720* L_5 = ___result0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t1333316625 * L_7 = GcAchievementData_ToAchievement_m962894180(((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Achievement_t1333316625 *)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t2283071720* L_10 = ___result0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t2511354027 * L_11 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_AchievementLoaderCallback_3();
		AchievementU5BU5D_t2450740364* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m3760172603(L_11, (IAchievementU5BU5D_t2709554645*)(IAchievementU5BU5D_t2709554645*)L_12, /*hidden argument*/Action_1_Invoke_m3760172603_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ProgressCallbackWrapper_m1005842297_MetadataUsageId;
extern "C"  void GameCenterPlatform_ProgressCallbackWrapper_m1005842297 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ProgressCallbackWrapper_m1005842297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ProgressCallback_4();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreCallbackWrapper_m2386426124_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreCallbackWrapper_m2386426124 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreCallbackWrapper_m2386426124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreCallback_5();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3504824494_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ScoreLoaderCallbackWrapper_m2336845377_MetadataUsageId;
extern "C"  void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2336845377 (Il2CppObject * __this /* static, unused */, GcScoreDataU5BU5D_t4052399267* ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ScoreLoaderCallbackWrapper_m2336845377_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t299013381* V_0 = NULL;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3039104018 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t4052399267* L_1 = ___result0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t299013381*)SZArrayNew(ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t299013381* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t4052399267* L_4 = ___result0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t2307748940 * L_6 = GcScoreData_ToScore_m3744988639(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t2307748940 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t4052399267* L_9 = ___result0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3039104018 * L_10 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ScoreLoaderCallback_6();
		ScoreU5BU5D_t299013381* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m3504824494(L_10, (IScoreU5BU5D_t3237304636*)(IScoreU5BU5D_t3237304636*)L_11, /*hidden argument*/Action_1_Invoke_m3504824494_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* LocalUser_t3019851150_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern const uint32_t GameCenterPlatform_get_localUser_m3187393722_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_get_localUser_m3187393722 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_get_localUser_m3187393722_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t3019851150 * L_1 = (LocalUser_t3019851150 *)il2cpp_codegen_object_new(LocalUser_t3019851150_il2cpp_TypeInfo_var);
		LocalUser__ctor_m456101162(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_m_LocalUser_13(L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m4294501884(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_3 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		NullCheck(L_3);
		String_t* L_4 = UserProfile_get_id_m1121636229(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral372029326, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m2282436159(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_6 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_PopulateLocalUser_m2282436159_MetadataUsageId;
extern "C"  void GameCenterPlatform_PopulateLocalUser_m2282436159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_PopulateLocalUser_m2282436159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		LocalUser_t3019851150 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m4294501884(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m3483845210(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m3048265218(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m3667428096(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_4 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m1103178632(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m3818116510(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_6 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		bool L_7 = GameCenterPlatform_Internal_Underage_m2690511558(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m3689639158(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t3019851150 * L_8 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_LocalUser_13();
		Texture2D_t3542995729 * L_9 = GameCenterPlatform_Internal_UserImage_m915316496(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m3142478163(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern Il2CppClass* AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m943750401_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievementDescriptions_m293745755_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievementDescriptions_m293745755 (GameCenterPlatform_t2156144444 * __this, Action_1_t3885079697 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievementDescriptions_m293745755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3885079697 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m943750401(L_1, (IAchievementDescriptionU5BU5D_t4083280315*)(IAchievementDescriptionU5BU5D_t4083280315*)((AchievementDescriptionU5BU5D_t847281182*)SZArrayNew(AchievementDescriptionU5BU5D_t847281182_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m943750401_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3885079697 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementDescriptionLoaderCallback_2(L_2);
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m631344173(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportProgress_m3585652631_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportProgress_m3585652631 (GameCenterPlatform_t2156144444 * __this, String_t* ___id0, double ___progress1, Action_1_t3627374100 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportProgress_m3585652631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t3627374100 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_ProgressCallback_4(L_2);
		String_t* L_3 = ___id0;
		double L_4 = ___progress1;
		GameCenterPlatform_Internal_ReportProgress_m1293200984(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern Il2CppClass* AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3760172603_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadAchievements_m200011543_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadAchievements_m200011543 (GameCenterPlatform_t2156144444 * __this, Action_1_t2511354027 * ___callback0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadAchievements_m200011543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t2511354027 * L_1 = ___callback0;
		NullCheck(L_1);
		Action_1_Invoke_m3760172603(L_1, (IAchievementU5BU5D_t2709554645*)(IAchievementU5BU5D_t2709554645*)((AchievementU5BU5D_t2450740364*)SZArrayNew(AchievementU5BU5D_t2450740364_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m3760172603_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t2511354027 * L_2 = ___callback0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_AchievementLoaderCallback_3(L_2);
		GameCenterPlatform_Internal_LoadAchievements_m2701153839(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_ReportScore_m3720143724_MetadataUsageId;
extern "C"  void GameCenterPlatform_ReportScore_m3720143724 (GameCenterPlatform_t2156144444 * __this, int64_t ___score0, String_t* ___board1, Action_1_t3627374100 * ___callback2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ReportScore_m3720143724_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback2;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t3627374100 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreCallback_5(L_2);
		int64_t L_3 = ___score0;
		String_t* L_4 = ___board1;
		GameCenterPlatform_Internal_ReportScore_m2380287015(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern Il2CppClass* ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3504824494_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2160889205_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2160889205 (GameCenterPlatform_t2156144444 * __this, String_t* ___category0, Action_1_t3039104018 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2160889205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3039104018 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m3504824494(L_1, (IScoreU5BU5D_t3237304636*)(IScoreU5BU5D_t3237304636*)((ScoreU5BU5D_t299013381*)SZArrayNew(ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m3504824494_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3039104018 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_ScoreLoaderCallback_6(L_2);
		String_t* L_3 = ___category0;
		GameCenterPlatform_Internal_LoadScores_m4195262886(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t4160680639_il2cpp_TypeInfo_var;
extern Il2CppClass* GcLeaderboard_t453887929_il2cpp_TypeInfo_var;
extern Il2CppClass* ILeaderboard_t77027648_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1201759976_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadScores_m2122243871_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadScores_m2122243871 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___board0, Action_1_t3627374100 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadScores_m2122243871_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t4160680639 * V_0 = NULL;
	GcLeaderboard_t453887929 * V_1 = NULL;
	Range_t3455291607  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Range_t3455291607  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t3627374100 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, (bool)0, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t3627374100 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_LeaderboardCallback_7(L_2);
		Il2CppObject * L_3 = ___board0;
		V_0 = ((Leaderboard_t4160680639 *)CastclassClass(L_3, Leaderboard_t4160680639_il2cpp_TypeInfo_var));
		Leaderboard_t4160680639 * L_4 = V_0;
		GcLeaderboard_t453887929 * L_5 = (GcLeaderboard_t453887929 *)il2cpp_codegen_object_new(GcLeaderboard_t453887929_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m983739183(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t4117976357 * L_6 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		GcLeaderboard_t453887929 * L_7 = V_1;
		NullCheck(L_6);
		List_1_Add_m1201759976(L_6, L_7, /*hidden argument*/List_1_Add_m1201759976_MethodInfo_var);
		Leaderboard_t4160680639 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t1642385972* L_9 = Leaderboard_GetUserFilter_m4114287667(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t453887929 * L_10 = V_1;
		Il2CppObject * L_11 = ___board0;
		NullCheck(L_11);
		String_t* L_12 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_11);
		Il2CppObject * L_13 = ___board0;
		NullCheck(L_13);
		int32_t L_14 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t4160680639 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t1642385972* L_16 = Leaderboard_GetUserFilter_m4114287667(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m1836788974(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t453887929 * L_17 = V_1;
		Il2CppObject * L_18 = ___board0;
		NullCheck(L_18);
		String_t* L_19 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_18);
		Il2CppObject * L_20 = ___board0;
		NullCheck(L_20);
		Range_t3455291607  L_21 = InterfaceFuncInvoker0< Range_t3455291607  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = (&V_2)->get_from_0();
		Il2CppObject * L_23 = ___board0;
		NullCheck(L_23);
		Range_t3455291607  L_24 = InterfaceFuncInvoker0< Range_t3455291607  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = (&V_3)->get_count_1();
		Il2CppObject * L_26 = ___board0;
		NullCheck(L_26);
		int32_t L_27 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_26);
		Il2CppObject * L_28 = ___board0;
		NullCheck(L_28);
		int32_t L_29 = InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t77027648_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m3771229037(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LeaderboardCallbackWrapper_m1317866993_MetadataUsageId;
extern "C"  void GameCenterPlatform_LeaderboardCallbackWrapper_m1317866993 (Il2CppObject * __this /* static, unused */, bool ___success0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LeaderboardCallbackWrapper_m1317866993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_LeaderboardCallback_7();
		bool L_2 = ___success0;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern Il2CppClass* Leaderboard_t4160680639_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t3652706031_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m835756033_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m844722661_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1731154041_MethodInfo_var;
extern const uint32_t GameCenterPlatform_GetLoading_m2902653631_MetadataUsageId;
extern "C"  bool GameCenterPlatform_GetLoading_m2902653631 (GameCenterPlatform_t2156144444 * __this, Il2CppObject * ___board0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_GetLoading_m2902653631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t453887929 * V_0 = NULL;
	Enumerator_t3652706031  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		List_1_t4117976357 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_m_GcBoards_14();
		NullCheck(L_1);
		Enumerator_t3652706031  L_2 = List_1_GetEnumerator_m835756033(L_1, /*hidden argument*/List_1_GetEnumerator_m835756033_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t453887929 * L_3 = Enumerator_get_Current_m844722661((&V_1), /*hidden argument*/Enumerator_get_Current_m844722661_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t453887929 * L_4 = V_0;
			Il2CppObject * L_5 = ___board0;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m3937847094(L_4, ((Leaderboard_t4160680639 *)CastclassClass(L_5, Leaderboard_t4160680639_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t453887929 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m1117879034(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m1731154041((&V_1), /*hidden argument*/Enumerator_MoveNext_m1731154041_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t3652706031  L_10 = V_1;
		Enumerator_t3652706031  L_11 = L_10;
		Il2CppObject * L_12 = Box(Enumerator_t3652706031_il2cpp_TypeInfo_var, &L_11);
		NullCheck((Il2CppObject *)L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005f:
	{
		return (bool)0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern Il2CppClass* ILocalUser_t2210666073_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4014904582;
extern const uint32_t GameCenterPlatform_VerifyAuthentication_m4148852888_MetadataUsageId;
extern "C"  bool GameCenterPlatform_VerifyAuthentication_m4148852888 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_VerifyAuthentication_m4148852888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = GameCenterPlatform_get_localUser_m3187393722(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t2210666073_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral4014904582, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_001c:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowAchievementsUI_m217572822_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowAchievementsUI_m217572822 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowAchievementsUI_m217572822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m4211451772(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ShowLeaderboardUI_m3149996419_MetadataUsageId;
extern "C"  void GameCenterPlatform_ShowLeaderboardUI_m3149996419 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ShowLeaderboardUI_m3149996419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m3138464075(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_ClearUsers_m28146411_MetadataUsageId;
extern "C"  void GameCenterPlatform_ClearUsers_m28146411 (Il2CppObject * __this /* static, unused */, int32_t ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_ClearUsers_m28146411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size0;
		GameCenterPlatform_SafeClearArray_m2690967919(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUser_m4136306572_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUser_m4136306572 (Il2CppObject * __this /* static, unused */, GcUserProfileData_t3198293052  ___data0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUser_m4136306572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number1;
		GcUserProfileData_AddToArray_m2451723029((&___data0), (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SetUserImage_m3665873800_MetadataUsageId;
extern "C"  void GameCenterPlatform_SetUserImage_m3665873800 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___texture0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SetUserImage_m3665873800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_0 = ___texture0;
		int32_t L_1 = ___number1;
		GameCenterPlatform_SafeSetUserImage_m4283674749(NULL /*static, unused*/, (((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_address_of_s_users_11()), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m9088308_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerUsersCallbackWrapper_m2751952045_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerUsersCallbackWrapper_m2751952045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerUsersCallbackWrapper_m2751952045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3263047812 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3263047812 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_UsersCallback_8();
		UserProfileU5BU5D_t2930725895* L_2 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_users_11();
		NullCheck(L_1);
		Action_1_Invoke_m9088308(L_1, (IUserProfileU5BU5D_t3461248430*)(IUserProfileU5BU5D_t3461248430*)L_2, /*hidden argument*/Action_1_Invoke_m9088308_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m9088308_MethodInfo_var;
extern const uint32_t GameCenterPlatform_LoadUsers_m4218470560_MetadataUsageId;
extern "C"  void GameCenterPlatform_LoadUsers_m4218470560 (GameCenterPlatform_t2156144444 * __this, StringU5BU5D_t1642385972* ___userIds0, Action_1_t3263047812 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_LoadUsers_m4218470560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m4148852888(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t3263047812 * L_1 = ___callback1;
		NullCheck(L_1);
		Action_1_Invoke_m9088308(L_1, (IUserProfileU5BU5D_t3461248430*)(IUserProfileU5BU5D_t3461248430*)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/Action_1_Invoke_m9088308_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t3263047812 * L_2 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->set_s_UsersCallback_8(L_2);
		StringU5BU5D_t1642385972* L_3 = ___userIds0;
		GameCenterPlatform_Internal_LoadUsers_m3870253977(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3515272376;
extern Il2CppCodeGenString* _stringLiteral1717534897;
extern const uint32_t GameCenterPlatform_SafeSetUserImage_m4283674749_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeSetUserImage_m4283674749 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2930725895** ___array0, Texture2D_t3542995729 * ___texture1, int32_t ___number2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeSetUserImage_m4283674749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2930725895** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_0)));
		int32_t L_1 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3515272376, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_3 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture1 = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t2930725895** L_4 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_4)));
		int32_t L_5 = ___number2;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_4)))->max_length))))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t2930725895** L_7 = ___array0;
		int32_t L_8 = ___number2;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2930725895**)L_7)), L_8);
		int32_t L_9 = L_8;
		UserProfile_t3365630962 * L_10 = ((*((UserProfileU5BU5D_t2930725895**)L_7)))->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Texture2D_t3542995729 * L_11 = ___texture1;
		NullCheck(L_10);
		UserProfile_SetImage_m3142478163(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1717534897, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_SafeClearArray_m2690967919_MetadataUsageId;
extern "C"  void GameCenterPlatform_SafeClearArray_m2690967919 (Il2CppObject * __this /* static, unused */, UserProfileU5BU5D_t2930725895** ___array0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_SafeClearArray_m2690967919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2930725895** L_0 = ___array0;
		if (!(*((UserProfileU5BU5D_t2930725895**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t2930725895** L_1 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_1)));
		int32_t L_2 = ___size1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_1)))->max_length))))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t2930725895** L_3 = ___array0;
		int32_t L_4 = ___size1;
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)L_4));
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)L_4)));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern Il2CppClass* Leaderboard_t4160680639_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateLeaderboard_m1959129937_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateLeaderboard_m1959129937 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateLeaderboard_m1959129937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t4160680639 * V_0 = NULL;
	{
		Leaderboard_t4160680639 * L_0 = (Leaderboard_t4160680639 *)il2cpp_codegen_object_new(Leaderboard_t4160680639_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m1521627019(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t4160680639 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern Il2CppClass* Achievement_t1333316625_il2cpp_TypeInfo_var;
extern const uint32_t GameCenterPlatform_CreateAchievement_m2992667237_MetadataUsageId;
extern "C"  Il2CppObject * GameCenterPlatform_CreateAchievement_m2992667237 (GameCenterPlatform_t2156144444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_CreateAchievement_m2992667237_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t1333316625 * V_0 = NULL;
	{
		Achievement_t1333316625 * L_0 = (Achievement_t1333316625 *)il2cpp_codegen_object_new(Achievement_t1333316625_il2cpp_TypeInfo_var);
		Achievement__ctor_m3960800585(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t1333316625 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern Il2CppClass* GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3662000152_MethodInfo_var;
extern const uint32_t GameCenterPlatform_TriggerResetAchievementCallback_m247723933_MetadataUsageId;
extern "C"  void GameCenterPlatform_TriggerResetAchievementCallback_m247723933 (Il2CppObject * __this /* static, unused */, bool ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameCenterPlatform_TriggerResetAchievementCallback_m247723933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_0 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var);
		Action_1_t3627374100 * L_1 = ((GameCenterPlatform_t2156144444_StaticFields*)GameCenterPlatform_t2156144444_il2cpp_TypeInfo_var->static_fields)->get_s_ResetAchievements_12();
		bool L_2 = ___result0;
		NullCheck(L_1);
		Action_1_Invoke_m3662000152(L_1, L_2, /*hidden argument*/Action_1_Invoke_m3662000152_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern Il2CppClass* Achievement_t1333316625_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementData_ToAchievement_m962894180_MetadataUsageId;
extern "C"  Achievement_t1333316625 * GcAchievementData_ToAchievement_m962894180 (GcAchievementData_t1754866149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementData_ToAchievement_m962894180_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	double G_B2_0 = 0.0;
	String_t* G_B2_1 = NULL;
	double G_B1_0 = 0.0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	double G_B3_1 = 0.0;
	String_t* G_B3_2 = NULL;
	int32_t G_B5_0 = 0;
	double G_B5_1 = 0.0;
	String_t* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	double G_B4_1 = 0.0;
	String_t* G_B4_2 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	double G_B6_2 = 0.0;
	String_t* G_B6_3 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		double L_1 = __this->get_m_PercentCompleted_1();
		int32_t L_2 = __this->get_m_Completed_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001e:
	{
		int32_t L_3 = __this->get_m_Hidden_3();
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
		if (L_3)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			G_B5_2 = G_B3_2;
			goto IL_002f;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0030;
	}

IL_002f:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0030:
	{
		DateTime__ctor_m2857738939((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_LastReportedDate_4();
		DateTime_t693205669  L_5 = DateTime_AddSeconds_m722082155((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		Achievement_t1333316625 * L_6 = (Achievement_t1333316625 *)il2cpp_codegen_object_new(Achievement_t1333316625_il2cpp_TypeInfo_var);
		Achievement__ctor_m4089961863(L_6, G_B6_3, G_B6_2, (bool)G_B6_1, (bool)G_B6_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  Achievement_t1333316625 * GcAchievementData_ToAchievement_m962894180_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementData_t1754866149 * _thisAdjusted = reinterpret_cast<GcAchievementData_t1754866149 *>(__this + 1);
	return GcAchievementData_ToAchievement_m962894180(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke(const GcAchievementData_t1754866149& unmarshaled, GcAchievementData_t1754866149_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_back(const GcAchievementData_t1754866149_marshaled_pinvoke& marshaled, GcAchievementData_t1754866149& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_pinvoke_cleanup(GcAchievementData_t1754866149_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_com(const GcAchievementData_t1754866149& unmarshaled, GcAchievementData_t1754866149_marshaled_com& marshaled)
{
	marshaled.___m_Identifier_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Identifier_0());
	marshaled.___m_PercentCompleted_1 = unmarshaled.get_m_PercentCompleted_1();
	marshaled.___m_Completed_2 = unmarshaled.get_m_Completed_2();
	marshaled.___m_Hidden_3 = unmarshaled.get_m_Hidden_3();
	marshaled.___m_LastReportedDate_4 = unmarshaled.get_m_LastReportedDate_4();
}
extern "C" void GcAchievementData_t1754866149_marshal_com_back(const GcAchievementData_t1754866149_marshaled_com& marshaled, GcAchievementData_t1754866149& unmarshaled)
{
	unmarshaled.set_m_Identifier_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Identifier_0));
	double unmarshaled_m_PercentCompleted_temp_1 = 0.0;
	unmarshaled_m_PercentCompleted_temp_1 = marshaled.___m_PercentCompleted_1;
	unmarshaled.set_m_PercentCompleted_1(unmarshaled_m_PercentCompleted_temp_1);
	int32_t unmarshaled_m_Completed_temp_2 = 0;
	unmarshaled_m_Completed_temp_2 = marshaled.___m_Completed_2;
	unmarshaled.set_m_Completed_2(unmarshaled_m_Completed_temp_2);
	int32_t unmarshaled_m_Hidden_temp_3 = 0;
	unmarshaled_m_Hidden_temp_3 = marshaled.___m_Hidden_3;
	unmarshaled.set_m_Hidden_3(unmarshaled_m_Hidden_temp_3);
	int32_t unmarshaled_m_LastReportedDate_temp_4 = 0;
	unmarshaled_m_LastReportedDate_temp_4 = marshaled.___m_LastReportedDate_4;
	unmarshaled.set_m_LastReportedDate_4(unmarshaled_m_LastReportedDate_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern "C" void GcAchievementData_t1754866149_marshal_com_cleanup(GcAchievementData_t1754866149_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Identifier_0);
	marshaled.___m_Identifier_0 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern Il2CppClass* AchievementDescription_t3110978151_il2cpp_TypeInfo_var;
extern const uint32_t GcAchievementDescriptionData_ToAchievementDescription_m1135716620_MetadataUsageId;
extern "C"  AchievementDescription_t3110978151 * GcAchievementDescriptionData_ToAchievementDescription_m1135716620 (GcAchievementDescriptionData_t960725851 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcAchievementDescriptionData_ToAchievementDescription_m1135716620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	Texture2D_t3542995729 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	String_t* G_B2_4 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	Texture2D_t3542995729 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	Texture2D_t3542995729 * G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	String_t* G_B3_5 = NULL;
	{
		String_t* L_0 = __this->get_m_Identifier_0();
		String_t* L_1 = __this->get_m_Title_1();
		Texture2D_t3542995729 * L_2 = __this->get_m_Image_2();
		String_t* L_3 = __this->get_m_AchievedDescription_3();
		String_t* L_4 = __this->get_m_UnachievedDescription_4();
		int32_t L_5 = __this->get_m_Hidden_5();
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		G_B1_4 = L_0;
		if (L_5)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			G_B2_4 = L_0;
			goto IL_002f;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0030:
	{
		int32_t L_6 = __this->get_m_Points_6();
		AchievementDescription_t3110978151 * L_7 = (AchievementDescription_t3110978151 *)il2cpp_codegen_object_new(AchievementDescription_t3110978151_il2cpp_TypeInfo_var);
		AchievementDescription__ctor_m3827663715(L_7, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, (bool)G_B3_0, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  AchievementDescription_t3110978151 * GcAchievementDescriptionData_ToAchievementDescription_m1135716620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcAchievementDescriptionData_t960725851 * _thisAdjusted = reinterpret_cast<GcAchievementDescriptionData_t960725851 *>(__this + 1);
	return GcAchievementDescriptionData_ToAchievementDescription_m1135716620(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke(const GcAchievementDescriptionData_t960725851& unmarshaled, GcAchievementDescriptionData_t960725851_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke_back(const GcAchievementDescriptionData_t960725851_marshaled_pinvoke& marshaled, GcAchievementDescriptionData_t960725851& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t960725851_marshal_pinvoke_cleanup(GcAchievementDescriptionData_t960725851_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t960725851_marshal_com(const GcAchievementDescriptionData_t960725851& unmarshaled, GcAchievementDescriptionData_t960725851_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
extern "C" void GcAchievementDescriptionData_t960725851_marshal_com_back(const GcAchievementDescriptionData_t960725851_marshaled_com& marshaled, GcAchievementDescriptionData_t960725851& unmarshaled)
{
	Il2CppCodeGenException* ___m_Image_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Image' of type 'GcAchievementDescriptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Image_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern "C" void GcAchievementDescriptionData_t960725851_marshal_com_cleanup(GcAchievementDescriptionData_t960725851_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  void GcLeaderboard__ctor_m983739183 (GcLeaderboard_t453887929 * __this, Leaderboard_t4160680639 * ___board0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Leaderboard_t4160680639 * L_0 = ___board0;
		__this->set_m_GenericLeaderboard_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C"  void GcLeaderboard_Finalize_m827643570 (GcLeaderboard_t453887929 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m3243478693(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C"  bool GcLeaderboard_Contains_m3937847094 (GcLeaderboard_t453887929 * __this, Leaderboard_t4160680639 * ___board0, const MethodInfo* method)
{
	{
		Leaderboard_t4160680639 * L_0 = __this->get_m_GenericLeaderboard_1();
		Leaderboard_t4160680639 * L_1 = ___board0;
		return (bool)((((Il2CppObject*)(Leaderboard_t4160680639 *)L_0) == ((Il2CppObject*)(Leaderboard_t4160680639 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern Il2CppClass* ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var;
extern const uint32_t GcLeaderboard_SetScores_m695766380_MetadataUsageId;
extern "C"  void GcLeaderboard_SetScores_m695766380 (GcLeaderboard_t453887929 * __this, GcScoreDataU5BU5D_t4052399267* ___scoreDatas0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcLeaderboard_SetScores_m695766380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t299013381* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Leaderboard_t4160680639 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t4052399267* L_1 = ___scoreDatas0;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t299013381*)SZArrayNew(ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t299013381* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t4052399267* L_4 = ___scoreDatas0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t2307748940 * L_6 = GcScoreData_ToScore_m3744988639(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Score_t2307748940 *)L_6);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t4052399267* L_9 = ___scoreDatas0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t4160680639 * L_10 = __this->get_m_GenericLeaderboard_1();
		ScoreU5BU5D_t299013381* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m2544027503(L_10, (IScoreU5BU5D_t3237304636*)(IScoreU5BU5D_t3237304636*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C"  void GcLeaderboard_SetLocalScore_m1455692368 (GcLeaderboard_t453887929 * __this, GcScoreData_t3676783238  ___scoreData0, const MethodInfo* method)
{
	{
		Leaderboard_t4160680639 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t4160680639 * L_1 = __this->get_m_GenericLeaderboard_1();
		Score_t2307748940 * L_2 = GcScoreData_ToScore_m3744988639((&___scoreData0), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m1546635330(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C"  void GcLeaderboard_SetMaxRange_m124828187 (GcLeaderboard_t453887929 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		Leaderboard_t4160680639 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t4160680639 * L_1 = __this->get_m_GenericLeaderboard_1();
		uint32_t L_2 = ___maxRange0;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m255256830(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C"  void GcLeaderboard_SetTitle_m404553404 (GcLeaderboard_t453887929 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		Leaderboard_t4160680639 * L_0 = __this->get_m_GenericLeaderboard_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t4160680639 * L_1 = __this->get_m_GenericLeaderboard_1();
		String_t* L_2 = ___title0;
		NullCheck(L_1);
		Leaderboard_SetTitle_m4056985215(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void GcLeaderboard_Internal_LoadScores_m3771229037 (GcLeaderboard_t453887929 * __this, String_t* ___category0, int32_t ___from1, int32_t ___count2, int32_t ___playerScope3, int32_t ___timeScope4, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m3771229037_ftn) (GcLeaderboard_t453887929 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m3771229037_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m3771229037_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category0, ___from1, ___count2, ___playerScope3, ___timeScope4);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C"  void GcLeaderboard_Internal_LoadScoresWithUsers_m1836788974 (GcLeaderboard_t453887929 * __this, String_t* ___category0, int32_t ___timeScope1, StringU5BU5D_t1642385972* ___userIDs2, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m1836788974_ftn) (GcLeaderboard_t453887929 *, String_t*, int32_t, StringU5BU5D_t1642385972*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m1836788974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m1836788974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category0, ___timeScope1, ___userIDs2);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C"  bool GcLeaderboard_Loading_m1117879034 (GcLeaderboard_t453887929 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m1117879034_ftn) (GcLeaderboard_t453887929 *);
	static GcLeaderboard_Loading_m1117879034_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m1117879034_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C"  void GcLeaderboard_Dispose_m3243478693 (GcLeaderboard_t453887929 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m3243478693_ftn) (GcLeaderboard_t453887929 *);
	static GcLeaderboard_Dispose_m3243478693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m3243478693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke(const GcLeaderboard_t453887929& unmarshaled, GcLeaderboard_t453887929_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke_back(const GcLeaderboard_t453887929_marshaled_pinvoke& marshaled, GcLeaderboard_t453887929& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t453887929_marshal_pinvoke_cleanup(GcLeaderboard_t453887929_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t453887929_marshal_com(const GcLeaderboard_t453887929& unmarshaled, GcLeaderboard_t453887929_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
extern "C" void GcLeaderboard_t453887929_marshal_com_back(const GcLeaderboard_t453887929_marshaled_com& marshaled, GcLeaderboard_t453887929& unmarshaled)
{
	Il2CppCodeGenException* ___m_GenericLeaderboard_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_GenericLeaderboard' of type 'GcLeaderboard': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_GenericLeaderboard_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern "C" void GcLeaderboard_t453887929_marshal_com_cleanup(GcLeaderboard_t453887929_marshaled_com& marshaled)
{
}
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern Il2CppClass* Score_t2307748940_il2cpp_TypeInfo_var;
extern const uint32_t GcScoreData_ToScore_m3744988639_MetadataUsageId;
extern "C"  Score_t2307748940 * GcScoreData_ToScore_m3744988639 (GcScoreData_t3676783238 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcScoreData_ToScore_m3744988639_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t693205669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_m_Category_0();
		int32_t L_1 = __this->get_m_ValueHigh_2();
		int32_t L_2 = __this->get_m_ValueLow_1();
		String_t* L_3 = __this->get_m_PlayerID_5();
		DateTime__ctor_m2857738939((&V_0), ((int32_t)1970), 1, 1, 0, 0, 0, 0, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_Date_3();
		DateTime_t693205669  L_5 = DateTime_AddSeconds_m722082155((&V_0), (((double)((double)L_4))), /*hidden argument*/NULL);
		String_t* L_6 = __this->get_m_FormattedValue_4();
		int32_t L_7 = __this->get_m_Rank_6();
		Score_t2307748940 * L_8 = (Score_t2307748940 *)il2cpp_codegen_object_new(Score_t2307748940_il2cpp_TypeInfo_var);
		Score__ctor_m449446173(L_8, L_0, ((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((int64_t)L_1)))<<(int32_t)((int32_t)32)))+(int64_t)(((int64_t)((int64_t)L_2))))), L_3, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Score_t2307748940 * GcScoreData_ToScore_m3744988639_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcScoreData_t3676783238 * _thisAdjusted = reinterpret_cast<GcScoreData_t3676783238 *>(__this + 1);
	return GcScoreData_ToScore_m3744988639(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t3676783238_marshal_pinvoke(const GcScoreData_t3676783238& unmarshaled, GcScoreData_t3676783238_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_string(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_string(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_string(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t3676783238_marshal_pinvoke_back(const GcScoreData_t3676783238_marshaled_pinvoke& marshaled, GcScoreData_t3676783238& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_string_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_string_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_string_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t3676783238_marshal_pinvoke_cleanup(GcScoreData_t3676783238_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t3676783238_marshal_com(const GcScoreData_t3676783238& unmarshaled, GcScoreData_t3676783238_marshaled_com& marshaled)
{
	marshaled.___m_Category_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_Category_0());
	marshaled.___m_ValueLow_1 = unmarshaled.get_m_ValueLow_1();
	marshaled.___m_ValueHigh_2 = unmarshaled.get_m_ValueHigh_2();
	marshaled.___m_Date_3 = unmarshaled.get_m_Date_3();
	marshaled.___m_FormattedValue_4 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_FormattedValue_4());
	marshaled.___m_PlayerID_5 = il2cpp_codegen_marshal_bstring(unmarshaled.get_m_PlayerID_5());
	marshaled.___m_Rank_6 = unmarshaled.get_m_Rank_6();
}
extern "C" void GcScoreData_t3676783238_marshal_com_back(const GcScoreData_t3676783238_marshaled_com& marshaled, GcScoreData_t3676783238& unmarshaled)
{
	unmarshaled.set_m_Category_0(il2cpp_codegen_marshal_bstring_result(marshaled.___m_Category_0));
	int32_t unmarshaled_m_ValueLow_temp_1 = 0;
	unmarshaled_m_ValueLow_temp_1 = marshaled.___m_ValueLow_1;
	unmarshaled.set_m_ValueLow_1(unmarshaled_m_ValueLow_temp_1);
	int32_t unmarshaled_m_ValueHigh_temp_2 = 0;
	unmarshaled_m_ValueHigh_temp_2 = marshaled.___m_ValueHigh_2;
	unmarshaled.set_m_ValueHigh_2(unmarshaled_m_ValueHigh_temp_2);
	int32_t unmarshaled_m_Date_temp_3 = 0;
	unmarshaled_m_Date_temp_3 = marshaled.___m_Date_3;
	unmarshaled.set_m_Date_3(unmarshaled_m_Date_temp_3);
	unmarshaled.set_m_FormattedValue_4(il2cpp_codegen_marshal_bstring_result(marshaled.___m_FormattedValue_4));
	unmarshaled.set_m_PlayerID_5(il2cpp_codegen_marshal_bstring_result(marshaled.___m_PlayerID_5));
	int32_t unmarshaled_m_Rank_temp_6 = 0;
	unmarshaled_m_Rank_temp_6 = marshaled.___m_Rank_6;
	unmarshaled.set_m_Rank_6(unmarshaled_m_Rank_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern "C" void GcScoreData_t3676783238_marshal_com_cleanup(GcScoreData_t3676783238_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_Category_0);
	marshaled.___m_Category_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_FormattedValue_4);
	marshaled.___m_FormattedValue_4 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___m_PlayerID_5);
	marshaled.___m_PlayerID_5 = NULL;
}
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern Il2CppClass* UserProfile_t3365630962_il2cpp_TypeInfo_var;
extern const uint32_t GcUserProfileData_ToUserProfile_m1649282029_MetadataUsageId;
extern "C"  UserProfile_t3365630962 * GcUserProfileData_ToUserProfile_m1649282029 (GcUserProfileData_t3198293052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_ToUserProfile_m1649282029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = __this->get_userName_0();
		String_t* L_1 = __this->get_userID_1();
		int32_t L_2 = __this->get_isFriend_2();
		G_B1_0 = L_1;
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_001e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_001f:
	{
		Texture2D_t3542995729 * L_3 = __this->get_image_3();
		UserProfile_t3365630962 * L_4 = (UserProfile_t3365630962 *)il2cpp_codegen_object_new(UserProfile_t3365630962_il2cpp_TypeInfo_var);
		UserProfile__ctor_m4176886497(L_4, G_B3_2, G_B3_1, (bool)G_B3_0, 3, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  UserProfile_t3365630962 * GcUserProfileData_ToUserProfile_m1649282029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	GcUserProfileData_t3198293052 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t3198293052 *>(__this + 1);
	return GcUserProfileData_ToUserProfile_m1649282029(_thisAdjusted, method);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral16750462;
extern const uint32_t GcUserProfileData_AddToArray_m2451723029_MetadataUsageId;
extern "C"  void GcUserProfileData_AddToArray_m2451723029 (GcUserProfileData_t3198293052 * __this, UserProfileU5BU5D_t2930725895** ___array0, int32_t ___number1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GcUserProfileData_AddToArray_m2451723029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t2930725895** L_0 = ___array0;
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_0)));
		int32_t L_1 = ___number1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)(*((UserProfileU5BU5D_t2930725895**)L_0)))->max_length))))) <= ((int32_t)L_1)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = ___number1;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		UserProfileU5BU5D_t2930725895** L_3 = ___array0;
		int32_t L_4 = ___number1;
		UserProfile_t3365630962 * L_5 = GcUserProfileData_ToUserProfile_m1649282029(__this, /*hidden argument*/NULL);
		NullCheck((*((UserProfileU5BU5D_t2930725895**)L_3)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t2930725895**)L_3)), L_4);
		ArrayElementTypeCheck ((*((UserProfileU5BU5D_t2930725895**)L_3)), L_5);
		((*((UserProfileU5BU5D_t2930725895**)L_3)))->SetAt(static_cast<il2cpp_array_size_t>(L_4), (UserProfile_t3365630962 *)L_5);
		goto IL_002a;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral16750462, /*hidden argument*/NULL);
	}

IL_002a:
	{
		return;
	}
}
extern "C"  void GcUserProfileData_AddToArray_m2451723029_AdjustorThunk (Il2CppObject * __this, UserProfileU5BU5D_t2930725895** ___array0, int32_t ___number1, const MethodInfo* method)
{
	GcUserProfileData_t3198293052 * _thisAdjusted = reinterpret_cast<GcUserProfileData_t3198293052 *>(__this + 1);
	GcUserProfileData_AddToArray_m2451723029(_thisAdjusted, ___array0, ___number1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke(const GcUserProfileData_t3198293052& unmarshaled, GcUserProfileData_t3198293052_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke_back(const GcUserProfileData_t3198293052_marshaled_pinvoke& marshaled, GcUserProfileData_t3198293052& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t3198293052_marshal_pinvoke_cleanup(GcUserProfileData_t3198293052_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t3198293052_marshal_com(const GcUserProfileData_t3198293052& unmarshaled, GcUserProfileData_t3198293052_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
extern "C" void GcUserProfileData_t3198293052_marshal_com_back(const GcUserProfileData_t3198293052_marshaled_com& marshaled, GcUserProfileData_t3198293052& unmarshaled)
{
	Il2CppCodeGenException* ___image_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'image' of type 'GcUserProfileData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___image_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern "C" void GcUserProfileData_t3198293052_marshal_com_cleanup(GcUserProfileData_t3198293052_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern "C"  void Achievement__ctor_m4089961863 (Achievement_t1333316625 * __this, String_t* ___id0, double ___percentCompleted1, bool ___completed2, bool ___hidden3, DateTime_t693205669  ___lastReportedDate4, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m1964322655(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percentCompleted1;
		Achievement_set_percentCompleted_m3787753314(__this, L_1, /*hidden argument*/NULL);
		bool L_2 = ___completed2;
		__this->set_m_Completed_0(L_2);
		bool L_3 = ___hidden3;
		__this->set_m_Hidden_1(L_3);
		DateTime_t693205669  L_4 = ___lastReportedDate4;
		__this->set_m_LastReportedDate_2(L_4);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t Achievement__ctor_m622897477_MetadataUsageId;
extern "C"  void Achievement__ctor_m622897477 (Achievement_t1333316625 * __this, String_t* ___id0, double ___percent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m622897477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		Achievement_set_id_m1964322655(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ___percent1;
		Achievement_set_percentCompleted_m3787753314(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_Hidden_1((bool)0);
		__this->set_m_Completed_0((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_2 = ((DateTime_t693205669_StaticFields*)DateTime_t693205669_il2cpp_TypeInfo_var->static_fields)->get_MinValue_3();
		__this->set_m_LastReportedDate_2(L_2);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern Il2CppCodeGenString* _stringLiteral2845190196;
extern const uint32_t Achievement__ctor_m3960800585_MetadataUsageId;
extern "C"  void Achievement__ctor_m3960800585 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement__ctor_m3960800585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Achievement__ctor_m622897477(__this, _stringLiteral2845190196, (0.0), /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1220271277;
extern const uint32_t Achievement_ToString_m4141703698_MetadataUsageId;
extern "C"  String_t* Achievement_ToString_m4141703698 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Achievement_ToString_m4141703698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9)));
		String_t* L_1 = Achievement_get_id_m3268291386(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1220271277);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		double L_4 = Achievement_get_percentCompleted_m3485898759(__this, /*hidden argument*/NULL);
		double L_5 = L_4;
		Il2CppObject * L_6 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_6);
		ObjectU5BU5D_t3614634134* L_7 = L_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, _stringLiteral1220271277);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_8 = L_7;
		bool L_9 = Achievement_get_completed_m2210587717(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Il2CppObject * L_11 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, _stringLiteral1220271277);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_13 = L_12;
		bool L_14 = Achievement_get_hidden_m2383026878(__this, /*hidden argument*/NULL);
		bool L_15 = L_14;
		Il2CppObject * L_16 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, _stringLiteral1220271277);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_18 = L_17;
		DateTime_t693205669  L_19 = Achievement_get_lastReportedDate_m4232356476(__this, /*hidden argument*/NULL);
		DateTime_t693205669  L_20 = L_19;
		Il2CppObject * L_21 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 8);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m3881798623(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern "C"  String_t* Achievement_get_id_m3268291386 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern "C"  void Achievement_set_id_m1964322655 (Achievement_t1333316625 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern "C"  double Achievement_get_percentCompleted_m3485898759 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_U3CpercentCompletedU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern "C"  void Achievement_set_percentCompleted_m3787753314 (Achievement_t1333316625 * __this, double ___value0, const MethodInfo* method)
{
	{
		double L_0 = ___value0;
		__this->set_U3CpercentCompletedU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern "C"  bool Achievement_get_completed_m2210587717 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Completed_0();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern "C"  bool Achievement_get_hidden_m2383026878 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_1();
		return L_0;
	}
}
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern "C"  DateTime_t693205669  Achievement_get_lastReportedDate_m4232356476 (Achievement_t1333316625 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = __this->get_m_LastReportedDate_2();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern "C"  void AchievementDescription__ctor_m3827663715 (AchievementDescription_t3110978151 * __this, String_t* ___id0, String_t* ___title1, Texture2D_t3542995729 * ___image2, String_t* ___achievedDescription3, String_t* ___unachievedDescription4, bool ___hidden5, int32_t ___points6, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___id0;
		AchievementDescription_set_id_m1767957413(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___title1;
		__this->set_m_Title_0(L_1);
		Texture2D_t3542995729 * L_2 = ___image2;
		__this->set_m_Image_1(L_2);
		String_t* L_3 = ___achievedDescription3;
		__this->set_m_AchievedDescription_2(L_3);
		String_t* L_4 = ___unachievedDescription4;
		__this->set_m_UnachievedDescription_3(L_4);
		bool L_5 = ___hidden5;
		__this->set_m_Hidden_4(L_5);
		int32_t L_6 = ___points6;
		__this->set_m_Points_5(L_6);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1220271277;
extern const uint32_t AchievementDescription_ToString_m1223743222_MetadataUsageId;
extern "C"  String_t* AchievementDescription_ToString_m1223743222 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AchievementDescription_ToString_m1223743222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)11)));
		String_t* L_1 = AchievementDescription_get_id_m4139130038(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1220271277);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = AchievementDescription_get_title_m2964721751(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral1220271277);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		String_t* L_7 = AchievementDescription_get_achievedDescription_m1849956442(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		ArrayElementTypeCheck (L_8, _stringLiteral1220271277);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_9 = L_8;
		String_t* L_10 = AchievementDescription_get_unachievedDescription_m697687775(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 6);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = L_9;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 7);
		ArrayElementTypeCheck (L_11, _stringLiteral1220271277);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_12 = L_11;
		int32_t L_13 = AchievementDescription_get_points_m508260507(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 8);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)9));
		ArrayElementTypeCheck (L_16, _stringLiteral1220271277);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_17 = L_16;
		bool L_18 = AchievementDescription_get_hidden_m2326761336(__this, /*hidden argument*/NULL);
		bool L_19 = L_18;
		Il2CppObject * L_20 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)10));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3881798623(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern "C"  void AchievementDescription_SetImage_m1395221782 (AchievementDescription_t3110978151 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = ___image0;
		__this->set_m_Image_1(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern "C"  String_t* AchievementDescription_get_id_m4139130038 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern "C"  void AchievementDescription_set_id_m1767957413 (AchievementDescription_t3110978151 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern "C"  String_t* AchievementDescription_get_title_m2964721751 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Title_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern "C"  String_t* AchievementDescription_get_achievedDescription_m1849956442 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_AchievedDescription_2();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern "C"  String_t* AchievementDescription_get_unachievedDescription_m697687775 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UnachievedDescription_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern "C"  bool AchievementDescription_get_hidden_m2326761336 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Hidden_4();
		return L_0;
	}
}
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern "C"  int32_t AchievementDescription_get_points_m508260507 (AchievementDescription_t3110978151 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Points_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern Il2CppClass* Score_t2307748940_il2cpp_TypeInfo_var;
extern Il2CppClass* ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2335436577;
extern const uint32_t Leaderboard__ctor_m1521627019_MetadataUsageId;
extern "C"  void Leaderboard__ctor_m1521627019 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard__ctor_m1521627019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Leaderboard_set_id_m3042129093(__this, _stringLiteral2335436577, /*hidden argument*/NULL);
		Range_t3455291607  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Range__ctor_m854749803(&L_0, 1, ((int32_t)10), /*hidden argument*/NULL);
		Leaderboard_set_range_m2444071236(__this, L_0, /*hidden argument*/NULL);
		Leaderboard_set_userScope_m4289859012(__this, 0, /*hidden argument*/NULL);
		Leaderboard_set_timeScope_m1932011396(__this, 2, /*hidden argument*/NULL);
		__this->set_m_Loading_0((bool)0);
		Score_t2307748940 * L_1 = (Score_t2307748940 *)il2cpp_codegen_object_new(Score_t2307748940_il2cpp_TypeInfo_var);
		Score__ctor_m4088571740(L_1, _stringLiteral2335436577, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		__this->set_m_LocalUserScore_1(L_1);
		__this->set_m_MaxRange_2(0);
		__this->set_m_Scores_3((IScoreU5BU5D_t3237304636*)((ScoreU5BU5D_t299013381*)SZArrayNew(ScoreU5BU5D_t299013381_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Title_4(_stringLiteral2335436577);
		__this->set_m_UserIDs_5(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* UserScope_t3775842435_il2cpp_TypeInfo_var;
extern Il2CppClass* TimeScope_t2583939667_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4162965558;
extern Il2CppCodeGenString* _stringLiteral1655410072;
extern Il2CppCodeGenString* _stringLiteral3068384690;
extern Il2CppCodeGenString* _stringLiteral2001796069;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral148682439;
extern Il2CppCodeGenString* _stringLiteral2252533417;
extern Il2CppCodeGenString* _stringLiteral560844573;
extern Il2CppCodeGenString* _stringLiteral2354113925;
extern Il2CppCodeGenString* _stringLiteral2060676439;
extern const uint32_t Leaderboard_ToString_m1566921590_MetadataUsageId;
extern "C"  String_t* Leaderboard_ToString_m1566921590 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Leaderboard_ToString_m1566921590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Range_t3455291607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Range_t3455291607  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral4162965558);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4162965558);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		String_t* L_2 = Leaderboard_get_id_m3631555582(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral1655410072);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1655410072);
		ObjectU5BU5D_t3614634134* L_4 = L_3;
		String_t* L_5 = __this->get_m_Title_4();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral3068384690);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral3068384690);
		ObjectU5BU5D_t3614634134* L_7 = L_6;
		bool L_8 = __this->get_m_Loading_0();
		bool L_9 = L_8;
		Il2CppObject * L_10 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 5);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_10);
		ObjectU5BU5D_t3614634134* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, _stringLiteral2001796069);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral2001796069);
		ObjectU5BU5D_t3614634134* L_12 = L_11;
		Range_t3455291607  L_13 = Leaderboard_get_range_m636637371(__this, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (&V_0)->get_from_0();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_16);
		ObjectU5BU5D_t3614634134* L_17 = L_12;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral372029314);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral372029314);
		ObjectU5BU5D_t3614634134* L_18 = L_17;
		Range_t3455291607  L_19 = Leaderboard_get_range_m636637371(__this, /*hidden argument*/NULL);
		V_1 = L_19;
		int32_t L_20 = (&V_1)->get_count_1();
		int32_t L_21 = L_20;
		Il2CppObject * L_22 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_22);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_22);
		ObjectU5BU5D_t3614634134* L_23 = L_18;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)10));
		ArrayElementTypeCheck (L_23, _stringLiteral148682439);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)_stringLiteral148682439);
		ObjectU5BU5D_t3614634134* L_24 = L_23;
		uint32_t L_25 = __this->get_m_MaxRange_2();
		uint32_t L_26 = L_25;
		Il2CppObject * L_27 = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)11));
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_24;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)12));
		ArrayElementTypeCheck (L_28, _stringLiteral2252533417);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)_stringLiteral2252533417);
		ObjectU5BU5D_t3614634134* L_29 = L_28;
		IScoreU5BU5D_t3237304636* L_30 = __this->get_m_Scores_3();
		NullCheck(L_30);
		int32_t L_31 = (((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))));
		Il2CppObject * L_32 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)13));
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_29;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)14));
		ArrayElementTypeCheck (L_33, _stringLiteral560844573);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)_stringLiteral560844573);
		ObjectU5BU5D_t3614634134* L_34 = L_33;
		int32_t L_35 = Leaderboard_get_userScope_m2481440059(__this, /*hidden argument*/NULL);
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(UserScope_t3775842435_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)15));
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_37);
		ObjectU5BU5D_t3614634134* L_38 = L_34;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)16));
		ArrayElementTypeCheck (L_38, _stringLiteral2354113925);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (Il2CppObject *)_stringLiteral2354113925);
		ObjectU5BU5D_t3614634134* L_39 = L_38;
		int32_t L_40 = Leaderboard_get_timeScope_m2249641019(__this, /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		Il2CppObject * L_42 = Box(TimeScope_t2583939667_il2cpp_TypeInfo_var, &L_41);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)17));
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (Il2CppObject *)L_42);
		ObjectU5BU5D_t3614634134* L_43 = L_39;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, ((int32_t)18));
		ArrayElementTypeCheck (L_43, _stringLiteral2060676439);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (Il2CppObject *)_stringLiteral2060676439);
		ObjectU5BU5D_t3614634134* L_44 = L_43;
		StringU5BU5D_t1642385972* L_45 = __this->get_m_UserIDs_5();
		NullCheck(L_45);
		int32_t L_46 = (((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length))));
		Il2CppObject * L_47 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)19));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (Il2CppObject *)L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m3881798623(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return L_48;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern "C"  void Leaderboard_SetLocalUserScore_m1546635330 (Leaderboard_t4160680639 * __this, Il2CppObject * ___score0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___score0;
		__this->set_m_LocalUserScore_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern "C"  void Leaderboard_SetMaxRange_m255256830 (Leaderboard_t4160680639 * __this, uint32_t ___maxRange0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___maxRange0;
		__this->set_m_MaxRange_2(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern "C"  void Leaderboard_SetScores_m2544027503 (Leaderboard_t4160680639 * __this, IScoreU5BU5D_t3237304636* ___scores0, const MethodInfo* method)
{
	{
		IScoreU5BU5D_t3237304636* L_0 = ___scores0;
		__this->set_m_Scores_3(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern "C"  void Leaderboard_SetTitle_m4056985215 (Leaderboard_t4160680639 * __this, String_t* ___title0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title0;
		__this->set_m_Title_4(L_0);
		return;
	}
}
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern "C"  StringU5BU5D_t1642385972* Leaderboard_GetUserFilter_m4114287667 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	{
		StringU5BU5D_t1642385972* L_0 = __this->get_m_UserIDs_5();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern "C"  String_t* Leaderboard_get_id_m3631555582 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CidU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern "C"  void Leaderboard_set_id_m3042129093 (Leaderboard_t4160680639 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CidU3Ek__BackingField_6(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern "C"  int32_t Leaderboard_get_userScope_m2481440059 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CuserScopeU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern "C"  void Leaderboard_set_userScope_m4289859012 (Leaderboard_t4160680639 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CuserScopeU3Ek__BackingField_7(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern "C"  Range_t3455291607  Leaderboard_get_range_m636637371 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	{
		Range_t3455291607  L_0 = __this->get_U3CrangeU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern "C"  void Leaderboard_set_range_m2444071236 (Leaderboard_t4160680639 * __this, Range_t3455291607  ___value0, const MethodInfo* method)
{
	{
		Range_t3455291607  L_0 = ___value0;
		__this->set_U3CrangeU3Ek__BackingField_8(L_0);
		return;
	}
}
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern "C"  int32_t Leaderboard_get_timeScope_m2249641019 (Leaderboard_t4160680639 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CtimeScopeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern "C"  void Leaderboard_set_timeScope_m1932011396 (Leaderboard_t4160680639 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CtimeScopeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern Il2CppClass* UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var;
extern const uint32_t LocalUser__ctor_m456101162_MetadataUsageId;
extern "C"  void LocalUser__ctor_m456101162 (LocalUser_t3019851150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LocalUser__ctor_m456101162_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfile__ctor_m1897167318(__this, /*hidden argument*/NULL);
		__this->set_m_Friends_5((IUserProfileU5BU5D_t3461248430*)((UserProfileU5BU5D_t2930725895*)SZArrayNew(UserProfileU5BU5D_t2930725895_il2cpp_TypeInfo_var, (uint32_t)0)));
		__this->set_m_Authenticated_6((bool)0);
		__this->set_m_Underage_7((bool)0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern "C"  void LocalUser_SetFriends_m3706685636 (LocalUser_t3019851150 * __this, IUserProfileU5BU5D_t3461248430* ___friends0, const MethodInfo* method)
{
	{
		IUserProfileU5BU5D_t3461248430* L_0 = ___friends0;
		__this->set_m_Friends_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern "C"  void LocalUser_SetAuthenticated_m3483845210 (LocalUser_t3019851150 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Authenticated_6(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern "C"  void LocalUser_SetUnderage_m3689639158 (LocalUser_t3019851150 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Underage_7(L_0);
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern "C"  bool LocalUser_get_authenticated_m1586290046 (LocalUser_t3019851150 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Authenticated_6();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern const uint32_t Score__ctor_m4088571740_MetadataUsageId;
extern "C"  void Score__ctor_m4088571740 (Score_t2307748940 * __this, String_t* ___leaderboardID0, int64_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score__ctor_m4088571740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID0;
		int64_t L_1 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_2 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		Score__ctor_m449446173(__this, L_0, L_1, _stringLiteral372029326, L_2, L_3, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern "C"  void Score__ctor_m449446173 (Score_t2307748940 * __this, String_t* ___leaderboardID0, int64_t ___value1, String_t* ___userID2, DateTime_t693205669  ___date3, String_t* ___formattedValue4, int32_t ___rank5, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___leaderboardID0;
		Score_set_leaderboardID_m2847392905(__this, L_0, /*hidden argument*/NULL);
		int64_t L_1 = ___value1;
		Score_set_value_m3584530198(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___userID2;
		__this->set_m_UserID_2(L_2);
		DateTime_t693205669  L_3 = ___date3;
		__this->set_m_Date_0(L_3);
		String_t* L_4 = ___formattedValue4;
		__this->set_m_FormattedValue_1(L_4);
		int32_t L_5 = ___rank5;
		__this->set_m_Rank_3(L_5);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1969136623;
extern Il2CppCodeGenString* _stringLiteral2088420153;
extern Il2CppCodeGenString* _stringLiteral705785168;
extern Il2CppCodeGenString* _stringLiteral3705328384;
extern Il2CppCodeGenString* _stringLiteral256488244;
extern const uint32_t Score_ToString_m2361881169_MetadataUsageId;
extern "C"  String_t* Score_ToString_m2361881169 (Score_t2307748940 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Score_ToString_m2361881169_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral1969136623);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1969136623);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		int32_t L_2 = __this->get_m_Rank_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral2088420153);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral2088420153);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		int64_t L_7 = Score_get_value_m1883227153(__this, /*hidden argument*/NULL);
		int64_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral705785168);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral705785168);
		ObjectU5BU5D_t3614634134* L_11 = L_10;
		String_t* L_12 = Score_get_leaderboardID_m2566602022(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = L_11;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral3705328384);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3705328384);
		ObjectU5BU5D_t3614634134* L_14 = L_13;
		String_t* L_15 = __this->get_m_UserID_2();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		ArrayElementTypeCheck (L_16, _stringLiteral256488244);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)_stringLiteral256488244);
		ObjectU5BU5D_t3614634134* L_17 = L_16;
		DateTime_t693205669  L_18 = __this->get_m_Date_0();
		DateTime_t693205669  L_19 = L_18;
		Il2CppObject * L_20 = Box(DateTime_t693205669_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)9));
		ArrayElementTypeCheck (L_17, L_20);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3881798623(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern "C"  String_t* Score_get_leaderboardID_m2566602022 (Score_t2307748940 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CleaderboardIDU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern "C"  void Score_set_leaderboardID_m2847392905 (Score_t2307748940 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CleaderboardIDU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern "C"  int64_t Score_get_value_m1883227153 (Score_t2307748940 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_U3CvalueU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern "C"  void Score_set_value_m3584530198 (Score_t2307748940 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_U3CvalueU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3771700375;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern const uint32_t UserProfile__ctor_m1897167318_MetadataUsageId;
extern "C"  void UserProfile__ctor_m1897167318 (UserProfile_t3365630962 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile__ctor_m1897167318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_m_UserName_0(_stringLiteral3771700375);
		__this->set_m_ID_1(_stringLiteral372029326);
		__this->set_m_IsFriend_2((bool)0);
		__this->set_m_State_3(3);
		Texture2D_t3542995729 * L_0 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_0, ((int32_t)32), ((int32_t)32), /*hidden argument*/NULL);
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern "C"  void UserProfile__ctor_m4176886497 (UserProfile_t3365630962 * __this, String_t* ___name0, String_t* ___id1, bool ___friend2, int32_t ___state3, Texture2D_t3542995729 * ___image4, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		String_t* L_1 = ___id1;
		__this->set_m_ID_1(L_1);
		bool L_2 = ___friend2;
		__this->set_m_IsFriend_2(L_2);
		int32_t L_3 = ___state3;
		__this->set_m_State_3(L_3);
		Texture2D_t3542995729 * L_4 = ___image4;
		__this->set_m_Image_4(L_4);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* UserState_t455716270_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1220271277;
extern const uint32_t UserProfile_ToString_m3584230475_MetadataUsageId;
extern "C"  String_t* UserProfile_ToString_m3584230475 (UserProfile_t3365630962 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_ToString_m3584230475_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)7));
		String_t* L_1 = UserProfile_get_id_m1121636229(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_1);
		ObjectU5BU5D_t3614634134* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral1220271277);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_3 = L_2;
		String_t* L_4 = UserProfile_get_userName_m2334139476(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_4);
		ObjectU5BU5D_t3614634134* L_5 = L_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, _stringLiteral1220271277);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_6 = L_5;
		bool L_7 = UserProfile_get_isFriend_m1824376057(__this, /*hidden argument*/NULL);
		bool L_8 = L_7;
		Il2CppObject * L_9 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, _stringLiteral1220271277);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)_stringLiteral1220271277);
		ObjectU5BU5D_t3614634134* L_11 = L_10;
		int32_t L_12 = UserProfile_get_state_m2818183789(__this, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(UserState_t455716270_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 6);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_14);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3881798623(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern "C"  void UserProfile_SetUserName_m3667428096 (UserProfile_t3365630962 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		__this->set_m_UserName_0(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern "C"  void UserProfile_SetUserID_m3818116510 (UserProfile_t3365630962 * __this, String_t* ___id0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___id0;
		__this->set_m_ID_1(L_0);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern "C"  void UserProfile_SetImage_m3142478163 (UserProfile_t3365630962 * __this, Texture2D_t3542995729 * ___image0, const MethodInfo* method)
{
	{
		Texture2D_t3542995729 * L_0 = ___image0;
		__this->set_m_Image_4(L_0);
		return;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern "C"  String_t* UserProfile_get_userName_m2334139476 (UserProfile_t3365630962 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_UserName_0();
		return L_0;
	}
}
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern "C"  String_t* UserProfile_get_id_m1121636229 (UserProfile_t3365630962 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_ID_1();
		return L_0;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern "C"  bool UserProfile_get_isFriend_m1824376057 (UserProfile_t3365630962 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_IsFriend_2();
		return L_0;
	}
}
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern "C"  int32_t UserProfile_get_state_m2818183789 (UserProfile_t3365630962 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_State_3();
		return L_0;
	}
}
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern "C"  void Range__ctor_m854749803 (Range_t3455291607 * __this, int32_t ___fromValue0, int32_t ___valueCount1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___fromValue0;
		__this->set_from_0(L_0);
		int32_t L_1 = ___valueCount1;
		__this->set_count_1(L_1);
		return;
	}
}
extern "C"  void Range__ctor_m854749803_AdjustorThunk (Il2CppObject * __this, int32_t ___fromValue0, int32_t ___valueCount1, const MethodInfo* method)
{
	Range_t3455291607 * _thisAdjusted = reinterpret_cast<Range_t3455291607 *>(__this + 1);
	Range__ctor_m854749803(_thisAdjusted, ___fromValue0, ___valueCount1, method);
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t3455291607_marshal_pinvoke(const Range_t3455291607& unmarshaled, Range_t3455291607_marshaled_pinvoke& marshaled)
{
	marshaled.___from_0 = unmarshaled.get_from_0();
	marshaled.___count_1 = unmarshaled.get_count_1();
}
extern "C" void Range_t3455291607_marshal_pinvoke_back(const Range_t3455291607_marshaled_pinvoke& marshaled, Range_t3455291607& unmarshaled)
{
	int32_t unmarshaled_from_temp_0 = 0;
	unmarshaled_from_temp_0 = marshaled.___from_0;
	unmarshaled.set_from_0(unmarshaled_from_temp_0);
	int32_t unmarshaled_count_temp_1 = 0;
	unmarshaled_count_temp_1 = marshaled.___count_1;
	unmarshaled.set_count_1(unmarshaled_count_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t3455291607_marshal_pinvoke_cleanup(Range_t3455291607_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t3455291607_marshal_com(const Range_t3455291607& unmarshaled, Range_t3455291607_marshaled_com& marshaled)
{
	marshaled.___from_0 = unmarshaled.get_from_0();
	marshaled.___count_1 = unmarshaled.get_count_1();
}
extern "C" void Range_t3455291607_marshal_com_back(const Range_t3455291607_marshaled_com& marshaled, Range_t3455291607& unmarshaled)
{
	int32_t unmarshaled_from_temp_0 = 0;
	unmarshaled_from_temp_0 = marshaled.___from_0;
	unmarshaled.set_from_0(unmarshaled_from_temp_0);
	int32_t unmarshaled_count_temp_1 = 0;
	unmarshaled_count_temp_1 = marshaled.___count_1;
	unmarshaled.set_count_1(unmarshaled_count_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.SocialPlatforms.Range
extern "C" void Range_t3455291607_marshal_com_cleanup(Range_t3455291607_marshaled_com& marshaled)
{
}
// System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern "C"  int32_t SortingLayer_GetLayerValueFromID_m4029056996 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method)
{
	typedef int32_t (*SortingLayer_GetLayerValueFromID_m4029056996_ftn) (int32_t);
	static SortingLayer_GetLayerValueFromID_m4029056996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SortingLayer_GetLayerValueFromID_m4029056996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)");
	return _il2cpp_icall_func(___id0);
}
// Conversion methods for marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t221838959_marshal_pinvoke(const SortingLayer_t221838959& unmarshaled, SortingLayer_t221838959_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Id_0 = unmarshaled.get_m_Id_0();
}
extern "C" void SortingLayer_t221838959_marshal_pinvoke_back(const SortingLayer_t221838959_marshaled_pinvoke& marshaled, SortingLayer_t221838959& unmarshaled)
{
	int32_t unmarshaled_m_Id_temp_0 = 0;
	unmarshaled_m_Id_temp_0 = marshaled.___m_Id_0;
	unmarshaled.set_m_Id_0(unmarshaled_m_Id_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t221838959_marshal_pinvoke_cleanup(SortingLayer_t221838959_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t221838959_marshal_com(const SortingLayer_t221838959& unmarshaled, SortingLayer_t221838959_marshaled_com& marshaled)
{
	marshaled.___m_Id_0 = unmarshaled.get_m_Id_0();
}
extern "C" void SortingLayer_t221838959_marshal_com_back(const SortingLayer_t221838959_marshaled_com& marshaled, SortingLayer_t221838959& unmarshaled)
{
	int32_t unmarshaled_m_Id_temp_0 = 0;
	unmarshaled_m_Id_temp_0 = marshaled.___m_Id_0;
	unmarshaled.set_m_Id_0(unmarshaled_m_Id_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SortingLayer
extern "C" void SortingLayer_t221838959_marshal_com_cleanup(SortingLayer_t221838959_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.SpaceAttribute::.ctor()
extern "C"  void SpaceAttribute__ctor_m187290553 (SpaceAttribute_t952253354 * __this, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		__this->set_height_0((8.0f));
		return;
	}
}
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern "C"  void SpaceAttribute__ctor_m1444406696 (SpaceAttribute_t952253354 * __this, float ___height0, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		float L_0 = ___height0;
		__this->set_height_0(L_0);
		return;
	}
}
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t3681755626  Sprite_get_rect_m4043737881 (Sprite_t309593783 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_INTERNAL_get_rect_m2819780324(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_rect_m2819780324 (Sprite_t309593783 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m2819780324_ftn) (Sprite_t309593783 *, Rect_t3681755626 *);
	static Sprite_INTERNAL_get_rect_m2819780324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m2819780324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C"  float Sprite_get_pixelsPerUnit_m1785791149 (Sprite_t309593783 * __this, const MethodInfo* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m1785791149_ftn) (Sprite_t309593783 *);
	static Sprite_get_pixelsPerUnit_m1785791149_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m1785791149_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t3542995729 * Sprite_get_texture_m2733552707 (Sprite_t309593783 * __this, const MethodInfo* method)
{
	typedef Texture2D_t3542995729 * (*Sprite_get_texture_m2733552707_ftn) (Sprite_t309593783 *);
	static Sprite_get_texture_m2733552707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m2733552707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern "C"  Texture2D_t3542995729 * Sprite_get_associatedAlphaSplitTexture_m3617241169 (Sprite_t309593783 * __this, const MethodInfo* method)
{
	typedef Texture2D_t3542995729 * (*Sprite_get_associatedAlphaSplitTexture_m3617241169_ftn) (Sprite_t309593783 *);
	static Sprite_get_associatedAlphaSplitTexture_m3617241169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_associatedAlphaSplitTexture_m3617241169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_associatedAlphaSplitTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t3681755626  Sprite_get_textureRect_m330371354 (Sprite_t309593783 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_INTERNAL_get_textureRect_m4189537307(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C"  void Sprite_INTERNAL_get_textureRect_m4189537307 (Sprite_t309593783 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m4189537307_ftn) (Sprite_t309593783 *, Rect_t3681755626 *);
	static Sprite_INTERNAL_get_textureRect_m4189537307_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m4189537307_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t2243707581  Sprite_get_border_m3513048554 (Sprite_t309593783 * __this, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_INTERNAL_get_border_m3676056217(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t2243707581  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C"  void Sprite_INTERNAL_get_border_m3676056217 (Sprite_t309593783 * __this, Vector4_t2243707581 * ___value0, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m3676056217_ftn) (Sprite_t309593783 *, Vector4_t2243707581 *);
	static Sprite_INTERNAL_get_border_m3676056217_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m3676056217_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern "C"  Color_t2020392075  SpriteRenderer_get_color_m345525162 (SpriteRenderer_t1209076198 * __this, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SpriteRenderer_INTERNAL_get_color_m4174522917(__this, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m2339931967 (SpriteRenderer_t1209076198 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		SpriteRenderer_INTERNAL_set_color_m637715817(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SpriteRenderer::INTERNAL_get_color(UnityEngine.Color&)
extern "C"  void SpriteRenderer_INTERNAL_get_color_m4174522917 (SpriteRenderer_t1209076198 * __this, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*SpriteRenderer_INTERNAL_get_color_m4174522917_ftn) (SpriteRenderer_t1209076198 *, Color_t2020392075 *);
	static SpriteRenderer_INTERNAL_get_color_m4174522917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpriteRenderer_INTERNAL_get_color_m4174522917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SpriteRenderer::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)
extern "C"  void SpriteRenderer_INTERNAL_set_color_m637715817 (SpriteRenderer_t1209076198 * __this, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*SpriteRenderer_INTERNAL_set_color_m637715817_ftn) (SpriteRenderer_t1209076198 *, Color_t2020392075 *);
	static SpriteRenderer_INTERNAL_set_color_m637715817_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpriteRenderer_INTERNAL_set_color_m637715817_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SpriteRenderer::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C"  Vector4_t2243707581  DataUtility_GetInnerUV_m1397313633 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t309593783 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetInnerUV_m3931988417(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t2243707581  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetInnerUV_m3931988417 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, Vector4_t2243707581 * ___value1, const MethodInfo* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetInnerUV_m3931988417_ftn) (Sprite_t309593783 *, Vector4_t2243707581 *);
	static DataUtility_INTERNAL_CALL_GetInnerUV_m3931988417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetInnerUV_m3931988417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C"  Vector4_t2243707581  DataUtility_GetOuterUV_m3416735524 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t309593783 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetOuterUV_m121019172(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t2243707581  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetOuterUV_m121019172 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, Vector4_t2243707581 * ___value1, const MethodInfo* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetOuterUV_m121019172_ftn) (Sprite_t309593783 *, Vector4_t2243707581 *);
	static DataUtility_INTERNAL_CALL_GetOuterUV_m121019172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetOuterUV_m121019172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C"  Vector4_t2243707581  DataUtility_GetPadding_m2260705841 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t309593783 * L_0 = ___sprite0;
		DataUtility_INTERNAL_CALL_GetPadding_m1621366113(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector4_t2243707581  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
extern "C"  void DataUtility_INTERNAL_CALL_GetPadding_m1621366113 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, Vector4_t2243707581 * ___value1, const MethodInfo* method)
{
	typedef void (*DataUtility_INTERNAL_CALL_GetPadding_m1621366113_ftn) (Sprite_t309593783 *, Vector4_t2243707581 *);
	static DataUtility_INTERNAL_CALL_GetPadding_m1621366113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_INTERNAL_CALL_GetPadding_m1621366113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___sprite0, ___value1);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C"  Vector2_t2243707579  DataUtility_GetMinSize_m1209791201 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Sprite_t309593783 * L_0 = ___sprite0;
		DataUtility_Internal_GetMinSize_m2711257990(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C"  void DataUtility_Internal_GetMinSize_m2711257990 (Il2CppObject * __this /* static, unused */, Sprite_t309593783 * ___sprite0, Vector2_t2243707579 * ___output1, const MethodInfo* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m2711257990_ftn) (Sprite_t309593783 *, Vector2_t2243707579 *);
	static DataUtility_Internal_GetMinSize_m2711257990_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m2711257990_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite0, ___output1);
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t1881293839_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility__cctor_m1132099289_MetadataUsageId;
extern "C"  void StackTraceUtility__cctor_m1132099289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility__cctor_m1132099289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		((StackTraceUtility_t1881293839_StaticFields*)StackTraceUtility_t1881293839_il2cpp_TypeInfo_var->static_fields)->set_projectFolder_0(L_0);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern Il2CppClass* StackTraceUtility_t1881293839_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_SetProjectFolder_m2154926761_MetadataUsageId;
extern "C"  void StackTraceUtility_SetProjectFolder_m2154926761 (Il2CppObject * __this /* static, unused */, String_t* ___folder0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_SetProjectFolder_m2154926761_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___folder0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		((StackTraceUtility_t1881293839_StaticFields*)StackTraceUtility_t1881293839_il2cpp_TypeInfo_var->static_fields)->set_projectFolder_0(L_0);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern Il2CppClass* StackTrace_t2500644597_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t1881293839_il2cpp_TypeInfo_var;
extern const uint32_t StackTraceUtility_ExtractStackTrace_m1593581205_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_ExtractStackTrace_m1593581205 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStackTrace_m1593581205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StackTrace_t2500644597 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		StackTrace_t2500644597 * L_0 = (StackTrace_t2500644597 *)il2cpp_codegen_object_new(StackTrace_t2500644597_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1811467992(L_0, 1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_0;
		StackTrace_t2500644597 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		String_t* L_2 = StackTraceUtility_ExtractFormattedStackTrace_m2242276521(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = String_ToString_m3226660001(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3811889820;
extern Il2CppCodeGenString* _stringLiteral3686613043;
extern Il2CppCodeGenString* _stringLiteral1616342375;
extern Il2CppCodeGenString* _stringLiteral2018634576;
extern Il2CppCodeGenString* _stringLiteral3388201006;
extern Il2CppCodeGenString* _stringLiteral157888904;
extern const uint32_t StackTraceUtility_IsSystemStacktraceType_m506502194_MetadataUsageId;
extern "C"  bool StackTraceUtility_IsSystemStacktraceType_m506502194 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___name0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_IsSystemStacktraceType_m506502194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___name0;
		V_0 = ((String_t*)CastclassSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_0;
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m1841920685(L_1, _stringLiteral3811889820, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1841920685(L_3, _stringLiteral3686613043, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = String_StartsWith_m1841920685(L_5, _stringLiteral1616342375, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_7 = V_0;
		NullCheck(L_7);
		bool L_8 = String_StartsWith_m1841920685(L_7, _stringLiteral2018634576, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1841920685(L_9, _stringLiteral3388201006, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0064;
		}
	}
	{
		String_t* L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = String_StartsWith_m1841920685(L_11, _stringLiteral157888904, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_12));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return (bool)G_B7_0;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTrace_t2500644597_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t1881293839_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3698476383;
extern Il2CppCodeGenString* _stringLiteral267581099;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern Il2CppCodeGenString* _stringLiteral811305496;
extern Il2CppCodeGenString* _stringLiteral4028943723;
extern const uint32_t StackTraceUtility_ExtractStringFromExceptionInternal_m2568950546_MetadataUsageId;
extern "C"  void StackTraceUtility_ExtractStringFromExceptionInternal_m2568950546 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___exceptiono0, String_t** ___message1, String_t** ___stackTrace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractStringFromExceptionInternal_m2568950546_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	StackTrace_t2500644597 * V_5 = NULL;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___exceptiono0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t3259014390 * L_1 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_1, _stringLiteral3698476383, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject * L_2 = ___exceptiono0;
		V_0 = ((Exception_t1927440687 *)IsInstClass(L_2, Exception_t1927440687_il2cpp_TypeInfo_var));
		Exception_t1927440687 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t3259014390 * L_4 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_4, _stringLiteral267581099, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0029:
	{
		Exception_t1927440687 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_5);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		Exception_t1927440687 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_7);
		NullCheck(L_8);
		int32_t L_9 = String_get_Length_m1606060069(L_8, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)((int32_t)L_9*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t1221177846 * L_10 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_10, G_B7_0, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t** L_11 = ___message1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		*((Il2CppObject **)(L_11)) = (Il2CppObject *)L_12;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_11), (Il2CppObject *)L_12);
		String_t* L_13 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_2 = L_13;
		goto IL_00ff;
	}

IL_0063:
	{
		String_t* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		Exception_t1927440687 * L_16 = V_0;
		NullCheck(L_16);
		String_t* L_17 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_16);
		V_2 = L_17;
		goto IL_008c;
	}

IL_007a:
	{
		Exception_t1927440687 * L_18 = V_0;
		NullCheck(L_18);
		String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_18);
		String_t* L_20 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m612901809(NULL /*static, unused*/, L_19, _stringLiteral372029352, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
	}

IL_008c:
	{
		Exception_t1927440687 * L_22 = V_0;
		NullCheck(L_22);
		Type_t * L_23 = Exception_GetType_m3898489832(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		V_3 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_25;
		Exception_t1927440687 * L_26 = V_0;
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_26);
		if (!L_27)
		{
			goto IL_00b2;
		}
	}
	{
		Exception_t1927440687 * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_28);
		V_4 = L_29;
	}

IL_00b2:
	{
		String_t* L_30 = V_4;
		NullCheck(L_30);
		String_t* L_31 = String_Trim_m2668767713(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_32 = String_get_Length_m1606060069(L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00d8;
		}
	}
	{
		String_t* L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, L_33, _stringLiteral811305496, /*hidden argument*/NULL);
		V_3 = L_34;
		String_t* L_35 = V_3;
		String_t* L_36 = V_4;
		String_t* L_37 = String_Concat_m2596409543(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
	}

IL_00d8:
	{
		String_t** L_38 = ___message1;
		String_t* L_39 = V_3;
		*((Il2CppObject **)(L_38)) = (Il2CppObject *)L_39;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_38), (Il2CppObject *)L_39);
		Exception_t1927440687 * L_40 = V_0;
		NullCheck(L_40);
		Exception_t1927440687 * L_41 = Exception_get_InnerException_m3722561235(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_00f8;
		}
	}
	{
		String_t* L_42 = V_3;
		String_t* L_43 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral4028943723, L_42, _stringLiteral372029352, L_43, /*hidden argument*/NULL);
		V_2 = L_44;
	}

IL_00f8:
	{
		Exception_t1927440687 * L_45 = V_0;
		NullCheck(L_45);
		Exception_t1927440687 * L_46 = Exception_get_InnerException_m3722561235(L_45, /*hidden argument*/NULL);
		V_0 = L_46;
	}

IL_00ff:
	{
		Exception_t1927440687 * L_47 = V_0;
		if (L_47)
		{
			goto IL_0063;
		}
	}
	{
		StringBuilder_t1221177846 * L_48 = V_1;
		String_t* L_49 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m2596409543(NULL /*static, unused*/, L_49, _stringLiteral372029352, /*hidden argument*/NULL);
		NullCheck(L_48);
		StringBuilder_Append_m3636508479(L_48, L_50, /*hidden argument*/NULL);
		StackTrace_t2500644597 * L_51 = (StackTrace_t2500644597 *)il2cpp_codegen_object_new(StackTrace_t2500644597_il2cpp_TypeInfo_var);
		StackTrace__ctor_m1811467992(L_51, 1, (bool)1, /*hidden argument*/NULL);
		V_5 = L_51;
		StringBuilder_t1221177846 * L_52 = V_1;
		StackTrace_t2500644597 * L_53 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		String_t* L_54 = StackTraceUtility_ExtractFormattedStackTrace_m2242276521(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		StringBuilder_Append_m3636508479(L_52, L_54, /*hidden argument*/NULL);
		String_t** L_55 = ___stackTrace2;
		StringBuilder_t1221177846 * L_56 = V_1;
		NullCheck(L_56);
		String_t* L_57 = StringBuilder_ToString_m1507807375(L_56, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_55)) = (Il2CppObject *)L_57;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_55), (Il2CppObject *)L_57);
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t1881293839_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1422698174;
extern Il2CppCodeGenString* _stringLiteral953296755;
extern Il2CppCodeGenString* _stringLiteral2320793441;
extern Il2CppCodeGenString* _stringLiteral1869581409;
extern Il2CppCodeGenString* _stringLiteral3234713698;
extern Il2CppCodeGenString* _stringLiteral878136096;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern Il2CppCodeGenString* _stringLiteral3875251417;
extern Il2CppCodeGenString* _stringLiteral104530063;
extern Il2CppCodeGenString* _stringLiteral641215582;
extern Il2CppCodeGenString* _stringLiteral572408963;
extern Il2CppCodeGenString* _stringLiteral2508277761;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t StackTraceUtility_PostprocessStacktrace_m2866903298_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_PostprocessStacktrace_m2866903298 (Il2CppObject * __this /* static, unused */, String_t* ___oldString0, bool ___stripEngineInternalInformation1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_PostprocessStacktrace_m2866903298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	StringBuilder_t1221177846 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___oldString0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_1;
	}

IL_000c:
	{
		String_t* L_2 = ___oldString0;
		CharU5BU5D_t1328083999* L_3 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)10));
		NullCheck(L_2);
		StringU5BU5D_t1642385972* L_4 = String_Split_m3326265864(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___oldString0;
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_7 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		StringU5BU5D_t1642385972* L_8 = V_0;
		int32_t L_9 = V_2;
		StringU5BU5D_t1642385972* L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		String_t* L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_13);
		String_t* L_14 = String_Trim_m2668767713(L_13, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_14);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (String_t*)L_14);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_16 = V_2;
		StringU5BU5D_t1642385972* L_17 = V_0;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		StringU5BU5D_t1642385972* L_18 = V_0;
		int32_t L_19 = V_3;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		String_t* L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_4 = L_21;
		String_t* L_22 = V_4;
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1606060069(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_24 = V_4;
		NullCheck(L_24);
		Il2CppChar L_25 = String_get_Chars_m4230566705(L_24, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_25) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		String_t* L_26 = V_4;
		NullCheck(L_26);
		bool L_27 = String_StartsWith_m1841920685(L_26, _stringLiteral1422698174, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		bool L_28 = ___stripEngineInternalInformation1;
		if (!L_28)
		{
			goto IL_00a7;
		}
	}
	{
		String_t* L_29 = V_4;
		NullCheck(L_29);
		bool L_30 = String_StartsWith_m1841920685(L_29, _stringLiteral953296755, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		bool L_31 = ___stripEngineInternalInformation1;
		if (!L_31)
		{
			goto IL_00fa;
		}
	}
	{
		int32_t L_32 = V_3;
		StringU5BU5D_t1642385972* L_33 = V_0;
		NullCheck(L_33);
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_33)->max_length))))-(int32_t)1)))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_34 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		bool L_35 = StackTraceUtility_IsSystemStacktraceType_m506502194(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t1642385972* L_36 = V_0;
		int32_t L_37 = V_3;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		String_t* L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		bool L_40 = StackTraceUtility_IsSystemStacktraceType_m506502194(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		String_t* L_41 = V_4;
		NullCheck(L_41);
		int32_t L_42 = String_IndexOf_m4251815737(L_41, _stringLiteral2320793441, /*hidden argument*/NULL);
		V_5 = L_42;
		int32_t L_43 = V_5;
		if ((((int32_t)L_43) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		String_t* L_44 = V_4;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		String_t* L_46 = String_Substring_m12482732(L_44, 0, L_45, /*hidden argument*/NULL);
		V_4 = L_46;
	}

IL_00fa:
	{
		String_t* L_47 = V_4;
		NullCheck(L_47);
		int32_t L_48 = String_IndexOf_m4251815737(L_47, _stringLiteral1869581409, /*hidden argument*/NULL);
		if ((((int32_t)L_48) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		String_t* L_49 = V_4;
		NullCheck(L_49);
		int32_t L_50 = String_IndexOf_m4251815737(L_49, _stringLiteral3234713698, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		String_t* L_51 = V_4;
		NullCheck(L_51);
		int32_t L_52 = String_IndexOf_m4251815737(L_51, _stringLiteral878136096, /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		bool L_53 = ___stripEngineInternalInformation1;
		if (!L_53)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_54 = V_4;
		NullCheck(L_54);
		bool L_55 = String_StartsWith_m1841920685(L_54, _stringLiteral372029431, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_016c;
		}
	}
	{
		String_t* L_56 = V_4;
		NullCheck(L_56);
		bool L_57 = String_EndsWith_m568509976(L_56, _stringLiteral372029425, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		String_t* L_58 = V_4;
		NullCheck(L_58);
		bool L_59 = String_StartsWith_m1841920685(L_58, _stringLiteral3875251417, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_0188;
		}
	}
	{
		String_t* L_60 = V_4;
		NullCheck(L_60);
		String_t* L_61 = String_Remove_m3580114465(L_60, 0, 3, /*hidden argument*/NULL);
		V_4 = L_61;
	}

IL_0188:
	{
		String_t* L_62 = V_4;
		NullCheck(L_62);
		int32_t L_63 = String_IndexOf_m4251815737(L_62, _stringLiteral104530063, /*hidden argument*/NULL);
		V_6 = L_63;
		V_7 = (-1);
		int32_t L_64 = V_6;
		if ((((int32_t)L_64) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		String_t* L_65 = V_4;
		int32_t L_66 = V_6;
		NullCheck(L_65);
		int32_t L_67 = String_IndexOf_m1887352430(L_65, _stringLiteral372029425, L_66, /*hidden argument*/NULL);
		V_7 = L_67;
	}

IL_01b1:
	{
		int32_t L_68 = V_6;
		if ((((int32_t)L_68) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_69 = V_7;
		int32_t L_70 = V_6;
		if ((((int32_t)L_69) <= ((int32_t)L_70)))
		{
			goto IL_01d4;
		}
	}
	{
		String_t* L_71 = V_4;
		int32_t L_72 = V_6;
		int32_t L_73 = V_7;
		int32_t L_74 = V_6;
		NullCheck(L_71);
		String_t* L_75 = String_Remove_m3580114465(L_71, L_72, ((int32_t)((int32_t)((int32_t)((int32_t)L_73-(int32_t)L_74))+(int32_t)1)), /*hidden argument*/NULL);
		V_4 = L_75;
	}

IL_01d4:
	{
		String_t* L_76 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_76);
		String_t* L_78 = String_Replace_m1941156251(L_76, _stringLiteral641215582, L_77, /*hidden argument*/NULL);
		V_4 = L_78;
		String_t* L_79 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		String_t* L_80 = ((StackTraceUtility_t1881293839_StaticFields*)StackTraceUtility_t1881293839_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_79);
		String_t* L_82 = String_Replace_m1941156251(L_79, L_80, L_81, /*hidden argument*/NULL);
		V_4 = L_82;
		String_t* L_83 = V_4;
		NullCheck(L_83);
		String_t* L_84 = String_Replace_m534438427(L_83, ((int32_t)92), ((int32_t)47), /*hidden argument*/NULL);
		V_4 = L_84;
		String_t* L_85 = V_4;
		NullCheck(L_85);
		int32_t L_86 = String_LastIndexOf_m1975817115(L_85, _stringLiteral572408963, /*hidden argument*/NULL);
		V_8 = L_86;
		int32_t L_87 = V_8;
		if ((((int32_t)L_87) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		String_t* L_88 = V_4;
		int32_t L_89 = V_8;
		NullCheck(L_88);
		String_t* L_90 = String_Remove_m3580114465(L_88, L_89, 5, /*hidden argument*/NULL);
		V_4 = L_90;
		String_t* L_91 = V_4;
		int32_t L_92 = V_8;
		NullCheck(L_91);
		String_t* L_93 = String_Insert_m1649676359(L_91, L_92, _stringLiteral2508277761, /*hidden argument*/NULL);
		V_4 = L_93;
		String_t* L_94 = V_4;
		String_t* L_95 = V_4;
		NullCheck(L_95);
		int32_t L_96 = String_get_Length_m1606060069(L_95, /*hidden argument*/NULL);
		NullCheck(L_94);
		String_t* L_97 = String_Insert_m1649676359(L_94, L_96, _stringLiteral372029317, /*hidden argument*/NULL);
		V_4 = L_97;
	}

IL_024e:
	{
		StringBuilder_t1221177846 * L_98 = V_1;
		String_t* L_99 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_100 = String_Concat_m2596409543(NULL /*static, unused*/, L_99, _stringLiteral372029352, /*hidden argument*/NULL);
		NullCheck(L_98);
		StringBuilder_Append_m3636508479(L_98, L_100, /*hidden argument*/NULL);
	}

IL_0261:
	{
		int32_t L_101 = V_3;
		V_3 = ((int32_t)((int32_t)L_101+(int32_t)1));
	}

IL_0265:
	{
		int32_t L_102 = V_3;
		StringU5BU5D_t1642385972* L_103 = V_0;
		NullCheck(L_103);
		if ((((int32_t)L_102) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_103)->max_length)))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		StringBuilder_t1221177846 * L_104 = V_1;
		NullCheck(L_104);
		String_t* L_105 = StringBuilder_ToString_m1507807375(L_104, /*hidden argument*/NULL);
		return L_105;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StackTraceUtility_t1881293839_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral882462221;
extern Il2CppCodeGenString* _stringLiteral3379456769;
extern Il2CppCodeGenString* _stringLiteral1583069542;
extern Il2CppCodeGenString* _stringLiteral449688297;
extern Il2CppCodeGenString* _stringLiteral2468699112;
extern Il2CppCodeGenString* _stringLiteral1799027380;
extern Il2CppCodeGenString* _stringLiteral2508277761;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t StackTraceUtility_ExtractFormattedStackTrace_m2242276521_MetadataUsageId;
extern "C"  String_t* StackTraceUtility_ExtractFormattedStackTrace_m2242276521 (Il2CppObject * __this /* static, unused */, StackTrace_t2500644597 * ___stackTrace0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StackTraceUtility_ExtractFormattedStackTrace_m2242276521_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	int32_t V_1 = 0;
	StackFrame_t2050294881 * V_2 = NULL;
	MethodBase_t904190842 * V_3 = NULL;
	Type_t * V_4 = NULL;
	String_t* V_5 = NULL;
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t2275869610* V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	bool V_10 = false;
	int32_t V_11 = 0;
	int32_t G_B24_0 = 0;
	int32_t G_B26_0 = 0;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)255), /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0257;
	}

IL_0012:
	{
		StackTrace_t2500644597 * L_1 = ___stackTrace0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		StackFrame_t2050294881 * L_3 = VirtFuncInvoker1< StackFrame_t2050294881 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_1, L_2);
		V_2 = L_3;
		StackFrame_t2050294881 * L_4 = V_2;
		NullCheck(L_4);
		MethodBase_t904190842 * L_5 = VirtFuncInvoker0< MethodBase_t904190842 * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_4);
		V_3 = L_5;
		MethodBase_t904190842 * L_6 = V_3;
		if (L_6)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0253;
	}

IL_002c:
	{
		MethodBase_t904190842 * L_7 = V_3;
		NullCheck(L_7);
		Type_t * L_8 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_7);
		V_4 = L_8;
		Type_t * L_9 = V_4;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_0253;
	}

IL_0040:
	{
		Type_t * L_10 = V_4;
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_10);
		V_5 = L_11;
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_13 = V_5;
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m1606060069(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0071;
		}
	}
	{
		StringBuilder_t1221177846 * L_15 = V_0;
		String_t* L_16 = V_5;
		NullCheck(L_15);
		StringBuilder_Append_m3636508479(L_15, L_16, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_17 = V_0;
		NullCheck(L_17);
		StringBuilder_Append_m3636508479(L_17, _stringLiteral372029316, /*hidden argument*/NULL);
	}

IL_0071:
	{
		StringBuilder_t1221177846 * L_18 = V_0;
		Type_t * L_19 = V_4;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_19);
		NullCheck(L_18);
		StringBuilder_Append_m3636508479(L_18, L_20, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_21 = V_0;
		NullCheck(L_21);
		StringBuilder_Append_m3636508479(L_21, _stringLiteral372029336, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_22 = V_0;
		MethodBase_t904190842 * L_23 = V_3;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_23);
		NullCheck(L_22);
		StringBuilder_Append_m3636508479(L_22, L_24, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_25 = V_0;
		NullCheck(L_25);
		StringBuilder_Append_m3636508479(L_25, _stringLiteral372029318, /*hidden argument*/NULL);
		V_6 = 0;
		MethodBase_t904190842 * L_26 = V_3;
		NullCheck(L_26);
		ParameterInfoU5BU5D_t2275869610* L_27 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_26);
		V_7 = L_27;
		V_8 = (bool)1;
		goto IL_00ee;
	}

IL_00b7:
	{
		bool L_28 = V_8;
		if (L_28)
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t1221177846 * L_29 = V_0;
		NullCheck(L_29);
		StringBuilder_Append_m3636508479(L_29, _stringLiteral811305474, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = (bool)0;
	}

IL_00d2:
	{
		StringBuilder_t1221177846 * L_30 = V_0;
		ParameterInfoU5BU5D_t2275869610* L_31 = V_7;
		int32_t L_32 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		ParameterInfo_t2249040075 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Type_t * L_35 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_34);
		NullCheck(L_35);
		String_t* L_36 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_35);
		NullCheck(L_30);
		StringBuilder_Append_m3636508479(L_30, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_38 = V_6;
		ParameterInfoU5BU5D_t2275869610* L_39 = V_7;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))))))
		{
			goto IL_00b7;
		}
	}
	{
		StringBuilder_t1221177846 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m3636508479(L_40, _stringLiteral372029317, /*hidden argument*/NULL);
		StackFrame_t2050294881 * L_41 = V_2;
		NullCheck(L_41);
		String_t* L_42 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Diagnostics.StackFrame::GetFileName() */, L_41);
		V_9 = L_42;
		String_t* L_43 = V_9;
		if (!L_43)
		{
			goto IL_0247;
		}
	}
	{
		Type_t * L_44 = V_4;
		NullCheck(L_44);
		String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_44);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_46 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_45, _stringLiteral882462221, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0140;
		}
	}
	{
		Type_t * L_47 = V_4;
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_47);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_48, _stringLiteral3379456769, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_01c4;
		}
	}

IL_0140:
	{
		Type_t * L_50 = V_4;
		NullCheck(L_50);
		String_t* L_51 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_51, _stringLiteral1583069542, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_016c;
		}
	}
	{
		Type_t * L_53 = V_4;
		NullCheck(L_53);
		String_t* L_54 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_55 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_54, _stringLiteral3379456769, /*hidden argument*/NULL);
		if (L_55)
		{
			goto IL_01c4;
		}
	}

IL_016c:
	{
		Type_t * L_56 = V_4;
		NullCheck(L_56);
		String_t* L_57 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_58 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_57, _stringLiteral449688297, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_0198;
		}
	}
	{
		Type_t * L_59 = V_4;
		NullCheck(L_59);
		String_t* L_60 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_59);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_61 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_60, _stringLiteral3379456769, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_01c4;
		}
	}

IL_0198:
	{
		Type_t * L_62 = V_4;
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_62);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_64 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_63, _stringLiteral2468699112, /*hidden argument*/NULL);
		if (!L_64)
		{
			goto IL_01c1;
		}
	}
	{
		Type_t * L_65 = V_4;
		NullCheck(L_65);
		String_t* L_66 = VirtFuncInvoker0< String_t* >::Invoke(34 /* System.String System.Type::get_Namespace() */, L_65);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_67 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_66, _stringLiteral1799027380, /*hidden argument*/NULL);
		G_B24_0 = ((int32_t)(L_67));
		goto IL_01c2;
	}

IL_01c1:
	{
		G_B24_0 = 0;
	}

IL_01c2:
	{
		G_B26_0 = G_B24_0;
		goto IL_01c5;
	}

IL_01c4:
	{
		G_B26_0 = 1;
	}

IL_01c5:
	{
		V_10 = (bool)G_B26_0;
		bool L_68 = V_10;
		if (L_68)
		{
			goto IL_0247;
		}
	}
	{
		StringBuilder_t1221177846 * L_69 = V_0;
		NullCheck(L_69);
		StringBuilder_Append_m3636508479(L_69, _stringLiteral2508277761, /*hidden argument*/NULL);
		String_t* L_70 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		String_t* L_71 = ((StackTraceUtility_t1881293839_StaticFields*)StackTraceUtility_t1881293839_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_70);
		bool L_72 = String_StartsWith_m1841920685(L_70, L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_0210;
		}
	}
	{
		String_t* L_73 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(StackTraceUtility_t1881293839_il2cpp_TypeInfo_var);
		String_t* L_74 = ((StackTraceUtility_t1881293839_StaticFields*)StackTraceUtility_t1881293839_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_74);
		int32_t L_75 = String_get_Length_m1606060069(L_74, /*hidden argument*/NULL);
		String_t* L_76 = V_9;
		NullCheck(L_76);
		int32_t L_77 = String_get_Length_m1606060069(L_76, /*hidden argument*/NULL);
		String_t* L_78 = ((StackTraceUtility_t1881293839_StaticFields*)StackTraceUtility_t1881293839_il2cpp_TypeInfo_var->static_fields)->get_projectFolder_0();
		NullCheck(L_78);
		int32_t L_79 = String_get_Length_m1606060069(L_78, /*hidden argument*/NULL);
		NullCheck(L_73);
		String_t* L_80 = String_Substring_m12482732(L_73, L_75, ((int32_t)((int32_t)L_77-(int32_t)L_79)), /*hidden argument*/NULL);
		V_9 = L_80;
	}

IL_0210:
	{
		StringBuilder_t1221177846 * L_81 = V_0;
		String_t* L_82 = V_9;
		NullCheck(L_81);
		StringBuilder_Append_m3636508479(L_81, L_82, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_83 = V_0;
		NullCheck(L_83);
		StringBuilder_Append_m3636508479(L_83, _stringLiteral372029336, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_84 = V_0;
		StackFrame_t2050294881 * L_85 = V_2;
		NullCheck(L_85);
		int32_t L_86 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackFrame::GetFileLineNumber() */, L_85);
		V_11 = L_86;
		String_t* L_87 = Int32_ToString_m2960866144((&V_11), /*hidden argument*/NULL);
		NullCheck(L_84);
		StringBuilder_Append_m3636508479(L_84, L_87, /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_88 = V_0;
		NullCheck(L_88);
		StringBuilder_Append_m3636508479(L_88, _stringLiteral372029317, /*hidden argument*/NULL);
	}

IL_0247:
	{
		StringBuilder_t1221177846 * L_89 = V_0;
		NullCheck(L_89);
		StringBuilder_Append_m3636508479(L_89, _stringLiteral372029352, /*hidden argument*/NULL);
	}

IL_0253:
	{
		int32_t L_90 = V_1;
		V_1 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_0257:
	{
		int32_t L_91 = V_1;
		StackTrace_t2500644597 * L_92 = ___stackTrace0;
		NullCheck(L_92);
		int32_t L_93 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_92);
		if ((((int32_t)L_91) < ((int32_t)L_93)))
		{
			goto IL_0012;
		}
	}
	{
		StringBuilder_t1221177846 * L_94 = V_0;
		NullCheck(L_94);
		String_t* L_95 = StringBuilder_ToString_m1507807375(L_94, /*hidden argument*/NULL);
		return L_95;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern "C"  void StateMachineBehaviour__ctor_m153194 (StateMachineBehaviour_t2151245329 * __this, const MethodInfo* method)
{
	{
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateEnter_m1095276890 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m2021794605 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateExit_m1490844990 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMove_m3403753809 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateIK_m3200380538 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m1007088766 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m526258580 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, int32_t ___stateMachinePathHash1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateEnter_m2847783286 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4078305555  ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m811412975 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4078305555  ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateExit_m4064436078 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4078305555  ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMove_m1941561287 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4078305555  ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateIK_m726770682 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, AnimatorStateInfo_t2577870592  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4078305555  ___controller3, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m1524239226 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t4078305555  ___controller2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32,UnityEngine.Experimental.Director.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m2213014568 (StateMachineBehaviour_t2151245329 * __this, Animator_t69676727 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t4078305555  ___controller2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.SystemClock::.cctor()
extern Il2CppClass* SystemClock_t104337557_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock__cctor_m2847627559_MetadataUsageId;
extern "C"  void SystemClock__cctor_m2847627559 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SystemClock__cctor_m2847627559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t693205669  L_0;
		memset(&L_0, 0, sizeof(L_0));
		DateTime__ctor_m3270618252(&L_0, ((int32_t)1970), 1, 1, 0, 0, 0, 1, /*hidden argument*/NULL);
		((SystemClock_t104337557_StaticFields*)SystemClock_t104337557_il2cpp_TypeInfo_var->static_fields)->set_s_Epoch_0(L_0);
		return;
	}
}
// System.DateTime UnityEngine.SystemClock::get_now()
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern const uint32_t SystemClock_get_now_m4108727544_MetadataUsageId;
extern "C"  DateTime_t693205669  SystemClock_get_now_m4108727544 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SystemClock_get_now_m4108727544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t693205669_il2cpp_TypeInfo_var);
		DateTime_t693205669  L_0 = DateTime_get_Now_m24136300(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C"  String_t* SystemInfo_get_operatingSystem_m2575097876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_operatingSystem_m2575097876_ftn) ();
	static SystemInfo_get_operatingSystem_m2575097876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystem_m2575097876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystem()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C"  String_t* SystemInfo_get_deviceUniqueIdentifier_m145206870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceUniqueIdentifier_m145206870_ftn) ();
	static SystemInfo_get_deviceUniqueIdentifier_m145206870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceUniqueIdentifier_m145206870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceUniqueIdentifier()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceName()
extern "C"  String_t* SystemInfo_get_deviceName_m1285252113 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceName_m1285252113_ftn) ();
	static SystemInfo_get_deviceName_m1285252113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceName_m1285252113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceModel()
extern "C"  String_t* SystemInfo_get_deviceModel_m3856615649 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceModel_m3856615649_ftn) ();
	static SystemInfo_get_deviceModel_m3856615649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceModel_m3856615649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceModel()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TextAreaAttribute__ctor_m2320572467 (TextAreaAttribute_t2454598508 * __this, int32_t ___minLines0, int32_t ___maxLines1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines0;
		__this->set_minLines_0(L_0);
		int32_t L_1 = ___maxLines1;
		__this->set_maxLines_1(L_1);
		return;
	}
}
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m2589865997 (TextAsset_t3973159845 * __this, const MethodInfo* method)
{
	typedef String_t* (*TextAsset_get_text_m2589865997_ftn) (TextAsset_t3973159845 *);
	static TextAsset_get_text_m2589865997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_text_m2589865997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.TextAsset::ToString()
extern "C"  String_t* TextAsset_ToString_m3842796771 (TextAsset_t3973159845 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextAsset_get_text_m2589865997(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor__ctor_m1990252461_MetadataUsageId;
extern "C"  void TextEditor__ctor_m1990252461 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor__ctor_m1990252461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_0 = GUIStyle_get_none_m4224270950(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_style_2(L_0);
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_7(L_1);
		GUIContent_t4210063000 * L_2 = (GUIContent_t4210063000 *)il2cpp_codegen_object_new(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent__ctor_m3889310883(L_2, /*hidden argument*/NULL);
		__this->set_m_Content_8(L_2);
		__this->set_m_iAltCursorPos_19((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::get_text()
extern "C"  String_t* TextEditor_get_text_m1324740216 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		GUIContent_t4210063000 * L_0 = __this->get_m_Content_8();
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m2984350578(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::set_text(System.String)
extern "C"  void TextEditor_set_text_m2231271679 (TextEditor_t3975561390 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		GUIContent_t4210063000 * L_0 = __this->get_m_Content_8();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		GUIContent_set_text_m1170206441(L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = __this->get_address_of_m_CursorIndex_10();
		TextEditor_ClampTextIndex_m3072502406(__this, L_2, /*hidden argument*/NULL);
		int32_t* L_3 = __this->get_address_of_m_SelectIndex_11();
		TextEditor_ClampTextIndex_m3072502406(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect UnityEngine.TextEditor::get_position()
extern "C"  Rect_t3681755626  TextEditor_get_position_m59912781 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0 = __this->get_m_Position_9();
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_position(UnityEngine.Rect)
extern "C"  void TextEditor_set_position_m2773135674 (TextEditor_t3975561390 * __this, Rect_t3681755626  ___value0, const MethodInfo* method)
{
	{
		Rect_t3681755626  L_0 = __this->get_m_Position_9();
		Rect_t3681755626  L_1 = ___value0;
		bool L_2 = Rect_op_Equality_m2793663577(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Rect_t3681755626  L_3 = ___value0;
		__this->set_m_Position_9(L_3);
		TextEditor_UpdateScrollOffset_m1017266912(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_cursorIndex()
extern "C"  int32_t TextEditor_get_cursorIndex_m486786154 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_CursorIndex_10();
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_cursorIndex(System.Int32)
extern "C"  void TextEditor_set_cursorIndex_m817296949 (TextEditor_t3975561390 * __this, int32_t ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_CursorIndex_10();
		V_0 = L_0;
		int32_t L_1 = ___value0;
		__this->set_m_CursorIndex_10(L_1);
		int32_t* L_2 = __this->get_address_of_m_CursorIndex_10();
		TextEditor_ClampTextIndex_m3072502406(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_m_CursorIndex_10();
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_m_RevealCursor_12((bool)1);
	}

IL_002d:
	{
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::get_selectIndex()
extern "C"  int32_t TextEditor_get_selectIndex_m1435337632 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_SelectIndex_11();
		return L_0;
	}
}
// System.Void UnityEngine.TextEditor::set_selectIndex(System.Int32)
extern "C"  void TextEditor_set_selectIndex_m2701963187 (TextEditor_t3975561390 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_SelectIndex_11(L_0);
		int32_t* L_1 = __this->get_address_of_m_SelectIndex_11();
		TextEditor_ClampTextIndex_m3072502406(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern "C"  void TextEditor_ClearCursorPos_m657928476 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		__this->set_hasHorizontalCursorPos_4((bool)0);
		__this->set_m_iAltCursorPos_19((-1));
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern "C"  void TextEditor_OnFocus_m4031289844 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_multiline_3();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 0;
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_2, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m435039312(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		__this->set_m_HasFocus_6((bool)1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnLostFocus()
extern "C"  void TextEditor_OnLostFocus_m1193078298 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		__this->set_m_HasFocus_6((bool)0);
		Vector2_t2243707579  L_0 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_7(L_0);
		return;
	}
}
// System.Void UnityEngine.TextEditor::GrabGraphicalCursorPos()
extern "C"  void TextEditor_GrabGraphicalCursorPos_m194203154 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_hasHorizontalCursorPos_4();
		if (L_0)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t1799908754 * L_1 = __this->get_style_2();
		Rect_t3681755626  L_2 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_3 = __this->get_m_Content_8();
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector2_t2243707579  L_5 = GUIStyle_GetCursorPixelPosition_m2488570694(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_5);
		GUIStyle_t1799908754 * L_6 = __this->get_style_2();
		Rect_t3681755626  L_7 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_8 = __this->get_m_Content_8();
		int32_t L_9 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector2_t2243707579  L_10 = GUIStyle_GetCursorPixelPosition_m2488570694(L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->set_graphicalSelectCursorPos_14(L_10);
		__this->set_hasHorizontalCursorPos_4((bool)0);
	}

IL_0058:
	{
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::HandleKeyEvent(UnityEngine.Event)
extern Il2CppClass* TextEditor_t3975561390_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1109105793_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m2897624762_MethodInfo_var;
extern const uint32_t TextEditor_HandleKeyEvent_m1463561879_MetadataUsageId;
extern "C"  bool TextEditor_HandleKeyEvent_m1463561879 (TextEditor_t3975561390 * __this, Event_t3028476042 * ___e0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_HandleKeyEvent_m1463561879_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		TextEditor_InitKeyActions_m3270054209(__this, /*hidden argument*/NULL);
		Event_t3028476042 * L_0 = ___e0;
		NullCheck(L_0);
		int32_t L_1 = Event_get_modifiers_m430092210(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t3028476042 * L_2 = ___e0;
		Event_t3028476042 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = Event_get_modifiers_m430092210(L_3, /*hidden argument*/NULL);
		NullCheck(L_3);
		Event_set_modifiers_m2565042639(L_3, ((int32_t)((int32_t)L_4&(int32_t)((int32_t)-33))), /*hidden argument*/NULL);
		Dictionary_2_t1747193563 * L_5 = ((TextEditor_t3975561390_StaticFields*)TextEditor_t3975561390_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		Event_t3028476042 * L_6 = ___e0;
		NullCheck(L_5);
		bool L_7 = Dictionary_2_ContainsKey_m1109105793(L_5, L_6, /*hidden argument*/Dictionary_2_ContainsKey_m1109105793_MethodInfo_var);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t1747193563 * L_8 = ((TextEditor_t3975561390_StaticFields*)TextEditor_t3975561390_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		Event_t3028476042 * L_9 = ___e0;
		NullCheck(L_8);
		int32_t L_10 = Dictionary_2_get_Item_m2897624762(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_m2897624762_MethodInfo_var);
		V_1 = L_10;
		int32_t L_11 = V_1;
		TextEditor_PerformOperation_m2560628023(__this, L_11, /*hidden argument*/NULL);
		Event_t3028476042 * L_12 = ___e0;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		Event_set_modifiers_m2565042639(L_12, L_13, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0049:
	{
		Event_t3028476042 * L_14 = ___e0;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		Event_set_modifiers_m2565042639(L_14, L_15, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteLineBack()
extern "C"  bool TextEditor_DeleteLineBack_m1837804025 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m3407150912(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		goto IL_003e;
	}

IL_0022:
	{
		String_t* L_3 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_3, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_6 = V_1;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		goto IL_0048;
	}

IL_003e:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = L_7;
		V_1 = ((int32_t)((int32_t)L_8-(int32_t)1));
		if (L_8)
		{
			goto IL_0022;
		}
	}

IL_0048:
	{
		int32_t L_9 = V_1;
		if ((!(((uint32_t)L_9) == ((uint32_t)(-1)))))
		{
			goto IL_0051;
		}
	}
	{
		V_0 = 0;
	}

IL_0051:
	{
		int32_t L_10 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) == ((int32_t)L_11)))
		{
			goto IL_008e;
		}
	}
	{
		GUIContent_t4210063000 * L_12 = __this->get_m_Content_8();
		String_t* L_13 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		int32_t L_15 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		NullCheck(L_13);
		String_t* L_17 = String_Remove_m3580114465(L_13, L_14, ((int32_t)((int32_t)L_15-(int32_t)L_16)), /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIContent_set_text_m1170206441(L_12, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		V_2 = L_18;
		int32_t L_19 = V_2;
		TextEditor_set_cursorIndex_m817296949(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = V_2;
		TextEditor_set_selectIndex_m2701963187(__this, L_20, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_008e:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordBack()
extern "C"  bool TextEditor_DeleteWordBack_m2547107185 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m3407150912(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindEndOfPreviousWord_m2880983930(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_005e;
		}
	}
	{
		GUIContent_t4210063000 * L_5 = __this->get_m_Content_8();
		String_t* L_6 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		int32_t L_8 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		NullCheck(L_6);
		String_t* L_10 = String_Remove_m3580114465(L_6, L_7, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		NullCheck(L_5);
		GUIContent_set_text_m1170206441(L_5, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		V_1 = L_11;
		int32_t L_12 = V_1;
		TextEditor_set_cursorIndex_m817296949(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		TextEditor_set_selectIndex_m2701963187(__this, L_13, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005e:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteWordForward()
extern "C"  bool TextEditor_DeleteWordForward_m3131302373 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m3407150912(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_FindStartOfNextWord_m2609021395(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_4 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_5)))
		{
			goto IL_005d;
		}
	}
	{
		GUIContent_t4210063000 * L_6 = __this->get_m_Content_8();
		String_t* L_7 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		int32_t L_10 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_11 = String_Remove_m3580114465(L_7, L_8, ((int32_t)((int32_t)L_9-(int32_t)L_10)), /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIContent_set_text_m1170206441(L_6, L_11, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005d:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::Delete()
extern "C"  bool TextEditor_Delete_m2400679556 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = TextEditor_get_hasSelection_m3407150912(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_2 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0049;
		}
	}
	{
		GUIContent_t4210063000 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Remove_m3580114465(L_5, L_6, 1, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m1170206441(L_4, L_7, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0049:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.TextEditor::Backspace()
extern "C"  bool TextEditor_Backspace_m3475645052 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = TextEditor_get_hasSelection_m3407150912(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0014:
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		GUIContent_t4210063000 * L_2 = __this->get_m_Content_8();
		String_t* L_3 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_5 = String_Remove_m3580114465(L_3, ((int32_t)((int32_t)L_4-(int32_t)1)), 1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIContent_set_text_m1170206441(L_2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)1));
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_8, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_005e:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
extern "C"  void TextEditor_SelectAll_m435039312 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m817296949(__this, 0, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_1, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectNone()
extern "C"  void TextEditor_SelectNone_m3992543199 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_0, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::get_hasSelection()
extern "C"  bool TextEditor_get_hasSelection_m3407150912 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_DeleteSelection_m501755976_MetadataUsageId;
extern "C"  bool TextEditor_DeleteSelection_m501755976 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DeleteSelection_m501755976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0013;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_007a;
		}
	}
	{
		GUIContent_t4210063000 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m12482732(L_5, 0, L_6, /*hidden argument*/NULL);
		String_t* L_8 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		String_t* L_10 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_13 = String_Substring_m12482732(L_8, L_9, ((int32_t)((int32_t)L_11-(int32_t)L_12)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m2596409543(NULL /*static, unused*/, L_7, L_13, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m1170206441(L_4, L_14, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_15, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_007a:
	{
		GUIContent_t4210063000 * L_16 = __this->get_m_Content_8();
		String_t* L_17 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m12482732(L_17, 0, L_18, /*hidden argument*/NULL);
		String_t* L_20 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_21 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_22 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_23 = String_get_Length_m1606060069(L_22, /*hidden argument*/NULL);
		int32_t L_24 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_25 = String_Substring_m12482732(L_20, L_21, ((int32_t)((int32_t)L_23-(int32_t)L_24)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2596409543(NULL /*static, unused*/, L_19, L_25, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUIContent_set_text_m1170206441(L_16, L_26, /*hidden argument*/NULL);
		int32_t L_27 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_27, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern "C"  void TextEditor_ReplaceSelection_m356220427 (TextEditor_t3975561390 * __this, String_t* ___replace0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_0 = __this->get_m_Content_8();
		String_t* L_1 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_3 = ___replace0;
		NullCheck(L_1);
		String_t* L_4 = String_Insert_m1649676359(L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		GUIContent_set_text_m1170206441(L_0, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_6 = ___replace0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ((int32_t)((int32_t)L_5+(int32_t)L_7));
		V_0 = L_8;
		TextEditor_set_cursorIndex_m817296949(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_9, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::Insert(System.Char)
extern "C"  void TextEditor_Insert_m2308889849 (TextEditor_t3975561390 * __this, Il2CppChar ___c0, const MethodInfo* method)
{
	{
		String_t* L_0 = Char_ToString_m1976753030((&___c0), /*hidden argument*/NULL);
		TextEditor_ReplaceSelection_m356220427(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveRight()
extern "C"  void TextEditor_MoveRight_m2990766682 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_DetectFocusChange_m2483493478(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_3, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003c:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_6 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_6, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_005e:
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_7, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLeft()
extern "C"  void TextEditor_MoveLeft_m2907864443 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_3, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0030:
	{
		int32_t L_4 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_6, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0052:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_7, /*hidden argument*/NULL);
	}

IL_005e:
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveUp()
extern "C"  void TextEditor_MoveUp_m799146301 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m194203154(__this, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_4 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t2243707579 * L_5 = L_4;
		float L_6 = L_5->get_y_2();
		L_5->set_y_2(((float)((float)L_6-(float)(1.0f))));
		GUIStyle_t1799908754 * L_7 = __this->get_style_2();
		Rect_t3681755626  L_8 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_9 = __this->get_m_Content_8();
		Vector2_t2243707579  L_10 = __this->get_graphicalCursorPos_13();
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m326283516(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveDown()
extern "C"  void TextEditor_MoveDown_m3610827042 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_2, /*hidden argument*/NULL);
		goto IL_002e;
	}

IL_0022:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		TextEditor_GrabGraphicalCursorPos_m194203154(__this, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_4 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t2243707579 * L_5 = L_4;
		float L_6 = L_5->get_y_2();
		GUIStyle_t1799908754 * L_7 = __this->get_style_2();
		NullCheck(L_7);
		float L_8 = GUIStyle_get_lineHeight_m2132859383(L_7, /*hidden argument*/NULL);
		L_5->set_y_2(((float)((float)L_6+(float)((float)((float)L_8+(float)(5.0f))))));
		GUIStyle_t1799908754 * L_9 = __this->get_style_2();
		Rect_t3681755626  L_10 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_11 = __this->get_m_Content_8();
		Vector2_t2243707579  L_12 = __this->get_graphicalCursorPos_13();
		NullCheck(L_9);
		int32_t L_13 = GUIStyle_GetCursorStringIndex_m326283516(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_15, /*hidden argument*/NULL);
		int32_t L_16 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_17 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		int32_t L_18 = String_get_Length_m1606060069(L_17, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_18))))
		{
			goto IL_009f;
		}
	}
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
	}

IL_009f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineStart()
extern "C"  void TextEditor_MoveLineStart_m3866297846 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		goto IL_0050;
	}

IL_002a:
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_5, L_6, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_8 = V_1;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
		int32_t L_9 = V_2;
		TextEditor_set_cursorIndex_m817296949(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_2;
		TextEditor_set_selectIndex_m2701963187(__this, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0050:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = L_11;
		V_1 = ((int32_t)((int32_t)L_12-(int32_t)1));
		if (L_12)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = 0;
		int32_t L_13 = V_2;
		TextEditor_set_cursorIndex_m817296949(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		TextEditor_set_selectIndex_m2701963187(__this, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveLineEnd()
extern "C"  void TextEditor_MoveLineEnd_m1288595337 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0022:
	{
		V_0 = G_B3_0;
		int32_t L_4 = V_0;
		V_1 = L_4;
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_005e;
	}

IL_0036:
	{
		String_t* L_7 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		Il2CppChar L_9 = String_get_Chars_m4230566705(L_7, L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_10 = V_1;
		V_3 = L_10;
		int32_t L_11 = V_3;
		TextEditor_set_cursorIndex_m817296949(__this, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_3;
		TextEditor_set_selectIndex_m2701963187(__this, L_12, /*hidden argument*/NULL);
		return;
	}

IL_005a:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_16 = V_2;
		V_3 = L_16;
		int32_t L_17 = V_3;
		TextEditor_set_cursorIndex_m817296949(__this, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		TextEditor_set_selectIndex_m2701963187(__this, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineStart()
extern "C"  void TextEditor_MoveGraphicalLineStart_m3705073185 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	TextEditor_t3975561390 * G_B2_1 = NULL;
	TextEditor_t3975561390 * G_B1_0 = NULL;
	TextEditor_t3975561390 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t3975561390 * G_B3_1 = NULL;
	TextEditor_t3975561390 * G_B3_2 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineStart_m2300210041(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m817296949(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveGraphicalLineEnd()
extern "C"  void TextEditor_MoveGraphicalLineEnd_m1918309850 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	TextEditor_t3975561390 * G_B2_1 = NULL;
	TextEditor_t3975561390 * G_B1_0 = NULL;
	TextEditor_t3975561390 * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t3975561390 * G_B3_1 = NULL;
	TextEditor_t3975561390 * G_B3_2 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		G_B1_1 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			G_B2_1 = __this;
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0024;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0024:
	{
		NullCheck(G_B3_1);
		int32_t L_4 = TextEditor_GetGraphicalLineEnd_m1122405818(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		NullCheck(G_B3_2);
		TextEditor_set_cursorIndex_m817296949(G_B3_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextStart()
extern "C"  void TextEditor_MoveTextStart_m926646761 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveTextEnd()
extern "C"  void TextEditor_MoveTextEnd_m158107192 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::IndexOfEndOfLine(System.Int32)
extern "C"  int32_t TextEditor_IndexOfEndOfLine_m156682161 (TextEditor_t3975561390 * __this, int32_t ___startIndex0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___startIndex0;
		NullCheck(L_0);
		int32_t L_2 = String_IndexOf_m3890362537(L_0, ((int32_t)10), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B3_0 = L_4;
		goto IL_0027;
	}

IL_001c:
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0027:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphForward()
extern "C"  void TextEditor_MoveParagraphForward_m2156700157 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	TextEditor_t3975561390 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t3975561390 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m817296949(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = String_get_Length_m1606060069(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) >= ((int32_t)L_6)))
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_IndexOfEndOfLine_m156682161(__this, ((int32_t)((int32_t)L_7+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_9, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_10, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveParagraphBackward()
extern "C"  void TextEditor_MoveParagraphBackward_m3489571549 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	TextEditor_t3975561390 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t3975561390 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m817296949(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)1)))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_7 = String_LastIndexOf_m3157515271(L_5, ((int32_t)10), ((int32_t)((int32_t)L_6-(int32_t)2)), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_9, /*hidden argument*/NULL);
		goto IL_006f;
	}

IL_005f:
	{
		V_0 = 0;
		int32_t L_10 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_11, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveCursorToPosition(UnityEngine.Vector2)
extern "C"  void TextEditor_MoveCursorToPosition_m1568397318 (TextEditor_t3975561390 * __this, Vector2_t2243707579  ___cursorPosition0, const MethodInfo* method)
{
	{
		GUIStyle_t1799908754 * L_0 = __this->get_style_2();
		Rect_t3681755626  L_1 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_2 = __this->get_m_Content_8();
		Vector2_t2243707579  L_3 = ___cursorPosition0;
		Vector2_t2243707579  L_4 = __this->get_scrollOffset_7();
		Vector2_t2243707579  L_5 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_6 = GUIStyle_GetCursorStringIndex_m326283516(L_0, L_1, L_2, L_5, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_6, /*hidden argument*/NULL);
		Event_t3028476042 * L_7 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Event_get_shift_m1229469022(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_9, /*hidden argument*/NULL);
	}

IL_0044:
	{
		TextEditor_DetectFocusChange_m2483493478(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToPosition(UnityEngine.Vector2)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_SelectToPosition_m3210086107_MetadataUsageId;
extern "C"  void TextEditor_SelectToPosition_m3210086107 (TextEditor_t3975561390 * __this, Vector2_t2243707579  ___cursorPosition0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_SelectToPosition_m3210086107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_MouseDragSelectsWholeWords_15();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		GUIStyle_t1799908754 * L_1 = __this->get_style_2();
		Rect_t3681755626  L_2 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_3 = __this->get_m_Content_8();
		Vector2_t2243707579  L_4 = ___cursorPosition0;
		Vector2_t2243707579  L_5 = __this->get_scrollOffset_7();
		Vector2_t2243707579  L_6 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m326283516(L_1, L_2, L_3, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_7, /*hidden argument*/NULL);
		goto IL_0193;
	}

IL_0039:
	{
		GUIStyle_t1799908754 * L_8 = __this->get_style_2();
		Rect_t3681755626  L_9 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_10 = __this->get_m_Content_8();
		Vector2_t2243707579  L_11 = ___cursorPosition0;
		Vector2_t2243707579  L_12 = __this->get_scrollOffset_7();
		Vector2_t2243707579  L_13 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_14 = GUIStyle_GetCursorStringIndex_m326283516(L_8, L_9, L_10, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		uint8_t L_15 = __this->get_m_DblClickSnap_17();
		if (L_15)
		{
			goto IL_00e1;
		}
	}
	{
		int32_t L_16 = V_0;
		int32_t L_17 = __this->get_m_DblClickInitPos_16();
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_18 = V_0;
		int32_t L_19 = TextEditor_FindEndOfClassification_m2938898406(__this, L_18, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_19, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_m_DblClickInitPos_16();
		int32_t L_21 = TextEditor_FindEndOfClassification_m2938898406(__this, L_20, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_21, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_009a:
	{
		int32_t L_22 = V_0;
		String_t* L_23 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		int32_t L_24 = String_get_Length_m1606060069(L_23, /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_00b9;
		}
	}
	{
		String_t* L_25 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		int32_t L_26 = String_get_Length_m1606060069(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_26-(int32_t)1));
	}

IL_00b9:
	{
		int32_t L_27 = V_0;
		int32_t L_28 = TextEditor_FindEndOfClassification_m2938898406(__this, L_27, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_28, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_m_DblClickInitPos_16();
		int32_t L_30 = TextEditor_FindEndOfClassification_m2938898406(__this, ((int32_t)((int32_t)L_29-(int32_t)1)), (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_30, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		goto IL_0193;
	}

IL_00e1:
	{
		int32_t L_31 = V_0;
		int32_t L_32 = __this->get_m_DblClickInitPos_16();
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_013c;
		}
	}
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) <= ((int32_t)0)))
		{
			goto IL_0117;
		}
	}
	{
		String_t* L_34 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_35 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_36 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_35-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_34);
		int32_t L_37 = String_LastIndexOf_m3157515271(L_34, ((int32_t)10), L_36, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_37+(int32_t)1)), /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_0117:
	{
		TextEditor_set_cursorIndex_m817296949(__this, 0, /*hidden argument*/NULL);
	}

IL_011e:
	{
		String_t* L_38 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_39 = __this->get_m_DblClickInitPos_16();
		NullCheck(L_38);
		int32_t L_40 = String_LastIndexOf_m3157515271(L_38, ((int32_t)10), L_39, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_40, /*hidden argument*/NULL);
		goto IL_0193;
	}

IL_013c:
	{
		int32_t L_41 = V_0;
		String_t* L_42 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = String_get_Length_m1606060069(L_42, /*hidden argument*/NULL);
		if ((((int32_t)L_41) >= ((int32_t)L_43)))
		{
			goto IL_015f;
		}
	}
	{
		int32_t L_44 = V_0;
		int32_t L_45 = TextEditor_IndexOfEndOfLine_m156682161(__this, L_44, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_45, /*hidden argument*/NULL);
		goto IL_0170;
	}

IL_015f:
	{
		String_t* L_46 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		int32_t L_47 = String_get_Length_m1606060069(L_46, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_47, /*hidden argument*/NULL);
	}

IL_0170:
	{
		String_t* L_48 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_49 = __this->get_m_DblClickInitPos_16();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_50 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_49-(int32_t)2)), /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_51 = String_LastIndexOf_m3157515271(L_48, ((int32_t)10), L_50, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, ((int32_t)((int32_t)L_51+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0193:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectLeft()
extern "C"  void TextEditor_SelectLeft_m1025549794 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_bJustSelected_18();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->set_m_bJustSelected_18((bool)0);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectRight()
extern "C"  void TextEditor_SelectRight_m2396682109 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_bJustSelected_18();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		__this->set_m_bJustSelected_18((bool)0);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectUp()
extern "C"  void TextEditor_SelectUp_m3826611706 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m194203154(__this, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_0 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t2243707579 * L_1 = L_0;
		float L_2 = L_1->get_y_2();
		L_1->set_y_2(((float)((float)L_2-(float)(1.0f))));
		GUIStyle_t1799908754 * L_3 = __this->get_style_2();
		Rect_t3681755626  L_4 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_5 = __this->get_m_Content_8();
		Vector2_t2243707579  L_6 = __this->get_graphicalCursorPos_13();
		NullCheck(L_3);
		int32_t L_7 = GUIStyle_GetCursorStringIndex_m326283516(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectDown()
extern "C"  void TextEditor_SelectDown_m322570859 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_GrabGraphicalCursorPos_m194203154(__this, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_0 = __this->get_address_of_graphicalCursorPos_13();
		Vector2_t2243707579 * L_1 = L_0;
		float L_2 = L_1->get_y_2();
		GUIStyle_t1799908754 * L_3 = __this->get_style_2();
		NullCheck(L_3);
		float L_4 = GUIStyle_get_lineHeight_m2132859383(L_3, /*hidden argument*/NULL);
		L_1->set_y_2(((float)((float)L_2+(float)((float)((float)L_4+(float)(5.0f))))));
		GUIStyle_t1799908754 * L_5 = __this->get_style_2();
		Rect_t3681755626  L_6 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_7 = __this->get_m_Content_8();
		Vector2_t2243707579  L_8 = __this->get_graphicalCursorPos_13();
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m326283516(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextEnd()
extern "C"  void TextEditor_SelectTextEnd_m1542698735 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectTextStart()
extern "C"  void TextEditor_SelectTextStart_m3661773790 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_set_cursorIndex_m817296949(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MouseDragSelectsWholeWords(System.Boolean)
extern "C"  void TextEditor_MouseDragSelectsWholeWords_m3862197328 (TextEditor_t3975561390 * __this, bool ___on0, const MethodInfo* method)
{
	{
		bool L_0 = ___on0;
		__this->set_m_MouseDragSelectsWholeWords_15(L_0);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		__this->set_m_DblClickInitPos_16(L_1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::DblClickSnap(UnityEngine.TextEditor/DblClickSnapping)
extern "C"  void TextEditor_DblClickSnap_m2540158713 (TextEditor_t3975561390 * __this, uint8_t ___snapping0, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___snapping0;
		__this->set_m_DblClickSnap_17(L_0);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineStart(System.Int32)
extern "C"  int32_t TextEditor_GetGraphicalLineStart_m2300210041 (TextEditor_t3975561390 * __this, int32_t ___p0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GUIStyle_t1799908754 * L_0 = __this->get_style_2();
		Rect_t3681755626  L_1 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_2 = __this->get_m_Content_8();
		int32_t L_3 = ___p0;
		NullCheck(L_0);
		Vector2_t2243707579  L_4 = GUIStyle_GetCursorPixelPosition_m2488570694(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		(&V_0)->set_x_1((0.0f));
		GUIStyle_t1799908754 * L_5 = __this->get_style_2();
		Rect_t3681755626  L_6 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_7 = __this->get_m_Content_8();
		Vector2_t2243707579  L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = GUIStyle_GetCursorStringIndex_m326283516(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Int32 UnityEngine.TextEditor::GetGraphicalLineEnd(System.Int32)
extern "C"  int32_t TextEditor_GetGraphicalLineEnd_m1122405818 (TextEditor_t3975561390 * __this, int32_t ___p0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		GUIStyle_t1799908754 * L_0 = __this->get_style_2();
		Rect_t3681755626  L_1 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_2 = __this->get_m_Content_8();
		int32_t L_3 = ___p0;
		NullCheck(L_0);
		Vector2_t2243707579  L_4 = GUIStyle_GetCursorPixelPosition_m2488570694(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t2243707579 * L_5 = (&V_0);
		float L_6 = L_5->get_x_1();
		L_5->set_x_1(((float)((float)L_6+(float)(5000.0f))));
		GUIStyle_t1799908754 * L_7 = __this->get_style_2();
		Rect_t3681755626  L_8 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_9 = __this->get_m_Content_8();
		Vector2_t2243707579  L_10 = V_0;
		NullCheck(L_7);
		int32_t L_11 = GUIStyle_GetCursorStringIndex_m326283516(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Int32 UnityEngine.TextEditor::FindNextSeperator(System.Int32)
extern "C"  int32_t TextEditor_FindNextSeperator_m4074817073 (TextEditor_t3975561390 * __this, int32_t ___startPos0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0016;
	}

IL_0011:
	{
		int32_t L_2 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0016:
	{
		int32_t L_3 = ___startPos0;
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___startPos0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_5, L_6, /*hidden argument*/NULL);
		bool L_8 = TextEditor_isLetterLikeChar_m4002314989(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0011;
		}
	}

IL_0033:
	{
		goto IL_003d;
	}

IL_0038:
	{
		int32_t L_9 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_10 = ___startPos0;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_12 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_13 = ___startPos0;
		NullCheck(L_12);
		Il2CppChar L_14 = String_get_Chars_m4230566705(L_12, L_13, /*hidden argument*/NULL);
		bool L_15 = TextEditor_isLetterLikeChar_m4002314989(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0038;
		}
	}

IL_005a:
	{
		int32_t L_16 = ___startPos0;
		return L_16;
	}
}
// System.Boolean UnityEngine.TextEditor::isLetterLikeChar(System.Char)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_isLetterLikeChar_m4002314989_MetadataUsageId;
extern "C"  bool TextEditor_isLetterLikeChar_m4002314989 (Il2CppObject * __this /* static, unused */, Il2CppChar ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_isLetterLikeChar_m4002314989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Il2CppChar L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsLetterOrDigit_m2164758816(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppChar L_2 = ___c0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)((int32_t)39)))? 1 : 0);
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 1;
	}

IL_0013:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 UnityEngine.TextEditor::FindPrevSeperator(System.Int32)
extern "C"  int32_t TextEditor_FindPrevSeperator_m3377670283 (TextEditor_t3975561390 * __this, int32_t ___startPos0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_0-(int32_t)1));
		goto IL_000f;
	}

IL_000a:
	{
		int32_t L_1 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_1-(int32_t)1));
	}

IL_000f:
	{
		int32_t L_2 = ___startPos0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_3 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_4 = ___startPos0;
		NullCheck(L_3);
		Il2CppChar L_5 = String_get_Chars_m4230566705(L_3, L_4, /*hidden argument*/NULL);
		bool L_6 = TextEditor_isLetterLikeChar_m4002314989(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_000a;
		}
	}

IL_002c:
	{
		goto IL_0036;
	}

IL_0031:
	{
		int32_t L_7 = ___startPos0;
		___startPos0 = ((int32_t)((int32_t)L_7-(int32_t)1));
	}

IL_0036:
	{
		int32_t L_8 = ___startPos0;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_9 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_10 = ___startPos0;
		NullCheck(L_9);
		Il2CppChar L_11 = String_get_Chars_m4230566705(L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = TextEditor_isLetterLikeChar_m4002314989(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0031;
		}
	}

IL_0053:
	{
		int32_t L_13 = ___startPos0;
		return ((int32_t)((int32_t)L_13+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::MoveWordRight()
extern "C"  void TextEditor_MoveWordRight_m2225371862 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	TextEditor_t3975561390 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t3975561390 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m817296949(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindNextSeperator_m4074817073(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_7, /*hidden argument*/NULL);
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToStartOfNextWord()
extern "C"  void TextEditor_MoveToStartOfNextWord_m893401463 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveRight_m2990766682(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindStartOfNextWord_m2609021395(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::MoveToEndOfPreviousWord()
extern "C"  void TextEditor_MoveToEndOfPreviousWord_m119836618 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001e;
		}
	}
	{
		TextEditor_MoveLeft_m2907864443(__this, /*hidden argument*/NULL);
		return;
	}

IL_001e:
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_FindEndOfPreviousWord_m2880983930(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToStartOfNextWord()
extern "C"  void TextEditor_SelectToStartOfNextWord_m2812882512 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindStartOfNextWord_m2609021395(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectToEndOfPreviousWord()
extern "C"  void TextEditor_SelectToEndOfPreviousWord_m3931162275 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_FindEndOfPreviousWord_m2880983930(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextEditor/CharacterType UnityEngine.TextEditor::ClassifyChar(System.Char)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_ClassifyChar_m1576648472_MetadataUsageId;
extern "C"  int32_t TextEditor_ClassifyChar_m1576648472 (TextEditor_t3975561390 * __this, Il2CppChar ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ClassifyChar_m1576648472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppChar L_0 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_1 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (int32_t)(3);
	}

IL_000d:
	{
		Il2CppChar L_2 = ___c0;
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_3 = Char_IsLetterOrDigit_m2164758816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppChar L_4 = ___c0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)39)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (int32_t)(0);
	}

IL_0022:
	{
		return (int32_t)(1);
	}
}
// System.Int32 UnityEngine.TextEditor::FindStartOfNextWord(System.Int32)
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_FindStartOfNextWord_m2609021395_MetadataUsageId;
extern "C"  int32_t TextEditor_FindStartOfNextWord_m2609021395 (TextEditor_t3975561390 * __this, int32_t ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_FindStartOfNextWord_m2609021395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___p0;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_4 = ___p0;
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___p0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Il2CppChar L_8 = V_1;
		int32_t L_9 = TextEditor_ClassifyChar_m1576648472(__this, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) == ((int32_t)3)))
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_11 = ___p0;
		___p0 = ((int32_t)((int32_t)L_11+(int32_t)1));
		goto IL_0040;
	}

IL_003b:
	{
		int32_t L_12 = ___p0;
		___p0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_13 = ___p0;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_15 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_16 = ___p0;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m4230566705(L_15, L_16, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_ClassifyChar_m1576648472(__this, L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_003b;
		}
	}

IL_005f:
	{
		goto IL_0078;
	}

IL_0064:
	{
		Il2CppChar L_20 = V_1;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)9))))
		{
			goto IL_0074;
		}
	}
	{
		Il2CppChar L_21 = V_1;
		if ((!(((uint32_t)L_21) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_0078;
		}
	}

IL_0074:
	{
		int32_t L_22 = ___p0;
		return ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_23 = ___p0;
		int32_t L_24 = V_0;
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_25 = ___p0;
		return L_25;
	}

IL_0081:
	{
		String_t* L_26 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_27 = ___p0;
		NullCheck(L_26);
		Il2CppChar L_28 = String_get_Chars_m4230566705(L_26, L_27, /*hidden argument*/NULL);
		V_1 = L_28;
		Il2CppChar L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)32)))))
		{
			goto IL_00c2;
		}
	}
	{
		goto IL_00a0;
	}

IL_009b:
	{
		int32_t L_30 = ___p0;
		___p0 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00a0:
	{
		int32_t L_31 = ___p0;
		int32_t L_32 = V_0;
		if ((((int32_t)L_31) >= ((int32_t)L_32)))
		{
			goto IL_00bd;
		}
	}
	{
		String_t* L_33 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_34 = ___p0;
		NullCheck(L_33);
		Il2CppChar L_35 = String_get_Chars_m4230566705(L_33, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_t3454481338_il2cpp_TypeInfo_var);
		bool L_36 = Char_IsWhiteSpace_m1507160293(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_009b;
		}
	}

IL_00bd:
	{
		goto IL_00d4;
	}

IL_00c2:
	{
		Il2CppChar L_37 = V_1;
		if ((((int32_t)L_37) == ((int32_t)((int32_t)9))))
		{
			goto IL_00d2;
		}
	}
	{
		Il2CppChar L_38 = V_1;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_00d4;
		}
	}

IL_00d2:
	{
		int32_t L_39 = ___p0;
		return L_39;
	}

IL_00d4:
	{
		int32_t L_40 = ___p0;
		return L_40;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfPreviousWord(System.Int32)
extern "C"  int32_t TextEditor_FindEndOfPreviousWord_m2880983930 (TextEditor_t3975561390 * __this, int32_t ___p0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___p0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		int32_t L_1 = ___p0;
		return L_1;
	}

IL_0008:
	{
		int32_t L_2 = ___p0;
		___p0 = ((int32_t)((int32_t)L_2-(int32_t)1));
		goto IL_0017;
	}

IL_0012:
	{
		int32_t L_3 = ___p0;
		___p0 = ((int32_t)((int32_t)L_3-(int32_t)1));
	}

IL_0017:
	{
		int32_t L_4 = ___p0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = ___p0;
		NullCheck(L_5);
		Il2CppChar L_7 = String_get_Chars_m4230566705(L_5, L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)((int32_t)32))))
		{
			goto IL_0012;
		}
	}

IL_0031:
	{
		String_t* L_8 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_9 = ___p0;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m4230566705(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_ClassifyChar_m1576648472(__this, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) == ((int32_t)3)))
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0055;
	}

IL_0050:
	{
		int32_t L_13 = ___p0;
		___p0 = ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0055:
	{
		int32_t L_14 = ___p0;
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_0076;
		}
	}
	{
		String_t* L_15 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_16 = ___p0;
		NullCheck(L_15);
		Il2CppChar L_17 = String_get_Chars_m4230566705(L_15, ((int32_t)((int32_t)L_16-(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_ClassifyChar_m1576648472(__this, L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_0050;
		}
	}

IL_0076:
	{
		int32_t L_20 = ___p0;
		return L_20;
	}
}
// System.Void UnityEngine.TextEditor::MoveWordLeft()
extern "C"  void TextEditor_MoveWordLeft_m1630395617 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	TextEditor_t3975561390 * G_B2_0 = NULL;
	TextEditor_t3975561390 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	TextEditor_t3975561390 * G_B3_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			G_B2_0 = __this;
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0023;
	}

IL_001d:
	{
		int32_t L_3 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0023:
	{
		NullCheck(G_B3_1);
		TextEditor_set_cursorIndex_m817296949(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_FindPrevSeperator_m3377670283(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordRight()
extern "C"  void TextEditor_SelectWordRight_m2211539767 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B3_0 = NULL;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TextEditor_t3975561390 * G_B4_1 = NULL;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m2225371862(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m817296949(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordRight_m2225371862(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectWordLeft()
extern "C"  void TextEditor_SelectWordLeft_m3805218630 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	TextEditor_t3975561390 * G_B3_0 = NULL;
	TextEditor_t3975561390 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	TextEditor_t3975561390 * G_B4_1 = NULL;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_3, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m1630395617(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			G_B3_0 = __this;
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		goto IL_005a;
	}

IL_0054:
	{
		int32_t L_8 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		G_B4_0 = L_8;
		G_B4_1 = G_B3_0;
	}

IL_005a:
	{
		NullCheck(G_B4_1);
		TextEditor_set_cursorIndex_m817296949(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		return;
	}

IL_0060:
	{
		int32_t L_9 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_9, /*hidden argument*/NULL);
		TextEditor_MoveWordLeft_m1630395617(__this, /*hidden argument*/NULL);
		int32_t L_10 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineStart()
extern "C"  void TextEditor_ExpandSelectGraphicalLineStart_m752964866 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineStart_m2300210041(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineStart_m2300210041(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ExpandSelectGraphicalLineEnd()
extern "C"  void TextEditor_ExpandSelectGraphicalLineEnd_m2701307649 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_3 = TextEditor_GetGraphicalLineEnd_m1122405818(__this, L_2, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_3, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_002e:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_GetGraphicalLineEnd_m1122405818(__this, L_5, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		TextEditor_set_selectIndex_m2701963187(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineStart()
extern "C"  void TextEditor_SelectGraphicalLineStart_m171383526 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineStart_m2300210041(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectGraphicalLineEnd()
extern "C"  void TextEditor_SelectGraphicalLineEnd_m1545870419 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_GetGraphicalLineEnd_m1122405818(__this, L_0, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphForward()
extern "C"  void TextEditor_SelectParagraphForward_m1117097520 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		String_t* L_3 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m1606060069(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_2) >= ((int32_t)L_4)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_5 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_IndexOfEndOfLine_m156682161(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_8 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)L_9)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_10 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_10, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectParagraphBackward()
extern "C"  void TextEditor_SelectParagraphBackward_m222420272 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0066;
		}
	}
	{
		String_t* L_3 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_5 = String_LastIndexOf_m3157515271(L_3, ((int32_t)10), ((int32_t)((int32_t)L_4-(int32_t)2)), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_7 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_9, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0076;
	}

IL_0066:
	{
		V_1 = 0;
		int32_t L_10 = V_1;
		TextEditor_set_cursorIndex_m817296949(__this, L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		TextEditor_set_selectIndex_m2701963187(__this, L_11, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentWord()
extern "C"  void TextEditor_SelectCurrentWord_m3661678550 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_6 = V_0;
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_6-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_003a:
	{
		int32_t L_7 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_9 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0054:
	{
		int32_t L_10 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_12 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_FindEndOfClassification_m2938898406(__this, L_12, (-1), /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_13, /*hidden argument*/NULL);
		int32_t L_14 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_15 = TextEditor_FindEndOfClassification_m2938898406(__this, L_14, 1, /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_15, /*hidden argument*/NULL);
		goto IL_00b6;
	}

IL_0090:
	{
		int32_t L_16 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_17 = TextEditor_FindEndOfClassification_m2938898406(__this, L_16, 1, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, L_17, /*hidden argument*/NULL);
		int32_t L_18 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_19 = TextEditor_FindEndOfClassification_m2938898406(__this, L_18, (-1), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, L_19, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		__this->set_m_bJustSelected_18((bool)1);
		return;
	}
}
// System.Int32 UnityEngine.TextEditor::FindEndOfClassification(System.Int32,System.Int32)
extern "C"  int32_t TextEditor_FindEndOfClassification_m2938898406 (TextEditor_t3975561390 * __this, int32_t ___p0, int32_t ___dir1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___p0;
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_4 = ___p0;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		int32_t L_5 = ___p0;
		return L_5;
	}

IL_001c:
	{
		String_t* L_6 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_7 = ___p0;
		NullCheck(L_6);
		Il2CppChar L_8 = String_get_Chars_m4230566705(L_6, L_7, /*hidden argument*/NULL);
		int32_t L_9 = TextEditor_ClassifyChar_m1576648472(__this, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
	}

IL_002f:
	{
		int32_t L_10 = ___p0;
		int32_t L_11 = ___dir1;
		___p0 = ((int32_t)((int32_t)L_10+(int32_t)L_11));
		int32_t L_12 = ___p0;
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		return 0;
	}

IL_003d:
	{
		int32_t L_13 = ___p0;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_15 = V_0;
		return L_15;
	}

IL_0046:
	{
		String_t* L_16 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_17 = ___p0;
		NullCheck(L_16);
		Il2CppChar L_18 = String_get_Chars_m4230566705(L_16, L_17, /*hidden argument*/NULL);
		int32_t L_19 = TextEditor_ClassifyChar_m1576648472(__this, L_18, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) == ((int32_t)L_20)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_21 = ___dir1;
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_22 = ___p0;
		return L_22;
	}

IL_0067:
	{
		int32_t L_23 = ___p0;
		return ((int32_t)((int32_t)L_23+(int32_t)1));
	}
}
// System.Void UnityEngine.TextEditor::SelectCurrentParagraph()
extern "C"  void TextEditor_SelectCurrentParagraph_m695527942 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TextEditor_ClearCursorPos_m657928476(__this, /*hidden argument*/NULL);
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m1606060069(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_4 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_5 = TextEditor_IndexOfEndOfLine_m156682161(__this, L_4, /*hidden argument*/NULL);
		TextEditor_set_cursorIndex_m817296949(__this, ((int32_t)((int32_t)L_5+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_0032:
	{
		int32_t L_6 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		String_t* L_7 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_9 = String_LastIndexOf_m3157515271(L_7, ((int32_t)10), ((int32_t)((int32_t)L_8-(int32_t)1)), /*hidden argument*/NULL);
		TextEditor_set_selectIndex_m2701963187(__this, ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffsetIfNeeded()
extern "C"  void TextEditor_UpdateScrollOffsetIfNeeded_m3388264718 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		Event_t3028476042 * L_0 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m2426033198(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0026;
		}
	}
	{
		Event_t3028476042 * L_2 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m2426033198(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)8)))
		{
			goto IL_0026;
		}
	}
	{
		TextEditor_UpdateScrollOffset_m1017266912(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern "C"  void TextEditor_UpdateScrollOffset_m1017266912 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t3681755626  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2243707579 * G_B19_0 = NULL;
	Vector2_t2243707579 * G_B18_0 = NULL;
	float G_B20_0 = 0.0f;
	Vector2_t2243707579 * G_B20_1 = NULL;
	{
		int32_t L_0 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GUIStyle_t1799908754 * L_1 = __this->get_style_2();
		Rect_t3681755626  L_2 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_3 = L_2;
		float L_3 = Rect_get_width_m1138015702((&V_3), /*hidden argument*/NULL);
		Rect_t3681755626  L_4 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_7 = __this->get_m_Content_8();
		int32_t L_8 = V_0;
		NullCheck(L_1);
		Vector2_t2243707579  L_9 = GUIStyle_GetCursorPixelPosition_m2488570694(L_1, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_9);
		GUIStyle_t1799908754 * L_10 = __this->get_style_2();
		NullCheck(L_10);
		RectOffset_t3387826427 * L_11 = GUIStyle_get_padding_m4076916754(L_10, /*hidden argument*/NULL);
		Rect_t3681755626  L_12 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Rect_t3681755626  L_13 = RectOffset_Remove_m1330811977(L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		GUIStyle_t1799908754 * L_14 = __this->get_style_2();
		GUIContent_t4210063000 * L_15 = __this->get_m_Content_8();
		NullCheck(L_14);
		Vector2_t2243707579  L_16 = GUIStyle_CalcSize_m4254746879(L_14, L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = (&V_5)->get_x_1();
		GUIStyle_t1799908754 * L_18 = __this->get_style_2();
		GUIContent_t4210063000 * L_19 = __this->get_m_Content_8();
		Rect_t3681755626  L_20 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_6 = L_20;
		float L_21 = Rect_get_width_m1138015702((&V_6), /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_22 = GUIStyle_CalcHeight_m1685124037(L_18, L_19, L_21, /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_2), L_17, L_22, /*hidden argument*/NULL);
		float L_23 = (&V_2)->get_x_1();
		Rect_t3681755626  L_24 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_7 = L_24;
		float L_25 = Rect_get_width_m1138015702((&V_7), /*hidden argument*/NULL);
		if ((!(((float)L_23) < ((float)L_25))))
		{
			goto IL_00d3;
		}
	}
	{
		Vector2_t2243707579 * L_26 = __this->get_address_of_scrollOffset_7();
		L_26->set_x_1((0.0f));
		goto IL_017a;
	}

IL_00d3:
	{
		bool L_27 = __this->get_m_RevealCursor_12();
		if (!L_27)
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t2243707579 * L_28 = __this->get_address_of_graphicalCursorPos_13();
		float L_29 = L_28->get_x_1();
		Vector2_t2243707579 * L_30 = __this->get_address_of_scrollOffset_7();
		float L_31 = L_30->get_x_1();
		float L_32 = Rect_get_width_m1138015702((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_29+(float)(1.0f)))) > ((float)((float)((float)L_31+(float)L_32))))))
		{
			goto IL_0125;
		}
	}
	{
		Vector2_t2243707579 * L_33 = __this->get_address_of_scrollOffset_7();
		Vector2_t2243707579 * L_34 = __this->get_address_of_graphicalCursorPos_13();
		float L_35 = L_34->get_x_1();
		float L_36 = Rect_get_width_m1138015702((&V_1), /*hidden argument*/NULL);
		L_33->set_x_1(((float)((float)L_35-(float)L_36)));
	}

IL_0125:
	{
		Vector2_t2243707579 * L_37 = __this->get_address_of_graphicalCursorPos_13();
		float L_38 = L_37->get_x_1();
		Vector2_t2243707579 * L_39 = __this->get_address_of_scrollOffset_7();
		float L_40 = L_39->get_x_1();
		GUIStyle_t1799908754 * L_41 = __this->get_style_2();
		NullCheck(L_41);
		RectOffset_t3387826427 * L_42 = GUIStyle_get_padding_m4076916754(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		int32_t L_43 = RectOffset_get_left_m439065308(L_42, /*hidden argument*/NULL);
		if ((!(((float)L_38) < ((float)((float)((float)L_40+(float)(((float)((float)L_43)))))))))
		{
			goto IL_017a;
		}
	}
	{
		Vector2_t2243707579 * L_44 = __this->get_address_of_scrollOffset_7();
		Vector2_t2243707579 * L_45 = __this->get_address_of_graphicalCursorPos_13();
		float L_46 = L_45->get_x_1();
		GUIStyle_t1799908754 * L_47 = __this->get_style_2();
		NullCheck(L_47);
		RectOffset_t3387826427 * L_48 = GUIStyle_get_padding_m4076916754(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_left_m439065308(L_48, /*hidden argument*/NULL);
		L_44->set_x_1(((float)((float)L_46-(float)(((float)((float)L_49))))));
	}

IL_017a:
	{
		float L_50 = (&V_2)->get_y_2();
		float L_51 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_50) < ((float)L_51))))
		{
			goto IL_01a2;
		}
	}
	{
		Vector2_t2243707579 * L_52 = __this->get_address_of_scrollOffset_7();
		L_52->set_y_2((0.0f));
		goto IL_027f;
	}

IL_01a2:
	{
		bool L_53 = __this->get_m_RevealCursor_12();
		if (!L_53)
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t2243707579 * L_54 = __this->get_address_of_graphicalCursorPos_13();
		float L_55 = L_54->get_y_2();
		GUIStyle_t1799908754 * L_56 = __this->get_style_2();
		NullCheck(L_56);
		float L_57 = GUIStyle_get_lineHeight_m2132859383(L_56, /*hidden argument*/NULL);
		Vector2_t2243707579 * L_58 = __this->get_address_of_scrollOffset_7();
		float L_59 = L_58->get_y_2();
		float L_60 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_61 = __this->get_style_2();
		NullCheck(L_61);
		RectOffset_t3387826427 * L_62 = GUIStyle_get_padding_m4076916754(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = RectOffset_get_top_m3629049358(L_62, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_55+(float)L_57))) > ((float)((float)((float)((float)((float)L_59+(float)L_60))+(float)(((float)((float)L_63)))))))))
		{
			goto IL_022a;
		}
	}
	{
		Vector2_t2243707579 * L_64 = __this->get_address_of_scrollOffset_7();
		Vector2_t2243707579 * L_65 = __this->get_address_of_graphicalCursorPos_13();
		float L_66 = L_65->get_y_2();
		float L_67 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_68 = __this->get_style_2();
		NullCheck(L_68);
		RectOffset_t3387826427 * L_69 = GUIStyle_get_padding_m4076916754(L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		int32_t L_70 = RectOffset_get_top_m3629049358(L_69, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_71 = __this->get_style_2();
		NullCheck(L_71);
		float L_72 = GUIStyle_get_lineHeight_m2132859383(L_71, /*hidden argument*/NULL);
		L_64->set_y_2(((float)((float)((float)((float)((float)((float)L_66-(float)L_67))-(float)(((float)((float)L_70)))))+(float)L_72)));
	}

IL_022a:
	{
		Vector2_t2243707579 * L_73 = __this->get_address_of_graphicalCursorPos_13();
		float L_74 = L_73->get_y_2();
		Vector2_t2243707579 * L_75 = __this->get_address_of_scrollOffset_7();
		float L_76 = L_75->get_y_2();
		GUIStyle_t1799908754 * L_77 = __this->get_style_2();
		NullCheck(L_77);
		RectOffset_t3387826427 * L_78 = GUIStyle_get_padding_m4076916754(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = RectOffset_get_top_m3629049358(L_78, /*hidden argument*/NULL);
		if ((!(((float)L_74) < ((float)((float)((float)L_76+(float)(((float)((float)L_79)))))))))
		{
			goto IL_027f;
		}
	}
	{
		Vector2_t2243707579 * L_80 = __this->get_address_of_scrollOffset_7();
		Vector2_t2243707579 * L_81 = __this->get_address_of_graphicalCursorPos_13();
		float L_82 = L_81->get_y_2();
		GUIStyle_t1799908754 * L_83 = __this->get_style_2();
		NullCheck(L_83);
		RectOffset_t3387826427 * L_84 = GUIStyle_get_padding_m4076916754(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_top_m3629049358(L_84, /*hidden argument*/NULL);
		L_80->set_y_2(((float)((float)L_82-(float)(((float)((float)L_85))))));
	}

IL_027f:
	{
		Vector2_t2243707579 * L_86 = __this->get_address_of_scrollOffset_7();
		float L_87 = L_86->get_y_2();
		if ((!(((float)L_87) > ((float)(0.0f)))))
		{
			goto IL_02f1;
		}
	}
	{
		float L_88 = (&V_2)->get_y_2();
		Vector2_t2243707579 * L_89 = __this->get_address_of_scrollOffset_7();
		float L_90 = L_89->get_y_2();
		float L_91 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_88-(float)L_90))) < ((float)L_91))))
		{
			goto IL_02f1;
		}
	}
	{
		Vector2_t2243707579 * L_92 = __this->get_address_of_scrollOffset_7();
		float L_93 = (&V_2)->get_y_2();
		float L_94 = Rect_get_height_m3128694305((&V_1), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_95 = __this->get_style_2();
		NullCheck(L_95);
		RectOffset_t3387826427 * L_96 = GUIStyle_get_padding_m4076916754(L_95, /*hidden argument*/NULL);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_top_m3629049358(L_96, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_98 = __this->get_style_2();
		NullCheck(L_98);
		RectOffset_t3387826427 * L_99 = GUIStyle_get_padding_m4076916754(L_98, /*hidden argument*/NULL);
		NullCheck(L_99);
		int32_t L_100 = RectOffset_get_bottom_m4112328858(L_99, /*hidden argument*/NULL);
		L_92->set_y_2(((float)((float)((float)((float)((float)((float)L_93-(float)L_94))-(float)(((float)((float)L_97)))))-(float)(((float)((float)L_100))))));
	}

IL_02f1:
	{
		Vector2_t2243707579 * L_101 = __this->get_address_of_scrollOffset_7();
		Vector2_t2243707579 * L_102 = __this->get_address_of_scrollOffset_7();
		float L_103 = L_102->get_y_2();
		G_B18_0 = L_101;
		if ((!(((float)L_103) < ((float)(0.0f)))))
		{
			G_B19_0 = L_101;
			goto IL_0316;
		}
	}
	{
		G_B20_0 = (0.0f);
		G_B20_1 = G_B18_0;
		goto IL_0321;
	}

IL_0316:
	{
		Vector2_t2243707579 * L_104 = __this->get_address_of_scrollOffset_7();
		float L_105 = L_104->get_y_2();
		G_B20_0 = L_105;
		G_B20_1 = G_B19_0;
	}

IL_0321:
	{
		G_B20_1->set_y_2(G_B20_0);
		__this->set_m_RevealCursor_12((bool)0);
		return;
	}
}
// System.Void UnityEngine.TextEditor::DrawCursor(System.String)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_DrawCursor_m2394985165_MetadataUsageId;
extern "C"  void TextEditor_DrawCursor_m2394985165 (TextEditor_t3975561390 * __this, String_t* ___newText0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DrawCursor_m2394985165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t3681755626  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		String_t* L_2 = Input_get_compositionString_m636835668(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_005e;
		}
	}
	{
		GUIContent_t4210063000 * L_4 = __this->get_m_Content_8();
		String_t* L_5 = ___newText0;
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_7 = String_Substring_m12482732(L_5, 0, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		String_t* L_8 = Input_get_compositionString_m636835668(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = ___newText0;
		int32_t L_10 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m2032624251(L_9, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, L_7, L_8, L_11, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIContent_set_text_m1170206441(L_4, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		String_t* L_14 = Input_get_compositionString_m636835668(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)L_15));
		goto IL_006a;
	}

IL_005e:
	{
		GUIContent_t4210063000 * L_16 = __this->get_m_Content_8();
		String_t* L_17 = ___newText0;
		NullCheck(L_16);
		GUIContent_set_text_m1170206441(L_16, L_17, /*hidden argument*/NULL);
	}

IL_006a:
	{
		GUIStyle_t1799908754 * L_18 = __this->get_style_2();
		Rect_t3681755626  L_19 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = Rect_get_width_m1138015702((&V_3), /*hidden argument*/NULL);
		Rect_t3681755626  L_21 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		Rect_t3681755626  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Rect__ctor_m1220545469(&L_23, (0.0f), (0.0f), L_20, L_22, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_24 = __this->get_m_Content_8();
		int32_t L_25 = V_1;
		NullCheck(L_18);
		Vector2_t2243707579  L_26 = GUIStyle_GetCursorPixelPosition_m2488570694(L_18, L_23, L_24, L_25, /*hidden argument*/NULL);
		__this->set_graphicalCursorPos_13(L_26);
		GUIStyle_t1799908754 * L_27 = __this->get_style_2();
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = GUIStyle_get_contentOffset_m1904924583(L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		GUIStyle_t1799908754 * L_29 = __this->get_style_2();
		GUIStyle_t1799908754 * L_30 = L_29;
		NullCheck(L_30);
		Vector2_t2243707579  L_31 = GUIStyle_get_contentOffset_m1904924583(L_30, /*hidden argument*/NULL);
		Vector2_t2243707579  L_32 = __this->get_scrollOffset_7();
		Vector2_t2243707579  L_33 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		GUIStyle_set_contentOffset_m2138084868(L_30, L_33, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_34 = __this->get_style_2();
		Vector2_t2243707579  L_35 = __this->get_scrollOffset_7();
		NullCheck(L_34);
		GUIStyle_set_Internal_clipOffset_m118330819(L_34, L_35, /*hidden argument*/NULL);
		Vector2_t2243707579  L_36 = __this->get_graphicalCursorPos_13();
		Rect_t3681755626  L_37 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_5 = L_37;
		float L_38 = Rect_get_x_m1393582490((&V_5), /*hidden argument*/NULL);
		Rect_t3681755626  L_39 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		V_6 = L_39;
		float L_40 = Rect_get_y_m1393582395((&V_6), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_41 = __this->get_style_2();
		NullCheck(L_41);
		float L_42 = GUIStyle_get_lineHeight_m2132859383(L_41, /*hidden argument*/NULL);
		Vector2_t2243707579  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Vector2__ctor_m3067419446(&L_43, L_38, ((float)((float)L_40+(float)L_42)), /*hidden argument*/NULL);
		Vector2_t2243707579  L_44 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_36, L_43, /*hidden argument*/NULL);
		Vector2_t2243707579  L_45 = __this->get_scrollOffset_7();
		Vector2_t2243707579  L_46 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Input_set_compositionCursorPos_m1615567306(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		String_t* L_47 = Input_get_compositionString_m636835668(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = String_get_Length_m1606060069(L_47, /*hidden argument*/NULL);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		GUIStyle_t1799908754 * L_49 = __this->get_style_2();
		Rect_t3681755626  L_50 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_51 = __this->get_m_Content_8();
		int32_t L_52 = __this->get_controlID_1();
		int32_t L_53 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_54 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		String_t* L_55 = Input_get_compositionString_m636835668(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_55);
		int32_t L_56 = String_get_Length_m1606060069(L_55, /*hidden argument*/NULL);
		NullCheck(L_49);
		GUIStyle_DrawWithTextSelection_m2215181902(L_49, L_50, L_51, L_52, L_53, ((int32_t)((int32_t)L_54+(int32_t)L_56)), (bool)1, /*hidden argument*/NULL);
		goto IL_01a4;
	}

IL_017b:
	{
		GUIStyle_t1799908754 * L_57 = __this->get_style_2();
		Rect_t3681755626  L_58 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_59 = __this->get_m_Content_8();
		int32_t L_60 = __this->get_controlID_1();
		int32_t L_61 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_62 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		GUIStyle_DrawWithTextSelection_m3191662157(L_57, L_58, L_59, L_60, L_61, L_62, /*hidden argument*/NULL);
	}

IL_01a4:
	{
		int32_t L_63 = __this->get_m_iAltCursorPos_19();
		if ((((int32_t)L_63) == ((int32_t)(-1))))
		{
			goto IL_01d3;
		}
	}
	{
		GUIStyle_t1799908754 * L_64 = __this->get_style_2();
		Rect_t3681755626  L_65 = TextEditor_get_position_m59912781(__this, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_66 = __this->get_m_Content_8();
		int32_t L_67 = __this->get_controlID_1();
		int32_t L_68 = __this->get_m_iAltCursorPos_19();
		NullCheck(L_64);
		GUIStyle_DrawCursor_m3727336857(L_64, L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
	}

IL_01d3:
	{
		GUIStyle_t1799908754 * L_69 = __this->get_style_2();
		Vector2_t2243707579  L_70 = V_2;
		NullCheck(L_69);
		GUIStyle_set_contentOffset_m2138084868(L_69, L_70, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_71 = __this->get_style_2();
		Vector2_t2243707579  L_72 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		GUIStyle_set_Internal_clipOffset_m118330819(L_71, L_72, /*hidden argument*/NULL);
		GUIContent_t4210063000 * L_73 = __this->get_m_Content_8();
		String_t* L_74 = V_0;
		NullCheck(L_73);
		GUIContent_set_text_m1170206441(L_73, L_74, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::PerformOperation(UnityEngine.TextEditor/TextEditOp)
extern Il2CppClass* TextEditOp_t3138797698_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2933387911;
extern const uint32_t TextEditor_PerformOperation_m2560628023_MetadataUsageId;
extern "C"  bool TextEditor_PerformOperation_m2560628023 (TextEditor_t3975561390 * __this, int32_t ___operation0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_PerformOperation_m2560628023_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set_m_RevealCursor_12((bool)1);
		int32_t L_0 = ___operation0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_00cc;
		}
		if (L_1 == 1)
		{
			goto IL_00d7;
		}
		if (L_1 == 2)
		{
			goto IL_00e2;
		}
		if (L_1 == 3)
		{
			goto IL_00ed;
		}
		if (L_1 == 4)
		{
			goto IL_00f8;
		}
		if (L_1 == 5)
		{
			goto IL_0103;
		}
		if (L_1 == 6)
		{
			goto IL_013a;
		}
		if (L_1 == 7)
		{
			goto IL_0145;
		}
		if (L_1 == 8)
		{
			goto IL_027e;
		}
		if (L_1 == 9)
		{
			goto IL_027e;
		}
		if (L_1 == 10)
		{
			goto IL_0166;
		}
		if (L_1 == 11)
		{
			goto IL_0171;
		}
		if (L_1 == 12)
		{
			goto IL_012f;
		}
		if (L_1 == 13)
		{
			goto IL_010e;
		}
		if (L_1 == 14)
		{
			goto IL_0150;
		}
		if (L_1 == 15)
		{
			goto IL_015b;
		}
		if (L_1 == 16)
		{
			goto IL_0119;
		}
		if (L_1 == 17)
		{
			goto IL_0124;
		}
		if (L_1 == 18)
		{
			goto IL_017c;
		}
		if (L_1 == 19)
		{
			goto IL_0187;
		}
		if (L_1 == 20)
		{
			goto IL_0192;
		}
		if (L_1 == 21)
		{
			goto IL_019d;
		}
		if (L_1 == 22)
		{
			goto IL_01d4;
		}
		if (L_1 == 23)
		{
			goto IL_01df;
		}
		if (L_1 == 24)
		{
			goto IL_027e;
		}
		if (L_1 == 25)
		{
			goto IL_027e;
		}
		if (L_1 == 26)
		{
			goto IL_01ea;
		}
		if (L_1 == 27)
		{
			goto IL_01f5;
		}
		if (L_1 == 28)
		{
			goto IL_0216;
		}
		if (L_1 == 29)
		{
			goto IL_0221;
		}
		if (L_1 == 30)
		{
			goto IL_01b3;
		}
		if (L_1 == 31)
		{
			goto IL_01a8;
		}
		if (L_1 == 32)
		{
			goto IL_01be;
		}
		if (L_1 == 33)
		{
			goto IL_01c9;
		}
		if (L_1 == 34)
		{
			goto IL_020b;
		}
		if (L_1 == 35)
		{
			goto IL_0200;
		}
		if (L_1 == 36)
		{
			goto IL_022c;
		}
		if (L_1 == 37)
		{
			goto IL_0233;
		}
		if (L_1 == 38)
		{
			goto IL_0269;
		}
		if (L_1 == 39)
		{
			goto IL_0277;
		}
		if (L_1 == 40)
		{
			goto IL_0270;
		}
		if (L_1 == 41)
		{
			goto IL_023a;
		}
		if (L_1 == 42)
		{
			goto IL_0241;
		}
		if (L_1 == 43)
		{
			goto IL_024c;
		}
		if (L_1 == 44)
		{
			goto IL_0253;
		}
		if (L_1 == 45)
		{
			goto IL_025e;
		}
	}
	{
		goto IL_027e;
	}

IL_00cc:
	{
		TextEditor_MoveLeft_m2907864443(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00d7:
	{
		TextEditor_MoveRight_m2990766682(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00e2:
	{
		TextEditor_MoveUp_m799146301(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00ed:
	{
		TextEditor_MoveDown_m3610827042(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_00f8:
	{
		TextEditor_MoveLineStart_m3866297846(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0103:
	{
		TextEditor_MoveLineEnd_m1288595337(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_010e:
	{
		TextEditor_MoveWordRight_m2225371862(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0119:
	{
		TextEditor_MoveToStartOfNextWord_m893401463(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0124:
	{
		TextEditor_MoveToEndOfPreviousWord_m119836618(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_012f:
	{
		TextEditor_MoveWordLeft_m1630395617(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_013a:
	{
		TextEditor_MoveTextStart_m926646761(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0145:
	{
		TextEditor_MoveTextEnd_m158107192(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0150:
	{
		TextEditor_MoveParagraphForward_m2156700157(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_015b:
	{
		TextEditor_MoveParagraphBackward_m3489571549(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0166:
	{
		TextEditor_MoveGraphicalLineStart_m3705073185(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0171:
	{
		TextEditor_MoveGraphicalLineEnd_m1918309850(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_017c:
	{
		TextEditor_SelectLeft_m1025549794(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0187:
	{
		TextEditor_SelectRight_m2396682109(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0192:
	{
		TextEditor_SelectUp_m3826611706(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_019d:
	{
		TextEditor_SelectDown_m322570859(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01a8:
	{
		TextEditor_SelectWordRight_m2211539767(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01b3:
	{
		TextEditor_SelectWordLeft_m3805218630(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01be:
	{
		TextEditor_SelectToEndOfPreviousWord_m3931162275(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01c9:
	{
		TextEditor_SelectToStartOfNextWord_m2812882512(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01d4:
	{
		TextEditor_SelectTextStart_m3661773790(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01df:
	{
		TextEditor_SelectTextEnd_m1542698735(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01ea:
	{
		TextEditor_ExpandSelectGraphicalLineStart_m752964866(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_01f5:
	{
		TextEditor_ExpandSelectGraphicalLineEnd_m2701307649(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0200:
	{
		TextEditor_SelectParagraphForward_m1117097520(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_020b:
	{
		TextEditor_SelectParagraphBackward_m222420272(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0216:
	{
		TextEditor_SelectGraphicalLineStart_m171383526(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0221:
	{
		TextEditor_SelectGraphicalLineEnd_m1545870419(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_022c:
	{
		bool L_2 = TextEditor_Delete_m2400679556(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0233:
	{
		bool L_3 = TextEditor_Backspace_m3475645052(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_023a:
	{
		bool L_4 = TextEditor_Cut_m487430403(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_0241:
	{
		TextEditor_Copy_m385124734(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_024c:
	{
		bool L_5 = TextEditor_Paste_m3893796664(__this, /*hidden argument*/NULL);
		return L_5;
	}

IL_0253:
	{
		TextEditor_SelectAll_m435039312(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_025e:
	{
		TextEditor_SelectNone_m3992543199(__this, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0269:
	{
		bool L_6 = TextEditor_DeleteWordBack_m2547107185(__this, /*hidden argument*/NULL);
		return L_6;
	}

IL_0270:
	{
		bool L_7 = TextEditor_DeleteLineBack_m1837804025(__this, /*hidden argument*/NULL);
		return L_7;
	}

IL_0277:
	{
		bool L_8 = TextEditor_DeleteWordForward_m3131302373(__this, /*hidden argument*/NULL);
		return L_8;
	}

IL_027e:
	{
		int32_t L_9 = ___operation0;
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(TextEditOp_t3138797698_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2933387911, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		goto IL_0298;
	}

IL_0298:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.TextEditor::SaveBackup()
extern "C"  void TextEditor_SaveBackup_m4030486676 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		__this->set_oldText_20(L_0);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		__this->set_oldPos_21(L_1);
		int32_t L_2 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		__this->set_oldSelectPos_22(L_2);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::Cut()
extern "C"  bool TextEditor_Cut_m487430403 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isPasswordField_5();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		TextEditor_Copy_m385124734(__this, /*hidden argument*/NULL);
		bool L_1 = TextEditor_DeleteSelection_m501755976(__this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_Copy_m385124734_MetadataUsageId;
extern "C"  void TextEditor_Copy_m385124734 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_Copy_m385124734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t L_0 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_1 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_isPasswordField_5();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_4 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0053;
		}
	}
	{
		String_t* L_5 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_6 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_7 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_8 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_9 = String_Substring_m12482732(L_5, L_6, ((int32_t)((int32_t)L_7-(int32_t)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0072;
	}

IL_0053:
	{
		String_t* L_10 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		int32_t L_11 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		int32_t L_12 = TextEditor_get_cursorIndex_m486786154(__this, /*hidden argument*/NULL);
		int32_t L_13 = TextEditor_get_selectIndex_m1435337632(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_14 = String_Substring_m12482732(L_10, L_11, ((int32_t)((int32_t)L_12-(int32_t)L_13)), /*hidden argument*/NULL);
		V_0 = L_14;
	}

IL_0072:
	{
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		GUIUtility_set_systemCopyBuffer_m2040945785(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern const uint32_t TextEditor_ReplaceNewlinesWithSpaces_m2633375220_MetadataUsageId;
extern "C"  String_t* TextEditor_ReplaceNewlinesWithSpaces_m2633375220 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ReplaceNewlinesWithSpaces_m2633375220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		NullCheck(L_0);
		String_t* L_1 = String_Replace_m1941156251(L_0, _stringLiteral2162321587, _stringLiteral372029310, /*hidden argument*/NULL);
		___value0 = L_1;
		String_t* L_2 = ___value0;
		NullCheck(L_2);
		String_t* L_3 = String_Replace_m534438427(L_2, ((int32_t)10), ((int32_t)32), /*hidden argument*/NULL);
		___value0 = L_3;
		String_t* L_4 = ___value0;
		NullCheck(L_4);
		String_t* L_5 = String_Replace_m534438427(L_4, ((int32_t)13), ((int32_t)32), /*hidden argument*/NULL);
		___value0 = L_5;
		String_t* L_6 = ___value0;
		return L_6;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_Paste_m3893796664_MetadataUsageId;
extern "C"  bool TextEditor_Paste_m3893796664 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_Paste_m3893796664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m396434174(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		bool L_4 = __this->get_multiline_3();
		if (L_4)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_5 = V_0;
		String_t* L_6 = TextEditor_ReplaceNewlinesWithSpaces_m2633375220(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0028:
	{
		String_t* L_7 = V_0;
		TextEditor_ReplaceSelection_m356220427(__this, L_7, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.TextEditor::MapKey(System.String,UnityEngine.TextEditor/TextEditOp)
extern Il2CppClass* TextEditor_t3975561390_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m313632737_MethodInfo_var;
extern const uint32_t TextEditor_MapKey_m3330329226_MetadataUsageId;
extern "C"  void TextEditor_MapKey_m3330329226 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___action1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_MapKey_m3330329226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1747193563 * L_0 = ((TextEditor_t3975561390_StaticFields*)TextEditor_t3975561390_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		String_t* L_1 = ___key0;
		Event_t3028476042 * L_2 = Event_KeyboardEvent_m1089796218(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___action1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m313632737(L_0, L_2, L_3, /*hidden argument*/Dictionary_2_set_Item_m313632737_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.TextEditor::InitKeyActions()
extern Il2CppClass* TextEditor_t3975561390_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1747193563_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1759590410_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral1543969241;
extern Il2CppCodeGenString* _stringLiteral1367190538;
extern Il2CppCodeGenString* _stringLiteral2986241634;
extern Il2CppCodeGenString* _stringLiteral1822878491;
extern Il2CppCodeGenString* _stringLiteral2309165382;
extern Il2CppCodeGenString* _stringLiteral3873364309;
extern Il2CppCodeGenString* _stringLiteral1381955065;
extern Il2CppCodeGenString* _stringLiteral4226874623;
extern Il2CppCodeGenString* _stringLiteral1918873406;
extern Il2CppCodeGenString* _stringLiteral696030431;
extern Il2CppCodeGenString* _stringLiteral2986369013;
extern Il2CppCodeGenString* _stringLiteral1823005134;
extern Il2CppCodeGenString* _stringLiteral2986236317;
extern Il2CppCodeGenString* _stringLiteral1822872918;
extern Il2CppCodeGenString* _stringLiteral2309165219;
extern Il2CppCodeGenString* _stringLiteral3873359248;
extern Il2CppCodeGenString* _stringLiteral2986239580;
extern Il2CppCodeGenString* _stringLiteral1822876181;
extern Il2CppCodeGenString* _stringLiteral2309165312;
extern Il2CppCodeGenString* _stringLiteral3873362383;
extern Il2CppCodeGenString* _stringLiteral3860067884;
extern Il2CppCodeGenString* _stringLiteral4136219612;
extern Il2CppCodeGenString* _stringLiteral1898470270;
extern Il2CppCodeGenString* _stringLiteral2997196425;
extern Il2CppCodeGenString* _stringLiteral2617964842;
extern Il2CppCodeGenString* _stringLiteral1145603351;
extern Il2CppCodeGenString* _stringLiteral816452294;
extern Il2CppCodeGenString* _stringLiteral2016193329;
extern Il2CppCodeGenString* _stringLiteral4283255346;
extern Il2CppCodeGenString* _stringLiteral164600255;
extern Il2CppCodeGenString* _stringLiteral4232236833;
extern Il2CppCodeGenString* _stringLiteral1137010572;
extern Il2CppCodeGenString* _stringLiteral3900173795;
extern Il2CppCodeGenString* _stringLiteral690135962;
extern Il2CppCodeGenString* _stringLiteral690345940;
extern Il2CppCodeGenString* _stringLiteral1187738801;
extern Il2CppCodeGenString* _stringLiteral3822513822;
extern Il2CppCodeGenString* _stringLiteral2706768575;
extern Il2CppCodeGenString* _stringLiteral287061536;
extern Il2CppCodeGenString* _stringLiteral1900199644;
extern Il2CppCodeGenString* _stringLiteral1093630590;
extern Il2CppCodeGenString* _stringLiteral3419229418;
extern Il2CppCodeGenString* _stringLiteral690346063;
extern Il2CppCodeGenString* _stringLiteral3015944891;
extern Il2CppCodeGenString* _stringLiteral2184547331;
extern Il2CppCodeGenString* _stringLiteral1912808569;
extern Il2CppCodeGenString* _stringLiteral1916366392;
extern Il2CppCodeGenString* _stringLiteral1414245067;
extern Il2CppCodeGenString* _stringLiteral3068682171;
extern Il2CppCodeGenString* _stringLiteral2309169211;
extern Il2CppCodeGenString* _stringLiteral3873491464;
extern Il2CppCodeGenString* _stringLiteral2188927163;
extern Il2CppCodeGenString* _stringLiteral2057332929;
extern Il2CppCodeGenString* _stringLiteral1187738924;
extern Il2CppCodeGenString* _stringLiteral3822513945;
extern Il2CppCodeGenString* _stringLiteral2706768698;
extern Il2CppCodeGenString* _stringLiteral2184722656;
extern Il2CppCodeGenString* _stringLiteral736181957;
extern Il2CppCodeGenString* _stringLiteral731988378;
extern const uint32_t TextEditor_InitKeyActions_m3270054209_MetadataUsageId;
extern "C"  void TextEditor_InitKeyActions_m3270054209 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_InitKeyActions_m3270054209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1747193563 * L_0 = ((TextEditor_t3975561390_StaticFields*)TextEditor_t3975561390_il2cpp_TypeInfo_var->static_fields)->get_s_Keyactions_23();
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		Dictionary_2_t1747193563 * L_1 = (Dictionary_2_t1747193563 *)il2cpp_codegen_object_new(Dictionary_2_t1747193563_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1759590410(L_1, /*hidden argument*/Dictionary_2__ctor_m1759590410_MethodInfo_var);
		((TextEditor_t3975561390_StaticFields*)TextEditor_t3975561390_il2cpp_TypeInfo_var->static_fields)->set_s_Keyactions_23(L_1);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3423761043, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral109637592, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1543969241, 2, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1367190538, 3, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2986241634, ((int32_t)18), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1822878491, ((int32_t)19), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2309165382, ((int32_t)20), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3873364309, ((int32_t)21), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1381955065, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral4226874623, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1918873406, ((int32_t)37), /*hidden argument*/NULL);
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)4)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_4 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_5 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_0290;
		}
	}
	{
		String_t* L_6 = SystemInfo_get_operatingSystem_m2575097876(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = String_StartsWith_m1841920685(L_6, _stringLiteral696030431, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0290;
		}
	}

IL_00d5:
	{
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2986369013, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1823005134, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2986236317, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1822872918, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2309165219, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3873359248, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2986239580, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1822876181, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2309165312, 6, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3873362383, 7, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3860067884, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral4136219612, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1898470270, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2997196425, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2617964842, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1145603351, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral816452294, ((int32_t)30), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2016193329, ((int32_t)31), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral4283255346, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral164600255, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral4232236833, ((int32_t)26), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1137010572, ((int32_t)27), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3900173795, ((int32_t)22), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral690135962, ((int32_t)23), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral690345940, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1187738801, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3822513822, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2706768575, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral287061536, ((int32_t)36), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1900199644, ((int32_t)37), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1093630590, 0, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3419229418, 1, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral690346063, 4, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3015944891, 5, /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2184547331, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1912808569, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1916366392, ((int32_t)40), /*hidden argument*/NULL);
		goto IL_03c8;
	}

IL_0290:
	{
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1414245067, ((int32_t)10), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3068682171, ((int32_t)11), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2986239580, ((int32_t)12), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1822876181, ((int32_t)13), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2309165312, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3873362383, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2986369013, ((int32_t)17), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1823005134, ((int32_t)16), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2309169211, ((int32_t)15), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3873491464, ((int32_t)14), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1898470270, ((int32_t)32), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2997196425, ((int32_t)33), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2617964842, ((int32_t)34), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1145603351, ((int32_t)35), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3860067884, ((int32_t)28), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral4136219612, ((int32_t)29), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2188927163, ((int32_t)39), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2057332929, ((int32_t)38), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1916366392, ((int32_t)40), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral690346063, ((int32_t)44), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral1187738924, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral3822513945, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2706768698, ((int32_t)43), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral2184722656, ((int32_t)41), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral736181957, ((int32_t)42), /*hidden argument*/NULL);
		TextEditor_MapKey_m3330329226(NULL /*static, unused*/, _stringLiteral731988378, ((int32_t)43), /*hidden argument*/NULL);
	}

IL_03c8:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::DetectFocusChange()
extern Il2CppClass* GUIUtility_t3275770671_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_DetectFocusChange_m2483493478_MetadataUsageId;
extern "C"  void TextEditor_DetectFocusChange_m2483493478 (TextEditor_t3975561390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_DetectFocusChange_m2483493478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_HasFocus_6();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = __this->get_controlID_1();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_2 = GUIUtility_get_keyboardControl_m2418463643(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		TextEditor_OnLostFocus_m1193078298(__this, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = __this->get_m_HasFocus_6();
		if (L_3)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_4 = __this->get_controlID_1();
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t3275770671_il2cpp_TypeInfo_var);
		int32_t L_5 = GUIUtility_get_keyboardControl_m2418463643(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0042;
		}
	}
	{
		TextEditor_OnFocus_m4031289844(__this, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClampTextIndex(System.Int32&)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor_ClampTextIndex_m3072502406_MetadataUsageId;
extern "C"  void TextEditor_ClampTextIndex_m3072502406 (TextEditor_t3975561390 * __this, int32_t* ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextEditor_ClampTextIndex_m3072502406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t* L_0 = ___index0;
		int32_t* L_1 = ___index0;
		String_t* L_2 = TextEditor_get_text_m1324740216(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, (*((int32_t*)L_1)), 0, L_3, /*hidden argument*/NULL);
		*((int32_t*)(L_0)) = (int32_t)L_4;
		return;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_CompareColors_m991725620_MetadataUsageId;
extern "C"  bool TextGenerationSettings_CompareColors_m991725620 (TextGenerationSettings_t2543476768 * __this, Color_t2020392075  ___left0, Color_t2020392075  ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_CompareColors_m991725620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___left0)->get_r_0();
		float L_1 = (&___right1)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005d;
		}
	}
	{
		float L_3 = (&___left0)->get_g_1();
		float L_4 = (&___right1)->get_g_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		float L_6 = (&___left0)->get_b_2();
		float L_7 = (&___right1)->get_b_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005d;
		}
	}
	{
		float L_9 = (&___left0)->get_a_3();
		float L_10 = (&___right1)->get_a_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_11 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_005e;
	}

IL_005d:
	{
		G_B5_0 = 0;
	}

IL_005e:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool TextGenerationSettings_CompareColors_m991725620_AdjustorThunk (Il2CppObject * __this, Color_t2020392075  ___left0, Color_t2020392075  ___right1, const MethodInfo* method)
{
	TextGenerationSettings_t2543476768 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t2543476768 *>(__this + 1);
	return TextGenerationSettings_CompareColors_m991725620(_thisAdjusted, ___left0, ___right1, method);
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_CompareVector2_m592645251_MetadataUsageId;
extern "C"  bool TextGenerationSettings_CompareVector2_m592645251 (TextGenerationSettings_t2543476768 * __this, Vector2_t2243707579  ___left0, Vector2_t2243707579  ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_CompareVector2_m592645251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		float L_0 = (&___left0)->get_x_1();
		float L_1 = (&___right1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = (&___left0)->get_y_2();
		float L_4 = (&___right1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool TextGenerationSettings_CompareVector2_m592645251_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___left0, Vector2_t2243707579  ___right1, const MethodInfo* method)
{
	TextGenerationSettings_t2543476768 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t2543476768 *>(__this + 1);
	return TextGenerationSettings_CompareVector2_m592645251(_thisAdjusted, ___left0, ___right1, method);
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_Equals_m3944651893_MetadataUsageId;
extern "C"  bool TextGenerationSettings_Equals_m3944651893 (TextGenerationSettings_t2543476768 * __this, TextGenerationSettings_t2543476768  ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_Equals_m3944651893_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B21_0 = 0;
	{
		Color_t2020392075  L_0 = __this->get_color_1();
		Color_t2020392075  L_1 = (&___other0)->get_color_1();
		bool L_2 = TextGenerationSettings_CompareColors_m991725620(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_3 = __this->get_fontSize_2();
		int32_t L_4 = (&___other0)->get_fontSize_2();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0186;
		}
	}
	{
		float L_5 = __this->get_scaleFactor_5();
		float L_6 = (&___other0)->get_scaleFactor_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_8 = __this->get_resizeTextMinSize_10();
		int32_t L_9 = (&___other0)->get_resizeTextMinSize_10();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_10 = __this->get_resizeTextMaxSize_11();
		int32_t L_11 = (&___other0)->get_resizeTextMaxSize_11();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0186;
		}
	}
	{
		float L_12 = __this->get_lineSpacing_3();
		float L_13 = (&___other0)->get_lineSpacing_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_14 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_15 = __this->get_fontStyle_6();
		int32_t L_16 = (&___other0)->get_fontStyle_6();
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_17 = __this->get_richText_4();
		bool L_18 = (&___other0)->get_richText_4();
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_19 = __this->get_textAnchor_7();
		int32_t L_20 = (&___other0)->get_textAnchor_7();
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_21 = __this->get_alignByGeometry_8();
		bool L_22 = (&___other0)->get_alignByGeometry_8();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_23 = __this->get_resizeTextForBestFit_9();
		bool L_24 = (&___other0)->get_resizeTextForBestFit_9();
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_25 = __this->get_resizeTextMinSize_10();
		int32_t L_26 = (&___other0)->get_resizeTextMinSize_10();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_27 = __this->get_resizeTextMaxSize_11();
		int32_t L_28 = (&___other0)->get_resizeTextMaxSize_11();
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_29 = __this->get_resizeTextForBestFit_9();
		bool L_30 = (&___other0)->get_resizeTextForBestFit_9();
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_0186;
		}
	}
	{
		bool L_31 = __this->get_updateBounds_12();
		bool L_32 = (&___other0)->get_updateBounds_12();
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_33 = __this->get_horizontalOverflow_14();
		int32_t L_34 = (&___other0)->get_horizontalOverflow_14();
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0186;
		}
	}
	{
		int32_t L_35 = __this->get_verticalOverflow_13();
		int32_t L_36 = (&___other0)->get_verticalOverflow_13();
		if ((!(((uint32_t)L_35) == ((uint32_t)L_36))))
		{
			goto IL_0186;
		}
	}
	{
		Vector2_t2243707579  L_37 = __this->get_generationExtents_15();
		Vector2_t2243707579  L_38 = (&___other0)->get_generationExtents_15();
		bool L_39 = TextGenerationSettings_CompareVector2_m592645251(__this, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0186;
		}
	}
	{
		Vector2_t2243707579  L_40 = __this->get_pivot_16();
		Vector2_t2243707579  L_41 = (&___other0)->get_pivot_16();
		bool L_42 = TextGenerationSettings_CompareVector2_m592645251(__this, L_40, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0186;
		}
	}
	{
		Font_t4239498691 * L_43 = __this->get_font_0();
		Font_t4239498691 * L_44 = (&___other0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_45 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)(L_45));
		goto IL_0187;
	}

IL_0186:
	{
		G_B21_0 = 0;
	}

IL_0187:
	{
		return (bool)G_B21_0;
	}
}
extern "C"  bool TextGenerationSettings_Equals_m3944651893_AdjustorThunk (Il2CppObject * __this, TextGenerationSettings_t2543476768  ___other0, const MethodInfo* method)
{
	TextGenerationSettings_t2543476768 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t2543476768 *>(__this + 1);
	return TextGenerationSettings_Equals_m3944651893(_thisAdjusted, ___other0, method);
}
// Conversion methods for marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke(const TextGenerationSettings_t2543476768& unmarshaled, TextGenerationSettings_t2543476768_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke_back(const TextGenerationSettings_t2543476768_marshaled_pinvoke& marshaled, TextGenerationSettings_t2543476768& unmarshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t2543476768_marshal_pinvoke_cleanup(TextGenerationSettings_t2543476768_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t2543476768_marshal_com(const TextGenerationSettings_t2543476768& unmarshaled, TextGenerationSettings_t2543476768_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
extern "C" void TextGenerationSettings_t2543476768_marshal_com_back(const TextGenerationSettings_t2543476768_marshaled_com& marshaled, TextGenerationSettings_t2543476768& unmarshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t2543476768_marshal_com_cleanup(TextGenerationSettings_t2543476768_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C"  void TextGenerator__ctor_m11880227 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m1169691060(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern Il2CppClass* List_1_t573379950_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2425757932_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2990399006_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2168280176_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3698273726_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2766376432_MethodInfo_var;
extern const uint32_t TextGenerator__ctor_m1169691060_MetadataUsageId;
extern "C"  void TextGenerator__ctor_m1169691060 (TextGenerator_t647235000 * __this, int32_t ___initialCapacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator__ctor_m1169691060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity0;
		List_1_t573379950 * L_1 = (List_1_t573379950 *)il2cpp_codegen_object_new(List_1_t573379950_il2cpp_TypeInfo_var);
		List_1__ctor_m2168280176(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m2168280176_MethodInfo_var);
		__this->set_m_Verts_5(L_1);
		int32_t L_2 = ___initialCapacity0;
		List_1_t2425757932 * L_3 = (List_1_t2425757932 *)il2cpp_codegen_object_new(List_1_t2425757932_il2cpp_TypeInfo_var);
		List_1__ctor_m3698273726(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m3698273726_MethodInfo_var);
		__this->set_m_Characters_6(L_3);
		List_1_t2990399006 * L_4 = (List_1_t2990399006 *)il2cpp_codegen_object_new(List_1_t2990399006_il2cpp_TypeInfo_var);
		List_1__ctor_m2766376432(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m2766376432_MethodInfo_var);
		__this->set_m_Lines_7(L_4);
		TextGenerator_Init_m293029225(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C"  void TextGenerator_System_IDisposable_Dispose_m1042601388 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m1755131202(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_Finalize_m4242493229_MetadataUsageId;
extern "C"  void TextGenerator_Finalize_m4242493229 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_Finalize_m4242493229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4147925158;
extern Il2CppCodeGenString* _stringLiteral1121739408;
extern const uint32_t TextGenerator_ValidatedSettings_m1640214759_MetadataUsageId;
extern "C"  TextGenerationSettings_t2543476768  TextGenerator_ValidatedSettings_m1640214759 (TextGenerator_t647235000 * __this, TextGenerationSettings_t2543476768  ___settings0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_ValidatedSettings_m1640214759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t4239498691 * L_0 = (&___settings0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t4239498691 * L_2 = (&___settings0)->get_font_0();
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m1803576936(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t2543476768  L_4 = ___settings0;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = (&___settings0)->get_fontSize_2();
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = (&___settings0)->get_fontStyle_6();
		if (!L_6)
		{
			goto IL_0085;
		}
	}

IL_003d:
	{
		Font_t4239498691 * L_7 = (&___settings0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0075;
		}
	}
	{
		Font_t4239498691 * L_9 = (&___settings0)->get_font_0();
		ObjectU5BU5D_t3614634134* L_10 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Font_t4239498691 * L_11 = (&___settings0)->get_font_0();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m2079638459(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarningFormat_m79553173(NULL /*static, unused*/, L_9, _stringLiteral4147925158, L_10, /*hidden argument*/NULL);
	}

IL_0075:
	{
		(&___settings0)->set_fontSize_2(0);
		(&___settings0)->set_fontStyle_6(0);
	}

IL_0085:
	{
		bool L_13 = (&___settings0)->get_resizeTextForBestFit_9();
		if (!L_13)
		{
			goto IL_00d1;
		}
	}
	{
		Font_t4239498691 * L_14 = (&___settings0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00c9;
		}
	}
	{
		Font_t4239498691 * L_16 = (&___settings0)->get_font_0();
		ObjectU5BU5D_t3614634134* L_17 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Font_t4239498691 * L_18 = (&___settings0)->get_font_0();
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m2079638459(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarningFormat_m79553173(NULL /*static, unused*/, L_16, _stringLiteral1121739408, L_17, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		(&___settings0)->set_resizeTextForBestFit_9((bool)0);
	}

IL_00d1:
	{
		TextGenerationSettings_t2543476768  L_20 = ___settings0;
		return L_20;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C"  void TextGenerator_Invalidate_m3217235344 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	{
		__this->set_m_HasGenerated_3((bool)0);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C"  void TextGenerator_GetCharacters_m2554471692 (TextGenerator_t647235000 * __this, List_1_t2425757932 * ___characters0, const MethodInfo* method)
{
	{
		List_1_t2425757932 * L_0 = ___characters0;
		TextGenerator_GetCharactersInternal_m1809798004(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C"  void TextGenerator_GetLines_m1873042509 (TextGenerator_t647235000 * __this, List_1_t2990399006 * ___lines0, const MethodInfo* method)
{
	{
		List_1_t2990399006 * L_0 = ___lines0;
		TextGenerator_GetLinesInternal_m2194096229(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C"  void TextGenerator_GetVertices_m4090838925 (TextGenerator_t647235000 * __this, List_1_t573379950 * ___vertices0, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___vertices0;
		TextGenerator_GetVerticesInternal_m393921805(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C"  float TextGenerator_GetPreferredWidth_m3480985839 (TextGenerator_t647235000 * __this, String_t* ___str0, TextGenerationSettings_t2543476768  ___settings1, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&___settings1)->set_horizontalOverflow_14(1);
		(&___settings1)->set_verticalOverflow_13(1);
		(&___settings1)->set_updateBounds_12((bool)1);
		String_t* L_0 = ___str0;
		TextGenerationSettings_t2543476768  L_1 = ___settings1;
		TextGenerator_Populate_m4139823822(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = TextGenerator_get_rectExtents_m1925360043(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C"  float TextGenerator_GetPreferredHeight_m274483688 (TextGenerator_t647235000 * __this, String_t* ___str0, TextGenerationSettings_t2543476768  ___settings1, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&___settings1)->set_verticalOverflow_13(1);
		(&___settings1)->set_updateBounds_12((bool)1);
		String_t* L_0 = ___str0;
		TextGenerationSettings_t2543476768  L_1 = ___settings1;
		TextGenerator_Populate_m4139823822(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = TextGenerator_get_rectExtents_m1925360043(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m3128694305((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_Populate_m4139823822_MetadataUsageId;
extern "C"  bool TextGenerator_Populate_m4139823822 (TextGenerator_t647235000 * __this, String_t* ___str0, TextGenerationSettings_t2543476768  ___settings1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_Populate_m4139823822_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_HasGenerated_3();
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str0;
		String_t* L_2 = __this->get_m_LastString_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t2543476768  L_4 = __this->get_m_LastSettings_2();
		bool L_5 = TextGenerationSettings_Equals_m3944651893((&___settings1), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = __this->get_m_LastValid_4();
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str0;
		TextGenerationSettings_t2543476768  L_8 = ___settings1;
		bool L_9 = TextGenerator_PopulateAlways_m1004549719(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C"  bool TextGenerator_PopulateAlways_m1004549719 (TextGenerator_t647235000 * __this, String_t* ___str0, TextGenerationSettings_t2543476768  ___settings1, const MethodInfo* method)
{
	TextGenerationSettings_t2543476768  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___str0;
		__this->set_m_LastString_1(L_0);
		__this->set_m_HasGenerated_3((bool)1);
		__this->set_m_CachedVerts_8((bool)0);
		__this->set_m_CachedCharacters_9((bool)0);
		__this->set_m_CachedLines_10((bool)0);
		TextGenerationSettings_t2543476768  L_1 = ___settings1;
		__this->set_m_LastSettings_2(L_1);
		TextGenerationSettings_t2543476768  L_2 = ___settings1;
		TextGenerationSettings_t2543476768  L_3 = TextGenerator_ValidatedSettings_m1640214759(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str0;
		Font_t4239498691 * L_5 = (&V_0)->get_font_0();
		Color_t2020392075  L_6 = (&V_0)->get_color_1();
		int32_t L_7 = (&V_0)->get_fontSize_2();
		float L_8 = (&V_0)->get_scaleFactor_5();
		float L_9 = (&V_0)->get_lineSpacing_3();
		int32_t L_10 = (&V_0)->get_fontStyle_6();
		bool L_11 = (&V_0)->get_richText_4();
		bool L_12 = (&V_0)->get_resizeTextForBestFit_9();
		int32_t L_13 = (&V_0)->get_resizeTextMinSize_10();
		int32_t L_14 = (&V_0)->get_resizeTextMaxSize_11();
		int32_t L_15 = (&V_0)->get_verticalOverflow_13();
		int32_t L_16 = (&V_0)->get_horizontalOverflow_14();
		bool L_17 = (&V_0)->get_updateBounds_12();
		int32_t L_18 = (&V_0)->get_textAnchor_7();
		Vector2_t2243707579  L_19 = (&V_0)->get_generationExtents_15();
		Vector2_t2243707579  L_20 = (&V_0)->get_pivot_16();
		bool L_21 = (&V_0)->get_generateOutOfBounds_17();
		bool L_22 = (&V_0)->get_alignByGeometry_8();
		bool L_23 = TextGenerator_Populate_Internal_m405821495(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		__this->set_m_LastValid_4(L_23);
		bool L_24 = __this->get_m_LastValid_4();
		return L_24;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C"  Il2CppObject* TextGenerator_get_verts_m3124035267 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CachedVerts_8();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t573379950 * L_1 = __this->get_m_Verts_5();
		TextGenerator_GetVertices_m4090838925(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedVerts_8((bool)1);
	}

IL_001e:
	{
		List_1_t573379950 * L_2 = __this->get_m_Verts_5();
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C"  Il2CppObject* TextGenerator_get_characters_m459389093 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CachedCharacters_9();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t2425757932 * L_1 = __this->get_m_Characters_6();
		TextGenerator_GetCharacters_m2554471692(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedCharacters_9((bool)1);
	}

IL_001e:
	{
		List_1_t2425757932 * L_2 = __this->get_m_Characters_6();
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C"  Il2CppObject* TextGenerator_get_lines_m916366224 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CachedLines_10();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t2990399006 * L_1 = __this->get_m_Lines_7();
		TextGenerator_GetLines_m1873042509(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedLines_10((bool)1);
	}

IL_001e:
	{
		List_1_t2990399006 * L_2 = __this->get_m_Lines_7();
		return L_2;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C"  void TextGenerator_Init_m293029225 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m293029225_ftn) (TextGenerator_t647235000 *);
	static TextGenerator_Init_m293029225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m293029225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C"  void TextGenerator_Dispose_cpp_m1755131202 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m1755131202_ftn) (TextGenerator_t647235000 *);
	static TextGenerator_Dispose_cpp_m1755131202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m1755131202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean,System.Boolean)
extern "C"  bool TextGenerator_Populate_Internal_m405821495 (TextGenerator_t647235000 * __this, String_t* ___str0, Font_t4239498691 * ___font1, Color_t2020392075  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, Vector2_t2243707579  ___extents15, Vector2_t2243707579  ___pivot16, bool ___generateOutOfBounds17, bool ___alignByGeometry18, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str0;
		Font_t4239498691 * L_1 = ___font1;
		Color_t2020392075  L_2 = ___color2;
		int32_t L_3 = ___fontSize3;
		float L_4 = ___scaleFactor4;
		float L_5 = ___lineSpacing5;
		int32_t L_6 = ___style6;
		bool L_7 = ___richText7;
		bool L_8 = ___resizeTextForBestFit8;
		int32_t L_9 = ___resizeTextMinSize9;
		int32_t L_10 = ___resizeTextMaxSize10;
		int32_t L_11 = ___verticalOverFlow11;
		int32_t L_12 = ___horizontalOverflow12;
		bool L_13 = ___updateBounds13;
		int32_t L_14 = ___anchor14;
		float L_15 = (&___extents15)->get_x_1();
		float L_16 = (&___extents15)->get_y_2();
		float L_17 = (&___pivot16)->get_x_1();
		float L_18 = (&___pivot16)->get_y_2();
		bool L_19 = ___generateOutOfBounds17;
		bool L_20 = ___alignByGeometry18;
		bool L_21 = TextGenerator_Populate_Internal_cpp_m2186254131(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  bool TextGenerator_Populate_Internal_cpp_m2186254131 (TextGenerator_t647235000 * __this, String_t* ___str0, Font_t4239498691 * ___font1, Color_t2020392075  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, float ___extentsX15, float ___extentsY16, float ___pivotX17, float ___pivotY18, bool ___generateOutOfBounds19, bool ___alignByGeometry20, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str0;
		Font_t4239498691 * L_1 = ___font1;
		int32_t L_2 = ___fontSize3;
		float L_3 = ___scaleFactor4;
		float L_4 = ___lineSpacing5;
		int32_t L_5 = ___style6;
		bool L_6 = ___richText7;
		bool L_7 = ___resizeTextForBestFit8;
		int32_t L_8 = ___resizeTextMinSize9;
		int32_t L_9 = ___resizeTextMaxSize10;
		int32_t L_10 = ___verticalOverFlow11;
		int32_t L_11 = ___horizontalOverflow12;
		bool L_12 = ___updateBounds13;
		int32_t L_13 = ___anchor14;
		float L_14 = ___extentsX15;
		float L_15 = ___extentsY16;
		float L_16 = ___pivotX17;
		float L_17 = ___pivotY18;
		bool L_18 = ___generateOutOfBounds19;
		bool L_19 = ___alignByGeometry20;
		bool L_20 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1252464769(NULL /*static, unused*/, __this, L_0, L_1, (&___color2), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)
extern "C"  bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1252464769 (Il2CppObject * __this /* static, unused */, TextGenerator_t647235000 * ___self0, String_t* ___str1, Font_t4239498691 * ___font2, Color_t2020392075 * ___color3, int32_t ___fontSize4, float ___scaleFactor5, float ___lineSpacing6, int32_t ___style7, bool ___richText8, bool ___resizeTextForBestFit9, int32_t ___resizeTextMinSize10, int32_t ___resizeTextMaxSize11, int32_t ___verticalOverFlow12, int32_t ___horizontalOverflow13, bool ___updateBounds14, int32_t ___anchor15, float ___extentsX16, float ___extentsY17, float ___pivotX18, float ___pivotY19, bool ___generateOutOfBounds20, bool ___alignByGeometry21, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1252464769_ftn) (TextGenerator_t647235000 *, String_t*, Font_t4239498691 *, Color_t2020392075 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1252464769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m1252464769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___self0, ___str1, ___font2, ___color3, ___fontSize4, ___scaleFactor5, ___lineSpacing6, ___style7, ___richText8, ___resizeTextForBestFit9, ___resizeTextMinSize10, ___resizeTextMaxSize11, ___verticalOverFlow12, ___horizontalOverflow13, ___updateBounds14, ___anchor15, ___extentsX16, ___extentsY17, ___pivotX18, ___pivotY19, ___generateOutOfBounds20, ___alignByGeometry21);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C"  Rect_t3681755626  TextGenerator_get_rectExtents_m1925360043 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TextGenerator_INTERNAL_get_rectExtents_m661810980(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C"  void TextGenerator_INTERNAL_get_rectExtents_m661810980 (TextGenerator_t647235000 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m661810980_ftn) (TextGenerator_t647235000 *, Rect_t3681755626 *);
	static TextGenerator_INTERNAL_get_rectExtents_m661810980_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m661810980_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C"  int32_t TextGenerator_get_vertexCount_m264332683 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m264332683_ftn) (TextGenerator_t647235000 *);
	static TextGenerator_get_vertexCount_m264332683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m264332683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C"  void TextGenerator_GetVerticesInternal_m393921805 (TextGenerator_t647235000 * __this, Il2CppObject * ___vertices0, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m393921805_ftn) (TextGenerator_t647235000 *, Il2CppObject *);
	static TextGenerator_GetVerticesInternal_m393921805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m393921805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices0);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C"  int32_t TextGenerator_get_characterCount_m970885214 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m970885214_ftn) (TextGenerator_t647235000 *);
	static TextGenerator_get_characterCount_m970885214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m970885214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_get_characterCountVisible_m1506817214_MetadataUsageId;
extern "C"  int32_t TextGenerator_get_characterCountVisible_m1506817214 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_get_characterCountVisible_m1506817214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = __this->get_m_LastString_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = __this->get_m_LastString_1();
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m264332683(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m1875893177(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m2906823867(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C"  void TextGenerator_GetCharactersInternal_m1809798004 (TextGenerator_t647235000 * __this, Il2CppObject * ___characters0, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m1809798004_ftn) (TextGenerator_t647235000 *, Il2CppObject *);
	static TextGenerator_GetCharactersInternal_m1809798004_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m1809798004_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters0);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C"  int32_t TextGenerator_get_lineCount_m4287181941 (TextGenerator_t647235000 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m4287181941_ftn) (TextGenerator_t647235000 *);
	static TextGenerator_get_lineCount_m4287181941_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m4287181941_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C"  void TextGenerator_GetLinesInternal_m2194096229 (TextGenerator_t647235000 * __this, Il2CppObject * ___lines0, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m2194096229_ftn) (TextGenerator_t647235000 *, Il2CppObject *);
	static TextGenerator_GetLinesInternal_m2194096229_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m2194096229_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines0);
}
// Conversion methods for marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t647235000_marshal_pinvoke(const TextGenerator_t647235000& unmarshaled, TextGenerator_t647235000_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
extern "C" void TextGenerator_t647235000_marshal_pinvoke_back(const TextGenerator_t647235000_marshaled_pinvoke& marshaled, TextGenerator_t647235000& unmarshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t647235000_marshal_pinvoke_cleanup(TextGenerator_t647235000_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t647235000_marshal_com(const TextGenerator_t647235000& unmarshaled, TextGenerator_t647235000_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
extern "C" void TextGenerator_t647235000_marshal_com_back(const TextGenerator_t647235000_marshaled_com& marshaled, TextGenerator_t647235000& unmarshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t647235000_marshal_com_cleanup(TextGenerator_t647235000_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Texture::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Texture__ctor_m4198984292_MetadataUsageId;
extern "C"  void Texture__ctor_m4198984292 (Texture_t2243626319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture__ctor_m4198984292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m56137242 (Il2CppObject * __this /* static, unused */, Texture_t2243626319 * ___t0, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m56137242_ftn) (Texture_t2243626319 *);
	static Texture_Internal_GetWidth_m56137242_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m56137242_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___t0);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m2775530157 (Il2CppObject * __this /* static, unused */, Texture_t2243626319 * ___t0, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m2775530157_ftn) (Texture_t2243626319 *);
	static Texture_Internal_GetHeight_m2775530157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m2775530157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___t0);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m2165436967 (Texture_t2243626319 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m56137242(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m2890247816 (Texture_t2243626319 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m2775530157(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern "C"  Vector2_t2243707579  Texture_get_texelSize_m4226268553 (Texture_t2243626319 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Texture_INTERNAL_get_texelSize_m3180609662(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m3180609662 (Texture_t2243626319 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Texture_INTERNAL_get_texelSize_m3180609662_ftn) (Texture_t2243626319 *, Vector2_t2243707579 *);
	static Texture_INTERNAL_get_texelSize_m3180609662_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_INTERNAL_get_texelSize_m3180609662_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m3598323350_MetadataUsageId;
extern "C"  void Texture2D__ctor_m3598323350 (Texture2D_t3542995729 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m3598323350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m4198984292(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		Texture2D_Internal_Create_m3012183307(NULL /*static, unused*/, __this, L_0, L_1, 5, (bool)1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m1873923924_MetadataUsageId;
extern "C"  void Texture2D__ctor_m1873923924 (Texture2D_t3542995729 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m1873923924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m4198984292(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		Texture2D_Internal_Create_m3012183307(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (bool)0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m3012183307 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, IntPtr_t ___nativeTex6, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m3012183307_ftn) (Texture2D_t3542995729 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m3012183307_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m3012183307_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C"  Texture2D_t3542995729 * Texture2D_get_whiteTexture_m1979591766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3542995729 * (*Texture2D_get_whiteTexture_m1979591766_ftn) ();
	static Texture2D_get_whiteTexture_m1979591766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m1979591766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_blackTexture()
extern "C"  Texture2D_t3542995729 * Texture2D_get_blackTexture_m3457275908 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t3542995729 * (*Texture2D_get_blackTexture_m3457275908_ftn) ();
	static Texture2D_get_blackTexture_m3457275908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_blackTexture_m3457275908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_blackTexture()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Texture2D_SetPixel_m609991514 (Texture2D_t3542995729 * __this, int32_t ___x0, int32_t ___y1, Color_t2020392075  ___color2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		Texture2D_INTERNAL_CALL_SetPixel_m3139530413(NULL /*static, unused*/, __this, L_0, L_1, (&___color2), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_SetPixel_m3139530413 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___self0, int32_t ___x1, int32_t ___y2, Color_t2020392075 * ___color3, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_SetPixel_m3139530413_ftn) (Texture2D_t3542995729 *, int32_t, int32_t, Color_t2020392075 *);
	static Texture2D_INTERNAL_CALL_SetPixel_m3139530413_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_SetPixel_m3139530413_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___x1, ___y2, ___color3);
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C"  Color_t2020392075  Texture2D_GetPixelBilinear_m3063031185 (Texture2D_t3542995729 * __this, float ___u0, float ___v1, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___u0;
		float L_1 = ___v1;
		Texture2D_INTERNAL_CALL_GetPixelBilinear_m570286059(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m570286059 (Il2CppObject * __this /* static, unused */, Texture2D_t3542995729 * ___self0, float ___u1, float ___v2, Color_t2020392075 * ___value3, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixelBilinear_m570286059_ftn) (Texture2D_t3542995729 *, float, float, Color_t2020392075 *);
	static Texture2D_INTERNAL_CALL_GetPixelBilinear_m570286059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixelBilinear_m570286059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___u1, ___v2, ___value3);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture2D_SetPixels_m2799169789 (Texture2D_t3542995729 * __this, ColorU5BU5D_t672350442* ___colors0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t672350442* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m1731341200(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m1731341200 (Texture2D_t3542995729 * __this, ColorU5BU5D_t672350442* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel1;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel1;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t672350442* L_8 = ___colors0;
		int32_t L_9 = ___miplevel1;
		Texture2D_SetPixels_m1131483856(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m1131483856 (Texture2D_t3542995729 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t672350442* ___colors4, int32_t ___miplevel5, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels_m1131483856_ftn) (Texture2D_t3542995729 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t672350442*, int32_t);
	static Texture2D_SetPixels_m1131483856_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m1131483856_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetAllPixels32_m622827385 (Texture2D_t3542995729 * __this, Color32U5BU5D_t30278651* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	typedef void (*Texture2D_SetAllPixels32_m622827385_ftn) (Texture2D_t3542995729 *, Color32U5BU5D_t30278651*, int32_t);
	static Texture2D_SetAllPixels32_m622827385_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetAllPixels32_m622827385_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)");
	_il2cpp_icall_func(__this, ___colors0, ___miplevel1);
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern "C"  void Texture2D_SetPixels32_m2480505405 (Texture2D_t3542995729 * __this, Color32U5BU5D_t30278651* ___colors0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Color32U5BU5D_t30278651* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels32_m3248271056(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern "C"  void Texture2D_SetPixels32_m3248271056 (Texture2D_t3542995729 * __this, Color32U5BU5D_t30278651* ___colors0, int32_t ___miplevel1, const MethodInfo* method)
{
	{
		Color32U5BU5D_t30278651* L_0 = ___colors0;
		int32_t L_1 = ___miplevel1;
		Texture2D_SetAllPixels32_m622827385(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[],System.Boolean)
extern "C"  bool Texture2D_LoadImage_m3883409353 (Texture2D_t3542995729 * __this, ByteU5BU5D_t3397334013* ___data0, bool ___markNonReadable1, const MethodInfo* method)
{
	typedef bool (*Texture2D_LoadImage_m3883409353_ftn) (Texture2D_t3542995729 *, ByteU5BU5D_t3397334013*, bool);
	static Texture2D_LoadImage_m3883409353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_LoadImage_m3883409353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::LoadImage(System.Byte[],System.Boolean)");
	return _il2cpp_icall_func(__this, ___data0, ___markNonReadable1);
}
// System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[])
extern "C"  bool Texture2D_LoadImage_m867542842 (Texture2D_t3542995729 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		bool L_1 = V_0;
		bool L_2 = Texture2D_LoadImage_m3883409353(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern "C"  Color32U5BU5D_t30278651* Texture2D_GetPixels32_m3251516747 (Texture2D_t3542995729 * __this, int32_t ___miplevel0, const MethodInfo* method)
{
	typedef Color32U5BU5D_t30278651* (*Texture2D_GetPixels32_m3251516747_ftn) (Texture2D_t3542995729 *, int32_t);
	static Texture2D_GetPixels32_m3251516747_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels32_m3251516747_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels32(System.Int32)");
	return _il2cpp_icall_func(__this, ___miplevel0);
}
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern "C"  Color32U5BU5D_t30278651* Texture2D_GetPixels32_m2977277634 (Texture2D_t3542995729 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Color32U5BU5D_t30278651* L_1 = Texture2D_GetPixels32_m3251516747(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m3753817130 (Texture2D_t3542995729 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const MethodInfo* method)
{
	typedef void (*Texture2D_Apply_m3753817130_ftn) (Texture2D_t3542995729 *, bool, bool);
	static Texture2D_Apply_m3753817130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m3753817130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m3543341930 (Texture2D_t3542995729 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m3753817130(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.Texture2D::EncodeToPNG()
extern "C"  ByteU5BU5D_t3397334013* Texture2D_EncodeToPNG_m2680110528 (Texture2D_t3542995729 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t3397334013* (*Texture2D_EncodeToPNG_m2680110528_ftn) (Texture2D_t3542995729 *);
	static Texture2D_EncodeToPNG_m2680110528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_EncodeToPNG_m2680110528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::EncodeToPNG()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ThreadAndSerializationSafe::.ctor()
extern "C"  void ThreadAndSerializationSafe__ctor_m84326599 (ThreadAndSerializationSafe_t2122816804 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2216684562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m2216684562_ftn) ();
	static Time_get_time_m2216684562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m2216684562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m1980066582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeSinceLevelLoad_m1980066582_ftn) ();
	static Time_get_timeSinceLevelLoad_m1980066582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeSinceLevelLoad_m1980066582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeSinceLevelLoad()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m2233168104_ftn) ();
	static Time_get_deltaTime_m2233168104_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m2233168104_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m862335845 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m862335845_ftn) ();
	static Time_get_unscaledTime_m862335845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m862335845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m4281640537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m4281640537_ftn) ();
	static Time_get_unscaledDeltaTime_m4281640537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m4281640537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_fixedDeltaTime()
extern "C"  float Time_get_fixedDeltaTime_m2734072926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_fixedDeltaTime_m2734072926_ftn) ();
	static Time_get_fixedDeltaTime_m2734072926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_fixedDeltaTime_m2734072926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_fixedDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_fixedDeltaTime(System.Single)
extern "C"  void Time_set_fixedDeltaTime_m3972646343 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Time_set_fixedDeltaTime_m3972646343_ftn) (float);
	static Time_set_fixedDeltaTime_m3972646343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_fixedDeltaTime_m3972646343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_fixedDeltaTime(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.Time::get_smoothDeltaTime()
extern "C"  float Time_get_smoothDeltaTime_m1294084638 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_smoothDeltaTime_m1294084638_ftn) ();
	static Time_get_smoothDeltaTime_m1294084638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_smoothDeltaTime_m1294084638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_smoothDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m3151482970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m3151482970_ftn) ();
	static Time_get_timeScale_m3151482970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m3151482970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C"  void Time_set_timeScale_m2194722837 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Time_set_timeScale_m2194722837_ftn) (float);
	static Time_set_timeScale_m2194722837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_set_timeScale_m2194722837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::set_timeScale(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m1198768813 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Time_get_frameCount_m1198768813_ftn) ();
	static Time_get_frameCount_m1198768813_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_frameCount_m1198768813_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_frameCount()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m357614587 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m357614587_ftn) ();
	static Time_get_realtimeSinceStartup_m357614587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m357614587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m2640804852 (TooltipAttribute_t4278647215 * __this, String_t* ___tooltip0, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_tooltip_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m4109475843 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FingerId_0();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_fingerId_m4109475843_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_fingerId_m4109475843(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2243707579  Touch_get_position_m2079703643 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_Position_1();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  Touch_get_position_m2079703643_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_position_m2079703643(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t2243707579  Touch_get_deltaPosition_m97688791 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_PositionDelta_3();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  Touch_get_deltaPosition_m97688791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_deltaPosition_m97688791(_thisAdjusted, method);
}
// System.Single UnityEngine.Touch::get_deltaTime()
extern "C"  float Touch_get_deltaTime_m2811155210 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_TimeDelta_4();
		return L_0;
	}
}
extern "C"  float Touch_get_deltaTime_m2811155210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_deltaTime_m2811155210(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Touch::get_tapCount()
extern "C"  int32_t Touch_get_tapCount_m4090741061 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_TapCount_5();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_tapCount_m4090741061_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_tapCount_m4090741061(_thisAdjusted, method);
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m196706494 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Phase_6();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_phase_m196706494_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_phase_m196706494(_thisAdjusted, method);
}
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3264731406 (Touch_t407273883 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Type_7();
		return L_0;
	}
}
extern "C"  int32_t Touch_get_type_m3264731406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Touch_t407273883 * _thisAdjusted = reinterpret_cast<Touch_t407273883 *>(__this + 1);
	return Touch_get_type_m3264731406(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t407273883_marshal_pinvoke(const Touch_t407273883& unmarshaled, Touch_t407273883_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.get_m_FingerId_0();
	Vector2_t2243707579_marshal_pinvoke(unmarshaled.get_m_Position_1(), marshaled.___m_Position_1);
	Vector2_t2243707579_marshal_pinvoke(unmarshaled.get_m_RawPosition_2(), marshaled.___m_RawPosition_2);
	Vector2_t2243707579_marshal_pinvoke(unmarshaled.get_m_PositionDelta_3(), marshaled.___m_PositionDelta_3);
	marshaled.___m_TimeDelta_4 = unmarshaled.get_m_TimeDelta_4();
	marshaled.___m_TapCount_5 = unmarshaled.get_m_TapCount_5();
	marshaled.___m_Phase_6 = unmarshaled.get_m_Phase_6();
	marshaled.___m_Type_7 = unmarshaled.get_m_Type_7();
	marshaled.___m_Pressure_8 = unmarshaled.get_m_Pressure_8();
	marshaled.___m_maximumPossiblePressure_9 = unmarshaled.get_m_maximumPossiblePressure_9();
	marshaled.___m_Radius_10 = unmarshaled.get_m_Radius_10();
	marshaled.___m_RadiusVariance_11 = unmarshaled.get_m_RadiusVariance_11();
	marshaled.___m_AltitudeAngle_12 = unmarshaled.get_m_AltitudeAngle_12();
	marshaled.___m_AzimuthAngle_13 = unmarshaled.get_m_AzimuthAngle_13();
}
extern "C" void Touch_t407273883_marshal_pinvoke_back(const Touch_t407273883_marshaled_pinvoke& marshaled, Touch_t407273883& unmarshaled)
{
	int32_t unmarshaled_m_FingerId_temp_0 = 0;
	unmarshaled_m_FingerId_temp_0 = marshaled.___m_FingerId_0;
	unmarshaled.set_m_FingerId_0(unmarshaled_m_FingerId_temp_0);
	Vector2_t2243707579  unmarshaled_m_Position_temp_1;
	memset(&unmarshaled_m_Position_temp_1, 0, sizeof(unmarshaled_m_Position_temp_1));
	Vector2_t2243707579_marshal_pinvoke_back(marshaled.___m_Position_1, unmarshaled_m_Position_temp_1);
	unmarshaled.set_m_Position_1(unmarshaled_m_Position_temp_1);
	Vector2_t2243707579  unmarshaled_m_RawPosition_temp_2;
	memset(&unmarshaled_m_RawPosition_temp_2, 0, sizeof(unmarshaled_m_RawPosition_temp_2));
	Vector2_t2243707579_marshal_pinvoke_back(marshaled.___m_RawPosition_2, unmarshaled_m_RawPosition_temp_2);
	unmarshaled.set_m_RawPosition_2(unmarshaled_m_RawPosition_temp_2);
	Vector2_t2243707579  unmarshaled_m_PositionDelta_temp_3;
	memset(&unmarshaled_m_PositionDelta_temp_3, 0, sizeof(unmarshaled_m_PositionDelta_temp_3));
	Vector2_t2243707579_marshal_pinvoke_back(marshaled.___m_PositionDelta_3, unmarshaled_m_PositionDelta_temp_3);
	unmarshaled.set_m_PositionDelta_3(unmarshaled_m_PositionDelta_temp_3);
	float unmarshaled_m_TimeDelta_temp_4 = 0.0f;
	unmarshaled_m_TimeDelta_temp_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.set_m_TimeDelta_4(unmarshaled_m_TimeDelta_temp_4);
	int32_t unmarshaled_m_TapCount_temp_5 = 0;
	unmarshaled_m_TapCount_temp_5 = marshaled.___m_TapCount_5;
	unmarshaled.set_m_TapCount_5(unmarshaled_m_TapCount_temp_5);
	int32_t unmarshaled_m_Phase_temp_6 = 0;
	unmarshaled_m_Phase_temp_6 = marshaled.___m_Phase_6;
	unmarshaled.set_m_Phase_6(unmarshaled_m_Phase_temp_6);
	int32_t unmarshaled_m_Type_temp_7 = 0;
	unmarshaled_m_Type_temp_7 = marshaled.___m_Type_7;
	unmarshaled.set_m_Type_7(unmarshaled_m_Type_temp_7);
	float unmarshaled_m_Pressure_temp_8 = 0.0f;
	unmarshaled_m_Pressure_temp_8 = marshaled.___m_Pressure_8;
	unmarshaled.set_m_Pressure_8(unmarshaled_m_Pressure_temp_8);
	float unmarshaled_m_maximumPossiblePressure_temp_9 = 0.0f;
	unmarshaled_m_maximumPossiblePressure_temp_9 = marshaled.___m_maximumPossiblePressure_9;
	unmarshaled.set_m_maximumPossiblePressure_9(unmarshaled_m_maximumPossiblePressure_temp_9);
	float unmarshaled_m_Radius_temp_10 = 0.0f;
	unmarshaled_m_Radius_temp_10 = marshaled.___m_Radius_10;
	unmarshaled.set_m_Radius_10(unmarshaled_m_Radius_temp_10);
	float unmarshaled_m_RadiusVariance_temp_11 = 0.0f;
	unmarshaled_m_RadiusVariance_temp_11 = marshaled.___m_RadiusVariance_11;
	unmarshaled.set_m_RadiusVariance_11(unmarshaled_m_RadiusVariance_temp_11);
	float unmarshaled_m_AltitudeAngle_temp_12 = 0.0f;
	unmarshaled_m_AltitudeAngle_temp_12 = marshaled.___m_AltitudeAngle_12;
	unmarshaled.set_m_AltitudeAngle_12(unmarshaled_m_AltitudeAngle_temp_12);
	float unmarshaled_m_AzimuthAngle_temp_13 = 0.0f;
	unmarshaled_m_AzimuthAngle_temp_13 = marshaled.___m_AzimuthAngle_13;
	unmarshaled.set_m_AzimuthAngle_13(unmarshaled_m_AzimuthAngle_temp_13);
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t407273883_marshal_pinvoke_cleanup(Touch_t407273883_marshaled_pinvoke& marshaled)
{
	Vector2_t2243707579_marshal_pinvoke_cleanup(marshaled.___m_Position_1);
	Vector2_t2243707579_marshal_pinvoke_cleanup(marshaled.___m_RawPosition_2);
	Vector2_t2243707579_marshal_pinvoke_cleanup(marshaled.___m_PositionDelta_3);
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t407273883_marshal_com(const Touch_t407273883& unmarshaled, Touch_t407273883_marshaled_com& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.get_m_FingerId_0();
	Vector2_t2243707579_marshal_com(unmarshaled.get_m_Position_1(), marshaled.___m_Position_1);
	Vector2_t2243707579_marshal_com(unmarshaled.get_m_RawPosition_2(), marshaled.___m_RawPosition_2);
	Vector2_t2243707579_marshal_com(unmarshaled.get_m_PositionDelta_3(), marshaled.___m_PositionDelta_3);
	marshaled.___m_TimeDelta_4 = unmarshaled.get_m_TimeDelta_4();
	marshaled.___m_TapCount_5 = unmarshaled.get_m_TapCount_5();
	marshaled.___m_Phase_6 = unmarshaled.get_m_Phase_6();
	marshaled.___m_Type_7 = unmarshaled.get_m_Type_7();
	marshaled.___m_Pressure_8 = unmarshaled.get_m_Pressure_8();
	marshaled.___m_maximumPossiblePressure_9 = unmarshaled.get_m_maximumPossiblePressure_9();
	marshaled.___m_Radius_10 = unmarshaled.get_m_Radius_10();
	marshaled.___m_RadiusVariance_11 = unmarshaled.get_m_RadiusVariance_11();
	marshaled.___m_AltitudeAngle_12 = unmarshaled.get_m_AltitudeAngle_12();
	marshaled.___m_AzimuthAngle_13 = unmarshaled.get_m_AzimuthAngle_13();
}
extern "C" void Touch_t407273883_marshal_com_back(const Touch_t407273883_marshaled_com& marshaled, Touch_t407273883& unmarshaled)
{
	int32_t unmarshaled_m_FingerId_temp_0 = 0;
	unmarshaled_m_FingerId_temp_0 = marshaled.___m_FingerId_0;
	unmarshaled.set_m_FingerId_0(unmarshaled_m_FingerId_temp_0);
	Vector2_t2243707579  unmarshaled_m_Position_temp_1;
	memset(&unmarshaled_m_Position_temp_1, 0, sizeof(unmarshaled_m_Position_temp_1));
	Vector2_t2243707579_marshal_com_back(marshaled.___m_Position_1, unmarshaled_m_Position_temp_1);
	unmarshaled.set_m_Position_1(unmarshaled_m_Position_temp_1);
	Vector2_t2243707579  unmarshaled_m_RawPosition_temp_2;
	memset(&unmarshaled_m_RawPosition_temp_2, 0, sizeof(unmarshaled_m_RawPosition_temp_2));
	Vector2_t2243707579_marshal_com_back(marshaled.___m_RawPosition_2, unmarshaled_m_RawPosition_temp_2);
	unmarshaled.set_m_RawPosition_2(unmarshaled_m_RawPosition_temp_2);
	Vector2_t2243707579  unmarshaled_m_PositionDelta_temp_3;
	memset(&unmarshaled_m_PositionDelta_temp_3, 0, sizeof(unmarshaled_m_PositionDelta_temp_3));
	Vector2_t2243707579_marshal_com_back(marshaled.___m_PositionDelta_3, unmarshaled_m_PositionDelta_temp_3);
	unmarshaled.set_m_PositionDelta_3(unmarshaled_m_PositionDelta_temp_3);
	float unmarshaled_m_TimeDelta_temp_4 = 0.0f;
	unmarshaled_m_TimeDelta_temp_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.set_m_TimeDelta_4(unmarshaled_m_TimeDelta_temp_4);
	int32_t unmarshaled_m_TapCount_temp_5 = 0;
	unmarshaled_m_TapCount_temp_5 = marshaled.___m_TapCount_5;
	unmarshaled.set_m_TapCount_5(unmarshaled_m_TapCount_temp_5);
	int32_t unmarshaled_m_Phase_temp_6 = 0;
	unmarshaled_m_Phase_temp_6 = marshaled.___m_Phase_6;
	unmarshaled.set_m_Phase_6(unmarshaled_m_Phase_temp_6);
	int32_t unmarshaled_m_Type_temp_7 = 0;
	unmarshaled_m_Type_temp_7 = marshaled.___m_Type_7;
	unmarshaled.set_m_Type_7(unmarshaled_m_Type_temp_7);
	float unmarshaled_m_Pressure_temp_8 = 0.0f;
	unmarshaled_m_Pressure_temp_8 = marshaled.___m_Pressure_8;
	unmarshaled.set_m_Pressure_8(unmarshaled_m_Pressure_temp_8);
	float unmarshaled_m_maximumPossiblePressure_temp_9 = 0.0f;
	unmarshaled_m_maximumPossiblePressure_temp_9 = marshaled.___m_maximumPossiblePressure_9;
	unmarshaled.set_m_maximumPossiblePressure_9(unmarshaled_m_maximumPossiblePressure_temp_9);
	float unmarshaled_m_Radius_temp_10 = 0.0f;
	unmarshaled_m_Radius_temp_10 = marshaled.___m_Radius_10;
	unmarshaled.set_m_Radius_10(unmarshaled_m_Radius_temp_10);
	float unmarshaled_m_RadiusVariance_temp_11 = 0.0f;
	unmarshaled_m_RadiusVariance_temp_11 = marshaled.___m_RadiusVariance_11;
	unmarshaled.set_m_RadiusVariance_11(unmarshaled_m_RadiusVariance_temp_11);
	float unmarshaled_m_AltitudeAngle_temp_12 = 0.0f;
	unmarshaled_m_AltitudeAngle_temp_12 = marshaled.___m_AltitudeAngle_12;
	unmarshaled.set_m_AltitudeAngle_12(unmarshaled_m_AltitudeAngle_temp_12);
	float unmarshaled_m_AzimuthAngle_temp_13 = 0.0f;
	unmarshaled_m_AzimuthAngle_temp_13 = marshaled.___m_AzimuthAngle_13;
	unmarshaled.set_m_AzimuthAngle_13(unmarshaled_m_AzimuthAngle_temp_13);
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t407273883_marshal_com_cleanup(Touch_t407273883_marshaled_com& marshaled)
{
	Vector2_t2243707579_marshal_com_cleanup(marshaled.___m_Position_1);
	Vector2_t2243707579_marshal_com_cleanup(marshaled.___m_RawPosition_2);
	Vector2_t2243707579_marshal_com_cleanup(marshaled.___m_PositionDelta_3);
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern Il2CppClass* TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_il2cpp_TypeInfo_var;
extern Il2CppClass* TouchScreenKeyboardType_t875112366_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard__ctor_m4200205334_MetadataUsageId;
extern "C"  void TouchScreenKeyboard__ctor_m4200205334 (TouchScreenKeyboard_t601950206 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard__ctor_m4200205334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType1;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(TouchScreenKeyboardType_t875112366_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m1952053309(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->set_keyboardType_0(L_3);
		bool L_4 = ___autocorrection2;
		uint32_t L_5 = Convert_ToUInt32_m3686071170(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->set_autocorrection_1(L_5);
		bool L_6 = ___multiline3;
		uint32_t L_7 = Convert_ToUInt32_m3686071170(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->set_multiline_2(L_7);
		bool L_8 = ___secure4;
		uint32_t L_9 = Convert_ToUInt32_m3686071170(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_secure_3(L_9);
		bool L_10 = ___alert5;
		uint32_t L_11 = Convert_ToUInt32_m3686071170(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->set_alert_4(L_11);
		String_t* L_12 = ___text0;
		String_t* L_13 = ___textPlaceholder6;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1440276778(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m1110429671 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m1110429671_ftn) (TouchScreenKeyboard_t601950206 *);
	static TouchScreenKeyboard_Destroy_m1110429671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m1110429671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m2608266435 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m1110429671(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1440276778 (TouchScreenKeyboard_t601950206 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1440276778_ftn) (TouchScreenKeyboard_t601950206 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1440276778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m1440276778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments0, ___text1, ___textPlaceholder2);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C"  bool TouchScreenKeyboard_get_isSupported_m798827778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0064;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 9)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 10)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 11)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 12)
		{
			goto IL_0062;
		}
	}

IL_0045:
	{
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 0)
		{
			goto IL_0062;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 1)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 2)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 3)
		{
			goto IL_0062;
		}
	}
	{
		goto IL_0066;
	}

IL_0062:
	{
		return (bool)1;
	}

IL_0064:
	{
		return (bool)0;
	}

IL_0066:
	{
		return (bool)0;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m2760130151_MetadataUsageId;
extern "C"  TouchScreenKeyboard_t601950206 * TouchScreenKeyboard_Open_m2760130151 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m2760130151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = (bool)0;
		String_t* L_1 = ___text0;
		int32_t L_2 = ___keyboardType1;
		bool L_3 = ___autocorrection2;
		bool L_4 = ___multiline3;
		bool L_5 = ___secure4;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t601950206 * L_8 = TouchScreenKeyboard_Open_m3410222954(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m913506328_MetadataUsageId;
extern "C"  TouchScreenKeyboard_t601950206 * TouchScreenKeyboard_Open_m913506328 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m913506328_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		V_1 = (bool)0;
		V_2 = (bool)0;
		String_t* L_1 = ___text0;
		int32_t L_2 = ___keyboardType1;
		bool L_3 = ___autocorrection2;
		bool L_4 = ___multiline3;
		bool L_5 = V_2;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t601950206 * L_8 = TouchScreenKeyboard_Open_m3410222954(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern Il2CppClass* TouchScreenKeyboard_t601950206_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m3410222954_MetadataUsageId;
extern "C"  TouchScreenKeyboard_t601950206 * TouchScreenKeyboard_Open_m3410222954 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m3410222954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = ___alert5;
		String_t* L_6 = ___textPlaceholder6;
		TouchScreenKeyboard_t601950206 * L_7 = (TouchScreenKeyboard_t601950206 *)il2cpp_codegen_object_new(TouchScreenKeyboard_t601950206_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m4200205334(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C"  String_t* TouchScreenKeyboard_get_text_m538529702 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m538529702_ftn) (TouchScreenKeyboard_t601950206 *);
	static TouchScreenKeyboard_get_text_m538529702_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m538529702_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C"  void TouchScreenKeyboard_set_text_m3456054179 (TouchScreenKeyboard_t601950206 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m3456054179_ftn) (TouchScreenKeyboard_t601950206 *, String_t*);
	static TouchScreenKeyboard_set_text_m3456054179_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m3456054179_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_hideInput_m1521802033 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m1521802033_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m1521802033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m1521802033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C"  bool TouchScreenKeyboard_get_active_m1442597648 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m1442597648_ftn) (TouchScreenKeyboard_t601950206 *);
	static TouchScreenKeyboard_get_active_m1442597648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m1442597648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_active_m3470073047 (TouchScreenKeyboard_t601950206 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m3470073047_ftn) (TouchScreenKeyboard_t601950206 *, bool);
	static TouchScreenKeyboard_set_active_m3470073047_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m3470073047_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C"  bool TouchScreenKeyboard_get_done_m406461410 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m406461410_ftn) (TouchScreenKeyboard_t601950206 *);
	static TouchScreenKeyboard_get_done_m406461410_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m406461410_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C"  bool TouchScreenKeyboard_get_wasCanceled_m1653175226 (TouchScreenKeyboard_t601950206 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m1653175226_ftn) (TouchScreenKeyboard_t601950206 *);
	static TouchScreenKeyboard_get_wasCanceled_m1653175226_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m1653175226_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshal_pinvoke(const TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188& unmarshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshaled_pinvoke& marshaled)
{
	marshaled.___keyboardType_0 = unmarshaled.get_keyboardType_0();
	marshaled.___autocorrection_1 = unmarshaled.get_autocorrection_1();
	marshaled.___multiline_2 = unmarshaled.get_multiline_2();
	marshaled.___secure_3 = unmarshaled.get_secure_3();
	marshaled.___alert_4 = unmarshaled.get_alert_4();
}
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshal_pinvoke_back(const TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshaled_pinvoke& marshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188& unmarshaled)
{
	uint32_t unmarshaled_keyboardType_temp_0 = 0;
	unmarshaled_keyboardType_temp_0 = marshaled.___keyboardType_0;
	unmarshaled.set_keyboardType_0(unmarshaled_keyboardType_temp_0);
	uint32_t unmarshaled_autocorrection_temp_1 = 0;
	unmarshaled_autocorrection_temp_1 = marshaled.___autocorrection_1;
	unmarshaled.set_autocorrection_1(unmarshaled_autocorrection_temp_1);
	uint32_t unmarshaled_multiline_temp_2 = 0;
	unmarshaled_multiline_temp_2 = marshaled.___multiline_2;
	unmarshaled.set_multiline_2(unmarshaled_multiline_temp_2);
	uint32_t unmarshaled_secure_temp_3 = 0;
	unmarshaled_secure_temp_3 = marshaled.___secure_3;
	unmarshaled.set_secure_3(unmarshaled_secure_temp_3);
	uint32_t unmarshaled_alert_temp_4 = 0;
	unmarshaled_alert_temp_4 = marshaled.___alert_4;
	unmarshaled.set_alert_4(unmarshaled_alert_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshal_pinvoke_cleanup(TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshal_com(const TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188& unmarshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshaled_com& marshaled)
{
	marshaled.___keyboardType_0 = unmarshaled.get_keyboardType_0();
	marshaled.___autocorrection_1 = unmarshaled.get_autocorrection_1();
	marshaled.___multiline_2 = unmarshaled.get_multiline_2();
	marshaled.___secure_3 = unmarshaled.get_secure_3();
	marshaled.___alert_4 = unmarshaled.get_alert_4();
}
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshal_com_back(const TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshaled_com& marshaled, TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188& unmarshaled)
{
	uint32_t unmarshaled_keyboardType_temp_0 = 0;
	unmarshaled_keyboardType_temp_0 = marshaled.___keyboardType_0;
	unmarshaled.set_keyboardType_0(unmarshaled_keyboardType_temp_0);
	uint32_t unmarshaled_autocorrection_temp_1 = 0;
	unmarshaled_autocorrection_temp_1 = marshaled.___autocorrection_1;
	unmarshaled.set_autocorrection_1(unmarshaled_autocorrection_temp_1);
	uint32_t unmarshaled_multiline_temp_2 = 0;
	unmarshaled_multiline_temp_2 = marshaled.___multiline_2;
	unmarshaled.set_multiline_2(unmarshaled_multiline_temp_2);
	uint32_t unmarshaled_secure_temp_3 = 0;
	unmarshaled_secure_temp_3 = marshaled.___secure_3;
	unmarshaled.set_secure_3(unmarshaled_secure_temp_3);
	uint32_t unmarshaled_alert_temp_4 = 0;
	unmarshaled_alert_temp_4 = marshaled.___alert_4;
	unmarshaled.set_alert_4(unmarshaled_alert_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern "C" void TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshal_com_cleanup(TouchScreenKeyboard_InternalConstructorHelperArguments_t1040270188_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern Il2CppClass* TrackedReference_t1045890189_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m3153703389_MetadataUsageId;
extern "C"  bool TrackedReference_Equals_m3153703389 (TrackedReference_t1045890189 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m3153703389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m3491334086(NULL /*static, unused*/, ((TrackedReference_t1045890189 *)IsInstClass(L_0, TrackedReference_t1045890189_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m811248179 (TrackedReference_t1045890189 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m1458664696(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_op_Equality_m3491334086_MetadataUsageId;
extern "C"  bool TrackedReference_op_Equality_m3491334086 (Il2CppObject * __this /* static, unused */, TrackedReference_t1045890189 * ___x0, TrackedReference_t1045890189 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m3491334086_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		TrackedReference_t1045890189 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t1045890189 * L_1 = ___y1;
		V_1 = L_1;
		Il2CppObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)1;
	}

IL_0012:
	{
		Il2CppObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		TrackedReference_t1045890189 * L_5 = ___x0;
		NullCheck(L_5);
		IntPtr_t L_6 = L_5->get_m_Ptr_0();
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_8 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0029:
	{
		Il2CppObject * L_9 = V_0;
		if (L_9)
		{
			goto IL_0040;
		}
	}
	{
		TrackedReference_t1045890189 * L_10 = ___y1;
		NullCheck(L_10);
		IntPtr_t L_11 = L_10->get_m_Ptr_0();
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_13 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}

IL_0040:
	{
		TrackedReference_t1045890189 * L_14 = ___x0;
		NullCheck(L_14);
		IntPtr_t L_15 = L_14->get_m_Ptr_0();
		TrackedReference_t1045890189 * L_16 = ___y1;
		NullCheck(L_16);
		IntPtr_t L_17 = L_16->get_m_Ptr_0();
		bool L_18 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		return L_18;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Inequality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Inequality_m2174687895 (Il2CppObject * __this /* static, unused */, TrackedReference_t1045890189 * ___x0, TrackedReference_t1045890189 * ___y1, const MethodInfo* method)
{
	{
		TrackedReference_t1045890189 * L_0 = ___x0;
		TrackedReference_t1045890189 * L_1 = ___y1;
		bool L_2 = TrackedReference_op_Equality_m3491334086(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_pinvoke(const TrackedReference_t1045890189& unmarshaled, TrackedReference_t1045890189_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_back(const TrackedReference_t1045890189_marshaled_pinvoke& marshaled, TrackedReference_t1045890189& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_pinvoke_cleanup(TrackedReference_t1045890189_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_com(const TrackedReference_t1045890189& unmarshaled, TrackedReference_t1045890189_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t1045890189_marshal_com_back(const TrackedReference_t1045890189_marshaled_com& marshaled, TrackedReference_t1045890189& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t1045890189_marshal_com_cleanup(TrackedReference_t1045890189_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.TrailRenderer::get_time()
extern "C"  float TrailRenderer_get_time_m3449437864 (TrailRenderer_t2490637367 * __this, const MethodInfo* method)
{
	typedef float (*TrailRenderer_get_time_m3449437864_ftn) (TrailRenderer_t2490637367 *);
	static TrailRenderer_get_time_m3449437864_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TrailRenderer_get_time_m3449437864_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TrailRenderer::get_time()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TrailRenderer::set_time(System.Single)
extern "C"  void TrailRenderer_set_time_m2582670875 (TrailRenderer_t2490637367 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*TrailRenderer_set_time_m2582670875_ftn) (TrailRenderer_t2490637367 *, float);
	static TrailRenderer_set_time_m2582670875_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TrailRenderer_set_time_m2582670875_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TrailRenderer::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.TrailRenderer::get_startWidth()
extern "C"  float TrailRenderer_get_startWidth_m1625094787 (TrailRenderer_t2490637367 * __this, const MethodInfo* method)
{
	typedef float (*TrailRenderer_get_startWidth_m1625094787_ftn) (TrailRenderer_t2490637367 *);
	static TrailRenderer_get_startWidth_m1625094787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TrailRenderer_get_startWidth_m1625094787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TrailRenderer::get_startWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TrailRenderer::set_startWidth(System.Single)
extern "C"  void TrailRenderer_set_startWidth_m2858856800 (TrailRenderer_t2490637367 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*TrailRenderer_set_startWidth_m2858856800_ftn) (TrailRenderer_t2490637367 *, float);
	static TrailRenderer_set_startWidth_m2858856800_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TrailRenderer_set_startWidth_m2858856800_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TrailRenderer::set_startWidth(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.TrailRenderer::get_endWidth()
extern "C"  float TrailRenderer_get_endWidth_m3952779290 (TrailRenderer_t2490637367 * __this, const MethodInfo* method)
{
	typedef float (*TrailRenderer_get_endWidth_m3952779290_ftn) (TrailRenderer_t2490637367 *);
	static TrailRenderer_get_endWidth_m3952779290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TrailRenderer_get_endWidth_m3952779290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TrailRenderer::get_endWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TrailRenderer::set_endWidth(System.Single)
extern "C"  void TrailRenderer_set_endWidth_m3133306587 (TrailRenderer_t2490637367 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*TrailRenderer_set_endWidth_m3133306587_ftn) (TrailRenderer_t2490637367 *, float);
	static TrailRenderer_set_endWidth_m3133306587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TrailRenderer_set_endWidth_m3133306587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TrailRenderer::set_endWidth(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_position_m1881704498(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m101633598(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m1881704498 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m1881704498_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_get_position_m1881704498_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m1881704498_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m101633598 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m101633598_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_set_position_m101633598_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m101633598_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2243707580  Transform_get_localPosition_m2533925116 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localPosition_m94028171(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1026930133 (Transform_t3275118058 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m432504087(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m94028171 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m94028171_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_get_localPosition_m94028171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m94028171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m432504087 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m432504087_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_set_localPosition_m432504087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m432504087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t2243707580  Transform_get_eulerAngles_m4066505159 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t4030073918  L_0 = Transform_get_rotation_m1033555130(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t2243707580  L_1 = Quaternion_get_eulerAngles_m3302573991((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m2881310872 (Transform_t3275118058 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		Quaternion_t4030073918  L_1 = Quaternion_Euler_m3586339259(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern "C"  Vector3_t2243707580  Transform_get_localEulerAngles_m4231787854 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t4030073918  L_0 = Transform_get_localRotation_m4001487205(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t2243707580  L_1 = Quaternion_get_eulerAngles_m3302573991((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m2927195985 (Transform_t3275118058 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		Quaternion_t4030073918  L_1 = Quaternion_Euler_m3586339259(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_localRotation_m2055111962(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t2243707580  Transform_get_up_m1603627763 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	{
		Quaternion_t4030073918  L_0 = Transform_get_rotation_m1033555130(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2243707580  Transform_get_forward_m1833488937 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	{
		Quaternion_t4030073918  L_0 = Transform_get_rotation_m1033555130(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_rotation_m2427701365(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m3703763817(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m2427701365 (Transform_t3275118058 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m2427701365_ftn) (Transform_t3275118058 *, Quaternion_t4030073918 *);
	static Transform_INTERNAL_get_rotation_m2427701365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m2427701365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m3703763817 (Transform_t3275118058 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m3703763817_ftn) (Transform_t3275118058 *, Quaternion_t4030073918 *);
	static Transform_INTERNAL_set_rotation_m3703763817_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m3703763817_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t4030073918  Transform_get_localRotation_m4001487205 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localRotation_m2064954684(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m2055111962 (Transform_t3275118058 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m37206568(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m2064954684 (Transform_t3275118058 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m2064954684_ftn) (Transform_t3275118058 *, Quaternion_t4030073918 *);
	static Transform_INTERNAL_get_localRotation_m2064954684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m2064954684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m37206568 (Transform_t3275118058 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m37206568_ftn) (Transform_t3275118058 *, Quaternion_t4030073918 *);
	static Transform_INTERNAL_set_localRotation_m37206568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m37206568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m3074381503 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_localScale_m2568549910(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m3680777866(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m2568549910 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m2568549910_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_get_localScale_m2568549910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m2568549910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m3680777866 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m3680777866_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_set_localScale_m3680777866_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m3680777866_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Transform_get_parentInternal_m927919099(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2212073539;
extern const uint32_t Transform_set_parent_m3281327839_MetadataUsageId;
extern "C"  void Transform_set_parent_m3281327839 (Transform_t3275118058 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_parent_m3281327839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t3349966182 *)IsInstSealed(__this, RectTransform_t3349966182_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, _stringLiteral2212073539, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t3275118058 * L_0 = ___value0;
		Transform_set_parentInternal_m4124721022(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t3275118058 * Transform_get_parentInternal_m927919099 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Transform_get_parentInternal_m927919099_ftn) (Transform_t3275118058 *);
	static Transform_get_parentInternal_m927919099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m927919099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m4124721022 (Transform_t3275118058 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m4124721022_ftn) (Transform_t3275118058 *, Transform_t3275118058 *);
	static Transform_set_parentInternal_m4124721022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m4124721022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m4124909910 (Transform_t3275118058 * __this, Transform_t3275118058 * ___parent0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___parent0;
		Transform_SetParent_m1963830867(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m1963830867 (Transform_t3275118058 * __this, Transform_t3275118058 * ___parent0, bool ___worldPositionStays1, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m1963830867_ftn) (Transform_t3275118058 *, Transform_t3275118058 *, bool);
	static Transform_SetParent_m1963830867_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m1963830867_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent0, ___worldPositionStays1);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t2933234003  Transform_get_worldToLocalMatrix_m3299477436 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m3394773201(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m3394773201 (Transform_t3275118058 * __this, Matrix4x4_t2933234003 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m3394773201_ftn) (Transform_t3275118058 *, Matrix4x4_t2933234003 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m3394773201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m3394773201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m423862381 (Transform_t3275118058 * __this, Vector3_t2243707580  ___translation0, int32_t ___relativeTo1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo1;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___translation0;
		Vector3_t2243707580  L_3 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m2469242620(__this, L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_001d:
	{
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = ___translation0;
		Vector3_t2243707580  L_6 = Transform_TransformDirection_m1639585047(__this, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m2469242620(__this, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m2612876682 (Transform_t3275118058 * __this, Vector3_t2243707580  ___eulerAngles0, int32_t ___relativeTo1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___eulerAngles0)->get_x_1();
		float L_1 = (&___eulerAngles0)->get_y_2();
		float L_2 = (&___eulerAngles0)->get_z_3();
		Quaternion_t4030073918  L_3 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t4030073918  L_5 = Transform_get_localRotation_m4001487205(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_6 = V_0;
		Quaternion_t4030073918  L_7 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m2055111962(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t4030073918  L_8 = Transform_get_rotation_m1033555130(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_9 = Transform_get_rotation_m1033555130(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_10 = Quaternion_Inverse_m3931399088(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_11 = V_0;
		Quaternion_t4030073918  L_12 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_13 = Transform_get_rotation_m1033555130(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_14 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_15 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m3411284563(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C"  void Transform_LookAt_m2514033256 (Transform_t3275118058 * __this, Transform_t3275118058 * ___target0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t3275118058 * L_1 = ___target0;
		Vector3_t2243707580  L_2 = V_0;
		Transform_LookAt_m335101033(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Transform_LookAt_m335101033_MetadataUsageId;
extern "C"  void Transform_LookAt_m335101033 (Transform_t3275118058 * __this, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___worldUp1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_LookAt_m335101033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t3275118058 * L_2 = ___target0;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = ___worldUp1;
		Transform_LookAt_m3392147815(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m3392147815 (Transform_t3275118058 * __this, Vector3_t2243707580  ___worldPosition0, Vector3_t2243707580  ___worldUp1, const MethodInfo* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m1443061981(NULL /*static, unused*/, __this, (&___worldPosition0), (&___worldUp1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_LookAt_m1443061981 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___self0, Vector3_t2243707580 * ___worldPosition1, Vector3_t2243707580 * ___worldUp2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m1443061981_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_CALL_LookAt_m1443061981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m1443061981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___worldPosition1, ___worldUp2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformDirection_m1639585047 (Transform_t3275118058 * __this, Vector3_t2243707580  ___direction0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_TransformDirection_m2899991790(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformDirection_m2899991790 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___self0, Vector3_t2243707580 * ___direction1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformDirection_m2899991790_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_CALL_TransformDirection_m2899991790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m2899991790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformDirection_m3595190459 (Transform_t3275118058 * __this, Vector3_t2243707580  ___direction0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_InverseTransformDirection_m1340750556(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformDirection_m1340750556 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___self0, Vector3_t2243707580 * ___direction1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformDirection_m1340750556_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_CALL_InverseTransformDirection_m1340750556_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformDirection_m1340750556_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformPoint_m3272254198 (Transform_t3275118058 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_TransformPoint_m4114689647(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m4114689647 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___self0, Vector3_t2243707580 * ___position1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m4114689647_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_CALL_TransformPoint_m4114689647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m4114689647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformPoint_m2648491174 (Transform_t3275118058 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m69330567(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m69330567 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___self0, Vector3_t2243707580 * ___position1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m69330567_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m69330567_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m69330567_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C"  Transform_t3275118058 * Transform_get_root_m3891257842 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Transform_get_root_m3891257842_ftn) (Transform_t3275118058 *);
	static Transform_get_root_m3891257842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m3891257842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m881385315 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m881385315_ftn) (Transform_t3275118058 *);
	static Transform_get_childCount_m881385315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m881385315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::DetachChildren()
extern "C"  void Transform_DetachChildren_m2873894053 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	typedef void (*Transform_DetachChildren_m2873894053_ftn) (Transform_t3275118058 *);
	static Transform_DetachChildren_m2873894053_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_DetachChildren_m2873894053_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::DetachChildren()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m3606528771 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m3606528771_ftn) (Transform_t3275118058 *);
	static Transform_SetAsFirstSibling_m3606528771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m3606528771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3275118058 * Transform_Find_m3323476454 (Transform_t3275118058 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Transform_Find_m3323476454_ftn) (Transform_t3275118058 *, String_t*);
	static Transform_Find_m3323476454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_Find_m3323476454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::Find(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C"  Vector3_t2243707580  Transform_get_lossyScale_m1638545862 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_INTERNAL_get_lossyScale_m3027364225(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_lossyScale_m3027364225 (Transform_t3275118058 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m3027364225_ftn) (Transform_t3275118058 *, Vector3_t2243707580 *);
	static Transform_INTERNAL_get_lossyScale_m3027364225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m3027364225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m10844547 (Transform_t3275118058 * __this, Transform_t3275118058 * ___parent0, const MethodInfo* method)
{
	typedef bool (*Transform_IsChildOf_m10844547_ftn) (Transform_t3275118058 *, Transform_t3275118058 *);
	static Transform_IsChildOf_m10844547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m10844547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	return _il2cpp_icall_func(__this, ___parent0);
}
// UnityEngine.Transform UnityEngine.Transform::FindChild(System.String)
extern "C"  Transform_t3275118058 * Transform_FindChild_m2677714886 (Transform_t3275118058 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Transform_t3275118058 * L_1 = Transform_Find_m3323476454(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern Il2CppClass* Enumerator_t1251553160_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m3479720613_MetadataUsageId;
extern "C"  Il2CppObject * Transform_GetEnumerator_m3479720613 (Transform_t3275118058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m3479720613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t1251553160 * L_0 = (Enumerator_t1251553160 *)il2cpp_codegen_object_new(Enumerator_t1251553160_il2cpp_TypeInfo_var);
		Enumerator__ctor_m147705785(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Transform_GetChild_m3838588184_ftn) (Transform_t3275118058 *, int32_t);
	static Transform_GetChild_m3838588184_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m3838588184_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m147705785 (Enumerator_t1251553160 * __this, Transform_t3275118058 * ___outer0, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2520481711 (Enumerator_t1251553160 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3838588184(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3980662062 (Enumerator_t1251553160 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t3275118058 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m881385315(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m950879083 (Enumerator_t1251553160 * __this, const MethodInfo* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t3056636800_marshal_pinvoke(const UICharInfo_t3056636800& unmarshaled, UICharInfo_t3056636800_marshaled_pinvoke& marshaled)
{
	Vector2_t2243707579_marshal_pinvoke(unmarshaled.get_cursorPos_0(), marshaled.___cursorPos_0);
	marshaled.___charWidth_1 = unmarshaled.get_charWidth_1();
}
extern "C" void UICharInfo_t3056636800_marshal_pinvoke_back(const UICharInfo_t3056636800_marshaled_pinvoke& marshaled, UICharInfo_t3056636800& unmarshaled)
{
	Vector2_t2243707579  unmarshaled_cursorPos_temp_0;
	memset(&unmarshaled_cursorPos_temp_0, 0, sizeof(unmarshaled_cursorPos_temp_0));
	Vector2_t2243707579_marshal_pinvoke_back(marshaled.___cursorPos_0, unmarshaled_cursorPos_temp_0);
	unmarshaled.set_cursorPos_0(unmarshaled_cursorPos_temp_0);
	float unmarshaled_charWidth_temp_1 = 0.0f;
	unmarshaled_charWidth_temp_1 = marshaled.___charWidth_1;
	unmarshaled.set_charWidth_1(unmarshaled_charWidth_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t3056636800_marshal_pinvoke_cleanup(UICharInfo_t3056636800_marshaled_pinvoke& marshaled)
{
	Vector2_t2243707579_marshal_pinvoke_cleanup(marshaled.___cursorPos_0);
}
// Conversion methods for marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t3056636800_marshal_com(const UICharInfo_t3056636800& unmarshaled, UICharInfo_t3056636800_marshaled_com& marshaled)
{
	Vector2_t2243707579_marshal_com(unmarshaled.get_cursorPos_0(), marshaled.___cursorPos_0);
	marshaled.___charWidth_1 = unmarshaled.get_charWidth_1();
}
extern "C" void UICharInfo_t3056636800_marshal_com_back(const UICharInfo_t3056636800_marshaled_com& marshaled, UICharInfo_t3056636800& unmarshaled)
{
	Vector2_t2243707579  unmarshaled_cursorPos_temp_0;
	memset(&unmarshaled_cursorPos_temp_0, 0, sizeof(unmarshaled_cursorPos_temp_0));
	Vector2_t2243707579_marshal_com_back(marshaled.___cursorPos_0, unmarshaled_cursorPos_temp_0);
	unmarshaled.set_cursorPos_0(unmarshaled_cursorPos_temp_0);
	float unmarshaled_charWidth_temp_1 = 0.0f;
	unmarshaled_charWidth_temp_1 = marshaled.___charWidth_1;
	unmarshaled.set_charWidth_1(unmarshaled_charWidth_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.UICharInfo
extern "C" void UICharInfo_t3056636800_marshal_com_cleanup(UICharInfo_t3056636800_marshaled_com& marshaled)
{
	Vector2_t2243707579_marshal_com_cleanup(marshaled.___cursorPos_0);
}
// Conversion methods for marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t3621277874_marshal_pinvoke(const UILineInfo_t3621277874& unmarshaled, UILineInfo_t3621277874_marshaled_pinvoke& marshaled)
{
	marshaled.___startCharIdx_0 = unmarshaled.get_startCharIdx_0();
	marshaled.___height_1 = unmarshaled.get_height_1();
	marshaled.___topY_2 = unmarshaled.get_topY_2();
}
extern "C" void UILineInfo_t3621277874_marshal_pinvoke_back(const UILineInfo_t3621277874_marshaled_pinvoke& marshaled, UILineInfo_t3621277874& unmarshaled)
{
	int32_t unmarshaled_startCharIdx_temp_0 = 0;
	unmarshaled_startCharIdx_temp_0 = marshaled.___startCharIdx_0;
	unmarshaled.set_startCharIdx_0(unmarshaled_startCharIdx_temp_0);
	int32_t unmarshaled_height_temp_1 = 0;
	unmarshaled_height_temp_1 = marshaled.___height_1;
	unmarshaled.set_height_1(unmarshaled_height_temp_1);
	float unmarshaled_topY_temp_2 = 0.0f;
	unmarshaled_topY_temp_2 = marshaled.___topY_2;
	unmarshaled.set_topY_2(unmarshaled_topY_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t3621277874_marshal_pinvoke_cleanup(UILineInfo_t3621277874_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t3621277874_marshal_com(const UILineInfo_t3621277874& unmarshaled, UILineInfo_t3621277874_marshaled_com& marshaled)
{
	marshaled.___startCharIdx_0 = unmarshaled.get_startCharIdx_0();
	marshaled.___height_1 = unmarshaled.get_height_1();
	marshaled.___topY_2 = unmarshaled.get_topY_2();
}
extern "C" void UILineInfo_t3621277874_marshal_com_back(const UILineInfo_t3621277874_marshaled_com& marshaled, UILineInfo_t3621277874& unmarshaled)
{
	int32_t unmarshaled_startCharIdx_temp_0 = 0;
	unmarshaled_startCharIdx_temp_0 = marshaled.___startCharIdx_0;
	unmarshaled.set_startCharIdx_0(unmarshaled_startCharIdx_temp_0);
	int32_t unmarshaled_height_temp_1 = 0;
	unmarshaled_height_temp_1 = marshaled.___height_1;
	unmarshaled.set_height_1(unmarshaled_height_temp_1);
	float unmarshaled_topY_temp_2 = 0.0f;
	unmarshaled_topY_temp_2 = marshaled.___topY_2;
	unmarshaled.set_topY_2(unmarshaled_topY_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.UILineInfo
extern "C" void UILineInfo_t3621277874_marshal_com_cleanup(UILineInfo_t3621277874_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.UIVertex::.cctor()
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t UIVertex__cctor_m803910084_MetadataUsageId;
extern "C"  void UIVertex__cctor_m803910084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIVertex__cctor_m803910084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t1204258818  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color32_t874517518  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m1932627809(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t1204258818_StaticFields*)UIVertex_t1204258818_il2cpp_TypeInfo_var->static_fields)->set_s_DefaultColor_6(L_0);
		Vector4_t2243707581  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m1222289168(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t1204258818_StaticFields*)UIVertex_t1204258818_il2cpp_TypeInfo_var->static_fields)->set_s_DefaultTangent_7(L_1);
		Initobj (UIVertex_t1204258818_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_2 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_position_0(L_2);
		Vector3_t2243707580  L_3 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_normal_1(L_3);
		Vector4_t2243707581  L_4 = ((UIVertex_t1204258818_StaticFields*)UIVertex_t1204258818_il2cpp_TypeInfo_var->static_fields)->get_s_DefaultTangent_7();
		(&V_0)->set_tangent_5(L_4);
		Color32_t874517518  L_5 = ((UIVertex_t1204258818_StaticFields*)UIVertex_t1204258818_il2cpp_TypeInfo_var->static_fields)->get_s_DefaultColor_6();
		(&V_0)->set_color_2(L_5);
		Vector2_t2243707579  L_6 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv0_3(L_6);
		Vector2_t2243707579  L_7 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv1_4(L_7);
		UIVertex_t1204258818  L_8 = V_0;
		((UIVertex_t1204258818_StaticFields*)UIVertex_t1204258818_il2cpp_TypeInfo_var->static_fields)->set_simpleVert_8(L_8);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t1204258818_marshal_pinvoke(const UIVertex_t1204258818& unmarshaled, UIVertex_t1204258818_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_position_0(), marshaled.___position_0);
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_normal_1(), marshaled.___normal_1);
	Color32_t874517518_marshal_pinvoke(unmarshaled.get_color_2(), marshaled.___color_2);
	Vector2_t2243707579_marshal_pinvoke(unmarshaled.get_uv0_3(), marshaled.___uv0_3);
	Vector2_t2243707579_marshal_pinvoke(unmarshaled.get_uv1_4(), marshaled.___uv1_4);
	Vector4_t2243707581_marshal_pinvoke(unmarshaled.get_tangent_5(), marshaled.___tangent_5);
}
extern "C" void UIVertex_t1204258818_marshal_pinvoke_back(const UIVertex_t1204258818_marshaled_pinvoke& marshaled, UIVertex_t1204258818& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_position_temp_0;
	memset(&unmarshaled_position_temp_0, 0, sizeof(unmarshaled_position_temp_0));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___position_0, unmarshaled_position_temp_0);
	unmarshaled.set_position_0(unmarshaled_position_temp_0);
	Vector3_t2243707580  unmarshaled_normal_temp_1;
	memset(&unmarshaled_normal_temp_1, 0, sizeof(unmarshaled_normal_temp_1));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___normal_1, unmarshaled_normal_temp_1);
	unmarshaled.set_normal_1(unmarshaled_normal_temp_1);
	Color32_t874517518  unmarshaled_color_temp_2;
	memset(&unmarshaled_color_temp_2, 0, sizeof(unmarshaled_color_temp_2));
	Color32_t874517518_marshal_pinvoke_back(marshaled.___color_2, unmarshaled_color_temp_2);
	unmarshaled.set_color_2(unmarshaled_color_temp_2);
	Vector2_t2243707579  unmarshaled_uv0_temp_3;
	memset(&unmarshaled_uv0_temp_3, 0, sizeof(unmarshaled_uv0_temp_3));
	Vector2_t2243707579_marshal_pinvoke_back(marshaled.___uv0_3, unmarshaled_uv0_temp_3);
	unmarshaled.set_uv0_3(unmarshaled_uv0_temp_3);
	Vector2_t2243707579  unmarshaled_uv1_temp_4;
	memset(&unmarshaled_uv1_temp_4, 0, sizeof(unmarshaled_uv1_temp_4));
	Vector2_t2243707579_marshal_pinvoke_back(marshaled.___uv1_4, unmarshaled_uv1_temp_4);
	unmarshaled.set_uv1_4(unmarshaled_uv1_temp_4);
	Vector4_t2243707581  unmarshaled_tangent_temp_5;
	memset(&unmarshaled_tangent_temp_5, 0, sizeof(unmarshaled_tangent_temp_5));
	Vector4_t2243707581_marshal_pinvoke_back(marshaled.___tangent_5, unmarshaled_tangent_temp_5);
	unmarshaled.set_tangent_5(unmarshaled_tangent_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t1204258818_marshal_pinvoke_cleanup(UIVertex_t1204258818_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___position_0);
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___normal_1);
	Color32_t874517518_marshal_pinvoke_cleanup(marshaled.___color_2);
	Vector2_t2243707579_marshal_pinvoke_cleanup(marshaled.___uv0_3);
	Vector2_t2243707579_marshal_pinvoke_cleanup(marshaled.___uv1_4);
	Vector4_t2243707581_marshal_pinvoke_cleanup(marshaled.___tangent_5);
}
// Conversion methods for marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t1204258818_marshal_com(const UIVertex_t1204258818& unmarshaled, UIVertex_t1204258818_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com(unmarshaled.get_position_0(), marshaled.___position_0);
	Vector3_t2243707580_marshal_com(unmarshaled.get_normal_1(), marshaled.___normal_1);
	Color32_t874517518_marshal_com(unmarshaled.get_color_2(), marshaled.___color_2);
	Vector2_t2243707579_marshal_com(unmarshaled.get_uv0_3(), marshaled.___uv0_3);
	Vector2_t2243707579_marshal_com(unmarshaled.get_uv1_4(), marshaled.___uv1_4);
	Vector4_t2243707581_marshal_com(unmarshaled.get_tangent_5(), marshaled.___tangent_5);
}
extern "C" void UIVertex_t1204258818_marshal_com_back(const UIVertex_t1204258818_marshaled_com& marshaled, UIVertex_t1204258818& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_position_temp_0;
	memset(&unmarshaled_position_temp_0, 0, sizeof(unmarshaled_position_temp_0));
	Vector3_t2243707580_marshal_com_back(marshaled.___position_0, unmarshaled_position_temp_0);
	unmarshaled.set_position_0(unmarshaled_position_temp_0);
	Vector3_t2243707580  unmarshaled_normal_temp_1;
	memset(&unmarshaled_normal_temp_1, 0, sizeof(unmarshaled_normal_temp_1));
	Vector3_t2243707580_marshal_com_back(marshaled.___normal_1, unmarshaled_normal_temp_1);
	unmarshaled.set_normal_1(unmarshaled_normal_temp_1);
	Color32_t874517518  unmarshaled_color_temp_2;
	memset(&unmarshaled_color_temp_2, 0, sizeof(unmarshaled_color_temp_2));
	Color32_t874517518_marshal_com_back(marshaled.___color_2, unmarshaled_color_temp_2);
	unmarshaled.set_color_2(unmarshaled_color_temp_2);
	Vector2_t2243707579  unmarshaled_uv0_temp_3;
	memset(&unmarshaled_uv0_temp_3, 0, sizeof(unmarshaled_uv0_temp_3));
	Vector2_t2243707579_marshal_com_back(marshaled.___uv0_3, unmarshaled_uv0_temp_3);
	unmarshaled.set_uv0_3(unmarshaled_uv0_temp_3);
	Vector2_t2243707579  unmarshaled_uv1_temp_4;
	memset(&unmarshaled_uv1_temp_4, 0, sizeof(unmarshaled_uv1_temp_4));
	Vector2_t2243707579_marshal_com_back(marshaled.___uv1_4, unmarshaled_uv1_temp_4);
	unmarshaled.set_uv1_4(unmarshaled_uv1_temp_4);
	Vector4_t2243707581  unmarshaled_tangent_temp_5;
	memset(&unmarshaled_tangent_temp_5, 0, sizeof(unmarshaled_tangent_temp_5));
	Vector4_t2243707581_marshal_com_back(marshaled.___tangent_5, unmarshaled_tangent_temp_5);
	unmarshaled.set_tangent_5(unmarshaled_tangent_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.UIVertex
extern "C" void UIVertex_t1204258818_marshal_com_cleanup(UIVertex_t1204258818_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___position_0);
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___normal_1);
	Color32_t874517518_marshal_com_cleanup(marshaled.___color_2);
	Vector2_t2243707579_marshal_com_cleanup(marshaled.___uv0_3);
	Vector2_t2243707579_marshal_com_cleanup(marshaled.___uv1_4);
	Vector4_t2243707581_marshal_com_cleanup(marshaled.___tangent_5);
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern Il2CppClass* UnhandledExceptionEventHandler_t1916531888_il2cpp_TypeInfo_var;
extern const MethodInfo* UnhandledExceptionHandler_HandleUnhandledException_m1413629867_MethodInfo_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m72343880_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m72343880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m72343880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AppDomain_t2719102437 * L_0 = AppDomain_get_CurrentDomain_m3432767403(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UnhandledExceptionHandler_HandleUnhandledException_m1413629867_MethodInfo_var);
		UnhandledExceptionEventHandler_t1916531888 * L_2 = (UnhandledExceptionEventHandler_t1916531888 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t1916531888_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m2731559345(L_2, NULL, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AppDomain_add_UnhandledException_m3486048175(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1919936450;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m1413629867_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m1413629867 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___sender0, UnhandledExceptionEventArgs_t3067050131 * ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m1413629867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t3067050131 * L_0 = ___args1;
		NullCheck(L_0);
		Il2CppObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m2339769046(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t1927440687 *)IsInstClass(L_1, Exception_t1927440687_il2cpp_TypeInfo_var));
		Exception_t1927440687 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Exception_t1927440687 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m1321283931(NULL /*static, unused*/, _stringLiteral1919936450, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4002467556(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral787955451;
extern const uint32_t UnhandledExceptionHandler_PrintException_m1321283931_MetadataUsageId;
extern "C"  void UnhandledExceptionHandler_PrintException_m1321283931 (Il2CppObject * __this /* static, unused */, String_t* ___title0, Exception_t1927440687 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m1321283931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title0;
		Exception_t1927440687 * L_1 = ___e1;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Exception::ToString() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Exception_t1927440687 * L_4 = ___e1;
		NullCheck(L_4);
		Exception_t1927440687 * L_5 = Exception_get_InnerException_m3722561235(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		Exception_t1927440687 * L_6 = ___e1;
		NullCheck(L_6);
		Exception_t1927440687 * L_7 = Exception_get_InnerException_m3722561235(L_6, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m1321283931(NULL /*static, unused*/, _stringLiteral787955451, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4002467556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4002467556_ftn) ();
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4002467556_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m4002467556_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.UnityAPICompatibilityVersionAttribute::.ctor(System.String)
extern "C"  void UnityAPICompatibilityVersionAttribute__ctor_m2146437932 (UnityAPICompatibilityVersionAttribute_t2508627033 * __this, String_t* ___version0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___version0;
		__this->set__version_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor()
extern Il2CppCodeGenString* _stringLiteral386007604;
extern const uint32_t UnityException__ctor_m3650417185_MetadataUsageId;
extern "C"  void UnityException__ctor_m3650417185 (UnityException_t2687879050 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m3650417185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception__ctor_m485833136(__this, _stringLiteral386007604, /*hidden argument*/NULL);
		Exception_set_HResult_m2376998645(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C"  void UnityException__ctor_m1554762831 (UnityException_t2687879050 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m2376998645(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C"  void UnityException__ctor_m2835958127 (UnityException_t2687879050 * __this, String_t* ___message0, Exception_t1927440687 * ___innerException1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t1927440687 * L_1 = ___innerException1;
		Exception__ctor_m2453009240(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m2376998645(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void UnityException__ctor_m1146123324 (UnityException_t2687879050 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t228987430 * L_0 = ___info0;
		StreamingContext_t1417235061  L_1 = ___context1;
		Exception__ctor_m3836998015(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UnityString_Format_m2949645127_MetadataUsageId;
extern "C"  String_t* UnityString_Format_m2949645127 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityString_Format_m2949645127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t3614634134* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m1263743648(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m3067419446_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	Vector2__ctor_m3067419446(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510762785;
extern const uint32_t Vector2_get_Item_m2792130561_MetadataUsageId;
extern "C"  float Vector2_get_Item_m2792130561 (Vector2_t2243707579 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_Item_m2792130561_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		float L_3 = __this->get_x_1();
		return L_3;
	}

IL_001b:
	{
		float L_4 = __this->get_y_2();
		return L_4;
	}

IL_0022:
	{
		IndexOutOfRangeException_t3527622107 * L_5 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_5, _stringLiteral3510762785, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  float Vector2_get_Item_m2792130561_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_get_Item_m2792130561(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510762785;
extern const uint32_t Vector2_set_Item_m3881967114_MetadataUsageId;
extern "C"  void Vector2_set_Item_m3881967114 (Vector2_t2243707579 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_set_Item_m3881967114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002c;
	}

IL_0014:
	{
		float L_3 = ___value1;
		__this->set_x_1(L_3);
		goto IL_0037;
	}

IL_0020:
	{
		float L_4 = ___value1;
		__this->set_y_2(L_4);
		goto IL_0037;
	}

IL_002c:
	{
		IndexOutOfRangeException_t3527622107 * L_5 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_5, _stringLiteral3510762785, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0037:
	{
		return;
	}
}
extern "C"  void Vector2_set_Item_m3881967114_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	Vector2_set_Item_m3881967114(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Vector2::Set(System.Single,System.Single)
extern "C"  void Vector2_Set_m3041191210 (Vector2_t2243707579 * __this, float ___new_x0, float ___new_y1, const MethodInfo* method)
{
	{
		float L_0 = ___new_x0;
		__this->set_x_1(L_0);
		float L_1 = ___new_y1;
		__this->set_y_2(L_1);
		return;
	}
}
extern "C"  void Vector2_Set_m3041191210_AdjustorThunk (Il2CppObject * __this, float ___new_x0, float ___new_y1, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	Vector2_Set_m3041191210(_thisAdjusted, ___new_x0, ___new_y1, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Lerp_m1511850087_MetadataUsageId;
extern "C"  Vector2_t2243707579  Vector2_Lerp_m1511850087 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Lerp_m1511850087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_1();
		float L_3 = (&___b1)->get_x_1();
		float L_4 = (&___a0)->get_x_1();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_2();
		float L_7 = (&___b1)->get_y_2();
		float L_8 = (&___a0)->get_y_2();
		float L_9 = ___t2;
		Vector2_t2243707579  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3067419446(&L_10, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::MoveTowards(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  Vector2_MoveTowards_m2864774657 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___current0, Vector2_t2243707579  ___target1, float ___maxDistanceDelta2, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector2_t2243707579  L_0 = ___target1;
		Vector2_t2243707579  L_1 = ___current0;
		Vector2_t2243707579  L_2 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ___maxDistanceDelta2;
		if ((((float)L_4) <= ((float)L_5)))
		{
			goto IL_0022;
		}
	}
	{
		float L_6 = V_1;
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}

IL_0022:
	{
		Vector2_t2243707579  L_7 = ___target1;
		return L_7;
	}

IL_0024:
	{
		Vector2_t2243707579  L_8 = ___current0;
		Vector2_t2243707579  L_9 = V_0;
		float L_10 = V_1;
		Vector2_t2243707579  L_11 = Vector2_op_Division_m96580069(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___maxDistanceDelta2;
		Vector2_t2243707579  L_13 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_Scale_m3228063809 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Vector2::Normalize()
extern "C"  void Vector2_Normalize_m2465777963 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Vector2_get_magnitude_m33802565(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_2 = V_0;
		Vector2_t2243707579  L_3 = Vector2_op_Division_m96580069(NULL /*static, unused*/, (*(Vector2_t2243707579 *)__this), L_2, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)__this) = L_3;
		goto IL_0034;
	}

IL_0029:
	{
		Vector2_t2243707579  L_4 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)__this) = L_4;
	}

IL_0034:
	{
		return;
	}
}
extern "C"  void Vector2_Normalize_m2465777963_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	Vector2_Normalize_m2465777963(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t2243707579  Vector2_get_normalized_m2985402409 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_y_2();
		Vector2__ctor_m3067419446((&V_0), L_0, L_1, /*hidden argument*/NULL);
		Vector2_Normalize_m2465777963((&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector2_t2243707579  Vector2_get_normalized_m2985402409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_get_normalized_m2985402409(_thisAdjusted, method);
}
// System.String UnityEngine.Vector2::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2736546956;
extern const uint32_t Vector2_ToString_m775491729_MetadataUsageId;
extern "C"  String_t* Vector2_ToString_m775491729 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m775491729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral2736546956, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Vector2_ToString_m775491729_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_ToString_m775491729(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m2353429373 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
extern "C"  int32_t Vector2_GetHashCode_m2353429373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_GetHashCode_m2353429373(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Equals_m1405920279_MetadataUsageId;
extern "C"  bool Vector2_Equals_m1405920279 (Vector2_t2243707579 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m1405920279_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector2_t2243707579_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_1, Vector2_t2243707579_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Vector2_Equals_m1405920279_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_Equals_m1405920279(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m778921987 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lhs0, Vector2_t2243707579  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::get_magnitude()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_get_magnitude_m33802565_MetadataUsageId;
extern "C"  float Vector2_get_magnitude_m33802565 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_magnitude_m33802565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = sqrtf(((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3)))));
		return L_4;
	}
}
extern "C"  float Vector2_get_magnitude_m33802565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_get_magnitude_m33802565(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m1226294581 (Vector2_t2243707579 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
extern "C"  float Vector2_get_sqrMagnitude_m1226294581_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector2_t2243707579 * _thisAdjusted = reinterpret_cast<Vector2_t2243707579 *>(__this + 1);
	return Vector2_get_sqrMagnitude_m1226294581(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Angle_m1064580737_MetadataUsageId;
extern "C"  float Vector2_Angle_m1064580737 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___from0, Vector2_t2243707579  ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Angle_m1064580737_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2243707579  L_0 = Vector2_get_normalized_m2985402409((&___from0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = Vector2_get_normalized_m2985402409((&___to1), /*hidden argument*/NULL);
		float L_2 = Vector2_Dot_m778921987(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Distance_m280750759 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___a0;
		Vector2_t2243707579  L_1 = ___b1;
		Vector2_t2243707579  L_2 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_magnitude_m33802565((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::ClampMagnitude(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  Vector2_ClampMagnitude_m951527075 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___vector0, float ___maxLength1, const MethodInfo* method)
{
	{
		float L_0 = Vector2_get_sqrMagnitude_m1226294581((&___vector0), /*hidden argument*/NULL);
		float L_1 = ___maxLength1;
		float L_2 = ___maxLength1;
		if ((!(((float)L_0) > ((float)((float)((float)L_1*(float)L_2))))))
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t2243707579  L_3 = Vector2_get_normalized_m2985402409((&___vector0), /*hidden argument*/NULL);
		float L_4 = ___maxLength1;
		Vector2_t2243707579  L_5 = Vector2_op_Multiply_m4236139442(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001d:
	{
		Vector2_t2243707579  L_6 = ___vector0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C"  float Vector2_SqrMagnitude_m1793890068 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Min(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Min_m2030888775_MetadataUsageId;
extern "C"  Vector2_t2243707579  Vector2_Min_m2030888775 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lhs0, Vector2_t2243707579  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Min_m2030888775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Max(UnityEngine.Vector2,UnityEngine.Vector2)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector2_Max_m78652577_MetadataUsageId;
extern "C"  Vector2_t2243707579  Vector2_Max_m78652577 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lhs0, Vector2_t2243707579  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Max_m78652577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m3067419446(&L_6, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2243707579  Vector2_get_one_m3174311904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t2243707579  Vector2_get_up_m977201173 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t2243707579  Vector2_get_right_m28012078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Addition_m1389598521 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Subtraction_m1984215297 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, Vector2_t2243707579  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_UnaryNegation_m2194368113 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_y_2();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, ((-L_0)), ((-L_1)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  Vector2_op_Multiply_m4236139442 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  Vector2_op_Division_m96580069 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m4168854394 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lhs0, Vector2_t2243707579  ___rhs1, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = ___lhs0;
		Vector2_t2243707579  L_1 = ___rhs1;
		Vector2_t2243707579  L_2 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m1793890068(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Inequality_m4283136193 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___lhs0, Vector2_t2243707579  ___rhs1, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = ___lhs0;
		Vector2_t2243707579  L_1 = ___rhs1;
		Vector2_t2243707579  L_2 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m1793890068(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m1064335535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector3_t2243707580  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m2638739322(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t2243707579_marshal_pinvoke(const Vector2_t2243707579& unmarshaled, Vector2_t2243707579_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
}
extern "C" void Vector2_t2243707579_marshal_pinvoke_back(const Vector2_t2243707579_marshaled_pinvoke& marshaled, Vector2_t2243707579& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t2243707579_marshal_pinvoke_cleanup(Vector2_t2243707579_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t2243707579_marshal_com(const Vector2_t2243707579& unmarshaled, Vector2_t2243707579_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
}
extern "C" void Vector2_t2243707579_marshal_com_back(const Vector2_t2243707579_marshaled_com& marshaled, Vector2_t2243707579& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector2
extern "C" void Vector2_t2243707579_marshal_com_cleanup(Vector2_t2243707579_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3__ctor_m2638739322_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	Vector3__ctor_m2638739322(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m2720820983 (Vector3_t2243707580 * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		__this->set_z_3((0.0f));
		return;
	}
}
extern "C"  void Vector3__ctor_m2720820983_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	Vector3__ctor_m2720820983(_thisAdjusted, ___x0, ___y1, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Lerp_m2935648359_MetadataUsageId;
extern "C"  Vector3_t2243707580  Vector3_Lerp_m2935648359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Lerp_m2935648359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_1();
		float L_3 = (&___b1)->get_x_1();
		float L_4 = (&___a0)->get_x_1();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_2();
		float L_7 = (&___b1)->get_y_2();
		float L_8 = (&___a0)->get_y_2();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_z_3();
		float L_11 = (&___b1)->get_z_3();
		float L_12 = (&___a0)->get_z_3();
		float L_13 = ___t2;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_MoveTowards_m1358638081 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___current0, Vector3_t2243707580  ___target1, float ___maxDistanceDelta2, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ___target1;
		Vector3_t2243707580  L_1 = ___current0;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ___maxDistanceDelta2;
		if ((((float)L_4) <= ((float)L_5)))
		{
			goto IL_0022;
		}
	}
	{
		float L_6 = V_1;
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}

IL_0022:
	{
		Vector3_t2243707580  L_7 = ___target1;
		return L_7;
	}

IL_0024:
	{
		Vector3_t2243707580  L_8 = ___current0;
		Vector3_t2243707580  L_9 = V_0;
		float L_10 = V_1;
		Vector3_t2243707580  L_11 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___maxDistanceDelta2;
		Vector3_t2243707580  L_13 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t2243707580  Vector3_RotateTowards_m4046679056 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___current0, Vector3_t2243707580  ___target1, float ___maxRadiansDelta2, float ___maxMagnitudeDelta3, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___maxRadiansDelta2;
		float L_1 = ___maxMagnitudeDelta3;
		Vector3_INTERNAL_CALL_RotateTowards_m3109427424(NULL /*static, unused*/, (&___current0), (&___target1), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)
extern "C"  void Vector3_INTERNAL_CALL_RotateTowards_m3109427424 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___current0, Vector3_t2243707580 * ___target1, float ___maxRadiansDelta2, float ___maxMagnitudeDelta3, Vector3_t2243707580 * ___value4, const MethodInfo* method)
{
	typedef void (*Vector3_INTERNAL_CALL_RotateTowards_m3109427424_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, float, Vector3_t2243707580 *);
	static Vector3_INTERNAL_CALL_RotateTowards_m3109427424_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Vector3_INTERNAL_CALL_RotateTowards_m3109427424_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___current0, ___target1, ___maxRadiansDelta2, ___maxMagnitudeDelta3, ___value4);
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510722560;
extern const uint32_t Vector3_get_Item_m3616014016_MetadataUsageId;
extern "C"  float Vector3_get_Item_m3616014016 (Vector3_t2243707580 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_Item_m3616014016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		float L_2 = __this->get_x_1();
		return L_2;
	}

IL_0020:
	{
		float L_3 = __this->get_y_2();
		return L_3;
	}

IL_0027:
	{
		float L_4 = __this->get_z_3();
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t3527622107 * L_5 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_5, _stringLiteral3510722560, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}
}
extern "C"  float Vector3_get_Item_m3616014016_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_get_Item_m3616014016(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510722560;
extern const uint32_t Vector3_set_Item_m499708011_MetadataUsageId;
extern "C"  void Vector3_set_Item_m499708011 (Vector3_t2243707580 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_set_Item_m499708011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		float L_2 = ___value1;
		__this->set_x_1(L_2);
		goto IL_0048;
	}

IL_0025:
	{
		float L_3 = ___value1;
		__this->set_y_2(L_3);
		goto IL_0048;
	}

IL_0031:
	{
		float L_4 = ___value1;
		__this->set_z_3(L_4);
		goto IL_0048;
	}

IL_003d:
	{
		IndexOutOfRangeException_t3527622107 * L_5 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_5, _stringLiteral3510722560, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0048:
	{
		return;
	}
}
extern "C"  void Vector3_set_Item_m499708011_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	Vector3_set_Item_m499708011(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Vector3::Set(System.Single,System.Single,System.Single)
extern "C"  void Vector3_Set_m61618334 (Vector3_t2243707580 * __this, float ___new_x0, float ___new_y1, float ___new_z2, const MethodInfo* method)
{
	{
		float L_0 = ___new_x0;
		__this->set_x_1(L_0);
		float L_1 = ___new_y1;
		__this->set_y_2(L_1);
		float L_2 = ___new_z2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3_Set_m61618334_AdjustorThunk (Il2CppObject * __this, float ___new_x0, float ___new_y1, float ___new_z2, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	Vector3_Set_m61618334(_thisAdjusted, ___new_x0, ___new_y1, ___new_z2, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Scale_m1087116865 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Cross_m4149044051 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_y_2();
		float L_1 = (&___rhs1)->get_z_3();
		float L_2 = (&___lhs0)->get_z_3();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_x_1();
		float L_6 = (&___lhs0)->get_x_1();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = (&___lhs0)->get_x_1();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_x_1();
		Vector3_t2243707580  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2638739322(&L_12, ((float)((float)((float)((float)L_0*(float)L_1))-(float)((float)((float)L_2*(float)L_3)))), ((float)((float)((float)((float)L_4*(float)L_5))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)L_8*(float)L_9))-(float)((float)((float)L_10*(float)L_11)))), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m1754570744 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
extern "C"  int32_t Vector3_GetHashCode_m1754570744_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_GetHashCode_m1754570744(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Equals_m2692262876_MetadataUsageId;
extern "C"  bool Vector3_Equals_m2692262876 (Vector3_t2243707580 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Equals_m2692262876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector3_t2243707580_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_1, Vector3_t2243707580_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return (bool)G_B6_0;
	}
}
extern "C"  bool Vector3_Equals_m2692262876_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_Equals_m2692262876(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Reflect(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Reflect_m2501902476 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___inDirection0, Vector3_t2243707580  ___inNormal1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___inNormal1;
		Vector3_t2243707580  L_1 = ___inDirection0;
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = ___inNormal1;
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m3872631309(NULL /*static, unused*/, ((float)((float)(-2.0f)*(float)L_2)), L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = ___inDirection0;
		Vector3_t2243707580  L_6 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Normalize_m2140428981 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ___value0;
		float L_1 = Vector3_Magnitude_m1349200714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t2243707580  L_3 = ___value0;
		float L_4 = V_0;
		Vector3_t2243707580  L_5 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t2243707580  L_6 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m3679112426 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Vector3_Magnitude_m1349200714(NULL /*static, unused*/, (*(Vector3_t2243707580 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002e;
		}
	}
	{
		float L_2 = V_0;
		Vector3_t2243707580  L_3 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, (*(Vector3_t2243707580 *)__this), L_2, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)__this) = L_3;
		goto IL_0039;
	}

IL_002e:
	{
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)__this) = L_4;
	}

IL_0039:
	{
		return;
	}
}
extern "C"  void Vector3_Normalize_m3679112426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	Vector3_Normalize_m3679112426(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m936072361 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, (*(Vector3_t2243707580 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m936072361_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_get_normalized_m936072361(_thisAdjusted, method);
}
// System.String UnityEngine.Vector3::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2889564913;
extern const uint32_t Vector3_ToString_m3857189970_MetadataUsageId;
extern "C"  String_t* Vector3_ToString_m3857189970 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ToString_m3857189970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral2889564913, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  String_t* Vector3_ToString_m3857189970_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_ToString_m3857189970(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m3161182818 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Project(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Project_m1396027688_MetadataUsageId;
extern "C"  Vector3_t2243707580  Vector3_Project_m1396027688 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___vector0, Vector3_t2243707580  ___onNormal1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Project_m1396027688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector3_t2243707580  L_0 = ___onNormal1;
		Vector3_t2243707580  L_1 = ___onNormal1;
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = ((Mathf_t2336485820_StaticFields*)Mathf_t2336485820_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		Vector3_t2243707580  L_5 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector3_t2243707580  L_6 = ___onNormal1;
		Vector3_t2243707580  L_7 = ___vector0;
		Vector3_t2243707580  L_8 = ___onNormal1;
		float L_9 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		float L_11 = V_0;
		Vector3_t2243707580  L_12 = Vector3_op_Division_m3315615850(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Angle_m2552334978_MetadataUsageId;
extern "C"  float Vector3_Angle_m2552334978 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___from0, Vector3_t2243707580  ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Angle_m2552334978_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t2243707580  L_0 = Vector3_get_normalized_m936072361((&___from0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_get_normalized_m936072361((&___to1), /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_2, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = acosf(L_3);
		return ((float)((float)L_4*(float)(57.29578f)));
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Distance_m1859670022_MetadataUsageId;
extern "C"  float Vector3_Distance_m1859670022 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Distance_m1859670022_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3__ctor_m2638739322((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_x_1();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = (&V_0)->get_y_2();
		float L_9 = (&V_0)->get_y_2();
		float L_10 = (&V_0)->get_z_3();
		float L_11 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		return L_12;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::ClampMagnitude(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_ClampMagnitude_m2652735362 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___vector0, float ___maxLength1, const MethodInfo* method)
{
	{
		float L_0 = Vector3_get_sqrMagnitude_m1814096310((&___vector0), /*hidden argument*/NULL);
		float L_1 = ___maxLength1;
		float L_2 = ___maxLength1;
		if ((!(((float)L_0) > ((float)((float)((float)L_1*(float)L_2))))))
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t2243707580  L_3 = Vector3_get_normalized_m936072361((&___vector0), /*hidden argument*/NULL);
		float L_4 = ___maxLength1;
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001d:
	{
		Vector3_t2243707580  L_6 = ___vector0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Magnitude_m1349200714_MetadataUsageId;
extern "C"  float Vector3_Magnitude_m1349200714 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Magnitude_m1349200714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___a0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_get_magnitude_m860342598_MetadataUsageId;
extern "C"  float Vector3_get_magnitude_m860342598 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_magnitude_m860342598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
extern "C"  float Vector3_get_magnitude_m860342598_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_get_magnitude_m860342598(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m3759098164 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___a0)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___a0)->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1814096310 (Vector3_t2243707580 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
extern "C"  float Vector3_get_sqrMagnitude_m1814096310_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector3_t2243707580 * _thisAdjusted = reinterpret_cast<Vector3_t2243707580 *>(__this + 1);
	return Vector3_get_sqrMagnitude_m1814096310(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Min_m4249067335_MetadataUsageId;
extern "C"  Vector3_t2243707580  Vector3_Min_m4249067335 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Min_m4249067335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector3_Max_m2105825185_MetadataUsageId;
extern "C"  Vector3_t2243707580  Vector3_Max_m2105825185 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Max_m2105825185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2638739322(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t2243707580  Vector3_get_one_m627547232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t2243707580  Vector3_get_back_m4246539215 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2243707580  Vector3_get_up_m2725403797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t2243707580  Vector3_get_down_m2372302126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C"  Vector3_t2243707580  Vector3_get_left_m2429378123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t2243707580  Vector3_get_right_m1884123822 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2638739322(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m2407545601 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, Vector3_t2243707580  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_UnaryNegation_m3383802608 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___a0)->get_y_2();
		float L_2 = (&___a0)->get_z_3();
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m3872631309 (Il2CppObject * __this /* static, unused */, float ___d0, Vector3_t2243707580  ___a1, const MethodInfo* method)
{
	{
		float L_0 = (&___a1)->get_x_1();
		float L_1 = ___d0;
		float L_2 = (&___a1)->get_y_2();
		float L_3 = ___d0;
		float L_4 = (&___a1)->get_z_3();
		float L_5 = ___d0;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Division_m3315615850 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m305888255 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___lhs0;
		Vector3_t2243707580  L_1 = ___rhs1;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m3759098164(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m799191452 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___lhs0, Vector3_t2243707580  ___rhs1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___lhs0;
		Vector3_t2243707580  L_1 = ___rhs1;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m3759098164(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t2243707580_marshal_pinvoke(const Vector3_t2243707580& unmarshaled, Vector3_t2243707580_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
}
extern "C" void Vector3_t2243707580_marshal_pinvoke_back(const Vector3_t2243707580_marshaled_pinvoke& marshaled, Vector3_t2243707580& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t2243707580_marshal_pinvoke_cleanup(Vector3_t2243707580_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t2243707580_marshal_com(const Vector3_t2243707580& unmarshaled, Vector3_t2243707580_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
}
extern "C" void Vector3_t2243707580_marshal_com_back(const Vector3_t2243707580_marshaled_com& marshaled, Vector3_t2243707580& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector3
extern "C" void Vector3_t2243707580_marshal_com_cleanup(Vector3_t2243707580_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1222289168 (Vector4_t2243707581 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Vector4__ctor_m1222289168_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	Vector4__ctor_m1222289168(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510973915;
extern const uint32_t Vector4_get_Item_m1912997891_MetadataUsageId;
extern "C"  float Vector4_get_Item_m1912997891 (Vector4_t2243707581 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_Item_m1912997891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = __this->get_x_1();
		return L_2;
	}

IL_0024:
	{
		float L_3 = __this->get_y_2();
		return L_3;
	}

IL_002b:
	{
		float L_4 = __this->get_z_3();
		return L_4;
	}

IL_0032:
	{
		float L_5 = __this->get_w_4();
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t3527622107 * L_6 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_6, _stringLiteral3510973915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
extern "C"  float Vector4_get_Item_m1912997891_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_get_Item_m1912997891(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3510973915;
extern const uint32_t Vector4_set_Item_m3077071044_MetadataUsageId;
extern "C"  void Vector4_set_Item_m3077071044 (Vector4_t2243707581 * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_set_Item_m3077071044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value1;
		__this->set_x_1(L_2);
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value1;
		__this->set_y_2(L_3);
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value1;
		__this->set_z_3(L_4);
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value1;
		__this->set_w_4(L_5);
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t3527622107 * L_6 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_6, _stringLiteral3510973915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0058:
	{
		return;
	}
}
extern "C"  void Vector4_set_Item_m3077071044_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, float ___value1, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	Vector4_set_Item_m3077071044(_thisAdjusted, ___index0, ___value1, method);
}
// System.Void UnityEngine.Vector4::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4_Set_m223799044 (Vector4_t2243707581 * __this, float ___new_x0, float ___new_y1, float ___new_z2, float ___new_w3, const MethodInfo* method)
{
	{
		float L_0 = ___new_x0;
		__this->set_x_1(L_0);
		float L_1 = ___new_y1;
		__this->set_y_2(L_1);
		float L_2 = ___new_z2;
		__this->set_z_3(L_2);
		float L_3 = ___new_w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Vector4_Set_m223799044_AdjustorThunk (Il2CppObject * __this, float ___new_x0, float ___new_y1, float ___new_z2, float ___new_w3, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	Vector4_Set_m223799044(_thisAdjusted, ___new_x0, ___new_y1, ___new_z2, ___new_w3, method);
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m1576457715 (Vector4_t2243707581 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m3102305584(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Vector4_GetHashCode_m1576457715_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_GetHashCode_m1576457715(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_Equals_m3783731577_MetadataUsageId;
extern "C"  bool Vector4_Equals_m3783731577 (Vector4_t2243707581 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Equals_m3783731577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Vector4_t2243707581_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_1, Vector4_t2243707581_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Vector4_Equals_m3783731577_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_Equals_m3783731577(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Vector4::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587482509;
extern const uint32_t Vector4_ToString_m2340321043_MetadataUsageId;
extern "C"  String_t* Vector4_ToString_m2340321043 (Vector4_t2243707581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_ToString_m2340321043_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3587482509, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Vector4_ToString_m2340321043_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_ToString_m2340321043(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2285943745 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___a0, Vector4_t2243707581  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// System.Single UnityEngine.Vector4::get_magnitude()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_get_magnitude_m4049434951_MetadataUsageId;
extern "C"  float Vector4_get_magnitude_m4049434951 (Vector4_t2243707581 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_magnitude_m4049434951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Vector4_Dot_m2285943745(NULL /*static, unused*/, (*(Vector4_t2243707581 *)__this), (*(Vector4_t2243707581 *)__this), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = sqrtf(L_0);
		return L_1;
	}
}
extern "C"  float Vector4_get_magnitude_m4049434951_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_get_magnitude_m4049434951(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m3109980116 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___a0, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = ___a0;
		Vector4_t2243707581  L_1 = ___a0;
		float L_2 = Vector4_Dot_m2285943745(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m2115578799 (Vector4_t2243707581 * __this, const MethodInfo* method)
{
	{
		float L_0 = Vector4_Dot_m2285943745(NULL /*static, unused*/, (*(Vector4_t2243707581 *)__this), (*(Vector4_t2243707581 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  float Vector4_get_sqrMagnitude_m2115578799_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Vector4_t2243707581 * _thisAdjusted = reinterpret_cast<Vector4_t2243707581 *>(__this + 1);
	return Vector4_get_sqrMagnitude_m2115578799(_thisAdjusted, method);
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t2243707581  Vector4_get_zero_m3810945132 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m1222289168(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Addition(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t2243707581  Vector4_op_Addition_m3367537977 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___a0, Vector4_t2243707581  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		Vector4_t2243707581  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1222289168(&L_8, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t2243707581  Vector4_op_Subtraction_m2837269249 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___a0, Vector4_t2243707581  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		Vector4_t2243707581  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1222289168(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  Vector4_op_Multiply_m3204903356 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		float L_6 = (&___a0)->get_w_4();
		float L_7 = ___d1;
		Vector4_t2243707581  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1222289168(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2243707581  Vector4_op_Division_m130892763 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___a0, float ___d1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		float L_6 = (&___a0)->get_w_4();
		float L_7 = ___d1;
		Vector4_t2243707581  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1222289168(&L_8, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m1825453464 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___lhs0, Vector4_t2243707581  ___rhs1, const MethodInfo* method)
{
	{
		Vector4_t2243707581  L_0 = ___lhs0;
		Vector4_t2243707581  L_1 = ___rhs1;
		Vector4_t2243707581  L_2 = Vector4_op_Subtraction_m2837269249(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m3109980116(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t2243707581  Vector4_op_Implicit_m1059320239 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		float L_2 = (&___v0)->get_z_3();
		Vector4_t2243707581  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m1222289168(&L_3, L_0, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector4_t2243707581  Vector4_op_Implicit_m2625404180 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector4_t2243707581  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m1222289168(&L_2, L_0, L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t2243707581_marshal_pinvoke(const Vector4_t2243707581& unmarshaled, Vector4_t2243707581_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Vector4_t2243707581_marshal_pinvoke_back(const Vector4_t2243707581_marshaled_pinvoke& marshaled, Vector4_t2243707581& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t2243707581_marshal_pinvoke_cleanup(Vector4_t2243707581_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t2243707581_marshal_com(const Vector4_t2243707581& unmarshaled, Vector4_t2243707581_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Vector4_t2243707581_marshal_com_back(const Vector4_t2243707581_marshaled_com& marshaled, Vector4_t2243707581& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Vector4
extern "C" void Vector4_t2243707581_marshal_com_cleanup(Vector4_t2243707581_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C"  void WaitForEndOfFrame__ctor_m3062480170 (WaitForEndOfFrame_t1785723201 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m3781413380 (WaitForFixedUpdate_t3968615785 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float ___seconds0, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds0;
		__this->set_m_Seconds_0(L_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke(const WaitForSeconds_t3839502067& unmarshaled, WaitForSeconds_t3839502067_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_back(const WaitForSeconds_t3839502067_marshaled_pinvoke& marshaled, WaitForSeconds_t3839502067& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_pinvoke_cleanup(WaitForSeconds_t3839502067_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_com(const WaitForSeconds_t3839502067& unmarshaled, WaitForSeconds_t3839502067_marshaled_com& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t3839502067_marshal_com_back(const WaitForSeconds_t3839502067_marshaled_com& marshaled, WaitForSeconds_t3839502067& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t3839502067_marshal_com_cleanup(WaitForSeconds_t3839502067_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
extern "C"  void WaitForSecondsRealtime__ctor_m1734539010 (WaitForSecondsRealtime_t2105307154 * __this, float ___time0, const MethodInfo* method)
{
	{
		CustomYieldInstruction__ctor_m1721050687(__this, /*hidden argument*/NULL);
		float L_0 = Time_get_realtimeSinceStartup_m357614587(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = ___time0;
		__this->set_waitTime_0(((float)((float)L_0+(float)L_1)));
		return;
	}
}
// System.Boolean UnityEngine.WaitForSecondsRealtime::get_keepWaiting()
extern "C"  bool WaitForSecondsRealtime_get_keepWaiting_m741039114 (WaitForSecondsRealtime_t2105307154 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_realtimeSinceStartup_m357614587(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_waitTime_0();
		return (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
	}
}
// UnityEngine.JointSuspension2D UnityEngine.WheelJoint2D::get_suspension()
extern "C"  JointSuspension2D_t1941285899  WheelJoint2D_get_suspension_m1233405115 (WheelJoint2D_t570080601 * __this, const MethodInfo* method)
{
	JointSuspension2D_t1941285899  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		WheelJoint2D_INTERNAL_get_suspension_m2554873286(__this, (&V_0), /*hidden argument*/NULL);
		JointSuspension2D_t1941285899  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.WheelJoint2D::set_suspension(UnityEngine.JointSuspension2D)
extern "C"  void WheelJoint2D_set_suspension_m940632710 (WheelJoint2D_t570080601 * __this, JointSuspension2D_t1941285899  ___value0, const MethodInfo* method)
{
	{
		WheelJoint2D_INTERNAL_set_suspension_m1983363850(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WheelJoint2D::INTERNAL_get_suspension(UnityEngine.JointSuspension2D&)
extern "C"  void WheelJoint2D_INTERNAL_get_suspension_m2554873286 (WheelJoint2D_t570080601 * __this, JointSuspension2D_t1941285899 * ___value0, const MethodInfo* method)
{
	typedef void (*WheelJoint2D_INTERNAL_get_suspension_m2554873286_ftn) (WheelJoint2D_t570080601 *, JointSuspension2D_t1941285899 *);
	static WheelJoint2D_INTERNAL_get_suspension_m2554873286_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_INTERNAL_get_suspension_m2554873286_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::INTERNAL_get_suspension(UnityEngine.JointSuspension2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WheelJoint2D::INTERNAL_set_suspension(UnityEngine.JointSuspension2D&)
extern "C"  void WheelJoint2D_INTERNAL_set_suspension_m1983363850 (WheelJoint2D_t570080601 * __this, JointSuspension2D_t1941285899 * ___value0, const MethodInfo* method)
{
	typedef void (*WheelJoint2D_INTERNAL_set_suspension_m1983363850_ftn) (WheelJoint2D_t570080601 *, JointSuspension2D_t1941285899 *);
	static WheelJoint2D_INTERNAL_set_suspension_m1983363850_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_INTERNAL_set_suspension_m1983363850_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::INTERNAL_set_suspension(UnityEngine.JointSuspension2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)
extern "C"  void WheelJoint2D_set_useMotor_m2831031726 (WheelJoint2D_t570080601 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*WheelJoint2D_set_useMotor_m2831031726_ftn) (WheelJoint2D_t570080601 *, bool);
	static WheelJoint2D_set_useMotor_m2831031726_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_set_useMotor_m2831031726_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.JointMotor2D UnityEngine.WheelJoint2D::get_motor()
extern "C"  JointMotor2D_t2112906529  WheelJoint2D_get_motor_m4205090403 (WheelJoint2D_t570080601 * __this, const MethodInfo* method)
{
	JointMotor2D_t2112906529  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		WheelJoint2D_INTERNAL_get_motor_m3405369598(__this, (&V_0), /*hidden argument*/NULL);
		JointMotor2D_t2112906529  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.WheelJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void WheelJoint2D_set_motor_m287781130 (WheelJoint2D_t570080601 * __this, JointMotor2D_t2112906529  ___value0, const MethodInfo* method)
{
	{
		WheelJoint2D_INTERNAL_set_motor_m2691972290(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WheelJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void WheelJoint2D_INTERNAL_get_motor_m3405369598 (WheelJoint2D_t570080601 * __this, JointMotor2D_t2112906529 * ___value0, const MethodInfo* method)
{
	typedef void (*WheelJoint2D_INTERNAL_get_motor_m3405369598_ftn) (WheelJoint2D_t570080601 *, JointMotor2D_t2112906529 *);
	static WheelJoint2D_INTERNAL_get_motor_m3405369598_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_INTERNAL_get_motor_m3405369598_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WheelJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void WheelJoint2D_INTERNAL_set_motor_m2691972290 (WheelJoint2D_t570080601 * __this, JointMotor2D_t2112906529 * ___value0, const MethodInfo* method)
{
	typedef void (*WheelJoint2D_INTERNAL_set_motor_m2691972290_ftn) (WheelJoint2D_t570080601 *, JointMotor2D_t2112906529 *);
	static WheelJoint2D_INTERNAL_set_motor_m2691972290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WheelJoint2D_INTERNAL_set_motor_m2691972290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WheelJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C"  void WrapperlessIcall__ctor_m4149541650 (WrapperlessIcall_t2585285711 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m761932763 (WritableAttribute_t3715198420 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C"  void WWW__ctor_m2024029190 (WWW_t2919945039 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		WWW_InitWWW_m1194933100(__this, L_0, (ByteU5BU5D_t3397334013*)(ByteU5BU5D_t3397334013*)NULL, (StringU5BU5D_t1642385972*)(StringU5BU5D_t1642385972*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C"  void WWW_Dispose_m2554269413 (WWW_t2919945039 * __this, const MethodInfo* method)
{
	{
		WWW_DestroyWWW_m2548500174(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Finalize()
extern "C"  void WWW_Finalize_m3300880244 (WWW_t2919945039 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		WWW_DestroyWWW_m2548500174(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
extern "C"  void WWW_DestroyWWW_m2548500174 (WWW_t2919945039 * __this, bool ___cancel0, const MethodInfo* method)
{
	typedef void (*WWW_DestroyWWW_m2548500174_ftn) (WWW_t2919945039 *, bool);
	static WWW_DestroyWWW_m2548500174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_DestroyWWW_m2548500174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::DestroyWWW(System.Boolean)");
	_il2cpp_icall_func(__this, ___cancel0);
}
// System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
extern "C"  void WWW_InitWWW_m1194933100 (WWW_t2919945039 * __this, String_t* ___url0, ByteU5BU5D_t3397334013* ___postData1, StringU5BU5D_t1642385972* ___iHeaders2, const MethodInfo* method)
{
	typedef void (*WWW_InitWWW_m1194933100_ftn) (WWW_t2919945039 *, String_t*, ByteU5BU5D_t3397334013*, StringU5BU5D_t1642385972*);
	static WWW_InitWWW_m1194933100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_InitWWW_m1194933100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])");
	_il2cpp_icall_func(__this, ___url0, ___postData1, ___iHeaders2);
}
// System.String UnityEngine.WWW::EscapeURL(System.String)
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t WWW_EscapeURL_m3653156031_MetadataUsageId;
extern "C"  String_t* WWW_EscapeURL_m3653156031 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_EscapeURL_m3653156031_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t663144255 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___s0;
		Encoding_t663144255 * L_2 = V_0;
		String_t* L_3 = WWW_EscapeURL_m1229826670(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::EscapeURL(System.String,System.Text.Encoding)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern const uint32_t WWW_EscapeURL_m1229826670_MetadataUsageId;
extern "C"  String_t* WWW_EscapeURL_m1229826670 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t663144255 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_EscapeURL_m1229826670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___s0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0008:
	{
		String_t* L_1 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}

IL_001e:
	{
		Encoding_t663144255 * L_5 = ___e1;
		if (L_5)
		{
			goto IL_0026;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_0026:
	{
		String_t* L_6 = ___s0;
		Encoding_t663144255 * L_7 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
		String_t* L_8 = WWWTranscoder_URLEncode_m3846210256(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Text.Encoding UnityEngine.WWW::get_DefaultEncoding()
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const uint32_t WWW_get_DefaultEncoding_m1497697991_MetadataUsageId;
extern "C"  Encoding_t663144255 * WWW_get_DefaultEncoding_m1497697991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_DefaultEncoding_m1497697991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_ASCII_m2727409419(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C"  ByteU5BU5D_t3397334013* WWW_get_bytes_m420718112 (WWW_t2919945039 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t3397334013* (*WWW_get_bytes_m420718112_ftn) (WWW_t2919945039 *);
	static WWW_get_bytes_m420718112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_bytes_m420718112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_bytes()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_error()
extern "C"  String_t* WWW_get_error_m3092701216 (WWW_t2919945039 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_error_m3092701216_ftn) (WWW_t2919945039 *);
	static WWW_get_error_m3092701216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_error_m3092701216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_error()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWWForm::get_headers()
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m760167321_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m4244870320_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1048821954;
extern Il2CppCodeGenString* _stringLiteral1605073222;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral730767724;
extern const uint32_t WWWForm_get_headers_m3744493569_MetadataUsageId;
extern "C"  Dictionary_2_t3943999495 * WWWForm_get_headers_m3744493569 (WWWForm_t3950226929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_get_headers_m3744493569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	{
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m760167321(L_0, /*hidden argument*/Dictionary_2__ctor_m760167321_MethodInfo_var);
		V_0 = L_0;
		bool L_1 = __this->get_containsFiles_5();
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t3943999495 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_4 = __this->get_boundary_4();
		ByteU5BU5D_t3397334013* L_5 = __this->get_boundary_4();
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1605073222, L_6, _stringLiteral372029312, /*hidden argument*/NULL);
		NullCheck(L_2);
		Dictionary_2_set_Item_m4244870320(L_2, _stringLiteral1048821954, L_7, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
		goto IL_0059;
	}

IL_0049:
	{
		Dictionary_2_t3943999495 * L_8 = V_0;
		NullCheck(L_8);
		Dictionary_2_set_Item_m4244870320(L_8, _stringLiteral1048821954, _stringLiteral730767724, /*hidden argument*/Dictionary_2_set_Item_m4244870320_MethodInfo_var);
	}

IL_0059:
	{
		Dictionary_2_t3943999495 * L_9 = V_0;
		return L_9;
	}
}
// System.Byte[] UnityEngine.WWWForm::get_data()
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m4077476129_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3195270850_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3625537567_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1214590000;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral1676307500;
extern Il2CppCodeGenString* _stringLiteral2204446021;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral4071688053;
extern Il2CppCodeGenString* _stringLiteral3634297178;
extern Il2CppCodeGenString* _stringLiteral2853613097;
extern Il2CppCodeGenString* _stringLiteral502129298;
extern Il2CppCodeGenString* _stringLiteral372029308;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern const uint32_t WWWForm_get_data_m1788094649_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WWWForm_get_data_m1788094649 (WWWForm_t3950226929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWForm_get_data_m1788094649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	ByteU5BU5D_t3397334013* V_3 = NULL;
	ByteU5BU5D_t3397334013* V_4 = NULL;
	ByteU5BU5D_t3397334013* V_5 = NULL;
	MemoryStream_t743994179 * V_6 = NULL;
	int32_t V_7 = 0;
	ByteU5BU5D_t3397334013* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	ByteU5BU5D_t3397334013* V_11 = NULL;
	String_t* V_12 = NULL;
	ByteU5BU5D_t3397334013* V_13 = NULL;
	ByteU5BU5D_t3397334013* V_14 = NULL;
	ByteU5BU5D_t3397334013* V_15 = NULL;
	ByteU5BU5D_t3397334013* V_16 = NULL;
	MemoryStream_t743994179 * V_17 = NULL;
	int32_t V_18 = 0;
	ByteU5BU5D_t3397334013* V_19 = NULL;
	ByteU5BU5D_t3397334013* V_20 = NULL;
	ByteU5BU5D_t3397334013* V_21 = NULL;
	ByteU5BU5D_t3397334013* V_22 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_containsFiles_5();
		if (!L_0)
		{
			goto IL_0311;
		}
	}
	{
		Encoding_t663144255 * L_1 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, _stringLiteral1214590000);
		V_0 = L_2;
		Encoding_t663144255 * L_3 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, _stringLiteral2162321587);
		V_1 = L_4;
		Encoding_t663144255 * L_5 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t3397334013* L_6 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, _stringLiteral1676307500);
		V_2 = L_6;
		Encoding_t663144255 * L_7 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t3397334013* L_8 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_7, _stringLiteral2204446021);
		V_3 = L_8;
		Encoding_t663144255 * L_9 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		ByteU5BU5D_t3397334013* L_10 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, _stringLiteral372029312);
		V_4 = L_10;
		Encoding_t663144255 * L_11 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		ByteU5BU5D_t3397334013* L_12 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, _stringLiteral4071688053);
		V_5 = L_12;
		MemoryStream_t743994179 * L_13 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1489366847(L_13, ((int32_t)1024), /*hidden argument*/NULL);
		V_6 = L_13;
	}

IL_0079:
	try
	{ // begin try (depth: 1)
		{
			V_7 = 0;
			goto IL_0297;
		}

IL_0081:
		{
			MemoryStream_t743994179 * L_14 = V_6;
			ByteU5BU5D_t3397334013* L_15 = V_1;
			ByteU5BU5D_t3397334013* L_16 = V_1;
			NullCheck(L_16);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_15, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))));
			MemoryStream_t743994179 * L_17 = V_6;
			ByteU5BU5D_t3397334013* L_18 = V_0;
			ByteU5BU5D_t3397334013* L_19 = V_0;
			NullCheck(L_19);
			NullCheck(L_17);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length)))));
			MemoryStream_t743994179 * L_20 = V_6;
			ByteU5BU5D_t3397334013* L_21 = __this->get_boundary_4();
			ByteU5BU5D_t3397334013* L_22 = __this->get_boundary_4();
			NullCheck(L_22);
			NullCheck(L_20);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length)))));
			MemoryStream_t743994179 * L_23 = V_6;
			ByteU5BU5D_t3397334013* L_24 = V_1;
			ByteU5BU5D_t3397334013* L_25 = V_1;
			NullCheck(L_25);
			NullCheck(L_23);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))));
			MemoryStream_t743994179 * L_26 = V_6;
			ByteU5BU5D_t3397334013* L_27 = V_2;
			ByteU5BU5D_t3397334013* L_28 = V_2;
			NullCheck(L_28);
			NullCheck(L_26);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_26, L_27, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))));
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_29 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t1398341365 * L_30 = __this->get_types_3();
			int32_t L_31 = V_7;
			NullCheck(L_30);
			String_t* L_32 = List_1_get_Item_m4077476129(L_30, L_31, /*hidden argument*/List_1_get_Item_m4077476129_MethodInfo_var);
			NullCheck(L_29);
			ByteU5BU5D_t3397334013* L_33 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_32);
			V_8 = L_33;
			MemoryStream_t743994179 * L_34 = V_6;
			ByteU5BU5D_t3397334013* L_35 = V_8;
			ByteU5BU5D_t3397334013* L_36 = V_8;
			NullCheck(L_36);
			NullCheck(L_34);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_36)->max_length)))));
			MemoryStream_t743994179 * L_37 = V_6;
			ByteU5BU5D_t3397334013* L_38 = V_1;
			ByteU5BU5D_t3397334013* L_39 = V_1;
			NullCheck(L_39);
			NullCheck(L_37);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_37, L_38, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length)))));
			MemoryStream_t743994179 * L_40 = V_6;
			ByteU5BU5D_t3397334013* L_41 = V_3;
			ByteU5BU5D_t3397334013* L_42 = V_3;
			NullCheck(L_42);
			NullCheck(L_40);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_40, L_41, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_42)->max_length)))));
			Encoding_t663144255 * L_43 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_43);
			String_t* L_44 = VirtFuncInvoker0< String_t* >::Invoke(23 /* System.String System.Text.Encoding::get_HeaderName() */, L_43);
			V_9 = L_44;
			List_1_t1398341365 * L_45 = __this->get_fieldNames_1();
			int32_t L_46 = V_7;
			NullCheck(L_45);
			String_t* L_47 = List_1_get_Item_m4077476129(L_45, L_46, /*hidden argument*/List_1_get_Item_m4077476129_MethodInfo_var);
			V_10 = L_47;
			String_t* L_48 = V_10;
			Encoding_t663144255 * L_49 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			bool L_50 = WWWTranscoder_SevenBitClean_m556517891(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
			if (!L_50)
			{
				goto IL_0144;
			}
		}

IL_0132:
		{
			String_t* L_51 = V_10;
			NullCheck(L_51);
			int32_t L_52 = String_IndexOf_m4251815737(L_51, _stringLiteral3634297178, /*hidden argument*/NULL);
			if ((((int32_t)L_52) <= ((int32_t)(-1))))
			{
				goto IL_017d;
			}
		}

IL_0144:
		{
			StringU5BU5D_t1642385972* L_53 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
			ArrayElementTypeCheck (L_53, _stringLiteral3634297178);
			(L_53)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3634297178);
			StringU5BU5D_t1642385972* L_54 = L_53;
			String_t* L_55 = V_9;
			NullCheck(L_54);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
			ArrayElementTypeCheck (L_54, L_55);
			(L_54)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_55);
			StringU5BU5D_t1642385972* L_56 = L_54;
			NullCheck(L_56);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
			ArrayElementTypeCheck (L_56, _stringLiteral2853613097);
			(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2853613097);
			StringU5BU5D_t1642385972* L_57 = L_56;
			String_t* L_58 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_59 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			String_t* L_60 = WWWTranscoder_QPEncode_m629571574(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
			NullCheck(L_57);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
			ArrayElementTypeCheck (L_57, L_60);
			(L_57)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_60);
			StringU5BU5D_t1642385972* L_61 = L_57;
			NullCheck(L_61);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 4);
			ArrayElementTypeCheck (L_61, _stringLiteral502129298);
			(L_61)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral502129298);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_62 = String_Concat_m626692867(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
			V_10 = L_62;
		}

IL_017d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_63 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_64 = V_10;
			NullCheck(L_63);
			ByteU5BU5D_t3397334013* L_65 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_63, L_64);
			V_11 = L_65;
			MemoryStream_t743994179 * L_66 = V_6;
			ByteU5BU5D_t3397334013* L_67 = V_11;
			ByteU5BU5D_t3397334013* L_68 = V_11;
			NullCheck(L_68);
			NullCheck(L_66);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_66, L_67, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_68)->max_length)))));
			MemoryStream_t743994179 * L_69 = V_6;
			ByteU5BU5D_t3397334013* L_70 = V_4;
			ByteU5BU5D_t3397334013* L_71 = V_4;
			NullCheck(L_71);
			NullCheck(L_69);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_69, L_70, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_71)->max_length)))));
			List_1_t1398341365 * L_72 = __this->get_fileNames_2();
			int32_t L_73 = V_7;
			NullCheck(L_72);
			String_t* L_74 = List_1_get_Item_m4077476129(L_72, L_73, /*hidden argument*/List_1_get_Item_m4077476129_MethodInfo_var);
			if (!L_74)
			{
				goto IL_025c;
			}
		}

IL_01b9:
		{
			List_1_t1398341365 * L_75 = __this->get_fileNames_2();
			int32_t L_76 = V_7;
			NullCheck(L_75);
			String_t* L_77 = List_1_get_Item_m4077476129(L_75, L_76, /*hidden argument*/List_1_get_Item_m4077476129_MethodInfo_var);
			V_12 = L_77;
			String_t* L_78 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_79 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			bool L_80 = WWWTranscoder_SevenBitClean_m556517891(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
			if (!L_80)
			{
				goto IL_01eb;
			}
		}

IL_01d9:
		{
			String_t* L_81 = V_12;
			NullCheck(L_81);
			int32_t L_82 = String_IndexOf_m4251815737(L_81, _stringLiteral3634297178, /*hidden argument*/NULL);
			if ((((int32_t)L_82) <= ((int32_t)(-1))))
			{
				goto IL_0224;
			}
		}

IL_01eb:
		{
			StringU5BU5D_t1642385972* L_83 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
			NullCheck(L_83);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 0);
			ArrayElementTypeCheck (L_83, _stringLiteral3634297178);
			(L_83)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3634297178);
			StringU5BU5D_t1642385972* L_84 = L_83;
			String_t* L_85 = V_9;
			NullCheck(L_84);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_84, 1);
			ArrayElementTypeCheck (L_84, L_85);
			(L_84)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_85);
			StringU5BU5D_t1642385972* L_86 = L_84;
			NullCheck(L_86);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_86, 2);
			ArrayElementTypeCheck (L_86, _stringLiteral2853613097);
			(L_86)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2853613097);
			StringU5BU5D_t1642385972* L_87 = L_86;
			String_t* L_88 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_89 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			String_t* L_90 = WWWTranscoder_QPEncode_m629571574(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
			NullCheck(L_87);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 3);
			ArrayElementTypeCheck (L_87, L_90);
			(L_87)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_90);
			StringU5BU5D_t1642385972* L_91 = L_87;
			NullCheck(L_91);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 4);
			ArrayElementTypeCheck (L_91, _stringLiteral502129298);
			(L_91)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral502129298);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m626692867(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			V_12 = L_92;
		}

IL_0224:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_93 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_94 = V_12;
			NullCheck(L_93);
			ByteU5BU5D_t3397334013* L_95 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_93, L_94);
			V_13 = L_95;
			MemoryStream_t743994179 * L_96 = V_6;
			ByteU5BU5D_t3397334013* L_97 = V_5;
			ByteU5BU5D_t3397334013* L_98 = V_5;
			NullCheck(L_98);
			NullCheck(L_96);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_96, L_97, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_98)->max_length)))));
			MemoryStream_t743994179 * L_99 = V_6;
			ByteU5BU5D_t3397334013* L_100 = V_13;
			ByteU5BU5D_t3397334013* L_101 = V_13;
			NullCheck(L_101);
			NullCheck(L_99);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_99, L_100, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_101)->max_length)))));
			MemoryStream_t743994179 * L_102 = V_6;
			ByteU5BU5D_t3397334013* L_103 = V_4;
			ByteU5BU5D_t3397334013* L_104 = V_4;
			NullCheck(L_104);
			NullCheck(L_102);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_102, L_103, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_104)->max_length)))));
		}

IL_025c:
		{
			MemoryStream_t743994179 * L_105 = V_6;
			ByteU5BU5D_t3397334013* L_106 = V_1;
			ByteU5BU5D_t3397334013* L_107 = V_1;
			NullCheck(L_107);
			NullCheck(L_105);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_105, L_106, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_107)->max_length)))));
			MemoryStream_t743994179 * L_108 = V_6;
			ByteU5BU5D_t3397334013* L_109 = V_1;
			ByteU5BU5D_t3397334013* L_110 = V_1;
			NullCheck(L_110);
			NullCheck(L_108);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_108, L_109, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_110)->max_length)))));
			List_1_t2766455145 * L_111 = __this->get_formData_0();
			int32_t L_112 = V_7;
			NullCheck(L_111);
			ByteU5BU5D_t3397334013* L_113 = List_1_get_Item_m3195270850(L_111, L_112, /*hidden argument*/List_1_get_Item_m3195270850_MethodInfo_var);
			V_14 = L_113;
			MemoryStream_t743994179 * L_114 = V_6;
			ByteU5BU5D_t3397334013* L_115 = V_14;
			ByteU5BU5D_t3397334013* L_116 = V_14;
			NullCheck(L_116);
			NullCheck(L_114);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_114, L_115, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_116)->max_length)))));
			int32_t L_117 = V_7;
			V_7 = ((int32_t)((int32_t)L_117+(int32_t)1));
		}

IL_0297:
		{
			int32_t L_118 = V_7;
			List_1_t2766455145 * L_119 = __this->get_formData_0();
			NullCheck(L_119);
			int32_t L_120 = List_1_get_Count_m3625537567(L_119, /*hidden argument*/List_1_get_Count_m3625537567_MethodInfo_var);
			if ((((int32_t)L_118) < ((int32_t)L_120)))
			{
				goto IL_0081;
			}
		}

IL_02a9:
		{
			MemoryStream_t743994179 * L_121 = V_6;
			ByteU5BU5D_t3397334013* L_122 = V_1;
			ByteU5BU5D_t3397334013* L_123 = V_1;
			NullCheck(L_123);
			NullCheck(L_121);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_121, L_122, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_123)->max_length)))));
			MemoryStream_t743994179 * L_124 = V_6;
			ByteU5BU5D_t3397334013* L_125 = V_0;
			ByteU5BU5D_t3397334013* L_126 = V_0;
			NullCheck(L_126);
			NullCheck(L_124);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_124, L_125, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_126)->max_length)))));
			MemoryStream_t743994179 * L_127 = V_6;
			ByteU5BU5D_t3397334013* L_128 = __this->get_boundary_4();
			ByteU5BU5D_t3397334013* L_129 = __this->get_boundary_4();
			NullCheck(L_129);
			NullCheck(L_127);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_127, L_128, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_129)->max_length)))));
			MemoryStream_t743994179 * L_130 = V_6;
			ByteU5BU5D_t3397334013* L_131 = V_0;
			ByteU5BU5D_t3397334013* L_132 = V_0;
			NullCheck(L_132);
			NullCheck(L_130);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_130, L_131, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_132)->max_length)))));
			MemoryStream_t743994179 * L_133 = V_6;
			ByteU5BU5D_t3397334013* L_134 = V_1;
			ByteU5BU5D_t3397334013* L_135 = V_1;
			NullCheck(L_135);
			NullCheck(L_133);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_133, L_134, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_135)->max_length)))));
			MemoryStream_t743994179 * L_136 = V_6;
			NullCheck(L_136);
			ByteU5BU5D_t3397334013* L_137 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_136);
			V_22 = L_137;
			IL2CPP_LEAVE(0x3F7, FINALLY_0302);
		}

IL_02fd:
		{
			; // IL_02fd: leave IL_0311
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0302;
	}

FINALLY_0302:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_138 = V_6;
			if (!L_138)
			{
				goto IL_0310;
			}
		}

IL_0309:
		{
			MemoryStream_t743994179 * L_139 = V_6;
			NullCheck(L_139);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_139);
		}

IL_0310:
		{
			IL2CPP_END_FINALLY(770)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(770)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0311:
	{
		Encoding_t663144255 * L_140 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_140);
		ByteU5BU5D_t3397334013* L_141 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_140, _stringLiteral372029308);
		V_15 = L_141;
		Encoding_t663144255 * L_142 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_142);
		ByteU5BU5D_t3397334013* L_143 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_142, _stringLiteral372029329);
		V_16 = L_143;
		MemoryStream_t743994179 * L_144 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1489366847(L_144, ((int32_t)1024), /*hidden argument*/NULL);
		V_17 = L_144;
	}

IL_033f:
	try
	{ // begin try (depth: 1)
		{
			V_18 = 0;
			goto IL_03c3;
		}

IL_0347:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_145 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t1398341365 * L_146 = __this->get_fieldNames_1();
			int32_t L_147 = V_18;
			NullCheck(L_146);
			String_t* L_148 = List_1_get_Item_m4077476129(L_146, L_147, /*hidden argument*/List_1_get_Item_m4077476129_MethodInfo_var);
			NullCheck(L_145);
			ByteU5BU5D_t3397334013* L_149 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_145, L_148);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_150 = WWWTranscoder_URLEncode_m82461225(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
			V_19 = L_150;
			List_1_t2766455145 * L_151 = __this->get_formData_0();
			int32_t L_152 = V_18;
			NullCheck(L_151);
			ByteU5BU5D_t3397334013* L_153 = List_1_get_Item_m3195270850(L_151, L_152, /*hidden argument*/List_1_get_Item_m3195270850_MethodInfo_var);
			V_20 = L_153;
			ByteU5BU5D_t3397334013* L_154 = V_20;
			ByteU5BU5D_t3397334013* L_155 = WWWTranscoder_URLEncode_m82461225(NULL /*static, unused*/, L_154, /*hidden argument*/NULL);
			V_21 = L_155;
			int32_t L_156 = V_18;
			if ((((int32_t)L_156) <= ((int32_t)0)))
			{
				goto IL_0393;
			}
		}

IL_0385:
		{
			MemoryStream_t743994179 * L_157 = V_17;
			ByteU5BU5D_t3397334013* L_158 = V_15;
			ByteU5BU5D_t3397334013* L_159 = V_15;
			NullCheck(L_159);
			NullCheck(L_157);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_157, L_158, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_159)->max_length)))));
		}

IL_0393:
		{
			MemoryStream_t743994179 * L_160 = V_17;
			ByteU5BU5D_t3397334013* L_161 = V_19;
			ByteU5BU5D_t3397334013* L_162 = V_19;
			NullCheck(L_162);
			NullCheck(L_160);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_160, L_161, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_162)->max_length)))));
			MemoryStream_t743994179 * L_163 = V_17;
			ByteU5BU5D_t3397334013* L_164 = V_16;
			ByteU5BU5D_t3397334013* L_165 = V_16;
			NullCheck(L_165);
			NullCheck(L_163);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_163, L_164, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_165)->max_length)))));
			MemoryStream_t743994179 * L_166 = V_17;
			ByteU5BU5D_t3397334013* L_167 = V_21;
			ByteU5BU5D_t3397334013* L_168 = V_21;
			NullCheck(L_168);
			NullCheck(L_166);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_166, L_167, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_168)->max_length)))));
			int32_t L_169 = V_18;
			V_18 = ((int32_t)((int32_t)L_169+(int32_t)1));
		}

IL_03c3:
		{
			int32_t L_170 = V_18;
			List_1_t2766455145 * L_171 = __this->get_formData_0();
			NullCheck(L_171);
			int32_t L_172 = List_1_get_Count_m3625537567(L_171, /*hidden argument*/List_1_get_Count_m3625537567_MethodInfo_var);
			if ((((int32_t)L_170) < ((int32_t)L_172)))
			{
				goto IL_0347;
			}
		}

IL_03d5:
		{
			MemoryStream_t743994179 * L_173 = V_17;
			NullCheck(L_173);
			ByteU5BU5D_t3397334013* L_174 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_173);
			V_22 = L_174;
			IL2CPP_LEAVE(0x3F7, FINALLY_03e8);
		}

IL_03e3:
		{
			; // IL_03e3: leave IL_03f7
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_03e8;
	}

FINALLY_03e8:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_175 = V_17;
			if (!L_175)
			{
				goto IL_03f6;
			}
		}

IL_03ef:
		{
			MemoryStream_t743994179 * L_176 = V_17;
			NullCheck(L_176);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_176);
		}

IL_03f6:
		{
			IL2CPP_END_FINALLY(1000)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1000)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_03f7:
	{
		ByteU5BU5D_t3397334013* L_177 = V_22;
		return L_177;
	}
}
// System.Void UnityEngine.WWWTranscoder::.cctor()
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2989085030;
extern Il2CppCodeGenString* _stringLiteral85642790;
extern Il2CppCodeGenString* _stringLiteral1149677451;
extern Il2CppCodeGenString* _stringLiteral329227207;
extern const uint32_t WWWTranscoder__cctor_m916878076_MetadataUsageId;
extern "C"  void WWWTranscoder__cctor_m916878076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder__cctor_m916878076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t663144255 * L_0 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_1 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral2989085030);
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_ucHexChars_0(L_1);
		Encoding_t663144255 * L_2 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_3 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, _stringLiteral85642790);
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_lcHexChars_1(L_3);
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_urlEscapeChar_2(((int32_t)37));
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_urlSpace_3(((int32_t)43));
		Encoding_t663144255 * L_4 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_5 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, _stringLiteral1149677451);
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_urlForbidden_4(L_5);
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_qpEscapeChar_5(((int32_t)61));
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_qpSpace_6(((int32_t)95));
		Encoding_t663144255 * L_6 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_7 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, _stringLiteral329227207);
		((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->set_qpForbidden_7(L_7);
		return;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_Byte2Hex_m1676183558_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WWWTranscoder_Byte2Hex_m1676183558 (Il2CppObject * __this /* static, unused */, uint8_t ___b0, ByteU5BU5D_t3397334013* ___hexChars1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_Byte2Hex_m1676183558_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)2));
		ByteU5BU5D_t3397334013* L_0 = V_0;
		ByteU5BU5D_t3397334013* L_1 = ___hexChars1;
		uint8_t L_2 = ___b0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2>>(int32_t)4)));
		int32_t L_3 = ((int32_t)((int32_t)L_2>>(int32_t)4));
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		ByteU5BU5D_t3397334013* L_5 = V_0;
		ByteU5BU5D_t3397334013* L_6 = ___hexChars1;
		uint8_t L_7 = ___b0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7&(int32_t)((int32_t)15))));
		int32_t L_8 = ((int32_t)((int32_t)L_7&(int32_t)((int32_t)15)));
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)L_9);
		ByteU5BU5D_t3397334013* L_10 = V_0;
		return L_10;
	}
}
// System.String UnityEngine.WWWTranscoder::URLEncode(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_URLEncode_m3846210256_MetadataUsageId;
extern "C"  String_t* WWWTranscoder_URLEncode_m3846210256 (Il2CppObject * __this /* static, unused */, String_t* ___toEncode0, Encoding_t663144255 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_URLEncode_m3846210256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		Encoding_t663144255 * L_0 = ___e1;
		String_t* L_1 = ___toEncode0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_urlEscapeChar_2();
		uint8_t L_4 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_urlSpace_3();
		ByteU5BU5D_t3397334013* L_5 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_urlForbidden_4();
		ByteU5BU5D_t3397334013* L_6 = WWWTranscoder_Encode_m4207510302(NULL /*static, unused*/, L_2, L_3, L_4, L_5, (bool)0, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t663144255 * L_7 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_8 = V_0;
		ByteU5BU5D_t3397334013* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_URLEncode_m82461225_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WWWTranscoder_URLEncode_m82461225 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___toEncode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_URLEncode_m82461225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___toEncode0;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
		uint8_t L_1 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_urlEscapeChar_2();
		uint8_t L_2 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_urlSpace_3();
		ByteU5BU5D_t3397334013* L_3 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_urlForbidden_4();
		ByteU5BU5D_t3397334013* L_4 = WWWTranscoder_Encode_m4207510302(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (bool)0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_QPEncode_m629571574_MetadataUsageId;
extern "C"  String_t* WWWTranscoder_QPEncode_m629571574 (Il2CppObject * __this /* static, unused */, String_t* ___toEncode0, Encoding_t663144255 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_QPEncode_m629571574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		Encoding_t663144255 * L_0 = ___e1;
		String_t* L_1 = ___toEncode0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_qpEscapeChar_5();
		uint8_t L_4 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_qpSpace_6();
		ByteU5BU5D_t3397334013* L_5 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_qpForbidden_7();
		ByteU5BU5D_t3397334013* L_6 = WWWTranscoder_Encode_m4207510302(NULL /*static, unused*/, L_2, L_3, L_4, L_5, (bool)1, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t663144255 * L_7 = WWW_get_DefaultEncoding_m1497697991(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_8 = V_0;
		ByteU5BU5D_t3397334013* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern Il2CppClass* MemoryStream_t743994179_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_Encode_m4207510302_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* WWWTranscoder_Encode_m4207510302 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___input0, uint8_t ___escapeChar1, uint8_t ___space2, ByteU5BU5D_t3397334013* ___forbidden3, bool ___uppercase4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_Encode_m4207510302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t743994179 * V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B9_0 = 0;
	MemoryStream_t743994179 * G_B9_1 = NULL;
	int32_t G_B8_0 = 0;
	MemoryStream_t743994179 * G_B8_1 = NULL;
	ByteU5BU5D_t3397334013* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	MemoryStream_t743994179 * G_B10_2 = NULL;
	{
		ByteU5BU5D_t3397334013* L_0 = ___input0;
		NullCheck(L_0);
		MemoryStream_t743994179 * L_1 = (MemoryStream_t743994179 *)il2cpp_codegen_object_new(MemoryStream_t743994179_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m1489366847(L_1, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))*(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_0089;
		}

IL_0012:
		{
			ByteU5BU5D_t3397334013* L_2 = ___input0;
			int32_t L_3 = V_1;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			int32_t L_4 = L_3;
			uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
			if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)32)))))
			{
				goto IL_0028;
			}
		}

IL_001c:
		{
			MemoryStream_t743994179 * L_6 = V_0;
			uint8_t L_7 = ___space2;
			NullCheck(L_6);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_6, L_7);
			goto IL_0085;
		}

IL_0028:
		{
			ByteU5BU5D_t3397334013* L_8 = ___input0;
			int32_t L_9 = V_1;
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
			int32_t L_10 = L_9;
			uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
			if ((((int32_t)L_11) < ((int32_t)((int32_t)32))))
			{
				goto IL_004a;
			}
		}

IL_0032:
		{
			ByteU5BU5D_t3397334013* L_12 = ___input0;
			int32_t L_13 = V_1;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
			int32_t L_14 = L_13;
			uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
			if ((((int32_t)L_15) > ((int32_t)((int32_t)126))))
			{
				goto IL_004a;
			}
		}

IL_003c:
		{
			ByteU5BU5D_t3397334013* L_16 = ___forbidden3;
			ByteU5BU5D_t3397334013* L_17 = ___input0;
			int32_t L_18 = V_1;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
			int32_t L_19 = L_18;
			uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			bool L_21 = WWWTranscoder_ByteArrayContains_m1587254237(NULL /*static, unused*/, L_16, L_20, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_007c;
			}
		}

IL_004a:
		{
			MemoryStream_t743994179 * L_22 = V_0;
			uint8_t L_23 = ___escapeChar1;
			NullCheck(L_22);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_22, L_23);
			MemoryStream_t743994179 * L_24 = V_0;
			ByteU5BU5D_t3397334013* L_25 = ___input0;
			int32_t L_26 = V_1;
			NullCheck(L_25);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
			int32_t L_27 = L_26;
			uint8_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
			bool L_29 = ___uppercase4;
			G_B8_0 = ((int32_t)(L_28));
			G_B8_1 = L_24;
			if (!L_29)
			{
				G_B9_0 = ((int32_t)(L_28));
				G_B9_1 = L_24;
				goto IL_0066;
			}
		}

IL_005c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_30 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_ucHexChars_0();
			G_B10_0 = L_30;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			goto IL_006b;
		}

IL_0066:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_31 = ((WWWTranscoder_t1124214756_StaticFields*)WWWTranscoder_t1124214756_il2cpp_TypeInfo_var->static_fields)->get_lcHexChars_1();
			G_B10_0 = L_31;
			G_B10_1 = G_B9_0;
			G_B10_2 = G_B9_1;
		}

IL_006b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
			ByteU5BU5D_t3397334013* L_32 = WWWTranscoder_Byte2Hex_m1676183558(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
			NullCheck(G_B10_2);
			VirtActionInvoker3< ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, G_B10_2, L_32, 0, 2);
			goto IL_0085;
		}

IL_007c:
		{
			MemoryStream_t743994179 * L_33 = V_0;
			ByteU5BU5D_t3397334013* L_34 = ___input0;
			int32_t L_35 = V_1;
			NullCheck(L_34);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
			int32_t L_36 = L_35;
			uint8_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
			NullCheck(L_33);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_33, L_37);
		}

IL_0085:
		{
			int32_t L_38 = V_1;
			V_1 = ((int32_t)((int32_t)L_38+(int32_t)1));
		}

IL_0089:
		{
			int32_t L_39 = V_1;
			ByteU5BU5D_t3397334013* L_40 = ___input0;
			NullCheck(L_40);
			if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_40)->max_length)))))))
			{
				goto IL_0012;
			}
		}

IL_0092:
		{
			MemoryStream_t743994179 * L_41 = V_0;
			NullCheck(L_41);
			ByteU5BU5D_t3397334013* L_42 = VirtFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_41);
			V_2 = L_42;
			IL2CPP_LEAVE(0xB0, FINALLY_00a3);
		}

IL_009e:
		{
			; // IL_009e: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t743994179 * L_43 = V_0;
			if (!L_43)
			{
				goto IL_00af;
			}
		}

IL_00a9:
		{
			MemoryStream_t743994179 * L_44 = V_0;
			NullCheck(L_44);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_44);
		}

IL_00af:
		{
			IL2CPP_END_FINALLY(163)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b0:
	{
		ByteU5BU5D_t3397334013* L_45 = V_2;
		return L_45;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C"  bool WWWTranscoder_ByteArrayContains_m1587254237 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___array0, uint8_t ___b1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t3397334013* L_0 = ___array0;
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		ByteU5BU5D_t3397334013* L_1 = ___array0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		uint8_t L_5 = ___b1;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000b;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern const uint32_t WWWTranscoder_SevenBitClean_m556517891_MetadataUsageId;
extern "C"  bool WWWTranscoder_SevenBitClean_m556517891 (Il2CppObject * __this /* static, unused */, String_t* ___s0, Encoding_t663144255 * ___e1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WWWTranscoder_SevenBitClean_m556517891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t663144255 * L_0 = ___e1;
		String_t* L_1 = ___s0;
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_2 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
		bool L_3 = WWWTranscoder_SevenBitClean_m1700940017(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C"  bool WWWTranscoder_SevenBitClean_m1700940017 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___input0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		ByteU5BU5D_t3397334013* L_0 = ___input0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		if ((((int32_t)L_3) < ((int32_t)((int32_t)32))))
		{
			goto IL_001b;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = ___input0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)126))))
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (bool)0;
	}

IL_001d:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_9 = V_0;
		ByteU5BU5D_t3397334013* L_10 = ___input0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m2014522928 (YieldInstruction_t3462875981 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke(const YieldInstruction_t3462875981& unmarshaled, YieldInstruction_t3462875981_marshaled_pinvoke& marshaled)
{
}
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_back(const YieldInstruction_t3462875981_marshaled_pinvoke& marshaled, YieldInstruction_t3462875981& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3462875981_marshal_pinvoke_cleanup(YieldInstruction_t3462875981_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3462875981_marshal_com(const YieldInstruction_t3462875981& unmarshaled, YieldInstruction_t3462875981_marshaled_com& marshaled)
{
}
extern "C" void YieldInstruction_t3462875981_marshal_com_back(const YieldInstruction_t3462875981_marshaled_com& marshaled, YieldInstruction_t3462875981& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t3462875981_marshal_com_cleanup(YieldInstruction_t3462875981_marshaled_com& marshaled)
{
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C"  void GenericStack__ctor_m1256224477 (GenericStack_t3718539591 * __this, const MethodInfo* method)
{
	{
		Stack__ctor_m521896492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern Il2CppClass* MathfInternal_t715669973_il2cpp_TypeInfo_var;
extern const uint32_t MathfInternal__cctor_m1836685460_MetadataUsageId;
extern "C"  void MathfInternal__cctor_m1836685460 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MathfInternal__cctor_m1836685460_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->set_FloatMinNormal_0((1.17549435E-38f));
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->set_FloatMinDenormal_1((1.401298E-45f));
		((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->set_IsFlushToZeroEnabled_2((bool)1);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t715669973_marshal_pinvoke(const MathfInternal_t715669973& unmarshaled, MathfInternal_t715669973_marshaled_pinvoke& marshaled)
{
}
extern "C" void MathfInternal_t715669973_marshal_pinvoke_back(const MathfInternal_t715669973_marshaled_pinvoke& marshaled, MathfInternal_t715669973& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t715669973_marshal_pinvoke_cleanup(MathfInternal_t715669973_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t715669973_marshal_com(const MathfInternal_t715669973& unmarshaled, MathfInternal_t715669973_marshaled_com& marshaled)
{
}
extern "C" void MathfInternal_t715669973_marshal_com_back(const MathfInternal_t715669973_marshaled_com& marshaled, MathfInternal_t715669973& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngineInternal.MathfInternal
extern "C" void MathfInternal_t715669973_marshal_com_cleanup(MathfInternal_t715669973_marshaled_com& marshaled)
{
}
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t3022476291 * NetFxCoreExtensions_CreateDelegate_m2492743074 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, Il2CppObject * ___target2, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___delegateType1;
		Il2CppObject * L_1 = ___target2;
		MethodInfo_t * L_2 = ___self0;
		Delegate_t3022476291 * L_3 = Delegate_CreateDelegate_m2101460062(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m2715372889 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * ___self0, const MethodInfo* method)
{
	{
		Delegate_t3022476291 * L_0 = ___self0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m2968370506(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern Il2CppClass* TypeInferenceRules_t1810425448_il2cpp_TypeInfo_var;
extern const uint32_t TypeInferenceRuleAttribute__ctor_m599630929_MetadataUsageId;
extern "C"  void TypeInferenceRuleAttribute__ctor_m599630929 (TypeInferenceRuleAttribute_t1390152093 * __this, int32_t ___rule0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TypeInferenceRuleAttribute__ctor_m599630929_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___rule0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(TypeInferenceRules_t1810425448_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2459695545 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2459695545 *)L_2);
		TypeInferenceRuleAttribute__ctor_m470566337(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m470566337 (TypeInferenceRuleAttribute_t1390152093 * __this, String_t* ___rule0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule0;
		__this->set__rule_0(L_0);
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C"  String_t* TypeInferenceRuleAttribute_ToString_m3941510216 (TypeInferenceRuleAttribute_t1390152093 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get__rule_0();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
