﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.UnityAdsEditorPlaceholder
struct UnityAdsEditorPlaceholder_t526501903;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::.ctor()
extern "C"  void UnityAdsEditorPlaceholder__ctor_m1414031528 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Advertisements.UnityAdsEditorPlaceholder::TextureFromFile(System.String)
extern "C"  Texture2D_t3542995729 * UnityAdsEditorPlaceholder_TextureFromFile_m1806185628 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::Load(System.String)
extern "C"  void UnityAdsEditorPlaceholder_Load_m1622005924 (UnityAdsEditorPlaceholder_t526501903 * __this, String_t* ___extensionPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::WindowFunc(System.Int32)
extern "C"  void UnityAdsEditorPlaceholder_WindowFunc_m980111513 (UnityAdsEditorPlaceholder_t526501903 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::OnGUI()
extern "C"  void UnityAdsEditorPlaceholder_OnGUI_m1249441572 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::Show()
extern "C"  void UnityAdsEditorPlaceholder_Show_m908840621 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::Hide()
extern "C"  void UnityAdsEditorPlaceholder_Hide_m2240550894 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
