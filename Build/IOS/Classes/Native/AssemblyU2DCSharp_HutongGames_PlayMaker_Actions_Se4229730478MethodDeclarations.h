﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMass2d
struct SetMass2d_t4229730478;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMass2d::.ctor()
extern "C"  void SetMass2d__ctor_m877160840 (SetMass2d_t4229730478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass2d::Reset()
extern "C"  void SetMass2d_Reset_m904685131 (SetMass2d_t4229730478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass2d::OnEnter()
extern "C"  void SetMass2d_OnEnter_m2416591443 (SetMass2d_t4229730478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMass2d::DoSetMass()
extern "C"  void SetMass2d_DoSetMass_m1070082979 (SetMass2d_t4229730478 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
