﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear
struct DOTweenAdditionalMethodsClear_t1546875992;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear::.ctor()
extern "C"  void DOTweenAdditionalMethodsClear__ctor_m3233013512 (DOTweenAdditionalMethodsClear_t1546875992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear::Reset()
extern "C"  void DOTweenAdditionalMethodsClear_Reset_m3035837781 (DOTweenAdditionalMethodsClear_t1546875992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAdditionalMethodsClear::OnEnter()
extern "C"  void DOTweenAdditionalMethodsClear_OnEnter_m1413985685 (DOTweenAdditionalMethodsClear_t1546875992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
