﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddRelativeForce2d
struct AddRelativeForce2d_t5056280;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddRelativeForce2d::.ctor()
extern "C"  void AddRelativeForce2d__ctor_m2224411656 (AddRelativeForce2d_t5056280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddRelativeForce2d::Reset()
extern "C"  void AddRelativeForce2d_Reset_m1395176905 (AddRelativeForce2d_t5056280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddRelativeForce2d::OnPreprocess()
extern "C"  void AddRelativeForce2d_OnPreprocess_m835756977 (AddRelativeForce2d_t5056280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddRelativeForce2d::OnEnter()
extern "C"  void AddRelativeForce2d_OnEnter_m2241647569 (AddRelativeForce2d_t5056280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddRelativeForce2d::OnFixedUpdate()
extern "C"  void AddRelativeForce2d_OnFixedUpdate_m2055208132 (AddRelativeForce2d_t5056280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddRelativeForce2d::DoAddRelativeForce()
extern "C"  void AddRelativeForce2d_DoAddRelativeForce_m4079392223 (AddRelativeForce2d_t5056280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
