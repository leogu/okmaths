﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t4177556671;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3830815681;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t381696854;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t643261629;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t4100123680;
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t1455296735;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t3489263757;
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t3601875862;
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t2051300532;
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1567627762;
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t3720629962;
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t2711026008;
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t2440162814;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVariables
struct  FsmVariables_t630687169  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::floatVariables
	FsmFloatU5BU5D_t4177556671* ___floatVariables_0;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::intVariables
	FsmIntU5BU5D_t2637547802* ___intVariables_1;
	// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::boolVariables
	FsmBoolU5BU5D_t3830815681* ___boolVariables_2;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::stringVariables
	FsmStringU5BU5D_t2699231328* ___stringVariables_3;
	// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::vector2Variables
	FsmVector2U5BU5D_t381696854* ___vector2Variables_4;
	// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::vector3Variables
	FsmVector3U5BU5D_t643261629* ___vector3Variables_5;
	// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::colorVariables
	FsmColorU5BU5D_t4100123680* ___colorVariables_6;
	// HutongGames.PlayMaker.FsmRect[] HutongGames.PlayMaker.FsmVariables::rectVariables
	FsmRectU5BU5D_t1455296735* ___rectVariables_7;
	// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::quaternionVariables
	FsmQuaternionU5BU5D_t3489263757* ___quaternionVariables_8;
	// HutongGames.PlayMaker.FsmGameObject[] HutongGames.PlayMaker.FsmVariables::gameObjectVariables
	FsmGameObjectU5BU5D_t3601875862* ___gameObjectVariables_9;
	// HutongGames.PlayMaker.FsmObject[] HutongGames.PlayMaker.FsmVariables::objectVariables
	FsmObjectU5BU5D_t2051300532* ___objectVariables_10;
	// HutongGames.PlayMaker.FsmMaterial[] HutongGames.PlayMaker.FsmVariables::materialVariables
	FsmMaterialU5BU5D_t1567627762* ___materialVariables_11;
	// HutongGames.PlayMaker.FsmTexture[] HutongGames.PlayMaker.FsmVariables::textureVariables
	FsmTextureU5BU5D_t3720629962* ___textureVariables_12;
	// HutongGames.PlayMaker.FsmArray[] HutongGames.PlayMaker.FsmVariables::arrayVariables
	FsmArrayU5BU5D_t2711026008* ___arrayVariables_13;
	// HutongGames.PlayMaker.FsmEnum[] HutongGames.PlayMaker.FsmVariables::enumVariables
	FsmEnumU5BU5D_t2440162814* ___enumVariables_14;
	// System.String[] HutongGames.PlayMaker.FsmVariables::categories
	StringU5BU5D_t1642385972* ___categories_15;
	// System.Int32[] HutongGames.PlayMaker.FsmVariables::variableCategoryIDs
	Int32U5BU5D_t3030399641* ___variableCategoryIDs_16;

public:
	inline static int32_t get_offset_of_floatVariables_0() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___floatVariables_0)); }
	inline FsmFloatU5BU5D_t4177556671* get_floatVariables_0() const { return ___floatVariables_0; }
	inline FsmFloatU5BU5D_t4177556671** get_address_of_floatVariables_0() { return &___floatVariables_0; }
	inline void set_floatVariables_0(FsmFloatU5BU5D_t4177556671* value)
	{
		___floatVariables_0 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariables_0, value);
	}

	inline static int32_t get_offset_of_intVariables_1() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___intVariables_1)); }
	inline FsmIntU5BU5D_t2637547802* get_intVariables_1() const { return ___intVariables_1; }
	inline FsmIntU5BU5D_t2637547802** get_address_of_intVariables_1() { return &___intVariables_1; }
	inline void set_intVariables_1(FsmIntU5BU5D_t2637547802* value)
	{
		___intVariables_1 = value;
		Il2CppCodeGenWriteBarrier(&___intVariables_1, value);
	}

	inline static int32_t get_offset_of_boolVariables_2() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___boolVariables_2)); }
	inline FsmBoolU5BU5D_t3830815681* get_boolVariables_2() const { return ___boolVariables_2; }
	inline FsmBoolU5BU5D_t3830815681** get_address_of_boolVariables_2() { return &___boolVariables_2; }
	inline void set_boolVariables_2(FsmBoolU5BU5D_t3830815681* value)
	{
		___boolVariables_2 = value;
		Il2CppCodeGenWriteBarrier(&___boolVariables_2, value);
	}

	inline static int32_t get_offset_of_stringVariables_3() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___stringVariables_3)); }
	inline FsmStringU5BU5D_t2699231328* get_stringVariables_3() const { return ___stringVariables_3; }
	inline FsmStringU5BU5D_t2699231328** get_address_of_stringVariables_3() { return &___stringVariables_3; }
	inline void set_stringVariables_3(FsmStringU5BU5D_t2699231328* value)
	{
		___stringVariables_3 = value;
		Il2CppCodeGenWriteBarrier(&___stringVariables_3, value);
	}

	inline static int32_t get_offset_of_vector2Variables_4() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___vector2Variables_4)); }
	inline FsmVector2U5BU5D_t381696854* get_vector2Variables_4() const { return ___vector2Variables_4; }
	inline FsmVector2U5BU5D_t381696854** get_address_of_vector2Variables_4() { return &___vector2Variables_4; }
	inline void set_vector2Variables_4(FsmVector2U5BU5D_t381696854* value)
	{
		___vector2Variables_4 = value;
		Il2CppCodeGenWriteBarrier(&___vector2Variables_4, value);
	}

	inline static int32_t get_offset_of_vector3Variables_5() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___vector3Variables_5)); }
	inline FsmVector3U5BU5D_t643261629* get_vector3Variables_5() const { return ___vector3Variables_5; }
	inline FsmVector3U5BU5D_t643261629** get_address_of_vector3Variables_5() { return &___vector3Variables_5; }
	inline void set_vector3Variables_5(FsmVector3U5BU5D_t643261629* value)
	{
		___vector3Variables_5 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variables_5, value);
	}

	inline static int32_t get_offset_of_colorVariables_6() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___colorVariables_6)); }
	inline FsmColorU5BU5D_t4100123680* get_colorVariables_6() const { return ___colorVariables_6; }
	inline FsmColorU5BU5D_t4100123680** get_address_of_colorVariables_6() { return &___colorVariables_6; }
	inline void set_colorVariables_6(FsmColorU5BU5D_t4100123680* value)
	{
		___colorVariables_6 = value;
		Il2CppCodeGenWriteBarrier(&___colorVariables_6, value);
	}

	inline static int32_t get_offset_of_rectVariables_7() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___rectVariables_7)); }
	inline FsmRectU5BU5D_t1455296735* get_rectVariables_7() const { return ___rectVariables_7; }
	inline FsmRectU5BU5D_t1455296735** get_address_of_rectVariables_7() { return &___rectVariables_7; }
	inline void set_rectVariables_7(FsmRectU5BU5D_t1455296735* value)
	{
		___rectVariables_7 = value;
		Il2CppCodeGenWriteBarrier(&___rectVariables_7, value);
	}

	inline static int32_t get_offset_of_quaternionVariables_8() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___quaternionVariables_8)); }
	inline FsmQuaternionU5BU5D_t3489263757* get_quaternionVariables_8() const { return ___quaternionVariables_8; }
	inline FsmQuaternionU5BU5D_t3489263757** get_address_of_quaternionVariables_8() { return &___quaternionVariables_8; }
	inline void set_quaternionVariables_8(FsmQuaternionU5BU5D_t3489263757* value)
	{
		___quaternionVariables_8 = value;
		Il2CppCodeGenWriteBarrier(&___quaternionVariables_8, value);
	}

	inline static int32_t get_offset_of_gameObjectVariables_9() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___gameObjectVariables_9)); }
	inline FsmGameObjectU5BU5D_t3601875862* get_gameObjectVariables_9() const { return ___gameObjectVariables_9; }
	inline FsmGameObjectU5BU5D_t3601875862** get_address_of_gameObjectVariables_9() { return &___gameObjectVariables_9; }
	inline void set_gameObjectVariables_9(FsmGameObjectU5BU5D_t3601875862* value)
	{
		___gameObjectVariables_9 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectVariables_9, value);
	}

	inline static int32_t get_offset_of_objectVariables_10() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___objectVariables_10)); }
	inline FsmObjectU5BU5D_t2051300532* get_objectVariables_10() const { return ___objectVariables_10; }
	inline FsmObjectU5BU5D_t2051300532** get_address_of_objectVariables_10() { return &___objectVariables_10; }
	inline void set_objectVariables_10(FsmObjectU5BU5D_t2051300532* value)
	{
		___objectVariables_10 = value;
		Il2CppCodeGenWriteBarrier(&___objectVariables_10, value);
	}

	inline static int32_t get_offset_of_materialVariables_11() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___materialVariables_11)); }
	inline FsmMaterialU5BU5D_t1567627762* get_materialVariables_11() const { return ___materialVariables_11; }
	inline FsmMaterialU5BU5D_t1567627762** get_address_of_materialVariables_11() { return &___materialVariables_11; }
	inline void set_materialVariables_11(FsmMaterialU5BU5D_t1567627762* value)
	{
		___materialVariables_11 = value;
		Il2CppCodeGenWriteBarrier(&___materialVariables_11, value);
	}

	inline static int32_t get_offset_of_textureVariables_12() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___textureVariables_12)); }
	inline FsmTextureU5BU5D_t3720629962* get_textureVariables_12() const { return ___textureVariables_12; }
	inline FsmTextureU5BU5D_t3720629962** get_address_of_textureVariables_12() { return &___textureVariables_12; }
	inline void set_textureVariables_12(FsmTextureU5BU5D_t3720629962* value)
	{
		___textureVariables_12 = value;
		Il2CppCodeGenWriteBarrier(&___textureVariables_12, value);
	}

	inline static int32_t get_offset_of_arrayVariables_13() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___arrayVariables_13)); }
	inline FsmArrayU5BU5D_t2711026008* get_arrayVariables_13() const { return ___arrayVariables_13; }
	inline FsmArrayU5BU5D_t2711026008** get_address_of_arrayVariables_13() { return &___arrayVariables_13; }
	inline void set_arrayVariables_13(FsmArrayU5BU5D_t2711026008* value)
	{
		___arrayVariables_13 = value;
		Il2CppCodeGenWriteBarrier(&___arrayVariables_13, value);
	}

	inline static int32_t get_offset_of_enumVariables_14() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___enumVariables_14)); }
	inline FsmEnumU5BU5D_t2440162814* get_enumVariables_14() const { return ___enumVariables_14; }
	inline FsmEnumU5BU5D_t2440162814** get_address_of_enumVariables_14() { return &___enumVariables_14; }
	inline void set_enumVariables_14(FsmEnumU5BU5D_t2440162814* value)
	{
		___enumVariables_14 = value;
		Il2CppCodeGenWriteBarrier(&___enumVariables_14, value);
	}

	inline static int32_t get_offset_of_categories_15() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___categories_15)); }
	inline StringU5BU5D_t1642385972* get_categories_15() const { return ___categories_15; }
	inline StringU5BU5D_t1642385972** get_address_of_categories_15() { return &___categories_15; }
	inline void set_categories_15(StringU5BU5D_t1642385972* value)
	{
		___categories_15 = value;
		Il2CppCodeGenWriteBarrier(&___categories_15, value);
	}

	inline static int32_t get_offset_of_variableCategoryIDs_16() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169, ___variableCategoryIDs_16)); }
	inline Int32U5BU5D_t3030399641* get_variableCategoryIDs_16() const { return ___variableCategoryIDs_16; }
	inline Int32U5BU5D_t3030399641** get_address_of_variableCategoryIDs_16() { return &___variableCategoryIDs_16; }
	inline void set_variableCategoryIDs_16(Int32U5BU5D_t3030399641* value)
	{
		___variableCategoryIDs_16 = value;
		Il2CppCodeGenWriteBarrier(&___variableCategoryIDs_16, value);
	}
};

struct FsmVariables_t630687169_StaticFields
{
public:
	// System.Boolean HutongGames.PlayMaker.FsmVariables::<GlobalVariablesSynced>k__BackingField
	bool ___U3CGlobalVariablesSyncedU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(FsmVariables_t630687169_StaticFields, ___U3CGlobalVariablesSyncedU3Ek__BackingField_17)); }
	inline bool get_U3CGlobalVariablesSyncedU3Ek__BackingField_17() const { return ___U3CGlobalVariablesSyncedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CGlobalVariablesSyncedU3Ek__BackingField_17() { return &___U3CGlobalVariablesSyncedU3Ek__BackingField_17; }
	inline void set_U3CGlobalVariablesSyncedU3Ek__BackingField_17(bool value)
	{
		___U3CGlobalVariablesSyncedU3Ek__BackingField_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
