﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll
struct  DOTweenControlMethodsGoToAll_t3559548431  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll::to
	FsmFloat_t937133978 * ___to_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll::andPlay
	FsmBool_t664485696 * ___andPlay_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToAll::debugThis
	FsmBool_t664485696 * ___debugThis_13;

public:
	inline static int32_t get_offset_of_to_11() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsGoToAll_t3559548431, ___to_11)); }
	inline FsmFloat_t937133978 * get_to_11() const { return ___to_11; }
	inline FsmFloat_t937133978 ** get_address_of_to_11() { return &___to_11; }
	inline void set_to_11(FsmFloat_t937133978 * value)
	{
		___to_11 = value;
		Il2CppCodeGenWriteBarrier(&___to_11, value);
	}

	inline static int32_t get_offset_of_andPlay_12() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsGoToAll_t3559548431, ___andPlay_12)); }
	inline FsmBool_t664485696 * get_andPlay_12() const { return ___andPlay_12; }
	inline FsmBool_t664485696 ** get_address_of_andPlay_12() { return &___andPlay_12; }
	inline void set_andPlay_12(FsmBool_t664485696 * value)
	{
		___andPlay_12 = value;
		Il2CppCodeGenWriteBarrier(&___andPlay_12, value);
	}

	inline static int32_t get_offset_of_debugThis_13() { return static_cast<int32_t>(offsetof(DOTweenControlMethodsGoToAll_t3559548431, ___debugThis_13)); }
	inline FsmBool_t664485696 * get_debugThis_13() const { return ___debugThis_13; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_13() { return &___debugThis_13; }
	inline void set_debugThis_13(FsmBool_t664485696 * value)
	{
		___debugThis_13 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
