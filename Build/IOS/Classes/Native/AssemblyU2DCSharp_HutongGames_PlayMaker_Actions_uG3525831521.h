﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused
struct  uGuiInputFieldGetIsFocused_t3525831521  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::isFocused
	FsmBool_t664485696 * ___isFocused_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::isfocusedEvent
	FsmEvent_t1258573736 * ___isfocusedEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::isNotFocusedEvent
	FsmEvent_t1258573736 * ___isNotFocusedEvent_14;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetIsFocused::_inputField
	InputField_t1631627530 * ____inputField_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetIsFocused_t3525831521, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isFocused_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetIsFocused_t3525831521, ___isFocused_12)); }
	inline FsmBool_t664485696 * get_isFocused_12() const { return ___isFocused_12; }
	inline FsmBool_t664485696 ** get_address_of_isFocused_12() { return &___isFocused_12; }
	inline void set_isFocused_12(FsmBool_t664485696 * value)
	{
		___isFocused_12 = value;
		Il2CppCodeGenWriteBarrier(&___isFocused_12, value);
	}

	inline static int32_t get_offset_of_isfocusedEvent_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetIsFocused_t3525831521, ___isfocusedEvent_13)); }
	inline FsmEvent_t1258573736 * get_isfocusedEvent_13() const { return ___isfocusedEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isfocusedEvent_13() { return &___isfocusedEvent_13; }
	inline void set_isfocusedEvent_13(FsmEvent_t1258573736 * value)
	{
		___isfocusedEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isfocusedEvent_13, value);
	}

	inline static int32_t get_offset_of_isNotFocusedEvent_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetIsFocused_t3525831521, ___isNotFocusedEvent_14)); }
	inline FsmEvent_t1258573736 * get_isNotFocusedEvent_14() const { return ___isNotFocusedEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotFocusedEvent_14() { return &___isNotFocusedEvent_14; }
	inline void set_isNotFocusedEvent_14(FsmEvent_t1258573736 * value)
	{
		___isNotFocusedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isNotFocusedEvent_14, value);
	}

	inline static int32_t get_offset_of__inputField_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetIsFocused_t3525831521, ____inputField_15)); }
	inline InputField_t1631627530 * get__inputField_15() const { return ____inputField_15; }
	inline InputField_t1631627530 ** get_address_of__inputField_15() { return &____inputField_15; }
	inline void set__inputField_15(InputField_t1631627530 * value)
	{
		____inputField_15 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
