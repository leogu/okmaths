﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindAll
struct DOTweenControlMethodsSmoothRewindAll_t3738495105;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindAll::.ctor()
extern "C"  void DOTweenControlMethodsSmoothRewindAll__ctor_m51838727 (DOTweenControlMethodsSmoothRewindAll_t3738495105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindAll::Reset()
extern "C"  void DOTweenControlMethodsSmoothRewindAll_Reset_m2389068742 (DOTweenControlMethodsSmoothRewindAll_t3738495105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsSmoothRewindAll::OnEnter()
extern "C"  void DOTweenControlMethodsSmoothRewindAll_OnEnter_m4120905656 (DOTweenControlMethodsSmoothRewindAll_t3738495105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
