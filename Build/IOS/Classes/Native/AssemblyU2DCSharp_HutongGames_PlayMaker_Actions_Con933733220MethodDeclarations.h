﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertSecondsToString
struct ConvertSecondsToString_t933733220;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::.ctor()
extern "C"  void ConvertSecondsToString__ctor_m419721296 (ConvertSecondsToString_t933733220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::Reset()
extern "C"  void ConvertSecondsToString_Reset_m3029533769 (ConvertSecondsToString_t933733220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::OnEnter()
extern "C"  void ConvertSecondsToString_OnEnter_m358076553 (ConvertSecondsToString_t933733220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::OnUpdate()
extern "C"  void ConvertSecondsToString_OnUpdate_m4142656062 (ConvertSecondsToString_t933733220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertSecondsToString::DoConvertSecondsToString()
extern "C"  void ConvertSecondsToString_DoConvertSecondsToString_m3700834721 (ConvertSecondsToString_t933733220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
