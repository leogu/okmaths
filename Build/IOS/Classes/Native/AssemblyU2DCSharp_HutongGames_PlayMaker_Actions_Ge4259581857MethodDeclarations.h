﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorRoot
struct GetAnimatorRoot_t4259581857;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::.ctor()
extern "C"  void GetAnimatorRoot__ctor_m1923722089 (GetAnimatorRoot_t4259581857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::Reset()
extern "C"  void GetAnimatorRoot_Reset_m1598677366 (GetAnimatorRoot_t4259581857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::OnEnter()
extern "C"  void GetAnimatorRoot_OnEnter_m3505225404 (GetAnimatorRoot_t4259581857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::OnActionUpdate()
extern "C"  void GetAnimatorRoot_OnActionUpdate_m3000227319 (GetAnimatorRoot_t4259581857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRoot::DoGetBodyPosition()
extern "C"  void GetAnimatorRoot_DoGetBodyPosition_m1024374505 (GetAnimatorRoot_t4259581857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
