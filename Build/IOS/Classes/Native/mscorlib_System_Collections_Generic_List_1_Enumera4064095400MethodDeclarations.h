﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m723218061(__this, ___l0, method) ((  void (*) (Enumerator_t4064095400 *, List_1_t234398430 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2531280517(__this, method) ((  void (*) (Enumerator_t4064095400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m31935813(__this, method) ((  Il2CppObject * (*) (Enumerator_t4064095400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::Dispose()
#define Enumerator_Dispose_m149187786(__this, method) ((  void (*) (Enumerator_t4064095400 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::VerifyState()
#define Enumerator_VerifyState_m1549726111(__this, method) ((  void (*) (Enumerator_t4064095400 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::MoveNext()
#define Enumerator_MoveNext_m2114632653(__this, method) ((  bool (*) (Enumerator_t4064095400 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmLogEntry>::get_Current()
#define Enumerator_get_Current_m3699021504(__this, method) ((  FsmLogEntry_t865277298 * (*) (Enumerator_t4064095400 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
