﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTouchInfo
struct GetTouchInfo_t896696537;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::.ctor()
extern "C"  void GetTouchInfo__ctor_m1014299879 (GetTouchInfo_t896696537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::Reset()
extern "C"  void GetTouchInfo_Reset_m2697285414 (GetTouchInfo_t896696537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::OnEnter()
extern "C"  void GetTouchInfo_OnEnter_m1704401744 (GetTouchInfo_t896696537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::OnUpdate()
extern "C"  void GetTouchInfo_OnUpdate_m3307510247 (GetTouchInfo_t896696537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchInfo::DoGetTouchInfo()
extern "C"  void GetTouchInfo_DoGetTouchInfo_m3316616641 (GetTouchInfo_t896696537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
