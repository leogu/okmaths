﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiCanvasForceUpdateCanvases
struct uGuiCanvasForceUpdateCanvases_t3630979762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasForceUpdateCanvases::.ctor()
extern "C"  void uGuiCanvasForceUpdateCanvases__ctor_m173683222 (uGuiCanvasForceUpdateCanvases_t3630979762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasForceUpdateCanvases::OnEnter()
extern "C"  void uGuiCanvasForceUpdateCanvases_OnEnter_m1716966003 (uGuiCanvasForceUpdateCanvases_t3630979762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
