﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition
struct DOTweenRigidbodyLookAtPosition_t2358630582;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::.ctor()
extern "C"  void DOTweenRigidbodyLookAtPosition__ctor_m3552298334 (DOTweenRigidbodyLookAtPosition_t2358630582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::Reset()
extern "C"  void DOTweenRigidbodyLookAtPosition_Reset_m698592863 (DOTweenRigidbodyLookAtPosition_t2358630582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::OnEnter()
extern "C"  void DOTweenRigidbodyLookAtPosition_OnEnter_m3942492207 (DOTweenRigidbodyLookAtPosition_t2358630582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::<OnEnter>m__86()
extern "C"  void DOTweenRigidbodyLookAtPosition_U3COnEnterU3Em__86_m4266647336 (DOTweenRigidbodyLookAtPosition_t2358630582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::<OnEnter>m__87()
extern "C"  void DOTweenRigidbodyLookAtPosition_U3COnEnterU3Em__87_m2720766371 (DOTweenRigidbodyLookAtPosition_t2358630582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
