﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorTrigger
struct SetAnimatorTrigger_t3111496129;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::.ctor()
extern "C"  void SetAnimatorTrigger__ctor_m1677728915 (SetAnimatorTrigger_t3111496129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::Reset()
extern "C"  void SetAnimatorTrigger_Reset_m3118463722 (SetAnimatorTrigger_t3111496129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::OnEnter()
extern "C"  void SetAnimatorTrigger_OnEnter_m2221520188 (SetAnimatorTrigger_t3111496129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorTrigger::SetTrigger()
extern "C"  void SetAnimatorTrigger_SetTrigger_m510368757 (SetAnimatorTrigger_t3111496129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
