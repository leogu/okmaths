﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatAddMultiple
struct FloatAddMultiple_t2301168955;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::.ctor()
extern "C"  void FloatAddMultiple__ctor_m1911014927 (FloatAddMultiple_t2301168955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::Reset()
extern "C"  void FloatAddMultiple_Reset_m3590984232 (FloatAddMultiple_t2301168955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::OnEnter()
extern "C"  void FloatAddMultiple_OnEnter_m533095286 (FloatAddMultiple_t2301168955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::OnUpdate()
extern "C"  void FloatAddMultiple_OnUpdate_m2723590919 (FloatAddMultiple_t2301168955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAddMultiple::DoFloatAdd()
extern "C"  void FloatAddMultiple_DoFloatAdd_m741793665 (FloatAddMultiple_t2301168955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
