﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0
struct U3CGetAdPlanU3Ec__Iterator0_t3446287961;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::.ctor()
extern "C"  void U3CGetAdPlanU3Ec__Iterator0__ctor_m1630137333 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetAdPlanU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m240623603 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAdPlanU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3377943867 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::MoveNext()
extern "C"  bool U3CGetAdPlanU3Ec__Iterator0_MoveNext_m1262663651 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::Dispose()
extern "C"  void U3CGetAdPlanU3Ec__Iterator0_Dispose_m2511147710 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::Reset()
extern "C"  void U3CGetAdPlanU3Ec__Iterator0_Reset_m2466990564 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
