﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t3395645806  : public Il2CppObject
{
public:
	// UnityEngine.Audio.AudioMixer DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::target
	AudioMixer_t3244290001 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::floatName
	String_t* ___floatName_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3395645806, ___target_0)); }
	inline AudioMixer_t3244290001 * get_target_0() const { return ___target_0; }
	inline AudioMixer_t3244290001 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioMixer_t3244290001 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}

	inline static int32_t get_offset_of_floatName_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3395645806, ___floatName_1)); }
	inline String_t* get_floatName_1() const { return ___floatName_1; }
	inline String_t** get_address_of_floatName_1() { return &___floatName_1; }
	inline void set_floatName_1(String_t* value)
	{
		___floatName_1 = value;
		Il2CppCodeGenWriteBarrier(&___floatName_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
