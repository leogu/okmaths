﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// System.Object
struct Il2CppObject;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// DG.Tweening.Sequence
struct Sequence_t110643099;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t4023650049;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t426334720;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t426334751;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t426334817;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t426334852;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t426334883;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t426334918;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t426334949;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTween43_U3CModuleU3E3783534214.h"
#include "DOTween43_U3CModuleU3E3783534214MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43301896125.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43301896125MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "mscorlib_System_Single2076509932.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334720MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOTween2276353038MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334720.h"
#include "mscorlib_System_Void1841601450.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334751MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334751.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334817MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334817.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334852MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334852.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334883MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334883.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334918MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334918.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334949MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334949.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec4023650049MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800MethodDeclarations.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec4023650049.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOVirtual1722510550MethodDeclarations.h"

// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2998039394 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2279406887 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Tweener>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
#define TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnUpdate_TisTweener_t760404022_m1105757124(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tweener>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702(__this /* static, unused */, p0, p1, p2, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tweener>(!!0)
#define TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372(__this /* static, unused */, p0, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Sequence>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586(__this /* static, unused */, p0, p1, method) ((  Sequence_t110643099 * (*) (Il2CppObject * /* static, unused */, Sequence_t110643099 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Sequence>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241(__this /* static, unused */, p0, p1, method) ((  Sequence_t110643099 * (*) (Il2CppObject * /* static, unused */, Sequence_t110643099 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<!!0,!!1,!!2> DG.Tweening.Core.Extensions::Blendable<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<!!0,!!1,!!2>)
extern "C"  TweenerCore_3_t2998039394 * Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2998039394 * p0, const MethodInfo* method);
#define Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691(__this /* static, unused */, p0, method) ((  TweenerCore_3_t2998039394 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass2_0_t426334720_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m100674497_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3987221727_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOColor_m243415068_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOColor_m243415068 (Il2CppObject * __this /* static, unused */, SpriteRenderer_t1209076198 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOColor_m243415068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t426334720 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t426334720 * L_0 = (U3CU3Ec__DisplayClass2_0_t426334720 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass2_0_t426334720_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass2_0__ctor_m610386080(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass2_0_t426334720 * L_1 = V_0;
		SpriteRenderer_t1209076198 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass2_0_t426334720 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m100674497_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass2_0_t426334720 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3987221727_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_11 = DOTween_To_m3916872722(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t426334720 * L_12 = V_0;
		NullCheck(L_12);
		SpriteRenderer_t1209076198 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2998039394 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass3_0_t426334751_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m3598030411_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m934261309_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOFade_m996156756_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOFade_m996156756 (Il2CppObject * __this /* static, unused */, SpriteRenderer_t1209076198 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOFade_m996156756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t426334751 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t426334751 * L_0 = (U3CU3Ec__DisplayClass3_0_t426334751 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t426334751_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_m647217151(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t426334751 * L_1 = V_0;
		SpriteRenderer_t1209076198 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass3_0_t426334751 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m3598030411_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass3_0_t426334751 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m934261309_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Tweener_t760404022 * L_11 = DOTween_ToAlpha_m4132971413(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t426334751 * L_12 = V_0;
		NullCheck(L_12);
		SpriteRenderer_t1209076198 * L_13 = L_12->get_target_0();
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass5_0_t426334817_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m1899174492_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* Rigidbody2D_MovePosition_m2716592358_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOMove_m620609262_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOMove_m620609262 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOMove_m620609262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass5_0_t426334817 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_t426334817 * L_0 = (U3CU3Ec__DisplayClass5_0_t426334817 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass5_0_t426334817_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass5_0__ctor_m725487805(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass5_0_t426334817 * L_1 = V_0;
		Rigidbody2D_t502193897 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass5_0_t426334817 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m1899174492_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass5_0_t426334817 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody2D_t502193897 * L_7 = L_6->get_target_0();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)Rigidbody2D_MovePosition_m2716592358_MethodInfo_var);
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_9, L_7, L_8, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		Vector2_t2243707579  L_10 = ___endValue1;
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_12 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_9, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping3;
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass5_0_t426334817 * L_15 = V_0;
		NullCheck(L_15);
		Rigidbody2D_t502193897 * L_16 = L_15->get_target_0();
		Tweener_t760404022 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass6_0_t426334852_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass6_0_U3CDOMoveXU3Eb__0_m2997082959_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* Rigidbody2D_MovePosition_m2716592358_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOMoveX_m1709679207_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOMoveX_m1709679207 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOMoveX_m1709679207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass6_0_t426334852 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass6_0_t426334852 * L_0 = (U3CU3Ec__DisplayClass6_0_t426334852 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass6_0_t426334852_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass6_0__ctor_m766927388(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass6_0_t426334852 * L_1 = V_0;
		Rigidbody2D_t502193897 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass6_0_t426334852 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass6_0_U3CDOMoveXU3Eb__0_m2997082959_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass6_0_t426334852 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody2D_t502193897 * L_7 = L_6->get_target_0();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)Rigidbody2D_MovePosition_m2716592358_MethodInfo_var);
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_9, L_7, L_8, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		float L_10 = ___endValue1;
		Vector2_t2243707579  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector2__ctor_m3067419446(&L_11, L_10, (0.0f), /*hidden argument*/NULL);
		float L_12 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_13 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_9, L_11, L_12, /*hidden argument*/NULL);
		bool L_14 = ___snapping3;
		Tweener_t760404022 * L_15 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_13, 2, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass6_0_t426334852 * L_16 = V_0;
		NullCheck(L_16);
		Rigidbody2D_t502193897 * L_17 = L_16->get_target_0();
		Tweener_t760404022 * L_18 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass7_0_t426334883_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass7_0_U3CDOMoveYU3Eb__0_m3402029071_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* Rigidbody2D_MovePosition_m2716592358_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOMoveY_m342879976_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOMoveY_m342879976 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOMoveY_m342879976_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass7_0_t426334883 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass7_0_t426334883 * L_0 = (U3CU3Ec__DisplayClass7_0_t426334883 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass7_0_t426334883_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass7_0__ctor_m803758459(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass7_0_t426334883 * L_1 = V_0;
		Rigidbody2D_t502193897 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass7_0_t426334883 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass7_0_U3CDOMoveYU3Eb__0_m3402029071_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass7_0_t426334883 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody2D_t502193897 * L_7 = L_6->get_target_0();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)Rigidbody2D_MovePosition_m2716592358_MethodInfo_var);
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_9, L_7, L_8, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		float L_10 = ___endValue1;
		Vector2_t2243707579  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector2__ctor_m3067419446(&L_11, (0.0f), L_10, /*hidden argument*/NULL);
		float L_12 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_13 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_9, L_11, L_12, /*hidden argument*/NULL);
		bool L_14 = ___snapping3;
		Tweener_t760404022 * L_15 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_13, 4, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass7_0_t426334883 * L_16 = V_0;
		NullCheck(L_16);
		Rigidbody2D_t502193897 * L_17 = L_16->get_target_0();
		Tweener_t760404022 * L_18 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass8_0_t426334918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3858616074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3734738918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2872299158_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3288120768_MethodInfo_var;
extern const MethodInfo* Rigidbody2D_MoveRotation_m1710763820_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2613085532_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DORotate_m177144424_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DORotate_m177144424 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DORotate_m177144424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass8_0_t426334918 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass8_0_t426334918 * L_0 = (U3CU3Ec__DisplayClass8_0_t426334918 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass8_0_t426334918_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass8_0__ctor_m844624090(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass8_0_t426334918 * L_1 = V_0;
		Rigidbody2D_t502193897 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass8_0_t426334918 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2872299158_MethodInfo_var);
		DOGetter_1_t3858616074 * L_5 = (DOGetter_1_t3858616074 *)il2cpp_codegen_object_new(DOGetter_1_t3858616074_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3288120768(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3288120768_MethodInfo_var);
		U3CU3Ec__DisplayClass8_0_t426334918 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody2D_t502193897 * L_7 = L_6->get_target_0();
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)Rigidbody2D_MoveRotation_m1710763820_MethodInfo_var);
		DOSetter_1_t3734738918 * L_9 = (DOSetter_1_t3734738918 *)il2cpp_codegen_object_new(DOSetter_1_t3734738918_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2613085532(L_9, L_7, L_8, /*hidden argument*/DOSetter_1__ctor_m2613085532_MethodInfo_var);
		float L_10 = ___endValue1;
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2279406887 * L_12 = DOTween_To_m914278462(NULL /*static, unused*/, L_5, L_9, L_10, L_11, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass8_0_t426334918 * L_13 = V_0;
		NullCheck(L_13);
		Rigidbody2D_t502193897 * L_14 = L_13->get_target_0();
		TweenerCore_3_t2279406887 * L_15 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var);
		return L_15;
	}
}
// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions43::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass9_0_t426334949_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* TweenCallback_t3697142134_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__0_m3655295499_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__1_m133828321_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__2_m2315173284_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnUpdate_TisTweener_t760404022_m1105757124_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__3_m3655295338_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__4_m2693710174_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOJump_m617362302_MetadataUsageId;
extern "C"  Sequence_t110643099 * ShortcutExtensions43_DOJump_m617362302 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___target0, Vector2_t2243707579  ___endValue1, float ___jumpPower2, int32_t ___numJumps3, float ___duration4, bool ___snapping5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOJump_m617362302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass9_0_t426334949 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass9_0_t426334949 * L_0 = (U3CU3Ec__DisplayClass9_0_t426334949 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass9_0_t426334949_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass9_0__ctor_m881455161(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass9_0_t426334949 * L_1 = V_0;
		Rigidbody2D_t502193897 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_3 = V_0;
		Vector2_t2243707579  L_4 = ___endValue1;
		NullCheck(L_3);
		L_3->set_endValue_4(L_4);
		int32_t L_5 = ___numJumps3;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		___numJumps3 = 1;
	}

IL_001b:
	{
		U3CU3Ec__DisplayClass9_0_t426334949 * L_6 = V_0;
		U3CU3Ec__DisplayClass9_0_t426334949 * L_7 = V_0;
		NullCheck(L_7);
		Rigidbody2D_t502193897 * L_8 = L_7->get_target_0();
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = Rigidbody2D_get_position_m1357256809(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_2();
		NullCheck(L_6);
		L_6->set_startPosY_5(L_10);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_11 = V_0;
		NullCheck(L_11);
		L_11->set_offsetY_2((-1.0f));
		U3CU3Ec__DisplayClass9_0_t426334949 * L_12 = V_0;
		NullCheck(L_12);
		L_12->set_offsetYSet_1((bool)0);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Sequence_t110643099 * L_14 = DOTween_Sequence_m3562267366(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_s_3(L_14);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_15 = V_0;
		NullCheck(L_15);
		Sequence_t110643099 * L_16 = L_15->get_s_3();
		U3CU3Ec__DisplayClass9_0_t426334949 * L_17 = V_0;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__0_m3655295499_MethodInfo_var);
		DOGetter_1_t4025813721 * L_19 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_19, L_17, L_18, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_20 = V_0;
		IntPtr_t L_21;
		L_21.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__1_m133828321_MethodInfo_var);
		DOSetter_1_t3901936565 * L_22 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_22, L_20, L_21, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_23 = V_0;
		NullCheck(L_23);
		Vector2_t2243707579 * L_24 = L_23->get_address_of_endValue_4();
		float L_25 = L_24->get_x_1();
		Vector2_t2243707579  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector2__ctor_m3067419446(&L_26, L_25, (0.0f), /*hidden argument*/NULL);
		float L_27 = ___duration4;
		TweenerCore_3_t3250868854 * L_28 = DOTween_To_m432804578(NULL /*static, unused*/, L_19, L_22, L_26, L_27, /*hidden argument*/NULL);
		bool L_29 = ___snapping5;
		Tweener_t760404022 * L_30 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_28, 2, L_29, /*hidden argument*/NULL);
		Tweener_t760404022 * L_31 = TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288(NULL /*static, unused*/, L_30, 1, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288_MethodInfo_var);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_32 = V_0;
		IntPtr_t L_33;
		L_33.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__2_m2315173284_MethodInfo_var);
		TweenCallback_t3697142134 * L_34 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_34, L_32, L_33, /*hidden argument*/NULL);
		Tweener_t760404022 * L_35 = TweenSettingsExtensions_OnUpdate_TisTweener_t760404022_m1105757124(NULL /*static, unused*/, L_31, L_34, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisTweener_t760404022_m1105757124_MethodInfo_var);
		Sequence_t110643099 * L_36 = TweenSettingsExtensions_Append_m388120539(NULL /*static, unused*/, L_16, L_35, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_37 = V_0;
		IntPtr_t L_38;
		L_38.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__3_m3655295338_MethodInfo_var);
		DOGetter_1_t4025813721 * L_39 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_39, L_37, L_38, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_40 = V_0;
		IntPtr_t L_41;
		L_41.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__4_m2693710174_MethodInfo_var);
		DOSetter_1_t3901936565 * L_42 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_42, L_40, L_41, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		float L_43 = ___jumpPower2;
		Vector2_t2243707579  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Vector2__ctor_m3067419446(&L_44, (0.0f), L_43, /*hidden argument*/NULL);
		float L_45 = ___duration4;
		int32_t L_46 = ___numJumps3;
		TweenerCore_3_t3250868854 * L_47 = DOTween_To_m432804578(NULL /*static, unused*/, L_39, L_42, L_44, ((float)((float)L_45/(float)(((float)((float)((int32_t)((int32_t)L_46*(int32_t)2))))))), /*hidden argument*/NULL);
		bool L_48 = ___snapping5;
		Tweener_t760404022 * L_49 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_47, 4, L_48, /*hidden argument*/NULL);
		Tweener_t760404022 * L_50 = TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288(NULL /*static, unused*/, L_49, 6, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288_MethodInfo_var);
		int32_t L_51 = ___numJumps3;
		Tweener_t760404022 * L_52 = TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702(NULL /*static, unused*/, L_50, ((int32_t)((int32_t)L_51*(int32_t)2)), 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702_MethodInfo_var);
		Tweener_t760404022 * L_53 = TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372(NULL /*static, unused*/, L_52, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372_MethodInfo_var);
		Sequence_t110643099 * L_54 = TweenSettingsExtensions_Join_m3909252375(NULL /*static, unused*/, L_36, L_53, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_55 = V_0;
		NullCheck(L_55);
		Rigidbody2D_t502193897 * L_56 = L_55->get_target_0();
		Sequence_t110643099 * L_57 = TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586(NULL /*static, unused*/, L_54, L_56, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586_MethodInfo_var);
		int32_t L_58 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_defaultEaseType_13();
		TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241_MethodInfo_var);
		U3CU3Ec__DisplayClass9_0_t426334949 * L_59 = V_0;
		NullCheck(L_59);
		Sequence_t110643099 * L_60 = L_59->get_s_3();
		return L_60;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass10_0_t4023650049_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass10_0_U3CDOBlendableColorU3Eb__0_m667894553_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass10_0_U3CDOBlendableColorU3Eb__1_m2026257583_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions43_DOBlendableColor_m606979107_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOBlendableColor_m606979107 (Il2CppObject * __this /* static, unused */, SpriteRenderer_t1209076198 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOBlendableColor_m606979107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass10_0_t4023650049 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_0 = (U3CU3Ec__DisplayClass10_0_t4023650049 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass10_0_t4023650049_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass10_0__ctor_m1002948133(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_1 = V_0;
		SpriteRenderer_t1209076198 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_1(L_2);
		Color_t2020392075  L_3 = ___endValue1;
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_4 = V_0;
		NullCheck(L_4);
		SpriteRenderer_t1209076198 * L_5 = L_4->get_target_1();
		NullCheck(L_5);
		Color_t2020392075  L_6 = SpriteRenderer_get_color_m345525162(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		___endValue1 = L_7;
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_8 = V_0;
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_to_0(L_9);
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass10_0_U3CDOBlendableColorU3Eb__0_m667894553_MethodInfo_var);
		DOGetter_1_t3802498217 * L_12 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_12, L_10, L_11, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass10_0_U3CDOBlendableColorU3Eb__1_m2026257583_MethodInfo_var);
		DOSetter_1_t3678621061 * L_15 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_15, L_13, L_14, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_16 = ___endValue1;
		float L_17 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_18 = DOTween_To_m3916872722(NULL /*static, unused*/, L_12, L_15, L_16, L_17, /*hidden argument*/NULL);
		TweenerCore_3_t2998039394 * L_19 = Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691(NULL /*static, unused*/, L_18, /*hidden argument*/Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_MethodInfo_var);
		U3CU3Ec__DisplayClass10_0_t4023650049 * L_20 = V_0;
		NullCheck(L_20);
		SpriteRenderer_t1209076198 * L_21 = L_20->get_target_1();
		TweenerCore_3_t2998039394 * L_22 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_22;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass10_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass10_0__ctor_m1002948133 (U3CU3Ec__DisplayClass10_0_t4023650049 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass10_0::<DOBlendableColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass10_0_U3CDOBlendableColorU3Eb__0_m667894553 (U3CU3Ec__DisplayClass10_0_t4023650049 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get_to_0();
		return L_0;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass10_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass10_0_U3CDOBlendableColorU3Eb__1_m2026257583 (U3CU3Ec__DisplayClass10_0_t4023650049 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = ___x0;
		Color_t2020392075  L_1 = __this->get_to_0();
		Color_t2020392075  L_2 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Color_t2020392075  L_3 = ___x0;
		__this->set_to_0(L_3);
		SpriteRenderer_t1209076198 * L_4 = __this->get_target_1();
		SpriteRenderer_t1209076198 * L_5 = L_4;
		NullCheck(L_5);
		Color_t2020392075  L_6 = SpriteRenderer_get_color_m345525162(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = V_0;
		Color_t2020392075  L_8 = Color_op_Addition_m1759335993(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		SpriteRenderer_set_color_m2339931967(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass2_0__ctor_m610386080 (U3CU3Ec__DisplayClass2_0_t426334720 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m100674497 (U3CU3Ec__DisplayClass2_0_t426334720 * __this, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = SpriteRenderer_get_color_m345525162(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3987221727 (U3CU3Ec__DisplayClass2_0_t426334720 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		SpriteRenderer_set_color_m2339931967(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m647217151 (U3CU3Ec__DisplayClass3_0_t426334751 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m3598030411 (U3CU3Ec__DisplayClass3_0_t426334751 * __this, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = SpriteRenderer_get_color_m345525162(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m934261309 (U3CU3Ec__DisplayClass3_0_t426334751 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		SpriteRenderer_t1209076198 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		SpriteRenderer_set_color_m2339931967(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m725487805 (U3CU3Ec__DisplayClass5_0_t426334817 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::<DOMove>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m1899174492 (U3CU3Ec__DisplayClass5_0_t426334817 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_position_m1357256809(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass6_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass6_0__ctor_m766927388 (U3CU3Ec__DisplayClass6_0_t426334852 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass6_0::<DOMoveX>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass6_0_U3CDOMoveXU3Eb__0_m2997082959 (U3CU3Ec__DisplayClass6_0_t426334852 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_position_m1357256809(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m803758459 (U3CU3Ec__DisplayClass7_0_t426334883 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass7_0::<DOMoveY>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass7_0_U3CDOMoveYU3Eb__0_m3402029071 (U3CU3Ec__DisplayClass7_0_t426334883 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_position_m1357256809(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0__ctor_m844624090 (U3CU3Ec__DisplayClass8_0_t426334918 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::<DORotate>b__0()
extern "C"  float U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2872299158 (U3CU3Ec__DisplayClass8_0_t426334918 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = Rigidbody2D_get_rotation_m485450105(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0__ctor_m881455161 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__0_m3655295499 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_position_m1357256809(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__1_m133828321 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		Rigidbody2D_set_position_m1185983402(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__2()
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__2_m2315173284 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	U3CU3Ec__DisplayClass9_0_t426334949 * G_B3_0 = NULL;
	U3CU3Ec__DisplayClass9_0_t426334949 * G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	U3CU3Ec__DisplayClass9_0_t426334949 * G_B4_1 = NULL;
	{
		bool L_0 = __this->get_offsetYSet_1();
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_offsetYSet_1((bool)1);
		Sequence_t110643099 * L_1 = __this->get_s_3();
		NullCheck(L_1);
		bool L_2 = ((Tween_t278478013 *)L_1)->get_isRelative_27();
		G_B2_0 = __this;
		if (L_2)
		{
			G_B3_0 = __this;
			goto IL_0031;
		}
	}
	{
		Vector2_t2243707579 * L_3 = __this->get_address_of_endValue_4();
		float L_4 = L_3->get_y_2();
		float L_5 = __this->get_startPosY_5();
		G_B4_0 = ((float)((float)L_4-(float)L_5));
		G_B4_1 = G_B2_0;
		goto IL_003c;
	}

IL_0031:
	{
		Vector2_t2243707579 * L_6 = __this->get_address_of_endValue_4();
		float L_7 = L_6->get_y_2();
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_003c:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_offsetY_2(G_B4_0);
	}

IL_0041:
	{
		Rigidbody2D_t502193897 * L_8 = __this->get_target_0();
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = Rigidbody2D_get_position_m1357256809(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float* L_10 = (&V_0)->get_address_of_y_2();
		float* L_11 = L_10;
		float L_12 = __this->get_offsetY_2();
		Sequence_t110643099 * L_13 = __this->get_s_3();
		float L_14 = TweenExtensions_ElapsedDirectionalPercentage_m968488947(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = DOVirtual_EasedValue_m1602872611(NULL /*static, unused*/, (0.0f), L_12, L_14, 6, /*hidden argument*/NULL);
		*((float*)(L_11)) = (float)((float)((float)(*((float*)L_11))+(float)L_15));
		Rigidbody2D_t502193897 * L_16 = __this->get_target_0();
		Vector2_t2243707579  L_17 = V_0;
		NullCheck(L_16);
		Rigidbody2D_set_position_m1185983402(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__3()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__3_m3655295338 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_position_m1357256809(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__4(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__4_m2693710174 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		Rigidbody2D_set_position_m1185983402(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
