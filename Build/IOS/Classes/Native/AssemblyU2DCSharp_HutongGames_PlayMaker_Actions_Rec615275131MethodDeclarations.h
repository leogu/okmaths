﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta
struct RectTransformSetSizeDelta_t615275131;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::.ctor()
extern "C"  void RectTransformSetSizeDelta__ctor_m2331568933 (RectTransformSetSizeDelta_t615275131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::Reset()
extern "C"  void RectTransformSetSizeDelta_Reset_m3172396952 (RectTransformSetSizeDelta_t615275131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::OnEnter()
extern "C"  void RectTransformSetSizeDelta_OnEnter_m629945042 (RectTransformSetSizeDelta_t615275131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::OnActionUpdate()
extern "C"  void RectTransformSetSizeDelta_OnActionUpdate_m1267418875 (RectTransformSetSizeDelta_t615275131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetSizeDelta::DoSetSizeDelta()
extern "C"  void RectTransformSetSizeDelta_DoSetSizeDelta_m3475952885 (RectTransformSetSizeDelta_t615275131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
