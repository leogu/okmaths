﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRaycastAllInfo
struct GetRaycastAllInfo_t885129436;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::.ctor()
extern "C"  void GetRaycastAllInfo__ctor_m649529600 (GetRaycastAllInfo_t885129436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::Reset()
extern "C"  void GetRaycastAllInfo_Reset_m2926724981 (GetRaycastAllInfo_t885129436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::StoreRaycastAllInfo()
extern "C"  void GetRaycastAllInfo_StoreRaycastAllInfo_m1252350503 (GetRaycastAllInfo_t885129436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::OnEnter()
extern "C"  void GetRaycastAllInfo_OnEnter_m3629120861 (GetRaycastAllInfo_t885129436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastAllInfo::OnUpdate()
extern "C"  void GetRaycastAllInfo_OnUpdate_m2024187782 (GetRaycastAllInfo_t885129436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
