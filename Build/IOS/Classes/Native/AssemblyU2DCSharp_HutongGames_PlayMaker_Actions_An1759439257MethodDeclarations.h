﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateFloat
struct AnimateFloat_t1759439257;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::.ctor()
extern "C"  void AnimateFloat__ctor_m2155376837 (AnimateFloat_t1759439257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::Reset()
extern "C"  void AnimateFloat_Reset_m3603956606 (AnimateFloat_t1759439257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::OnEnter()
extern "C"  void AnimateFloat_OnEnter_m4147745636 (AnimateFloat_t1759439257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateFloat::OnUpdate()
extern "C"  void AnimateFloat_OnUpdate_m1474208137 (AnimateFloat_t1759439257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
