﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBox
struct GUILayoutBox_t2333350582;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::.ctor()
extern "C"  void GUILayoutBox__ctor_m2673235222 (GUILayoutBox_t2333350582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::Reset()
extern "C"  void GUILayoutBox_Reset_m3469156263 (GUILayoutBox_t2333350582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBox::OnGUI()
extern "C"  void GUILayoutBox_OnGUI_m1069909874 (GUILayoutBox_t2333350582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
