﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsAll
struct DOTweenControlMethodsPlayBackwardsAll_t3569807462;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsAll::.ctor()
extern "C"  void DOTweenControlMethodsPlayBackwardsAll__ctor_m3917245848 (DOTweenControlMethodsPlayBackwardsAll_t3569807462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsAll::Reset()
extern "C"  void DOTweenControlMethodsPlayBackwardsAll_Reset_m3935051387 (DOTweenControlMethodsPlayBackwardsAll_t3569807462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsAll::OnEnter()
extern "C"  void DOTweenControlMethodsPlayBackwardsAll_OnEnter_m3439097035 (DOTweenControlMethodsPlayBackwardsAll_t3569807462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
