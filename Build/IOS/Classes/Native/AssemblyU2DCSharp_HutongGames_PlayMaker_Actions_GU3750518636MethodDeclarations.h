﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider
struct GUILayoutVerticalSlider_t3750518636;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::.ctor()
extern "C"  void GUILayoutVerticalSlider__ctor_m2763836436 (GUILayoutVerticalSlider_t3750518636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::Reset()
extern "C"  void GUILayoutVerticalSlider_Reset_m2581012521 (GUILayoutVerticalSlider_t3750518636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutVerticalSlider::OnGUI()
extern "C"  void GUILayoutVerticalSlider_OnGUI_m3611597704 (GUILayoutVerticalSlider_t3750518636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
