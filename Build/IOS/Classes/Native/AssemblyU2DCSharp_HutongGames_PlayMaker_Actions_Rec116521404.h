﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax
struct  RectTransformSetAnchorMinAndMax_t116521404  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::anchorMax
	FsmVector2_t2430450063 * ___anchorMax_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::anchorMin
	FsmVector2_t2430450063 * ___anchorMin_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::xMax
	FsmFloat_t937133978 * ___xMax_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::yMax
	FsmFloat_t937133978 * ___yMax_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::xMin
	FsmFloat_t937133978 * ___xMin_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::yMin
	FsmFloat_t937133978 * ___yMin_19;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::_rt
	RectTransform_t3349966182 * ____rt_20;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_anchorMax_14() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___anchorMax_14)); }
	inline FsmVector2_t2430450063 * get_anchorMax_14() const { return ___anchorMax_14; }
	inline FsmVector2_t2430450063 ** get_address_of_anchorMax_14() { return &___anchorMax_14; }
	inline void set_anchorMax_14(FsmVector2_t2430450063 * value)
	{
		___anchorMax_14 = value;
		Il2CppCodeGenWriteBarrier(&___anchorMax_14, value);
	}

	inline static int32_t get_offset_of_anchorMin_15() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___anchorMin_15)); }
	inline FsmVector2_t2430450063 * get_anchorMin_15() const { return ___anchorMin_15; }
	inline FsmVector2_t2430450063 ** get_address_of_anchorMin_15() { return &___anchorMin_15; }
	inline void set_anchorMin_15(FsmVector2_t2430450063 * value)
	{
		___anchorMin_15 = value;
		Il2CppCodeGenWriteBarrier(&___anchorMin_15, value);
	}

	inline static int32_t get_offset_of_xMax_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___xMax_16)); }
	inline FsmFloat_t937133978 * get_xMax_16() const { return ___xMax_16; }
	inline FsmFloat_t937133978 ** get_address_of_xMax_16() { return &___xMax_16; }
	inline void set_xMax_16(FsmFloat_t937133978 * value)
	{
		___xMax_16 = value;
		Il2CppCodeGenWriteBarrier(&___xMax_16, value);
	}

	inline static int32_t get_offset_of_yMax_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___yMax_17)); }
	inline FsmFloat_t937133978 * get_yMax_17() const { return ___yMax_17; }
	inline FsmFloat_t937133978 ** get_address_of_yMax_17() { return &___yMax_17; }
	inline void set_yMax_17(FsmFloat_t937133978 * value)
	{
		___yMax_17 = value;
		Il2CppCodeGenWriteBarrier(&___yMax_17, value);
	}

	inline static int32_t get_offset_of_xMin_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___xMin_18)); }
	inline FsmFloat_t937133978 * get_xMin_18() const { return ___xMin_18; }
	inline FsmFloat_t937133978 ** get_address_of_xMin_18() { return &___xMin_18; }
	inline void set_xMin_18(FsmFloat_t937133978 * value)
	{
		___xMin_18 = value;
		Il2CppCodeGenWriteBarrier(&___xMin_18, value);
	}

	inline static int32_t get_offset_of_yMin_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ___yMin_19)); }
	inline FsmFloat_t937133978 * get_yMin_19() const { return ___yMin_19; }
	inline FsmFloat_t937133978 ** get_address_of_yMin_19() { return &___yMin_19; }
	inline void set_yMin_19(FsmFloat_t937133978 * value)
	{
		___yMin_19 = value;
		Il2CppCodeGenWriteBarrier(&___yMin_19, value);
	}

	inline static int32_t get_offset_of__rt_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorMinAndMax_t116521404, ____rt_20)); }
	inline RectTransform_t3349966182 * get__rt_20() const { return ____rt_20; }
	inline RectTransform_t3349966182 ** get_address_of__rt_20() { return &____rt_20; }
	inline void set__rt_20(RectTransform_t3349966182 * value)
	{
		____rt_20 = value;
		Il2CppCodeGenWriteBarrier(&____rt_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
