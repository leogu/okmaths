﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddForce
struct AddForce_t1939251706;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddForce::.ctor()
extern "C"  void AddForce__ctor_m1787017766 (AddForce_t1939251706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::Reset()
extern "C"  void AddForce_Reset_m346840391 (AddForce_t1939251706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnPreprocess()
extern "C"  void AddForce_OnPreprocess_m588623167 (AddForce_t1939251706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnEnter()
extern "C"  void AddForce_OnEnter_m3594095455 (AddForce_t1939251706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::OnFixedUpdate()
extern "C"  void AddForce_OnFixedUpdate_m4005689122 (AddForce_t1939251706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddForce::DoAddForce()
extern "C"  void AddForce_DoAddForce_m3437487105 (AddForce_t1939251706 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
