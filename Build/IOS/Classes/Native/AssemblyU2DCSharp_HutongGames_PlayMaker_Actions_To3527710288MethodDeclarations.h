﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TouchObject2dEvent
struct TouchObject2dEvent_t3527710288;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TouchObject2dEvent::.ctor()
extern "C"  void TouchObject2dEvent__ctor_m3091425434 (TouchObject2dEvent_t3527710288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchObject2dEvent::Reset()
extern "C"  void TouchObject2dEvent_Reset_m2258392449 (TouchObject2dEvent_t3527710288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchObject2dEvent::OnUpdate()
extern "C"  void TouchObject2dEvent_OnUpdate_m637761804 (TouchObject2dEvent_t3527710288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
