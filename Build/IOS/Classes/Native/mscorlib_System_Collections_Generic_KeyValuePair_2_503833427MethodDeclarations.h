﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21413314124MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1452290190(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t503833427 *, Fsm_t917886356 *, RaycastHit2D_t4063908774 , const MethodInfo*))KeyValuePair_2__ctor_m50054624_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Key()
#define KeyValuePair_2_get_Key_m3118539364(__this, method) ((  Fsm_t917886356 * (*) (KeyValuePair_2_t503833427 *, const MethodInfo*))KeyValuePair_2_get_Key_m2460284350_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2320933565(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t503833427 *, Fsm_t917886356 *, const MethodInfo*))KeyValuePair_2_set_Key_m3052986025_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Value()
#define KeyValuePair_2_get_Value_m1892363908(__this, method) ((  RaycastHit2D_t4063908774  (*) (KeyValuePair_2_t503833427 *, const MethodInfo*))KeyValuePair_2_get_Value_m2648938910_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2153438541(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t503833427 *, RaycastHit2D_t4063908774 , const MethodInfo*))KeyValuePair_2_set_Value_m3606416665_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::ToString()
#define KeyValuePair_2_ToString_m3374083679(__this, method) ((  String_t* (*) (KeyValuePair_2_t503833427 *, const MethodInfo*))KeyValuePair_2_ToString_m1150022243_gshared)(__this, method)
