﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag
struct ArrayListFindGameObjectsByTag_t1509256103;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::.ctor()
extern "C"  void ArrayListFindGameObjectsByTag__ctor_m137988353 (ArrayListFindGameObjectsByTag_t1509256103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::Reset()
extern "C"  void ArrayListFindGameObjectsByTag_Reset_m1041392060 (ArrayListFindGameObjectsByTag_t1509256103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::OnEnter()
extern "C"  void ArrayListFindGameObjectsByTag_OnEnter_m2187235966 (ArrayListFindGameObjectsByTag_t1509256103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::FindGOByTag()
extern "C"  void ArrayListFindGameObjectsByTag_FindGOByTag_m2674852381 (ArrayListFindGameObjectsByTag_t1509256103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
