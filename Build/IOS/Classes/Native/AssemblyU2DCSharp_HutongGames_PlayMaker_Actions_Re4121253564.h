﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Re1889022726.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition
struct  RectTransformSetAnchorRectPosition_t4121253564  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition/AnchorReference HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::anchorReference
	int32_t ___anchorReference_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::normalized
	FsmBool_t664485696 * ___normalized_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::anchor
	FsmVector2_t2430450063 * ___anchor_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::x
	FsmFloat_t937133978 * ___x_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::y
	FsmFloat_t937133978 * ___y_18;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::_rt
	RectTransform_t3349966182 * ____rt_19;
	// UnityEngine.Rect HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::_anchorRect
	Rect_t3681755626  ____anchorRect_20;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_anchorReference_14() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ___anchorReference_14)); }
	inline int32_t get_anchorReference_14() const { return ___anchorReference_14; }
	inline int32_t* get_address_of_anchorReference_14() { return &___anchorReference_14; }
	inline void set_anchorReference_14(int32_t value)
	{
		___anchorReference_14 = value;
	}

	inline static int32_t get_offset_of_normalized_15() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ___normalized_15)); }
	inline FsmBool_t664485696 * get_normalized_15() const { return ___normalized_15; }
	inline FsmBool_t664485696 ** get_address_of_normalized_15() { return &___normalized_15; }
	inline void set_normalized_15(FsmBool_t664485696 * value)
	{
		___normalized_15 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_15, value);
	}

	inline static int32_t get_offset_of_anchor_16() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ___anchor_16)); }
	inline FsmVector2_t2430450063 * get_anchor_16() const { return ___anchor_16; }
	inline FsmVector2_t2430450063 ** get_address_of_anchor_16() { return &___anchor_16; }
	inline void set_anchor_16(FsmVector2_t2430450063 * value)
	{
		___anchor_16 = value;
		Il2CppCodeGenWriteBarrier(&___anchor_16, value);
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ___x_17)); }
	inline FsmFloat_t937133978 * get_x_17() const { return ___x_17; }
	inline FsmFloat_t937133978 ** get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(FsmFloat_t937133978 * value)
	{
		___x_17 = value;
		Il2CppCodeGenWriteBarrier(&___x_17, value);
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ___y_18)); }
	inline FsmFloat_t937133978 * get_y_18() const { return ___y_18; }
	inline FsmFloat_t937133978 ** get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(FsmFloat_t937133978 * value)
	{
		___y_18 = value;
		Il2CppCodeGenWriteBarrier(&___y_18, value);
	}

	inline static int32_t get_offset_of__rt_19() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ____rt_19)); }
	inline RectTransform_t3349966182 * get__rt_19() const { return ____rt_19; }
	inline RectTransform_t3349966182 ** get_address_of__rt_19() { return &____rt_19; }
	inline void set__rt_19(RectTransform_t3349966182 * value)
	{
		____rt_19 = value;
		Il2CppCodeGenWriteBarrier(&____rt_19, value);
	}

	inline static int32_t get_offset_of__anchorRect_20() { return static_cast<int32_t>(offsetof(RectTransformSetAnchorRectPosition_t4121253564, ____anchorRect_20)); }
	inline Rect_t3681755626  get__anchorRect_20() const { return ____anchorRect_20; }
	inline Rect_t3681755626 * get_address_of__anchorRect_20() { return &____anchorRect_20; }
	inline void set__anchorRect_20(Rect_t3681755626  value)
	{
		____anchorRect_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
