﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkInitializeServer
struct NetworkInitializeServer_t3084672651;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkInitializeServer::.ctor()
extern "C"  void NetworkInitializeServer__ctor_m2227846805 (NetworkInitializeServer_t3084672651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInitializeServer::Reset()
extern "C"  void NetworkInitializeServer_Reset_m2889308360 (NetworkInitializeServer_t3084672651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInitializeServer::OnEnter()
extern "C"  void NetworkInitializeServer_OnEnter_m111500946 (NetworkInitializeServer_t3084672651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
