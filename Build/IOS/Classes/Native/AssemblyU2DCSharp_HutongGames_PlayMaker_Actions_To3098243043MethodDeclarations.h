﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TouchEvent
struct TouchEvent_t3098243043;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TouchEvent::.ctor()
extern "C"  void TouchEvent__ctor_m3403977295 (TouchEvent_t3098243043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchEvent::Reset()
extern "C"  void TouchEvent_Reset_m3223300200 (TouchEvent_t3098243043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TouchEvent::OnUpdate()
extern "C"  void TouchEvent_OnUpdate_m350699359 (TouchEvent_t3098243043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
