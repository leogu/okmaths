﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll
struct DOTweenControlMethodsKillAll_t2793033768;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll::.ctor()
extern "C"  void DOTweenControlMethodsKillAll__ctor_m3729181912 (DOTweenControlMethodsKillAll_t2793033768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll::Reset()
extern "C"  void DOTweenControlMethodsKillAll_Reset_m1118559289 (DOTweenControlMethodsKillAll_t2793033768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillAll::OnEnter()
extern "C"  void DOTweenControlMethodsKillAll_OnEnter_m4009938529 (DOTweenControlMethodsKillAll_t2793033768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
