﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ForwardEvent
struct ForwardEvent_t1462622173;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"

// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::.ctor()
extern "C"  void ForwardEvent__ctor_m2011589049 (ForwardEvent_t1462622173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ForwardEvent::Reset()
extern "C"  void ForwardEvent_Reset_m2808588266 (ForwardEvent_t1462622173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ForwardEvent::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool ForwardEvent_Event_m3131734135 (ForwardEvent_t1462622173 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
