﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenSpriteRendererFade
struct DOTweenSpriteRendererFade_t189215658;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererFade::.ctor()
extern "C"  void DOTweenSpriteRendererFade__ctor_m1836752918 (DOTweenSpriteRendererFade_t189215658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererFade::Reset()
extern "C"  void DOTweenSpriteRendererFade_Reset_m1498645515 (DOTweenSpriteRendererFade_t189215658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererFade::OnEnter()
extern "C"  void DOTweenSpriteRendererFade_OnEnter_m3913066683 (DOTweenSpriteRendererFade_t189215658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererFade::<OnEnter>m__98()
extern "C"  void DOTweenSpriteRendererFade_U3COnEnterU3Em__98_m3101050437 (DOTweenSpriteRendererFade_t189215658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenSpriteRendererFade::<OnEnter>m__99()
extern "C"  void DOTweenSpriteRendererFade_U3COnEnterU3Em__99_m3101050404 (DOTweenSpriteRendererFade_t189215658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
