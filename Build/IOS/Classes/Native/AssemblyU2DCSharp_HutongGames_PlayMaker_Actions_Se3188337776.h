﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetEventData
struct  SetEventData_t3188337776  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.SetEventData::setGameObjectData
	FsmGameObject_t3097142863 * ___setGameObjectData_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SetEventData::setIntData
	FsmInt_t1273009179 * ___setIntData_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetEventData::setFloatData
	FsmFloat_t937133978 * ___setFloatData_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SetEventData::setStringData
	FsmString_t2414474701 * ___setStringData_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.SetEventData::setBoolData
	FsmBool_t664485696 * ___setBoolData_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.SetEventData::setVector2Data
	FsmVector2_t2430450063 * ___setVector2Data_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.SetEventData::setVector3Data
	FsmVector3_t3996534004 * ___setVector3Data_17;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.SetEventData::setRectData
	FsmRect_t19023354 * ___setRectData_18;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Actions.SetEventData::setQuaternionData
	FsmQuaternion_t878438756 * ___setQuaternionData_19;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.SetEventData::setColorData
	FsmColor_t118301965 * ___setColorData_20;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Actions.SetEventData::setMaterialData
	FsmMaterial_t1421632035 * ___setMaterialData_21;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Actions.SetEventData::setTextureData
	FsmTexture_t3372293163 * ___setTextureData_22;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.SetEventData::setObjectData
	FsmObject_t2785794313 * ___setObjectData_23;

public:
	inline static int32_t get_offset_of_setGameObjectData_11() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setGameObjectData_11)); }
	inline FsmGameObject_t3097142863 * get_setGameObjectData_11() const { return ___setGameObjectData_11; }
	inline FsmGameObject_t3097142863 ** get_address_of_setGameObjectData_11() { return &___setGameObjectData_11; }
	inline void set_setGameObjectData_11(FsmGameObject_t3097142863 * value)
	{
		___setGameObjectData_11 = value;
		Il2CppCodeGenWriteBarrier(&___setGameObjectData_11, value);
	}

	inline static int32_t get_offset_of_setIntData_12() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setIntData_12)); }
	inline FsmInt_t1273009179 * get_setIntData_12() const { return ___setIntData_12; }
	inline FsmInt_t1273009179 ** get_address_of_setIntData_12() { return &___setIntData_12; }
	inline void set_setIntData_12(FsmInt_t1273009179 * value)
	{
		___setIntData_12 = value;
		Il2CppCodeGenWriteBarrier(&___setIntData_12, value);
	}

	inline static int32_t get_offset_of_setFloatData_13() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setFloatData_13)); }
	inline FsmFloat_t937133978 * get_setFloatData_13() const { return ___setFloatData_13; }
	inline FsmFloat_t937133978 ** get_address_of_setFloatData_13() { return &___setFloatData_13; }
	inline void set_setFloatData_13(FsmFloat_t937133978 * value)
	{
		___setFloatData_13 = value;
		Il2CppCodeGenWriteBarrier(&___setFloatData_13, value);
	}

	inline static int32_t get_offset_of_setStringData_14() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setStringData_14)); }
	inline FsmString_t2414474701 * get_setStringData_14() const { return ___setStringData_14; }
	inline FsmString_t2414474701 ** get_address_of_setStringData_14() { return &___setStringData_14; }
	inline void set_setStringData_14(FsmString_t2414474701 * value)
	{
		___setStringData_14 = value;
		Il2CppCodeGenWriteBarrier(&___setStringData_14, value);
	}

	inline static int32_t get_offset_of_setBoolData_15() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setBoolData_15)); }
	inline FsmBool_t664485696 * get_setBoolData_15() const { return ___setBoolData_15; }
	inline FsmBool_t664485696 ** get_address_of_setBoolData_15() { return &___setBoolData_15; }
	inline void set_setBoolData_15(FsmBool_t664485696 * value)
	{
		___setBoolData_15 = value;
		Il2CppCodeGenWriteBarrier(&___setBoolData_15, value);
	}

	inline static int32_t get_offset_of_setVector2Data_16() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setVector2Data_16)); }
	inline FsmVector2_t2430450063 * get_setVector2Data_16() const { return ___setVector2Data_16; }
	inline FsmVector2_t2430450063 ** get_address_of_setVector2Data_16() { return &___setVector2Data_16; }
	inline void set_setVector2Data_16(FsmVector2_t2430450063 * value)
	{
		___setVector2Data_16 = value;
		Il2CppCodeGenWriteBarrier(&___setVector2Data_16, value);
	}

	inline static int32_t get_offset_of_setVector3Data_17() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setVector3Data_17)); }
	inline FsmVector3_t3996534004 * get_setVector3Data_17() const { return ___setVector3Data_17; }
	inline FsmVector3_t3996534004 ** get_address_of_setVector3Data_17() { return &___setVector3Data_17; }
	inline void set_setVector3Data_17(FsmVector3_t3996534004 * value)
	{
		___setVector3Data_17 = value;
		Il2CppCodeGenWriteBarrier(&___setVector3Data_17, value);
	}

	inline static int32_t get_offset_of_setRectData_18() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setRectData_18)); }
	inline FsmRect_t19023354 * get_setRectData_18() const { return ___setRectData_18; }
	inline FsmRect_t19023354 ** get_address_of_setRectData_18() { return &___setRectData_18; }
	inline void set_setRectData_18(FsmRect_t19023354 * value)
	{
		___setRectData_18 = value;
		Il2CppCodeGenWriteBarrier(&___setRectData_18, value);
	}

	inline static int32_t get_offset_of_setQuaternionData_19() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setQuaternionData_19)); }
	inline FsmQuaternion_t878438756 * get_setQuaternionData_19() const { return ___setQuaternionData_19; }
	inline FsmQuaternion_t878438756 ** get_address_of_setQuaternionData_19() { return &___setQuaternionData_19; }
	inline void set_setQuaternionData_19(FsmQuaternion_t878438756 * value)
	{
		___setQuaternionData_19 = value;
		Il2CppCodeGenWriteBarrier(&___setQuaternionData_19, value);
	}

	inline static int32_t get_offset_of_setColorData_20() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setColorData_20)); }
	inline FsmColor_t118301965 * get_setColorData_20() const { return ___setColorData_20; }
	inline FsmColor_t118301965 ** get_address_of_setColorData_20() { return &___setColorData_20; }
	inline void set_setColorData_20(FsmColor_t118301965 * value)
	{
		___setColorData_20 = value;
		Il2CppCodeGenWriteBarrier(&___setColorData_20, value);
	}

	inline static int32_t get_offset_of_setMaterialData_21() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setMaterialData_21)); }
	inline FsmMaterial_t1421632035 * get_setMaterialData_21() const { return ___setMaterialData_21; }
	inline FsmMaterial_t1421632035 ** get_address_of_setMaterialData_21() { return &___setMaterialData_21; }
	inline void set_setMaterialData_21(FsmMaterial_t1421632035 * value)
	{
		___setMaterialData_21 = value;
		Il2CppCodeGenWriteBarrier(&___setMaterialData_21, value);
	}

	inline static int32_t get_offset_of_setTextureData_22() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setTextureData_22)); }
	inline FsmTexture_t3372293163 * get_setTextureData_22() const { return ___setTextureData_22; }
	inline FsmTexture_t3372293163 ** get_address_of_setTextureData_22() { return &___setTextureData_22; }
	inline void set_setTextureData_22(FsmTexture_t3372293163 * value)
	{
		___setTextureData_22 = value;
		Il2CppCodeGenWriteBarrier(&___setTextureData_22, value);
	}

	inline static int32_t get_offset_of_setObjectData_23() { return static_cast<int32_t>(offsetof(SetEventData_t3188337776, ___setObjectData_23)); }
	inline FsmObject_t2785794313 * get_setObjectData_23() const { return ___setObjectData_23; }
	inline FsmObject_t2785794313 ** get_address_of_setObjectData_23() { return &___setObjectData_23; }
	inline void set_setObjectData_23(FsmObject_t2785794313 * value)
	{
		___setObjectData_23 = value;
		Il2CppCodeGenWriteBarrier(&___setObjectData_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
