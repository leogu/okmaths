﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScreenPick2d
struct ScreenPick2d_t3932806665;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScreenPick2d::.ctor()
extern "C"  void ScreenPick2d__ctor_m2081740077 (ScreenPick2d_t3932806665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick2d::Reset()
extern "C"  void ScreenPick2d_Reset_m2881479446 (ScreenPick2d_t3932806665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick2d::OnEnter()
extern "C"  void ScreenPick2d_OnEnter_m660735700 (ScreenPick2d_t3932806665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick2d::OnUpdate()
extern "C"  void ScreenPick2d_OnUpdate_m1176534169 (ScreenPick2d_t3932806665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenPick2d::DoScreenPick()
extern "C"  void ScreenPick2d_DoScreenPick_m766231039 (ScreenPick2d_t3932806665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
