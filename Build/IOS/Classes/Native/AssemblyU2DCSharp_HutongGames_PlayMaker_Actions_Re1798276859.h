﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Camera
struct Camera_t189460977;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint
struct  RectTransformWorldToScreenPoint_t1798276859  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::camera
	FsmOwnerDefault_t2023674184 * ___camera_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::screenPoint
	FsmVector3_t3996534004 * ___screenPoint_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::screenX
	FsmFloat_t937133978 * ___screenX_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::screenY
	FsmFloat_t937133978 * ___screenY_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::normalize
	FsmBool_t664485696 * ___normalize_18;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::_rt
	RectTransform_t3349966182 * ____rt_19;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::_cam
	Camera_t189460977 * ____cam_20;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_camera_14() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ___camera_14)); }
	inline FsmOwnerDefault_t2023674184 * get_camera_14() const { return ___camera_14; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_camera_14() { return &___camera_14; }
	inline void set_camera_14(FsmOwnerDefault_t2023674184 * value)
	{
		___camera_14 = value;
		Il2CppCodeGenWriteBarrier(&___camera_14, value);
	}

	inline static int32_t get_offset_of_screenPoint_15() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ___screenPoint_15)); }
	inline FsmVector3_t3996534004 * get_screenPoint_15() const { return ___screenPoint_15; }
	inline FsmVector3_t3996534004 ** get_address_of_screenPoint_15() { return &___screenPoint_15; }
	inline void set_screenPoint_15(FsmVector3_t3996534004 * value)
	{
		___screenPoint_15 = value;
		Il2CppCodeGenWriteBarrier(&___screenPoint_15, value);
	}

	inline static int32_t get_offset_of_screenX_16() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ___screenX_16)); }
	inline FsmFloat_t937133978 * get_screenX_16() const { return ___screenX_16; }
	inline FsmFloat_t937133978 ** get_address_of_screenX_16() { return &___screenX_16; }
	inline void set_screenX_16(FsmFloat_t937133978 * value)
	{
		___screenX_16 = value;
		Il2CppCodeGenWriteBarrier(&___screenX_16, value);
	}

	inline static int32_t get_offset_of_screenY_17() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ___screenY_17)); }
	inline FsmFloat_t937133978 * get_screenY_17() const { return ___screenY_17; }
	inline FsmFloat_t937133978 ** get_address_of_screenY_17() { return &___screenY_17; }
	inline void set_screenY_17(FsmFloat_t937133978 * value)
	{
		___screenY_17 = value;
		Il2CppCodeGenWriteBarrier(&___screenY_17, value);
	}

	inline static int32_t get_offset_of_normalize_18() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ___normalize_18)); }
	inline FsmBool_t664485696 * get_normalize_18() const { return ___normalize_18; }
	inline FsmBool_t664485696 ** get_address_of_normalize_18() { return &___normalize_18; }
	inline void set_normalize_18(FsmBool_t664485696 * value)
	{
		___normalize_18 = value;
		Il2CppCodeGenWriteBarrier(&___normalize_18, value);
	}

	inline static int32_t get_offset_of__rt_19() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ____rt_19)); }
	inline RectTransform_t3349966182 * get__rt_19() const { return ____rt_19; }
	inline RectTransform_t3349966182 ** get_address_of__rt_19() { return &____rt_19; }
	inline void set__rt_19(RectTransform_t3349966182 * value)
	{
		____rt_19 = value;
		Il2CppCodeGenWriteBarrier(&____rt_19, value);
	}

	inline static int32_t get_offset_of__cam_20() { return static_cast<int32_t>(offsetof(RectTransformWorldToScreenPoint_t1798276859, ____cam_20)); }
	inline Camera_t189460977 * get__cam_20() const { return ____cam_20; }
	inline Camera_t189460977 ** get_address_of__cam_20() { return &____cam_20; }
	inline void set__cam_20(Camera_t189460977 * value)
	{
		____cam_20 = value;
		Il2CppCodeGenWriteBarrier(&____cam_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
