﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SelectRandomString
struct SelectRandomString_t2820022428;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SelectRandomString::.ctor()
extern "C"  void SelectRandomString__ctor_m2436301178 (SelectRandomString_t2820022428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomString::Reset()
extern "C"  void SelectRandomString_Reset_m102886921 (SelectRandomString_t2820022428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomString::OnEnter()
extern "C"  void SelectRandomString_OnEnter_m1642187305 (SelectRandomString_t2820022428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SelectRandomString::DoSelectRandomString()
extern "C"  void SelectRandomString_DoSelectRandomString_m147476385 (SelectRandomString_t2820022428 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
