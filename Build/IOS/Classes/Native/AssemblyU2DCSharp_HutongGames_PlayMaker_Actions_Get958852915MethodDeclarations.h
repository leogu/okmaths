﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetCosine
struct GetCosine_t958852915;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetCosine::.ctor()
extern "C"  void GetCosine__ctor_m1637261453 (GetCosine_t958852915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::Reset()
extern "C"  void GetCosine_Reset_m2298731952 (GetCosine_t958852915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::OnEnter()
extern "C"  void GetCosine_OnEnter_m2096754618 (GetCosine_t958852915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::OnUpdate()
extern "C"  void GetCosine_OnUpdate_m3807997033 (GetCosine_t958852915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCosine::DoCosine()
extern "C"  void GetCosine_DoCosine_m199352707 (GetCosine_t958852915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
