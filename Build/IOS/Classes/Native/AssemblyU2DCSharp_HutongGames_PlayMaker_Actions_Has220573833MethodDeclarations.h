﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableGetMany
struct HashTableGetMany_t220573833;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableGetMany::.ctor()
extern "C"  void HashTableGetMany__ctor_m3669609923 (HashTableGetMany_t220573833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetMany::Reset()
extern "C"  void HashTableGetMany_Reset_m1947071034 (HashTableGetMany_t220573833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetMany::OnEnter()
extern "C"  void HashTableGetMany_OnEnter_m3429206708 (HashTableGetMany_t220573833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetMany::Get()
extern "C"  void HashTableGetMany_Get_m2984194309 (HashTableGetMany_t220573833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
