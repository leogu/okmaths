﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetHideMobileInput
struct uGuiInputFieldGetHideMobileInput_t1548690012;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetHideMobileInput::.ctor()
extern "C"  void uGuiInputFieldGetHideMobileInput__ctor_m2937028868 (uGuiInputFieldGetHideMobileInput_t1548690012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetHideMobileInput::Reset()
extern "C"  void uGuiInputFieldGetHideMobileInput_Reset_m1489533549 (uGuiInputFieldGetHideMobileInput_t1548690012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetHideMobileInput::OnEnter()
extern "C"  void uGuiInputFieldGetHideMobileInput_OnEnter_m2654880837 (uGuiInputFieldGetHideMobileInput_t1548690012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetHideMobileInput::DoGetValue()
extern "C"  void uGuiInputFieldGetHideMobileInput_DoGetValue_m2860106450 (uGuiInputFieldGetHideMobileInput_t1548690012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
