﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayClear
struct ArrayClear_t2210461178;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayClear::.ctor()
extern "C"  void ArrayClear__ctor_m4078390770 (ArrayClear_t2210461178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayClear::Reset()
extern "C"  void ArrayClear_Reset_m1747139403 (ArrayClear_t2210461178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayClear::OnEnter()
extern "C"  void ArrayClear_OnEnter_m2706120819 (ArrayClear_t2210461178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
