﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextLineCast2d
struct GetNextLineCast2d_t3575895894;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::.ctor()
extern "C"  void GetNextLineCast2d__ctor_m2788315700 (GetNextLineCast2d_t3575895894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::Reset()
extern "C"  void GetNextLineCast2d_Reset_m3473876367 (GetNextLineCast2d_t3575895894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::OnEnter()
extern "C"  void GetNextLineCast2d_OnEnter_m2751482767 (GetNextLineCast2d_t3575895894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextLineCast2d::DoGetNextCollider()
extern "C"  void GetNextLineCast2d_DoGetNextCollider_m1507091310 (GetNextLineCast2d_t3575895894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextLineCast2d::GetLineCastAll()
extern "C"  RaycastHit2DU5BU5D_t4176517891* GetNextLineCast2d_GetLineCastAll_m2323431842 (GetNextLineCast2d_t3575895894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
