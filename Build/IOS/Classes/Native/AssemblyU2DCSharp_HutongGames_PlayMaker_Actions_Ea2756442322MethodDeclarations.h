﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseVector3
struct EaseVector3_t2756442322;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseVector3::.ctor()
extern "C"  void EaseVector3__ctor_m484586904 (EaseVector3_t2756442322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::Reset()
extern "C"  void EaseVector3_Reset_m2812937707 (EaseVector3_t2756442322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::OnEnter()
extern "C"  void EaseVector3_OnEnter_m2541979403 (EaseVector3_t2756442322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::OnExit()
extern "C"  void EaseVector3_OnExit_m2434333467 (EaseVector3_t2756442322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseVector3::OnUpdate()
extern "C"  void EaseVector3_OnUpdate_m3143017574 (EaseVector3_t2756442322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
