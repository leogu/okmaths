﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLookAtGameObject
struct DOTweenTransformLookAtGameObject_t3383442769;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtGameObject::.ctor()
extern "C"  void DOTweenTransformLookAtGameObject__ctor_m2248820325 (DOTweenTransformLookAtGameObject_t3383442769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtGameObject::Reset()
extern "C"  void DOTweenTransformLookAtGameObject_Reset_m525188542 (DOTweenTransformLookAtGameObject_t3383442769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtGameObject::OnEnter()
extern "C"  void DOTweenTransformLookAtGameObject_OnEnter_m3164620876 (DOTweenTransformLookAtGameObject_t3383442769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtGameObject::<OnEnter>m__C0()
extern "C"  void DOTweenTransformLookAtGameObject_U3COnEnterU3Em__C0_m2266099356 (DOTweenTransformLookAtGameObject_t3383442769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLookAtGameObject::<OnEnter>m__C1()
extern "C"  void DOTweenTransformLookAtGameObject_U3COnEnterU3Em__C1_m3811980321 (DOTweenTransformLookAtGameObject_t3383442769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
