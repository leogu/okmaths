﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmBool
struct GetFsmBool_t2873181814;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::.ctor()
extern "C"  void GetFsmBool__ctor_m2070223626 (GetFsmBool_t2873181814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::Reset()
extern "C"  void GetFsmBool_Reset_m1241213571 (GetFsmBool_t2873181814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::OnEnter()
extern "C"  void GetFsmBool_OnEnter_m384565083 (GetFsmBool_t2873181814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::OnUpdate()
extern "C"  void GetFsmBool_OnUpdate_m3361616172 (GetFsmBool_t2873181814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmBool::DoGetFsmBool()
extern "C"  void GetFsmBool_DoGetFsmBool_m63726049 (GetFsmBool_t2873181814 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
