﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal
struct uGuiInputFieldScreenToLocal_t2650285434;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::.ctor()
extern "C"  void uGuiInputFieldScreenToLocal__ctor_m2375425992 (uGuiInputFieldScreenToLocal_t2650285434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::Reset()
extern "C"  void uGuiInputFieldScreenToLocal_Reset_m3036891771 (uGuiInputFieldScreenToLocal_t2650285434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::OnEnter()
extern "C"  void uGuiInputFieldScreenToLocal_OnEnter_m686851619 (uGuiInputFieldScreenToLocal_t2650285434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::OnUpdate()
extern "C"  void uGuiInputFieldScreenToLocal_OnUpdate_m105915166 (uGuiInputFieldScreenToLocal_t2650285434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldScreenToLocal::DoAction()
extern "C"  void uGuiInputFieldScreenToLocal_DoAction_m2933950419 (uGuiInputFieldScreenToLocal_t2650285434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
