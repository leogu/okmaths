﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll
struct EventSystemCurrentRayCastAll_t1077445024;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::.ctor()
extern "C"  void EventSystemCurrentRayCastAll__ctor_m1649875336 (EventSystemCurrentRayCastAll_t1077445024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::Reset()
extern "C"  void EventSystemCurrentRayCastAll_Reset_m857985545 (EventSystemCurrentRayCastAll_t1077445024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::OnEnter()
extern "C"  void EventSystemCurrentRayCastAll_OnEnter_m3339187529 (EventSystemCurrentRayCastAll_t1077445024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::OnUpdate()
extern "C"  void EventSystemCurrentRayCastAll_OnUpdate_m572787518 (EventSystemCurrentRayCastAll_t1077445024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EventSystemCurrentRayCastAll::ExecuteRayCastAll()
extern "C"  void EventSystemCurrentRayCastAll_ExecuteRayCastAll_m400503969 (EventSystemCurrentRayCastAll_t1077445024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
