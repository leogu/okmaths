﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Normalize
struct Vector2Normalize_t315952060;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::.ctor()
extern "C"  void Vector2Normalize__ctor_m2793196226 (Vector2Normalize_t315952060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::Reset()
extern "C"  void Vector2Normalize_Reset_m1106407969 (Vector2Normalize_t315952060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::OnEnter()
extern "C"  void Vector2Normalize_OnEnter_m3609211449 (Vector2Normalize_t315952060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Normalize::OnUpdate()
extern "C"  void Vector2Normalize_OnUpdate_m840505284 (Vector2Normalize_t315952060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
