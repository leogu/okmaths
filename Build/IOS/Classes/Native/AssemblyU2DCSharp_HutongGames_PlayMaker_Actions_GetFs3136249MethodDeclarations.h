﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmEnum
struct GetFsmEnum_t3136249;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::.ctor()
extern "C"  void GetFsmEnum__ctor_m3619114375 (GetFsmEnum_t3136249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::Reset()
extern "C"  void GetFsmEnum_Reset_m1009855110 (GetFsmEnum_t3136249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::OnEnter()
extern "C"  void GetFsmEnum_OnEnter_m1846737200 (GetFsmEnum_t3136249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::OnUpdate()
extern "C"  void GetFsmEnum_OnUpdate_m3921135559 (GetFsmEnum_t3136249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmEnum::DoGetFsmEnum()
extern "C"  void GetFsmEnum_DoGetFsmEnum_m404573185 (GetFsmEnum_t3136249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
