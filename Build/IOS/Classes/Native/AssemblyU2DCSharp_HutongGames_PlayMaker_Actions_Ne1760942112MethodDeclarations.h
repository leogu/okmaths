﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties
struct NetworkGetNextConnectedPlayerProperties_t1760942112;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::.ctor()
extern "C"  void NetworkGetNextConnectedPlayerProperties__ctor_m2425963392 (NetworkGetNextConnectedPlayerProperties_t1760942112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::Reset()
extern "C"  void NetworkGetNextConnectedPlayerProperties_Reset_m2073491005 (NetworkGetNextConnectedPlayerProperties_t1760942112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::OnEnter()
extern "C"  void NetworkGetNextConnectedPlayerProperties_OnEnter_m484456669 (NetworkGetNextConnectedPlayerProperties_t1760942112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetNextConnectedPlayerProperties::DoGetNextPlayerProperties()
extern "C"  void NetworkGetNextConnectedPlayerProperties_DoGetNextPlayerProperties_m1148559934 (NetworkGetNextConnectedPlayerProperties_t1760942112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
