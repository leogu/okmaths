﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerSetProperties
struct MasterServerSetProperties_t2790154512;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::.ctor()
extern "C"  void MasterServerSetProperties__ctor_m2374052100 (MasterServerSetProperties_t2790154512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::Reset()
extern "C"  void MasterServerSetProperties_Reset_m3065803089 (MasterServerSetProperties_t2790154512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::OnEnter()
extern "C"  void MasterServerSetProperties_OnEnter_m1354889553 (MasterServerSetProperties_t2790154512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerSetProperties::SetMasterServerProperties()
extern "C"  void MasterServerSetProperties_SetMasterServerProperties_m2515657932 (MasterServerSetProperties_t2790154512 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
