﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayerName
struct GetAnimatorLayerName_t4121410571;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::.ctor()
extern "C"  void GetAnimatorLayerName__ctor_m1168997447 (GetAnimatorLayerName_t4121410571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::Reset()
extern "C"  void GetAnimatorLayerName_Reset_m369830864 (GetAnimatorLayerName_t4121410571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::OnEnter()
extern "C"  void GetAnimatorLayerName_OnEnter_m582837206 (GetAnimatorLayerName_t4121410571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerName::DoGetLayerName()
extern "C"  void GetAnimatorLayerName_DoGetLayerName_m1259991036 (GetAnimatorLayerName_t4121410571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
