﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge
struct TransformEventsBridge_t2958852998;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::.ctor()
extern "C"  void TransformEventsBridge__ctor_m3142366622 (TransformEventsBridge_t2958852998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::OnTransformParentChanged()
extern "C"  void TransformEventsBridge_OnTransformParentChanged_m2730921497 (TransformEventsBridge_t2958852998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.TransformEventsBridge::OnTransformChildrenChanged()
extern "C"  void TransformEventsBridge_OnTransformChildrenChanged_m904439500 (TransformEventsBridge_t2958852998 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
