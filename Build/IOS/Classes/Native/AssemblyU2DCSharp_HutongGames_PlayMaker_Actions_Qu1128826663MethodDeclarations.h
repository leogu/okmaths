﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionCompare
struct QuaternionCompare_t1128826663;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::.ctor()
extern "C"  void QuaternionCompare__ctor_m2025677365 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::Reset()
extern "C"  void QuaternionCompare_Reset_m741223264 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnEnter()
extern "C"  void QuaternionCompare_OnEnter_m2030393138 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnUpdate()
extern "C"  void QuaternionCompare_OnUpdate_m2542054321 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnLateUpdate()
extern "C"  void QuaternionCompare_OnLateUpdate_m2166311373 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::OnFixedUpdate()
extern "C"  void QuaternionCompare_OnFixedUpdate_m2216313215 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionCompare::DoQuatCompare()
extern "C"  void QuaternionCompare_DoQuatCompare_m2774086162 (QuaternionCompare_t1128826663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
