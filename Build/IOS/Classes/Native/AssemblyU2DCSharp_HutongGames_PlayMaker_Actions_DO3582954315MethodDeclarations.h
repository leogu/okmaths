﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardAll
struct DOTweenControlMethodsPlayForwardAll_t3582954315;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardAll::.ctor()
extern "C"  void DOTweenControlMethodsPlayForwardAll__ctor_m3592990229 (DOTweenControlMethodsPlayForwardAll_t3582954315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardAll::Reset()
extern "C"  void DOTweenControlMethodsPlayForwardAll_Reset_m177348648 (DOTweenControlMethodsPlayForwardAll_t3582954315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayForwardAll::OnEnter()
extern "C"  void DOTweenControlMethodsPlayForwardAll_OnEnter_m3208975506 (DOTweenControlMethodsPlayForwardAll_t3582954315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
