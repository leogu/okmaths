﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Camera
struct Camera_t189460977;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle
struct  RectTransformScreenPointToLocalPointInRectangle_t3742213830  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::screenPointVector2
	FsmVector2_t2430450063 * ___screenPointVector2_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::orScreenPointVector3
	FsmVector3_t3996534004 * ___orScreenPointVector3_13;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::normalizedScreenPoint
	bool ___normalizedScreenPoint_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::camera
	FsmGameObject_t3097142863 * ___camera_15;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::everyFrame
	bool ___everyFrame_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::localPosition
	FsmVector3_t3996534004 * ___localPosition_17;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::localPosition2d
	FsmVector2_t2430450063 * ___localPosition2d_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::isHit
	FsmBool_t664485696 * ___isHit_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::hitEvent
	FsmEvent_t1258573736 * ___hitEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::noHitEvent
	FsmEvent_t1258573736 * ___noHitEvent_21;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::_rt
	RectTransform_t3349966182 * ____rt_22;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::_camera
	Camera_t189460977 * ____camera_23;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_screenPointVector2_12() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___screenPointVector2_12)); }
	inline FsmVector2_t2430450063 * get_screenPointVector2_12() const { return ___screenPointVector2_12; }
	inline FsmVector2_t2430450063 ** get_address_of_screenPointVector2_12() { return &___screenPointVector2_12; }
	inline void set_screenPointVector2_12(FsmVector2_t2430450063 * value)
	{
		___screenPointVector2_12 = value;
		Il2CppCodeGenWriteBarrier(&___screenPointVector2_12, value);
	}

	inline static int32_t get_offset_of_orScreenPointVector3_13() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___orScreenPointVector3_13)); }
	inline FsmVector3_t3996534004 * get_orScreenPointVector3_13() const { return ___orScreenPointVector3_13; }
	inline FsmVector3_t3996534004 ** get_address_of_orScreenPointVector3_13() { return &___orScreenPointVector3_13; }
	inline void set_orScreenPointVector3_13(FsmVector3_t3996534004 * value)
	{
		___orScreenPointVector3_13 = value;
		Il2CppCodeGenWriteBarrier(&___orScreenPointVector3_13, value);
	}

	inline static int32_t get_offset_of_normalizedScreenPoint_14() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___normalizedScreenPoint_14)); }
	inline bool get_normalizedScreenPoint_14() const { return ___normalizedScreenPoint_14; }
	inline bool* get_address_of_normalizedScreenPoint_14() { return &___normalizedScreenPoint_14; }
	inline void set_normalizedScreenPoint_14(bool value)
	{
		___normalizedScreenPoint_14 = value;
	}

	inline static int32_t get_offset_of_camera_15() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___camera_15)); }
	inline FsmGameObject_t3097142863 * get_camera_15() const { return ___camera_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_camera_15() { return &___camera_15; }
	inline void set_camera_15(FsmGameObject_t3097142863 * value)
	{
		___camera_15 = value;
		Il2CppCodeGenWriteBarrier(&___camera_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_localPosition_17() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___localPosition_17)); }
	inline FsmVector3_t3996534004 * get_localPosition_17() const { return ___localPosition_17; }
	inline FsmVector3_t3996534004 ** get_address_of_localPosition_17() { return &___localPosition_17; }
	inline void set_localPosition_17(FsmVector3_t3996534004 * value)
	{
		___localPosition_17 = value;
		Il2CppCodeGenWriteBarrier(&___localPosition_17, value);
	}

	inline static int32_t get_offset_of_localPosition2d_18() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___localPosition2d_18)); }
	inline FsmVector2_t2430450063 * get_localPosition2d_18() const { return ___localPosition2d_18; }
	inline FsmVector2_t2430450063 ** get_address_of_localPosition2d_18() { return &___localPosition2d_18; }
	inline void set_localPosition2d_18(FsmVector2_t2430450063 * value)
	{
		___localPosition2d_18 = value;
		Il2CppCodeGenWriteBarrier(&___localPosition2d_18, value);
	}

	inline static int32_t get_offset_of_isHit_19() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___isHit_19)); }
	inline FsmBool_t664485696 * get_isHit_19() const { return ___isHit_19; }
	inline FsmBool_t664485696 ** get_address_of_isHit_19() { return &___isHit_19; }
	inline void set_isHit_19(FsmBool_t664485696 * value)
	{
		___isHit_19 = value;
		Il2CppCodeGenWriteBarrier(&___isHit_19, value);
	}

	inline static int32_t get_offset_of_hitEvent_20() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___hitEvent_20)); }
	inline FsmEvent_t1258573736 * get_hitEvent_20() const { return ___hitEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_hitEvent_20() { return &___hitEvent_20; }
	inline void set_hitEvent_20(FsmEvent_t1258573736 * value)
	{
		___hitEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___hitEvent_20, value);
	}

	inline static int32_t get_offset_of_noHitEvent_21() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ___noHitEvent_21)); }
	inline FsmEvent_t1258573736 * get_noHitEvent_21() const { return ___noHitEvent_21; }
	inline FsmEvent_t1258573736 ** get_address_of_noHitEvent_21() { return &___noHitEvent_21; }
	inline void set_noHitEvent_21(FsmEvent_t1258573736 * value)
	{
		___noHitEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___noHitEvent_21, value);
	}

	inline static int32_t get_offset_of__rt_22() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ____rt_22)); }
	inline RectTransform_t3349966182 * get__rt_22() const { return ____rt_22; }
	inline RectTransform_t3349966182 ** get_address_of__rt_22() { return &____rt_22; }
	inline void set__rt_22(RectTransform_t3349966182 * value)
	{
		____rt_22 = value;
		Il2CppCodeGenWriteBarrier(&____rt_22, value);
	}

	inline static int32_t get_offset_of__camera_23() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToLocalPointInRectangle_t3742213830, ____camera_23)); }
	inline Camera_t189460977 * get__camera_23() const { return ____camera_23; }
	inline Camera_t189460977 ** get_address_of__camera_23() { return &____camera_23; }
	inline void set__camera_23(Camera_t189460977 * value)
	{
		____camera_23 = value;
		Il2CppCodeGenWriteBarrier(&____camera_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
