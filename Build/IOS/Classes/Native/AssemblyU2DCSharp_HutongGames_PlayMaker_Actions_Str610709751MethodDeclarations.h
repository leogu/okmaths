﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StringSwitch
struct StringSwitch_t610709751;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StringSwitch::.ctor()
extern "C"  void StringSwitch__ctor_m1449255377 (StringSwitch_t610709751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::Reset()
extern "C"  void StringSwitch_Reset_m650091360 (StringSwitch_t610709751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::OnEnter()
extern "C"  void StringSwitch_OnEnter_m1870286322 (StringSwitch_t610709751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::OnUpdate()
extern "C"  void StringSwitch_OnUpdate_m2941975429 (StringSwitch_t610709751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StringSwitch::DoStringSwitch()
extern "C"  void StringSwitch_DoStringSwitch_m2180285569 (StringSwitch_t610709751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
