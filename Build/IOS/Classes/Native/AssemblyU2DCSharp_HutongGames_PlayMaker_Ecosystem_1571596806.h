﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct  PlayMakerEvent_t1571596806  : public Il2CppObject
{
public:
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::eventName
	String_t* ___eventName_1;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::allowLocalEvents
	bool ___allowLocalEvents_2;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::defaultEventName
	String_t* ___defaultEventName_3;

public:
	inline static int32_t get_offset_of_eventName_1() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806, ___eventName_1)); }
	inline String_t* get_eventName_1() const { return ___eventName_1; }
	inline String_t** get_address_of_eventName_1() { return &___eventName_1; }
	inline void set_eventName_1(String_t* value)
	{
		___eventName_1 = value;
		Il2CppCodeGenWriteBarrier(&___eventName_1, value);
	}

	inline static int32_t get_offset_of_allowLocalEvents_2() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806, ___allowLocalEvents_2)); }
	inline bool get_allowLocalEvents_2() const { return ___allowLocalEvents_2; }
	inline bool* get_address_of_allowLocalEvents_2() { return &___allowLocalEvents_2; }
	inline void set_allowLocalEvents_2(bool value)
	{
		___allowLocalEvents_2 = value;
	}

	inline static int32_t get_offset_of_defaultEventName_3() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806, ___defaultEventName_3)); }
	inline String_t* get_defaultEventName_3() const { return ___defaultEventName_3; }
	inline String_t** get_address_of_defaultEventName_3() { return &___defaultEventName_3; }
	inline void set_defaultEventName_3(String_t* value)
	{
		___defaultEventName_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultEventName_3, value);
	}
};

struct PlayMakerEvent_t1571596806_StaticFields
{
public:
	// PlayMakerFSM HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::FsmEventSender
	PlayMakerFSM_t437737208 * ___FsmEventSender_0;

public:
	inline static int32_t get_offset_of_FsmEventSender_0() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806_StaticFields, ___FsmEventSender_0)); }
	inline PlayMakerFSM_t437737208 * get_FsmEventSender_0() const { return ___FsmEventSender_0; }
	inline PlayMakerFSM_t437737208 ** get_address_of_FsmEventSender_0() { return &___FsmEventSender_0; }
	inline void set_FsmEventSender_0(PlayMakerFSM_t437737208 * value)
	{
		___FsmEventSender_0 = value;
		Il2CppCodeGenWriteBarrier(&___FsmEventSender_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
