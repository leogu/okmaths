﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSetIsInteractable
struct uGuiSetIsInteractable_t1942692376;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::.ctor()
extern "C"  void uGuiSetIsInteractable__ctor_m3509962098 (uGuiSetIsInteractable_t1942692376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::Reset()
extern "C"  void uGuiSetIsInteractable_Reset_m4132749677 (uGuiSetIsInteractable_t1942692376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::OnEnter()
extern "C"  void uGuiSetIsInteractable_OnEnter_m2207586381 (uGuiSetIsInteractable_t1942692376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::DoSetValue()
extern "C"  void uGuiSetIsInteractable_DoSetValue_m2508090672 (uGuiSetIsInteractable_t1942692376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetIsInteractable::OnExit()
extern "C"  void uGuiSetIsInteractable_OnExit_m1409670841 (uGuiSetIsInteractable_t1942692376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
