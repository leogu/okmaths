﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialOffset
struct DOTweenMaterialOffset_t1624340250;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffset::.ctor()
extern "C"  void DOTweenMaterialOffset__ctor_m3511203176 (DOTweenMaterialOffset_t1624340250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffset::Reset()
extern "C"  void DOTweenMaterialOffset_Reset_m4235245403 (DOTweenMaterialOffset_t1624340250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffset::OnEnter()
extern "C"  void DOTweenMaterialOffset_OnEnter_m442983811 (DOTweenMaterialOffset_t1624340250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffset::<OnEnter>m__5A()
extern "C"  void DOTweenMaterialOffset_U3COnEnterU3Em__5A_m2530517418 (DOTweenMaterialOffset_t1624340250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialOffset::<OnEnter>m__5B()
extern "C"  void DOTweenMaterialOffset_U3COnEnterU3Em__5B_m2530517451 (DOTweenMaterialOffset_t1624340250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
