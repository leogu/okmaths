﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::.ctor()
#define List_1__ctor_m572945232(__this, method) ((  void (*) (List_1_t2177637235 *, const MethodInfo*))List_1__ctor_m365405030_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4275374490(__this, ___collection0, method) ((  void (*) (List_1_t2177637235 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1612406893_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::.ctor(System.Int32)
#define List_1__ctor_m1080839776(__this, ___capacity0, method) ((  void (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::.cctor()
#define List_1__cctor_m1464597544(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m696753351(__this, method) ((  Il2CppObject* (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3809219707(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2177637235 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3377279238(__this, method) ((  Il2CppObject * (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m4098212523(__this, ___item0, method) ((  int32_t (*) (List_1_t2177637235 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1805070867(__this, ___item0, method) ((  bool (*) (List_1_t2177637235 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1782565905(__this, ___item0, method) ((  int32_t (*) (List_1_t2177637235 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1756263300(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2177637235 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2547806178(__this, ___item0, method) ((  void (*) (List_1_t2177637235 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4248561826(__this, method) ((  bool (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1074739043(__this, method) ((  bool (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m1067118651(__this, method) ((  Il2CppObject * (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1846750504(__this, method) ((  bool (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2237405247(__this, method) ((  bool (*) (List_1_t2177637235 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2055195236(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m998279121(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2177637235 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Add(T)
#define List_1_Add_m3469046652(__this, ___item0, method) ((  void (*) (List_1_t2177637235 *, FsmEnum_t2808516103 *, const MethodInfo*))List_1_Add_m567051994_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1194728167(__this, ___newCount0, method) ((  void (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m4129784736(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2177637235 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m4258042071(__this, ___collection0, method) ((  void (*) (List_1_t2177637235 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1716526359(__this, ___enumerable0, method) ((  void (*) (List_1_t2177637235 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3582630062(__this, ___collection0, method) ((  void (*) (List_1_t2177637235 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::AsReadOnly()
#define List_1_AsReadOnly_m1318329463(__this, method) ((  ReadOnlyCollection_1_t2994301795 * (*) (List_1_t2177637235 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Clear()
#define List_1_Clear_m1524428532(__this, method) ((  void (*) (List_1_t2177637235 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Contains(T)
#define List_1_Contains_m205831526(__this, ___item0, method) ((  bool (*) (List_1_t2177637235 *, FsmEnum_t2808516103 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1964587420(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2177637235 *, FsmEnumU5BU5D_t2440162814*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Find(System.Predicate`1<T>)
#define List_1_Find_m898436496(__this, ___match0, method) ((  FsmEnum_t2808516103 * (*) (List_1_t2177637235 *, Predicate_1_t1251486218 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2069343699(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1251486218 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m141218720(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2177637235 *, int32_t, int32_t, Predicate_1_t1251486218 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::GetEnumerator()
#define List_1_GetEnumerator_m607659381(__this, method) ((  Enumerator_t1712366909  (*) (List_1_t2177637235 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::IndexOf(T)
#define List_1_IndexOf_m557961010(__this, ___item0, method) ((  int32_t (*) (List_1_t2177637235 *, FsmEnum_t2808516103 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m1337726471(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2177637235 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m134203452(__this, ___index0, method) ((  void (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Insert(System.Int32,T)
#define List_1_Insert_m154578493(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2177637235 *, int32_t, FsmEnum_t2808516103 *, const MethodInfo*))List_1_Insert_m283311118_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3067749082(__this, ___collection0, method) ((  void (*) (List_1_t2177637235 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Remove(T)
#define List_1_Remove_m2957404413(__this, ___item0, method) ((  bool (*) (List_1_t2177637235 *, FsmEnum_t2808516103 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m4085875899(__this, ___match0, method) ((  int32_t (*) (List_1_t2177637235 *, Predicate_1_t1251486218 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m1889079801(__this, ___index0, method) ((  void (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m2603257216(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2177637235 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Reverse()
#define List_1_Reverse_m3445154927(__this, method) ((  void (*) (List_1_t2177637235 *, const MethodInfo*))List_1_Reverse_m3280715839_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Sort()
#define List_1_Sort_m4084198533(__this, method) ((  void (*) (List_1_t2177637235 *, const MethodInfo*))List_1_Sort_m1888745365_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3099660432(__this, ___comparison0, method) ((  void (*) (List_1_t2177637235 *, Comparison_1_t4070254954 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::ToArray()
#define List_1_ToArray_m406780408(__this, method) ((  FsmEnumU5BU5D_t2440162814* (*) (List_1_t2177637235 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::TrimExcess()
#define List_1_TrimExcess_m126011022(__this, method) ((  void (*) (List_1_t2177637235 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::get_Capacity()
#define List_1_get_Capacity_m2456742356(__this, method) ((  int32_t (*) (List_1_t2177637235 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4166179223(__this, ___value0, method) ((  void (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::get_Count()
#define List_1_get_Count_m1429113714(__this, method) ((  int32_t (*) (List_1_t2177637235 *, const MethodInfo*))List_1_get_Count_m1012630527_gshared)(__this, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::get_Item(System.Int32)
#define List_1_get_Item_m3590507367(__this, ___index0, method) ((  FsmEnum_t2808516103 * (*) (List_1_t2177637235 *, int32_t, const MethodInfo*))List_1_get_Item_m1650084162_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>::set_Item(System.Int32,T)
#define List_1_set_Item_m4243323438(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2177637235 *, int32_t, FsmEnum_t2808516103 *, const MethodInfo*))List_1_set_Item_m1410262499_gshared)(__this, ___index0, ___value1, method)
