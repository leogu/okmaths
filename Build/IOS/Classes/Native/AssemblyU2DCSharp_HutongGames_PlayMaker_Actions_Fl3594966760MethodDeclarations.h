﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatSwitch
struct FloatSwitch_t3594966760;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::.ctor()
extern "C"  void FloatSwitch__ctor_m2854543956 (FloatSwitch_t3594966760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::Reset()
extern "C"  void FloatSwitch_Reset_m3405375681 (FloatSwitch_t3594966760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnEnter()
extern "C"  void FloatSwitch_OnEnter_m2202039929 (FloatSwitch_t3594966760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::OnUpdate()
extern "C"  void FloatSwitch_OnUpdate_m3533949498 (FloatSwitch_t3594966760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatSwitch::DoFloatSwitch()
extern "C"  void FloatSwitch_DoFloatSwitch_m3815385597 (FloatSwitch_t3594966760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
