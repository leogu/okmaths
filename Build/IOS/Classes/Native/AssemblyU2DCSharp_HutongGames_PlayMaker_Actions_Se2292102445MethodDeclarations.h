﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SendMessage
struct SendMessage_t2292102445;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SendMessage::.ctor()
extern "C"  void SendMessage__ctor_m1424318385 (SendMessage_t2292102445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendMessage::Reset()
extern "C"  void SendMessage_Reset_m269574214 (SendMessage_t2292102445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendMessage::OnEnter()
extern "C"  void SendMessage_OnEnter_m861365468 (SendMessage_t2292102445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SendMessage::DoSendMessage()
extern "C"  void SendMessage_DoSendMessage_m1290580433 (SendMessage_t2292102445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
