﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.UnityAdsUnsupported
struct UnityAdsUnsupported_t1914686426;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1591511100.h"

// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::.ctor()
extern "C"  void UnityAdsUnsupported__ctor_m1034993361 (UnityAdsUnsupported_t1914686426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::RegisterNative(System.String)
extern "C"  void UnityAdsUnsupported_RegisterNative_m3985146115 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___extensionPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::Init(System.String,System.Boolean)
extern "C"  void UnityAdsUnsupported_Init_m144900996 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___gameId0, bool ___testModeEnabled1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsUnsupported::CanShowAds(System.String)
extern "C"  bool UnityAdsUnsupported_CanShowAds_m756742030 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___zoneId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::SetCampaignDataURL(System.String)
extern "C"  void UnityAdsUnsupported_SetCampaignDataURL_m2292937510 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::SetLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C"  void UnityAdsUnsupported_SetLogLevel_m4284147799 (UnityAdsUnsupported_t1914686426 * __this, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Advertisements.UnityAdsUnsupported::Show(System.String,System.String)
extern "C"  bool UnityAdsUnsupported_Show_m886011964 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___zoneId0, String_t* ___gamerSid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
