﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax
struct  uGuiSliderSetMinMax_t2976885599  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::minValue
	FsmFloat_t937133978 * ___minValue_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::maxValue
	FsmFloat_t937133978 * ___maxValue_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::_slider
	Slider_t297367283 * ____slider_16;
	// System.Single HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::_originalMinValue
	float ____originalMinValue_17;
	// System.Single HutongGames.PlayMaker.Actions.uGuiSliderSetMinMax::_originalMaxValue
	float ____originalMaxValue_18;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_minValue_12() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ___minValue_12)); }
	inline FsmFloat_t937133978 * get_minValue_12() const { return ___minValue_12; }
	inline FsmFloat_t937133978 ** get_address_of_minValue_12() { return &___minValue_12; }
	inline void set_minValue_12(FsmFloat_t937133978 * value)
	{
		___minValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___minValue_12, value);
	}

	inline static int32_t get_offset_of_maxValue_13() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ___maxValue_13)); }
	inline FsmFloat_t937133978 * get_maxValue_13() const { return ___maxValue_13; }
	inline FsmFloat_t937133978 ** get_address_of_maxValue_13() { return &___maxValue_13; }
	inline void set_maxValue_13(FsmFloat_t937133978 * value)
	{
		___maxValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___maxValue_13, value);
	}

	inline static int32_t get_offset_of_resetOnExit_14() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ___resetOnExit_14)); }
	inline FsmBool_t664485696 * get_resetOnExit_14() const { return ___resetOnExit_14; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_14() { return &___resetOnExit_14; }
	inline void set_resetOnExit_14(FsmBool_t664485696 * value)
	{
		___resetOnExit_14 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of__slider_16() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ____slider_16)); }
	inline Slider_t297367283 * get__slider_16() const { return ____slider_16; }
	inline Slider_t297367283 ** get_address_of__slider_16() { return &____slider_16; }
	inline void set__slider_16(Slider_t297367283 * value)
	{
		____slider_16 = value;
		Il2CppCodeGenWriteBarrier(&____slider_16, value);
	}

	inline static int32_t get_offset_of__originalMinValue_17() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ____originalMinValue_17)); }
	inline float get__originalMinValue_17() const { return ____originalMinValue_17; }
	inline float* get_address_of__originalMinValue_17() { return &____originalMinValue_17; }
	inline void set__originalMinValue_17(float value)
	{
		____originalMinValue_17 = value;
	}

	inline static int32_t get_offset_of__originalMaxValue_18() { return static_cast<int32_t>(offsetof(uGuiSliderSetMinMax_t2976885599, ____originalMaxValue_18)); }
	inline float get__originalMaxValue_18() const { return ____originalMaxValue_18; }
	inline float* get_address_of__originalMaxValue_18() { return &____originalMaxValue_18; }
	inline void set__originalMaxValue_18(float value)
	{
		____originalMaxValue_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
