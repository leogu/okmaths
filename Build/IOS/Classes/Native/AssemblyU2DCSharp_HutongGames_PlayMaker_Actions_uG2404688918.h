﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiTextGetText
struct  uGuiTextGetText_t2404688918  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiTextGetText::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiTextGetText::text
	FsmString_t2414474701 * ___text_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiTextGetText::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.UI.Text HutongGames.PlayMaker.Actions.uGuiTextGetText::_text
	Text_t356221433 * ____text_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiTextGetText_t2404688918, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(uGuiTextGetText_t2404688918, ___text_12)); }
	inline FsmString_t2414474701 * get_text_12() const { return ___text_12; }
	inline FsmString_t2414474701 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(FsmString_t2414474701 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier(&___text_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(uGuiTextGetText_t2404688918, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__text_14() { return static_cast<int32_t>(offsetof(uGuiTextGetText_t2404688918, ____text_14)); }
	inline Text_t356221433 * get__text_14() const { return ____text_14; }
	inline Text_t356221433 ** get_address_of__text_14() { return &____text_14; }
	inline void set__text_14(Text_t356221433 * value)
	{
		____text_14 = value;
		Il2CppCodeGenWriteBarrier(&____text_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
