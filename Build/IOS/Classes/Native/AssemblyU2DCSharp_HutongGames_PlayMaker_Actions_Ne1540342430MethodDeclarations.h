﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkDestroy
struct NetworkDestroy_t1540342430;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::.ctor()
extern "C"  void NetworkDestroy__ctor_m3860764076 (NetworkDestroy_t1540342430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::Reset()
extern "C"  void NetworkDestroy_Reset_m3027726603 (NetworkDestroy_t1540342430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::OnEnter()
extern "C"  void NetworkDestroy_OnEnter_m1917442155 (NetworkDestroy_t1540342430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkDestroy::DoDestroy()
extern "C"  void NetworkDestroy_DoDestroy_m1553965027 (NetworkDestroy_t1540342430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
