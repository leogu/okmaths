﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect
struct  RectTransformPixelAdjustRect_t1825828749  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::canvas
	FsmGameObject_t3097142863 * ___canvas_14;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::pixelRect
	FsmRect_t19023354 * ___pixelRect_15;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::_rt
	RectTransform_t3349966182 * ____rt_16;
	// UnityEngine.Canvas HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::_canvas
	Canvas_t209405766 * ____canvas_17;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t1825828749, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_canvas_14() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t1825828749, ___canvas_14)); }
	inline FsmGameObject_t3097142863 * get_canvas_14() const { return ___canvas_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_canvas_14() { return &___canvas_14; }
	inline void set_canvas_14(FsmGameObject_t3097142863 * value)
	{
		___canvas_14 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_14, value);
	}

	inline static int32_t get_offset_of_pixelRect_15() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t1825828749, ___pixelRect_15)); }
	inline FsmRect_t19023354 * get_pixelRect_15() const { return ___pixelRect_15; }
	inline FsmRect_t19023354 ** get_address_of_pixelRect_15() { return &___pixelRect_15; }
	inline void set_pixelRect_15(FsmRect_t19023354 * value)
	{
		___pixelRect_15 = value;
		Il2CppCodeGenWriteBarrier(&___pixelRect_15, value);
	}

	inline static int32_t get_offset_of__rt_16() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t1825828749, ____rt_16)); }
	inline RectTransform_t3349966182 * get__rt_16() const { return ____rt_16; }
	inline RectTransform_t3349966182 ** get_address_of__rt_16() { return &____rt_16; }
	inline void set__rt_16(RectTransform_t3349966182 * value)
	{
		____rt_16 = value;
		Il2CppCodeGenWriteBarrier(&____rt_16, value);
	}

	inline static int32_t get_offset_of__canvas_17() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustRect_t1825828749, ____canvas_17)); }
	inline Canvas_t209405766 * get__canvas_17() const { return ____canvas_17; }
	inline Canvas_t209405766 ** get_address_of__canvas_17() { return &____canvas_17; }
	inline void set__canvas_17(Canvas_t209405766 * value)
	{
		____canvas_17 = value;
		Il2CppCodeGenWriteBarrier(&____canvas_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
