﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_1036036659.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::.ctor()
extern "C"  void PlayMakerEventTarget__ctor_m2985987291 (PlayMakerEventTarget_t2104288969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::.ctor(System.Boolean)
extern "C"  void PlayMakerEventTarget__ctor_m2185263400 (PlayMakerEventTarget_t2104288969 * __this, bool ___includeChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::.ctor(HutongGames.PlayMaker.Ecosystem.Utils.ProxyEventTarget,System.Boolean)
extern "C"  void PlayMakerEventTarget__ctor_m2528146872 (PlayMakerEventTarget_t2104288969 * __this, int32_t ___evenTarget0, bool ___includeChildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::ToString()
extern "C"  String_t* PlayMakerEventTarget_ToString_m2030820816 (PlayMakerEventTarget_t2104288969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
