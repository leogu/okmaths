﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTouchCount
struct GetTouchCount_t1183844550;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::.ctor()
extern "C"  void GetTouchCount__ctor_m184058104 (GetTouchCount_t1183844550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::Reset()
extern "C"  void GetTouchCount_Reset_m201863643 (GetTouchCount_t1183844550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::OnEnter()
extern "C"  void GetTouchCount_OnEnter_m502981611 (GetTouchCount_t1183844550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::OnUpdate()
extern "C"  void GetTouchCount_OnUpdate_m3186714550 (GetTouchCount_t1183844550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTouchCount::DoGetTouchCount()
extern "C"  void GetTouchCount_DoGetTouchCount_m4174813993 (GetTouchCount_t1183844550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
