﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosY
struct DOTweenRectTransformAnchorPosY_t1194942340;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosY::.ctor()
extern "C"  void DOTweenRectTransformAnchorPosY__ctor_m21013562 (DOTweenRectTransformAnchorPosY_t1194942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosY::Reset()
extern "C"  void DOTweenRectTransformAnchorPosY_Reset_m3239556169 (DOTweenRectTransformAnchorPosY_t1194942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosY::OnEnter()
extern "C"  void DOTweenRectTransformAnchorPosY_OnEnter_m1627194753 (DOTweenRectTransformAnchorPosY_t1194942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosY::<OnEnter>m__6E()
extern "C"  void DOTweenRectTransformAnchorPosY_U3COnEnterU3Em__6E_m1435140837 (DOTweenRectTransformAnchorPosY_t1194942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosY::<OnEnter>m__6F()
extern "C"  void DOTweenRectTransformAnchorPosY_U3COnEnterU3Em__6F_m3901902166 (DOTweenRectTransformAnchorPosY_t1194942340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
