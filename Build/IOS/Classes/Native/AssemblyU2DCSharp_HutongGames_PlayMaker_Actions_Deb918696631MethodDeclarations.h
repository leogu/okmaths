﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugBool
struct DebugBool_t918696631;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugBool::.ctor()
extern "C"  void DebugBool__ctor_m89453885 (DebugBool_t918696631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugBool::Reset()
extern "C"  void DebugBool_Reset_m573127544 (DebugBool_t918696631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugBool::OnEnter()
extern "C"  void DebugBool_OnEnter_m4037501186 (DebugBool_t918696631 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
