﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawDebugLine
struct DrawDebugLine_t3762416975;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::.ctor()
extern "C"  void DrawDebugLine__ctor_m3496564049 (DrawDebugLine_t3762416975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::Reset()
extern "C"  void DrawDebugLine_Reset_m1655272012 (DrawDebugLine_t3762416975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugLine::OnUpdate()
extern "C"  void DrawDebugLine_OnUpdate_m2277615885 (DrawDebugLine_t3762416975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
