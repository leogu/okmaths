﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalMove
struct DOTweenTransformLocalMove_t1604248356;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMove::.ctor()
extern "C"  void DOTweenTransformLocalMove__ctor_m1492776432 (DOTweenTransformLocalMove_t1604248356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMove::Reset()
extern "C"  void DOTweenTransformLocalMove_Reset_m2174800485 (DOTweenTransformLocalMove_t1604248356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMove::OnEnter()
extern "C"  void DOTweenTransformLocalMove_OnEnter_m3251386213 (DOTweenTransformLocalMove_t1604248356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMove::<OnEnter>m__B4()
extern "C"  void DOTweenTransformLocalMove_U3COnEnterU3Em__B4_m2282783934 (DOTweenTransformLocalMove_t1604248356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMove::<OnEnter>m__B5()
extern "C"  void DOTweenTransformLocalMove_U3COnEnterU3Em__B5_m2282783901 (DOTweenTransformLocalMove_t1604248356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
