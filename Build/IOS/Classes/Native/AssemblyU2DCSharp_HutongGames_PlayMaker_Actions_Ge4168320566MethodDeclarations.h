﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetKeyUp
struct GetKeyUp_t4168320566;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::.ctor()
extern "C"  void GetKeyUp__ctor_m4054895990 (GetKeyUp_t4168320566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::Reset()
extern "C"  void GetKeyUp_Reset_m1723923143 (GetKeyUp_t4168320566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKeyUp::OnUpdate()
extern "C"  void GetKeyUp_OnUpdate_m3601055528 (GetKeyUp_t4168320566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
