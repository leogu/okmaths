﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntModulo
struct IntModulo_t1135994075;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntModulo::.ctor()
extern "C"  void IntModulo__ctor_m1493980709 (IntModulo_t1135994075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntModulo::Reset()
extern "C"  void IntModulo_Reset_m2310738424 (IntModulo_t1135994075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntModulo::OnEnter()
extern "C"  void IntModulo_OnEnter_m1730943074 (IntModulo_t1135994075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntModulo::OnUpdate()
extern "C"  void IntModulo_OnUpdate_m4292487201 (IntModulo_t1135994075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntModulo::DoModulo()
extern "C"  void IntModulo_DoModulo_m3904594052 (IntModulo_t1135994075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
