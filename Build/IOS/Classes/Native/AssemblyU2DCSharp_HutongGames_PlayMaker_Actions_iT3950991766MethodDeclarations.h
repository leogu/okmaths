﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateAdd
struct iTweenRotateAdd_t3950991766;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::.ctor()
extern "C"  void iTweenRotateAdd__ctor_m746569734 (iTweenRotateAdd_t3950991766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::Reset()
extern "C"  void iTweenRotateAdd_Reset_m3923925611 (iTweenRotateAdd_t3950991766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::OnEnter()
extern "C"  void iTweenRotateAdd_OnEnter_m3250069987 (iTweenRotateAdd_t3950991766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::OnExit()
extern "C"  void iTweenRotateAdd_OnExit_m2882351003 (iTweenRotateAdd_t3950991766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateAdd::DoiTween()
extern "C"  void iTweenRotateAdd_DoiTween_m3203195381 (iTweenRotateAdd_t3950991766 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
