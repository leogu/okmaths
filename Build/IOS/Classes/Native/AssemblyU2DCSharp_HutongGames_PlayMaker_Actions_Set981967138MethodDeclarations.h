﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIText
struct SetGUIText_t981967138;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIText::.ctor()
extern "C"  void SetGUIText__ctor_m588527260 (SetGUIText_t981967138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::Reset()
extern "C"  void SetGUIText_Reset_m2028408587 (SetGUIText_t981967138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::OnEnter()
extern "C"  void SetGUIText_OnEnter_m3756727715 (SetGUIText_t981967138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::OnUpdate()
extern "C"  void SetGUIText_OnUpdate_m177837978 (SetGUIText_t981967138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIText::DoSetGUIText()
extern "C"  void SetGUIText_DoSetGUIText_m1793377953 (SetGUIText_t981967138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
