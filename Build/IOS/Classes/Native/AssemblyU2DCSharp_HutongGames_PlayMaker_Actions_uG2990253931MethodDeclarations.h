﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax
struct uGuiSliderGetMinMax_t2990253931;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::.ctor()
extern "C"  void uGuiSliderGetMinMax__ctor_m184153365 (uGuiSliderGetMinMax_t2990253931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::Reset()
extern "C"  void uGuiSliderGetMinMax_Reset_m932270120 (uGuiSliderGetMinMax_t2990253931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::OnEnter()
extern "C"  void uGuiSliderGetMinMax_OnEnter_m3642929538 (uGuiSliderGetMinMax_t2990253931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::DoGetValue()
extern "C"  void uGuiSliderGetMinMax_DoGetValue_m3926350111 (uGuiSliderGetMinMax_t2990253931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
