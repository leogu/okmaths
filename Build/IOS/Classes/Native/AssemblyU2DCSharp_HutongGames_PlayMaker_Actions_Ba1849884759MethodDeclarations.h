﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BaseFsmVariableAction
struct BaseFsmVariableAction_t1849884759;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableAction::.ctor()
extern "C"  void BaseFsmVariableAction__ctor_m2274704639 (BaseFsmVariableAction_t1849884759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableAction::Reset()
extern "C"  void BaseFsmVariableAction_Reset_m401129812 (BaseFsmVariableAction_t1849884759 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.BaseFsmVariableAction::UpdateCache(UnityEngine.GameObject,System.String)
extern "C"  bool BaseFsmVariableAction_UpdateCache_m3608133638 (BaseFsmVariableAction_t1849884759 * __this, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BaseFsmVariableAction::DoVariableNotFound(System.String)
extern "C"  void BaseFsmVariableAction_DoVariableNotFound_m1061033127 (BaseFsmVariableAction_t1849884759 * __this, String_t* ___variableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
