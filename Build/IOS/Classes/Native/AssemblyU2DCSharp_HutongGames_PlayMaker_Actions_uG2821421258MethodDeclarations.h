﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties
struct uGuiRadialLayoutSetProperties_t2821421258;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::.ctor()
extern "C"  void uGuiRadialLayoutSetProperties__ctor_m448000470 (uGuiRadialLayoutSetProperties_t2821421258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::Reset()
extern "C"  void uGuiRadialLayoutSetProperties_Reset_m3541832619 (uGuiRadialLayoutSetProperties_t2821421258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::OnEnter()
extern "C"  void uGuiRadialLayoutSetProperties_OnEnter_m3500365131 (uGuiRadialLayoutSetProperties_t2821421258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::OnUpdate()
extern "C"  void uGuiRadialLayoutSetProperties_OnUpdate_m4140768664 (uGuiRadialLayoutSetProperties_t2821421258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::DoSetValues()
extern "C"  void uGuiRadialLayoutSetProperties_DoSetValues_m1953659811 (uGuiRadialLayoutSetProperties_t2821421258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
