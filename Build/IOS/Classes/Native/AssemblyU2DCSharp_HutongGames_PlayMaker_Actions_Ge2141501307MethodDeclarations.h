﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight
struct GetAnimatorGravityWeight_t2141501307;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::.ctor()
extern "C"  void GetAnimatorGravityWeight__ctor_m2744900633 (GetAnimatorGravityWeight_t2141501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::Reset()
extern "C"  void GetAnimatorGravityWeight_Reset_m1056801184 (GetAnimatorGravityWeight_t2141501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnEnter()
extern "C"  void GetAnimatorGravityWeight_OnEnter_m4001753586 (GetAnimatorGravityWeight_t2141501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::OnActionUpdate()
extern "C"  void GetAnimatorGravityWeight_OnActionUpdate_m136384031 (GetAnimatorGravityWeight_t2141501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorGravityWeight::DoGetGravityWeight()
extern "C"  void GetAnimatorGravityWeight_DoGetGravityWeight_m3165945980 (GetAnimatorGravityWeight_t2141501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
