﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ResetGUIMatrix
struct ResetGUIMatrix_t1993943153;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ResetGUIMatrix::.ctor()
extern "C"  void ResetGUIMatrix__ctor_m1124395855 (ResetGUIMatrix_t1993943153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ResetGUIMatrix::OnGUI()
extern "C"  void ResetGUIMatrix_OnGUI_m3030653573 (ResetGUIMatrix_t1993943153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
