﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorBody
struct GetAnimatorBody_t3923296129;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::.ctor()
extern "C"  void GetAnimatorBody__ctor_m2789214227 (GetAnimatorBody_t3923296129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::Reset()
extern "C"  void GetAnimatorBody_Reset_m2831098782 (GetAnimatorBody_t3923296129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnEnter()
extern "C"  void GetAnimatorBody_OnEnter_m1896957208 (GetAnimatorBody_t3923296129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::OnActionUpdate()
extern "C"  void GetAnimatorBody_OnActionUpdate_m4020240713 (GetAnimatorBody_t3923296129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorBody::DoGetBodyPosition()
extern "C"  void GetAnimatorBody_DoGetBodyPosition_m2055180147 (GetAnimatorBody_t3923296129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
