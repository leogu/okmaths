﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// System.String
struct String_t;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// System.Object
struct Il2CppObject;
// DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t3395645806;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTween50_U3CModuleU3E3783534214.h"
#include "DOTween50_U3CModuleU3E3783534214MethodDeclarations.h"
#include "DOTween50_DG_Tweening_ShortcutExtensions501867980067.h"
#include "DOTween50_DG_Tweening_ShortcutExtensions501867980067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixer3244290001.h"
#include "mscorlib_System_String2029220233.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "mscorlib_System_Single2076509932.h"
#include "DOTween50_DG_Tweening_ShortcutExtensions50_U3CU3Ec3395645806MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOTween2276353038MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830MethodDeclarations.h"
#include "DOTween50_DG_Tweening_ShortcutExtensions50_U3CU3Ec3395645806.h"
#include "mscorlib_System_Void1841601450.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixer3244290001MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"

// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2279406887 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions50::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass0_0_t3395645806_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3858616074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3734738918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__0_m1497172051_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3288120768_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__1_m4215540369_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2613085532_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var;
extern const uint32_t ShortcutExtensions50_DOSetFloat_m3076787227_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions50_DOSetFloat_m3076787227 (Il2CppObject * __this /* static, unused */, AudioMixer_t3244290001 * ___target0, String_t* ___floatName1, float ___endValue2, float ___duration3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions50_DOSetFloat_m3076787227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass0_0_t3395645806 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass0_0_t3395645806 * L_0 = (U3CU3Ec__DisplayClass0_0_t3395645806 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_0_t3395645806_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_0__ctor_m1183603446(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass0_0_t3395645806 * L_1 = V_0;
		AudioMixer_t3244290001 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass0_0_t3395645806 * L_3 = V_0;
		String_t* L_4 = ___floatName1;
		NullCheck(L_3);
		L_3->set_floatName_1(L_4);
		U3CU3Ec__DisplayClass0_0_t3395645806 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__0_m1497172051_MethodInfo_var);
		DOGetter_1_t3858616074 * L_7 = (DOGetter_1_t3858616074 *)il2cpp_codegen_object_new(DOGetter_1_t3858616074_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3288120768(L_7, L_5, L_6, /*hidden argument*/DOGetter_1__ctor_m3288120768_MethodInfo_var);
		U3CU3Ec__DisplayClass0_0_t3395645806 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__1_m4215540369_MethodInfo_var);
		DOSetter_1_t3734738918 * L_10 = (DOSetter_1_t3734738918 *)il2cpp_codegen_object_new(DOSetter_1_t3734738918_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2613085532(L_10, L_8, L_9, /*hidden argument*/DOSetter_1__ctor_m2613085532_MethodInfo_var);
		float L_11 = ___endValue2;
		float L_12 = ___duration3;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2279406887 * L_13 = DOTween_To_m914278462(NULL /*static, unused*/, L_7, L_10, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass0_0_t3395645806 * L_14 = V_0;
		NullCheck(L_14);
		AudioMixer_t3244290001 * L_15 = L_14->get_target_0();
		TweenerCore_3_t2279406887 * L_16 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var);
		return L_16;
	}
}
// System.Void DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m1183603446 (U3CU3Ec__DisplayClass0_0_t3395645806 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::<DOSetFloat>b__0()
extern "C"  float U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__0_m1497172051 (U3CU3Ec__DisplayClass0_0_t3395645806 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		AudioMixer_t3244290001 * L_0 = __this->get_target_0();
		String_t* L_1 = __this->get_floatName_1();
		NullCheck(L_0);
		AudioMixer_GetFloat_m2814101327(L_0, L_1, (&V_0), /*hidden argument*/NULL);
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::<DOSetFloat>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__1_m4215540369 (U3CU3Ec__DisplayClass0_0_t3395645806 * __this, float ___x0, const MethodInfo* method)
{
	{
		AudioMixer_t3244290001 * L_0 = __this->get_target_0();
		String_t* L_1 = __this->get_floatName_1();
		float L_2 = ___x0;
		NullCheck(L_0);
		AudioMixer_SetFloat_m1339880289(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
