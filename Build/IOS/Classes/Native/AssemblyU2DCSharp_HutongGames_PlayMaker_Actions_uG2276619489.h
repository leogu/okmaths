﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiGraphicGetColor
struct  uGuiGraphicGetColor_t2276619489  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::color
	FsmColor_t118301965 * ___color_12;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::everyFrame
	bool ___everyFrame_13;
	// UnityEngine.UI.Graphic HutongGames.PlayMaker.Actions.uGuiGraphicGetColor::_component
	Graphic_t2426225576 * ____component_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiGraphicGetColor_t2276619489, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_color_12() { return static_cast<int32_t>(offsetof(uGuiGraphicGetColor_t2276619489, ___color_12)); }
	inline FsmColor_t118301965 * get_color_12() const { return ___color_12; }
	inline FsmColor_t118301965 ** get_address_of_color_12() { return &___color_12; }
	inline void set_color_12(FsmColor_t118301965 * value)
	{
		___color_12 = value;
		Il2CppCodeGenWriteBarrier(&___color_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(uGuiGraphicGetColor_t2276619489, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of__component_14() { return static_cast<int32_t>(offsetof(uGuiGraphicGetColor_t2276619489, ____component_14)); }
	inline Graphic_t2426225576 * get__component_14() const { return ____component_14; }
	inline Graphic_t2426225576 ** get_address_of__component_14() { return &____component_14; }
	inline void set__component_14(Graphic_t2426225576 * value)
	{
		____component_14 = value;
		Il2CppCodeGenWriteBarrier(&____component_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
