﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetNextRayCast2d
struct GetNextRayCast2d_t3283411550;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::.ctor()
extern "C"  void GetNextRayCast2d__ctor_m807646730 (GetNextRayCast2d_t3283411550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::Reset()
extern "C"  void GetNextRayCast2d_Reset_m625608323 (GetNextRayCast2d_t3283411550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::OnEnter()
extern "C"  void GetNextRayCast2d_OnEnter_m2643393683 (GetNextRayCast2d_t3283411550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetNextRayCast2d::DoGetNextCollider()
extern "C"  void GetNextRayCast2d_DoGetNextCollider_m244976992 (GetNextRayCast2d_t3283411550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] HutongGames.PlayMaker.Actions.GetNextRayCast2d::GetRayCastAll()
extern "C"  RaycastHit2DU5BU5D_t4176517891* GetNextRayCast2d_GetRayCastAll_m2153860008 (GetNextRayCast2d_t3283411550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
