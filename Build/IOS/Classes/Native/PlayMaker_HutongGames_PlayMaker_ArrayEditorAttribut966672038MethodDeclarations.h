﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ArrayEditorAttribute
struct ArrayEditorAttribute_t966672038;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"

// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.ArrayEditorAttribute::get_VariableType()
extern "C"  int32_t ArrayEditorAttribute_get_VariableType_m1006147853 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ArrayEditorAttribute::get_ObjectType()
extern "C"  Type_t * ArrayEditorAttribute_get_ObjectType_m4098360645 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ArrayEditorAttribute::get_ElementName()
extern "C"  String_t* ArrayEditorAttribute_get_ElementName_m441634056 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::get_FixedSize()
extern "C"  int32_t ArrayEditorAttribute_get_FixedSize_m286832485 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ArrayEditorAttribute::get_Resizable()
extern "C"  bool ArrayEditorAttribute_get_Resizable_m1384443605 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::get_MinSize()
extern "C"  int32_t ArrayEditorAttribute_get_MinSize_m452562973 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ArrayEditorAttribute::get_MaxSize()
extern "C"  int32_t ArrayEditorAttribute_get_MaxSize_m2503592375 (ArrayEditorAttribute_t966672038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ArrayEditorAttribute::.ctor(HutongGames.PlayMaker.VariableType,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void ArrayEditorAttribute__ctor_m1786204934 (ArrayEditorAttribute_t966672038 * __this, int32_t ___variableType0, String_t* ___elementName1, int32_t ___fixedSize2, int32_t ___minSize3, int32_t ___maxSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ArrayEditorAttribute::.ctor(System.Type,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void ArrayEditorAttribute__ctor_m4205892161 (ArrayEditorAttribute_t966672038 * __this, Type_t * ___objectType0, String_t* ___elementName1, int32_t ___fixedSize2, int32_t ___minSize3, int32_t ___maxSize4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
