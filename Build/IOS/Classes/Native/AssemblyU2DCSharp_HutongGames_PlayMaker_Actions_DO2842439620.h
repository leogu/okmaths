﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty
struct  DOTweenMaterialFloatProperty_t2842439620  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::to
	FsmFloat_t937133978 * ___to_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::setRelative
	FsmBool_t664485696 * ___setRelative_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::property
	FsmString_t2414474701 * ___property_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::duration
	FsmFloat_t937133978 * ___duration_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::startDelay
	FsmFloat_t937133978 * ___startDelay_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::playInReverse
	FsmBool_t664485696 * ___playInReverse_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::setReverseRelative
	FsmBool_t664485696 * ___setReverseRelative_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::startEvent
	FsmEvent_t1258573736 * ___startEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_21;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_22;
	// System.String HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::tweenIdDescription
	String_t* ___tweenIdDescription_23;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::tweenIdType
	int32_t ___tweenIdType_24;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::stringAsId
	FsmString_t2414474701 * ___stringAsId_25;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::tagAsId
	FsmString_t2414474701 * ___tagAsId_26;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::selectedEase
	int32_t ___selectedEase_27;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::easeType
	int32_t ___easeType_28;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_29;
	// System.String HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_30;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::loops
	FsmInt_t1273009179 * ___loops_31;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::loopType
	int32_t ___loopType_32;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::recyclable
	FsmBool_t664485696 * ___recyclable_34;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::updateType
	int32_t ___updateType_35;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_36;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::debugThis
	FsmBool_t664485696 * ___debugThis_37;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenMaterialFloatProperty::tweener
	Tweener_t760404022 * ___tweener_38;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_to_12() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___to_12)); }
	inline FsmFloat_t937133978 * get_to_12() const { return ___to_12; }
	inline FsmFloat_t937133978 ** get_address_of_to_12() { return &___to_12; }
	inline void set_to_12(FsmFloat_t937133978 * value)
	{
		___to_12 = value;
		Il2CppCodeGenWriteBarrier(&___to_12, value);
	}

	inline static int32_t get_offset_of_setRelative_13() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___setRelative_13)); }
	inline FsmBool_t664485696 * get_setRelative_13() const { return ___setRelative_13; }
	inline FsmBool_t664485696 ** get_address_of_setRelative_13() { return &___setRelative_13; }
	inline void set_setRelative_13(FsmBool_t664485696 * value)
	{
		___setRelative_13 = value;
		Il2CppCodeGenWriteBarrier(&___setRelative_13, value);
	}

	inline static int32_t get_offset_of_property_14() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___property_14)); }
	inline FsmString_t2414474701 * get_property_14() const { return ___property_14; }
	inline FsmString_t2414474701 ** get_address_of_property_14() { return &___property_14; }
	inline void set_property_14(FsmString_t2414474701 * value)
	{
		___property_14 = value;
		Il2CppCodeGenWriteBarrier(&___property_14, value);
	}

	inline static int32_t get_offset_of_duration_15() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___duration_15)); }
	inline FsmFloat_t937133978 * get_duration_15() const { return ___duration_15; }
	inline FsmFloat_t937133978 ** get_address_of_duration_15() { return &___duration_15; }
	inline void set_duration_15(FsmFloat_t937133978 * value)
	{
		___duration_15 = value;
		Il2CppCodeGenWriteBarrier(&___duration_15, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_16() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___setSpeedBased_16)); }
	inline FsmBool_t664485696 * get_setSpeedBased_16() const { return ___setSpeedBased_16; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_16() { return &___setSpeedBased_16; }
	inline void set_setSpeedBased_16(FsmBool_t664485696 * value)
	{
		___setSpeedBased_16 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_16, value);
	}

	inline static int32_t get_offset_of_startDelay_17() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___startDelay_17)); }
	inline FsmFloat_t937133978 * get_startDelay_17() const { return ___startDelay_17; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_17() { return &___startDelay_17; }
	inline void set_startDelay_17(FsmFloat_t937133978 * value)
	{
		___startDelay_17 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_17, value);
	}

	inline static int32_t get_offset_of_playInReverse_18() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___playInReverse_18)); }
	inline FsmBool_t664485696 * get_playInReverse_18() const { return ___playInReverse_18; }
	inline FsmBool_t664485696 ** get_address_of_playInReverse_18() { return &___playInReverse_18; }
	inline void set_playInReverse_18(FsmBool_t664485696 * value)
	{
		___playInReverse_18 = value;
		Il2CppCodeGenWriteBarrier(&___playInReverse_18, value);
	}

	inline static int32_t get_offset_of_setReverseRelative_19() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___setReverseRelative_19)); }
	inline FsmBool_t664485696 * get_setReverseRelative_19() const { return ___setReverseRelative_19; }
	inline FsmBool_t664485696 ** get_address_of_setReverseRelative_19() { return &___setReverseRelative_19; }
	inline void set_setReverseRelative_19(FsmBool_t664485696 * value)
	{
		___setReverseRelative_19 = value;
		Il2CppCodeGenWriteBarrier(&___setReverseRelative_19, value);
	}

	inline static int32_t get_offset_of_startEvent_20() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___startEvent_20)); }
	inline FsmEvent_t1258573736 * get_startEvent_20() const { return ___startEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_20() { return &___startEvent_20; }
	inline void set_startEvent_20(FsmEvent_t1258573736 * value)
	{
		___startEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_20, value);
	}

	inline static int32_t get_offset_of_finishEvent_21() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___finishEvent_21)); }
	inline FsmEvent_t1258573736 * get_finishEvent_21() const { return ___finishEvent_21; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_21() { return &___finishEvent_21; }
	inline void set_finishEvent_21(FsmEvent_t1258573736 * value)
	{
		___finishEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_21, value);
	}

	inline static int32_t get_offset_of_finishImmediately_22() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___finishImmediately_22)); }
	inline FsmBool_t664485696 * get_finishImmediately_22() const { return ___finishImmediately_22; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_22() { return &___finishImmediately_22; }
	inline void set_finishImmediately_22(FsmBool_t664485696 * value)
	{
		___finishImmediately_22 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_22, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_23() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___tweenIdDescription_23)); }
	inline String_t* get_tweenIdDescription_23() const { return ___tweenIdDescription_23; }
	inline String_t** get_address_of_tweenIdDescription_23() { return &___tweenIdDescription_23; }
	inline void set_tweenIdDescription_23(String_t* value)
	{
		___tweenIdDescription_23 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_23, value);
	}

	inline static int32_t get_offset_of_tweenIdType_24() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___tweenIdType_24)); }
	inline int32_t get_tweenIdType_24() const { return ___tweenIdType_24; }
	inline int32_t* get_address_of_tweenIdType_24() { return &___tweenIdType_24; }
	inline void set_tweenIdType_24(int32_t value)
	{
		___tweenIdType_24 = value;
	}

	inline static int32_t get_offset_of_stringAsId_25() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___stringAsId_25)); }
	inline FsmString_t2414474701 * get_stringAsId_25() const { return ___stringAsId_25; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_25() { return &___stringAsId_25; }
	inline void set_stringAsId_25(FsmString_t2414474701 * value)
	{
		___stringAsId_25 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_25, value);
	}

	inline static int32_t get_offset_of_tagAsId_26() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___tagAsId_26)); }
	inline FsmString_t2414474701 * get_tagAsId_26() const { return ___tagAsId_26; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_26() { return &___tagAsId_26; }
	inline void set_tagAsId_26(FsmString_t2414474701 * value)
	{
		___tagAsId_26 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_26, value);
	}

	inline static int32_t get_offset_of_selectedEase_27() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___selectedEase_27)); }
	inline int32_t get_selectedEase_27() const { return ___selectedEase_27; }
	inline int32_t* get_address_of_selectedEase_27() { return &___selectedEase_27; }
	inline void set_selectedEase_27(int32_t value)
	{
		___selectedEase_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_animationCurve_29() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___animationCurve_29)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_29() const { return ___animationCurve_29; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_29() { return &___animationCurve_29; }
	inline void set_animationCurve_29(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_29 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_29, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_30() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___loopsDescriptionArea_30)); }
	inline String_t* get_loopsDescriptionArea_30() const { return ___loopsDescriptionArea_30; }
	inline String_t** get_address_of_loopsDescriptionArea_30() { return &___loopsDescriptionArea_30; }
	inline void set_loopsDescriptionArea_30(String_t* value)
	{
		___loopsDescriptionArea_30 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_30, value);
	}

	inline static int32_t get_offset_of_loops_31() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___loops_31)); }
	inline FsmInt_t1273009179 * get_loops_31() const { return ___loops_31; }
	inline FsmInt_t1273009179 ** get_address_of_loops_31() { return &___loops_31; }
	inline void set_loops_31(FsmInt_t1273009179 * value)
	{
		___loops_31 = value;
		Il2CppCodeGenWriteBarrier(&___loops_31, value);
	}

	inline static int32_t get_offset_of_loopType_32() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___loopType_32)); }
	inline int32_t get_loopType_32() const { return ___loopType_32; }
	inline int32_t* get_address_of_loopType_32() { return &___loopType_32; }
	inline void set_loopType_32(int32_t value)
	{
		___loopType_32 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_33() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___autoKillOnCompletion_33)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_33() const { return ___autoKillOnCompletion_33; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_33() { return &___autoKillOnCompletion_33; }
	inline void set_autoKillOnCompletion_33(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_33 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_33, value);
	}

	inline static int32_t get_offset_of_recyclable_34() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___recyclable_34)); }
	inline FsmBool_t664485696 * get_recyclable_34() const { return ___recyclable_34; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_34() { return &___recyclable_34; }
	inline void set_recyclable_34(FsmBool_t664485696 * value)
	{
		___recyclable_34 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_34, value);
	}

	inline static int32_t get_offset_of_updateType_35() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___updateType_35)); }
	inline int32_t get_updateType_35() const { return ___updateType_35; }
	inline int32_t* get_address_of_updateType_35() { return &___updateType_35; }
	inline void set_updateType_35(int32_t value)
	{
		___updateType_35 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_36() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___isIndependentUpdate_36)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_36() const { return ___isIndependentUpdate_36; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_36() { return &___isIndependentUpdate_36; }
	inline void set_isIndependentUpdate_36(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_36 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_36, value);
	}

	inline static int32_t get_offset_of_debugThis_37() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___debugThis_37)); }
	inline FsmBool_t664485696 * get_debugThis_37() const { return ___debugThis_37; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_37() { return &___debugThis_37; }
	inline void set_debugThis_37(FsmBool_t664485696 * value)
	{
		___debugThis_37 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_37, value);
	}

	inline static int32_t get_offset_of_tweener_38() { return static_cast<int32_t>(offsetof(DOTweenMaterialFloatProperty_t2842439620, ___tweener_38)); }
	inline Tweener_t760404022 * get_tweener_38() const { return ___tweener_38; }
	inline Tweener_t760404022 ** get_address_of_tweener_38() { return &___tweener_38; }
	inline void set_tweener_38(Tweener_t760404022 * value)
	{
		___tweener_38 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
