﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArraySort
struct ArraySort_t2283982609;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArraySort::.ctor()
extern "C"  void ArraySort__ctor_m3338096593 (ArraySort_t2283982609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySort::Reset()
extern "C"  void ArraySort_Reset_m3009711406 (ArraySort_t2283982609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArraySort::OnEnter()
extern "C"  void ArraySort_OnEnter_m1843651900 (ArraySort_t2283982609 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
