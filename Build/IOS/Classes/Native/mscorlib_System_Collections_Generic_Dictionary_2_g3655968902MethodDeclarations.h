﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3655968902;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.IDictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct IDictionary_2_t1655052323;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.ICollection`1<UnityEngine.RaycastHit2D>
struct ICollection_1_t721016783;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>[]
struct KeyValuePair_2U5BU5D_t3416804101;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.RaycastHit2D>>
struct IEnumerator_1_t3183805247;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.RaycastHit2D>
struct KeyCollection_t1844499377;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.RaycastHit2D>
struct ValueCollection_t2359028745;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21413314124.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En681026308.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void Dictionary_2__ctor_m496307294_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m496307294(__this, method) ((  void (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2__ctor_m496307294_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m103048033_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m103048033(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m103048033_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m316995296_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m316995296(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m316995296_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3854564803_gshared (Dictionary_2_t3655968902 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3854564803(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3655968902 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3854564803_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m867279155_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m867279155(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m867279155_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m364748794_gshared (Dictionary_2_t3655968902 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m364748794(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t3655968902 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m364748794_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1658843349_gshared (Dictionary_2_t3655968902 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1658843349(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3655968902 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1658843349_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2975360490_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2975360490(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2975360490_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m72218154_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m72218154(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m72218154_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m74578348_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m74578348(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m74578348_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2057635521_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2057635521(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2057635521_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m557254600_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m557254600(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m557254600_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3481615104_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3481615104(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3481615104_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1846050957_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1846050957(__this, ___key0, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1846050957_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4063020286_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4063020286(__this, method) ((  bool (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4063020286_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1395971824_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1395971824(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1395971824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1215408108_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1215408108(__this, method) ((  bool (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1215408108_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m519721077_gshared (Dictionary_2_t3655968902 * __this, KeyValuePair_2_t1413314124  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m519721077(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3655968902 *, KeyValuePair_2_t1413314124 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m519721077_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m376339079_gshared (Dictionary_2_t3655968902 * __this, KeyValuePair_2_t1413314124  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m376339079(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3655968902 *, KeyValuePair_2_t1413314124 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m376339079_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2216615433_gshared (Dictionary_2_t3655968902 * __this, KeyValuePair_2U5BU5D_t3416804101* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2216615433(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3655968902 *, KeyValuePair_2U5BU5D_t3416804101*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2216615433_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m983276970_gshared (Dictionary_2_t3655968902 * __this, KeyValuePair_2_t1413314124  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m983276970(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3655968902 *, KeyValuePair_2_t1413314124 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m983276970_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m130727914_gshared (Dictionary_2_t3655968902 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m130727914(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m130727914_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2192922071_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2192922071(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2192922071_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4230958902_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4230958902(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4230958902_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m321501985_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m321501985(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m321501985_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3690460486_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3690460486(__this, method) ((  int32_t (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_get_Count_m3690460486_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Item(TKey)
extern "C"  RaycastHit2D_t4063908774  Dictionary_2_get_Item_m2299367061_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2299367061(__this, ___key0, method) ((  RaycastHit2D_t4063908774  (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m2299367061_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4156649328_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4156649328(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_set_Item_m4156649328_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2089280550_gshared (Dictionary_2_t3655968902 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2089280550(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3655968902 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2089280550_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3022650567_gshared (Dictionary_2_t3655968902 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3022650567(__this, ___size0, method) ((  void (*) (Dictionary_2_t3655968902 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3022650567_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m455074901_gshared (Dictionary_2_t3655968902 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m455074901(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m455074901_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1413314124  Dictionary_2_make_pair_m759320235_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m759320235(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1413314124  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_make_pair_m759320235_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m1100596403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1100596403(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_pick_key_m1100596403_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::pick_value(TKey,TValue)
extern "C"  RaycastHit2D_t4063908774  Dictionary_2_pick_value_m2722610771_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2722610771(__this /* static, unused */, ___key0, ___value1, method) ((  RaycastHit2D_t4063908774  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_pick_value_m2722610771_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3747917988_gshared (Dictionary_2_t3655968902 * __this, KeyValuePair_2U5BU5D_t3416804101* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3747917988(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3655968902 *, KeyValuePair_2U5BU5D_t3416804101*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3747917988_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Resize()
extern "C"  void Dictionary_2_Resize_m3077458784_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3077458784(__this, method) ((  void (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_Resize_m3077458784_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m538111031_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m538111031(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_Add_m538111031_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Clear()
extern "C"  void Dictionary_2_Clear_m1150826967_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1150826967(__this, method) ((  void (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_Clear_m1150826967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3845181613_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3845181613(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m3845181613_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m679542045_gshared (Dictionary_2_t3655968902 * __this, RaycastHit2D_t4063908774  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m679542045(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3655968902 *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_ContainsValue_m679542045_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2822544206_gshared (Dictionary_2_t3655968902 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2822544206(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3655968902 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m2822544206_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2672868176_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2672868176(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2672868176_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3828974543_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3828974543(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m3828974543_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3969863484_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, RaycastHit2D_t4063908774 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3969863484(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3655968902 *, Il2CppObject *, RaycastHit2D_t4063908774 *, const MethodInfo*))Dictionary_2_TryGetValue_m3969863484_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Keys()
extern "C"  KeyCollection_t1844499377 * Dictionary_2_get_Keys_m2438570997_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2438570997(__this, method) ((  KeyCollection_t1844499377 * (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_get_Keys_m2438570997_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::get_Values()
extern "C"  ValueCollection_t2359028745 * Dictionary_2_get_Values_m3325086197_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3325086197(__this, method) ((  ValueCollection_t2359028745 * (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_get_Values_m3325086197_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m590834102_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m590834102(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m590834102_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ToTValue(System.Object)
extern "C"  RaycastHit2D_t4063908774  Dictionary_2_ToTValue_m1208894902_gshared (Dictionary_2_t3655968902 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1208894902(__this, ___value0, method) ((  RaycastHit2D_t4063908774  (*) (Dictionary_2_t3655968902 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1208894902_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2666365700_gshared (Dictionary_2_t3655968902 * __this, KeyValuePair_2_t1413314124  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2666365700(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3655968902 *, KeyValuePair_2_t1413314124 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2666365700_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::GetEnumerator()
extern "C"  Enumerator_t681026308  Dictionary_2_GetEnumerator_m2954630611_gshared (Dictionary_2_t3655968902 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2954630611(__this, method) ((  Enumerator_t681026308  (*) (Dictionary_2_t3655968902 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2954630611_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m1803210436_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, RaycastHit2D_t4063908774  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1803210436(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, RaycastHit2D_t4063908774 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1803210436_gshared)(__this /* static, unused */, ___key0, ___value1, method)
