﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax
struct  RectTransformSetOffsetMax_t3074450205  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::offsetMax
	FsmVector2_t2430450063 * ___offsetMax_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::x
	FsmFloat_t937133978 * ___x_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::y
	FsmFloat_t937133978 * ___y_16;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::_rt
	RectTransform_t3349966182 * ____rt_17;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t3074450205, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_offsetMax_14() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t3074450205, ___offsetMax_14)); }
	inline FsmVector2_t2430450063 * get_offsetMax_14() const { return ___offsetMax_14; }
	inline FsmVector2_t2430450063 ** get_address_of_offsetMax_14() { return &___offsetMax_14; }
	inline void set_offsetMax_14(FsmVector2_t2430450063 * value)
	{
		___offsetMax_14 = value;
		Il2CppCodeGenWriteBarrier(&___offsetMax_14, value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t3074450205, ___x_15)); }
	inline FsmFloat_t937133978 * get_x_15() const { return ___x_15; }
	inline FsmFloat_t937133978 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmFloat_t937133978 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier(&___x_15, value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t3074450205, ___y_16)); }
	inline FsmFloat_t937133978 * get_y_16() const { return ___y_16; }
	inline FsmFloat_t937133978 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmFloat_t937133978 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier(&___y_16, value);
	}

	inline static int32_t get_offset_of__rt_17() { return static_cast<int32_t>(offsetof(RectTransformSetOffsetMax_t3074450205, ____rt_17)); }
	inline RectTransform_t3349966182 * get__rt_17() const { return ____rt_17; }
	inline RectTransform_t3349966182 ** get_address_of__rt_17() { return &____rt_17; }
	inline void set__rt_17(RectTransform_t3349966182 * value)
	{
		____rt_17 = value;
		Il2CppCodeGenWriteBarrier(&____rt_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
