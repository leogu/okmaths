﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixer3244290001.h"
#include "mscorlib_System_String2029220233.h"

// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions50::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions50_DOSetFloat_m3076787227 (Il2CppObject * __this /* static, unused */, AudioMixer_t3244290001 * ___target0, String_t* ___floatName1, float ___endValue2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
