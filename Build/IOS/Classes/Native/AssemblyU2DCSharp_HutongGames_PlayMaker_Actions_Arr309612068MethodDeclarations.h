﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetMaxValue
struct ArrayListGetMaxValue_t309612068;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::.ctor()
extern "C"  void ArrayListGetMaxValue__ctor_m3074519524 (ArrayListGetMaxValue_t309612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::.cctor()
extern "C"  void ArrayListGetMaxValue__cctor_m3248168883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::Reset()
extern "C"  void ArrayListGetMaxValue_Reset_m2275357165 (ArrayListGetMaxValue_t309612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::OnEnter()
extern "C"  void ArrayListGetMaxValue_OnEnter_m3955182877 (ArrayListGetMaxValue_t309612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::OnUpdate()
extern "C"  void ArrayListGetMaxValue_OnUpdate_m20621914 (ArrayListGetMaxValue_t309612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::DoFindMaximumValue()
extern "C"  void ArrayListGetMaxValue_DoFindMaximumValue_m4283254505 (ArrayListGetMaxValue_t309612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.ArrayListGetMaxValue::ErrorCheck()
extern "C"  String_t* ArrayListGetMaxValue_ErrorCheck_m2688717431 (ArrayListGetMaxValue_t309612068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
