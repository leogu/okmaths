﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t591124924;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass23_0__ctor_m939556505 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
