﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionExit
struct PlayMakerCollisionExit_t3953531962;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void PlayMakerCollisionExit::OnCollisionExit(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionExit_OnCollisionExit_m342138039 (PlayMakerCollisionExit_t3953531962 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionExit::.ctor()
extern "C"  void PlayMakerCollisionExit__ctor_m3708387017 (PlayMakerCollisionExit_t3953531962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
