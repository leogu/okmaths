﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetKey
struct GetKey_t3369588153;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetKey::.ctor()
extern "C"  void GetKey__ctor_m1626610643 (GetKey_t3369588153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::Reset()
extern "C"  void GetKey_Reset_m1807059210 (GetKey_t3369588153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::OnEnter()
extern "C"  void GetKey_OnEnter_m2759156084 (GetKey_t3369588153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::OnUpdate()
extern "C"  void GetKey_OnUpdate_m4047315907 (GetKey_t3369588153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetKey::DoGetKey()
extern "C"  void GetKey_DoGetKey_m3993824897 (GetKey_t3369588153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
