﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue
struct uGuiScrollbarSetValue_t2999105249;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::.ctor()
extern "C"  void uGuiScrollbarSetValue__ctor_m1122488637 (uGuiScrollbarSetValue_t2999105249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::Reset()
extern "C"  void uGuiScrollbarSetValue_Reset_m3364527546 (uGuiScrollbarSetValue_t2999105249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::OnEnter()
extern "C"  void uGuiScrollbarSetValue_OnEnter_m29340672 (uGuiScrollbarSetValue_t2999105249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::OnUpdate()
extern "C"  void uGuiScrollbarSetValue_OnUpdate_m221592961 (uGuiScrollbarSetValue_t2999105249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::DoSetValue()
extern "C"  void uGuiScrollbarSetValue_DoSetValue_m3017571615 (uGuiScrollbarSetValue_t2999105249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetValue::OnExit()
extern "C"  void uGuiScrollbarSetValue_OnExit_m4125811692 (uGuiScrollbarSetValue_t2999105249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
