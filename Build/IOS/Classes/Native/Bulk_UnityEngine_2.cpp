﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.Logger
struct Logger_t3328995178;
// UnityEngine.ILogHandler
struct ILogHandler_t264057413;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Object
struct Object_t1021602117;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Exception
struct Exception_t1927440687;
// UnityEngine.HostData[]
struct HostDataU5BU5D_t1011608759;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Array
struct Il2CppArray;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Motion
struct Motion_t2415020824;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t2171372499;
// UnityEngine.NetworkPlayer[]
struct NetworkPlayerU5BU5D_t2821705394;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266;
// UnityEngine.Networking.DownloadHandlerAssetBundle
struct DownloadHandlerAssetBundle_t1224150142;
// UnityEngine.Networking.DownloadHandlerAudioClip
struct DownloadHandlerAudioClip_t1520641178;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558;
// UnityEngine.Networking.DownloadHandlerTexture
struct DownloadHandlerTexture_t2365358851;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t3552561393;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>
struct List_1_t603865270;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t3420491431;
// UnityEngine.NetworkView
struct NetworkView_t172525251;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// System.Type
struct Type_t;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t462843629;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t3229544204;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t2606999759;
// UnityEngine.RangeAttribute
struct RangeAttribute_t3336560921;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.RequireComponent
struct RequireComponent_t864575032;
// UnityEngine.ResourceRequest
struct ResourceRequest_t2560315377;
// UnityEngine.RPC
struct RPC_t3323229423;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_t3126475234;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t665107673;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_t4182602970;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t1913052472;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t3212052468;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t936505999;
// UnityEngine.GUILayer
struct GUILayer_t3254902478;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t3673080018;
// UnityEngine.SerializeField
struct SerializeField_t3073427462;
// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t2241034664;
// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1565472209;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus2482073234.h"
#include "UnityEngine_UnityEngine_LocationServiceStatus2482073234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "UnityEngine_UnityEngine_Logger3328995178MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Exception1927440687.h"
#include "UnityEngine_UnityEngine_LogType1559732862MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MasterServer990672255.h"
#include "UnityEngine_UnityEngine_MasterServer990672255MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_HostData3480691970.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2097711603.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2097711603MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask296470556.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask296470556MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Shader2430389951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973.h"
#include "UnityEngine_UnityEngineInternal_MathfInternal715669973MethodDeclarations.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107MethodDeclarations.h"
#include "mscorlib_System_IndexOutOfRangeException3527622107.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_UnityString276356480MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_ArgumentException3259014390MethodDeclarations.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828713.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "UnityEngine_UnityEngine_Mesh_InternalShaderChannel3331827198MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh_InternalVertexChannel2178520045MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104.h"
#include "UnityEngine_UnityEngine_MeshRenderer1268241104MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion2415020824.h"
#include "UnityEngine_UnityEngine_Motion2415020824MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent2171372499.h"
#include "UnityEngine_UnityEngine_NavMeshAgent2171372499MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Network2142671112.h"
#include "UnityEngine_UnityEngine_Network2142671112MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1607866742.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel1480423150.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPeerType2458303118.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1607866742MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection45590380.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection45590380MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1216180266MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Hash1282836532937.h"
#include "UnityEngine_UnityEngine_AudioType4076847944.h"
#include "mscorlib_System_GC2902933594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1224150142.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1224150142MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Hash1282836532937MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1520641178.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler1520641178MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler3443159558.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler3443159558MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2365358851.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2365358851MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest254341728.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest254341728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler3552561393.h"
#include "System_System_Text_RegularExpressions_Regex1803876613MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Regex1803876613.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandlerRa3420491431MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandlerRa3420491431.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "UnityEngine_UnityEngine_WWWTranscoder1124214756MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UploadHandler3552561393MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929.h"
#include "UnityEngine_UnityEngine_WWWForm3950226929MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen603865270.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen603865270MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat138594944.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3052225568.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat138594944MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517MethodDeclarations.h"
#include "System_System_Uri19570940MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest2654069489.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_FormatException2948921286.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_StringComparer1574862926MethodDeclarations.h"
#include "mscorlib_System_StringComparer1574862926.h"
#include "mscorlib_System_Char3454481338.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest3177762630.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest3177762630MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_UnityWebRequest2654069489MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkLogLevel1480423150MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkView172525251.h"
#include "UnityEngine_UnityEngine_NetworkView172525251MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkPeerType2458303118MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_Physics634932869MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Ray2469606224MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4166282325MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4166282325.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D851691520.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D851691520MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane3727654732.h"
#include "UnityEngine_UnityEngine_Plane3727654732MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3229544204MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException3229544204.h"
#include "mscorlib_System_Exception1927440687MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ProceduralMaterial2622875796.h"
#include "UnityEngine_UnityEngine_ProceduralMaterial2622875796MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "UnityEngine_UnityEngine_PropertyAttribute2606999759MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random1170710517.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921.h"
#include "UnityEngine_UnityEngine_RangeAttribute3336560921MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Collider2D646061738MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive2020713228MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge3306019089.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge3306019089MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask926634530.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask926634530MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction457874581.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction457874581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp2936374925.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp2936374925MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535.h"
#include "UnityEngine_UnityEngine_RenderSettings1057812535MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resolution3693662728.h"
#include "UnityEngine_UnityEngine_Resolution3693662728MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377.h"
#include "UnityEngine_UnityEngine_ResourceRequest2560315377MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources339470017.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D1256536801.h"
#include "UnityEngine_UnityEngine_ForceMode2D4177575466.h"
#include "UnityEngine_UnityEngine_RigidbodyConstraints2D1256536801MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RPC3323229423.h"
#include "UnityEngine_UnityEngine_RPC3323229423MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType205334256.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType205334256MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeOnLoadMet3126475234.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeOnLoadMet3126475234MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1903595547.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen3051495417.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen606618774.h"
#include "UnityEngine_UnityEngine_Screen786852042.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut4182602970.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative1913052472MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode3212052468MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute936505999MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032.h"
#include "UnityEngine_UnityEngine_SendMouseEvents3505065032MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayer3254902478.h"
#include "UnityEngine_UnityEngine_GUIElement3381083099.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo1761367055MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri3673080018MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462.h"
#include "UnityEngine_UnityEngine_SerializeField3073427462MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664.h"
#include "UnityEngine_UnityEngine_SerializePrivateVariables2241034664MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563.h"
#include "UnityEngine_UnityEngine_SetupCoroutine3582942563MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"
#include "mscorlib_System_Reflection_Binder3404612058.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr1565472209MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847MethodDeclarations.h"

// System.Int32 UnityEngine.Mesh::SafeLength<System.Int32>(System.Collections.Generic.List`1<!!0>)
extern "C"  int32_t Mesh_SafeLength_TisInt32_t2071877448_m2161861392_gshared (Mesh_t1356156583 * __this, List_1_t1440998580 * p0, const MethodInfo* method);
#define Mesh_SafeLength_TisInt32_t2071877448_m2161861392(__this, p0, method) ((  int32_t (*) (Mesh_t1356156583 *, List_1_t1440998580 *, const MethodInfo*))Mesh_SafeLength_TisInt32_t2071877448_m2161861392_gshared)(__this, p0, method)
// !!0[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector3U5BU5D_t1172311765* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417_gshared (Mesh_t1356156583 * __this, int32_t p0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417(__this, p0, method) ((  Vector3U5BU5D_t1172311765* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417_gshared)(__this, p0, method)
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,!!0[])
extern "C"  void Mesh_SetArrayForChannel_TisVector3_t2243707580_m2048180582_gshared (Mesh_t1356156583 * __this, int32_t p0, Vector3U5BU5D_t1172311765* p1, const MethodInfo* method);
#define Mesh_SetArrayForChannel_TisVector3_t2243707580_m2048180582(__this, p0, p1, method) ((  void (*) (Mesh_t1356156583 *, int32_t, Vector3U5BU5D_t1172311765*, const MethodInfo*))Mesh_SetArrayForChannel_TisVector3_t2243707580_m2048180582_gshared)(__this, p0, p1, method)
// !!0[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector4U5BU5D_t1658499504* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m1116766164_gshared (Mesh_t1356156583 * __this, int32_t p0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m1116766164(__this, p0, method) ((  Vector4U5BU5D_t1658499504* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m1116766164_gshared)(__this, p0, method)
// !!0[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Vector2>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  Vector2U5BU5D_t686124026* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534_gshared (Mesh_t1356156583 * __this, int32_t p0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534(__this, p0, method) ((  Vector2U5BU5D_t686124026* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534_gshared)(__this, p0, method)
// !!0[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color>(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  ColorU5BU5D_t672350442* Mesh_GetAllocArrayFromChannel_TisColor_t2020392075_m1713004566_gshared (Mesh_t1356156583 * __this, int32_t p0, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisColor_t2020392075_m1713004566(__this, p0, method) ((  ColorU5BU5D_t672350442* (*) (Mesh_t1356156583 *, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisColor_t2020392075_m1713004566_gshared)(__this, p0, method)
// System.Void UnityEngine.Mesh::SetArrayForChannel<UnityEngine.Color>(UnityEngine.Mesh/InternalShaderChannel,!!0[])
extern "C"  void Mesh_SetArrayForChannel_TisColor_t2020392075_m3773097225_gshared (Mesh_t1356156583 * __this, int32_t p0, ColorU5BU5D_t672350442* p1, const MethodInfo* method);
#define Mesh_SetArrayForChannel_TisColor_t2020392075_m3773097225(__this, p0, p1, method) ((  void (*) (Mesh_t1356156583 *, int32_t, ColorU5BU5D_t672350442*, const MethodInfo*))Mesh_SetArrayForChannel_TisColor_t2020392075_m3773097225_gshared)(__this, p0, p1, method)
// !!0[] UnityEngine.Mesh::GetAllocArrayFromChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Color32U5BU5D_t30278651* Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m992949857_gshared (Mesh_t1356156583 * __this, int32_t p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m992949857(__this, p0, p1, p2, method) ((  Color32U5BU5D_t30278651* (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, const MethodInfo*))Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m992949857_gshared)(__this, p0, p1, p2, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector3>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<!!0>)
extern "C"  void Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609_gshared (Mesh_t1356156583 * __this, int32_t p0, List_1_t1612828712 * p1, const MethodInfo* method);
#define Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609(__this, p0, p1, method) ((  void (*) (Mesh_t1356156583 *, int32_t, List_1_t1612828712 *, const MethodInfo*))Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Vector4>(UnityEngine.Mesh/InternalShaderChannel,System.Collections.Generic.List`1<!!0>)
extern "C"  void Mesh_SetListForChannel_TisVector4_t2243707581_m2402795402_gshared (Mesh_t1356156583 * __this, int32_t p0, List_1_t1612828713 * p1, const MethodInfo* method);
#define Mesh_SetListForChannel_TisVector4_t2243707581_m2402795402(__this, p0, p1, method) ((  void (*) (Mesh_t1356156583 *, int32_t, List_1_t1612828713 *, const MethodInfo*))Mesh_SetListForChannel_TisVector4_t2243707581_m2402795402_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Mesh::SetListForChannel<UnityEngine.Color32>(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Collections.Generic.List`1<!!0>)
extern "C"  void Mesh_SetListForChannel_TisColor32_t874517518_m3239432057_gshared (Mesh_t1356156583 * __this, int32_t p0, int32_t p1, int32_t p2, List_1_t243638650 * p3, const MethodInfo* method);
#define Mesh_SetListForChannel_TisColor32_t874517518_m3239432057(__this, p0, p1, p2, p3, method) ((  void (*) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, List_1_t243638650 *, const MethodInfo*))Mesh_SetListForChannel_TisColor32_t874517518_m3239432057_gshared)(__this, p0, p1, p2, p3, method)
// System.Void UnityEngine.Mesh::SetUvsImpl<UnityEngine.Vector2>(System.Int32,System.Int32,System.Collections.Generic.List`1<!!0>)
extern "C"  void Mesh_SetUvsImpl_TisVector2_t2243707579_m1129533604_gshared (Mesh_t1356156583 * __this, int32_t p0, int32_t p1, List_1_t1612828711 * p2, const MethodInfo* method);
#define Mesh_SetUvsImpl_TisVector2_t2243707579_m1129533604(__this, p0, p1, p2, method) ((  void (*) (Mesh_t1356156583 *, int32_t, int32_t, List_1_t1612828711 *, const MethodInfo*))Mesh_SetUvsImpl_TisVector2_t2243707579_m1129533604_gshared)(__this, p0, p1, p2, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUILayer>()
#define Component_GetComponent_TisGUILayer_t3254902478_m2431610297(__this, method) ((  GUILayer_t3254902478 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m3834134587 (Logger_t3328995178 * __this, Il2CppObject * ___logHandler0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___logHandler0;
		Logger_set_logHandler_m2851576632(__this, L_0, /*hidden argument*/NULL);
		Logger_set_logEnabled_m3852234466(__this, (bool)1, /*hidden argument*/NULL);
		Logger_set_filterLogType_m1452353615(__this, 3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern "C"  Il2CppObject * Logger_get_logHandler_m4190583509 (Logger_t3328995178 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3ClogHandlerU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern "C"  void Logger_set_logHandler_m2851576632 (Logger_t3328995178 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3ClogHandlerU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::get_logEnabled()
extern "C"  bool Logger_get_logEnabled_m3807759477 (Logger_t3328995178 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3ClogEnabledU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern "C"  void Logger_set_logEnabled_m3852234466 (Logger_t3328995178 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3ClogEnabledU3Ek__BackingField_1(L_0);
		return;
	}
}
// UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern "C"  int32_t Logger_get_filterLogType_m3672438698 (Logger_t3328995178 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_U3CfilterLogTypeU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern "C"  void Logger_set_filterLogType_m1452353615 (Logger_t3328995178 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CfilterLogTypeU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern "C"  bool Logger_IsLogTypeAllowed_m1750132386 (Logger_t3328995178 * __this, int32_t ___logType0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		bool L_0 = Logger_get_logEnabled_m3807759477(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1 = ___logType0;
		int32_t L_2 = Logger_get_filterLogType_m3672438698(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_3 = ___logType0;
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)4))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 1;
	}

IL_001e:
	{
		return (bool)G_B4_0;
	}

IL_001f:
	{
		return (bool)0;
	}
}
// System.String UnityEngine.Logger::GetString(System.Object)
extern Il2CppCodeGenString* _stringLiteral1743625299;
extern const uint32_t Logger_GetString_m4086587133_MetadataUsageId;
extern "C"  String_t* Logger_GetString_m4086587133 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_GetString_m4086587133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B3_0 = NULL;
	{
		Il2CppObject * L_0 = ___message0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		goto IL_0016;
	}

IL_0011:
	{
		G_B3_0 = _stringLiteral1743625299;
	}

IL_0016:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104529068;
extern const uint32_t Logger_Log_m3587255568_MetadataUsageId;
extern "C"  void Logger_Log_m3587255568 (Logger_t3328995178 * __this, int32_t ___logType0, Il2CppObject * ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m3587255568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1750132386(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		ObjectU5BU5D_t3614634134* L_4 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_5 = ___message1;
		String_t* L_6 = Logger_GetString_m4086587133(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_6);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1021602117 *, String_t*, ObjectU5BU5D_t3614634134* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_2, L_3, (Object_t1021602117 *)NULL, _stringLiteral104529068, L_4);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104529068;
extern const uint32_t Logger_Log_m4012064130_MetadataUsageId;
extern "C"  void Logger_Log_m4012064130 (Logger_t3328995178 * __this, int32_t ___logType0, Il2CppObject * ___message1, Object_t1021602117 * ___context2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_Log_m4012064130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1750132386(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t1021602117 * L_4 = ___context2;
		ObjectU5BU5D_t3614634134* L_5 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_6 = ___message1;
		String_t* L_7 = Logger_GetString_m4086587133(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_7);
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1021602117 *, String_t*, ObjectU5BU5D_t3614634134* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_2, L_3, L_4, _stringLiteral104529068, L_5);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Logger_LogFormat_m193464629_MetadataUsageId;
extern "C"  void Logger_LogFormat_m193464629 (Logger_t3328995178 * __this, int32_t ___logType0, Object_t1021602117 * ___context1, String_t* ___format2, ObjectU5BU5D_t3614634134* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogFormat_m193464629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		bool L_1 = Logger_IsLogTypeAllowed_m1750132386(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Il2CppObject * L_2 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		int32_t L_3 = ___logType0;
		Object_t1021602117 * L_4 = ___context1;
		String_t* L_5 = ___format2;
		ObjectU5BU5D_t3614634134* L_6 = ___args3;
		NullCheck(L_2);
		InterfaceActionInvoker4< int32_t, Object_t1021602117 *, String_t*, ObjectU5BU5D_t3614634134* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Logger_LogException_m206035446_MetadataUsageId;
extern "C"  void Logger_LogException_m206035446 (Logger_t3328995178 * __this, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Logger_LogException_m206035446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Logger_get_logEnabled_m3807759477(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = Logger_get_logHandler_m4190583509(__this, /*hidden argument*/NULL);
		Exception_t1927440687 * L_2 = ___exception0;
		Object_t1021602117 * L_3 = ___context1;
		NullCheck(L_1);
		InterfaceActionInvoker2< Exception_t1927440687 *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_1, L_2, L_3);
	}

IL_0018:
	{
		return;
	}
}
// System.String UnityEngine.MasterServer::get_ipAddress()
extern "C"  String_t* MasterServer_get_ipAddress_m1552175907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*MasterServer_get_ipAddress_m1552175907_ftn) ();
	static MasterServer_get_ipAddress_m1552175907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_ipAddress_m1552175907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_ipAddress()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_ipAddress(System.String)
extern "C"  void MasterServer_set_ipAddress_m518203496 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_ipAddress_m518203496_ftn) (String_t*);
	static MasterServer_set_ipAddress_m518203496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_ipAddress_m518203496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_ipAddress(System.String)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.MasterServer::get_port()
extern "C"  int32_t MasterServer_get_port_m3416376690 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*MasterServer_get_port_m3416376690_ftn) ();
	static MasterServer_get_port_m3416376690_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_port_m3416376690_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_port()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_port(System.Int32)
extern "C"  void MasterServer_set_port_m3293017953 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_port_m3293017953_ftn) (int32_t);
	static MasterServer_set_port_m3293017953_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_port_m3293017953_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_port(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.MasterServer::RequestHostList(System.String)
extern "C"  void MasterServer_RequestHostList_m660473625 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, const MethodInfo* method)
{
	typedef void (*MasterServer_RequestHostList_m660473625_ftn) (String_t*);
	static MasterServer_RequestHostList_m660473625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_RequestHostList_m660473625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::RequestHostList(System.String)");
	_il2cpp_icall_func(___gameTypeName0);
}
// UnityEngine.HostData[] UnityEngine.MasterServer::PollHostList()
extern "C"  HostDataU5BU5D_t1011608759* MasterServer_PollHostList_m2376545823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef HostDataU5BU5D_t1011608759* (*MasterServer_PollHostList_m2376545823_ftn) ();
	static MasterServer_PollHostList_m2376545823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_PollHostList_m2376545823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::PollHostList()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::RegisterHost(System.String,System.String,System.String)
extern "C"  void MasterServer_RegisterHost_m2321457831 (Il2CppObject * __this /* static, unused */, String_t* ___gameTypeName0, String_t* ___gameName1, String_t* ___comment2, const MethodInfo* method)
{
	typedef void (*MasterServer_RegisterHost_m2321457831_ftn) (String_t*, String_t*, String_t*);
	static MasterServer_RegisterHost_m2321457831_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_RegisterHost_m2321457831_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::RegisterHost(System.String,System.String,System.String)");
	_il2cpp_icall_func(___gameTypeName0, ___gameName1, ___comment2);
}
// System.Void UnityEngine.MasterServer::UnregisterHost()
extern "C"  void MasterServer_UnregisterHost_m4253193212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*MasterServer_UnregisterHost_m4253193212_ftn) ();
	static MasterServer_UnregisterHost_m4253193212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_UnregisterHost_m4253193212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::UnregisterHost()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::ClearHostList()
extern "C"  void MasterServer_ClearHostList_m1356680311 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*MasterServer_ClearHostList_m1356680311_ftn) ();
	static MasterServer_ClearHostList_m1356680311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_ClearHostList_m1356680311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::ClearHostList()");
	_il2cpp_icall_func();
}
// System.Int32 UnityEngine.MasterServer::get_updateRate()
extern "C"  int32_t MasterServer_get_updateRate_m2849397026 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*MasterServer_get_updateRate_m2849397026_ftn) ();
	static MasterServer_get_updateRate_m2849397026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_updateRate_m2849397026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_updateRate()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_updateRate(System.Int32)
extern "C"  void MasterServer_set_updateRate_m3353854091 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_updateRate_m3353854091_ftn) (int32_t);
	static MasterServer_set_updateRate_m3353854091_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_updateRate_m3353854091_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_updateRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.MasterServer::get_dedicatedServer()
extern "C"  bool MasterServer_get_dedicatedServer_m1786223459 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*MasterServer_get_dedicatedServer_m1786223459_ftn) ();
	static MasterServer_get_dedicatedServer_m1786223459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_get_dedicatedServer_m1786223459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::get_dedicatedServer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.MasterServer::set_dedicatedServer(System.Boolean)
extern "C"  void MasterServer_set_dedicatedServer_m3331804062 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*MasterServer_set_dedicatedServer_m3331804062_ftn) (bool);
	static MasterServer_set_dedicatedServer_m3331804062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MasterServer_set_dedicatedServer_m3331804062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MasterServer::set_dedicatedServer(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.MatchTargetWeightMask::.ctor(UnityEngine.Vector3,System.Single)
extern "C"  void MatchTargetWeightMask__ctor_m3489090191 (MatchTargetWeightMask_t296470556 * __this, Vector3_t2243707580  ___positionXYZWeight0, float ___rotationWeight1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___positionXYZWeight0;
		__this->set_m_PositionXYZWeight_0(L_0);
		float L_1 = ___rotationWeight1;
		__this->set_m_RotationWeight_1(L_1);
		return;
	}
}
extern "C"  void MatchTargetWeightMask__ctor_m3489090191_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___positionXYZWeight0, float ___rotationWeight1, const MethodInfo* method)
{
	MatchTargetWeightMask_t296470556 * _thisAdjusted = reinterpret_cast<MatchTargetWeightMask_t296470556 *>(__this + 1);
	MatchTargetWeightMask__ctor_m3489090191(_thisAdjusted, ___positionXYZWeight0, ___rotationWeight1, method);
}
// Conversion methods for marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t296470556_marshal_pinvoke(const MatchTargetWeightMask_t296470556& unmarshaled, MatchTargetWeightMask_t296470556_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_PositionXYZWeight_0(), marshaled.___m_PositionXYZWeight_0);
	marshaled.___m_RotationWeight_1 = unmarshaled.get_m_RotationWeight_1();
}
extern "C" void MatchTargetWeightMask_t296470556_marshal_pinvoke_back(const MatchTargetWeightMask_t296470556_marshaled_pinvoke& marshaled, MatchTargetWeightMask_t296470556& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_PositionXYZWeight_temp_0;
	memset(&unmarshaled_m_PositionXYZWeight_temp_0, 0, sizeof(unmarshaled_m_PositionXYZWeight_temp_0));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_PositionXYZWeight_0, unmarshaled_m_PositionXYZWeight_temp_0);
	unmarshaled.set_m_PositionXYZWeight_0(unmarshaled_m_PositionXYZWeight_temp_0);
	float unmarshaled_m_RotationWeight_temp_1 = 0.0f;
	unmarshaled_m_RotationWeight_temp_1 = marshaled.___m_RotationWeight_1;
	unmarshaled.set_m_RotationWeight_1(unmarshaled_m_RotationWeight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t296470556_marshal_pinvoke_cleanup(MatchTargetWeightMask_t296470556_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_PositionXYZWeight_0);
}
// Conversion methods for marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t296470556_marshal_com(const MatchTargetWeightMask_t296470556& unmarshaled, MatchTargetWeightMask_t296470556_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_PositionXYZWeight_0(), marshaled.___m_PositionXYZWeight_0);
	marshaled.___m_RotationWeight_1 = unmarshaled.get_m_RotationWeight_1();
}
extern "C" void MatchTargetWeightMask_t296470556_marshal_com_back(const MatchTargetWeightMask_t296470556_marshaled_com& marshaled, MatchTargetWeightMask_t296470556& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_PositionXYZWeight_temp_0;
	memset(&unmarshaled_m_PositionXYZWeight_temp_0, 0, sizeof(unmarshaled_m_PositionXYZWeight_temp_0));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_PositionXYZWeight_0, unmarshaled_m_PositionXYZWeight_temp_0);
	unmarshaled.set_m_PositionXYZWeight_0(unmarshaled_m_PositionXYZWeight_temp_0);
	float unmarshaled_m_RotationWeight_temp_1 = 0.0f;
	unmarshaled_m_RotationWeight_temp_1 = marshaled.___m_RotationWeight_1;
	unmarshaled.set_m_RotationWeight_1(unmarshaled_m_RotationWeight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.MatchTargetWeightMask
extern "C" void MatchTargetWeightMask_t296470556_marshal_com_cleanup(MatchTargetWeightMask_t296470556_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_PositionXYZWeight_0);
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Material__ctor_m1440882780_MetadataUsageId;
extern "C"  void Material__ctor_m1440882780 (Material_t193706927 * __this, Material_t193706927 * ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material__ctor_m1440882780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		Material_t193706927 * L_0 = ___source0;
		Material_Internal_CreateWithMaterial_m2907597451(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.Material::get_color()
extern Il2CppCodeGenString* _stringLiteral895546098;
extern const uint32_t Material_get_color_m668215843_MetadataUsageId;
extern "C"  Color_t2020392075  Material_get_color_m668215843 (Material_t193706927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_get_color_m668215843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t2020392075  L_0 = Material_GetColor_m2326771174(__this, _stringLiteral895546098, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern Il2CppCodeGenString* _stringLiteral895546098;
extern const uint32_t Material_set_color_m577844242_MetadataUsageId;
extern "C"  void Material_set_color_m577844242 (Material_t193706927 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_color_m577844242_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t2020392075  L_0 = ___value0;
		Material_SetColor_m650857509(__this, _stringLiteral895546098, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t Material_get_mainTexture_m432794412_MetadataUsageId;
extern "C"  Texture_t2243626319 * Material_get_mainTexture_m432794412 (Material_t193706927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_get_mainTexture_m432794412_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t2243626319 * L_0 = Material_GetTexture_m1257877102(__this, _stringLiteral4026354833, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Material::get_mainTextureOffset()
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t Material_get_mainTextureOffset_m786294629_MetadataUsageId;
extern "C"  Vector2_t2243707579  Material_get_mainTextureOffset_m786294629 (Material_t193706927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_get_mainTextureOffset_m786294629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2243707579  L_0 = Material_GetTextureOffset_m4148619067(__this, _stringLiteral4026354833, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_mainTextureOffset(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t Material_set_mainTextureOffset_m3533368774_MetadataUsageId;
extern "C"  void Material_set_mainTextureOffset_m3533368774 (Material_t193706927 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_mainTextureOffset_m3533368774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2243707579  L_0 = ___value0;
		Material_SetTextureOffset_m3084369360(__this, _stringLiteral4026354833, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Material::get_mainTextureScale()
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t Material_get_mainTextureScale_m2123078452_MetadataUsageId;
extern "C"  Vector2_t2243707579  Material_get_mainTextureScale_m2123078452 (Material_t193706927 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_get_mainTextureScale_m2123078452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2243707579  L_0 = Material_GetTextureScale_m1359469258(__this, _stringLiteral4026354833, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_mainTextureScale(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral4026354833;
extern const uint32_t Material_set_mainTextureScale_m723074403_MetadataUsageId;
extern "C"  void Material_set_mainTextureScale_m723074403 (Material_t193706927 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Material_set_mainTextureScale_m723074403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2243707579  L_0 = ___value0;
		Material_SetTextureScale_m1622979841(__this, _stringLiteral4026354833, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C"  void Material_SetColor_m650857509 (Material_t193706927 * __this, String_t* ___propertyName0, Color_t2020392075  ___color1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = ___color1;
		Material_SetColor_m1191533068(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C"  void Material_SetColor_m1191533068 (Material_t193706927 * __this, int32_t ___nameID0, Color_t2020392075  ___color1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_SetColor_m1364511001(NULL /*static, unused*/, __this, L_0, (&___color1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_SetColor_m1364511001 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___self0, int32_t ___nameID1, Color_t2020392075 * ___color2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColor_m1364511001_ftn) (Material_t193706927 *, int32_t, Color_t2020392075 *);
	static Material_INTERNAL_CALL_SetColor_m1364511001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColor_m1364511001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___color2);
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern "C"  Color_t2020392075  Material_GetColor_m2326771174 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = Material_GetColor_m2968960621(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern "C"  Color_t2020392075  Material_GetColor_m2968960621 (Material_t193706927 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___nameID0;
		Material_INTERNAL_CALL_GetColor_m558759949(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_GetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C"  void Material_INTERNAL_CALL_GetColor_m558759949 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___self0, int32_t ___nameID1, Color_t2020392075 * ___value2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_GetColor_m558759949_ftn) (Material_t193706927 *, int32_t, Color_t2020392075 *);
	static Material_INTERNAL_CALL_GetColor_m558759949_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_GetColor_m558759949_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_GetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___nameID1, ___value2);
}
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m3298399397 (Material_t193706927 * __this, String_t* ___propertyName0, Vector4_t2243707581  ___vector1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		float L_1 = (&___vector1)->get_x_1();
		float L_2 = (&___vector1)->get_y_2();
		float L_3 = (&___vector1)->get_z_3();
		float L_4 = (&___vector1)->get_w_4();
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m1909920690(&L_5, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		Material_SetColor_m650857509(__this, L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern "C"  Vector4_t2243707581  Material_GetVector_m4150288440 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___propertyName0;
		Color_t2020392075  L_1 = Material_GetColor_m2326771174(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_r_0();
		float L_3 = (&V_0)->get_g_1();
		float L_4 = (&V_0)->get_b_2();
		float L_5 = (&V_0)->get_a_3();
		Vector4_t2243707581  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector4__ctor_m1222289168(&L_6, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m141095205 (Material_t193706927 * __this, String_t* ___propertyName0, Texture_t2243626319 * ___texture1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2243626319 * L_2 = ___texture1;
		Material_SetTexture_m58378708(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m58378708 (Material_t193706927 * __this, int32_t ___nameID0, Texture_t2243626319 * ___texture1, const MethodInfo* method)
{
	typedef void (*Material_SetTexture_m58378708_ftn) (Material_t193706927 *, int32_t, Texture_t2243626319 *);
	static Material_SetTexture_m58378708_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTexture_m58378708_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID0, ___texture1);
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C"  Texture_t2243626319 * Material_GetTexture_m1257877102 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t2243626319 * L_2 = Material_GetTexture_m648312929(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C"  Texture_t2243626319 * Material_GetTexture_m648312929 (Material_t193706927 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef Texture_t2243626319 * (*Material_GetTexture_m648312929_ftn) (Material_t193706927 *, int32_t);
	static Material_GetTexture_m648312929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTexture_m648312929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTexture(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID0);
}
// System.Void UnityEngine.Material::Internal_GetTextureScaleAndOffset(UnityEngine.Material,System.String,UnityEngine.Vector4&)
extern "C"  void Material_Internal_GetTextureScaleAndOffset_m1384230327 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___mat0, String_t* ___name1, Vector4_t2243707581 * ___output2, const MethodInfo* method)
{
	typedef void (*Material_Internal_GetTextureScaleAndOffset_m1384230327_ftn) (Material_t193706927 *, String_t*, Vector4_t2243707581 *);
	static Material_Internal_GetTextureScaleAndOffset_m1384230327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_GetTextureScaleAndOffset_m1384230327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_GetTextureScaleAndOffset(UnityEngine.Material,System.String,UnityEngine.Vector4&)");
	_il2cpp_icall_func(___mat0, ___name1, ___output2);
}
// System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
extern "C"  void Material_SetTextureOffset_m3084369360 (Material_t193706927 * __this, String_t* ___propertyName0, Vector2_t2243707579  ___offset1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		Material_INTERNAL_CALL_SetTextureOffset_m4252917387(NULL /*static, unused*/, __this, L_0, (&___offset1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)
extern "C"  void Material_INTERNAL_CALL_SetTextureOffset_m4252917387 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___self0, String_t* ___propertyName1, Vector2_t2243707579 * ___offset2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetTextureOffset_m4252917387_ftn) (Material_t193706927 *, String_t*, Vector2_t2243707579 *);
	static Material_INTERNAL_CALL_SetTextureOffset_m4252917387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetTextureOffset_m4252917387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___propertyName1, ___offset2);
}
// UnityEngine.Vector2 UnityEngine.Material::GetTextureOffset(System.String)
extern "C"  Vector2_t2243707579  Material_GetTextureOffset_m4148619067 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___propertyName0;
		Material_Internal_GetTextureScaleAndOffset_m1384230327(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		float L_1 = (&V_0)->get_z_3();
		float L_2 = (&V_0)->get_w_4();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Material::SetTextureScale(System.String,UnityEngine.Vector2)
extern "C"  void Material_SetTextureScale_m1622979841 (Material_t193706927 * __this, String_t* ___propertyName0, Vector2_t2243707579  ___scale1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		Material_INTERNAL_CALL_SetTextureScale_m2049074868(NULL /*static, unused*/, __this, L_0, (&___scale1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)
extern "C"  void Material_INTERNAL_CALL_SetTextureScale_m2049074868 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___self0, String_t* ___propertyName1, Vector2_t2243707579 * ___scale2, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetTextureScale_m2049074868_ftn) (Material_t193706927 *, String_t*, Vector2_t2243707579 *);
	static Material_INTERNAL_CALL_SetTextureScale_m2049074868_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetTextureScale_m2049074868_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___propertyName1, ___scale2);
}
// UnityEngine.Vector2 UnityEngine.Material::GetTextureScale(System.String)
extern "C"  Vector2_t2243707579  Material_GetTextureScale_m1359469258 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = ___propertyName0;
		Material_Internal_GetTextureScaleAndOffset_m1384230327(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		float L_1 = (&V_0)->get_x_1();
		float L_2 = (&V_0)->get_y_2();
		Vector2_t2243707579  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3067419446(&L_3, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C"  void Material_SetFloat_m1926275467 (Material_t193706927 * __this, String_t* ___propertyName0, float ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value1;
		Material_SetFloat_m953675160(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern "C"  void Material_SetFloat_m953675160 (Material_t193706927 * __this, int32_t ___nameID0, float ___value1, const MethodInfo* method)
{
	typedef void (*Material_SetFloat_m953675160_ftn) (Material_t193706927 *, int32_t, float);
	static Material_SetFloat_m953675160_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetFloat_m953675160_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// System.Single UnityEngine.Material::GetFloat(System.String)
extern "C"  float Material_GetFloat_m562289878 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = Material_GetFloat_m4250722315(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Material::GetFloat(System.Int32)
extern "C"  float Material_GetFloat_m4250722315 (Material_t193706927 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef float (*Material_GetFloat_m4250722315_ftn) (Material_t193706927 *, int32_t);
	static Material_GetFloat_m4250722315_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetFloat_m4250722315_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetFloat(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID0);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C"  void Material_SetInt_m522302436 (Material_t193706927 * __this, String_t* ___propertyName0, int32_t ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = ___value1;
		Material_SetFloat_m1926275467(__this, L_0, (((float)((float)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C"  bool Material_HasProperty_m3511389613 (Material_t193706927 * __this, String_t* ___propertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName0;
		int32_t L_1 = Shader_PropertyToID_m678579425(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m3175512802(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C"  bool Material_HasProperty_m3175512802 (Material_t193706927 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef bool (*Material_HasProperty_m3175512802_ftn) (Material_t193706927 *, int32_t);
	static Material_HasProperty_m3175512802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m3175512802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID0);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C"  void Material_Internal_CreateWithMaterial_m2907597451 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___mono0, Material_t193706927 * ___source1, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m2907597451_ftn) (Material_t193706927 *, Material_t193706927 *);
	static Material_Internal_CreateWithMaterial_m2907597451_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m2907597451_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono0, ___source1);
}
// System.Void UnityEngine.Material::EnableKeyword(System.String)
extern "C"  void Material_EnableKeyword_m3724752646 (Material_t193706927 * __this, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Material_EnableKeyword_m3724752646_ftn) (Material_t193706927 *, String_t*);
	static Material_EnableKeyword_m3724752646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_EnableKeyword_m3724752646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::EnableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Material::DisableKeyword(System.String)
extern "C"  void Material_DisableKeyword_m1204728089 (Material_t193706927 * __this, String_t* ___keyword0, const MethodInfo* method)
{
	typedef void (*Material_DisableKeyword_m1204728089_ftn) (Material_t193706927 *, String_t*);
	static Material_DisableKeyword_m1204728089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_DisableKeyword_m1204728089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::DisableKeyword(System.String)");
	_il2cpp_icall_func(__this, ___keyword0);
}
// System.Void UnityEngine.Mathf::.cctor()
extern Il2CppClass* MathfInternal_t715669973_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf__cctor_m326403828_MetadataUsageId;
extern "C"  void Mathf__cctor_m326403828 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf__cctor_m326403828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t715669973_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->get_IsFlushToZeroEnabled_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t715669973_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->get_FloatMinNormal_0();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t715669973_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t715669973_StaticFields*)MathfInternal_t715669973_il2cpp_TypeInfo_var->static_fields)->get_FloatMinDenormal_1();
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t2336485820_StaticFields*)Mathf_t2336485820_il2cpp_TypeInfo_var->static_fields)->set_Epsilon_0(G_B3_0);
		return;
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C"  float Mathf_Sin_m831310046 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = sin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C"  float Mathf_Cos_m238853451 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = cos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Tan(System.Single)
extern "C"  float Mathf_Tan_m3348734175 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = tan((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Asin(System.Single)
extern "C"  float Mathf_Asin_m1367675601 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = asin((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Acos(System.Single)
extern "C"  float Mathf_Acos_m3800958570 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = acos((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Atan(System.Single)
extern "C"  float Mathf_Atan_m3769407270 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = atan((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern "C"  float Mathf_Atan2_m496071197 (Il2CppObject * __this /* static, unused */, float ___y0, float ___x1, const MethodInfo* method)
{
	{
		float L_0 = ___y0;
		float L_1 = ___x1;
		double L_2 = atan2((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C"  float Mathf_Sqrt_m2213915910 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = sqrt((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C"  float Mathf_Abs_m1942863256 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = fabsf(L_0);
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern "C"  int32_t Mathf_Abs_m1192949464 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = abs(L_0);
		return L_1;
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1648492575 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m2906823867 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m2564622569 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m1875893177 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a0;
		int32_t L_1 = ___b1;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a0;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b1;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C"  float Mathf_Pow_m3865188073 (Il2CppObject * __this /* static, unused */, float ___f0, float ___p1, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = pow((((double)((double)L_0))), (((double)((double)L_1))));
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C"  float Mathf_Log_m3878563109 (Il2CppObject * __this /* static, unused */, float ___f0, float ___p1, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		float L_1 = ___p1;
		double L_2 = Math_Log_m3325929366(NULL /*static, unused*/, (((double)((double)L_0))), (((double)((double)L_1))), /*hidden argument*/NULL);
		return (((float)((float)L_2)));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single)
extern "C"  float Mathf_Log_m2145566002 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = log((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Ceil(System.Single)
extern "C"  float Mathf_Ceil_m3621406599 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C"  float Mathf_Floor_m3472740614 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C"  float Mathf_Round_m227387340 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((float)((float)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C"  int32_t Mathf_CeilToInt_m2672598779 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = ceil((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m4005035722 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = floor((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m2927198556 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	{
		float L_0 = ___f0;
		double L_1 = bankers_round((((double)((double)L_0))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m2039143327 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f0;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2354025655 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min1;
		___value0 = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value0;
		float L_4 = ___max2;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max2;
		___value0 = L_5;
	}

IL_0019:
	{
		float L_6 = ___value0;
		return L_6;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m3542052159 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___min1, int32_t ___max2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___min1;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___min1;
		___value0 = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		int32_t L_3 = ___value0;
		int32_t L_4 = ___max2;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___max2;
		___value0 = L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___value0;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m3888954684 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value0;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value0;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Lerp_m1686556575_MetadataUsageId;
extern "C"  float Mathf_Lerp_m1686556575 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Lerp_m1686556575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		float L_2 = ___a0;
		float L_3 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::LerpAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_LerpAngle_m3501252860_MetadataUsageId;
extern "C"  float Mathf_LerpAngle_m3501252860 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_LerpAngle_m3501252860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = ___a0;
		float L_6 = V_0;
		float L_7 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return ((float)((float)L_5+(float)((float)((float)L_6*(float)L_8))));
	}
}
// System.Single UnityEngine.Mathf::SmoothStep(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothStep_m1375889224_MetadataUsageId;
extern "C"  float Mathf_SmoothStep_m1375889224 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothStep_m1375889224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = ___t2;
		float L_3 = ___t2;
		float L_4 = ___t2;
		float L_5 = ___t2;
		float L_6 = ___t2;
		___t2 = ((float)((float)((float)((float)((float)((float)((float)((float)(-2.0f)*(float)L_2))*(float)L_3))*(float)L_4))+(float)((float)((float)((float)((float)(3.0f)*(float)L_5))*(float)L_6))));
		float L_7 = ___to1;
		float L_8 = ___t2;
		float L_9 = ___from0;
		float L_10 = ___t2;
		return ((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)((float)((float)(1.0f)-(float)L_10))))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Approximately_m1064446634_MetadataUsageId;
extern "C"  bool Mathf_Approximately_m1064446634 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Approximately_m1064446634_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___b1;
		float L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a0;
		float L_4 = fabsf(L_3);
		float L_5 = ___b1;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t2336485820_StaticFields*)Mathf_t2336485820_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		float L_9 = Mathf_Max_m2564622569(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		return (bool)((((float)L_2) < ((float)L_9))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDamp_m1166236953_MetadataUsageId;
extern "C"  float Mathf_SmoothDamp_m1166236953 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDamp_m1166236953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current0;
		float L_2 = ___target1;
		float* L_3 = ___currentVelocity2;
		float L_4 = ___smoothTime3;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDamp_m1604773625(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDamp_m1604773625_MetadataUsageId;
extern "C"  float Mathf_SmoothDamp_m1604773625 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, float ___maxSpeed4, float ___deltaTime5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDamp_m1604773625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		float L_0 = ___smoothTime3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m2564622569(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime3 = L_1;
		float L_2 = ___smoothTime3;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime5;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current0;
		float L_12 = ___target1;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target1;
		V_4 = L_13;
		float L_14 = ___maxSpeed4;
		float L_15 = ___smoothTime3;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current0;
		float L_21 = V_3;
		___target1 = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity2;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime5;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity2;
		float* L_27 = ___currentVelocity2;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target1;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current0;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity2;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime5;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a0:
	{
		float L_44 = V_7;
		return L_44;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDampAngle_m935764888_MetadataUsageId;
extern "C"  float Mathf_SmoothDampAngle_m935764888 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDampAngle_m935764888_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current0;
		float L_2 = ___target1;
		float* L_3 = ___currentVelocity2;
		float L_4 = ___smoothTime3;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDampAngle_m461293304(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_SmoothDampAngle_m461293304_MetadataUsageId;
extern "C"  float Mathf_SmoothDampAngle_m461293304 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, float* ___currentVelocity2, float ___smoothTime3, float ___maxSpeed4, float ___deltaTime5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_SmoothDampAngle_m461293304_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___current0;
		float L_1 = ___current0;
		float L_2 = ___target1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_DeltaAngle_m1024687732(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		___target1 = ((float)((float)L_0+(float)L_3));
		float L_4 = ___current0;
		float L_5 = ___target1;
		float* L_6 = ___currentVelocity2;
		float L_7 = ___smoothTime3;
		float L_8 = ___maxSpeed4;
		float L_9 = ___deltaTime5;
		float L_10 = Mathf_SmoothDamp_m1604773625(NULL /*static, unused*/, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_Repeat_m943844734_MetadataUsageId;
extern "C"  float Mathf_Repeat_m943844734 (Il2CppObject * __this /* static, unused */, float ___t0, float ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_Repeat_m943844734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t0;
		float L_1 = ___t0;
		float L_2 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length1;
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_InverseLerp_m55890283_MetadataUsageId;
extern "C"  float Mathf_InverseLerp_m55890283 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, float ___value2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_InverseLerp_m55890283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		float L_2 = ___value2;
		float L_3 = ___a0;
		float L_4 = ___b1;
		float L_5 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)((float)((float)L_2-(float)L_3))/(float)((float)((float)L_4-(float)L_5)))), /*hidden argument*/NULL);
		return L_6;
	}

IL_0014:
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
extern "C"  float Mathf_GammaToLinearSpace_m3697142437 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef float (*Mathf_GammaToLinearSpace_m3697142437_ftn) (float);
	static Mathf_GammaToLinearSpace_m3697142437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mathf_GammaToLinearSpace_m3697142437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mathf::GammaToLinearSpace(System.Single)");
	return _il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.Mathf::LinearToGammaSpace(System.Single)
extern "C"  float Mathf_LinearToGammaSpace_m1586839609 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef float (*Mathf_LinearToGammaSpace_m1586839609_ftn) (float);
	static Mathf_LinearToGammaSpace_m1586839609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mathf_LinearToGammaSpace_m1586839609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mathf::LinearToGammaSpace(System.Single)");
	return _il2cpp_icall_func(___value0);
}
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Mathf_DeltaAngle_m1024687732_MetadataUsageId;
extern "C"  float Mathf_DeltaAngle_m1024687732 (Il2CppObject * __this /* static, unused */, float ___current0, float ___target1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mathf_DeltaAngle_m1024687732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___target1;
		float L_1 = ___current0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// Conversion methods for marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t2336485820_marshal_pinvoke(const Mathf_t2336485820& unmarshaled, Mathf_t2336485820_marshaled_pinvoke& marshaled)
{
}
extern "C" void Mathf_t2336485820_marshal_pinvoke_back(const Mathf_t2336485820_marshaled_pinvoke& marshaled, Mathf_t2336485820& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t2336485820_marshal_pinvoke_cleanup(Mathf_t2336485820_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t2336485820_marshal_com(const Mathf_t2336485820& unmarshaled, Mathf_t2336485820_marshaled_com& marshaled)
{
}
extern "C" void Mathf_t2336485820_marshal_com_back(const Mathf_t2336485820_marshaled_com& marshaled, Mathf_t2336485820& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.Mathf
extern "C" void Mathf_t2336485820_marshal_com_cleanup(Mathf_t2336485820_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C"  float Matrix4x4_get_Item_m312280350 (Matrix4x4_t2933234003 * __this, int32_t ___row0, int32_t ___column1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row0;
		int32_t L_1 = ___column1;
		float L_2 = Matrix4x4_get_Item_m3317262185(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  float Matrix4x4_get_Item_m312280350_AdjustorThunk (Il2CppObject * __this, int32_t ___row0, int32_t ___column1, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_get_Item_m312280350(_thisAdjusted, ___row0, ___column1, method);
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern Il2CppClass* IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1338169977;
extern const uint32_t Matrix4x4_get_Item_m3317262185_MetadataUsageId;
extern "C"  float Matrix4x4_get_Item_m3317262185 (Matrix4x4_t2933234003 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_Item_m3317262185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = __this->get_m00_0();
		return L_2;
	}

IL_0054:
	{
		float L_3 = __this->get_m10_1();
		return L_3;
	}

IL_005b:
	{
		float L_4 = __this->get_m20_2();
		return L_4;
	}

IL_0062:
	{
		float L_5 = __this->get_m30_3();
		return L_5;
	}

IL_0069:
	{
		float L_6 = __this->get_m01_4();
		return L_6;
	}

IL_0070:
	{
		float L_7 = __this->get_m11_5();
		return L_7;
	}

IL_0077:
	{
		float L_8 = __this->get_m21_6();
		return L_8;
	}

IL_007e:
	{
		float L_9 = __this->get_m31_7();
		return L_9;
	}

IL_0085:
	{
		float L_10 = __this->get_m02_8();
		return L_10;
	}

IL_008c:
	{
		float L_11 = __this->get_m12_9();
		return L_11;
	}

IL_0093:
	{
		float L_12 = __this->get_m22_10();
		return L_12;
	}

IL_009a:
	{
		float L_13 = __this->get_m32_11();
		return L_13;
	}

IL_00a1:
	{
		float L_14 = __this->get_m03_12();
		return L_14;
	}

IL_00a8:
	{
		float L_15 = __this->get_m13_13();
		return L_15;
	}

IL_00af:
	{
		float L_16 = __this->get_m23_14();
		return L_16;
	}

IL_00b6:
	{
		float L_17 = __this->get_m33_15();
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t3527622107 * L_18 = (IndexOutOfRangeException_t3527622107 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t3527622107_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m1847153122(L_18, _stringLiteral1338169977, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}
}
extern "C"  float Matrix4x4_get_Item_m3317262185_AdjustorThunk (Il2CppObject * __this, int32_t ___index0, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_get_Item_m3317262185(_thisAdjusted, ___index0, method);
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C"  int32_t Matrix4x4_GetHashCode_m3649708037 (Matrix4x4_t2933234003 * __this, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2243707581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t2243707581  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t2243707581  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector4_t2243707581  L_0 = Matrix4x4_GetColumn_m1367096730(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m1576457715((&V_0), /*hidden argument*/NULL);
		Vector4_t2243707581  L_2 = Matrix4x4_GetColumn_m1367096730(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m1576457715((&V_1), /*hidden argument*/NULL);
		Vector4_t2243707581  L_4 = Matrix4x4_GetColumn_m1367096730(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m1576457715((&V_2), /*hidden argument*/NULL);
		Vector4_t2243707581  L_6 = Matrix4x4_GetColumn_m1367096730(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m1576457715((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Matrix4x4_GetHashCode_m3649708037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_GetHashCode_m3649708037(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_Equals_m1321236479_MetadataUsageId;
extern "C"  bool Matrix4x4_Equals_m1321236479 (Matrix4x4_t2933234003 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_Equals_m1321236479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t2243707581  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t2243707581  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t2243707581  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t2243707581  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Matrix4x4_t2933234003_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Matrix4x4_t2933234003 *)((Matrix4x4_t2933234003 *)UnBox (L_1, Matrix4x4_t2933234003_il2cpp_TypeInfo_var))));
		Vector4_t2243707581  L_2 = Matrix4x4_GetColumn_m1367096730(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t2243707581  L_3 = Matrix4x4_GetColumn_m1367096730((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t2243707581  L_4 = L_3;
		Il2CppObject * L_5 = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m3783731577((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t2243707581  L_7 = Matrix4x4_GetColumn_m1367096730(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t2243707581  L_8 = Matrix4x4_GetColumn_m1367096730((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t2243707581  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m3783731577((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t2243707581  L_12 = Matrix4x4_GetColumn_m1367096730(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t2243707581  L_13 = Matrix4x4_GetColumn_m1367096730((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t2243707581  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m3783731577((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t2243707581  L_17 = Matrix4x4_GetColumn_m1367096730(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t2243707581  L_18 = Matrix4x4_GetColumn_m1367096730((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t2243707581  L_19 = L_18;
		Il2CppObject * L_20 = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m3783731577((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Matrix4x4_Equals_m1321236479_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_Equals_m1321236479(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C"  Vector4_t2243707581  Matrix4x4_GetColumn_m1367096730 (Matrix4x4_t2933234003 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i0;
		float L_1 = Matrix4x4_get_Item_m312280350(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i0;
		float L_3 = Matrix4x4_get_Item_m312280350(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i0;
		float L_5 = Matrix4x4_get_Item_m312280350(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i0;
		float L_7 = Matrix4x4_get_Item_m312280350(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t2243707581  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m1222289168(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  Vector4_t2243707581  Matrix4x4_GetColumn_m1367096730_AdjustorThunk (Il2CppObject * __this, int32_t ___i0, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_GetColumn_m1367096730(_thisAdjusted, ___i0, method);
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Matrix4x4_MultiplyPoint3x4_m1007952212 (Matrix4x4_t2933234003 * __this, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m00_0();
		float L_1 = (&___v0)->get_x_1();
		float L_2 = __this->get_m01_4();
		float L_3 = (&___v0)->get_y_2();
		float L_4 = __this->get_m02_8();
		float L_5 = (&___v0)->get_z_3();
		float L_6 = __this->get_m03_12();
		(&V_0)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6)));
		float L_7 = __this->get_m10_1();
		float L_8 = (&___v0)->get_x_1();
		float L_9 = __this->get_m11_5();
		float L_10 = (&___v0)->get_y_2();
		float L_11 = __this->get_m12_9();
		float L_12 = (&___v0)->get_z_3();
		float L_13 = __this->get_m13_13();
		(&V_0)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13)));
		float L_14 = __this->get_m20_2();
		float L_15 = (&___v0)->get_x_1();
		float L_16 = __this->get_m21_6();
		float L_17 = (&___v0)->get_y_2();
		float L_18 = __this->get_m22_10();
		float L_19 = (&___v0)->get_z_3();
		float L_20 = __this->get_m23_14();
		(&V_0)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20)));
		Vector3_t2243707580  L_21 = V_0;
		return L_21;
	}
}
extern "C"  Vector3_t2243707580  Matrix4x4_MultiplyPoint3x4_m1007952212_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_MultiplyPoint3x4_m1007952212(_thisAdjusted, ___v0, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_get_identity_m3039560904_MetadataUsageId;
extern "C"  Matrix4x4_t2933234003  Matrix4x4_get_identity_m3039560904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_get_identity_m3039560904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_m00_0((1.0f));
		(&V_0)->set_m01_4((0.0f));
		(&V_0)->set_m02_8((0.0f));
		(&V_0)->set_m03_12((0.0f));
		(&V_0)->set_m10_1((0.0f));
		(&V_0)->set_m11_5((1.0f));
		(&V_0)->set_m12_9((0.0f));
		(&V_0)->set_m13_13((0.0f));
		(&V_0)->set_m20_2((0.0f));
		(&V_0)->set_m21_6((0.0f));
		(&V_0)->set_m22_10((1.0f));
		(&V_0)->set_m23_14((0.0f));
		(&V_0)->set_m30_3((0.0f));
		(&V_0)->set_m31_7((0.0f));
		(&V_0)->set_m32_11((0.0f));
		(&V_0)->set_m33_15((1.0f));
		Matrix4x4_t2933234003  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t2933234003  Matrix4x4_TRS_m1913765359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___q1, Vector3_t2243707580  ___s2, const MethodInfo* method)
{
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Matrix4x4_INTERNAL_CALL_TRS_m2077933669(NULL /*static, unused*/, (&___pos0), (&___q1), (&___s2), (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern "C"  void Matrix4x4_INTERNAL_CALL_TRS_m2077933669 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___pos0, Quaternion_t4030073918 * ___q1, Vector3_t2243707580 * ___s2, Matrix4x4_t2933234003 * ___value3, const MethodInfo* method)
{
	typedef void (*Matrix4x4_INTERNAL_CALL_TRS_m2077933669_ftn) (Vector3_t2243707580 *, Quaternion_t4030073918 *, Vector3_t2243707580 *, Matrix4x4_t2933234003 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m2077933669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m2077933669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___pos0, ___q1, ___s2, ___value3);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3991437666;
extern const uint32_t Matrix4x4_ToString_m1982554457_MetadataUsageId;
extern "C"  String_t* Matrix4x4_ToString_m1982554457 (Matrix4x4_t2933234003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_ToString_m1982554457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		float L_1 = __this->get_m00_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_m01_4();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_m02_8();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_m03_12();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_12;
		float L_17 = __this->get_m10_1();
		float L_18 = L_17;
		Il2CppObject * L_19 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)L_19);
		ObjectU5BU5D_t3614634134* L_20 = L_16;
		float L_21 = __this->get_m11_5();
		float L_22 = L_21;
		Il2CppObject * L_23 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_23);
		ObjectU5BU5D_t3614634134* L_24 = L_20;
		float L_25 = __this->get_m12_9();
		float L_26 = L_25;
		Il2CppObject * L_27 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)L_27);
		ObjectU5BU5D_t3614634134* L_28 = L_24;
		float L_29 = __this->get_m13_13();
		float L_30 = L_29;
		Il2CppObject * L_31 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_31);
		ObjectU5BU5D_t3614634134* L_32 = L_28;
		float L_33 = __this->get_m20_2();
		float L_34 = L_33;
		Il2CppObject * L_35 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(8), (Il2CppObject *)L_35);
		ObjectU5BU5D_t3614634134* L_36 = L_32;
		float L_37 = __this->get_m21_6();
		float L_38 = L_37;
		Il2CppObject * L_39 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Il2CppObject *)L_39);
		ObjectU5BU5D_t3614634134* L_40 = L_36;
		float L_41 = __this->get_m22_10();
		float L_42 = L_41;
		Il2CppObject * L_43 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (Il2CppObject *)L_43);
		ObjectU5BU5D_t3614634134* L_44 = L_40;
		float L_45 = __this->get_m23_14();
		float L_46 = L_45;
		Il2CppObject * L_47 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (Il2CppObject *)L_47);
		ObjectU5BU5D_t3614634134* L_48 = L_44;
		float L_49 = __this->get_m30_3();
		float L_50 = L_49;
		Il2CppObject * L_51 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (Il2CppObject *)L_51);
		ObjectU5BU5D_t3614634134* L_52 = L_48;
		float L_53 = __this->get_m31_7();
		float L_54 = L_53;
		Il2CppObject * L_55 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (Il2CppObject *)L_55);
		ObjectU5BU5D_t3614634134* L_56 = L_52;
		float L_57 = __this->get_m32_11();
		float L_58 = L_57;
		Il2CppObject * L_59 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (Il2CppObject *)L_59);
		ObjectU5BU5D_t3614634134* L_60 = L_56;
		float L_61 = __this->get_m33_15();
		float L_62 = L_61;
		Il2CppObject * L_63 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (Il2CppObject *)L_63);
		String_t* L_64 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3991437666, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
extern "C"  String_t* Matrix4x4_ToString_m1982554457_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Matrix4x4_t2933234003 * _thisAdjusted = reinterpret_cast<Matrix4x4_t2933234003 *>(__this + 1);
	return Matrix4x4_ToString_m1982554457(_thisAdjusted, method);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern Il2CppClass* Matrix4x4_t2933234003_il2cpp_TypeInfo_var;
extern const uint32_t Matrix4x4_op_Multiply_m2352863493_MetadataUsageId;
extern "C"  Matrix4x4_t2933234003  Matrix4x4_op_Multiply_m2352863493 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___lhs0, Matrix4x4_t2933234003  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Matrix4x4_op_Multiply_m2352863493_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t2933234003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Matrix4x4_t2933234003_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = (&___lhs0)->get_m00_0();
		float L_1 = (&___rhs1)->get_m00_0();
		float L_2 = (&___lhs0)->get_m01_4();
		float L_3 = (&___rhs1)->get_m10_1();
		float L_4 = (&___lhs0)->get_m02_8();
		float L_5 = (&___rhs1)->get_m20_2();
		float L_6 = (&___lhs0)->get_m03_12();
		float L_7 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m00_0(((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7)))));
		float L_8 = (&___lhs0)->get_m00_0();
		float L_9 = (&___rhs1)->get_m01_4();
		float L_10 = (&___lhs0)->get_m01_4();
		float L_11 = (&___rhs1)->get_m11_5();
		float L_12 = (&___lhs0)->get_m02_8();
		float L_13 = (&___rhs1)->get_m21_6();
		float L_14 = (&___lhs0)->get_m03_12();
		float L_15 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m01_4(((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15)))));
		float L_16 = (&___lhs0)->get_m00_0();
		float L_17 = (&___rhs1)->get_m02_8();
		float L_18 = (&___lhs0)->get_m01_4();
		float L_19 = (&___rhs1)->get_m12_9();
		float L_20 = (&___lhs0)->get_m02_8();
		float L_21 = (&___rhs1)->get_m22_10();
		float L_22 = (&___lhs0)->get_m03_12();
		float L_23 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m02_8(((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23)))));
		float L_24 = (&___lhs0)->get_m00_0();
		float L_25 = (&___rhs1)->get_m03_12();
		float L_26 = (&___lhs0)->get_m01_4();
		float L_27 = (&___rhs1)->get_m13_13();
		float L_28 = (&___lhs0)->get_m02_8();
		float L_29 = (&___rhs1)->get_m23_14();
		float L_30 = (&___lhs0)->get_m03_12();
		float L_31 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m03_12(((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31)))));
		float L_32 = (&___lhs0)->get_m10_1();
		float L_33 = (&___rhs1)->get_m00_0();
		float L_34 = (&___lhs0)->get_m11_5();
		float L_35 = (&___rhs1)->get_m10_1();
		float L_36 = (&___lhs0)->get_m12_9();
		float L_37 = (&___rhs1)->get_m20_2();
		float L_38 = (&___lhs0)->get_m13_13();
		float L_39 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m10_1(((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39)))));
		float L_40 = (&___lhs0)->get_m10_1();
		float L_41 = (&___rhs1)->get_m01_4();
		float L_42 = (&___lhs0)->get_m11_5();
		float L_43 = (&___rhs1)->get_m11_5();
		float L_44 = (&___lhs0)->get_m12_9();
		float L_45 = (&___rhs1)->get_m21_6();
		float L_46 = (&___lhs0)->get_m13_13();
		float L_47 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m11_5(((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47)))));
		float L_48 = (&___lhs0)->get_m10_1();
		float L_49 = (&___rhs1)->get_m02_8();
		float L_50 = (&___lhs0)->get_m11_5();
		float L_51 = (&___rhs1)->get_m12_9();
		float L_52 = (&___lhs0)->get_m12_9();
		float L_53 = (&___rhs1)->get_m22_10();
		float L_54 = (&___lhs0)->get_m13_13();
		float L_55 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m12_9(((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55)))));
		float L_56 = (&___lhs0)->get_m10_1();
		float L_57 = (&___rhs1)->get_m03_12();
		float L_58 = (&___lhs0)->get_m11_5();
		float L_59 = (&___rhs1)->get_m13_13();
		float L_60 = (&___lhs0)->get_m12_9();
		float L_61 = (&___rhs1)->get_m23_14();
		float L_62 = (&___lhs0)->get_m13_13();
		float L_63 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m13_13(((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63)))));
		float L_64 = (&___lhs0)->get_m20_2();
		float L_65 = (&___rhs1)->get_m00_0();
		float L_66 = (&___lhs0)->get_m21_6();
		float L_67 = (&___rhs1)->get_m10_1();
		float L_68 = (&___lhs0)->get_m22_10();
		float L_69 = (&___rhs1)->get_m20_2();
		float L_70 = (&___lhs0)->get_m23_14();
		float L_71 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m20_2(((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71)))));
		float L_72 = (&___lhs0)->get_m20_2();
		float L_73 = (&___rhs1)->get_m01_4();
		float L_74 = (&___lhs0)->get_m21_6();
		float L_75 = (&___rhs1)->get_m11_5();
		float L_76 = (&___lhs0)->get_m22_10();
		float L_77 = (&___rhs1)->get_m21_6();
		float L_78 = (&___lhs0)->get_m23_14();
		float L_79 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m21_6(((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79)))));
		float L_80 = (&___lhs0)->get_m20_2();
		float L_81 = (&___rhs1)->get_m02_8();
		float L_82 = (&___lhs0)->get_m21_6();
		float L_83 = (&___rhs1)->get_m12_9();
		float L_84 = (&___lhs0)->get_m22_10();
		float L_85 = (&___rhs1)->get_m22_10();
		float L_86 = (&___lhs0)->get_m23_14();
		float L_87 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m22_10(((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87)))));
		float L_88 = (&___lhs0)->get_m20_2();
		float L_89 = (&___rhs1)->get_m03_12();
		float L_90 = (&___lhs0)->get_m21_6();
		float L_91 = (&___rhs1)->get_m13_13();
		float L_92 = (&___lhs0)->get_m22_10();
		float L_93 = (&___rhs1)->get_m23_14();
		float L_94 = (&___lhs0)->get_m23_14();
		float L_95 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m23_14(((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95)))));
		float L_96 = (&___lhs0)->get_m30_3();
		float L_97 = (&___rhs1)->get_m00_0();
		float L_98 = (&___lhs0)->get_m31_7();
		float L_99 = (&___rhs1)->get_m10_1();
		float L_100 = (&___lhs0)->get_m32_11();
		float L_101 = (&___rhs1)->get_m20_2();
		float L_102 = (&___lhs0)->get_m33_15();
		float L_103 = (&___rhs1)->get_m30_3();
		(&V_0)->set_m30_3(((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103)))));
		float L_104 = (&___lhs0)->get_m30_3();
		float L_105 = (&___rhs1)->get_m01_4();
		float L_106 = (&___lhs0)->get_m31_7();
		float L_107 = (&___rhs1)->get_m11_5();
		float L_108 = (&___lhs0)->get_m32_11();
		float L_109 = (&___rhs1)->get_m21_6();
		float L_110 = (&___lhs0)->get_m33_15();
		float L_111 = (&___rhs1)->get_m31_7();
		(&V_0)->set_m31_7(((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111)))));
		float L_112 = (&___lhs0)->get_m30_3();
		float L_113 = (&___rhs1)->get_m02_8();
		float L_114 = (&___lhs0)->get_m31_7();
		float L_115 = (&___rhs1)->get_m12_9();
		float L_116 = (&___lhs0)->get_m32_11();
		float L_117 = (&___rhs1)->get_m22_10();
		float L_118 = (&___lhs0)->get_m33_15();
		float L_119 = (&___rhs1)->get_m32_11();
		(&V_0)->set_m32_11(((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119)))));
		float L_120 = (&___lhs0)->get_m30_3();
		float L_121 = (&___rhs1)->get_m03_12();
		float L_122 = (&___lhs0)->get_m31_7();
		float L_123 = (&___rhs1)->get_m13_13();
		float L_124 = (&___lhs0)->get_m32_11();
		float L_125 = (&___rhs1)->get_m23_14();
		float L_126 = (&___lhs0)->get_m33_15();
		float L_127 = (&___rhs1)->get_m33_15();
		(&V_0)->set_m33_15(((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127)))));
		Matrix4x4_t2933234003  L_128 = V_0;
		return L_128;
	}
}
// Conversion methods for marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t2933234003_marshal_pinvoke(const Matrix4x4_t2933234003& unmarshaled, Matrix4x4_t2933234003_marshaled_pinvoke& marshaled)
{
	marshaled.___m00_0 = unmarshaled.get_m00_0();
	marshaled.___m10_1 = unmarshaled.get_m10_1();
	marshaled.___m20_2 = unmarshaled.get_m20_2();
	marshaled.___m30_3 = unmarshaled.get_m30_3();
	marshaled.___m01_4 = unmarshaled.get_m01_4();
	marshaled.___m11_5 = unmarshaled.get_m11_5();
	marshaled.___m21_6 = unmarshaled.get_m21_6();
	marshaled.___m31_7 = unmarshaled.get_m31_7();
	marshaled.___m02_8 = unmarshaled.get_m02_8();
	marshaled.___m12_9 = unmarshaled.get_m12_9();
	marshaled.___m22_10 = unmarshaled.get_m22_10();
	marshaled.___m32_11 = unmarshaled.get_m32_11();
	marshaled.___m03_12 = unmarshaled.get_m03_12();
	marshaled.___m13_13 = unmarshaled.get_m13_13();
	marshaled.___m23_14 = unmarshaled.get_m23_14();
	marshaled.___m33_15 = unmarshaled.get_m33_15();
}
extern "C" void Matrix4x4_t2933234003_marshal_pinvoke_back(const Matrix4x4_t2933234003_marshaled_pinvoke& marshaled, Matrix4x4_t2933234003& unmarshaled)
{
	float unmarshaled_m00_temp_0 = 0.0f;
	unmarshaled_m00_temp_0 = marshaled.___m00_0;
	unmarshaled.set_m00_0(unmarshaled_m00_temp_0);
	float unmarshaled_m10_temp_1 = 0.0f;
	unmarshaled_m10_temp_1 = marshaled.___m10_1;
	unmarshaled.set_m10_1(unmarshaled_m10_temp_1);
	float unmarshaled_m20_temp_2 = 0.0f;
	unmarshaled_m20_temp_2 = marshaled.___m20_2;
	unmarshaled.set_m20_2(unmarshaled_m20_temp_2);
	float unmarshaled_m30_temp_3 = 0.0f;
	unmarshaled_m30_temp_3 = marshaled.___m30_3;
	unmarshaled.set_m30_3(unmarshaled_m30_temp_3);
	float unmarshaled_m01_temp_4 = 0.0f;
	unmarshaled_m01_temp_4 = marshaled.___m01_4;
	unmarshaled.set_m01_4(unmarshaled_m01_temp_4);
	float unmarshaled_m11_temp_5 = 0.0f;
	unmarshaled_m11_temp_5 = marshaled.___m11_5;
	unmarshaled.set_m11_5(unmarshaled_m11_temp_5);
	float unmarshaled_m21_temp_6 = 0.0f;
	unmarshaled_m21_temp_6 = marshaled.___m21_6;
	unmarshaled.set_m21_6(unmarshaled_m21_temp_6);
	float unmarshaled_m31_temp_7 = 0.0f;
	unmarshaled_m31_temp_7 = marshaled.___m31_7;
	unmarshaled.set_m31_7(unmarshaled_m31_temp_7);
	float unmarshaled_m02_temp_8 = 0.0f;
	unmarshaled_m02_temp_8 = marshaled.___m02_8;
	unmarshaled.set_m02_8(unmarshaled_m02_temp_8);
	float unmarshaled_m12_temp_9 = 0.0f;
	unmarshaled_m12_temp_9 = marshaled.___m12_9;
	unmarshaled.set_m12_9(unmarshaled_m12_temp_9);
	float unmarshaled_m22_temp_10 = 0.0f;
	unmarshaled_m22_temp_10 = marshaled.___m22_10;
	unmarshaled.set_m22_10(unmarshaled_m22_temp_10);
	float unmarshaled_m32_temp_11 = 0.0f;
	unmarshaled_m32_temp_11 = marshaled.___m32_11;
	unmarshaled.set_m32_11(unmarshaled_m32_temp_11);
	float unmarshaled_m03_temp_12 = 0.0f;
	unmarshaled_m03_temp_12 = marshaled.___m03_12;
	unmarshaled.set_m03_12(unmarshaled_m03_temp_12);
	float unmarshaled_m13_temp_13 = 0.0f;
	unmarshaled_m13_temp_13 = marshaled.___m13_13;
	unmarshaled.set_m13_13(unmarshaled_m13_temp_13);
	float unmarshaled_m23_temp_14 = 0.0f;
	unmarshaled_m23_temp_14 = marshaled.___m23_14;
	unmarshaled.set_m23_14(unmarshaled_m23_temp_14);
	float unmarshaled_m33_temp_15 = 0.0f;
	unmarshaled_m33_temp_15 = marshaled.___m33_15;
	unmarshaled.set_m33_15(unmarshaled_m33_temp_15);
}
// Conversion method for clean up from marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t2933234003_marshal_pinvoke_cleanup(Matrix4x4_t2933234003_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t2933234003_marshal_com(const Matrix4x4_t2933234003& unmarshaled, Matrix4x4_t2933234003_marshaled_com& marshaled)
{
	marshaled.___m00_0 = unmarshaled.get_m00_0();
	marshaled.___m10_1 = unmarshaled.get_m10_1();
	marshaled.___m20_2 = unmarshaled.get_m20_2();
	marshaled.___m30_3 = unmarshaled.get_m30_3();
	marshaled.___m01_4 = unmarshaled.get_m01_4();
	marshaled.___m11_5 = unmarshaled.get_m11_5();
	marshaled.___m21_6 = unmarshaled.get_m21_6();
	marshaled.___m31_7 = unmarshaled.get_m31_7();
	marshaled.___m02_8 = unmarshaled.get_m02_8();
	marshaled.___m12_9 = unmarshaled.get_m12_9();
	marshaled.___m22_10 = unmarshaled.get_m22_10();
	marshaled.___m32_11 = unmarshaled.get_m32_11();
	marshaled.___m03_12 = unmarshaled.get_m03_12();
	marshaled.___m13_13 = unmarshaled.get_m13_13();
	marshaled.___m23_14 = unmarshaled.get_m23_14();
	marshaled.___m33_15 = unmarshaled.get_m33_15();
}
extern "C" void Matrix4x4_t2933234003_marshal_com_back(const Matrix4x4_t2933234003_marshaled_com& marshaled, Matrix4x4_t2933234003& unmarshaled)
{
	float unmarshaled_m00_temp_0 = 0.0f;
	unmarshaled_m00_temp_0 = marshaled.___m00_0;
	unmarshaled.set_m00_0(unmarshaled_m00_temp_0);
	float unmarshaled_m10_temp_1 = 0.0f;
	unmarshaled_m10_temp_1 = marshaled.___m10_1;
	unmarshaled.set_m10_1(unmarshaled_m10_temp_1);
	float unmarshaled_m20_temp_2 = 0.0f;
	unmarshaled_m20_temp_2 = marshaled.___m20_2;
	unmarshaled.set_m20_2(unmarshaled_m20_temp_2);
	float unmarshaled_m30_temp_3 = 0.0f;
	unmarshaled_m30_temp_3 = marshaled.___m30_3;
	unmarshaled.set_m30_3(unmarshaled_m30_temp_3);
	float unmarshaled_m01_temp_4 = 0.0f;
	unmarshaled_m01_temp_4 = marshaled.___m01_4;
	unmarshaled.set_m01_4(unmarshaled_m01_temp_4);
	float unmarshaled_m11_temp_5 = 0.0f;
	unmarshaled_m11_temp_5 = marshaled.___m11_5;
	unmarshaled.set_m11_5(unmarshaled_m11_temp_5);
	float unmarshaled_m21_temp_6 = 0.0f;
	unmarshaled_m21_temp_6 = marshaled.___m21_6;
	unmarshaled.set_m21_6(unmarshaled_m21_temp_6);
	float unmarshaled_m31_temp_7 = 0.0f;
	unmarshaled_m31_temp_7 = marshaled.___m31_7;
	unmarshaled.set_m31_7(unmarshaled_m31_temp_7);
	float unmarshaled_m02_temp_8 = 0.0f;
	unmarshaled_m02_temp_8 = marshaled.___m02_8;
	unmarshaled.set_m02_8(unmarshaled_m02_temp_8);
	float unmarshaled_m12_temp_9 = 0.0f;
	unmarshaled_m12_temp_9 = marshaled.___m12_9;
	unmarshaled.set_m12_9(unmarshaled_m12_temp_9);
	float unmarshaled_m22_temp_10 = 0.0f;
	unmarshaled_m22_temp_10 = marshaled.___m22_10;
	unmarshaled.set_m22_10(unmarshaled_m22_temp_10);
	float unmarshaled_m32_temp_11 = 0.0f;
	unmarshaled_m32_temp_11 = marshaled.___m32_11;
	unmarshaled.set_m32_11(unmarshaled_m32_temp_11);
	float unmarshaled_m03_temp_12 = 0.0f;
	unmarshaled_m03_temp_12 = marshaled.___m03_12;
	unmarshaled.set_m03_12(unmarshaled_m03_temp_12);
	float unmarshaled_m13_temp_13 = 0.0f;
	unmarshaled_m13_temp_13 = marshaled.___m13_13;
	unmarshaled.set_m13_13(unmarshaled_m13_temp_13);
	float unmarshaled_m23_temp_14 = 0.0f;
	unmarshaled_m23_temp_14 = marshaled.___m23_14;
	unmarshaled.set_m23_14(unmarshaled_m23_temp_14);
	float unmarshaled_m33_temp_15 = 0.0f;
	unmarshaled_m33_temp_15 = marshaled.___m33_15;
	unmarshaled.set_m33_15(unmarshaled_m33_temp_15);
}
// Conversion method for clean up from marshalling of: UnityEngine.Matrix4x4
extern "C" void Matrix4x4_t2933234003_marshal_com_cleanup(Matrix4x4_t2933234003_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Mesh::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Mesh__ctor_m2975981674_MetadataUsageId;
extern "C"  void Mesh__ctor_m2975981674 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh__ctor_m2975981674_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m1486058998(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m1486058998 (Il2CppObject * __this /* static, unused */, Mesh_t1356156583 * ___mono0, const MethodInfo* method)
{
	typedef void (*Mesh_Internal_Create_m1486058998_ftn) (Mesh_t1356156583 *);
	static Mesh_Internal_Create_m1486058998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m1486058998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C"  void Mesh_Clear_m3100797454 (Mesh_t1356156583 * __this, bool ___keepVertexLayout0, const MethodInfo* method)
{
	typedef void (*Mesh_Clear_m3100797454_ftn) (Mesh_t1356156583 *, bool);
	static Mesh_Clear_m3100797454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Clear_m3100797454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Clear(System.Boolean)");
	_il2cpp_icall_func(__this, ___keepVertexLayout0);
}
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m231813403 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		Mesh_Clear_m3100797454(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m2763498171 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef bool (*Mesh_get_canAccess_m2763498171_ftn) (Mesh_t1356156583 *);
	static Mesh_get_canAccess_m2763498171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_canAccess_m2763498171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_canAccess()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessMesh(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  void Mesh_PrintErrorCantAccessMesh_m2827771108 (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	typedef void (*Mesh_PrintErrorCantAccessMesh_m2827771108_ftn) (Mesh_t1356156583 *, int32_t);
	static Mesh_PrintErrorCantAccessMesh_m2827771108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorCantAccessMesh_m2827771108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorCantAccessMesh(UnityEngine.Mesh/InternalShaderChannel)");
	_il2cpp_icall_func(__this, ___channel0);
}
// System.Void UnityEngine.Mesh::PrintErrorCantAccessMeshForIndices()
extern "C"  void Mesh_PrintErrorCantAccessMeshForIndices_m3276222682 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_PrintErrorCantAccessMeshForIndices_m3276222682_ftn) (Mesh_t1356156583 *);
	static Mesh_PrintErrorCantAccessMeshForIndices_m3276222682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorCantAccessMeshForIndices_m3276222682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorCantAccessMeshForIndices()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::PrintErrorBadSubmeshIndexTriangles()
extern "C"  void Mesh_PrintErrorBadSubmeshIndexTriangles_m3830862268 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_PrintErrorBadSubmeshIndexTriangles_m3830862268_ftn) (Mesh_t1356156583 *);
	static Mesh_PrintErrorBadSubmeshIndexTriangles_m3830862268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorBadSubmeshIndexTriangles_m3830862268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorBadSubmeshIndexTriangles()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::PrintErrorBadSubmeshIndexIndices()
extern "C"  void Mesh_PrintErrorBadSubmeshIndexIndices_m2104981732 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_PrintErrorBadSubmeshIndexIndices_m2104981732_ftn) (Mesh_t1356156583 *);
	static Mesh_PrintErrorBadSubmeshIndexIndices_m2104981732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_PrintErrorBadSubmeshIndexIndices_m2104981732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::PrintErrorBadSubmeshIndexIndices()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetArrayForChannelImpl_m271696022 (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, Il2CppArray * ___values3, int32_t ___arraySize4, const MethodInfo* method)
{
	typedef void (*Mesh_SetArrayForChannelImpl_m271696022_ftn) (Mesh_t1356156583 *, int32_t, int32_t, int32_t, Il2CppArray *, int32_t);
	static Mesh_SetArrayForChannelImpl_m271696022_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetArrayForChannelImpl_m271696022_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)");
	_il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2, ___values3, ___arraySize4);
}
// System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
extern "C"  Il2CppArray * Mesh_GetAllocArrayFromChannelImpl_m1663415136 (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, const MethodInfo* method)
{
	typedef Il2CppArray * (*Mesh_GetAllocArrayFromChannelImpl_m1663415136_ftn) (Mesh_t1356156583 *, int32_t, int32_t, int32_t);
	static Mesh_GetAllocArrayFromChannelImpl_m1663415136_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetAllocArrayFromChannelImpl_m1663415136_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)");
	return _il2cpp_icall_func(__this, ___channel0, ___format1, ___dim2);
}
// System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
extern "C"  bool Mesh_HasChannel_m3616583481 (Mesh_t1356156583 * __this, int32_t ___channel0, const MethodInfo* method)
{
	typedef bool (*Mesh_HasChannel_m3616583481_ftn) (Mesh_t1356156583 *, int32_t);
	static Mesh_HasChannel_m3616583481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_HasChannel_m3616583481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)");
	return _il2cpp_icall_func(__this, ___channel0);
}
// System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
extern "C"  Il2CppArray * Mesh_ExtractArrayFromList_m1349408169 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method)
{
	typedef Il2CppArray * (*Mesh_ExtractArrayFromList_m1349408169_ftn) (Il2CppObject *);
	static Mesh_ExtractArrayFromList_m1349408169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_ExtractArrayFromList_m1349408169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::ExtractArrayFromList(System.Object)");
	return _il2cpp_icall_func(___list0);
}
// System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
extern "C"  Int32U5BU5D_t3030399641* Mesh_GetIndicesImpl_m2398977086 (Mesh_t1356156583 * __this, int32_t ___submesh0, const MethodInfo* method)
{
	typedef Int32U5BU5D_t3030399641* (*Mesh_GetIndicesImpl_m2398977086_ftn) (Mesh_t1356156583 *, int32_t);
	static Mesh_GetIndicesImpl_m2398977086_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_GetIndicesImpl_m2398977086_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::GetIndicesImpl(System.Int32)");
	return _il2cpp_icall_func(__this, ___submesh0);
}
// System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
extern "C"  void Mesh_SetTrianglesImpl_m2743099196 (Mesh_t1356156583 * __this, int32_t ___submesh0, Il2CppArray * ___triangles1, int32_t ___arraySize2, bool ___calculateBounds3, const MethodInfo* method)
{
	typedef void (*Mesh_SetTrianglesImpl_m2743099196_ftn) (Mesh_t1356156583 *, int32_t, Il2CppArray *, int32_t, bool);
	static Mesh_SetTrianglesImpl_m2743099196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_SetTrianglesImpl_m2743099196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___submesh0, ___triangles1, ___arraySize2, ___calculateBounds3);
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void Mesh_SetTriangles_m2017297103 (Mesh_t1356156583 * __this, List_1_t1440998580 * ___triangles0, int32_t ___submesh1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		List_1_t1440998580 * L_0 = ___triangles0;
		int32_t L_1 = ___submesh1;
		bool L_2 = V_0;
		Mesh_SetTriangles_m2506325172(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean)
extern const MethodInfo* Mesh_SafeLength_TisInt32_t2071877448_m2161861392_MethodInfo_var;
extern const uint32_t Mesh_SetTriangles_m2506325172_MetadataUsageId;
extern "C"  void Mesh_SetTriangles_m2506325172 (Mesh_t1356156583 * __this, List_1_t1440998580 * ___triangles0, int32_t ___submesh1, bool ___calculateBounds2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetTriangles_m2506325172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___submesh1;
		bool L_1 = Mesh_CheckCanAccessSubmeshTriangles_m3203185587(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = ___submesh1;
		List_1_t1440998580 * L_3 = ___triangles0;
		Il2CppArray * L_4 = Mesh_ExtractArrayFromList_m1349408169(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		List_1_t1440998580 * L_5 = ___triangles0;
		int32_t L_6 = Mesh_SafeLength_TisInt32_t2071877448_m2161861392(__this, L_5, /*hidden argument*/Mesh_SafeLength_TisInt32_t2071877448_m2161861392_MethodInfo_var);
		bool L_7 = ___calculateBounds2;
		Mesh_SetTrianglesImpl_m2743099196(__this, L_2, L_4, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m3559909024 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_RecalculateBounds_m3559909024_ftn) (Mesh_t1356156583 *);
	static Mesh_RecalculateBounds_m3559909024_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_RecalculateBounds_m3559909024_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::RecalculateBounds()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Mesh::get_vertexCount()
extern "C"  int32_t Mesh_get_vertexCount_m989566320 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef int32_t (*Mesh_get_vertexCount_m989566320_ftn) (Mesh_t1356156583 *);
	static Mesh_get_vertexCount_m989566320_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_vertexCount_m989566320_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m1945011773 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	typedef int32_t (*Mesh_get_subMeshCount_m1945011773_ftn) (Mesh_t1356156583 *);
	static Mesh_get_subMeshCount_m1945011773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_get_subMeshCount_m1945011773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::get_subMeshCount()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Mesh/InternalShaderChannel UnityEngine.Mesh::GetUVChannel(System.Int32)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1489833396;
extern Il2CppCodeGenString* _stringLiteral1296238845;
extern const uint32_t Mesh_GetUVChannel_m364477864_MetadataUsageId;
extern "C"  int32_t Mesh_GetUVChannel_m364477864 (Mesh_t1356156583 * __this, int32_t ___uvIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_GetUVChannel_m364477864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___uvIndex0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___uvIndex0;
		if ((((int32_t)L_1) <= ((int32_t)3)))
		{
			goto IL_001e;
		}
	}

IL_000e:
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_2, _stringLiteral1489833396, _stringLiteral1296238845, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_001e:
	{
		int32_t L_3 = ___uvIndex0;
		return (int32_t)(((int32_t)((int32_t)3+(int32_t)L_3)));
	}
}
// System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Mesh/InternalShaderChannel)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1066778515;
extern Il2CppCodeGenString* _stringLiteral3637194755;
extern const uint32_t Mesh_DefaultDimensionForChannel_m153181993_MetadataUsageId;
extern "C"  int32_t Mesh_DefaultDimensionForChannel_m153181993 (Il2CppObject * __this /* static, unused */, int32_t ___channel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_DefaultDimensionForChannel_m153181993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___channel0;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___channel0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_000f;
		}
	}

IL_000d:
	{
		return 3;
	}

IL_000f:
	{
		int32_t L_2 = ___channel0;
		if ((((int32_t)L_2) < ((int32_t)3)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = ___channel0;
		if ((((int32_t)L_3) > ((int32_t)6)))
		{
			goto IL_001f;
		}
	}
	{
		return 2;
	}

IL_001f:
	{
		int32_t L_4 = ___channel0;
		if ((((int32_t)L_4) == ((int32_t)7)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_5 = ___channel0;
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_002f;
		}
	}

IL_002d:
	{
		return 4;
	}

IL_002f:
	{
		ArgumentException_t3259014390 * L_6 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_6, _stringLiteral1066778515, _stringLiteral3637194755, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}
}
// System.Int32 UnityEngine.Mesh::SafeLength(System.Array)
extern "C"  int32_t Mesh_SafeLength_m3028452891 (Mesh_t1356156583 * __this, Il2CppArray * ___values0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppArray * L_0 = ___values0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Il2CppArray * L_1 = ___values0;
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m1498215565(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
extern "C"  void Mesh_SetSizedArrayForChannel_m299035387 (Mesh_t1356156583 * __this, int32_t ___channel0, int32_t ___format1, int32_t ___dim2, Il2CppArray * ___values3, int32_t ___valuesCount4, const MethodInfo* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m2763498171(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = ___channel0;
		int32_t L_2 = ___format1;
		int32_t L_3 = ___dim2;
		Il2CppArray * L_4 = ___values3;
		int32_t L_5 = ___valuesCount4;
		Mesh_SetArrayForChannelImpl_m271696022(__this, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		goto IL_0024;
	}

IL_001d:
	{
		int32_t L_6 = ___channel0;
		Mesh_PrintErrorCantAccessMesh_m2827771108(__this, L_6, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417_MethodInfo_var;
extern const uint32_t Mesh_get_vertices_m626989480_MetadataUsageId;
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_vertices_m626989480 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_vertices_m626989480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3U5BU5D_t1172311765* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417(__this, 0, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417_MethodInfo_var);
		return L_0;
	}
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern const MethodInfo* Mesh_SetArrayForChannel_TisVector3_t2243707580_m2048180582_MethodInfo_var;
extern const uint32_t Mesh_set_vertices_m2936804213_MetadataUsageId;
extern "C"  void Mesh_set_vertices_m2936804213 (Mesh_t1356156583 * __this, Vector3U5BU5D_t1172311765* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_set_vertices_m2936804213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3U5BU5D_t1172311765* L_0 = ___value0;
		Mesh_SetArrayForChannel_TisVector3_t2243707580_m2048180582(__this, 0, L_0, /*hidden argument*/Mesh_SetArrayForChannel_TisVector3_t2243707580_m2048180582_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417_MethodInfo_var;
extern const uint32_t Mesh_get_normals_m1837187359_MetadataUsageId;
extern "C"  Vector3U5BU5D_t1172311765* Mesh_get_normals_m1837187359 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_normals_m1837187359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3U5BU5D_t1172311765* L_0 = Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417(__this, 1, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector3_t2243707580_m873518417_MethodInfo_var);
		return L_0;
	}
}
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m1116766164_MethodInfo_var;
extern const uint32_t Mesh_get_tangents_m2910922714_MetadataUsageId;
extern "C"  Vector4U5BU5D_t1658499504* Mesh_get_tangents_m2910922714 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_tangents_m2910922714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector4U5BU5D_t1658499504* L_0 = Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m1116766164(__this, 7, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector4_t2243707581_m1116766164_MethodInfo_var);
		return L_0;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534_MethodInfo_var;
extern const uint32_t Mesh_get_uv_m3811151337_MetadataUsageId;
extern "C"  Vector2U5BU5D_t686124026* Mesh_get_uv_m3811151337 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv_m3811151337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2U5BU5D_t686124026* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534(__this, 3, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534_MethodInfo_var);
		return L_0;
	}
}
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534_MethodInfo_var;
extern const uint32_t Mesh_get_uv2_m3215494975_MetadataUsageId;
extern "C"  Vector2U5BU5D_t686124026* Mesh_get_uv2_m3215494975 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_uv2_m3215494975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2U5BU5D_t686124026* L_0 = Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534(__this, 4, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisVector2_t2243707579_m3884092534_MethodInfo_var);
		return L_0;
	}
}
// UnityEngine.Color[] UnityEngine.Mesh::get_colors()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisColor_t2020392075_m1713004566_MethodInfo_var;
extern const uint32_t Mesh_get_colors_m3140088914_MetadataUsageId;
extern "C"  ColorU5BU5D_t672350442* Mesh_get_colors_m3140088914 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_colors_m3140088914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ColorU5BU5D_t672350442* L_0 = Mesh_GetAllocArrayFromChannel_TisColor_t2020392075_m1713004566(__this, 2, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisColor_t2020392075_m1713004566_MethodInfo_var);
		return L_0;
	}
}
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
extern const MethodInfo* Mesh_SetArrayForChannel_TisColor_t2020392075_m3773097225_MethodInfo_var;
extern const uint32_t Mesh_set_colors_m864557505_MetadataUsageId;
extern "C"  void Mesh_set_colors_m864557505 (Mesh_t1356156583 * __this, ColorU5BU5D_t672350442* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_set_colors_m864557505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ColorU5BU5D_t672350442* L_0 = ___value0;
		Mesh_SetArrayForChannel_TisColor_t2020392075_m3773097225(__this, 2, L_0, /*hidden argument*/Mesh_SetArrayForChannel_TisColor_t2020392075_m3773097225_MethodInfo_var);
		return;
	}
}
// UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern const MethodInfo* Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m992949857_MethodInfo_var;
extern const uint32_t Mesh_get_colors32_m4153271224_MetadataUsageId;
extern "C"  Color32U5BU5D_t30278651* Mesh_get_colors32_m4153271224 (Mesh_t1356156583 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_get_colors32_m4153271224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color32U5BU5D_t30278651* L_0 = Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m992949857(__this, 2, 2, 1, /*hidden argument*/Mesh_GetAllocArrayFromChannel_TisColor32_t874517518_m992949857_MethodInfo_var);
		return L_0;
	}
}
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern const MethodInfo* Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609_MethodInfo_var;
extern const uint32_t Mesh_SetVertices_m3500868388_MetadataUsageId;
extern "C"  void Mesh_SetVertices_m3500868388 (Mesh_t1356156583 * __this, List_1_t1612828712 * ___inVertices0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetVertices_m3500868388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1612828712 * L_0 = ___inVertices0;
		Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609(__this, 0, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern const MethodInfo* Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609_MethodInfo_var;
extern const uint32_t Mesh_SetNormals_m3341225499_MetadataUsageId;
extern "C"  void Mesh_SetNormals_m3341225499 (Mesh_t1356156583 * __this, List_1_t1612828712 * ___inNormals0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetNormals_m3341225499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1612828712 * L_0 = ___inNormals0;
		Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609(__this, 1, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector3_t2243707580_m4266574609_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern const MethodInfo* Mesh_SetListForChannel_TisVector4_t2243707581_m2402795402_MethodInfo_var;
extern const uint32_t Mesh_SetTangents_m282399504_MetadataUsageId;
extern "C"  void Mesh_SetTangents_m282399504 (Mesh_t1356156583 * __this, List_1_t1612828713 * ___inTangents0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetTangents_m282399504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1612828713 * L_0 = ___inTangents0;
		Mesh_SetListForChannel_TisVector4_t2243707581_m2402795402(__this, 7, L_0, /*hidden argument*/Mesh_SetListForChannel_TisVector4_t2243707581_m2402795402_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern const MethodInfo* Mesh_SetListForChannel_TisColor32_t874517518_m3239432057_MethodInfo_var;
extern const uint32_t Mesh_SetColors_m3438776703_MetadataUsageId;
extern "C"  void Mesh_SetColors_m3438776703 (Mesh_t1356156583 * __this, List_1_t243638650 * ___inColors0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetColors_m3438776703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t243638650 * L_0 = ___inColors0;
		Mesh_SetListForChannel_TisColor32_t874517518_m3239432057(__this, 2, 2, 1, L_0, /*hidden argument*/Mesh_SetListForChannel_TisColor32_t874517518_m3239432057_MethodInfo_var);
		return;
	}
}
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern const MethodInfo* Mesh_SetUvsImpl_TisVector2_t2243707579_m1129533604_MethodInfo_var;
extern const uint32_t Mesh_SetUVs_m841280343_MetadataUsageId;
extern "C"  void Mesh_SetUVs_m841280343 (Mesh_t1356156583 * __this, int32_t ___channel0, List_1_t1612828711 * ___uvs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_SetUVs_m841280343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___channel0;
		List_1_t1612828711 * L_1 = ___uvs1;
		Mesh_SetUvsImpl_TisVector2_t2243707579_m1129533604(__this, L_0, 2, L_1, /*hidden argument*/Mesh_SetUvsImpl_TisVector2_t2243707579_m1129533604_MethodInfo_var);
		return;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern "C"  bool Mesh_CheckCanAccessSubmesh_m1826512193 (Mesh_t1356156583 * __this, int32_t ___submesh0, bool ___errorAboutTriangles1, const MethodInfo* method)
{
	{
		bool L_0 = Mesh_get_canAccess_m2763498171(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Mesh_PrintErrorCantAccessMeshForIndices_m3276222682(__this, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0013:
	{
		int32_t L_1 = ___submesh0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_2 = ___submesh0;
		int32_t L_3 = Mesh_get_subMeshCount_m1945011773(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_003f;
		}
	}

IL_0026:
	{
		bool L_4 = ___errorAboutTriangles1;
		if (!L_4)
		{
			goto IL_0037;
		}
	}
	{
		Mesh_PrintErrorBadSubmeshIndexTriangles_m3830862268(__this, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0037:
	{
		Mesh_PrintErrorBadSubmeshIndexIndices_m2104981732(__this, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return (bool)0;
	}

IL_003f:
	{
		return (bool)1;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshTriangles_m3203185587 (Mesh_t1356156583 * __this, int32_t ___submesh0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmesh_m1826512193(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern "C"  bool Mesh_CheckCanAccessSubmeshIndices_m2098382465 (Mesh_t1356156583 * __this, int32_t ___submesh0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmesh_m1826512193(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t Mesh_GetIndices_m3085881884_MetadataUsageId;
extern "C"  Int32U5BU5D_t3030399641* Mesh_GetIndices_m3085881884 (Mesh_t1356156583 * __this, int32_t ___submesh0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Mesh_GetIndices_m3085881884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3030399641* G_B3_0 = NULL;
	{
		int32_t L_0 = ___submesh0;
		bool L_1 = Mesh_CheckCanAccessSubmeshIndices_m2098382465(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = ___submesh0;
		Int32U5BU5D_t3030399641* L_3 = Mesh_GetIndicesImpl_m2398977086(__this, L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_001e;
	}

IL_0018:
	{
		G_B3_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_001e:
	{
		return G_B3_0;
	}
}
// UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_mesh_m977520135 (MeshFilter_t3026937449 * __this, const MethodInfo* method)
{
	typedef Mesh_t1356156583 * (*MeshFilter_get_mesh_m977520135_ftn) (MeshFilter_t3026937449 *);
	static MeshFilter_get_mesh_m977520135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MeshFilter_get_mesh_m977520135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MeshFilter::get_mesh()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m2699265412(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
extern "C"  void MonoBehaviour_Internal_CancelInvokeAll_m3154116776 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_CancelInvokeAll_m3154116776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
extern "C"  bool MonoBehaviour_Internal_IsInvokingAll_m3504849565 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Internal_IsInvokingAll_m3504849565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Internal_IsInvokingAll()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m666563676 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m666563676_ftn) (MonoBehaviour_t1158329972 *, String_t*, float);
	static MonoBehaviour_Invoke_m666563676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m666563676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1);
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m3468262484 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, float ___time1, float ___repeatRate2, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m3468262484_ftn) (MonoBehaviour_t1158329972 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m3468262484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m3468262484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName0, ___time1, ___repeatRate2);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern "C"  void MonoBehaviour_CancelInvoke_m744713777 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_Internal_CancelInvokeAll_m3154116776(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C"  void MonoBehaviour_CancelInvoke_m2508161963 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m2508161963_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_CancelInvoke_m2508161963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m2508161963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern "C"  bool MonoBehaviour_IsInvoking_m1469271462 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_IsInvoking_m1469271462_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_IsInvoking_m1469271462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_IsInvoking_m1469271462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::IsInvoking(System.String)");
	return _il2cpp_icall_func(__this, ___methodName0);
}
// System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern "C"  bool MonoBehaviour_IsInvoking_m345622956 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	{
		bool L_0 = MonoBehaviour_Internal_IsInvokingAll_m3504849565(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		Coroutine_t2299508840 * L_1 = MonoBehaviour_StartCoroutine_Auto_m1744905232(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_Auto_m1744905232 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_Auto_m1744905232_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_Auto_m1744905232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m1744905232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine0);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m296997955 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	typedef Coroutine_t2299508840 * (*MonoBehaviour_StartCoroutine_m296997955_ftn) (MonoBehaviour_t1158329972 *, String_t*, Il2CppObject *);
	static MonoBehaviour_StartCoroutine_m296997955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m296997955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName0, ___value1);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m1399371129 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_0;
		Coroutine_t2299508840 * L_2 = MonoBehaviour_StartCoroutine_m296997955(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m987450539 (MonoBehaviour_t1158329972 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m987450539_ftn) (MonoBehaviour_t1158329972 *, String_t*);
	static MonoBehaviour_StopCoroutine_m987450539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m987450539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutine_m1170478282 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___routine0;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m1668572632 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	{
		Coroutine_t2299508840 * L_0 = ___routine0;
		MonoBehaviour_StopCoroutine_Auto_m1923670638(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C"  void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826 (MonoBehaviour_t1158329972 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn) (MonoBehaviour_t1158329972 *, Il2CppObject *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m4046945826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_Auto_m1923670638 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn) (MonoBehaviour_t1158329972 *, Coroutine_t2299508840 *);
	static MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m1923670638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine0);
}
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1675795839 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopAllCoroutines_m1675795839_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_StopAllCoroutines_m1675795839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopAllCoroutines_m1675795839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopAllCoroutines()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t MonoBehaviour_print_m3437620292_MetadataUsageId;
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (MonoBehaviour_print_m3437620292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern "C"  bool MonoBehaviour_get_useGUILayout_m524237270 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method)
{
	typedef bool (*MonoBehaviour_get_useGUILayout_m524237270_ftn) (MonoBehaviour_t1158329972 *);
	static MonoBehaviour_get_useGUILayout_m524237270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_get_useGUILayout_m524237270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::get_useGUILayout()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m2666356651 (MonoBehaviour_t1158329972 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_set_useGUILayout_m2666356651_ftn) (MonoBehaviour_t1158329972 *, bool);
	static MonoBehaviour_set_useGUILayout_m2666356651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_set_useGUILayout_m2666356651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Motion::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Motion__ctor_m784531835_MetadataUsageId;
extern "C"  void Motion__ctor_m784531835 (Motion_t2415020824 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Motion__ctor_m784531835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_velocity_m2966493589 (NavMeshAgent_t2171372499 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		NavMeshAgent_INTERNAL_set_velocity_m987960335(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_velocity_m987960335 (NavMeshAgent_t2171372499 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*NavMeshAgent_INTERNAL_set_velocity_m987960335_ftn) (NavMeshAgent_t2171372499 *, Vector3_t2243707580 *);
	static NavMeshAgent_INTERNAL_set_velocity_m987960335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NavMeshAgent_INTERNAL_set_velocity_m987960335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)
extern "C"  int32_t Network_InitializeServer_m492610505 (Il2CppObject * __this /* static, unused */, int32_t ___connections0, int32_t ___listenPort1, bool ___useNat2, const MethodInfo* method)
{
	typedef int32_t (*Network_InitializeServer_m492610505_ftn) (int32_t, int32_t, bool);
	static Network_InitializeServer_m492610505_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_InitializeServer_m492610505_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::InitializeServer(System.Int32,System.Int32,System.Boolean)");
	return _il2cpp_icall_func(___connections0, ___listenPort1, ___useNat2);
}
// System.Void UnityEngine.Network::set_incomingPassword(System.String)
extern "C"  void Network_set_incomingPassword_m3038067325 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_incomingPassword_m3038067325_ftn) (String_t*);
	static Network_set_incomingPassword_m3038067325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_incomingPassword_m3038067325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_incomingPassword(System.String)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)
extern "C"  void Network_set_logLevel_m786315563 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_logLevel_m786315563_ftn) (int32_t);
	static Network_set_logLevel_m786315563_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_logLevel_m786315563_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_logLevel(UnityEngine.NetworkLogLevel)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::InitializeSecurity()
extern "C"  void Network_InitializeSecurity_m1229341969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Network_InitializeSecurity_m1229341969_ftn) ();
	static Network_InitializeSecurity_m1229341969_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_InitializeSecurity_m1229341969_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::InitializeSecurity()");
	_il2cpp_icall_func();
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)
extern "C"  int32_t Network_Internal_ConnectToSingleIP_m1433145057 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, int32_t ___localPort2, String_t* ___password3, const MethodInfo* method)
{
	typedef int32_t (*Network_Internal_ConnectToSingleIP_m1433145057_ftn) (String_t*, int32_t, int32_t, String_t*);
	static Network_Internal_ConnectToSingleIP_m1433145057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_ConnectToSingleIP_m1433145057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_ConnectToSingleIP(System.String,System.Int32,System.Int32,System.String)");
	return _il2cpp_icall_func(___IP0, ___remotePort1, ___localPort2, ___password3);
}
// UnityEngine.NetworkConnectionError UnityEngine.Network::Connect(System.String,System.Int32,System.String)
extern "C"  int32_t Network_Connect_m294459100 (Il2CppObject * __this /* static, unused */, String_t* ___IP0, int32_t ___remotePort1, String_t* ___password2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___IP0;
		int32_t L_1 = ___remotePort1;
		String_t* L_2 = ___password2;
		int32_t L_3 = Network_Internal_ConnectToSingleIP_m1433145057(NULL /*static, unused*/, L_0, L_1, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Network::Disconnect(System.Int32)
extern "C"  void Network_Disconnect_m2376044840 (Il2CppObject * __this /* static, unused */, int32_t ___timeout0, const MethodInfo* method)
{
	typedef void (*Network_Disconnect_m2376044840_ftn) (int32_t);
	static Network_Disconnect_m2376044840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Disconnect_m2376044840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Disconnect(System.Int32)");
	_il2cpp_icall_func(___timeout0);
}
// System.Void UnityEngine.Network::Disconnect()
extern "C"  void Network_Disconnect_m607059163 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)200);
		int32_t L_0 = V_0;
		Network_Disconnect_m2376044840(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)
extern "C"  void Network_CloseConnection_m2751000432 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___target0, bool ___sendDisconnectionNotification1, const MethodInfo* method)
{
	typedef void (*Network_CloseConnection_m2751000432_ftn) (NetworkPlayer_t1243528291 , bool);
	static Network_CloseConnection_m2751000432_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_CloseConnection_m2751000432_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::CloseConnection(UnityEngine.NetworkPlayer,System.Boolean)");
	_il2cpp_icall_func(___target0, ___sendDisconnectionNotification1);
}
// UnityEngine.NetworkPlayer[] UnityEngine.Network::get_connections()
extern "C"  NetworkPlayerU5BU5D_t2821705394* Network_get_connections_m538736002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef NetworkPlayerU5BU5D_t2821705394* (*Network_get_connections_m538736002_ftn) ();
	static Network_get_connections_m538736002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_connections_m538736002_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_connections()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Network::Internal_GetPlayer()
extern "C"  int32_t Network_Internal_GetPlayer_m2085539076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_Internal_GetPlayer_m2085539076_ftn) ();
	static Network_Internal_GetPlayer_m2085539076_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_GetPlayer_m2085539076_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_GetPlayer()");
	return _il2cpp_icall_func();
}
// UnityEngine.NetworkPlayer UnityEngine.Network::get_player()
extern "C"  NetworkPlayer_t1243528291  Network_get_player_m2218319246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Network_Internal_GetPlayer_m2085539076(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_index_0(L_0);
		NetworkPlayer_t1243528291  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Network::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  Object_t1021602117 * Network_Instantiate_m1019383205 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___prefab0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, int32_t ___group3, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = ___prefab0;
		int32_t L_1 = ___group3;
		Object_t1021602117 * L_2 = Network_INTERNAL_CALL_Instantiate_m1722130588(NULL /*static, unused*/, L_0, (&___position1), (&___rotation2), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C"  Object_t1021602117 * Network_INTERNAL_CALL_Instantiate_m1722130588 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___prefab0, Vector3_t2243707580 * ___position1, Quaternion_t4030073918 * ___rotation2, int32_t ___group3, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Network_INTERNAL_CALL_Instantiate_m1722130588_ftn) (Object_t1021602117 *, Vector3_t2243707580 *, Quaternion_t4030073918 *, int32_t);
	static Network_INTERNAL_CALL_Instantiate_m1722130588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_INTERNAL_CALL_Instantiate_m1722130588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::INTERNAL_CALL_Instantiate(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)");
	return _il2cpp_icall_func(___prefab0, ___position1, ___rotation2, ___group3);
}
// System.Void UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)
extern "C"  void Network_DestroyPlayerObjects_m2401047830 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, const MethodInfo* method)
{
	typedef void (*Network_DestroyPlayerObjects_m2401047830_ftn) (NetworkPlayer_t1243528291 );
	static Network_DestroyPlayerObjects_m2401047830_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_DestroyPlayerObjects_m2401047830_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::DestroyPlayerObjects(UnityEngine.NetworkPlayer)");
	_il2cpp_icall_func(___playerID0);
}
// System.Void UnityEngine.Network::Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID,System.UInt32)
extern "C"  void Network_Internal_RemoveRPCs_m708688402 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, NetworkViewID_t3942988548  ___viewID1, uint32_t ___channelMask2, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291  L_0 = ___playerID0;
		uint32_t L_1 = ___channelMask2;
		Network_INTERNAL_CALL_Internal_RemoveRPCs_m2146465587(NULL /*static, unused*/, L_0, (&___viewID1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)
extern "C"  void Network_INTERNAL_CALL_Internal_RemoveRPCs_m2146465587 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, NetworkViewID_t3942988548 * ___viewID1, uint32_t ___channelMask2, const MethodInfo* method)
{
	typedef void (*Network_INTERNAL_CALL_Internal_RemoveRPCs_m2146465587_ftn) (NetworkPlayer_t1243528291 , NetworkViewID_t3942988548 *, uint32_t);
	static Network_INTERNAL_CALL_Internal_RemoveRPCs_m2146465587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_INTERNAL_CALL_Internal_RemoveRPCs_m2146465587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::INTERNAL_CALL_Internal_RemoveRPCs(UnityEngine.NetworkPlayer,UnityEngine.NetworkViewID&,System.UInt32)");
	_il2cpp_icall_func(___playerID0, ___viewID1, ___channelMask2);
}
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkPlayer)
extern "C"  void Network_RemoveRPCs_m636089305 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___playerID0, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291  L_0 = ___playerID0;
		NetworkViewID_t3942988548  L_1 = NetworkViewID_get_unassigned_m2814913999(NULL /*static, unused*/, /*hidden argument*/NULL);
		Network_Internal_RemoveRPCs_m708688402(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Network::RemoveRPCs(UnityEngine.NetworkViewID)
extern "C"  void Network_RemoveRPCs_m2643009806 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___viewID0, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291  L_0 = NetworkPlayer_get_unassigned_m3961648431(NULL /*static, unused*/, /*hidden argument*/NULL);
		NetworkViewID_t3942988548  L_1 = ___viewID0;
		Network_Internal_RemoveRPCs_m708688402(NULL /*static, unused*/, L_0, L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Network::get_isClient()
extern "C"  bool Network_get_isClient_m3471532805 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isClient_m3471532805_ftn) ();
	static Network_get_isClient_m3471532805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isClient_m3471532805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isClient()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Network::get_isServer()
extern "C"  bool Network_get_isServer_m4174143497 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isServer_m4174143497_ftn) ();
	static Network_get_isServer_m4174143497_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isServer_m4174143497_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isServer()");
	return _il2cpp_icall_func();
}
// UnityEngine.NetworkPeerType UnityEngine.Network::get_peerType()
extern "C"  int32_t Network_get_peerType_m1422231822 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_peerType_m1422231822_ftn) ();
	static Network_get_peerType_m1422231822_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_peerType_m1422231822_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_peerType()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::SetLevelPrefix(System.Int32)
extern "C"  void Network_SetLevelPrefix_m387506644 (Il2CppObject * __this /* static, unused */, int32_t ___prefix0, const MethodInfo* method)
{
	typedef void (*Network_SetLevelPrefix_m387506644_ftn) (int32_t);
	static Network_SetLevelPrefix_m387506644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_SetLevelPrefix_m387506644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::SetLevelPrefix(System.Int32)");
	_il2cpp_icall_func(___prefix0);
}
// System.Int32 UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetLastPing_m350006847 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method)
{
	typedef int32_t (*Network_GetLastPing_m350006847_ftn) (NetworkPlayer_t1243528291 );
	static Network_GetLastPing_m350006847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_GetLastPing_m350006847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::GetLastPing(UnityEngine.NetworkPlayer)");
	return _il2cpp_icall_func(___player0);
}
// System.Int32 UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)
extern "C"  int32_t Network_GetAveragePing_m2710622608 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method)
{
	typedef int32_t (*Network_GetAveragePing_m2710622608_ftn) (NetworkPlayer_t1243528291 );
	static Network_GetAveragePing_m2710622608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_GetAveragePing_m2710622608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::GetAveragePing(UnityEngine.NetworkPlayer)");
	return _il2cpp_icall_func(___player0);
}
// System.Single UnityEngine.Network::get_sendRate()
extern "C"  float Network_get_sendRate_m2240556848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Network_get_sendRate_m2240556848_ftn) ();
	static Network_get_sendRate_m2240556848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_sendRate_m2240556848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_sendRate()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_sendRate(System.Single)
extern "C"  void Network_set_sendRate_m2532595313 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_sendRate_m2532595313_ftn) (float);
	static Network_set_sendRate_m2532595313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_sendRate_m2532595313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_sendRate(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Network::get_isMessageQueueRunning()
extern "C"  bool Network_get_isMessageQueueRunning_m3067227829 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_get_isMessageQueueRunning_m3067227829_ftn) ();
	static Network_get_isMessageQueueRunning_m3067227829_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_isMessageQueueRunning_m3067227829_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_isMessageQueueRunning()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)
extern "C"  void Network_set_isMessageQueueRunning_m298779940 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_isMessageQueueRunning_m298779940_ftn) (bool);
	static Network_set_isMessageQueueRunning_m298779940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_isMessageQueueRunning_m298779940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_isMessageQueueRunning(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Network::Internal_GetTime(System.Double&)
extern "C"  void Network_Internal_GetTime_m2548642614 (Il2CppObject * __this /* static, unused */, double* ___t0, const MethodInfo* method)
{
	typedef void (*Network_Internal_GetTime_m2548642614_ftn) (double*);
	static Network_Internal_GetTime_m2548642614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_Internal_GetTime_m2548642614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::Internal_GetTime(System.Double&)");
	_il2cpp_icall_func(___t0);
}
// System.Double UnityEngine.Network::get_time()
extern "C"  double Network_get_time_m955449524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	double V_0 = 0.0;
	{
		Network_Internal_GetTime_m2548642614(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		double L_0 = V_0;
		return L_0;
	}
}
// System.Int32 UnityEngine.Network::get_minimumAllocatableViewIDs()
extern "C"  int32_t Network_get_minimumAllocatableViewIDs_m1995281259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_minimumAllocatableViewIDs_m1995281259_ftn) ();
	static Network_get_minimumAllocatableViewIDs_m1995281259_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_minimumAllocatableViewIDs_m1995281259_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_minimumAllocatableViewIDs()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)
extern "C"  void Network_set_minimumAllocatableViewIDs_m832310412 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_minimumAllocatableViewIDs_m832310412_ftn) (int32_t);
	static Network_set_minimumAllocatableViewIDs_m832310412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_minimumAllocatableViewIDs_m832310412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_minimumAllocatableViewIDs(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Network::HavePublicAddress()
extern "C"  bool Network_HavePublicAddress_m2110731574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Network_HavePublicAddress_m2110731574_ftn) ();
	static Network_HavePublicAddress_m2110731574_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_HavePublicAddress_m2110731574_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::HavePublicAddress()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Network::get_maxConnections()
extern "C"  int32_t Network_get_maxConnections_m2063963077 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Network_get_maxConnections_m2063963077_ftn) ();
	static Network_get_maxConnections_m2063963077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_get_maxConnections_m2063963077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::get_maxConnections()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Network::set_maxConnections(System.Int32)
extern "C"  void Network_set_maxConnections_m3373629084 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Network_set_maxConnections_m3373629084_ftn) (int32_t);
	static Network_set_maxConnections_m3373629084_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Network_set_maxConnections_m3373629084_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Network::set_maxConnections(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Networking.DownloadHandler::.ctor()
extern "C"  void DownloadHandler__ctor_m328121149 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateBuffer()
extern "C"  void DownloadHandler_InternalCreateBuffer_m3933395410 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateBuffer_m3933395410_ftn) (DownloadHandler_t1216180266 *);
	static DownloadHandler_InternalCreateBuffer_m3933395410_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateBuffer_m3933395410_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateBuffer()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateTexture(System.Boolean)
extern "C"  void DownloadHandler_InternalCreateTexture_m491895728 (DownloadHandler_t1216180266 * __this, bool ___readable0, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateTexture_m491895728_ftn) (DownloadHandler_t1216180266 *, bool);
	static DownloadHandler_InternalCreateTexture_m491895728_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateTexture_m491895728_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateTexture(System.Boolean)");
	_il2cpp_icall_func(__this, ___readable0);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateAssetBundle(System.String,System.UInt32)
extern "C"  void DownloadHandler_InternalCreateAssetBundle_m2686838992 (DownloadHandler_t1216180266 * __this, String_t* ___url0, uint32_t ___crc1, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateAssetBundle_m2686838992_ftn) (DownloadHandler_t1216180266 *, String_t*, uint32_t);
	static DownloadHandler_InternalCreateAssetBundle_m2686838992_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateAssetBundle_m2686838992_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateAssetBundle(System.String,System.UInt32)");
	_il2cpp_icall_func(__this, ___url0, ___crc1);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateAssetBundle(System.String,UnityEngine.Hash128,System.UInt32)
extern "C"  void DownloadHandler_InternalCreateAssetBundle_m2870018240 (DownloadHandler_t1216180266 * __this, String_t* ___url0, Hash128_t2836532937  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url0;
		uint32_t L_1 = ___crc2;
		DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m2741800430(NULL /*static, unused*/, __this, L_0, (&___hash1), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::INTERNAL_CALL_InternalCreateAssetBundle(UnityEngine.Networking.DownloadHandler,System.String,UnityEngine.Hash128&,System.UInt32)
extern "C"  void DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m2741800430 (Il2CppObject * __this /* static, unused */, DownloadHandler_t1216180266 * ___self0, String_t* ___url1, Hash128_t2836532937 * ___hash2, uint32_t ___crc3, const MethodInfo* method)
{
	typedef void (*DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m2741800430_ftn) (DownloadHandler_t1216180266 *, String_t*, Hash128_t2836532937 *, uint32_t);
	static DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m2741800430_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_INTERNAL_CALL_InternalCreateAssetBundle_m2741800430_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::INTERNAL_CALL_InternalCreateAssetBundle(UnityEngine.Networking.DownloadHandler,System.String,UnityEngine.Hash128&,System.UInt32)");
	_il2cpp_icall_func(___self0, ___url1, ___hash2, ___crc3);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalCreateAudioClip(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandler_InternalCreateAudioClip_m86520921 (DownloadHandler_t1216180266 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalCreateAudioClip_m86520921_ftn) (DownloadHandler_t1216180266 *, String_t*, int32_t);
	static DownloadHandler_InternalCreateAudioClip_m86520921_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalCreateAudioClip_m86520921_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalCreateAudioClip(System.String,UnityEngine.AudioType)");
	_il2cpp_icall_func(__this, ___url0, ___audioType1);
}
// System.Void UnityEngine.Networking.DownloadHandler::InternalDestroy()
extern "C"  void DownloadHandler_InternalDestroy_m761762988 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	typedef void (*DownloadHandler_InternalDestroy_m761762988_ftn) (DownloadHandler_t1216180266 *);
	static DownloadHandler_InternalDestroy_m761762988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DownloadHandler_InternalDestroy_m761762988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.DownloadHandler::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.DownloadHandler::Finalize()
extern "C"  void DownloadHandler_Finalize_m3781185347 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DownloadHandler_InternalDestroy_m761762988(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandler::Dispose()
extern "C"  void DownloadHandler_Dispose_m2043077428 (DownloadHandler_t1216180266 * __this, const MethodInfo* method)
{
	{
		DownloadHandler_InternalDestroy_m761762988(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke(const DownloadHandler_t1216180266& unmarshaled, DownloadHandler_t1216180266_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_back(const DownloadHandler_t1216180266_marshaled_pinvoke& marshaled, DownloadHandler_t1216180266& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_pinvoke_cleanup(DownloadHandler_t1216180266_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_com(const DownloadHandler_t1216180266& unmarshaled, DownloadHandler_t1216180266_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandler_t1216180266_marshal_com_back(const DownloadHandler_t1216180266_marshaled_com& marshaled, DownloadHandler_t1216180266& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandler
extern "C" void DownloadHandler_t1216180266_marshal_com_cleanup(DownloadHandler_t1216180266_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32)
extern "C"  void DownloadHandlerAssetBundle__ctor_m2066903751 (DownloadHandlerAssetBundle_t1224150142 * __this, String_t* ___url0, uint32_t ___crc1, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		uint32_t L_1 = ___crc1;
		DownloadHandler_InternalCreateAssetBundle_m2686838992(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,System.UInt32,System.UInt32)
extern "C"  void DownloadHandlerAssetBundle__ctor_m626379351 (DownloadHandlerAssetBundle_t1224150142 * __this, String_t* ___url0, uint32_t ___version1, uint32_t ___crc2, const MethodInfo* method)
{
	Hash128_t2836532937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		uint32_t L_0 = ___version1;
		Hash128__ctor_m3123900356((&V_0), 0, 0, 0, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___url0;
		Hash128_t2836532937  L_2 = V_0;
		uint32_t L_3 = ___crc2;
		DownloadHandler_InternalCreateAssetBundle_m2870018240(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.DownloadHandlerAssetBundle::.ctor(System.String,UnityEngine.Hash128,System.UInt32)
extern "C"  void DownloadHandlerAssetBundle__ctor_m3244180881 (DownloadHandlerAssetBundle_t1224150142 * __this, String_t* ___url0, Hash128_t2836532937  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		Hash128_t2836532937  L_1 = ___hash1;
		uint32_t L_2 = ___crc2;
		DownloadHandler_InternalCreateAssetBundle_m2870018240(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t1224150142_marshal_pinvoke(const DownloadHandlerAssetBundle_t1224150142& unmarshaled, DownloadHandlerAssetBundle_t1224150142_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAssetBundle_t1224150142_marshal_pinvoke_back(const DownloadHandlerAssetBundle_t1224150142_marshaled_pinvoke& marshaled, DownloadHandlerAssetBundle_t1224150142& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t1224150142_marshal_pinvoke_cleanup(DownloadHandlerAssetBundle_t1224150142_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t1224150142_marshal_com(const DownloadHandlerAssetBundle_t1224150142& unmarshaled, DownloadHandlerAssetBundle_t1224150142_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAssetBundle_t1224150142_marshal_com_back(const DownloadHandlerAssetBundle_t1224150142_marshaled_com& marshaled, DownloadHandlerAssetBundle_t1224150142& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAssetBundle
extern "C" void DownloadHandlerAssetBundle_t1224150142_marshal_com_cleanup(DownloadHandlerAssetBundle_t1224150142_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerAudioClip::.ctor(System.String,UnityEngine.AudioType)
extern "C"  void DownloadHandlerAudioClip__ctor_m1779320660 (DownloadHandlerAudioClip_t1520641178 * __this, String_t* ___url0, int32_t ___audioType1, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		int32_t L_1 = ___audioType1;
		DownloadHandler_InternalCreateAudioClip_m86520921(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke(const DownloadHandlerAudioClip_t1520641178& unmarshaled, DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_back(const DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled, DownloadHandlerAudioClip_t1520641178& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_pinvoke_cleanup(DownloadHandlerAudioClip_t1520641178_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com(const DownloadHandlerAudioClip_t1520641178& unmarshaled, DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com_back(const DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled, DownloadHandlerAudioClip_t1520641178& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerAudioClip
extern "C" void DownloadHandlerAudioClip_t1520641178_marshal_com_cleanup(DownloadHandlerAudioClip_t1520641178_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
extern "C"  void DownloadHandlerBuffer__ctor_m1363181913 (DownloadHandlerBuffer_t3443159558 * __this, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		DownloadHandler_InternalCreateBuffer_m3933395410(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke(const DownloadHandlerBuffer_t3443159558& unmarshaled, DownloadHandlerBuffer_t3443159558_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke_back(const DownloadHandlerBuffer_t3443159558_marshaled_pinvoke& marshaled, DownloadHandlerBuffer_t3443159558& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_pinvoke_cleanup(DownloadHandlerBuffer_t3443159558_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_com(const DownloadHandlerBuffer_t3443159558& unmarshaled, DownloadHandlerBuffer_t3443159558_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_com_back(const DownloadHandlerBuffer_t3443159558_marshaled_com& marshaled, DownloadHandlerBuffer_t3443159558& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerBuffer
extern "C" void DownloadHandlerBuffer_t3443159558_marshal_com_cleanup(DownloadHandlerBuffer_t3443159558_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.DownloadHandlerTexture::.ctor(System.Boolean)
extern "C"  void DownloadHandlerTexture__ctor_m2962555213 (DownloadHandlerTexture_t2365358851 * __this, bool ___readable0, const MethodInfo* method)
{
	{
		DownloadHandler__ctor_m328121149(__this, /*hidden argument*/NULL);
		bool L_0 = ___readable0;
		DownloadHandler_InternalCreateTexture_m491895728(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2365358851_marshal_pinvoke(const DownloadHandlerTexture_t2365358851& unmarshaled, DownloadHandlerTexture_t2365358851_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerTexture_t2365358851_marshal_pinvoke_back(const DownloadHandlerTexture_t2365358851_marshaled_pinvoke& marshaled, DownloadHandlerTexture_t2365358851& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2365358851_marshal_pinvoke_cleanup(DownloadHandlerTexture_t2365358851_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2365358851_marshal_com(const DownloadHandlerTexture_t2365358851& unmarshaled, DownloadHandlerTexture_t2365358851_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void DownloadHandlerTexture_t2365358851_marshal_com_back(const DownloadHandlerTexture_t2365358851_marshaled_com& marshaled, DownloadHandlerTexture_t2365358851& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.DownloadHandlerTexture
extern "C" void DownloadHandlerTexture_t2365358851_marshal_com_cleanup(DownloadHandlerTexture_t2365358851_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor()
extern "C"  void UnityWebRequest__ctor_m432596795 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2618341326(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m1411946726(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String)
extern "C"  void UnityWebRequest__ctor_m2424401853 (UnityWebRequest_t254341728 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2618341326(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m1411946726(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_set_url_m2494687159(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
extern "C"  void UnityWebRequest__ctor_m1187779891 (UnityWebRequest_t254341728 * __this, String_t* ___url0, String_t* ___method1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2618341326(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m1411946726(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_set_url_m2494687159(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___method1;
		UnityWebRequest_set_method_m4118764027(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String,UnityEngine.Networking.DownloadHandler,UnityEngine.Networking.UploadHandler)
extern "C"  void UnityWebRequest__ctor_m2378599348 (UnityWebRequest_t254341728 * __this, String_t* ___url0, String_t* ___method1, DownloadHandler_t1216180266 * ___downloadHandler2, UploadHandler_t3552561393 * ___uploadHandler3, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalCreate_m2618341326(__this, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetDefaults_m1411946726(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_set_url_m2494687159(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___method1;
		UnityWebRequest_set_method_m4118764027(__this, L_1, /*hidden argument*/NULL);
		DownloadHandler_t1216180266 * L_2 = ___downloadHandler2;
		UnityWebRequest_set_downloadHandler_m1587492897(__this, L_2, /*hidden argument*/NULL);
		UploadHandler_t3552561393 * L_3 = ___uploadHandler3;
		UnityWebRequest_set_uploadHandler_m2033861625(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::.cctor()
extern Il2CppClass* Regex_t1803876613_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2697683047;
extern Il2CppCodeGenString* _stringLiteral925218559;
extern Il2CppCodeGenString* _stringLiteral2082586684;
extern Il2CppCodeGenString* _stringLiteral3962854787;
extern Il2CppCodeGenString* _stringLiteral1093983178;
extern Il2CppCodeGenString* _stringLiteral172550198;
extern Il2CppCodeGenString* _stringLiteral435779042;
extern Il2CppCodeGenString* _stringLiteral2328218522;
extern Il2CppCodeGenString* _stringLiteral3068682156;
extern Il2CppCodeGenString* _stringLiteral1991988847;
extern Il2CppCodeGenString* _stringLiteral3430667732;
extern Il2CppCodeGenString* _stringLiteral4045715149;
extern Il2CppCodeGenString* _stringLiteral3886432928;
extern Il2CppCodeGenString* _stringLiteral3276845361;
extern Il2CppCodeGenString* _stringLiteral3015944849;
extern Il2CppCodeGenString* _stringLiteral1203931413;
extern Il2CppCodeGenString* _stringLiteral2170876777;
extern Il2CppCodeGenString* _stringLiteral3607284354;
extern Il2CppCodeGenString* _stringLiteral1094988949;
extern Il2CppCodeGenString* _stringLiteral339799502;
extern Il2CppCodeGenString* _stringLiteral3282611705;
extern const uint32_t UnityWebRequest__cctor_m126744318_MetadataUsageId;
extern "C"  void UnityWebRequest__cctor_m126744318 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest__cctor_m126744318_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Regex_t1803876613 * L_0 = (Regex_t1803876613 *)il2cpp_codegen_object_new(Regex_t1803876613_il2cpp_TypeInfo_var);
		Regex__ctor_m2794328522(L_0, _stringLiteral2697683047, /*hidden argument*/NULL);
		((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->set_domainRegex_7(L_0);
		StringU5BU5D_t1642385972* L_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)((int32_t)20)));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral925218559);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral925218559);
		StringU5BU5D_t1642385972* L_2 = L_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, _stringLiteral2082586684);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2082586684);
		StringU5BU5D_t1642385972* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral3962854787);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3962854787);
		StringU5BU5D_t1642385972* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, _stringLiteral1093983178);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral1093983178);
		StringU5BU5D_t1642385972* L_5 = L_4;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 4);
		ArrayElementTypeCheck (L_5, _stringLiteral172550198);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral172550198);
		StringU5BU5D_t1642385972* L_6 = L_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 5);
		ArrayElementTypeCheck (L_6, _stringLiteral435779042);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral435779042);
		StringU5BU5D_t1642385972* L_7 = L_6;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 6);
		ArrayElementTypeCheck (L_7, _stringLiteral2328218522);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral2328218522);
		StringU5BU5D_t1642385972* L_8 = L_7;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 7);
		ArrayElementTypeCheck (L_8, _stringLiteral3068682156);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral3068682156);
		StringU5BU5D_t1642385972* L_9 = L_8;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 8);
		ArrayElementTypeCheck (L_9, _stringLiteral1991988847);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral1991988847);
		StringU5BU5D_t1642385972* L_10 = L_9;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)9));
		ArrayElementTypeCheck (L_10, _stringLiteral3430667732);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral3430667732);
		StringU5BU5D_t1642385972* L_11 = L_10;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)10));
		ArrayElementTypeCheck (L_11, _stringLiteral4045715149);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral4045715149);
		StringU5BU5D_t1642385972* L_12 = L_11;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)11));
		ArrayElementTypeCheck (L_12, _stringLiteral3886432928);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral3886432928);
		StringU5BU5D_t1642385972* L_13 = L_12;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)12));
		ArrayElementTypeCheck (L_13, _stringLiteral3276845361);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral3276845361);
		StringU5BU5D_t1642385972* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)13));
		ArrayElementTypeCheck (L_14, _stringLiteral3015944849);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral3015944849);
		StringU5BU5D_t1642385972* L_15 = L_14;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)14));
		ArrayElementTypeCheck (L_15, _stringLiteral1203931413);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral1203931413);
		StringU5BU5D_t1642385972* L_16 = L_15;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)15));
		ArrayElementTypeCheck (L_16, _stringLiteral2170876777);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral2170876777);
		StringU5BU5D_t1642385972* L_17 = L_16;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)16));
		ArrayElementTypeCheck (L_17, _stringLiteral3607284354);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral3607284354);
		StringU5BU5D_t1642385972* L_18 = L_17;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)17));
		ArrayElementTypeCheck (L_18, _stringLiteral1094988949);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral1094988949);
		StringU5BU5D_t1642385972* L_19 = L_18;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, ((int32_t)18));
		ArrayElementTypeCheck (L_19, _stringLiteral339799502);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral339799502);
		StringU5BU5D_t1642385972* L_20 = L_19;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)19));
		ArrayElementTypeCheck (L_20, _stringLiteral3282611705);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral3282611705);
		((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->set_forbiddenHeaderKeys_8(L_20);
		return;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern const uint32_t UnityWebRequest_Get_m3762133581_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Get_m3762133581 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Get_m3762133581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		DownloadHandlerBuffer_t3443159558 * L_1 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_1, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_2 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_2, L_0, _stringLiteral1596707798, L_1, (UploadHandler_t3552561393 *)NULL, /*hidden argument*/NULL);
		V_0 = L_2;
		UnityWebRequest_t254341728 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Delete(System.String)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1328429497;
extern const uint32_t UnityWebRequest_Delete_m943504872_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Delete_m943504872 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Delete_m943504872_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t254341728 * L_1 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m1187779891(L_1, L_0, _stringLiteral1328429497, /*hidden argument*/NULL);
		V_0 = L_1;
		UnityWebRequest_t254341728 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Head(System.String)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1488409984;
extern const uint32_t UnityWebRequest_Head_m1698896027_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Head_m1698896027 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Head_m1698896027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t254341728 * L_1 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m1187779891(L_1, L_0, _stringLiteral1488409984, /*hidden argument*/NULL);
		V_0 = L_1;
		UnityWebRequest_t254341728 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetTexture(System.String)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_GetTexture_m3150653648_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetTexture_m3150653648 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetTexture_m3150653648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest_t254341728 * L_1 = UnityWebRequest_GetTexture_m333882849(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetTexture(System.String,System.Boolean)
extern Il2CppClass* DownloadHandlerTexture_t2365358851_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern const uint32_t UnityWebRequest_GetTexture_m333882849_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetTexture_m333882849 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, bool ___nonReadable1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetTexture_m333882849_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		bool L_1 = ___nonReadable1;
		DownloadHandlerTexture_t2365358851 * L_2 = (DownloadHandlerTexture_t2365358851 *)il2cpp_codegen_object_new(DownloadHandlerTexture_t2365358851_il2cpp_TypeInfo_var);
		DownloadHandlerTexture__ctor_m2962555213(L_2, L_1, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_3 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_3, L_0, _stringLiteral1596707798, L_2, (UploadHandler_t3552561393 *)NULL, /*hidden argument*/NULL);
		V_0 = L_3;
		UnityWebRequest_t254341728 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAudioClip(System.String,UnityEngine.AudioType)
extern Il2CppClass* DownloadHandlerAudioClip_t1520641178_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern const uint32_t UnityWebRequest_GetAudioClip_m3990624924_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetAudioClip_m3990624924 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, int32_t ___audioType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAudioClip_m3990624924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		int32_t L_2 = ___audioType1;
		DownloadHandlerAudioClip_t1520641178 * L_3 = (DownloadHandlerAudioClip_t1520641178 *)il2cpp_codegen_object_new(DownloadHandlerAudioClip_t1520641178_il2cpp_TypeInfo_var);
		DownloadHandlerAudioClip__ctor_m1779320660(L_3, L_1, L_2, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_4 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_4, L_0, _stringLiteral1596707798, L_3, (UploadHandler_t3552561393 *)NULL, /*hidden argument*/NULL);
		V_0 = L_4;
		UnityWebRequest_t254341728 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_GetAssetBundle_m2918223789_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetAssetBundle_m2918223789 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m2918223789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___uri0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest_t254341728 * L_1 = UnityWebRequest_GetAssetBundle_m4116539413(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String,System.UInt32)
extern Il2CppClass* DownloadHandlerAssetBundle_t1224150142_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern const uint32_t UnityWebRequest_GetAssetBundle_m4116539413_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetAssetBundle_m4116539413 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, uint32_t ___crc1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m4116539413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		uint32_t L_2 = ___crc1;
		DownloadHandlerAssetBundle_t1224150142 * L_3 = (DownloadHandlerAssetBundle_t1224150142 *)il2cpp_codegen_object_new(DownloadHandlerAssetBundle_t1224150142_il2cpp_TypeInfo_var);
		DownloadHandlerAssetBundle__ctor_m2066903751(L_3, L_1, L_2, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_4 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_4, L_0, _stringLiteral1596707798, L_3, (UploadHandler_t3552561393 *)NULL, /*hidden argument*/NULL);
		V_0 = L_4;
		UnityWebRequest_t254341728 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String,System.UInt32,System.UInt32)
extern Il2CppClass* DownloadHandlerAssetBundle_t1224150142_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern const uint32_t UnityWebRequest_GetAssetBundle_m4072853997_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetAssetBundle_m4072853997 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, uint32_t ___version1, uint32_t ___crc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m4072853997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		uint32_t L_2 = ___version1;
		uint32_t L_3 = ___crc2;
		DownloadHandlerAssetBundle_t1224150142 * L_4 = (DownloadHandlerAssetBundle_t1224150142 *)il2cpp_codegen_object_new(DownloadHandlerAssetBundle_t1224150142_il2cpp_TypeInfo_var);
		DownloadHandlerAssetBundle__ctor_m626379351(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_5 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_5, L_0, _stringLiteral1596707798, L_4, (UploadHandler_t3552561393 *)NULL, /*hidden argument*/NULL);
		V_0 = L_5;
		UnityWebRequest_t254341728 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::GetAssetBundle(System.String,UnityEngine.Hash128,System.UInt32)
extern Il2CppClass* DownloadHandlerAssetBundle_t1224150142_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern const uint32_t UnityWebRequest_GetAssetBundle_m361877199_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_GetAssetBundle_m361877199 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, Hash128_t2836532937  ___hash1, uint32_t ___crc2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetAssetBundle_m361877199_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		String_t* L_1 = ___uri0;
		Hash128_t2836532937  L_2 = ___hash1;
		uint32_t L_3 = ___crc2;
		DownloadHandlerAssetBundle_t1224150142 * L_4 = (DownloadHandlerAssetBundle_t1224150142 *)il2cpp_codegen_object_new(DownloadHandlerAssetBundle_t1224150142_il2cpp_TypeInfo_var);
		DownloadHandlerAssetBundle__ctor_m3244180881(L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_5 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_5, L_0, _stringLiteral1596707798, L_4, (UploadHandler_t3552561393 *)NULL, /*hidden argument*/NULL);
		V_0 = L_5;
		UnityWebRequest_t254341728 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Put(System.String,System.Byte[])
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884247665;
extern const uint32_t UnityWebRequest_Put_m1139563229_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Put_m1139563229 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, ByteU5BU5D_t3397334013* ___bodyData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Put_m1139563229_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		DownloadHandlerBuffer_t3443159558 * L_1 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_2 = ___bodyData1;
		UploadHandlerRaw_t3420491431 * L_3 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m2445981389(L_3, L_2, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_4 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_4, L_0, _stringLiteral884247665, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		UnityWebRequest_t254341728 * L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Put(System.String,System.String)
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884247665;
extern const uint32_t UnityWebRequest_Put_m1681506174_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Put_m1681506174 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, String_t* ___bodyData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Put_m1681506174_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	{
		String_t* L_0 = ___uri0;
		DownloadHandlerBuffer_t3443159558 * L_1 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_2 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ___bodyData1;
		NullCheck(L_2);
		ByteU5BU5D_t3397334013* L_4 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, L_3);
		UploadHandlerRaw_t3420491431 * L_5 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m2445981389(L_5, L_4, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_6 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m2378599348(L_6, L_0, _stringLiteral884247665, L_1, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		UnityWebRequest_t254341728 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.String)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* WWWTranscoder_t1124214756_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral730767724;
extern const uint32_t UnityWebRequest_Post_m3438227921_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Post_m3438227921 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, String_t* ___postData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m3438227921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t254341728 * L_1 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m1187779891(L_1, L_0, _stringLiteral782856060, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___postData1;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t1124214756_il2cpp_TypeInfo_var);
		String_t* L_4 = WWWTranscoder_URLEncode_m3846210256(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		UnityWebRequest_t254341728 * L_5 = V_0;
		Encoding_t663144255 * L_6 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = V_1;
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_8 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, L_7);
		UploadHandlerRaw_t3420491431 * L_9 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m2445981389(L_9, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityWebRequest_set_uploadHandler_m2033861625(L_5, L_9, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_10 = V_0;
		NullCheck(L_10);
		UploadHandler_t3552561393 * L_11 = UnityWebRequest_get_uploadHandler_m1968885984(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		UploadHandler_set_contentType_m2720939452(L_11, _stringLiteral730767724, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_12 = V_0;
		DownloadHandlerBuffer_t3443159558 * L_13 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		UnityWebRequest_set_downloadHandler_m1587492897(L_12, L_13, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_14 = V_0;
		return L_14;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t969056901_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern const uint32_t UnityWebRequest_Post_m1002780585_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Post_m1002780585 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, WWWForm_t3950226929 * ___formData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m1002780585_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	Dictionary_2_t3943999495 * V_1 = NULL;
	KeyValuePair_2_t1701344717  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t969056901  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t254341728 * L_1 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m1187779891(L_1, L_0, _stringLiteral782856060, /*hidden argument*/NULL);
		V_0 = L_1;
		UnityWebRequest_t254341728 * L_2 = V_0;
		WWWForm_t3950226929 * L_3 = ___formData1;
		NullCheck(L_3);
		ByteU5BU5D_t3397334013* L_4 = WWWForm_get_data_m1788094649(L_3, /*hidden argument*/NULL);
		UploadHandlerRaw_t3420491431 * L_5 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m2445981389(L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		UnityWebRequest_set_uploadHandler_m2033861625(L_2, L_5, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_6 = V_0;
		DownloadHandlerBuffer_t3443159558 * L_7 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityWebRequest_set_downloadHandler_m1587492897(L_6, L_7, /*hidden argument*/NULL);
		WWWForm_t3950226929 * L_8 = ___formData1;
		NullCheck(L_8);
		Dictionary_2_t3943999495 * L_9 = WWWForm_get_headers_m3744493569(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Dictionary_2_t3943999495 * L_10 = V_1;
		NullCheck(L_10);
		Enumerator_t969056901  L_11 = Dictionary_2_GetEnumerator_m2895728349(L_10, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
		V_3 = L_11;
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0057;
		}

IL_003b:
		{
			KeyValuePair_2_t1701344717  L_12 = Enumerator_get_Current_m1989408781((&V_3), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
			V_2 = L_12;
			UnityWebRequest_t254341728 * L_13 = V_0;
			String_t* L_14 = KeyValuePair_2_get_Key_m1372024679((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
			String_t* L_15 = KeyValuePair_2_get_Value_m1710042386((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
			NullCheck(L_13);
			UnityWebRequest_SetRequestHeader_m466367223(L_13, L_14, L_15, /*hidden argument*/NULL);
		}

IL_0057:
		{
			bool L_16 = Enumerator_MoveNext_m4005245300((&V_3), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
			if (L_16)
			{
				goto IL_003b;
			}
		}

IL_0063:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		Enumerator_t969056901  L_17 = V_3;
		Enumerator_t969056901  L_18 = L_17;
		Il2CppObject * L_19 = Box(Enumerator_t969056901_il2cpp_TypeInfo_var, &L_18);
		NullCheck((Il2CppObject *)L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_19);
		IL2CPP_END_FINALLY(104)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0074:
	{
		UnityWebRequest_t254341728 * L_20 = V_0;
		return L_20;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_Post_m2105097678_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Post_m2105097678 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, List_1_t603865270 * ___multipartFormSections1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m2105097678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_0 = UnityWebRequest_GenerateBoundary_m1443103734(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___uri0;
		List_1_t603865270 * L_2 = ___multipartFormSections1;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		UnityWebRequest_t254341728 * L_4 = UnityWebRequest_Post_m2626544445(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral2996724324;
extern const uint32_t UnityWebRequest_Post_m2626544445_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Post_m2626544445 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, List_1_t603865270 * ___multipartFormSections1, ByteU5BU5D_t3397334013* ___boundary2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m2626544445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	UploadHandler_t3552561393 * V_2 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t254341728 * L_1 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m1187779891(L_1, L_0, _stringLiteral782856060, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t603865270 * L_2 = ___multipartFormSections1;
		ByteU5BU5D_t3397334013* L_3 = ___boundary2;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_4 = UnityWebRequest_SerializeFormSections_m616620563(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		ByteU5BU5D_t3397334013* L_5 = V_1;
		UploadHandlerRaw_t3420491431 * L_6 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m2445981389(L_6, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		UploadHandler_t3552561393 * L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_8 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_9 = ___boundary2;
		ByteU5BU5D_t3397334013* L_10 = ___boundary2;
		NullCheck(L_10);
		NullCheck(L_8);
		String_t* L_11 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_8, L_9, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2996724324, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		UploadHandler_set_contentType_m2720939452(L_7, L_12, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_13 = V_0;
		UploadHandler_t3552561393 * L_14 = V_2;
		NullCheck(L_13);
		UnityWebRequest_set_uploadHandler_m2033861625(L_13, L_14, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_15 = V_0;
		DownloadHandlerBuffer_t3443159558 * L_16 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		UnityWebRequest_set_downloadHandler_m1587492897(L_15, L_16, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern Il2CppClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral730767724;
extern const uint32_t UnityWebRequest_Post_m2879499414_MetadataUsageId;
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Post_m2879499414 (Il2CppObject * __this /* static, unused */, String_t* ___uri0, Dictionary_2_t3943999495 * ___formFields1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_Post_m2879499414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityWebRequest_t254341728 * V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	UploadHandler_t3552561393 * V_2 = NULL;
	{
		String_t* L_0 = ___uri0;
		UnityWebRequest_t254341728 * L_1 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m1187779891(L_1, L_0, _stringLiteral782856060, /*hidden argument*/NULL);
		V_0 = L_1;
		Dictionary_2_t3943999495 * L_2 = ___formFields1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_3 = UnityWebRequest_SerializeSimpleForm_m1079282452(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		ByteU5BU5D_t3397334013* L_4 = V_1;
		UploadHandlerRaw_t3420491431 * L_5 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m2445981389(L_5, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		UploadHandler_t3552561393 * L_6 = V_2;
		NullCheck(L_6);
		UploadHandler_set_contentType_m2720939452(L_6, _stringLiteral730767724, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_7 = V_0;
		UploadHandler_t3552561393 * L_8 = V_2;
		NullCheck(L_7);
		UnityWebRequest_set_uploadHandler_m2033861625(L_7, L_8, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_9 = V_0;
		DownloadHandlerBuffer_t3443159558 * L_10 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m1363181913(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityWebRequest_set_downloadHandler_m1587492897(L_9, L_10, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_11 = V_0;
		return L_11;
	}
}
// System.Byte[] UnityEngine.Networking.UnityWebRequest::SerializeFormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>,System.Byte[])
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* IMultipartFormSection_t1234744138_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t138594944_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3052225568_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m301336464_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2185366972_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4256584366_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2367726798_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m3872295241_MethodInfo_var;
extern const MethodInfo* List_1_TrimExcess_m505787576_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2815648607_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2162321587;
extern Il2CppCodeGenString* _stringLiteral2476186031;
extern Il2CppCodeGenString* _stringLiteral3457518716;
extern Il2CppCodeGenString* _stringLiteral603199997;
extern Il2CppCodeGenString* _stringLiteral1089396559;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral4071688053;
extern Il2CppCodeGenString* _stringLiteral1676307500;
extern const uint32_t UnityWebRequest_SerializeFormSections_m616620563_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* UnityWebRequest_SerializeFormSections_m616620563 (Il2CppObject * __this /* static, unused */, List_1_t603865270 * ___multipartFormSections0, ByteU5BU5D_t3397334013* ___boundary1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_SerializeFormSections_m616620563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	Enumerator_t138594944  V_3;
	memset(&V_3, 0, sizeof(V_3));
	List_1_t3052225568 * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	Enumerator_t138594944  V_6;
	memset(&V_6, 0, sizeof(V_6));
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_0 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t3397334013* L_1 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral2162321587);
		V_0 = L_1;
		V_1 = 0;
		List_1_t603865270 * L_2 = ___multipartFormSections0;
		NullCheck(L_2);
		Enumerator_t138594944  L_3 = List_1_GetEnumerator_m301336464(L_2, /*hidden argument*/List_1_GetEnumerator_m301336464_MethodInfo_var);
		V_3 = L_3;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_001e:
		{
			Il2CppObject * L_4 = Enumerator_get_Current_m2185366972((&V_3), /*hidden argument*/Enumerator_get_Current_m2185366972_MethodInfo_var);
			V_2 = L_4;
			int32_t L_5 = V_1;
			Il2CppObject * L_6 = V_2;
			NullCheck(L_6);
			ByteU5BU5D_t3397334013* L_7 = InterfaceFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(1 /* System.Byte[] UnityEngine.Networking.IMultipartFormSection::get_sectionData() */, IMultipartFormSection_t1234744138_il2cpp_TypeInfo_var, L_6);
			NullCheck(L_7);
			V_1 = ((int32_t)((int32_t)L_5+(int32_t)((int32_t)((int32_t)((int32_t)64)+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))))))));
		}

IL_0034:
		{
			bool L_8 = Enumerator_MoveNext_m4256584366((&V_3), /*hidden argument*/Enumerator_MoveNext_m4256584366_MethodInfo_var);
			if (L_8)
			{
				goto IL_001e;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Enumerator_t138594944  L_9 = V_3;
		Enumerator_t138594944  L_10 = L_9;
		Il2CppObject * L_11 = Box(Enumerator_t138594944_il2cpp_TypeInfo_var, &L_10);
		NullCheck((Il2CppObject *)L_11);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0051:
	{
		int32_t L_12 = V_1;
		List_1_t3052225568 * L_13 = (List_1_t3052225568 *)il2cpp_codegen_object_new(List_1_t3052225568_il2cpp_TypeInfo_var);
		List_1__ctor_m2367726798(L_13, L_12, /*hidden argument*/List_1__ctor_m2367726798_MethodInfo_var);
		V_4 = L_13;
		List_1_t603865270 * L_14 = ___multipartFormSections0;
		NullCheck(L_14);
		Enumerator_t138594944  L_15 = List_1_GetEnumerator_m301336464(L_14, /*hidden argument*/List_1_GetEnumerator_m301336464_MethodInfo_var);
		V_6 = L_15;
	}

IL_0061:
	try
	{ // begin try (depth: 1)
		{
			goto IL_015c;
		}

IL_0066:
		{
			Il2CppObject * L_16 = Enumerator_get_Current_m2185366972((&V_6), /*hidden argument*/Enumerator_get_Current_m2185366972_MethodInfo_var);
			V_5 = L_16;
			V_7 = _stringLiteral2476186031;
			Il2CppObject * L_17 = V_5;
			NullCheck(L_17);
			String_t* L_18 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.Networking.IMultipartFormSection::get_sectionName() */, IMultipartFormSection_t1234744138_il2cpp_TypeInfo_var, L_17);
			V_8 = L_18;
			Il2CppObject * L_19 = V_5;
			NullCheck(L_19);
			String_t* L_20 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String UnityEngine.Networking.IMultipartFormSection::get_fileName() */, IMultipartFormSection_t1234744138_il2cpp_TypeInfo_var, L_19);
			V_9 = L_20;
			String_t* L_21 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_22 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
			if (L_22)
			{
				goto IL_009b;
			}
		}

IL_0094:
		{
			V_7 = _stringLiteral3457518716;
		}

IL_009b:
		{
			String_t* L_23 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral603199997, L_23, /*hidden argument*/NULL);
			V_10 = L_24;
			String_t* L_25 = V_8;
			bool L_26 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
			if (L_26)
			{
				goto IL_00ca;
			}
		}

IL_00b5:
		{
			String_t* L_27 = V_10;
			String_t* L_28 = V_8;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_29 = String_Concat_m1561703559(NULL /*static, unused*/, L_27, _stringLiteral1089396559, L_28, _stringLiteral372029312, /*hidden argument*/NULL);
			V_10 = L_29;
		}

IL_00ca:
		{
			String_t* L_30 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_31 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
			if (L_31)
			{
				goto IL_00eb;
			}
		}

IL_00d6:
		{
			String_t* L_32 = V_10;
			String_t* L_33 = V_9;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_34 = String_Concat_m1561703559(NULL /*static, unused*/, L_32, _stringLiteral4071688053, L_33, _stringLiteral372029312, /*hidden argument*/NULL);
			V_10 = L_34;
		}

IL_00eb:
		{
			String_t* L_35 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_36 = String_Concat_m2596409543(NULL /*static, unused*/, L_35, _stringLiteral2162321587, /*hidden argument*/NULL);
			V_10 = L_36;
			Il2CppObject * L_37 = V_5;
			NullCheck(L_37);
			String_t* L_38 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.Networking.IMultipartFormSection::get_contentType() */, IMultipartFormSection_t1234744138_il2cpp_TypeInfo_var, L_37);
			V_11 = L_38;
			String_t* L_39 = V_11;
			bool L_40 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
			if (L_40)
			{
				goto IL_0123;
			}
		}

IL_010e:
		{
			String_t* L_41 = V_10;
			String_t* L_42 = V_11;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_43 = String_Concat_m1561703559(NULL /*static, unused*/, L_41, _stringLiteral1676307500, L_42, _stringLiteral2162321587, /*hidden argument*/NULL);
			V_10 = L_43;
		}

IL_0123:
		{
			List_1_t3052225568 * L_44 = V_4;
			ByteU5BU5D_t3397334013* L_45 = ___boundary1;
			NullCheck(L_44);
			List_1_AddRange_m3872295241(L_44, (Il2CppObject*)(Il2CppObject*)L_45, /*hidden argument*/List_1_AddRange_m3872295241_MethodInfo_var);
			List_1_t3052225568 * L_46 = V_4;
			ByteU5BU5D_t3397334013* L_47 = V_0;
			NullCheck(L_46);
			List_1_AddRange_m3872295241(L_46, (Il2CppObject*)(Il2CppObject*)L_47, /*hidden argument*/List_1_AddRange_m3872295241_MethodInfo_var);
			List_1_t3052225568 * L_48 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
			Encoding_t663144255 * L_49 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_50 = V_10;
			NullCheck(L_49);
			ByteU5BU5D_t3397334013* L_51 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_49, L_50);
			NullCheck(L_48);
			List_1_AddRange_m3872295241(L_48, (Il2CppObject*)(Il2CppObject*)L_51, /*hidden argument*/List_1_AddRange_m3872295241_MethodInfo_var);
			List_1_t3052225568 * L_52 = V_4;
			ByteU5BU5D_t3397334013* L_53 = V_0;
			NullCheck(L_52);
			List_1_AddRange_m3872295241(L_52, (Il2CppObject*)(Il2CppObject*)L_53, /*hidden argument*/List_1_AddRange_m3872295241_MethodInfo_var);
			List_1_t3052225568 * L_54 = V_4;
			Il2CppObject * L_55 = V_5;
			NullCheck(L_55);
			ByteU5BU5D_t3397334013* L_56 = InterfaceFuncInvoker0< ByteU5BU5D_t3397334013* >::Invoke(1 /* System.Byte[] UnityEngine.Networking.IMultipartFormSection::get_sectionData() */, IMultipartFormSection_t1234744138_il2cpp_TypeInfo_var, L_55);
			NullCheck(L_54);
			List_1_AddRange_m3872295241(L_54, (Il2CppObject*)(Il2CppObject*)L_56, /*hidden argument*/List_1_AddRange_m3872295241_MethodInfo_var);
		}

IL_015c:
		{
			bool L_57 = Enumerator_MoveNext_m4256584366((&V_6), /*hidden argument*/Enumerator_MoveNext_m4256584366_MethodInfo_var);
			if (L_57)
			{
				goto IL_0066;
			}
		}

IL_0168:
		{
			IL2CPP_LEAVE(0x17A, FINALLY_016d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_016d;
	}

FINALLY_016d:
	{ // begin finally (depth: 1)
		Enumerator_t138594944  L_58 = V_6;
		Enumerator_t138594944  L_59 = L_58;
		Il2CppObject * L_60 = Box(Enumerator_t138594944_il2cpp_TypeInfo_var, &L_59);
		NullCheck((Il2CppObject *)L_60);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_60);
		IL2CPP_END_FINALLY(365)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(365)
	{
		IL2CPP_JUMP_TBL(0x17A, IL_017a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_017a:
	{
		List_1_t3052225568 * L_61 = V_4;
		NullCheck(L_61);
		List_1_TrimExcess_m505787576(L_61, /*hidden argument*/List_1_TrimExcess_m505787576_MethodInfo_var);
		List_1_t3052225568 * L_62 = V_4;
		NullCheck(L_62);
		ByteU5BU5D_t3397334013* L_63 = List_1_ToArray_m2815648607(L_62, /*hidden argument*/List_1_ToArray_m2815648607_MethodInfo_var);
		return L_63;
	}
}
// System.Byte[] UnityEngine.Networking.UnityWebRequest::GenerateBoundary()
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_GenerateBoundary_m1443103734_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* UnityWebRequest_GenerateBoundary_m1443103734 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GenerateBoundary_m1443103734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)40)));
		V_1 = 0;
		goto IL_003a;
	}

IL_000f:
	{
		int32_t L_0 = Random_Range_m694320887(NULL /*static, unused*/, ((int32_t)48), ((int32_t)110), /*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)57))))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_2 = V_2;
		V_2 = ((int32_t)((int32_t)L_2+(int32_t)7));
	}

IL_0025:
	{
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0031;
		}
	}
	{
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)6));
	}

IL_0031:
	{
		ByteU5BU5D_t3397334013* L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)(((int32_t)((uint8_t)L_7))));
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)40))))
		{
			goto IL_000f;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_10 = V_0;
		return L_10;
	}
}
// System.Byte[] UnityEngine.Networking.UnityWebRequest::SerializeSimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t969056901_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1989408781_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m1372024679_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1710042386_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4005245300_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029308;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern const uint32_t UnityWebRequest_SerializeSimpleForm_m1079282452_MetadataUsageId;
extern "C"  ByteU5BU5D_t3397334013* UnityWebRequest_SerializeSimpleForm_m1079282452 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___formFields0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_SerializeSimpleForm_m1079282452_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t969056901  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_0;
		Dictionary_2_t3943999495 * L_1 = ___formFields0;
		NullCheck(L_1);
		Enumerator_t969056901  L_2 = Dictionary_2_GetEnumerator_m2895728349(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m2895728349_MethodInfo_var);
		V_2 = L_2;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0012:
		{
			KeyValuePair_2_t1701344717  L_3 = Enumerator_get_Current_m1989408781((&V_2), /*hidden argument*/Enumerator_get_Current_m1989408781_MethodInfo_var);
			V_1 = L_3;
			String_t* L_4 = V_0;
			NullCheck(L_4);
			int32_t L_5 = String_get_Length_m1606060069(L_4, /*hidden argument*/NULL);
			if ((((int32_t)L_5) <= ((int32_t)0)))
			{
				goto IL_0032;
			}
		}

IL_0026:
		{
			String_t* L_6 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, L_6, _stringLiteral372029308, /*hidden argument*/NULL);
			V_0 = L_7;
		}

IL_0032:
		{
			String_t* L_8 = V_0;
			String_t* L_9 = KeyValuePair_2_get_Key_m1372024679((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m1372024679_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(Uri_t19570940_il2cpp_TypeInfo_var);
			String_t* L_10 = Uri_EscapeDataString_m1533885280(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			String_t* L_11 = KeyValuePair_2_get_Value_m1710042386((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m1710042386_MethodInfo_var);
			String_t* L_12 = Uri_EscapeDataString_m1533885280(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_13 = String_Concat_m1561703559(NULL /*static, unused*/, L_8, L_10, _stringLiteral372029329, L_12, /*hidden argument*/NULL);
			V_0 = L_13;
		}

IL_0056:
		{
			bool L_14 = Enumerator_MoveNext_m4005245300((&V_2), /*hidden argument*/Enumerator_MoveNext_m4005245300_MethodInfo_var);
			if (L_14)
			{
				goto IL_0012;
			}
		}

IL_0062:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0067);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0067;
	}

FINALLY_0067:
	{ // begin finally (depth: 1)
		Enumerator_t969056901  L_15 = V_2;
		Enumerator_t969056901  L_16 = L_15;
		Il2CppObject * L_17 = Box(Enumerator_t969056901_il2cpp_TypeInfo_var, &L_16);
		NullCheck((Il2CppObject *)L_17);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_17);
		IL2CPP_END_FINALLY(103)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(103)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_18 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_19 = V_0;
		NullCheck(L_18);
		ByteU5BU5D_t3397334013* L_20 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_18, L_19);
		return L_20;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeDownloadHandlerOnDispose()
extern "C"  bool UnityWebRequest_get_disposeDownloadHandlerOnDispose_m977096671 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_disposeDownloadHandlerOnDispose(System.Boolean)
extern "C"  void UnityWebRequest_set_disposeDownloadHandlerOnDispose_m1872424902 (UnityWebRequest_t254341728 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_disposeUploadHandlerOnDispose()
extern "C"  bool UnityWebRequest_get_disposeUploadHandlerOnDispose_m573074478 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_disposeUploadHandlerOnDispose(System.Boolean)
extern "C"  void UnityWebRequest_set_disposeUploadHandlerOnDispose_m11086527 (UnityWebRequest_t254341728 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalCreate()
extern "C"  void UnityWebRequest_InternalCreate_m2618341326 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalCreate_m2618341326_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalCreate_m2618341326_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalCreate_m2618341326_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalCreate()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalDestroy()
extern "C"  void UnityWebRequest_InternalDestroy_m1188536142 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalDestroy_m1188536142_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalDestroy_m1188536142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalDestroy_m1188536142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetDefaults()
extern "C"  void UnityWebRequest_InternalSetDefaults_m1411946726 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		UnityWebRequest_set_disposeDownloadHandlerOnDispose_m1872424902(__this, (bool)1, /*hidden argument*/NULL);
		UnityWebRequest_set_disposeUploadHandlerOnDispose_m11086527(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::Finalize()
extern "C"  void UnityWebRequest_Finalize_m1113857405 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		UnityWebRequest_InternalDestroy_m1188536142(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::Dispose()
extern "C"  void UnityWebRequest_Dispose_m437623634 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	DownloadHandler_t1216180266 * V_0 = NULL;
	UploadHandler_t3552561393 * V_1 = NULL;
	{
		bool L_0 = UnityWebRequest_get_disposeDownloadHandlerOnDispose_m977096671(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		DownloadHandler_t1216180266 * L_1 = UnityWebRequest_get_downloadHandler_m2794451840(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		DownloadHandler_t1216180266 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		DownloadHandler_t1216180266 * L_3 = V_0;
		NullCheck(L_3);
		DownloadHandler_Dispose_m2043077428(L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		bool L_4 = UnityWebRequest_get_disposeUploadHandlerOnDispose_m573074478(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		UploadHandler_t3552561393 * L_5 = UnityWebRequest_get_uploadHandler_m1968885984(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		UploadHandler_t3552561393 * L_6 = V_1;
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		UploadHandler_t3552561393 * L_7 = V_1;
		NullCheck(L_7);
		UploadHandler_Dispose_m1405762689(L_7, /*hidden argument*/NULL);
	}

IL_003c:
	{
		UnityWebRequest_InternalDestroy_m1188536142(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Networking.UnityWebRequest::InternalBegin()
extern "C"  AsyncOperation_t3814632279 * UnityWebRequest_InternalBegin_m987291664 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef AsyncOperation_t3814632279 * (*UnityWebRequest_InternalBegin_m987291664_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalBegin_m987291664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalBegin_m987291664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalBegin()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalAbort()
extern "C"  void UnityWebRequest_InternalAbort_m1744334222 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalAbort_m1744334222_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalAbort_m1744334222_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalAbort_m1744334222_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalAbort()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.AsyncOperation UnityEngine.Networking.UnityWebRequest::Send()
extern "C"  AsyncOperation_t3814632279 * UnityWebRequest_Send_m4070133578 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		AsyncOperation_t3814632279 * L_0 = UnityWebRequest_InternalBegin_m987291664(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::Abort()
extern "C"  void UnityWebRequest_Abort_m3433435881 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		UnityWebRequest_InternalAbort_m1744334222(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)
extern "C"  void UnityWebRequest_InternalSetMethod_m969165462 (UnityWebRequest_t254341728 * __this, int32_t ___methodType0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetMethod_m969165462_ftn) (UnityWebRequest_t254341728 *, int32_t);
	static UnityWebRequest_InternalSetMethod_m969165462_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetMethod_m969165462_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetMethod(UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod)");
	_il2cpp_icall_func(__this, ___methodType0);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)
extern "C"  void UnityWebRequest_InternalSetCustomMethod_m1107638252 (UnityWebRequest_t254341728 * __this, String_t* ___customMethodName0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetCustomMethod_m1107638252_ftn) (UnityWebRequest_t254341728 *, String_t*);
	static UnityWebRequest_InternalSetCustomMethod_m1107638252_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetCustomMethod_m1107638252_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetCustomMethod(System.String)");
	_il2cpp_icall_func(__this, ___customMethodName0);
}
// System.Int32 UnityEngine.Networking.UnityWebRequest::InternalGetMethod()
extern "C"  int32_t UnityWebRequest_InternalGetMethod_m1439155887 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef int32_t (*UnityWebRequest_InternalGetMethod_m1439155887_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalGetMethod_m1439155887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetMethod_m1439155887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetMethod()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Networking.UnityWebRequest::InternalGetCustomMethod()
extern "C"  String_t* UnityWebRequest_InternalGetCustomMethod_m2293739551 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_InternalGetCustomMethod_m2293739551_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalGetCustomMethod_m2293739551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetCustomMethod_m2293739551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetCustomMethod()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Networking.UnityWebRequest::get_method()
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral884247665;
extern Il2CppCodeGenString* _stringLiteral1488409984;
extern const uint32_t UnityWebRequest_get_method_m1735183858_MetadataUsageId;
extern "C"  String_t* UnityWebRequest_get_method_m1735183858 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_get_method_m1735183858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = UnityWebRequest_InternalGetMethod_m1439155887(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (L_2 == 0)
		{
			goto IL_0024;
		}
		if (L_2 == 1)
		{
			goto IL_002a;
		}
		if (L_2 == 2)
		{
			goto IL_0030;
		}
		if (L_2 == 3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_003c;
	}

IL_0024:
	{
		return _stringLiteral1596707798;
	}

IL_002a:
	{
		return _stringLiteral782856060;
	}

IL_0030:
	{
		return _stringLiteral884247665;
	}

IL_0036:
	{
		return _stringLiteral1488409984;
	}

IL_003c:
	{
		String_t* L_3 = UnityWebRequest_InternalGetCustomMethod_m2293739551(__this, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_method(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2118310873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2977303364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1341399209;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral884247665;
extern Il2CppCodeGenString* _stringLiteral1488409984;
extern const uint32_t UnityWebRequest_set_method_m4118764027_MetadataUsageId;
extern "C"  void UnityWebRequest_set_method_m4118764027 (UnityWebRequest_t254341728 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_set_method_m4118764027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t3986656710 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, _stringLiteral1341399209, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___value0;
		NullCheck(L_3);
		String_t* L_4 = String_ToUpper_m3715743312(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		if (!L_5)
		{
			goto IL_00c7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		Dictionary_2_t3986656710 * L_6 = ((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_11();
		if (L_6)
		{
			goto IL_006a;
		}
	}
	{
		Dictionary_2_t3986656710 * L_7 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2118310873(L_7, 4, /*hidden argument*/Dictionary_2__ctor_m2118310873_MethodInfo_var);
		V_1 = L_7;
		Dictionary_2_t3986656710 * L_8 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m1209957957(L_8, _stringLiteral1596707798, 0, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_9 = V_1;
		NullCheck(L_9);
		Dictionary_2_Add_m1209957957(L_9, _stringLiteral782856060, 1, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_10 = V_1;
		NullCheck(L_10);
		Dictionary_2_Add_m1209957957(L_10, _stringLiteral884247665, 2, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_11 = V_1;
		NullCheck(L_11);
		Dictionary_2_Add_m1209957957(L_11, _stringLiteral1488409984, 3, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map1_11(L_12);
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		Dictionary_2_t3986656710 * L_13 = ((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map1_11();
		String_t* L_14 = V_0;
		NullCheck(L_13);
		bool L_15 = Dictionary_2_TryGetValue_m2977303364(L_13, L_14, (&V_2), /*hidden argument*/Dictionary_2_TryGetValue_m2977303364_MethodInfo_var);
		if (!L_15)
		{
			goto IL_00c7;
		}
	}
	{
		int32_t L_16 = V_2;
		if (L_16 == 0)
		{
			goto IL_0097;
		}
		if (L_16 == 1)
		{
			goto IL_00a3;
		}
		if (L_16 == 2)
		{
			goto IL_00af;
		}
		if (L_16 == 3)
		{
			goto IL_00bb;
		}
	}
	{
		goto IL_00c7;
	}

IL_0097:
	{
		UnityWebRequest_InternalSetMethod_m969165462(__this, 0, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00a3:
	{
		UnityWebRequest_InternalSetMethod_m969165462(__this, 1, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00af:
	{
		UnityWebRequest_InternalSetMethod_m969165462(__this, 2, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00bb:
	{
		UnityWebRequest_InternalSetMethod_m969165462(__this, 3, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00c7:
	{
		String_t* L_17 = ___value0;
		NullCheck(L_17);
		String_t* L_18 = String_ToUpper_m3715743312(L_17, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetCustomMethod_m1107638252(__this, L_18, /*hidden argument*/NULL);
		goto IL_00d8;
	}

IL_00d8:
	{
		return;
	}
}
// System.Int32 UnityEngine.Networking.UnityWebRequest::InternalGetError()
extern "C"  int32_t UnityWebRequest_InternalGetError_m3993195296 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef int32_t (*UnityWebRequest_InternalGetError_m3993195296_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalGetError_m3993195296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetError_m3993195296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetError()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern "C"  String_t* UnityWebRequest_get_error_m1819765147 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_get_error_m1819765147_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_error_m1819765147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_error_m1819765147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_error()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_useHttpContinue()
extern "C"  bool UnityWebRequest_get_useHttpContinue_m25605422 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_useHttpContinue_m25605422_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_useHttpContinue_m25605422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_useHttpContinue_m25605422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_useHttpContinue()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_useHttpContinue(System.Boolean)
extern "C"  void UnityWebRequest_set_useHttpContinue_m3212280575 (UnityWebRequest_t254341728 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_useHttpContinue_m3212280575_ftn) (UnityWebRequest_t254341728 *, bool);
	static UnityWebRequest_set_useHttpContinue_m3212280575_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_useHttpContinue_m3212280575_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_useHttpContinue(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Networking.UnityWebRequest::get_url()
extern "C"  String_t* UnityWebRequest_get_url_m798565458 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = UnityWebRequest_InternalGetUrl_m1739605410(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_url(System.String)
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppClass* FormatException_t2948921286_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1591141764;
extern Il2CppCodeGenString* _stringLiteral51790588;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral372029315;
extern Il2CppCodeGenString* _stringLiteral57472706;
extern const uint32_t UnityWebRequest_set_url_m2494687159_MetadataUsageId;
extern "C"  void UnityWebRequest_set_url_m2494687159 (UnityWebRequest_t254341728 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_set_url_m2494687159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	Uri_t19570940 * V_2 = NULL;
	Uri_t19570940 * V_3 = NULL;
	FormatException_t2948921286 * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___value0;
		V_0 = L_0;
		V_1 = _stringLiteral1591141764;
		String_t* L_1 = V_1;
		Uri_t19570940 * L_2 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_2, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		String_t* L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = String_StartsWith_m1841920685(L_3, _stringLiteral51790588, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		Uri_t19570940 * L_5 = V_2;
		NullCheck(L_5);
		String_t* L_6 = Uri_get_Scheme_m55908894(L_5, /*hidden argument*/NULL);
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m612901809(NULL /*static, unused*/, L_6, _stringLiteral372029336, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0031:
	{
		String_t* L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = String_StartsWith_m1841920685(L_9, _stringLiteral372029315, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		Uri_t19570940 * L_11 = V_2;
		NullCheck(L_11);
		String_t* L_12 = Uri_get_Scheme_m55908894(L_11, /*hidden argument*/NULL);
		Uri_t19570940 * L_13 = V_2;
		NullCheck(L_13);
		String_t* L_14 = Uri_get_Host_m2492204157(L_13, /*hidden argument*/NULL);
		String_t* L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m1561703559(NULL /*static, unused*/, L_12, _stringLiteral57472706, L_14, L_15, /*hidden argument*/NULL);
		V_0 = L_16;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		Regex_t1803876613 * L_17 = ((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->get_domainRegex_7();
		String_t* L_18 = V_0;
		NullCheck(L_17);
		bool L_19 = Regex_IsMatch_m2159202025(L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_007b;
		}
	}
	{
		Uri_t19570940 * L_20 = V_2;
		NullCheck(L_20);
		String_t* L_21 = Uri_get_Scheme_m55908894(L_20, /*hidden argument*/NULL);
		String_t* L_22 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m612901809(NULL /*static, unused*/, L_21, _stringLiteral57472706, L_22, /*hidden argument*/NULL);
		V_0 = L_23;
	}

IL_007b:
	{
		V_3 = (Uri_t19570940 *)NULL;
	}

IL_007d:
	try
	{ // begin try (depth: 1)
		String_t* L_24 = V_0;
		Uri_t19570940 * L_25 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
		Uri__ctor_m3927533881(L_25, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		goto IL_00a6;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0089;
		throw e;
	}

CATCH_0089:
	{ // begin catch(System.FormatException)
		{
			V_4 = ((FormatException_t2948921286 *)__exception_local);
		}

IL_008b:
		try
		{ // begin try (depth: 2)
			Uri_t19570940 * L_26 = V_2;
			String_t* L_27 = V_0;
			Uri_t19570940 * L_28 = (Uri_t19570940 *)il2cpp_codegen_object_new(Uri_t19570940_il2cpp_TypeInfo_var);
			Uri__ctor_m3550796566(L_28, L_26, L_27, /*hidden argument*/NULL);
			V_3 = L_28;
			goto IL_00a1;
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t1927440687 *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (FormatException_t2948921286_il2cpp_TypeInfo_var, e.ex->object.klass))
				goto CATCH_0098;
			throw e;
		}

CATCH_0098:
		{ // begin catch(System.FormatException)
			{
				FormatException_t2948921286 * L_29 = V_4;
				IL2CPP_RAISE_MANAGED_EXCEPTION(L_29);
			}

IL_009c:
			{
				goto IL_00a1;
			}
		} // end catch (depth: 2)

IL_00a1:
		{
			goto IL_00a6;
		}
	} // end catch (depth: 1)

IL_00a6:
	{
		Uri_t19570940 * L_30 = V_3;
		NullCheck(L_30);
		String_t* L_31 = Uri_get_AbsoluteUri_m2120317928(L_30, /*hidden argument*/NULL);
		UnityWebRequest_InternalSetUrl_m1669953255(__this, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.UnityWebRequest::InternalGetUrl()
extern "C"  String_t* UnityWebRequest_InternalGetUrl_m1739605410 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_InternalGetUrl_m1739605410_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalGetUrl_m1739605410_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetUrl_m1739605410_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetUrl()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String)
extern "C"  void UnityWebRequest_InternalSetUrl_m1669953255 (UnityWebRequest_t254341728 * __this, String_t* ___url0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetUrl_m1669953255_ftn) (UnityWebRequest_t254341728 *, String_t*);
	static UnityWebRequest_InternalSetUrl_m1669953255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetUrl_m1669953255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetUrl(System.String)");
	_il2cpp_icall_func(__this, ___url0);
}
// System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
extern "C"  int64_t UnityWebRequest_get_responseCode_m1590258701 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef int64_t (*UnityWebRequest_get_responseCode_m1590258701_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_responseCode_m1590258701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_responseCode_m1590258701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_responseCode()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Networking.UnityWebRequest::get_uploadProgress()
extern "C"  float UnityWebRequest_get_uploadProgress_m29797802 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef float (*UnityWebRequest_get_uploadProgress_m29797802_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_uploadProgress_m29797802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_uploadProgress_m29797802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_uploadProgress()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isModifiable()
extern "C"  bool UnityWebRequest_get_isModifiable_m865629448 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_isModifiable_m865629448_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_isModifiable_m865629448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_isModifiable_m865629448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_isModifiable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
extern "C"  bool UnityWebRequest_get_isDone_m3929051890 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_isDone_m3929051890_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_isDone_m3929051890_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_isDone_m3929051890_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isError()
extern "C"  bool UnityWebRequest_get_isError_m2487408374 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_isError_m2487408374_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_isError_m2487408374_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_isError_m2487408374_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_isError()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Networking.UnityWebRequest::get_downloadProgress()
extern "C"  float UnityWebRequest_get_downloadProgress_m3197899461 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef float (*UnityWebRequest_get_downloadProgress_m3197899461_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_downloadProgress_m3197899461_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_downloadProgress_m3197899461_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_downloadProgress()");
	return _il2cpp_icall_func(__this);
}
// System.UInt64 UnityEngine.Networking.UnityWebRequest::get_uploadedBytes()
extern "C"  uint64_t UnityWebRequest_get_uploadedBytes_m2170144001 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef uint64_t (*UnityWebRequest_get_uploadedBytes_m2170144001_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_uploadedBytes_m2170144001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_uploadedBytes_m2170144001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_uploadedBytes()");
	return _il2cpp_icall_func(__this);
}
// System.UInt64 UnityEngine.Networking.UnityWebRequest::get_downloadedBytes()
extern "C"  uint64_t UnityWebRequest_get_downloadedBytes_m1777838196 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef uint64_t (*UnityWebRequest_get_downloadedBytes_m1777838196_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_downloadedBytes_m1777838196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_downloadedBytes_m1777838196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_downloadedBytes()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Networking.UnityWebRequest::get_redirectLimit()
extern "C"  int32_t UnityWebRequest_get_redirectLimit_m259403435 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef int32_t (*UnityWebRequest_get_redirectLimit_m259403435_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_redirectLimit_m259403435_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_redirectLimit_m259403435_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_redirectLimit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_redirectLimit(System.Int32)
extern "C"  void UnityWebRequest_set_redirectLimit_m1226497466 (UnityWebRequest_t254341728 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_redirectLimit_m1226497466_ftn) (UnityWebRequest_t254341728 *, int32_t);
	static UnityWebRequest_set_redirectLimit_m1226497466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_redirectLimit_m1226497466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_redirectLimit(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_chunkedTransfer()
extern "C"  bool UnityWebRequest_get_chunkedTransfer_m3818596779 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef bool (*UnityWebRequest_get_chunkedTransfer_m3818596779_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_chunkedTransfer_m3818596779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_chunkedTransfer_m3818596779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_chunkedTransfer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_chunkedTransfer(System.Boolean)
extern "C"  void UnityWebRequest_set_chunkedTransfer_m299823738 (UnityWebRequest_t254341728 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_chunkedTransfer_m299823738_ftn) (UnityWebRequest_t254341728 *, bool);
	static UnityWebRequest_set_chunkedTransfer_m299823738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_chunkedTransfer_m299823738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_chunkedTransfer(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Networking.UnityWebRequest::GetRequestHeader(System.String)
extern "C"  String_t* UnityWebRequest_GetRequestHeader_m2981224728 (UnityWebRequest_t254341728 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_GetRequestHeader_m2981224728_ftn) (UnityWebRequest_t254341728 *, String_t*);
	static UnityWebRequest_GetRequestHeader_m2981224728_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_GetRequestHeader_m2981224728_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::GetRequestHeader(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_InternalSetRequestHeader_m2536447480 (UnityWebRequest_t254341728 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_InternalSetRequestHeader_m2536447480_ftn) (UnityWebRequest_t254341728 *, String_t*, String_t*);
	static UnityWebRequest_InternalSetRequestHeader_m2536447480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalSetRequestHeader_m2536447480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalSetRequestHeader(System.String,System.String)");
	_il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2417366839;
extern Il2CppCodeGenString* _stringLiteral199808707;
extern Il2CppCodeGenString* _stringLiteral1830004213;
extern Il2CppCodeGenString* _stringLiteral1497495018;
extern Il2CppCodeGenString* _stringLiteral1433428610;
extern const uint32_t UnityWebRequest_SetRequestHeader_m466367223_MetadataUsageId;
extern "C"  void UnityWebRequest_SetRequestHeader_m466367223 (UnityWebRequest_t254341728 * __this, String_t* ___name0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_SetRequestHeader_m466367223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, _stringLiteral2417366839, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0016:
	{
		String_t* L_3 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		ArgumentException_t3259014390 * L_5 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_5, _stringLiteral199808707, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002c:
	{
		String_t* L_6 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		bool L_7 = UnityWebRequest_IsHeaderNameLegal_m453693478(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_8 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1830004213, L_8, _stringLiteral1497495018, /*hidden argument*/NULL);
		ArgumentException_t3259014390 * L_10 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_004d:
	{
		String_t* L_11 = ___value1;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		bool L_12 = UnityWebRequest_IsHeaderValueLegal_m925827794(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0063;
		}
	}
	{
		ArgumentException_t3259014390 * L_13 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_13, _stringLiteral1433428610, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13);
	}

IL_0063:
	{
		String_t* L_14 = ___name0;
		String_t* L_15 = ___value1;
		UnityWebRequest_InternalSetRequestHeader_m2536447480(__this, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.UnityWebRequest::GetResponseHeader(System.String)
extern "C"  String_t* UnityWebRequest_GetResponseHeader_m1822960276 (UnityWebRequest_t254341728 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef String_t* (*UnityWebRequest_GetResponseHeader_m1822960276_ftn) (UnityWebRequest_t254341728 *, String_t*);
	static UnityWebRequest_GetResponseHeader_m1822960276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_GetResponseHeader_m1822960276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::GetResponseHeader(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.String[] UnityEngine.Networking.UnityWebRequest::InternalGetResponseHeaderKeys()
extern "C"  StringU5BU5D_t1642385972* UnityWebRequest_InternalGetResponseHeaderKeys_m1297904277 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef StringU5BU5D_t1642385972* (*UnityWebRequest_InternalGetResponseHeaderKeys_m1297904277_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_InternalGetResponseHeaderKeys_m1297904277_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_InternalGetResponseHeaderKeys_m1297904277_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::InternalGetResponseHeaderKeys()");
	return _il2cpp_icall_func(__this);
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Networking.UnityWebRequest::GetResponseHeaders()
extern Il2CppClass* StringComparer_t1574862926_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2975482961_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2509825489_MethodInfo_var;
extern const uint32_t UnityWebRequest_GetResponseHeaders_m3806990876_MetadataUsageId;
extern "C"  Dictionary_2_t3943999495 * UnityWebRequest_GetResponseHeaders_m3806990876 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetResponseHeaders_m3806990876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	Dictionary_2_t3943999495 * V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		StringU5BU5D_t1642385972* L_0 = UnityWebRequest_InternalGetResponseHeaderKeys_m1297904277(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		StringU5BU5D_t1642385972* L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (Dictionary_2_t3943999495 *)NULL;
	}

IL_000f:
	{
		StringU5BU5D_t1642385972* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1574862926_il2cpp_TypeInfo_var);
		StringComparer_t1574862926 * L_3 = StringComparer_get_OrdinalIgnoreCase_m3428639861(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t3943999495 * L_4 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2975482961(L_4, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), L_3, /*hidden argument*/Dictionary_2__ctor_m2975482961_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		goto IL_003c;
	}

IL_0024:
	{
		StringU5BU5D_t1642385972* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		String_t* L_9 = UnityWebRequest_GetResponseHeader_m1822960276(__this, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Dictionary_2_t3943999495 * L_10 = V_1;
		StringU5BU5D_t1642385972* L_11 = V_0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		String_t* L_15 = V_3;
		NullCheck(L_10);
		Dictionary_2_Add_m2509825489(L_10, L_14, L_15, /*hidden argument*/Dictionary_2_Add_m2509825489_MethodInfo_var);
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_17 = V_2;
		StringU5BU5D_t1642385972* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0024;
		}
	}
	{
		Dictionary_2_t3943999495 * L_19 = V_1;
		return L_19;
	}
}
// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::get_uploadHandler()
extern "C"  UploadHandler_t3552561393 * UnityWebRequest_get_uploadHandler_m1968885984 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef UploadHandler_t3552561393 * (*UnityWebRequest_get_uploadHandler_m1968885984_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_uploadHandler_m1968885984_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_uploadHandler_m1968885984_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_uploadHandler()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
extern "C"  void UnityWebRequest_set_uploadHandler_m2033861625 (UnityWebRequest_t254341728 * __this, UploadHandler_t3552561393 * ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_uploadHandler_m2033861625_ftn) (UnityWebRequest_t254341728 *, UploadHandler_t3552561393 *);
	static UnityWebRequest_set_uploadHandler_m2033861625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_uploadHandler_m2033861625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
extern "C"  DownloadHandler_t1216180266 * UnityWebRequest_get_downloadHandler_m2794451840 (UnityWebRequest_t254341728 * __this, const MethodInfo* method)
{
	typedef DownloadHandler_t1216180266 * (*UnityWebRequest_get_downloadHandler_m2794451840_ftn) (UnityWebRequest_t254341728 *);
	static UnityWebRequest_get_downloadHandler_m2794451840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_get_downloadHandler_m2794451840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::get_downloadHandler()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
extern "C"  void UnityWebRequest_set_downloadHandler_m1587492897 (UnityWebRequest_t254341728 * __this, DownloadHandler_t1216180266 * ___value0, const MethodInfo* method)
{
	typedef void (*UnityWebRequest_set_downloadHandler_m1587492897_ftn) (UnityWebRequest_t254341728 *, DownloadHandler_t1216180266 *);
	static UnityWebRequest_set_downloadHandler_m1587492897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityWebRequest_set_downloadHandler_m1587492897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::ContainsForbiddenCharacters(System.String,System.Int32)
extern "C"  bool UnityWebRequest_ContainsForbiddenCharacters_m2308554832 (Il2CppObject * __this /* static, unused */, String_t* ___s0, int32_t ___firstAllowedCharCode1, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___s0;
		V_1 = L_0;
		V_2 = 0;
		goto IL_0026;
	}

IL_0009:
	{
		String_t* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		Il2CppChar L_3 = String_get_Chars_m4230566705(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Il2CppChar L_4 = V_0;
		int32_t L_5 = ___firstAllowedCharCode1;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0020;
		}
	}
	{
		Il2CppChar L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)127)))))
		{
			goto IL_0022;
		}
	}

IL_0020:
	{
		return (bool)1;
	}

IL_0022:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_8 = V_2;
		String_t* L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m1606060069(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0009;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::IsHeaderNameLegal(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2496233078;
extern Il2CppCodeGenString* _stringLiteral115715693;
extern const uint32_t UnityWebRequest_IsHeaderNameLegal_m453693478_MetadataUsageId;
extern "C"  bool UnityWebRequest_IsHeaderNameLegal_m453693478 (Il2CppObject * __this /* static, unused */, String_t* ___headerName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_IsHeaderNameLegal_m453693478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___headerName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		String_t* L_2 = ___headerName0;
		NullCheck(L_2);
		String_t* L_3 = String_ToLower_m2994460523(L_2, /*hidden argument*/NULL);
		___headerName0 = L_3;
		String_t* L_4 = ___headerName0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		bool L_5 = UnityWebRequest_ContainsForbiddenCharacters_m2308554832(NULL /*static, unused*/, L_4, ((int32_t)33), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		return (bool)0;
	}

IL_0024:
	{
		String_t* L_6 = ___headerName0;
		NullCheck(L_6);
		bool L_7 = String_StartsWith_m1841920685(L_6, _stringLiteral2496233078, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_8 = ___headerName0;
		NullCheck(L_8);
		bool L_9 = String_StartsWith_m1841920685(L_8, _stringLiteral115715693, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0046;
		}
	}

IL_0044:
	{
		return (bool)0;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		StringU5BU5D_t1642385972* L_10 = ((UnityWebRequest_t254341728_StaticFields*)UnityWebRequest_t254341728_il2cpp_TypeInfo_var->static_fields)->get_forbiddenHeaderKeys_8();
		V_1 = L_10;
		V_2 = 0;
		goto IL_0069;
	}

IL_0053:
	{
		StringU5BU5D_t1642385972* L_11 = V_1;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_0 = L_14;
		String_t* L_15 = ___headerName0;
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_Equals_m3568148125(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0065;
		}
	}
	{
		return (bool)0;
	}

IL_0065:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0069:
	{
		int32_t L_19 = V_2;
		StringU5BU5D_t1642385972* L_20 = V_1;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0053;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean UnityEngine.Networking.UnityWebRequest::IsHeaderValueLegal(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern const uint32_t UnityWebRequest_IsHeaderValueLegal_m925827794_MetadataUsageId;
extern "C"  bool UnityWebRequest_IsHeaderValueLegal_m925827794 (Il2CppObject * __this /* static, unused */, String_t* ___headerValue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_IsHeaderValueLegal_m925827794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___headerValue0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		String_t* L_2 = ___headerValue0;
		IL2CPP_RUNTIME_CLASS_INIT(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		bool L_3 = UnityWebRequest_ContainsForbiddenCharacters_m2308554832(NULL /*static, unused*/, L_2, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		return (bool)1;
	}
}
// System.String UnityEngine.Networking.UnityWebRequest::GetErrorDescription(UnityEngine.Networking.UnityWebRequest/UnityWebRequestError)
extern Il2CppCodeGenString* _stringLiteral1205070113;
extern Il2CppCodeGenString* _stringLiteral3385197949;
extern Il2CppCodeGenString* _stringLiteral1875492866;
extern Il2CppCodeGenString* _stringLiteral2745530655;
extern Il2CppCodeGenString* _stringLiteral542169285;
extern Il2CppCodeGenString* _stringLiteral2142545588;
extern Il2CppCodeGenString* _stringLiteral2560529461;
extern Il2CppCodeGenString* _stringLiteral1726676501;
extern Il2CppCodeGenString* _stringLiteral268802340;
extern Il2CppCodeGenString* _stringLiteral689004583;
extern Il2CppCodeGenString* _stringLiteral3406579856;
extern Il2CppCodeGenString* _stringLiteral2896366454;
extern Il2CppCodeGenString* _stringLiteral3681567527;
extern Il2CppCodeGenString* _stringLiteral1727977821;
extern Il2CppCodeGenString* _stringLiteral2379889999;
extern Il2CppCodeGenString* _stringLiteral2713140683;
extern Il2CppCodeGenString* _stringLiteral3291170486;
extern Il2CppCodeGenString* _stringLiteral3998302596;
extern Il2CppCodeGenString* _stringLiteral3975279680;
extern Il2CppCodeGenString* _stringLiteral2341554480;
extern Il2CppCodeGenString* _stringLiteral2033159156;
extern Il2CppCodeGenString* _stringLiteral512622384;
extern Il2CppCodeGenString* _stringLiteral1080518633;
extern Il2CppCodeGenString* _stringLiteral2269746443;
extern Il2CppCodeGenString* _stringLiteral596401044;
extern Il2CppCodeGenString* _stringLiteral503819731;
extern Il2CppCodeGenString* _stringLiteral3840104669;
extern Il2CppCodeGenString* _stringLiteral1421870506;
extern const uint32_t UnityWebRequest_GetErrorDescription_m3167852288_MetadataUsageId;
extern "C"  String_t* UnityWebRequest_GetErrorDescription_m3167852288 (Il2CppObject * __this /* static, unused */, int32_t ___errorCode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityWebRequest_GetErrorDescription_m3167852288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___errorCode0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_007d;
		}
		if (L_1 == 1)
		{
			goto IL_011f;
		}
		if (L_1 == 2)
		{
			goto IL_0083;
		}
		if (L_1 == 3)
		{
			goto IL_0089;
		}
		if (L_1 == 4)
		{
			goto IL_008f;
		}
		if (L_1 == 5)
		{
			goto IL_0095;
		}
		if (L_1 == 6)
		{
			goto IL_009b;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00a7;
		}
		if (L_1 == 9)
		{
			goto IL_00ad;
		}
		if (L_1 == 10)
		{
			goto IL_00b3;
		}
		if (L_1 == 11)
		{
			goto IL_00b9;
		}
		if (L_1 == 12)
		{
			goto IL_00bf;
		}
		if (L_1 == 13)
		{
			goto IL_00c5;
		}
		if (L_1 == 14)
		{
			goto IL_00cb;
		}
		if (L_1 == 15)
		{
			goto IL_00d1;
		}
		if (L_1 == 16)
		{
			goto IL_00d7;
		}
		if (L_1 == 17)
		{
			goto IL_00dd;
		}
		if (L_1 == 18)
		{
			goto IL_00e3;
		}
		if (L_1 == 19)
		{
			goto IL_00e9;
		}
		if (L_1 == 20)
		{
			goto IL_00ef;
		}
		if (L_1 == 21)
		{
			goto IL_00f5;
		}
		if (L_1 == 22)
		{
			goto IL_00fb;
		}
		if (L_1 == 23)
		{
			goto IL_0101;
		}
		if (L_1 == 24)
		{
			goto IL_0107;
		}
		if (L_1 == 25)
		{
			goto IL_010d;
		}
		if (L_1 == 26)
		{
			goto IL_0113;
		}
		if (L_1 == 27)
		{
			goto IL_0119;
		}
	}
	{
		goto IL_011f;
	}

IL_007d:
	{
		return _stringLiteral1205070113;
	}

IL_0083:
	{
		return _stringLiteral3385197949;
	}

IL_0089:
	{
		return _stringLiteral1875492866;
	}

IL_008f:
	{
		return _stringLiteral2745530655;
	}

IL_0095:
	{
		return _stringLiteral542169285;
	}

IL_009b:
	{
		return _stringLiteral2142545588;
	}

IL_00a1:
	{
		return _stringLiteral2560529461;
	}

IL_00a7:
	{
		return _stringLiteral1726676501;
	}

IL_00ad:
	{
		return _stringLiteral268802340;
	}

IL_00b3:
	{
		return _stringLiteral689004583;
	}

IL_00b9:
	{
		return _stringLiteral3406579856;
	}

IL_00bf:
	{
		return _stringLiteral2896366454;
	}

IL_00c5:
	{
		return _stringLiteral3681567527;
	}

IL_00cb:
	{
		return _stringLiteral1727977821;
	}

IL_00d1:
	{
		return _stringLiteral2379889999;
	}

IL_00d7:
	{
		return _stringLiteral2713140683;
	}

IL_00dd:
	{
		return _stringLiteral3291170486;
	}

IL_00e3:
	{
		return _stringLiteral3998302596;
	}

IL_00e9:
	{
		return _stringLiteral3975279680;
	}

IL_00ef:
	{
		return _stringLiteral2341554480;
	}

IL_00f5:
	{
		return _stringLiteral2033159156;
	}

IL_00fb:
	{
		return _stringLiteral512622384;
	}

IL_0101:
	{
		return _stringLiteral1080518633;
	}

IL_0107:
	{
		return _stringLiteral2269746443;
	}

IL_010d:
	{
		return _stringLiteral596401044;
	}

IL_0113:
	{
		return _stringLiteral503819731;
	}

IL_0119:
	{
		return _stringLiteral3840104669;
	}

IL_011f:
	{
		return _stringLiteral1421870506;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t254341728_marshal_pinvoke(const UnityWebRequest_t254341728& unmarshaled, UnityWebRequest_t254341728_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_6 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_6()).get_m_value_0());
	marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9 = unmarshaled.get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9();
	marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10 = unmarshaled.get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10();
}
extern "C" void UnityWebRequest_t254341728_marshal_pinvoke_back(const UnityWebRequest_t254341728_marshaled_pinvoke& marshaled, UnityWebRequest_t254341728& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_6));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_6(unmarshaled_m_Ptr_temp_0);
	bool unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = false;
	unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9;
	unmarshaled.set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1);
	bool unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = false;
	unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10;
	unmarshaled.set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t254341728_marshal_pinvoke_cleanup(UnityWebRequest_t254341728_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t254341728_marshal_com(const UnityWebRequest_t254341728& unmarshaled, UnityWebRequest_t254341728_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_6 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_6()).get_m_value_0());
	marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9 = unmarshaled.get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9();
	marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10 = unmarshaled.get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10();
}
extern "C" void UnityWebRequest_t254341728_marshal_com_back(const UnityWebRequest_t254341728_marshaled_com& marshaled, UnityWebRequest_t254341728& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_6));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_6(unmarshaled_m_Ptr_temp_0);
	bool unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = false;
	unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1 = marshaled.___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9;
	unmarshaled.set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_9(unmarshaled_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_temp_1);
	bool unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = false;
	unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2 = marshaled.___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10;
	unmarshaled.set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_10(unmarshaled_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UnityWebRequest
extern "C" void UnityWebRequest_t254341728_marshal_com_cleanup(UnityWebRequest_t254341728_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.UploadHandler::.ctor()
extern "C"  void UploadHandler__ctor_m3605312080 (UploadHandler_t3552561393 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::InternalCreateRaw(System.Byte[])
extern "C"  void UploadHandler_InternalCreateRaw_m3935811756 (UploadHandler_t3552561393 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	typedef void (*UploadHandler_InternalCreateRaw_m3935811756_ftn) (UploadHandler_t3552561393 *, ByteU5BU5D_t3397334013*);
	static UploadHandler_InternalCreateRaw_m3935811756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UploadHandler_InternalCreateRaw_m3935811756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UploadHandler::InternalCreateRaw(System.Byte[])");
	_il2cpp_icall_func(__this, ___data0);
}
// System.Void UnityEngine.Networking.UploadHandler::InternalDestroy()
extern "C"  void UploadHandler_InternalDestroy_m3064454419 (UploadHandler_t3552561393 * __this, const MethodInfo* method)
{
	typedef void (*UploadHandler_InternalDestroy_m3064454419_ftn) (UploadHandler_t3552561393 *);
	static UploadHandler_InternalDestroy_m3064454419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UploadHandler_InternalDestroy_m3064454419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UploadHandler::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Networking.UploadHandler::Finalize()
extern "C"  void UploadHandler_Finalize_m177635988 (UploadHandler_t3552561393 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		UploadHandler_InternalDestroy_m3064454419(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::Dispose()
extern "C"  void UploadHandler_Dispose_m1405762689 (UploadHandler_t3552561393 * __this, const MethodInfo* method)
{
	{
		UploadHandler_InternalDestroy_m3064454419(__this, /*hidden argument*/NULL);
		GC_SuppressFinalize_m953228702(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::set_contentType(System.String)
extern "C"  void UploadHandler_set_contentType_m2720939452 (UploadHandler_t3552561393 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UnityEngine.Networking.UploadHandler::SetContentType(System.String) */, __this, L_0);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandler::SetContentType(System.String)
extern "C"  void UploadHandler_SetContentType_m3179619127 (UploadHandler_t3552561393 * __this, String_t* ___newContentType0, const MethodInfo* method)
{
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t3552561393_marshal_pinvoke(const UploadHandler_t3552561393& unmarshaled, UploadHandler_t3552561393_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandler_t3552561393_marshal_pinvoke_back(const UploadHandler_t3552561393_marshaled_pinvoke& marshaled, UploadHandler_t3552561393& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t3552561393_marshal_pinvoke_cleanup(UploadHandler_t3552561393_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t3552561393_marshal_com(const UploadHandler_t3552561393& unmarshaled, UploadHandler_t3552561393_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandler_t3552561393_marshal_com_back(const UploadHandler_t3552561393_marshaled_com& marshaled, UploadHandler_t3552561393& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandler
extern "C" void UploadHandler_t3552561393_marshal_com_cleanup(UploadHandler_t3552561393_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
extern "C"  void UploadHandlerRaw__ctor_m2445981389 (UploadHandlerRaw_t3420491431 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method)
{
	{
		UploadHandler__ctor_m3605312080(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___data0;
		UploadHandler_InternalCreateRaw_m3935811756(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.UploadHandlerRaw::InternalSetContentType(System.String)
extern "C"  void UploadHandlerRaw_InternalSetContentType_m3851129148 (UploadHandlerRaw_t3420491431 * __this, String_t* ___newContentType0, const MethodInfo* method)
{
	typedef void (*UploadHandlerRaw_InternalSetContentType_m3851129148_ftn) (UploadHandlerRaw_t3420491431 *, String_t*);
	static UploadHandlerRaw_InternalSetContentType_m3851129148_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UploadHandlerRaw_InternalSetContentType_m3851129148_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Networking.UploadHandlerRaw::InternalSetContentType(System.String)");
	_il2cpp_icall_func(__this, ___newContentType0);
}
// System.Void UnityEngine.Networking.UploadHandlerRaw::SetContentType(System.String)
extern "C"  void UploadHandlerRaw_SetContentType_m787854167 (UploadHandlerRaw_t3420491431 * __this, String_t* ___newContentType0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___newContentType0;
		UploadHandlerRaw_InternalSetContentType_m3851129148(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t3420491431_marshal_pinvoke(const UploadHandlerRaw_t3420491431& unmarshaled, UploadHandlerRaw_t3420491431_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandlerRaw_t3420491431_marshal_pinvoke_back(const UploadHandlerRaw_t3420491431_marshaled_pinvoke& marshaled, UploadHandlerRaw_t3420491431& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t3420491431_marshal_pinvoke_cleanup(UploadHandlerRaw_t3420491431_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t3420491431_marshal_com(const UploadHandlerRaw_t3420491431& unmarshaled, UploadHandlerRaw_t3420491431_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void UploadHandlerRaw_t3420491431_marshal_com_back(const UploadHandlerRaw_t3420491431_marshaled_com& marshaled, UploadHandlerRaw_t3420491431& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Networking.UploadHandlerRaw
extern "C" void UploadHandlerRaw_t3420491431_marshal_com_cleanup(UploadHandlerRaw_t3420491431_marshaled_com& marshaled)
{
}
// System.Double UnityEngine.NetworkMessageInfo::get_timestamp()
extern "C"  double NetworkMessageInfo_get_timestamp_m462964950 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	{
		double L_0 = __this->get_m_TimeStamp_0();
		return L_0;
	}
}
extern "C"  double NetworkMessageInfo_get_timestamp_m462964950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_timestamp_m462964950(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkMessageInfo::get_sender()
extern "C"  NetworkPlayer_t1243528291  NetworkMessageInfo_get_sender_m2366451889 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291  L_0 = __this->get_m_Sender_1();
		return L_0;
	}
}
extern "C"  NetworkPlayer_t1243528291  NetworkMessageInfo_get_sender_m2366451889_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_sender_m2366451889(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::get_networkView()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3523052579;
extern const uint32_t NetworkMessageInfo_get_networkView_m1180629659_MetadataUsageId;
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_get_networkView_m1180629659 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkMessageInfo_get_networkView_m1180629659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NetworkViewID_t3942988548  L_0 = __this->get_m_ViewID_2();
		NetworkViewID_t3942988548  L_1 = NetworkViewID_get_unassigned_m2814913999(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = NetworkViewID_op_Equality_m4173239775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3523052579, /*hidden argument*/NULL);
		NetworkView_t172525251 * L_3 = NetworkMessageInfo_NullNetworkView_m230600625(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0026:
	{
		NetworkViewID_t3942988548  L_4 = __this->get_m_ViewID_2();
		NetworkView_t172525251 * L_5 = NetworkView_Find_m143547961(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_get_networkView_m1180629659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_get_networkView_m1180629659(_thisAdjusted, method);
}
// UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_NullNetworkView_m230600625 (NetworkMessageInfo_t614064059 * __this, const MethodInfo* method)
{
	typedef NetworkView_t172525251 * (*NetworkMessageInfo_NullNetworkView_m230600625_ftn) (NetworkMessageInfo_t614064059 *);
	static NetworkMessageInfo_NullNetworkView_m230600625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkMessageInfo_NullNetworkView_m230600625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkMessageInfo::NullNetworkView()");
	return _il2cpp_icall_func(__this);
}
extern "C"  NetworkView_t172525251 * NetworkMessageInfo_NullNetworkView_m230600625_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkMessageInfo_t614064059 * _thisAdjusted = reinterpret_cast<NetworkMessageInfo_t614064059 *>(__this + 1);
	return NetworkMessageInfo_NullNetworkView_m230600625(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t614064059_marshal_pinvoke(const NetworkMessageInfo_t614064059& unmarshaled, NetworkMessageInfo_t614064059_marshaled_pinvoke& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t1243528291_marshal_pinvoke(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3942988548_marshal_pinvoke(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t614064059_marshal_pinvoke_back(const NetworkMessageInfo_t614064059_marshaled_pinvoke& marshaled, NetworkMessageInfo_t614064059& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t1243528291  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t1243528291_marshal_pinvoke_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3942988548  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3942988548_marshal_pinvoke_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t614064059_marshal_pinvoke_cleanup(NetworkMessageInfo_t614064059_marshaled_pinvoke& marshaled)
{
	NetworkPlayer_t1243528291_marshal_pinvoke_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3942988548_marshal_pinvoke_cleanup(marshaled.___m_ViewID_2);
}
// Conversion methods for marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t614064059_marshal_com(const NetworkMessageInfo_t614064059& unmarshaled, NetworkMessageInfo_t614064059_marshaled_com& marshaled)
{
	marshaled.___m_TimeStamp_0 = unmarshaled.get_m_TimeStamp_0();
	NetworkPlayer_t1243528291_marshal_com(unmarshaled.get_m_Sender_1(), marshaled.___m_Sender_1);
	NetworkViewID_t3942988548_marshal_com(unmarshaled.get_m_ViewID_2(), marshaled.___m_ViewID_2);
}
extern "C" void NetworkMessageInfo_t614064059_marshal_com_back(const NetworkMessageInfo_t614064059_marshaled_com& marshaled, NetworkMessageInfo_t614064059& unmarshaled)
{
	double unmarshaled_m_TimeStamp_temp_0 = 0.0;
	unmarshaled_m_TimeStamp_temp_0 = marshaled.___m_TimeStamp_0;
	unmarshaled.set_m_TimeStamp_0(unmarshaled_m_TimeStamp_temp_0);
	NetworkPlayer_t1243528291  unmarshaled_m_Sender_temp_1;
	memset(&unmarshaled_m_Sender_temp_1, 0, sizeof(unmarshaled_m_Sender_temp_1));
	NetworkPlayer_t1243528291_marshal_com_back(marshaled.___m_Sender_1, unmarshaled_m_Sender_temp_1);
	unmarshaled.set_m_Sender_1(unmarshaled_m_Sender_temp_1);
	NetworkViewID_t3942988548  unmarshaled_m_ViewID_temp_2;
	memset(&unmarshaled_m_ViewID_temp_2, 0, sizeof(unmarshaled_m_ViewID_temp_2));
	NetworkViewID_t3942988548_marshal_com_back(marshaled.___m_ViewID_2, unmarshaled_m_ViewID_temp_2);
	unmarshaled.set_m_ViewID_2(unmarshaled_m_ViewID_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkMessageInfo
extern "C" void NetworkMessageInfo_t614064059_marshal_com_cleanup(NetworkMessageInfo_t614064059_marshaled_com& marshaled)
{
	NetworkPlayer_t1243528291_marshal_com_cleanup(marshaled.___m_Sender_1);
	NetworkViewID_t3942988548_marshal_com_cleanup(marshaled.___m_ViewID_2);
}
// System.Void UnityEngine.NetworkPlayer::.ctor(System.String,System.Int32)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1640566403;
extern const uint32_t NetworkPlayer__ctor_m3970319947_MetadataUsageId;
extern "C"  void NetworkPlayer__ctor_m3970319947 (NetworkPlayer_t1243528291 * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer__ctor_m3970319947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1640566403, /*hidden argument*/NULL);
		__this->set_index_0(0);
		return;
	}
}
extern "C"  void NetworkPlayer__ctor_m3970319947_AdjustorThunk (Il2CppObject * __this, String_t* ___ip0, int32_t ___port1, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	NetworkPlayer__ctor_m3970319947(_thisAdjusted, ___ip0, ___port1, method);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetIPAddress_m2394140501 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn) (int32_t);
	static NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetIPAddress_m2394140501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
extern "C"  int32_t NetworkPlayer_Internal_GetPort_m3852797570 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPort_m3852797570_ftn) (int32_t);
	static NetworkPlayer_Internal_GetPort_m3852797570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPort_m3852797570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetExternalIP_m2789058499 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn) ();
	static NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalIP_m2789058499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetExternalPort_m2755622672 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn) ();
	static NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetExternalPort_m2755622672_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetExternalPort()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalIP_m2626871687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn) ();
	static NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalIP_m2626871687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalIP()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
extern "C"  int32_t NetworkPlayer_Internal_GetLocalPort_m2373269974 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn) ();
	static NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalPort_m2373269974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalPort()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
extern "C"  int32_t NetworkPlayer_Internal_GetPlayerIndex_m2582225917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn) ();
	static NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetPlayerIndex_m2582225917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
extern "C"  String_t* NetworkPlayer_Internal_GetGUID_m281231101 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetGUID_m281231101_ftn) (int32_t);
	static NetworkPlayer_Internal_GetGUID_m281231101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetGUID_m281231101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)");
	return _il2cpp_icall_func(___index0);
}
// System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
extern "C"  String_t* NetworkPlayer_Internal_GetLocalGUID_m49665029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn) ();
	static NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkPlayer_Internal_GetLocalGUID_m49665029_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkPlayer::Internal_GetLocalGUID()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.NetworkPlayer::GetHashCode()
extern "C"  int32_t NetworkPlayer_GetHashCode_m2788349399 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		int32_t L_1 = Int32_GetHashCode_m1381647448(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t NetworkPlayer_GetHashCode_m2788349399_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_GetHashCode_m2788349399(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkPlayer::Equals(System.Object)
extern Il2CppClass* NetworkPlayer_t1243528291_il2cpp_TypeInfo_var;
extern const uint32_t NetworkPlayer_Equals_m1960901089_MetadataUsageId;
extern "C"  bool NetworkPlayer_Equals_m1960901089 (NetworkPlayer_t1243528291 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkPlayer_Equals_m1960901089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkPlayer_t1243528291_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkPlayer_t1243528291 *)((NetworkPlayer_t1243528291 *)UnBox (L_1, NetworkPlayer_t1243528291_il2cpp_TypeInfo_var))));
		int32_t L_2 = (&V_0)->get_index_0();
		int32_t L_3 = __this->get_index_0();
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool NetworkPlayer_Equals_m1960901089_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_Equals_m1960901089(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.NetworkPlayer::get_ipAddress()
extern "C"  String_t* NetworkPlayer_get_ipAddress_m3020320555 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalIP_m2626871687(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetIPAddress_m2394140501(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_ipAddress_m3020320555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_ipAddress_m3020320555(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_port()
extern "C"  int32_t NetworkPlayer_get_port_m2797602516 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_2 = NetworkPlayer_Internal_GetLocalPort_m2373269974(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		int32_t L_4 = NetworkPlayer_Internal_GetPort_m3852797570(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  int32_t NetworkPlayer_get_port_m2797602516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_port_m2797602516(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_guid()
extern "C"  String_t* NetworkPlayer_get_guid_m1047639783 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_index_0();
		int32_t L_1 = NetworkPlayer_Internal_GetPlayerIndex_m2582225917(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_2 = NetworkPlayer_Internal_GetLocalGUID_m49665029(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		int32_t L_3 = __this->get_index_0();
		String_t* L_4 = NetworkPlayer_Internal_GetGUID_m281231101(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  String_t* NetworkPlayer_get_guid_m1047639783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_guid_m1047639783(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::ToString()
extern "C"  String_t* NetworkPlayer_ToString_m537794039 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		int32_t* L_0 = __this->get_address_of_index_0();
		String_t* L_1 = Int32_ToString_m2960866144(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* NetworkPlayer_ToString_m537794039_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_ToString_m537794039(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkPlayer::get_externalIP()
extern "C"  String_t* NetworkPlayer_get_externalIP_m343056966 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkPlayer_Internal_GetExternalIP_m2789058499(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkPlayer_get_externalIP_m343056966_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_externalIP_m343056966(_thisAdjusted, method);
}
// System.Int32 UnityEngine.NetworkPlayer::get_externalPort()
extern "C"  int32_t NetworkPlayer_get_externalPort_m114464875 (NetworkPlayer_t1243528291 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = NetworkPlayer_Internal_GetExternalPort_m2755622672(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  int32_t NetworkPlayer_get_externalPort_m114464875_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291 * _thisAdjusted = reinterpret_cast<NetworkPlayer_t1243528291 *>(__this + 1);
	return NetworkPlayer_get_externalPort_m114464875(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkPlayer::get_unassigned()
extern "C"  NetworkPlayer_t1243528291  NetworkPlayer_get_unassigned_m3961648431 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		(&V_0)->set_index_0((-1));
		NetworkPlayer_t1243528291  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Equality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Equality_m2104137122 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___lhs0, NetworkPlayer_t1243528291  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.NetworkPlayer::op_Inequality(UnityEngine.NetworkPlayer,UnityEngine.NetworkPlayer)
extern "C"  bool NetworkPlayer_op_Inequality_m3070881435 (Il2CppObject * __this /* static, unused */, NetworkPlayer_t1243528291  ___lhs0, NetworkPlayer_t1243528291  ___rhs1, const MethodInfo* method)
{
	{
		int32_t L_0 = (&___lhs0)->get_index_0();
		int32_t L_1 = (&___rhs1)->get_index_0();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t1243528291_marshal_pinvoke(const NetworkPlayer_t1243528291& unmarshaled, NetworkPlayer_t1243528291_marshaled_pinvoke& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t1243528291_marshal_pinvoke_back(const NetworkPlayer_t1243528291_marshaled_pinvoke& marshaled, NetworkPlayer_t1243528291& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t1243528291_marshal_pinvoke_cleanup(NetworkPlayer_t1243528291_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t1243528291_marshal_com(const NetworkPlayer_t1243528291& unmarshaled, NetworkPlayer_t1243528291_marshaled_com& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
}
extern "C" void NetworkPlayer_t1243528291_marshal_com_back(const NetworkPlayer_t1243528291_marshaled_com& marshaled, NetworkPlayer_t1243528291& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkPlayer
extern "C" void NetworkPlayer_t1243528291_marshal_com_cleanup(NetworkPlayer_t1243528291_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)
extern "C"  void NetworkView_Internal_GetViewID_m634412743 (NetworkView_t172525251 * __this, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	typedef void (*NetworkView_Internal_GetViewID_m634412743_ftn) (NetworkView_t172525251 *, NetworkViewID_t3942988548 *);
	static NetworkView_Internal_GetViewID_m634412743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_Internal_GetViewID_m634412743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::Internal_GetViewID(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(__this, ___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkView::get_viewID()
extern "C"  NetworkViewID_t3942988548  NetworkView_get_viewID_m1244180137 (NetworkView_t172525251 * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkView_Internal_GetViewID_m634412743(__this, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3942988548  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkView::get_isMine()
extern "C"  bool NetworkView_get_isMine_m1275126880 (NetworkView_t172525251 * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_t3942988548  L_0 = NetworkView_get_viewID_m1244180137(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = NetworkViewID_get_isMine_m1234363003((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkView::get_owner()
extern "C"  NetworkPlayer_t1243528291  NetworkView_get_owner_m4018248619 (NetworkView_t172525251 * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_t3942988548  L_0 = NetworkView_get_viewID_m1244180137(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		NetworkPlayer_t1243528291  L_1 = NetworkViewID_get_owner_m1900957708((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::Find(UnityEngine.NetworkViewID)
extern "C"  NetworkView_t172525251 * NetworkView_Find_m143547961 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___viewID0, const MethodInfo* method)
{
	{
		NetworkView_t172525251 * L_0 = NetworkView_INTERNAL_CALL_Find_m526018730(NULL /*static, unused*/, (&___viewID0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
extern "C"  NetworkView_t172525251 * NetworkView_INTERNAL_CALL_Find_m526018730 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	typedef NetworkView_t172525251 * (*NetworkView_INTERNAL_CALL_Find_m526018730_ftn) (NetworkViewID_t3942988548 *);
	static NetworkView_INTERNAL_CALL_Find_m526018730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkView_INTERNAL_CALL_Find_m526018730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___viewID0);
}
// UnityEngine.NetworkViewID UnityEngine.NetworkViewID::get_unassigned()
extern "C"  NetworkViewID_t3942988548  NetworkViewID_get_unassigned_m2814913999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_INTERNAL_get_unassigned_m132572206(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		NetworkViewID_t3942988548  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
extern "C"  void NetworkViewID_INTERNAL_get_unassigned_m132572206 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_get_unassigned_m132572206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_IsMine(UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_IsMine_m763014699 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_IsMine_m753595398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.NetworkViewID::Internal_GetOwner(UnityEngine.NetworkViewID,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_Internal_GetOwner_m89862041 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, NetworkPlayer_t1243528291 * ___player1, const MethodInfo* method)
{
	{
		NetworkPlayer_t1243528291 * L_0 = ___player1;
		NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604(NULL /*static, unused*/, (&___value0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
extern "C"  void NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, NetworkPlayer_t1243528291 * ___player1, const MethodInfo* method)
{
	typedef void (*NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn) (NetworkViewID_t3942988548 *, NetworkPlayer_t1243528291 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetOwner_m874918604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)");
	_il2cpp_icall_func(___value0, ___player1);
}
// System.String UnityEngine.NetworkViewID::Internal_GetString(UnityEngine.NetworkViewID)
extern "C"  String_t* NetworkViewID_Internal_GetString_m403971590 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
extern "C"  String_t* NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___value0, const MethodInfo* method)
{
	typedef String_t* (*NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn) (NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_GetString_m346869803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.NetworkViewID::Internal_Compare(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_Internal_Compare_m3248992772 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333(NULL /*static, unused*/, (&___lhs0), (&___rhs1), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
extern "C"  bool NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548 * ___lhs0, NetworkViewID_t3942988548 * ___rhs1, const MethodInfo* method)
{
	typedef bool (*NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn) (NetworkViewID_t3942988548 *, NetworkViewID_t3942988548 *);
	static NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NetworkViewID_INTERNAL_CALL_Internal_Compare_m61154333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)");
	return _il2cpp_icall_func(___lhs0, ___rhs1);
}
// System.Int32 UnityEngine.NetworkViewID::GetHashCode()
extern "C"  int32_t NetworkViewID_GetHashCode_m3141878442 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_a_0();
		int32_t L_1 = __this->get_b_1();
		int32_t L_2 = __this->get_c_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))^(int32_t)L_2));
	}
}
extern "C"  int32_t NetworkViewID_GetHashCode_m3141878442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_GetHashCode_m3141878442(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::Equals(System.Object)
extern Il2CppClass* NetworkViewID_t3942988548_il2cpp_TypeInfo_var;
extern const uint32_t NetworkViewID_Equals_m809788370_MetadataUsageId;
extern "C"  bool NetworkViewID_Equals_m809788370 (NetworkViewID_t3942988548 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NetworkViewID_Equals_m809788370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NetworkViewID_t3942988548  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, NetworkViewID_t3942988548_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(NetworkViewID_t3942988548 *)((NetworkViewID_t3942988548 *)UnBox (L_1, NetworkViewID_t3942988548_il2cpp_TypeInfo_var))));
		NetworkViewID_t3942988548  L_2 = V_0;
		bool L_3 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
extern "C"  bool NetworkViewID_Equals_m809788370_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_Equals_m809788370(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.NetworkViewID::get_isMine()
extern "C"  bool NetworkViewID_get_isMine_m1234363003 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	{
		bool L_0 = NetworkViewID_Internal_IsMine_m763014699(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  bool NetworkViewID_get_isMine_m1234363003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_get_isMine_m1234363003(_thisAdjusted, method);
}
// UnityEngine.NetworkPlayer UnityEngine.NetworkViewID::get_owner()
extern "C"  NetworkPlayer_t1243528291  NetworkViewID_get_owner_m1900957708 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	NetworkPlayer_t1243528291  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NetworkViewID_Internal_GetOwner_m89862041(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t1243528291  L_0 = V_0;
		return L_0;
	}
}
extern "C"  NetworkPlayer_t1243528291  NetworkViewID_get_owner_m1900957708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_get_owner_m1900957708(_thisAdjusted, method);
}
// System.String UnityEngine.NetworkViewID::ToString()
extern "C"  String_t* NetworkViewID_ToString_m3348378544 (NetworkViewID_t3942988548 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = NetworkViewID_Internal_GetString_m403971590(NULL /*static, unused*/, (*(NetworkViewID_t3942988548 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C"  String_t* NetworkViewID_ToString_m3348378544_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	NetworkViewID_t3942988548 * _thisAdjusted = reinterpret_cast<NetworkViewID_t3942988548 *>(__this + 1);
	return NetworkViewID_ToString_m3348378544(_thisAdjusted, method);
}
// System.Boolean UnityEngine.NetworkViewID::op_Equality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Equality_m4173239775 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3942988548  L_0 = ___lhs0;
		NetworkViewID_t3942988548  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.NetworkViewID::op_Inequality(UnityEngine.NetworkViewID,UnityEngine.NetworkViewID)
extern "C"  bool NetworkViewID_op_Inequality_m309368134 (Il2CppObject * __this /* static, unused */, NetworkViewID_t3942988548  ___lhs0, NetworkViewID_t3942988548  ___rhs1, const MethodInfo* method)
{
	{
		NetworkViewID_t3942988548  L_0 = ___lhs0;
		NetworkViewID_t3942988548  L_1 = ___rhs1;
		bool L_2 = NetworkViewID_Internal_Compare_m3248992772(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3942988548_marshal_pinvoke(const NetworkViewID_t3942988548& unmarshaled, NetworkViewID_t3942988548_marshaled_pinvoke& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3942988548_marshal_pinvoke_back(const NetworkViewID_t3942988548_marshaled_pinvoke& marshaled, NetworkViewID_t3942988548& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3942988548_marshal_pinvoke_cleanup(NetworkViewID_t3942988548_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3942988548_marshal_com(const NetworkViewID_t3942988548& unmarshaled, NetworkViewID_t3942988548_marshaled_com& marshaled)
{
	marshaled.___a_0 = unmarshaled.get_a_0();
	marshaled.___b_1 = unmarshaled.get_b_1();
	marshaled.___c_2 = unmarshaled.get_c_2();
}
extern "C" void NetworkViewID_t3942988548_marshal_com_back(const NetworkViewID_t3942988548_marshaled_com& marshaled, NetworkViewID_t3942988548& unmarshaled)
{
	int32_t unmarshaled_a_temp_0 = 0;
	unmarshaled_a_temp_0 = marshaled.___a_0;
	unmarshaled.set_a_0(unmarshaled_a_temp_0);
	int32_t unmarshaled_b_temp_1 = 0;
	unmarshaled_b_temp_1 = marshaled.___b_1;
	unmarshaled.set_b_1(unmarshaled_b_temp_1);
	int32_t unmarshaled_c_temp_2 = 0;
	unmarshaled_c_temp_2 = marshaled.___c_2;
	unmarshaled.set_c_2(unmarshaled_c_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.NetworkViewID
extern "C" void NetworkViewID_t3942988548_marshal_com_cleanup(NetworkViewID_t3942988548_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m197157284 (Object_t1021602117 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object__cctor_m2991092887_MetadataUsageId;
extern "C"  void Object__cctor_m2991092887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m2991092887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingle_m260620116 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingle_m260620116_ftn) (Object_t1021602117 *);
	static Object_Internal_CloneSingle_m260620116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m260620116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t1021602117 * Object_Internal_CloneSingleWithParent_m665572246 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_Internal_CloneSingleWithParent_m665572246_ftn) (Object_t1021602117 *, Transform_t3275118058 *, bool);
	static Object_Internal_CloneSingleWithParent_m665572246_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m665572246_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	return _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingle_m2776302597 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m2776302597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Vector3_t2243707580 * ___pos1, Quaternion_t4030073918 * ___rot2, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn) (Object_t1021602117 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m3932420250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Internal_InstantiateSingleWithParent_m509082884 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580  ___pos2, Quaternion_t4030073918  ___rot3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m509082884_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___data0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t1021602117 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___data0, Transform_t3275118058 * ___parent1, Vector3_t2243707580 * ___pos2, Quaternion_t4030073918 * ___rot3, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn) (Object_t1021602117 *, Transform_t3275118058 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m1401308849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m3042842193 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m3042842193_ftn) (Object_t1021602117 *);
	static Object_EnsureRunningOnMainThread_m3042842193_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m3042842193_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m4279412553_ftn) (Object_t1021602117 *, float);
	static Object_Destroy_m4279412553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m4279412553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Destroy_m4145850038_MetadataUsageId;
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m4145850038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m3563317232 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m3563317232_ftn) (Object_t1021602117 *, bool);
	static Object_DestroyImmediate_m3563317232_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m3563317232_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyImmediate_m95027445_MetadataUsageId;
extern "C"  void Object_DestroyImmediate_m95027445 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m95027445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t1021602117 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3563317232(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfType_m2121813744 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfType_m2121813744_ftn) (Type_t *);
	static Object_FindObjectsOfType_m2121813744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m2121813744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m2079638459_ftn) (Object_t1021602117 *);
	static Object_get_name_m2079638459_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m2079638459_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m4157836998_ftn) (Object_t1021602117 *, String_t*);
	static Object_set_name_m4157836998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m4157836998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m2330762974_ftn) (Object_t1021602117 *);
	static Object_DontDestroyOnLoad_m2330762974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m2330762974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m4158950869 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m4158950869_ftn) (Object_t1021602117 *);
	static Object_get_hideFlags_m4158950869_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m4158950869_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m2204253440 (Object_t1021602117 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m2204253440_ftn) (Object_t1021602117 *, int32_t);
	static Object_set_hideFlags_m2204253440_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2204253440_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m282495858 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m282495858_ftn) (Object_t1021602117 *, float);
	static Object_DestroyObject_m282495858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m282495858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_DestroyObject_m2343493981_MetadataUsageId;
extern "C"  void Object_DestroyObject_m2343493981 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m2343493981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t1021602117 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DestroyObject_m282495858(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindSceneObjectsOfType_m1833688338 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindSceneObjectsOfType_m1833688338_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m1833688338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m1833688338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t4217747464* Object_FindObjectsOfTypeIncludingAssets_m3988851426 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m3988851426_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m1947404527 (Object_t1021602117 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m1947404527_ftn) (Object_t1021602117 *);
	static Object_ToString_m1947404527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m1947404527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m2570795274 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m2570795274_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m2570795274_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m2570795274_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m1920497914_MetadataUsageId;
extern "C"  int32_t Object_GetInstanceID_m1920497914 (Object_t1021602117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m1920497914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return 0;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m1587840561(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_002c:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m39971741(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t1021602117_StaticFields*)Object_t1021602117_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m3803259710(&L_8, ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (*((int32_t*)L_9));
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m3431642059 (Object_t1021602117 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetHashCode_m1715190285(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Equals_m4029628913_MetadataUsageId;
extern "C"  bool Object_Equals_m4029628913 (Object_t1021602117 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m4029628913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___o0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, __this, ((Object_t1021602117 *)IsInstClass(L_0, Object_t1021602117_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_CompareBaseObjects_m3953996214_MetadataUsageId;
extern "C"  bool Object_CompareBaseObjects_m3953996214 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___lhs0, Object_t1021602117 * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m3953996214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t1021602117 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t1021602117 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t1021602117 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t1021602117 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t1021602117 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m4056217615(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t1021602117 * L_10 = ___lhs0;
		Object_t1021602117 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_IsNativeObjectAlive_m4056217615_MetadataUsageId;
extern "C"  bool Object_IsNativeObjectAlive_m4056217615 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m4056217615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m943750213(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m943750213 (Object_t1021602117 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m938141395_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m938141395 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m938141395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		Vector3_t2243707580  L_2 = ___position1;
		Quaternion_t4030073918  L_3 = ___rotation2;
		Object_t1021602117 * L_4 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2160322936_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2160322936 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Vector3_t2243707580  ___position1, Quaternion_t4030073918  ___rotation2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2160322936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		Vector3_t2243707580  L_3 = ___position1;
		Quaternion_t4030073918  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_5 = Object_Internal_InstantiateSingle_m2776302597(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0015:
	{
		Object_t1021602117 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_6, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_7 = ___original0;
		Transform_t3275118058 * L_8 = ___parent3;
		Vector3_t2243707580  L_9 = ___position1;
		Quaternion_t4030073918  L_10 = ___rotation2;
		Object_t1021602117 * L_11 = Object_Internal_InstantiateSingleWithParent_m509082884(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2439155489_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2439155489 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2439155489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_0, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___original0;
		Object_t1021602117 * L_2 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_Instantiate_m2177117080_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2177117080 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2177117080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___original0;
		Transform_t3275118058 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_2 = Object_Instantiate_m2489341053(NULL /*static, unused*/, L_0, L_1, (bool)1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral444318565;
extern const uint32_t Object_Instantiate_m2489341053_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_Instantiate_m2489341053 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___original0, Transform_t3275118058 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2489341053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Object_t1021602117 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_Internal_CloneSingle_m260620116(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0013:
	{
		Object_t1021602117 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m1711119106(NULL /*static, unused*/, L_4, _stringLiteral444318565, /*hidden argument*/NULL);
		Object_t1021602117 * L_5 = ___original0;
		Transform_t3275118058 * L_6 = ___parent1;
		bool L_7 = ___worldPositionStays2;
		Object_t1021602117 * L_8 = Object_Internal_CloneSingleWithParent_m665572246(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern const uint32_t Object_CheckNullArgument_m1711119106_MetadataUsageId;
extern "C"  void Object_CheckNullArgument_m1711119106 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m1711119106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t3259014390 * L_2 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3739475201(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_m2330404063_MetadataUsageId;
extern "C"  Object_t1021602117 * Object_FindObjectOfType_m2330404063 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m2330404063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t4217747464* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_1 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t4217747464* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t4217747464* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		Object_t1021602117 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}

IL_0014:
	{
		return (Object_t1021602117 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Implicit_m2856731593_MetadataUsageId;
extern "C"  bool Object_op_Implicit_m2856731593 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m2856731593_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Equality_m3764089466_MetadataUsageId;
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m3764089466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Object_op_Inequality_m2402264703_MetadataUsageId;
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___x0, Object_t1021602117 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m2402264703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t1021602117 * L_0 = ___x0;
		Object_t1021602117 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m3953996214(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_pinvoke_back(const Object_t1021602117_marshaled_pinvoke& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_pinvoke_cleanup(Object_t1021602117_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com(const Object_t1021602117& unmarshaled, Object_t1021602117_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t1021602117_marshal_com_back(const Object_t1021602117_marshaled_com& marshaled, Object_t1021602117& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t1021602117_marshal_com_cleanup(Object_t1021602117_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Physics::set_gravity(UnityEngine.Vector3)
extern "C"  void Physics_set_gravity_m2705728856 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Physics_INTERNAL_set_gravity_m2999035988(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
extern "C"  void Physics_INTERNAL_set_gravity_m2999035988 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_set_gravity_m2999035988_ftn) (Vector3_t2243707580 *);
	static Physics_INTERNAL_set_gravity_m2999035988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_set_gravity_m2999035988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1929115794 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m2036777053 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___origin0;
		Vector3_t2243707580  L_1 = ___direction1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m1160243045(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m2009151399 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2308457076 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		RaycastHit_t87180320 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m233619224(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m233619224 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, RaycastHit_t87180320 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t87180320 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m233036521 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1214023521* L_4 = Physics_RaycastAll_m410413656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m410413656 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t1214023521* L_5 = Physics_RaycastAll_m3908263591(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3908263591 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_RaycastAll_m3256436970 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, float ___maxDistance2, int32_t ___layermask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1214023521* L_3 = Physics_INTERNAL_CALL_RaycastAll_m2126789092(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1214023521* Physics_INTERNAL_CALL_RaycastAll_m2126789092 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t1214023521* (*Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m2126789092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
extern "C"  bool Physics_Linecast_m1120910627 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, RaycastHit_t87180320 * ___hitInfo2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		Vector3_t2243707580  L_0 = ___start0;
		Vector3_t2243707580  L_1 = ___end1;
		RaycastHit_t87180320 * L_2 = ___hitInfo2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Linecast_m1896185200(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Linecast_m1896185200 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, RaycastHit_t87180320 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = ___end1;
		Vector3_t2243707580  L_1 = ___start0;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = ___start0;
		Vector3_t2243707580  L_4 = V_0;
		RaycastHit_t87180320 * L_5 = ___hitInfo2;
		float L_6 = Vector3_get_magnitude_m860342598((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = ___layerMask3;
		int32_t L_8 = ___queryTriggerInteraction4;
		bool L_9 = Physics_Raycast_m2036777053(NULL /*static, unused*/, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single)
extern "C"  ColliderU5BU5D_t462843629* Physics_OverlapSphere_m3967613486 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___position0, float ___radius1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = (-1);
		float L_0 = ___radius1;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		ColliderU5BU5D_t462843629* L_3 = Physics_INTERNAL_CALL_OverlapSphere_m2407088301(NULL /*static, unused*/, (&___position0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t462843629* Physics_INTERNAL_CALL_OverlapSphere_m2407088301 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	typedef ColliderU5BU5D_t462843629* (*Physics_INTERNAL_CALL_OverlapSphere_m2407088301_ftn) (Vector3_t2243707580 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_OverlapSphere_m2407088301_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_OverlapSphere_m2407088301_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___position0, ___radius1, ___layerMask2, ___queryTriggerInteraction3);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1160243045 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	{
		RaycastHit_t87180320 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m93849932(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m93849932 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___origin0, Vector3_t2243707580 * ___direction1, RaycastHit_t87180320 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, RaycastHit_t87180320 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m93849932_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern Il2CppClass* List_1_t4166282325_il2cpp_TypeInfo_var;
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2338710192_MethodInfo_var;
extern const uint32_t Physics2D__cctor_m3532647019_MetadataUsageId;
extern "C"  void Physics2D__cctor_m3532647019 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m3532647019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4166282325 * L_0 = (List_1_t4166282325 *)il2cpp_codegen_object_new(List_1_t4166282325_il2cpp_TypeInfo_var);
		List_1__ctor_m2338710192(L_0, /*hidden argument*/List_1__ctor_m2338710192_MethodInfo_var);
		((Physics2D_t2540166467_StaticFields*)Physics2D_t2540166467_il2cpp_TypeInfo_var->static_fields)->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Physics2D::set_gravity(UnityEngine.Vector2)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_set_gravity_m1622171379_MetadataUsageId;
extern "C"  void Physics2D_set_gravity_m1622171379 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_set_gravity_m1622171379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_set_gravity_m3564632273(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)
extern "C"  void Physics2D_INTERNAL_set_gravity_m3564632273 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_set_gravity_m3564632273_ftn) (Vector2_t2243707579 *);
	static Physics2D_INTERNAL_set_gravity_m3564632273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_set_gravity_m3564632273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Physics2D::Internal_Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_Linecast_m1593067799_MetadataUsageId;
extern "C"  void Physics2D_Internal_Linecast_m1593067799 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___start0, Vector2_t2243707579  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t4063908774 * ___raycastHit5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Linecast_m1593067799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		RaycastHit2D_t4063908774 * L_3 = ___raycastHit5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Linecast_m578113300(NULL /*static, unused*/, (&___start0), (&___end1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Linecast_m578113300 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___start0, Vector2_t2243707579 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, RaycastHit2D_t4063908774 * ___raycastHit5, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Linecast_m578113300_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, int32_t, float, float, RaycastHit2D_t4063908774 *);
	static Physics2D_INTERNAL_CALL_Internal_Linecast_m578113300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Linecast_m578113300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___start0, ___end1, ___layerMask2, ___minDepth3, ___maxDepth4, ___raycastHit5);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Linecast_m2374117908_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Linecast_m2374117908 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___start0, Vector2_t2243707579  ___end1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_m2374117908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___start0;
		Vector2_t2243707579  L_1 = ___end1;
		int32_t L_2 = ___layerMask2;
		float L_3 = V_1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_5 = Physics2D_Linecast_m3607674036(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Linecast_m3607674036_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Linecast_m3607674036 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___start0, Vector2_t2243707579  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Linecast_m3607674036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t4063908774  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___start0;
		Vector2_t2243707579  L_1 = ___end1;
		int32_t L_2 = ___layerMask2;
		float L_3 = ___minDepth3;
		float L_4 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_Internal_Linecast_m1593067799(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t4063908774  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_LinecastAll_m940686717_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_LinecastAll_m940686717 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___start0, Vector2_t2243707579  ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_m940686717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_3 = Physics2D_INTERNAL_CALL_LinecastAll_m2740607626(NULL /*static, unused*/, (&___start0), (&___end1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::LinecastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_LinecastAll_m2255970717_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_LinecastAll_m2255970717 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___start0, Vector2_t2243707579  ___end1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_LinecastAll_m2255970717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___layerMask2;
		float L_1 = V_1;
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_3 = Physics2D_INTERNAL_CALL_LinecastAll_m2740607626(NULL /*static, unused*/, (&___start0), (&___end1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_INTERNAL_CALL_LinecastAll_m2740607626 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___start0, Vector2_t2243707579 * ___end1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t4176517891* (*Physics2D_INTERNAL_CALL_LinecastAll_m2740607626_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_LinecastAll_m2740607626_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_LinecastAll_m2740607626_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___start0, ___end1, ___layerMask2, ___minDepth3, ___maxDepth4);
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_Raycast_m683685528_MetadataUsageId;
extern "C"  void Physics2D_Internal_Raycast_m683685528 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t4063908774 * ___raycastHit6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Raycast_m683685528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = ___minDepth4;
		float L_3 = ___maxDepth5;
		RaycastHit2D_t4063908774 * L_4 = ___raycastHit6;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, RaycastHit2D_t4063908774 * ___raycastHit6, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, float, int32_t, float, float, RaycastHit2D_t4063908774 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m3956067819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___minDepth4, ___maxDepth5, ___raycastHit6);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m122312471_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m122312471 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m122312471_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_6 = Physics2D_Raycast_m2303387255(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m2303387255_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2303387255 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2303387255_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t4063908774  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___origin0;
		Vector2_t2243707579  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = ___maxDepth5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m683685528(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t4063908774  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_RaycastAll_m2212121930_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_RaycastAll_m2212121930 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_m2212121930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = ___minDepth4;
		float L_3 = ___maxDepth5;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m2386382509(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_RaycastAll_m3373320618_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_RaycastAll_m3373320618 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___origin0, Vector2_t2243707579  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_RaycastAll_m3373320618_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance2;
		int32_t L_1 = ___layerMask3;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m2386382509(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_INTERNAL_CALL_RaycastAll_m2386382509 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___origin0, Vector2_t2243707579 * ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t4176517891* (*Physics2D_INTERNAL_CALL_RaycastAll_m2386382509_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m2386382509_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m2386382509_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___layerMask3, ___minDepth4, ___maxDepth5);
}
// System.Void UnityEngine.Physics2D::Internal_GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Internal_GetRayIntersection_m1668677285_MetadataUsageId;
extern "C"  void Physics2D_Internal_GetRayIntersection_m1668677285 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t4063908774 * ___raycastHit3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_GetRayIntersection_m1668677285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		RaycastHit2D_t4063908774 * L_2 = ___raycastHit3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m3764202486(NULL /*static, unused*/, (&___ray0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m3764202486 (Il2CppObject * __this /* static, unused */, Ray_t2469606224 * ___ray0, float ___distance1, int32_t ___layerMask2, RaycastHit2D_t4063908774 * ___raycastHit3, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m3764202486_ftn) (Ray_t2469606224 *, float, int32_t, RaycastHit2D_t4063908774 *);
	static Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m3764202486_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_GetRayIntersection_m3764202486_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___ray0, ___distance1, ___layerMask2, ___raycastHit3);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersection_m2295701053_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_GetRayIntersection_m2295701053 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_m2295701053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)-5);
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___distance1;
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_3 = Physics2D_GetRayIntersection_m1570215952(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::GetRayIntersection(UnityEngine.Ray,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersection_m1570215952_MetadataUsageId;
extern "C"  RaycastHit2D_t4063908774  Physics2D_GetRayIntersection_m1570215952 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersection_m1570215952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t4063908774  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Ray_t2469606224  L_0 = ___ray0;
		float L_1 = ___distance1;
		int32_t L_2 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_Internal_GetRayIntersection_m1668677285(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t4063908774  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_GetRayIntersectionAll_m253330691_MetadataUsageId;
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_GetRayIntersectionAll_m253330691 (Il2CppObject * __this /* static, unused */, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m253330691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4176517891* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4176517891* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998 (Il2CppObject * __this /* static, unused */, Ray_t2469606224 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t4176517891* (*Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn) (Ray_t2469606224 *, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m161475998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray0, ___distance1, ___layerMask2);
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapPointAll_m986417785_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_OverlapPointAll_m986417785 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_m986417785_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask1;
		float L_1 = ___minDepth2;
		float L_2 = ___maxDepth3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t3535523695* L_3 = Physics2D_INTERNAL_CALL_OverlapPointAll_m398803016(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapPointAll(UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapPointAll_m2474093721_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_OverlapPointAll_m2474093721 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, int32_t ___layerMask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPointAll_m2474093721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___layerMask1;
		float L_1 = V_1;
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t3535523695* L_3 = Physics2D_INTERNAL_CALL_OverlapPointAll_m398803016(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_INTERNAL_CALL_OverlapPointAll_m398803016 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method)
{
	typedef Collider2DU5BU5D_t3535523695* (*Physics2D_INTERNAL_CALL_OverlapPointAll_m398803016_ftn) (Vector2_t2243707579 *, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_OverlapPointAll_m398803016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_OverlapPointAll_m398803016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_OverlapPointAll(UnityEngine.Vector2&,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___point0, ___layerMask1, ___minDepth2, ___maxDepth3);
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapCircleAll_m876788576_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_OverlapCircleAll_m876788576 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_m876788576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___radius1;
		int32_t L_1 = ___layerMask2;
		float L_2 = ___minDepth3;
		float L_3 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t3535523695* L_4 = Physics2D_INTERNAL_CALL_OverlapCircleAll_m3054339757(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapCircleAll(UnityEngine.Vector2,System.Single,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapCircleAll_m3232836672_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_OverlapCircleAll_m3232836672 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapCircleAll_m3232836672_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___radius1;
		int32_t L_1 = ___layerMask2;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t3535523695* L_4 = Physics2D_INTERNAL_CALL_OverlapCircleAll_m3054339757(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_INTERNAL_CALL_OverlapCircleAll_m3054339757 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___point0, float ___radius1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	typedef Collider2DU5BU5D_t3535523695* (*Physics2D_INTERNAL_CALL_OverlapCircleAll_m3054339757_ftn) (Vector2_t2243707579 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_OverlapCircleAll_m3054339757_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_OverlapCircleAll_m3054339757_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_OverlapCircleAll(UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___point0, ___radius1, ___layerMask2, ___minDepth3, ___maxDepth4);
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapAreaAll_m525743120_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_OverlapAreaAll_m525743120 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___pointA0, Vector2_t2243707579  ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAll_m525743120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask2;
		float L_1 = ___minDepth3;
		float L_2 = ___maxDepth4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t3535523695* L_3 = Physics2D_INTERNAL_CALL_OverlapAreaAll_m2962112631(NULL /*static, unused*/, (&___pointA0), (&___pointB1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::OverlapAreaAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern Il2CppClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_OverlapAreaAll_m1743869552_MetadataUsageId;
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_OverlapAreaAll_m1743869552 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___pointA0, Vector2_t2243707579  ___pointB1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapAreaAll_m1743869552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		int32_t L_0 = ___layerMask2;
		float L_1 = V_1;
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Collider2DU5BU5D_t3535523695* L_3 = Physics2D_INTERNAL_CALL_OverlapAreaAll_m2962112631(NULL /*static, unused*/, (&___pointA0), (&___pointB1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)
extern "C"  Collider2DU5BU5D_t3535523695* Physics2D_INTERNAL_CALL_OverlapAreaAll_m2962112631 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___pointA0, Vector2_t2243707579 * ___pointB1, int32_t ___layerMask2, float ___minDepth3, float ___maxDepth4, const MethodInfo* method)
{
	typedef Collider2DU5BU5D_t3535523695* (*Physics2D_INTERNAL_CALL_OverlapAreaAll_m2962112631_ftn) (Vector2_t2243707579 *, Vector2_t2243707579 *, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_OverlapAreaAll_m2962112631_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_OverlapAreaAll_m2962112631_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___pointA0, ___pointB1, ___layerMask2, ___minDepth3, ___maxDepth4);
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m3187718367 (Plane_t3727654732 * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___inNormal0;
		Vector3_t2243707580  L_1 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t2243707580  L_2 = ___inNormal0;
		Vector3_t2243707580  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m3187718367_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___inNormal0, Vector3_t2243707580  ___inPoint1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	Plane__ctor_m3187718367(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823 (Plane_t3727654732 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_0();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Plane_get_normal_m1872443823_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_get_normal_m1872443823(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m1834776091 (Plane_t3727654732 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_1();
		return L_0;
	}
}
extern "C"  float Plane_get_distance_m1834776091_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_get_distance_m1834776091(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m2870142810_MetadataUsageId;
extern "C"  bool Plane_Raycast_m2870142810 (Plane_t3727654732 * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m2870142810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t2243707580  L_0 = Ray_get_direction_m4059191533((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580  L_3 = Ray_get_origin_m3339262500((&___ray0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Plane_get_normal_m1872443823(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m3161182818(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m1834776091(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		return (bool)0;
	}

IL_0047:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		return (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
extern "C"  bool Plane_Raycast_m2870142810_AdjustorThunk (Il2CppObject * __this, Ray_t2469606224  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t3727654732 * _thisAdjusted = reinterpret_cast<Plane_t3727654732 *>(__this + 1);
	return Plane_Raycast_m2870142810(_thisAdjusted, ___ray0, ___enter1, method);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t3727654732_marshal_pinvoke(const Plane_t3727654732& unmarshaled, Plane_t3727654732_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t3727654732_marshal_pinvoke_back(const Plane_t3727654732_marshaled_pinvoke& marshaled, Plane_t3727654732& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t3727654732_marshal_pinvoke_cleanup(Plane_t3727654732_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Normal_0);
}
// Conversion methods for marshalling of: UnityEngine.Plane
extern "C" void Plane_t3727654732_marshal_com(const Plane_t3727654732& unmarshaled, Plane_t3727654732_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Normal_0(), marshaled.___m_Normal_0);
	marshaled.___m_Distance_1 = unmarshaled.get_m_Distance_1();
}
extern "C" void Plane_t3727654732_marshal_com_back(const Plane_t3727654732_marshaled_com& marshaled, Plane_t3727654732& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Normal_temp_0;
	memset(&unmarshaled_m_Normal_temp_0, 0, sizeof(unmarshaled_m_Normal_temp_0));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Normal_0, unmarshaled_m_Normal_temp_0);
	unmarshaled.set_m_Normal_0(unmarshaled_m_Normal_temp_0);
	float unmarshaled_m_Distance_temp_1 = 0.0f;
	unmarshaled_m_Distance_temp_1 = marshaled.___m_Distance_1;
	unmarshaled.set_m_Distance_1(unmarshaled_m_Distance_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Plane
extern "C" void Plane_t3727654732_marshal_com_cleanup(Plane_t3727654732_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Normal_0);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C"  bool PlayerPrefs_TrySetInt_m965228897 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m965228897_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m965228897_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m965228897_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
extern "C"  bool PlayerPrefs_TrySetFloat_m2145496754 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetFloat_m2145496754_ftn) (String_t*, float);
	static PlayerPrefs_TrySetFloat_m2145496754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetFloat_m2145496754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C"  bool PlayerPrefs_TrySetSetString_m3460332240 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m3460332240_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m3460332240_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m3460332240_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___value1);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern Il2CppClass* PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2331435015;
extern const uint32_t PlayerPrefs_SetInt_m3351928596_MetadataUsageId;
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetInt_m3351928596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetInt_m965228897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3229544204 * L_3 = (PlayerPrefsException_t3229544204 *)il2cpp_codegen_object_new(PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m1748847897(L_3, _stringLiteral2331435015, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m136681260_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m136681260_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m136681260_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
extern Il2CppClass* PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2331435015;
extern const uint32_t PlayerPrefs_SetFloat_m1496426569_MetadataUsageId;
extern "C"  void PlayerPrefs_SetFloat_m1496426569 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetFloat_m1496426569_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		float L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetFloat_m2145496754(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3229544204 * L_3 = (PlayerPrefsException_t3229544204 *)il2cpp_codegen_object_new(PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m1748847897(L_3, _stringLiteral2331435015, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
extern "C"  float PlayerPrefs_GetFloat_m2188986939 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, const MethodInfo* method)
{
	typedef float (*PlayerPrefs_GetFloat_m2188986939_ftn) (String_t*, float);
	static PlayerPrefs_GetFloat_m2188986939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetFloat_m2188986939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern Il2CppClass* PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2331435015;
extern const uint32_t PlayerPrefs_SetString_m2547809843_MetadataUsageId;
extern "C"  void PlayerPrefs_SetString_m2547809843 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefs_SetString_m2547809843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		bool L_2 = PlayerPrefs_TrySetSetString_m3460332240(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t3229544204 * L_3 = (PlayerPrefsException_t3229544204 *)il2cpp_codegen_object_new(PlayerPrefsException_t3229544204_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m1748847897(L_3, _stringLiteral2331435015, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C"  String_t* PlayerPrefs_GetString_m1246323344 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m1246323344_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m1246323344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m1246323344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key0, ___defaultValue1);
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C"  bool PlayerPrefs_HasKey_m1212656251 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m1212656251_ftn) (String_t*);
	static PlayerPrefs_HasKey_m1212656251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m1212656251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
extern "C"  void PlayerPrefs_DeleteKey_m2519743590 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteKey_m2519743590_ftn) (String_t*);
	static PlayerPrefs_DeleteKey_m2519743590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteKey_m2519743590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteKey(System.String)");
	_il2cpp_icall_func(___key0);
}
// System.Void UnityEngine.PlayerPrefs::DeleteAll()
extern "C"  void PlayerPrefs_DeleteAll_m345245756 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*PlayerPrefs_DeleteAll_m345245756_ftn) ();
	static PlayerPrefs_DeleteAll_m345245756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_DeleteAll_m345245756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::DeleteAll()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C"  void PlayerPrefsException__ctor_m1748847897 (PlayerPrefsException_t3229544204 * __this, String_t* ___error0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error0;
		Exception__ctor_m485833136(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3663555848 (PropertyAttribute_t2606999759 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3196903881 (Quaternion_t4030073918 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m3196903881_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	Quaternion__ctor_m3196903881(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t4030073918  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m3196903881(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m952616600 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_AngleAxis_m2806222563 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580  ___axis1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m3310327005(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m3310327005 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t2243707580 * ___axis1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn) (float, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m3310327005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_FromToRotation_m1685306068 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___fromDirection0, Vector3_t2243707580  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m2839236110(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m2839236110 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___fromDirection0, Vector3_t2243707580 * ___toDirection1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m2839236110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m700700634 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___forward0, Vector3_t2243707580  ___upwards1, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m3606560944(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_LookRotation_m633695927 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___forward0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t4030073918  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t2243707580  L_0 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_INTERNAL_CALL_LookRotation_m3606560944(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m3606560944 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___forward0, Vector3_t2243707580 * ___upwards1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m3606560944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Slerp_m1992855400 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Slerp_m1926970492(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m1926970492 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m1926970492_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Slerp_m1926970492_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m1926970492_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::SlerpUnclamped(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_SlerpUnclamped_m276819893 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_SlerpUnclamped_m2272819665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Lerp_m3623055223 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m1920404743(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m1920404743 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___a0, Quaternion_t4030073918 * ___b1, float ___t2, Quaternion_t4030073918 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *, float, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m1920404743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_RotateTowards_m83980725_MetadataUsageId;
extern "C"  Quaternion_t4030073918  Quaternion_RotateTowards_m83980725 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___from0, Quaternion_t4030073918  ___to1, float ___maxDegreesDelta2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_RotateTowards_m83980725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t4030073918  L_0 = ___from0;
		Quaternion_t4030073918  L_1 = ___to1;
		float L_2 = Quaternion_Angle_m1045775740(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		Quaternion_t4030073918  L_4 = ___to1;
		return L_4;
	}

IL_0015:
	{
		float L_5 = ___maxDegreesDelta2;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m1648492575(NULL /*static, unused*/, (1.0f), ((float)((float)L_5/(float)L_6)), /*hidden argument*/NULL);
		V_1 = L_7;
		Quaternion_t4030073918  L_8 = ___from0;
		Quaternion_t4030073918  L_9 = ___to1;
		float L_10 = V_1;
		Quaternion_t4030073918  L_11 = Quaternion_SlerpUnclamped_m276819893(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_Inverse_m3931399088 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Inverse_m1043108654(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1043108654 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn) (Quaternion_t4030073918 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m1043108654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// System.String UnityEngine.Quaternion::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3587482509;
extern const uint32_t Quaternion_ToString_m2638853272_MetadataUsageId;
extern "C"  String_t* Quaternion_ToString_m2638853272 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m2638853272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3587482509, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Quaternion_ToString_m2638853272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_ToString_m2638853272(_thisAdjusted, method);
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Angle_m1045775740_MetadataUsageId;
extern "C"  float Quaternion_Angle_m1045775740 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___a0, Quaternion_t4030073918  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Angle_m1045775740_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		Quaternion_t4030073918  L_0 = ___a0;
		Quaternion_t4030073918  L_1 = ___b1;
		float L_2 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m1648492575(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		return ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Quaternion_Internal_ToEulerRad_m2807508879(NULL /*static, unused*/, (*(Quaternion_t4030073918 *)__this), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Quaternion_Internal_MakePositive_m2921671247(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector3_t2243707580  Quaternion_get_eulerAngles_m3302573991_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_get_eulerAngles_m3302573991(_thisAdjusted, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t2243707580  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m2638739322(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m3586339259 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___euler0;
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_Internal_FromEulerRad_m1121344272(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_Internal_MakePositive_m2921671247 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (-0.005729578f);
		float L_0 = V_0;
		V_1 = ((float)((float)(360.0f)+(float)L_0));
		float L_1 = (&___euler0)->get_x_1();
		float L_2 = V_0;
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_0033;
		}
	}
	{
		Vector3_t2243707580 * L_3 = (&___euler0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4+(float)(360.0f))));
		goto IL_0053;
	}

IL_0033:
	{
		float L_5 = (&___euler0)->get_x_1();
		float L_6 = V_1;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0053;
		}
	}
	{
		Vector3_t2243707580 * L_7 = (&___euler0);
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8-(float)(360.0f))));
	}

IL_0053:
	{
		float L_9 = (&___euler0)->get_y_2();
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0078;
		}
	}
	{
		Vector3_t2243707580 * L_11 = (&___euler0);
		float L_12 = L_11->get_y_2();
		L_11->set_y_2(((float)((float)L_12+(float)(360.0f))));
		goto IL_0098;
	}

IL_0078:
	{
		float L_13 = (&___euler0)->get_y_2();
		float L_14 = V_1;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0098;
		}
	}
	{
		Vector3_t2243707580 * L_15 = (&___euler0);
		float L_16 = L_15->get_y_2();
		L_15->set_y_2(((float)((float)L_16-(float)(360.0f))));
	}

IL_0098:
	{
		float L_17 = (&___euler0)->get_z_3();
		float L_18 = V_0;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00bd;
		}
	}
	{
		Vector3_t2243707580 * L_19 = (&___euler0);
		float L_20 = L_19->get_z_3();
		L_19->set_z_3(((float)((float)L_20+(float)(360.0f))));
		goto IL_00dd;
	}

IL_00bd:
	{
		float L_21 = (&___euler0)->get_z_3();
		float L_22 = V_1;
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00dd;
		}
	}
	{
		Vector3_t2243707580 * L_23 = (&___euler0);
		float L_24 = L_23->get_z_3();
		L_23->set_z_3(((float)((float)L_24-(float)(360.0f))));
	}

IL_00dd:
	{
		Vector3_t2243707580  L_25 = ___euler0;
		return L_25;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t2243707580  Quaternion_Internal_ToEulerRad_m2807508879 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918 * ___rotation0, Vector3_t2243707580 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn) (Quaternion_t4030073918 *, Vector3_t2243707580 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m1077217777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_Internal_FromEulerRad_m1121344272 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___euler0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___euler0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn) (Vector3_t2243707580 *, Quaternion_t4030073918 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1113788132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m2270520528 (Quaternion_t4030073918 * __this, const MethodInfo* method)
{
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m3102305584(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m3102305584(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m3102305584(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m3102305584(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m2270520528_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_GetHashCode_m2270520528(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m3730391696_MetadataUsageId;
extern "C"  bool Quaternion_Equals_m3730391696 (Quaternion_t4030073918 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m3730391696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t4030073918_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox (L_1, Quaternion_t4030073918_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_0)->get_x_1();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_0)->get_y_2();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_0)->get_z_3();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_0)->get_w_4();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Quaternion_Equals_m3730391696_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t4030073918 * _thisAdjusted = reinterpret_cast<Quaternion_t4030073918 *>(__this + 1);
	return Quaternion_Equals_m3730391696(_thisAdjusted, ___other0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  Quaternion_op_Multiply_m2426727589 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	{
		float L_0 = (&___lhs0)->get_w_4();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_x_1();
		float L_3 = (&___rhs1)->get_w_4();
		float L_4 = (&___lhs0)->get_y_2();
		float L_5 = (&___rhs1)->get_z_3();
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_y_2();
		float L_8 = (&___lhs0)->get_w_4();
		float L_9 = (&___rhs1)->get_y_2();
		float L_10 = (&___lhs0)->get_y_2();
		float L_11 = (&___rhs1)->get_w_4();
		float L_12 = (&___lhs0)->get_z_3();
		float L_13 = (&___rhs1)->get_x_1();
		float L_14 = (&___lhs0)->get_x_1();
		float L_15 = (&___rhs1)->get_z_3();
		float L_16 = (&___lhs0)->get_w_4();
		float L_17 = (&___rhs1)->get_z_3();
		float L_18 = (&___lhs0)->get_z_3();
		float L_19 = (&___rhs1)->get_w_4();
		float L_20 = (&___lhs0)->get_x_1();
		float L_21 = (&___rhs1)->get_y_2();
		float L_22 = (&___lhs0)->get_y_2();
		float L_23 = (&___rhs1)->get_x_1();
		float L_24 = (&___lhs0)->get_w_4();
		float L_25 = (&___rhs1)->get_w_4();
		float L_26 = (&___lhs0)->get_x_1();
		float L_27 = (&___rhs1)->get_x_1();
		float L_28 = (&___lhs0)->get_y_2();
		float L_29 = (&___rhs1)->get_y_2();
		float L_30 = (&___lhs0)->get_z_3();
		float L_31 = (&___rhs1)->get_z_3();
		Quaternion_t4030073918  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m3196903881(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Quaternion_op_Multiply_m1483423721 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___rotation0, Vector3_t2243707580  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t2243707580  V_12;
	memset(&V_12, 0, sizeof(V_12));
	{
		float L_0 = (&___rotation0)->get_x_1();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_2();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_3();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_1();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_2();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_3();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_1();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_1();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_2();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_4();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_4();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_4();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t2243707580  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m3629786166 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___lhs0, Quaternion_t4030073918  ___rhs1, const MethodInfo* method)
{
	{
		Quaternion_t4030073918  L_0 = ___lhs0;
		Quaternion_t4030073918  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m952616600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t4030073918_marshal_pinvoke(const Quaternion_t4030073918& unmarshaled, Quaternion_t4030073918_marshaled_pinvoke& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t4030073918_marshal_pinvoke_back(const Quaternion_t4030073918_marshaled_pinvoke& marshaled, Quaternion_t4030073918& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t4030073918_marshal_pinvoke_cleanup(Quaternion_t4030073918_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t4030073918_marshal_com(const Quaternion_t4030073918& unmarshaled, Quaternion_t4030073918_marshaled_com& marshaled)
{
	marshaled.___x_1 = unmarshaled.get_x_1();
	marshaled.___y_2 = unmarshaled.get_y_2();
	marshaled.___z_3 = unmarshaled.get_z_3();
	marshaled.___w_4 = unmarshaled.get_w_4();
}
extern "C" void Quaternion_t4030073918_marshal_com_back(const Quaternion_t4030073918_marshaled_com& marshaled, Quaternion_t4030073918& unmarshaled)
{
	float unmarshaled_x_temp_0 = 0.0f;
	unmarshaled_x_temp_0 = marshaled.___x_1;
	unmarshaled.set_x_1(unmarshaled_x_temp_0);
	float unmarshaled_y_temp_1 = 0.0f;
	unmarshaled_y_temp_1 = marshaled.___y_2;
	unmarshaled.set_y_2(unmarshaled_y_temp_1);
	float unmarshaled_z_temp_2 = 0.0f;
	unmarshaled_z_temp_2 = marshaled.___z_3;
	unmarshaled.set_z_3(unmarshaled_z_temp_2);
	float unmarshaled_w_temp_3 = 0.0f;
	unmarshaled_w_temp_3 = marshaled.___w_4;
	unmarshaled.set_w_4(unmarshaled_w_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Quaternion
extern "C" void Quaternion_t4030073918_marshal_com_cleanup(Quaternion_t4030073918_marshaled_com& marshaled)
{
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m2884721203_ftn) (float, float);
	static Random_Range_m2884721203_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m2884721203_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m374035151(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m374035151 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m374035151_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m374035151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m374035151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m1657271662 (RangeAttribute_t3336560921 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3663555848(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m3379034047 (Ray_t2469606224 * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t2243707580  L_1 = Vector3_get_normalized_m936072361((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m3379034047_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___origin0, Vector3_t2243707580  ___direction1, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	Ray__ctor_m3379034047(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Origin_0();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_origin_m3339262500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_origin_m3339262500(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Direction_1();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Ray_get_direction_m4059191533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_get_direction_m4059191533(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366 (Ray_t2469606224 * __this, float ___distance0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector3_t2243707580  Ray_GetPoint_m1353702366_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_GetPoint_m1353702366(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1807026812;
extern const uint32_t Ray_ToString_m2019179238_MetadataUsageId;
extern "C"  String_t* Ray_ToString_m2019179238 (Ray_t2469606224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m2019179238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t2243707580  L_1 = __this->get_m_Origin_0();
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		Vector3_t2243707580  L_5 = __this->get_m_Direction_1();
		Vector3_t2243707580  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1807026812, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Ray_ToString_m2019179238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t2469606224 * _thisAdjusted = reinterpret_cast<Ray_t2469606224 *>(__this + 1);
	return Ray_ToString_m2019179238(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t2469606224_marshal_pinvoke(const Ray_t2469606224& unmarshaled, Ray_t2469606224_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t2469606224_marshal_pinvoke_back(const Ray_t2469606224_marshaled_pinvoke& marshaled, Ray_t2469606224& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t2243707580  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t2469606224_marshal_pinvoke_cleanup(Ray_t2469606224_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Origin_0);
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Direction_1);
}
// Conversion methods for marshalling of: UnityEngine.Ray
extern "C" void Ray_t2469606224_marshal_com(const Ray_t2469606224& unmarshaled, Ray_t2469606224_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Origin_0(), marshaled.___m_Origin_0);
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Direction_1(), marshaled.___m_Direction_1);
}
extern "C" void Ray_t2469606224_marshal_com_back(const Ray_t2469606224_marshaled_com& marshaled, Ray_t2469606224& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Origin_temp_0;
	memset(&unmarshaled_m_Origin_temp_0, 0, sizeof(unmarshaled_m_Origin_temp_0));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Origin_0, unmarshaled_m_Origin_temp_0);
	unmarshaled.set_m_Origin_0(unmarshaled_m_Origin_temp_0);
	Vector3_t2243707580  unmarshaled_m_Direction_temp_1;
	memset(&unmarshaled_m_Direction_temp_1, 0, sizeof(unmarshaled_m_Direction_temp_1));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Direction_1, unmarshaled_m_Direction_temp_1);
	unmarshaled.set_m_Direction_1(unmarshaled_m_Direction_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Ray
extern "C" void Ray_t2469606224_marshal_com_cleanup(Ray_t2469606224_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Origin_0);
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Direction_1);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  RaycastHit_get_point_m326143462_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_point_m326143462(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  RaycastHit_get_normal_m817665579_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_normal_m817665579(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m1178709367 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit_get_distance_m1178709367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_distance_m1178709367(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider_t3497673348 * RaycastHit_get_collider_m301198172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_collider_m301198172(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit_get_rigidbody_m480380820_MetadataUsageId;
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m480380820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t4233889191 * G_B3_0 = NULL;
	{
		Collider_t3497673348 * L_0 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t3497673348 * L_2 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t4233889191 * L_3 = Collider_get_attachedRigidbody_m3279305420(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t4233889191 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody_t4233889191 * RaycastHit_get_rigidbody_m480380820_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_rigidbody_m480380820(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit_get_transform_m3290290036_MetadataUsageId;
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036 (RaycastHit_t87180320 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_transform_m3290290036_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t4233889191 * V_0 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = RaycastHit_get_rigidbody_m480380820(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t4233889191 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody_t4233889191 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider_t3497673348 * L_5 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider_t3497673348 * L_7 = RaycastHit_get_collider_m301198172(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t3275118058 *)NULL;
	}
}
extern "C"  Transform_t3275118058 * RaycastHit_get_transform_m3290290036_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t87180320 * _thisAdjusted = reinterpret_cast<RaycastHit_t87180320 *>(__this + 1);
	return RaycastHit_get_transform_m3290290036(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_pinvoke(const RaycastHit_t87180320& unmarshaled, RaycastHit_t87180320_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t87180320_marshal_pinvoke_back(const RaycastHit_t87180320_marshaled_pinvoke& marshaled, RaycastHit_t87180320& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_pinvoke_cleanup(RaycastHit_t87180320_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_com(const RaycastHit_t87180320& unmarshaled, RaycastHit_t87180320_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t87180320_marshal_com_back(const RaycastHit_t87180320_marshaled_com& marshaled, RaycastHit_t87180320& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t87180320_marshal_com_cleanup(RaycastHit_t87180320_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_Point_1();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  RaycastHit2D_get_point_m442317739_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_point_m442317739(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_Normal_2();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  RaycastHit2D_get_normal_m3768105386_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_normal_m3768105386(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C"  float RaycastHit2D_get_distance_m4065977169 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Distance_3();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_distance_m4065977169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_distance_m4065977169(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m1296150410 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Fraction_4();
		return L_0;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m1296150410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_fraction_m1296150410(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	{
		Collider2D_t646061738 * L_0 = __this->get_m_Collider_5();
		return L_0;
	}
}
extern "C"  Collider2D_t646061738 * RaycastHit2D_get_collider_m2568504212_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_collider_m2568504212(_thisAdjusted, method);
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit2D_get_rigidbody_m3268023420_MetadataUsageId;
extern "C"  Rigidbody2D_t502193897 * RaycastHit2D_get_rigidbody_m3268023420 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_rigidbody_m3268023420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody2D_t502193897 * G_B3_0 = NULL;
	{
		Collider2D_t646061738 * L_0 = RaycastHit2D_get_collider_m2568504212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t646061738 * L_2 = RaycastHit2D_get_collider_m2568504212(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t502193897 * L_3 = Collider2D_get_attachedRigidbody_m1321121400(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t502193897 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
extern "C"  Rigidbody2D_t502193897 * RaycastHit2D_get_rigidbody_m3268023420_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_rigidbody_m3268023420(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RaycastHit2D_get_transform_m747355930_MetadataUsageId;
extern "C"  Transform_t3275118058 * RaycastHit2D_get_transform_m747355930 (RaycastHit2D_t4063908774 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit2D_get_transform_m747355930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody2D_t502193897 * V_0 = NULL;
	{
		Rigidbody2D_t502193897 * L_0 = RaycastHit2D_get_rigidbody_m3268023420(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t502193897 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t502193897 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t646061738 * L_5 = RaycastHit2D_get_collider_m2568504212(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t646061738 * L_7 = RaycastHit2D_get_collider_m2568504212(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t3275118058 *)NULL;
	}
}
extern "C"  Transform_t3275118058 * RaycastHit2D_get_transform_m747355930_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t4063908774 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t4063908774 *>(__this + 1);
	return RaycastHit2D_get_transform_m747355930(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke(const RaycastHit2D_t4063908774& unmarshaled, RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_back(const RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled, RaycastHit2D_t4063908774& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_pinvoke_cleanup(RaycastHit2D_t4063908774_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_com(const RaycastHit2D_t4063908774& unmarshaled, RaycastHit2D_t4063908774_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t4063908774_marshal_com_back(const RaycastHit2D_t4063908774_marshaled_com& marshaled, RaycastHit2D_t4063908774& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t4063908774_marshal_com_cleanup(RaycastHit2D_t4063908774_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m1220545469_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect__ctor_m1220545469(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Rect)
extern "C"  void Rect__ctor_m3896897500 (Rect_t3681755626 * __this, Rect_t3681755626  ___source0, const MethodInfo* method)
{
	{
		float L_0 = (&___source0)->get_m_XMin_0();
		__this->set_m_XMin_0(L_0);
		float L_1 = (&___source0)->get_m_YMin_1();
		__this->set_m_YMin_1(L_1);
		float L_2 = (&___source0)->get_m_Width_2();
		__this->set_m_Width_2(L_2);
		float L_3 = (&___source0)->get_m_Height_3();
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m3896897500_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___source0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect__ctor_m3896897500(_thisAdjusted, ___source0, method);
}
// UnityEngine.Rect UnityEngine.Rect::MinMaxRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  Rect_t3681755626  Rect_MinMaxRect_m4237641803 (Il2CppObject * __this /* static, unused */, float ___xmin0, float ___ymin1, float ___xmax2, float ___ymax3, const MethodInfo* method)
{
	{
		float L_0 = ___xmin0;
		float L_1 = ___ymin1;
		float L_2 = ___xmax2;
		float L_3 = ___xmin0;
		float L_4 = ___ymax3;
		float L_5 = ___ymin1;
		Rect_t3681755626  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m1220545469(&L_6, L_0, L_1, ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Rect::Set(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect_Set_m1972211443 (Rect_t3681755626 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect_Set_m1972211443_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_Set_m1972211443(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m1393582490 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_x_m1393582490_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_x_m1393582490(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m3783700513 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m3783700513_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_x_m3783700513(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1393582395 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_y_m1393582395_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_y_m1393582395(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m4294916608 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m4294916608_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_y_m4294916608(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_position_m24550734_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_position_m24550734(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_center_m3049923624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_center_m3049923624(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_min_m2549872833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_min_m2549872833(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_min(UnityEngine.Vector2)
extern "C"  void Rect_set_min_m2178285060 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		Rect_set_xMin_m4214255623(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (&___value0)->get_y_2();
		Rect_set_yMin_m734445288(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Rect_set_min_m2178285060_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_min_m2178285060(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_max_m96665935_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_max_m96665935(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_max(UnityEngine.Vector2)
extern "C"  void Rect_set_max_m2794417386 (Rect_t3681755626 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_1();
		Rect_set_xMax_m3501625033(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (&___value0)->get_y_2();
		Rect_set_yMax_m21814698(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Rect_set_max_m2794417386_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_max_m2794417386(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m1138015702 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		return L_0;
	}
}
extern "C"  float Rect_get_width_m1138015702_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_width_m1138015702(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m1921257731 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m1921257731_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_width_m1921257731(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m3128694305 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		return L_0;
	}
}
extern "C"  float Rect_get_height_m3128694305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_height_m3128694305(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m2019122814 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m2019122814_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_height_m2019122814(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector2_t2243707579  Rect_get_size_m3833121112_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_size_m3833121112(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m1161102488 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_XMin_0();
		return L_0;
	}
}
extern "C"  float Rect_get_xMin_m1161102488_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMin_m1161102488(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m4214255623 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m4214255623_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_xMin_m4214255623(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m1161103577 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_YMin_1();
		return L_0;
	}
}
extern "C"  float Rect_get_yMin_m1161103577_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMin_m1161103577(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m734445288 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m734445288_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_yMin_m734445288(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2915145014 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_xMax_m2915145014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_xMax_m2915145014(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m3501625033 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m3501625033_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_xMax_m3501625033(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2915146103 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		return ((float)((float)L_0+(float)L_1));
	}
}
extern "C"  float Rect_get_yMax_m2915146103_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_get_yMax_m2915146103(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m21814698 (Rect_t3681755626 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m21814698_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	Rect_set_yMax_m21814698(_thisAdjusted, ___value0, method);
}
// System.String UnityEngine.Rect::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1853817013;
extern const uint32_t Rect_ToString_m2728794442_MetadataUsageId;
extern "C"  String_t* Rect_ToString_m2728794442 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2728794442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1853817013, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Rect_ToString_m2728794442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_ToString_m2728794442(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m1334685290 (Rect_t3681755626 * __this, Vector2_t2243707579  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m1334685290_AdjustorThunk (Il2CppObject * __this, Vector2_t2243707579  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685290(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m1334685291 (Rect_t3681755626 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Contains_m1334685291_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Contains_m1334685291(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t3681755626  Rect_OrderMinMax_m1783437776 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Rect_get_xMin_m1161102488((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m2915145014((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0031;
		}
	}
	{
		float L_2 = Rect_get_xMin_m1161102488((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m2915145014((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m4214255623((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m3501625033((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0031:
	{
		float L_5 = Rect_get_yMin_m1161103577((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m2915146103((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0062;
		}
	}
	{
		float L_7 = Rect_get_yMin_m1161103577((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m2915146103((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m734445288((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m21814698((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0062:
	{
		Rect_t3681755626  L_10 = ___rect0;
		return L_10;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m210444568 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m2915145014((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m1161102488(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = Rect_get_xMin_m1161102488((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m2915145014(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = Rect_get_yMax_m2915146103((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m1161103577(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = Rect_get_yMin_m1161103577((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m2915146103(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Rect_Overlaps_m210444568_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Overlaps_m210444568(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m4145874649 (Rect_t3681755626 * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		V_0 = (*(Rect_t3681755626 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Rect_t3681755626  L_1 = V_0;
		Rect_t3681755626  L_2 = Rect_OrderMinMax_m1783437776(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t3681755626  L_3 = ___other0;
		Rect_t3681755626  L_4 = Rect_OrderMinMax_m1783437776(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001c:
	{
		Rect_t3681755626  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m210444568((&V_0), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
extern "C"  bool Rect_Overlaps_m4145874649_AdjustorThunk (Il2CppObject * __this, Rect_t3681755626  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Overlaps_m4145874649(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m559954498 (Rect_t3681755626 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m3102305584((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m3102305584((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m3102305584((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m3102305584((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
extern "C"  int32_t Rect_GetHashCode_m559954498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_GetHashCode_m559954498(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m3806390726_MetadataUsageId;
extern "C"  bool Rect_Equals_m3806390726 (Rect_t3681755626 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m3806390726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t3681755626_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox (L_1, Rect_t3681755626_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m1393582490(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m3359827399((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m1393582395(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m1393582395((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m3359827399((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m1138015702(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m1138015702((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m3359827399((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m3128694305(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m3128694305((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m3359827399((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Rect_Equals_m3806390726_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t3681755626 * _thisAdjusted = reinterpret_cast<Rect_t3681755626 *>(__this + 1);
	return Rect_Equals_m3806390726(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m3595915756 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m1393582490((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1393582490((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004e;
		}
	}
	{
		float L_2 = Rect_get_y_m1393582395((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m1393582395((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004e;
		}
	}
	{
		float L_4 = Rect_get_width_m1138015702((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m1138015702((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004e;
		}
	}
	{
		float L_6 = Rect_get_height_m3128694305((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m3128694305((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((int32_t)((((float)L_6) == ((float)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B5_0 = 1;
	}

IL_004f:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m2793663577 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___lhs0, Rect_t3681755626  ___rhs1, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m1393582490((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m1393582490((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m1393582395((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m1393582395((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m1138015702((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m1138015702((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m3128694305((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m3128694305((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return (bool)G_B5_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t3681755626_marshal_pinvoke(const Rect_t3681755626& unmarshaled, Rect_t3681755626_marshaled_pinvoke& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t3681755626_marshal_pinvoke_back(const Rect_t3681755626_marshaled_pinvoke& marshaled, Rect_t3681755626& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t3681755626_marshal_pinvoke_cleanup(Rect_t3681755626_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Rect
extern "C" void Rect_t3681755626_marshal_com(const Rect_t3681755626& unmarshaled, Rect_t3681755626_marshaled_com& marshaled)
{
	marshaled.___m_XMin_0 = unmarshaled.get_m_XMin_0();
	marshaled.___m_YMin_1 = unmarshaled.get_m_YMin_1();
	marshaled.___m_Width_2 = unmarshaled.get_m_Width_2();
	marshaled.___m_Height_3 = unmarshaled.get_m_Height_3();
}
extern "C" void Rect_t3681755626_marshal_com_back(const Rect_t3681755626_marshaled_com& marshaled, Rect_t3681755626& unmarshaled)
{
	float unmarshaled_m_XMin_temp_0 = 0.0f;
	unmarshaled_m_XMin_temp_0 = marshaled.___m_XMin_0;
	unmarshaled.set_m_XMin_0(unmarshaled_m_XMin_temp_0);
	float unmarshaled_m_YMin_temp_1 = 0.0f;
	unmarshaled_m_YMin_temp_1 = marshaled.___m_YMin_1;
	unmarshaled.set_m_YMin_1(unmarshaled_m_YMin_temp_1);
	float unmarshaled_m_Width_temp_2 = 0.0f;
	unmarshaled_m_Width_temp_2 = marshaled.___m_Width_2;
	unmarshaled.set_m_Width_2(unmarshaled_m_Width_temp_2);
	float unmarshaled_m_Height_temp_3 = 0.0f;
	unmarshaled_m_Height_temp_3 = marshaled.___m_Height_3;
	unmarshaled.set_m_Height_3(unmarshaled_m_Height_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Rect
extern "C" void Rect_t3681755626_marshal_com_cleanup(Rect_t3681755626_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2227510254 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4361650(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C"  void RectOffset__ctor_m3541945769 (RectOffset_t3387826427 * __this, GUIStyle_t1799908754 * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m4133355596 (RectOffset_t3387826427 * __this, int32_t ___left0, int32_t ___right1, int32_t ___top2, int32_t ___bottom3, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		RectOffset_Init_m4361650(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left0;
		RectOffset_set_left_m620681523(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right1;
		RectOffset_set_right_m1671272302(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top2;
		RectOffset_set_top_m3579196427(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom3;
		RectOffset_set_bottom_m4065521443(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m4361650 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m4361650_ftn) (RectOffset_t3387826427 *);
	static RectOffset_Init_m4361650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m4361650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m3198970074 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m3198970074_ftn) (RectOffset_t3387826427 *);
	static RectOffset_Cleanup_m3198970074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m3198970074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m439065308 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m439065308_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_left_m439065308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m439065308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m620681523 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m620681523_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_left_m620681523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m620681523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m281378687 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m281378687_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_right_m281378687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m281378687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m1671272302 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m1671272302_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_right_m1671272302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m1671272302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m3629049358 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m3629049358_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_top_m3629049358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m3629049358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m3579196427 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m3579196427_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_top_m3579196427_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m3579196427_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m4112328858 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m4112328858_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_bottom_m4112328858_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m4112328858_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m4065521443 (RectOffset_t3387826427 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m4065521443_ftn) (RectOffset_t3387826427 *, int32_t);
	static RectOffset_set_bottom_m4065521443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m4065521443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m3818523637 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m3818523637_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_horizontal_m3818523637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m3818523637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m3856345169 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m3856345169_ftn) (RectOffset_t3387826427 *);
	static RectOffset_get_vertical_m3856345169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m3856345169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Add(UnityEngine.Rect)
extern "C"  Rect_t3681755626  RectOffset_Add_m3336105852 (RectOffset_t3387826427 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Add_m3705097782(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Add_m3705097782 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Add_m3705097782_ftn) (RectOffset_t3387826427 *, Rect_t3681755626 *, Rect_t3681755626 *);
	static RectOffset_INTERNAL_CALL_Add_m3705097782_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Add_m3705097782_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C"  Rect_t3681755626  RectOffset_Remove_m1330811977 (RectOffset_t3387826427 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectOffset_INTERNAL_CALL_Remove_m1194095901(NULL /*static, unused*/, __this, (&___rect0), (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
extern "C"  void RectOffset_INTERNAL_CALL_Remove_m1194095901 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___self0, Rect_t3681755626 * ___rect1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn) (RectOffset_t3387826427 *, Rect_t3681755626 *, Rect_t3681755626 *);
	static RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m1194095901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1, ___value2);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m901770914 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t1799908754 * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m3198970074(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_001d:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3899275604;
extern const uint32_t RectOffset_ToString_m1281517011_MetadataUsageId;
extern "C"  String_t* RectOffset_ToString_m1281517011 (RectOffset_t3387826427 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m1281517011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m439065308(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m281378687(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m3629049358(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m4112328858(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3899275604, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_pinvoke(const RectOffset_t3387826427& unmarshaled, RectOffset_t3387826427_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3387826427_marshal_pinvoke_back(const RectOffset_t3387826427_marshaled_pinvoke& marshaled, RectOffset_t3387826427& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_pinvoke_cleanup(RectOffset_t3387826427_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_com(const RectOffset_t3387826427& unmarshaled, RectOffset_t3387826427_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
extern "C" void RectOffset_t3387826427_marshal_com_back(const RectOffset_t3387826427_marshaled_com& marshaled, RectOffset_t3387826427& unmarshaled)
{
	Il2CppCodeGenException* ___m_SourceStyle_1Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_SourceStyle' of type 'RectOffset': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_SourceStyle_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t3387826427_marshal_com_cleanup(RectOffset_t3387826427_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1603911943_MetadataUsageId;
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1603911943 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t2020713228 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1603911943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t2020713228 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t2020713228 *)CastclassSealed(L_2, ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m4209881182_MetadataUsageId;
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m4209881182 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t2020713228 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m4209881182_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		ReapplyDrivenProperties_t2020713228 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->set_reapplyDrivenProperties_2(((ReapplyDrivenProperties_t2020713228 *)CastclassSealed(L_2, ReapplyDrivenProperties_t2020713228_il2cpp_TypeInfo_var)));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3681755626  RectTransform_get_rect_m73954734 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_rect_m1177342209(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1177342209 (RectTransform_t3349966182 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1177342209_ftn) (RectTransform_t3349966182 *, Rect_t3681755626 *);
	static RectTransform_INTERNAL_get_rect_m1177342209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1177342209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMin_m1497323108 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMin_m3180545469(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4247668187 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m885423409(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m3180545469 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m3180545469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m885423409 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m885423409_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchorMin_m885423409_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m885423409_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2243707579  RectTransform_get_anchorMax_m3816015142 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchorMax_m834202955(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2955899993 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m1551648727(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m834202955 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m834202955_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchorMax_m834202955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m834202955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m1551648727 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m1551648727_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.RectTransform::get_anchoredPosition3D()
extern "C"  Vector3_t2243707580  RectTransform_get_anchoredPosition3D_m1202614648 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector2_t2243707579  L_0 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		float L_2 = (&V_0)->get_y_2();
		Vector3_t2243707580  L_3 = Transform_get_localPosition_m2533925116(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_z_3();
		Vector3_t2243707580  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m2638739322(&L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
extern "C"  void RectTransform_set_anchoredPosition3D_m3116733735 (RectTransform_t3349966182 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___value0)->get_x_1();
		float L_1 = (&___value0)->get_y_2();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Transform_get_localPosition_m2533925116(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&___value0)->get_z_3();
		(&V_0)->set_z_3(L_4);
		Vector3_t2243707580  L_5 = V_0;
		Transform_set_localPosition_m1026930133(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2243707579  RectTransform_get_anchoredPosition_m3570822376 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m3564306187(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m2077229449 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m693024247(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m3564306187 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m3564306187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m693024247 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m693024247_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2243707579  RectTransform_get_sizeDelta_m2157326342 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_sizeDelta_m3975625099(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m2319668137 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m1402803191(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m3975625099 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m3975625099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m1402803191 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m1402803191_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2243707579  RectTransform_get_pivot_m759087479 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_INTERNAL_get_pivot_m3003734630(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m1360548980 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m2764958706(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m3003734630 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m3003734630_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_get_pivot_m3003734630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m3003734630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m2764958706 (RectTransform_t3349966182 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m2764958706_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *);
	static RectTransform_INTERNAL_set_pivot_m2764958706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m2764958706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId;
extern "C"  void RectTransform_SendReapplyDrivenProperties_m90487700 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m90487700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_0 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t2020713228 * L_1 = ((RectTransform_t3349966182_StaticFields*)RectTransform_t3349966182_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t3349966182 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m1090213637(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral895140023;
extern const uint32_t RectTransform_GetLocalCorners_m1836626405_MetadataUsageId;
extern "C"  void RectTransform_GetLocalCorners_m1836626405 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m1836626405_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t1172311765* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral895140023, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t3681755626  L_2 = RectTransform_get_rect_m73954734(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m1393582490((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m1393582395((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m2915145014((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m2915146103((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t1172311765* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t1172311765* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t1172311765* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t2243707580  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m2638739322(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t1172311765* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t2243707580  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m2638739322(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3518247078;
extern const uint32_t RectTransform_GetWorldCorners_m3873546362_MetadataUsageId;
extern "C"  void RectTransform_GetWorldCorners_m3873546362 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m3873546362_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t1172311765* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3518247078, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t1172311765* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m1836626405(__this, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t1172311765* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t3275118058 * L_6 = V_0;
		Vector3U5BU5D_t1172311765* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t2243707580  L_9 = Transform_TransformPoint_m3272254198(L_6, (*(Vector3_t2243707580 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMin()
extern "C"  Vector2_t2243707579  RectTransform_get_offsetMin_m2864869694 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m2982698987 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___value0;
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t2243707579  L_7 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = V_0;
		Vector2_t2243707579  L_9 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(__this, L_9, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_11 = V_0;
		Vector2_t2243707579  L_12 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMax()
extern "C"  Vector2_t2243707579  RectTransform_get_offsetMax_m888594432 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m3702115945 (RectTransform_t3349966182 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___value0;
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_2 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = Vector2_get_one_m3174311904(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2243707579  L_6 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t2243707579  L_9 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = V_0;
		Vector2_t2243707579  L_11 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m2319668137(__this, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = V_0;
		Vector2_t2243707579  L_14 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_15 = Vector2_Scale_m3228063809(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m2077229449(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m2835026182 (RectTransform_t3349966182 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t2243707579 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t2243707579 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t2243707579 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t2243707579  L_5 = RectTransform_get_anchorMin_m1497323108(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m3881967114((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = V_3;
		RectTransform_set_anchorMin_m4247668187(__this, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_9 = RectTransform_get_anchorMax_m3816015142(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m3881967114((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t2243707579  L_12 = V_3;
		RectTransform_set_anchorMax_m2955899993(__this, L_12, /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m3881967114((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = V_4;
		RectTransform_set_sizeDelta_m2319668137(__this, L_16, /*hidden argument*/NULL);
		Vector2_t2243707579  L_17 = RectTransform_get_anchoredPosition_m3570822376(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t2243707579  L_22 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m2792130561((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t2243707579  L_27 = RectTransform_get_pivot_m759087479(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m2792130561((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m3881967114(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t2243707579  L_30 = V_5;
		RectTransform_set_anchoredPosition_m2077229449(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m2368352721 (RectTransform_t3349966182 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t2243707579  L_4 = RectTransform_GetParentSize_m1571597933(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m2792130561((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = RectTransform_get_anchorMax_m3816015142(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m2792130561((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = RectTransform_get_anchorMin_m1497323108(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m2792130561((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_13 = V_1;
		RectTransform_set_sizeDelta_m2319668137(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_GetParentSize_m1571597933_MetadataUsageId;
extern "C"  Vector2_t2243707579  RectTransform_GetParentSize_m1571597933 (RectTransform_t3349966182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m1571597933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	Rect_t3681755626  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = Transform_get_parent_m147407266(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3349966182 *)IsInstSealed(L_0, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t3349966182 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t3681755626  L_5 = RectTransform_get_rect_m73954734(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t2243707579  L_6 = Rect_get_size_m3833121112((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m210072638 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m1090213637 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m1090213637((ReapplyDrivenProperties_t2020713228 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3349966182 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m2337529776 (ReapplyDrivenProperties_t2020713228 * __this, RectTransform_t3349966182 * ___driven0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m2375002944 (ReapplyDrivenProperties_t2020713228 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m1866023382_MetadataUsageId;
extern "C"  void RectTransformUtility__cctor_m1866023382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m1866023382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t2941082270_StaticFields*)RectTransformUtility_t2941082270_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1244853728_MetadataUsageId;
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1244853728 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1244853728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		Camera_t189460977 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579 * ___screenPoint1, Camera_t189460977 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn) (RectTransform_t3349966182 *, Vector2_t2243707579 *, Camera_t189460977 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3362263993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m560908615_MetadataUsageId;
extern "C"  Vector2_t2243707579  RectTransformUtility_PixelAdjustPoint_m560908615 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m560908615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579  L_0 = ___point0;
		Transform_t3275118058 * L_1 = ___elementTransform1;
		Canvas_t209405766 * L_2 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m2939996260(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m2939996260_MetadataUsageId;
extern "C"  void RectTransformUtility_PixelAdjustPoint_m2939996260 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, Vector2_t2243707579 * ___output3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m2939996260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___elementTransform1;
		Canvas_t209405766 * L_1 = ___canvas2;
		Vector2_t2243707579 * L_2 = ___output3;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669(NULL /*static, unused*/, (&___point0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579 * ___point0, Transform_t3275118058 * ___elementTransform1, Canvas_t209405766 * ___canvas2, Vector2_t2243707579 * ___output3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn) (Vector2_t2243707579 *, Transform_t3275118058 *, Canvas_t209405766 *, Vector2_t2243707579 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2153046669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___output3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m93024038_MetadataUsageId;
extern "C"  Rect_t3681755626  RectTransformUtility_PixelAdjustRect_m93024038 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m93024038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RectTransform_t3349966182 * L_0 = ___rectTransform0;
		Canvas_t209405766 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rectTransform0, Canvas_t209405766 * ___canvas1, Rect_t3681755626 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn) (RectTransform_t3349966182 *, Canvas_t209405766 *, Rect_t3681755626 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1237215542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector3_t2243707580 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t3727654732  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Vector3_t2243707580 * L_0 = ___worldPoint3;
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_0) = L_2;
		Camera_t189460977 * L_3 = ___cam2;
		Vector2_t2243707579  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Ray_t2469606224  L_5 = RectTransformUtility_ScreenPointToRay_m1842507230(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t3349966182 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_get_back_m4246539215(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Plane__ctor_m3187718367((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t2469606224  L_12 = V_0;
		bool L_13 = Plane_Raycast_m2870142810((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return (bool)0;
	}

IL_0046:
	{
		Vector3_t2243707580 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t2243707580  L_16 = Ray_GetPoint_m1353702366((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_14) = L_16;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080_MetadataUsageId;
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, Vector2_t2243707579  ___screenPoint1, Camera_t189460977 * ___cam2, Vector2_t2243707579 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2243707579 * L_0 = ___localPoint3;
		Vector2_t2243707579  L_1 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)L_0) = L_1;
		RectTransform_t3349966182 * L_2 = ___rect0;
		Vector2_t2243707579  L_3 = ___screenPoint1;
		Camera_t189460977 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m2304638810(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t2243707579 * L_6 = ___localPoint3;
		RectTransform_t3349966182 * L_7 = ___rect0;
		Vector3_t2243707580  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t2243707580  L_9 = Transform_InverseTransformPoint_m2648491174(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t2243707579  L_10 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t2243707579 *)L_6) = L_10;
		return (bool)1;
	}

IL_002e:
	{
		return (bool)0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m1842507230_MetadataUsageId;
extern "C"  Ray_t2469606224  RectTransformUtility_ScreenPointToRay_m1842507230 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, Vector2_t2243707579  ___screenPos1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m1842507230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t189460977 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t189460977 * L_2 = ___cam0;
		Vector2_t2243707579  L_3 = ___screenPos1;
		Vector3_t2243707580  L_4 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t2469606224  L_5 = Camera_ScreenPointToRay_m614889538(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t2243707579  L_6 = ___screenPos1;
		Vector3_t2243707580  L_7 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t2243707580 * L_8 = (&V_0);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t2243707580  L_10 = V_0;
		Vector3_t2243707580  L_11 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t2469606224  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m3379034047(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_WorldToScreenPoint_m1650782138_MetadataUsageId;
extern "C"  Vector2_t2243707579  RectTransformUtility_WorldToScreenPoint_m1650782138 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, Vector3_t2243707580  ___worldPoint1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_WorldToScreenPoint_m1650782138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t189460977 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		float L_2 = (&___worldPoint1)->get_x_1();
		float L_3 = (&___worldPoint1)->get_y_2();
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0020:
	{
		Camera_t189460977 * L_5 = ___cam0;
		Vector3_t2243707580  L_6 = ___worldPoint1;
		NullCheck(L_5);
		Vector3_t2243707580  L_7 = Camera_WorldToScreenPoint_m638747266(L_5, L_6, /*hidden argument*/NULL);
		Vector2_t2243707579  L_8 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m3920364518_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m3920364518 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m3920364518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t3349966182 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t3349966182 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m3920364518(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t3349966182 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m881385315(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t3349966182 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_pivot_m759087479(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m2792130561((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_19 = ___rect0;
		Vector2_t2243707579  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m1360548980(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t3349966182 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t2243707579  L_23 = RectTransform_get_anchoredPosition_m3570822376(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m2792130561((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_27 = ___rect0;
		Vector2_t2243707579  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m2077229449(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t2243707579  L_30 = RectTransform_get_anchorMin_m1497323108(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t3349966182 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2243707579  L_32 = RectTransform_get_anchorMax_m3816015142(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m2792130561((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m2792130561((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m3881967114((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m3881967114((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_40 = ___rect0;
		Vector2_t2243707579  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m4247668187(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_42 = ___rect0;
		Vector2_t2243707579  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m2955899993(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m532748168_MetadataUsageId;
extern "C"  void RectTransformUtility_FlipLayoutAxes_m532748168 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m532748168_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	{
		RectTransform_t3349966182 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t3349966182 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t3349966182 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m532748168(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t3349966182 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m881385315(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t3349966182 * L_13 = ___rect0;
		RectTransform_t3349966182 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_pivot_m759087479(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_16 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m1360548980(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_17 = ___rect0;
		RectTransform_t3349966182 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = RectTransform_get_sizeDelta_m2157326342(L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m2319668137(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t3349966182 * L_22 = ___rect0;
		RectTransform_t3349966182 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = RectTransform_get_anchoredPosition_m3570822376(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_25 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m2077229449(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_26 = ___rect0;
		RectTransform_t3349966182 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t2243707579  L_28 = RectTransform_get_anchorMin_m1497323108(L_27, /*hidden argument*/NULL);
		Vector2_t2243707579  L_29 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4247668187(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_30 = ___rect0;
		RectTransform_t3349966182 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2243707579  L_32 = RectTransform_get_anchorMax_m3816015142(L_31, /*hidden argument*/NULL);
		Vector2_t2243707579  L_33 = RectTransformUtility_GetTransposed_m1770338235(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m2955899993(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  RectTransformUtility_GetTransposed_m1770338235 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___input0, const MethodInfo* method)
{
	{
		float L_0 = (&___input0)->get_y_2();
		float L_1 = (&___input0)->get_x_1();
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m2362836534 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_enabled_m2362836534_ftn) (Renderer_t257310565 *);
	static Renderer_get_enabled_m2362836534_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_enabled_m2362836534_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m142717579 (Renderer_t257310565 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m142717579_ftn) (Renderer_t257310565 *, bool);
	static Renderer_set_enabled_m142717579_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m142717579_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Renderer_get_material_m2553789785_ftn) (Renderer_t257310565 *);
	static Renderer_get_material_m2553789785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2553789785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1053097112 (Renderer_t257310565 * __this, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m1053097112_ftn) (Renderer_t257310565 *, Material_t193706927 *);
	static Renderer_set_material_m1053097112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m1053097112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t193706927 * Renderer_get_sharedMaterial_m155010392 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Renderer_get_sharedMaterial_m155010392_ftn) (Renderer_t257310565 *);
	static Renderer_get_sharedMaterial_m155010392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m155010392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t3123989686* Renderer_get_materials_m810004692 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t3123989686* (*Renderer_get_materials_m810004692_ftn) (Renderer_t257310565 *);
	static Renderer_get_materials_m810004692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m810004692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern "C"  void Renderer_set_materials_m1556465155 (Renderer_t257310565 * __this, MaterialU5BU5D_t3123989686* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_materials_m1556465155_ftn) (Renderer_t257310565 *, MaterialU5BU5D_t3123989686*);
	static Renderer_set_materials_m1556465155_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_materials_m1556465155_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_materials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t3123989686* Renderer_get_sharedMaterials_m4026934221 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t3123989686* (*Renderer_get_sharedMaterials_m4026934221_ftn) (Renderer_t257310565 *);
	static Renderer_get_sharedMaterials_m4026934221_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m4026934221_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern "C"  void Renderer_set_sharedMaterials_m2669445156 (Renderer_t257310565 * __this, MaterialU5BU5D_t3123989686* ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterials_m2669445156_ftn) (Renderer_t257310565 *, MaterialU5BU5D_t3123989686*);
	static Renderer_set_sharedMaterials_m2669445156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterials_m2669445156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t3033363703  Renderer_get_bounds_m3832626589 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_INTERNAL_get_bounds_m1299274192(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t3033363703  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m1299274192 (Renderer_t257310565 * __this, Bounds_t3033363703 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_bounds_m1299274192_ftn) (Renderer_t257310565 *, Bounds_t3033363703 *);
	static Renderer_INTERNAL_get_bounds_m1299274192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_bounds_m1299274192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Renderer::get_isVisible()
extern "C"  bool Renderer_get_isVisible_m3612355717 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef bool (*Renderer_get_isVisible_m3612355717_ftn) (Renderer_t257310565 *);
	static Renderer_get_isVisible_m3612355717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_isVisible_m3612355717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_isVisible()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m2403577271 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m2403577271_ftn) (Renderer_t257310565 *);
	static Renderer_get_sortingLayerID_m2403577271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m2403577271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m1544525007 (Renderer_t257310565 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m1544525007_ftn) (Renderer_t257310565 *);
	static Renderer_get_sortingOrder_m1544525007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m1544525007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern "C"  void RenderSettings_set_fog_m4141163796 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fog_m4141163796_ftn) (bool);
	static RenderSettings_set_fog_m4141163796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fog_m4141163796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fog(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern "C"  void RenderSettings_set_fogColor_m3716873294 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_fogColor_m2591134422(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_fogColor_m2591134422 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_fogColor_m2591134422_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_set_fogColor_m2591134422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_fogColor_m2591134422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern "C"  void RenderSettings_set_fogDensity_m2940444706 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogDensity_m2940444706_ftn) (float);
	static RenderSettings_set_fogDensity_m2940444706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogDensity_m2940444706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogDensity(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_ambientLight(UnityEngine.Color)
extern "C"  void RenderSettings_set_ambientLight_m2849768839 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		RenderSettings_INTERNAL_set_ambientLight_m1935599789(NULL /*static, unused*/, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
extern "C"  void RenderSettings_INTERNAL_set_ambientLight_m1935599789 (Il2CppObject * __this /* static, unused */, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_INTERNAL_set_ambientLight_m1935599789_ftn) (Color_t2020392075 *);
	static RenderSettings_INTERNAL_set_ambientLight_m1935599789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_INTERNAL_set_ambientLight_m1935599789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
extern "C"  void RenderSettings_set_haloStrength_m1605591213 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_haloStrength_m1605591213_ftn) (float);
	static RenderSettings_set_haloStrength_m1605591213_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_haloStrength_m1605591213_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_haloStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
extern "C"  void RenderSettings_set_flareStrength_m3519736811 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_flareStrength_m3519736811_ftn) (float);
	static RenderSettings_set_flareStrength_m3519736811_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_flareStrength_m3519736811_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_flareStrength(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m2565496707 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___value0, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_skybox_m2565496707_ftn) (Material_t193706927 *);
	static RenderSettings_set_skybox_m2565496707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_skybox_m2565496707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m2317917654 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m2317917654_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_GetWidth_m2317917654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m2317917654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m2780941261 (Il2CppObject * __this /* static, unused */, RenderTexture_t2666733923 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m2780941261_ftn) (RenderTexture_t2666733923 *);
	static RenderTexture_Internal_GetHeight_m2780941261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m2780941261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m1471807677 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m2317917654(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m1108175848 (RenderTexture_t2666733923 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m2780941261(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m3475141952 (RequireComponent_t864575032 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m1438273472 (Resolution_t3693662728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Width_0();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_width_m1438273472_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t3693662728 * _thisAdjusted = reinterpret_cast<Resolution_t3693662728 *>(__this + 1);
	return Resolution_get_width_m1438273472(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m882683003 (Resolution_t3693662728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Height_1();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_height_m882683003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t3693662728 * _thisAdjusted = reinterpret_cast<Resolution_t3693662728 *>(__this + 1);
	return Resolution_get_height_m882683003(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern "C"  int32_t Resolution_get_refreshRate_m1509667735 (Resolution_t3693662728 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_RefreshRate_2();
		return L_0;
	}
}
extern "C"  int32_t Resolution_get_refreshRate_m1509667735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t3693662728 * _thisAdjusted = reinterpret_cast<Resolution_t3693662728 *>(__this + 1);
	return Resolution_get_refreshRate_m1509667735(_thisAdjusted, method);
}
// System.String UnityEngine.Resolution::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral16809271;
extern const uint32_t Resolution_ToString_m3711510886_MetadataUsageId;
extern "C"  String_t* Resolution_ToString_m3711510886 (Resolution_t3693662728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resolution_ToString_m3711510886_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Width_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		int32_t L_5 = __this->get_m_Height_1();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		int32_t L_9 = __this->get_m_RefreshRate_2();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral16809271, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
extern "C"  String_t* Resolution_ToString_m3711510886_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t3693662728 * _thisAdjusted = reinterpret_cast<Resolution_t3693662728 *>(__this + 1);
	return Resolution_ToString_m3711510886(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t3693662728_marshal_pinvoke(const Resolution_t3693662728& unmarshaled, Resolution_t3693662728_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Width_0 = unmarshaled.get_m_Width_0();
	marshaled.___m_Height_1 = unmarshaled.get_m_Height_1();
	marshaled.___m_RefreshRate_2 = unmarshaled.get_m_RefreshRate_2();
}
extern "C" void Resolution_t3693662728_marshal_pinvoke_back(const Resolution_t3693662728_marshaled_pinvoke& marshaled, Resolution_t3693662728& unmarshaled)
{
	int32_t unmarshaled_m_Width_temp_0 = 0;
	unmarshaled_m_Width_temp_0 = marshaled.___m_Width_0;
	unmarshaled.set_m_Width_0(unmarshaled_m_Width_temp_0);
	int32_t unmarshaled_m_Height_temp_1 = 0;
	unmarshaled_m_Height_temp_1 = marshaled.___m_Height_1;
	unmarshaled.set_m_Height_1(unmarshaled_m_Height_temp_1);
	int32_t unmarshaled_m_RefreshRate_temp_2 = 0;
	unmarshaled_m_RefreshRate_temp_2 = marshaled.___m_RefreshRate_2;
	unmarshaled.set_m_RefreshRate_2(unmarshaled_m_RefreshRate_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t3693662728_marshal_pinvoke_cleanup(Resolution_t3693662728_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t3693662728_marshal_com(const Resolution_t3693662728& unmarshaled, Resolution_t3693662728_marshaled_com& marshaled)
{
	marshaled.___m_Width_0 = unmarshaled.get_m_Width_0();
	marshaled.___m_Height_1 = unmarshaled.get_m_Height_1();
	marshaled.___m_RefreshRate_2 = unmarshaled.get_m_RefreshRate_2();
}
extern "C" void Resolution_t3693662728_marshal_com_back(const Resolution_t3693662728_marshaled_com& marshaled, Resolution_t3693662728& unmarshaled)
{
	int32_t unmarshaled_m_Width_temp_0 = 0;
	unmarshaled_m_Width_temp_0 = marshaled.___m_Width_0;
	unmarshaled.set_m_Width_0(unmarshaled_m_Width_temp_0);
	int32_t unmarshaled_m_Height_temp_1 = 0;
	unmarshaled_m_Height_temp_1 = marshaled.___m_Height_1;
	unmarshaled.set_m_Height_1(unmarshaled_m_Height_temp_1);
	int32_t unmarshaled_m_RefreshRate_temp_2 = 0;
	unmarshaled_m_RefreshRate_temp_2 = marshaled.___m_RefreshRate_2;
	unmarshaled.set_m_RefreshRate_2(unmarshaled_m_RefreshRate_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.Resolution
extern "C" void Resolution_t3693662728_marshal_com_cleanup(Resolution_t3693662728_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m3340010930 (ResourceRequest_t2560315377 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t1021602117 * ResourceRequest_get_asset_m3527928488 (ResourceRequest_t2560315377 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke(const ResourceRequest_t2560315377& unmarshaled, ResourceRequest_t2560315377_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_back(const ResourceRequest_t2560315377_marshaled_pinvoke& marshaled, ResourceRequest_t2560315377& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_pinvoke_cleanup(ResourceRequest_t2560315377_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_com(const ResourceRequest_t2560315377& unmarshaled, ResourceRequest_t2560315377_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t2560315377_marshal_com_back(const ResourceRequest_t2560315377_marshaled_com& marshaled, ResourceRequest_t2560315377& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t2560315377_marshal_com_cleanup(ResourceRequest_t2560315377_marshaled_com& marshaled)
{
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t1021602117_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m2041782325_MetadataUsageId;
extern "C"  Object_t1021602117 * Resources_Load_m2041782325 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m2041782325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Object_t1021602117_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1021602117 * Resources_Load_m243305716 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Resources_Load_m243305716_ftn) (String_t*, Type_t *);
	static Resources_Load_m243305716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m243305716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t1021602117 * Resources_GetBuiltinResource_m582410469 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*Resources_GetBuiltinResource_m582410469_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m582410469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m582410469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type0, ___path1);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t2243707580  Rigidbody_get_velocity_m2022666970 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_velocity_m1938461825(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m2514070071 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m3384354677(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_velocity_m1938461825 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m1938461825_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_velocity_m1938461825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m1938461825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m3384354677 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m3384354677_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_set_velocity_m3384354677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m3384354677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
extern "C"  void Rigidbody_set_drag_m431766562 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_drag_m431766562_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_drag_m431766562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_drag_m431766562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_drag(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody::get_mass()
extern "C"  float Rigidbody_get_mass_m2290366311 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody_get_mass_m2290366311_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_get_mass_m2290366311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_mass_m2290366311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_mass(System.Single)
extern "C"  void Rigidbody_set_mass_m4155402692 (Rigidbody_t4233889191 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_mass_m4155402692_ftn) (Rigidbody_t4233889191 *, float);
	static Rigidbody_set_mass_m4155402692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_mass_m4155402692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C"  void Rigidbody_set_useGravity_m2606656539 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m2606656539_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_useGravity_m2606656539_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m2606656539_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody::get_isKinematic()
extern "C"  bool Rigidbody_get_isKinematic_m2907467582 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody_get_isKinematic_m2907467582_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_get_isKinematic_m2907467582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_get_isKinematic_m2907467582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody_set_isKinematic_m738793415 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m738793415_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_isKinematic_m738793415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m738793415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m2131864169 (Rigidbody_t4233889191 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m2131864169_ftn) (Rigidbody_t4233889191 *, bool);
	static Rigidbody_set_freezeRotation_m2131864169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m2131864169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m3219459786 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m3164777073(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m3164777073 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m3164777073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeForce_m2360431750 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeForce_m394474805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddTorque_m3174039081 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddTorque_m929838442(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddTorque_m929838442 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddTorque_m929838442_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddRelativeTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddRelativeTorque_m1544184357 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddRelativeTorque_m3108799878(NULL /*static, unused*/, __this, (&___torque0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddRelativeTorque_m3108799878 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___torque1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddRelativeTorque_m3108799878_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddRelativeTorque_m3108799878_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddRelativeTorque_m3108799878_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___torque1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::AddForceAtPosition(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForceAtPosition_m4129134921 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___force0, Vector3_t2243707580  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___force1, Vector3_t2243707580 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *, Vector3_t2243707580 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForceAtPosition_m1863309522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody::AddExplosionForce(System.Single,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddExplosionForce_m1666381818 (Rigidbody_t4233889191 * __this, float ___explosionForce0, Vector3_t2243707580  ___explosionPosition1, float ___explosionRadius2, float ___upwardsModifier3, int32_t ___mode4, const MethodInfo* method)
{
	{
		float L_0 = ___explosionForce0;
		float L_1 = ___explosionRadius2;
		float L_2 = ___upwardsModifier3;
		int32_t L_3 = ___mode4;
		Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993(NULL /*static, unused*/, __this, L_0, (&___explosionPosition1), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, float ___explosionForce1, Vector3_t2243707580 * ___explosionPosition2, float ___explosionRadius3, float ___upwardsModifier4, int32_t ___mode5, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993_ftn) (Rigidbody_t4233889191 *, float, Vector3_t2243707580 *, float, float, int32_t);
	static Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddExplosionForce_m3156773993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___explosionForce1, ___explosionPosition2, ___explosionRadius3, ___upwardsModifier4, ___mode5);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C"  Vector3_t2243707580  Rigidbody_get_position_m3465583110 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_position_m2227872991(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_get_position_m2227872991 (Rigidbody_t4233889191 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_position_m2227872991_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_get_position_m2227872991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_position_m2227872991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Rigidbody::get_rotation()
extern "C"  Quaternion_t4030073918  Rigidbody_get_rotation_m4203325509 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_INTERNAL_get_rotation_m109669412(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_get_rotation_m109669412 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_rotation_m109669412_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_get_rotation_m109669412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_rotation_m109669412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m1810529681 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m2709492198(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m2709492198 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Vector3_t2243707580 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn) (Rigidbody_t4233889191 *, Vector3_t2243707580 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m2709492198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m3412525692 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, Quaternion_t4030073918 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn) (Rigidbody_t4233889191 *, Quaternion_t4030073918 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m2205051919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m416287251 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m493241514(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_Sleep_m493241514 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m493241514_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m493241514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m493241514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.Rigidbody::IsSleeping()
extern "C"  bool Rigidbody_IsSleeping_m2925992345 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Rigidbody_INTERNAL_CALL_IsSleeping_m697921952(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)
extern "C"  bool Rigidbody_INTERNAL_CALL_IsSleeping_m697921952 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, const MethodInfo* method)
{
	typedef bool (*Rigidbody_INTERNAL_CALL_IsSleeping_m697921952_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_INTERNAL_CALL_IsSleeping_m697921952_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_IsSleeping_m697921952_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Rigidbody::WakeUp()
extern "C"  void Rigidbody_WakeUp_m2756094289 (Rigidbody_t4233889191 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_WakeUp_m584443448(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)
extern "C"  void Rigidbody_INTERNAL_CALL_WakeUp_m584443448 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___self0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_WakeUp_m584443448_ftn) (Rigidbody_t4233889191 *);
	static Rigidbody_INTERNAL_CALL_WakeUp_m584443448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_WakeUp_m584443448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C"  Vector2_t2243707579  Rigidbody2D_get_position_m1357256809 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_position_m1367902426(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_position_m1185983402 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_position_m3007007470(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_position_m1367902426 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_position_m1367902426_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *);
	static Rigidbody2D_INTERNAL_get_position_m1367902426_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_position_m1367902426_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_position(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_position_m3007007470 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_position_m3007007470_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *);
	static Rigidbody2D_INTERNAL_set_position_m3007007470_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_position_m3007007470_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_position(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_rotation()
extern "C"  float Rigidbody2D_get_rotation_m485450105 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_rotation_m485450105_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_get_rotation_m485450105_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_rotation_m485450105_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_rotation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_MovePosition_m2716592358 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  ___position0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_CALL_MovePosition_m2161463205(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MovePosition_m2161463205 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___self0, Vector2_t2243707579 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_MovePosition_m2161463205_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *);
	static Rigidbody2D_INTERNAL_CALL_MovePosition_m2161463205_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_MovePosition_m2161463205_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody2D::MoveRotation(System.Single)
extern "C"  void Rigidbody2D_MoveRotation_m1710763820 (Rigidbody2D_t502193897 * __this, float ___angle0, const MethodInfo* method)
{
	{
		float L_0 = ___angle0;
		Rigidbody2D_INTERNAL_CALL_MoveRotation_m4168162731(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody2D,System.Single)
extern "C"  void Rigidbody2D_INTERNAL_CALL_MoveRotation_m4168162731 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___self0, float ___angle1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_MoveRotation_m4168162731_ftn) (Rigidbody2D_t502193897 *, float);
	static Rigidbody2D_INTERNAL_CALL_MoveRotation_m4168162731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_MoveRotation_m4168162731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody2D,System.Single)");
	_il2cpp_icall_func(___self0, ___angle1);
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2243707579  Rigidbody2D_get_velocity_m3310151195 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody2D_INTERNAL_get_velocity_m3018296454(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m3592751374 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_velocity_m1537663346(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_get_velocity_m3018296454 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_velocity_m3018296454_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *);
	static Rigidbody2D_INTERNAL_get_velocity_m3018296454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_velocity_m3018296454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_velocity_m1537663346 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_velocity_m1537663346_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *);
	static Rigidbody2D_INTERNAL_set_velocity_m1537663346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_velocity_m1537663346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Rigidbody2D::get_mass()
extern "C"  float Rigidbody2D_get_mass_m410633361 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef float (*Rigidbody2D_get_mass_m410633361_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_get_mass_m410633361_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_mass_m410633361_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_mass()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
extern "C"  void Rigidbody2D_set_mass_m2409729346 (Rigidbody2D_t502193897 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_mass_m2409729346_ftn) (Rigidbody2D_t502193897 *, float);
	static Rigidbody2D_set_mass_m2409729346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_mass_m2409729346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_mass(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
extern "C"  void Rigidbody2D_set_gravityScale_m1426625078 (Rigidbody2D_t502193897 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_gravityScale_m1426625078_ftn) (Rigidbody2D_t502193897 *, float);
	static Rigidbody2D_set_gravityScale_m1426625078_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_gravityScale_m1426625078_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_gravityScale(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern "C"  bool Rigidbody2D_get_isKinematic_m796625912 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_get_isKinematic_m796625912_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_get_isKinematic_m796625912_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_isKinematic_m796625912_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_isKinematic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)
extern "C"  void Rigidbody2D_set_isKinematic_m548319077 (Rigidbody2D_t502193897 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_isKinematic_m548319077_ftn) (Rigidbody2D_t502193897 *, bool);
	static Rigidbody2D_set_isKinematic_m548319077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_isKinematic_m548319077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
extern "C"  int32_t Rigidbody2D_get_constraints_m256029512 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef int32_t (*Rigidbody2D_get_constraints_m256029512_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_get_constraints_m256029512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_get_constraints_m256029512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::get_constraints()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
extern "C"  void Rigidbody2D_set_constraints_m1595222955 (Rigidbody2D_t502193897 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_set_constraints_m1595222955_ftn) (Rigidbody2D_t502193897 *, int32_t);
	static Rigidbody2D_set_constraints_m1595222955_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_set_constraints_m1595222955_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
extern "C"  bool Rigidbody2D_IsSleeping_m1852006803 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef bool (*Rigidbody2D_IsSleeping_m1852006803_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_IsSleeping_m1852006803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_IsSleeping_m1852006803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::IsSleeping()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::Sleep()
extern "C"  void Rigidbody2D_Sleep_m3196697853 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_Sleep_m3196697853_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_Sleep_m3196697853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_Sleep_m3196697853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::Sleep()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::WakeUp()
extern "C"  void Rigidbody2D_WakeUp_m1402807111 (Rigidbody2D_t502193897 * __this, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_WakeUp_m1402807111_ftn) (Rigidbody2D_t502193897 *);
	static Rigidbody2D_WakeUp_m1402807111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_WakeUp_m1402807111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::WakeUp()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForce_m4245830473 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_INTERNAL_CALL_AddForce_m1958598324(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForce_m1958598324 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___self0, Vector2_t2243707579 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForce_m1958598324_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForce_m1958598324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForce_m1958598324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody2D::AddRelativeForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddRelativeForce_m3147815557 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  ___relativeForce0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m168719000(NULL /*static, unused*/, __this, (&___relativeForce0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m168719000 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___self0, Vector2_t2243707579 * ___relativeForce1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m168719000_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m168719000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddRelativeForce_m168719000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___relativeForce1, ___mode2);
}
// System.Void UnityEngine.Rigidbody2D::AddForceAtPosition(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForceAtPosition_m1961203817 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  ___force0, Vector2_t2243707579  ___position1, int32_t ___mode2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode2;
		Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m2346914166(NULL /*static, unused*/, __this, (&___force0), (&___position1), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m2346914166 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * ___self0, Vector2_t2243707579 * ___force1, Vector2_t2243707579 * ___position2, int32_t ___mode3, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m2346914166_ftn) (Rigidbody2D_t502193897 *, Vector2_t2243707579 *, Vector2_t2243707579 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m2346914166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForceAtPosition_m2346914166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___position2, ___mode3);
}
// System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddTorque_m714733013 (Rigidbody2D_t502193897 * __this, float ___torque0, int32_t ___mode1, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_AddTorque_m714733013_ftn) (Rigidbody2D_t502193897 *, float, int32_t);
	static Rigidbody2D_AddTorque_m714733013_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_AddTorque_m714733013_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(__this, ___torque0, ___mode1);
}
// System.Void UnityEngine.RPC::.ctor()
extern "C"  void RPC__ctor_m1432086380 (RPC_t3323229423 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor()
extern "C"  void RuntimeInitializeOnLoadMethodAttribute__ctor_m3558775001 (RuntimeInitializeOnLoadMethodAttribute_t3126475234 * __this, const MethodInfo* method)
{
	{
		PreserveAttribute__ctor_m2437378488(__this, /*hidden argument*/NULL);
		RuntimeInitializeOnLoadMethodAttribute_set_loadType_m1308196171(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m1308196171 (RuntimeInitializeOnLoadMethodAttribute_t3126475234 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CloadTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m1555912301 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_get_handle_m1555912301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_get_handle_m1555912301(_thisAdjusted, method);
}
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m745914591 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Scene_get_handle_m1555912301(__this, /*hidden argument*/NULL);
		String_t* L_1 = Scene_GetNameInternal_m3140297940(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  String_t* Scene_get_name_m745914591_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_get_name_m745914591(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m3223653899 (Scene_t1684909666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Handle_0();
		return L_0;
	}
}
extern "C"  int32_t Scene_GetHashCode_m3223653899_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_GetHashCode_m3223653899(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern Il2CppClass* Scene_t1684909666_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m3588907349_MetadataUsageId;
extern "C"  bool Scene_Equals_m3588907349 (Scene_t1684909666 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m3588907349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t1684909666_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Scene_t1684909666 *)((Scene_t1684909666 *)UnBox (L_1, Scene_t1684909666_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m1555912301(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m1555912301((&V_0), /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
	}
}
extern "C"  bool Scene_Equals_m3588907349_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t1684909666 * _thisAdjusted = reinterpret_cast<Scene_t1684909666 *>(__this + 1);
	return Scene_Equals_m3588907349(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m3140297940 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef String_t* (*Scene_GetNameInternal_m3140297940_ftn) (int32_t);
	static Scene_GetNameInternal_m3140297940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetNameInternal_m3140297940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1684909666_marshal_pinvoke(const Scene_t1684909666& unmarshaled, Scene_t1684909666_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1684909666_marshal_pinvoke_back(const Scene_t1684909666_marshaled_pinvoke& marshaled, Scene_t1684909666& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1684909666_marshal_pinvoke_cleanup(Scene_t1684909666_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1684909666_marshal_com(const Scene_t1684909666& unmarshaled, Scene_t1684909666_marshaled_com& marshaled)
{
	marshaled.___m_Handle_0 = unmarshaled.get_m_Handle_0();
}
extern "C" void Scene_t1684909666_marshal_com_back(const Scene_t1684909666_marshaled_com& marshaled, Scene_t1684909666& unmarshaled)
{
	int32_t unmarshaled_m_Handle_temp_0 = 0;
	unmarshaled_m_Handle_temp_0 = marshaled.___m_Handle_0;
	unmarshaled.set_m_Handle_0(unmarshaled_m_Handle_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.SceneManagement.Scene
extern "C" void Scene_t1684909666_marshal_com_cleanup(Scene_t1684909666_marshaled_com& marshaled)
{
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t1684909666  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318 (Il2CppObject * __this /* static, unused */, Scene_t1684909666 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn) (Scene_t1684909666 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m1595803318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1619949821 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m1386820036(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m1386820036 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m592643733 (Il2CppObject * __this /* static, unused */, int32_t ___sceneBuildIndex0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	Il2CppObject * G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	Il2CppObject * G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	Il2CppObject * G_B3_2 = NULL;
	{
		int32_t L_0 = ___sceneBuildIndex0;
		int32_t L_1 = ___mode1;
		G_B1_0 = L_0;
		G_B1_1 = NULL;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = L_0;
			G_B2_1 = NULL;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, (String_t*)G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m4130852156 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		AsyncOperation_t3814632279 * L_2 = SceneManager_LoadSceneAsync_m2648120039(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsync_m2648120039 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_000f;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0010;
	}

IL_000f:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0010:
	{
		AsyncOperation_t3814632279 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t3814632279 * SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t3814632279 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m3279056043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m1528820797_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m4005732915_MetadataUsageId;
extern "C"  void SceneManager_Internal_SceneLoaded_m4005732915 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___scene0, int32_t ___mode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m4005732915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_2_t1903595547 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityAction_2_t1903595547 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		Scene_t1684909666  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1528820797(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1528820797_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_1_Invoke_m3061904506_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m4108957131_MetadataUsageId;
extern "C"  void SceneManager_Internal_SceneUnloaded_m4108957131 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___scene0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m4108957131_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_1_t3051495417 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		UnityAction_1_t3051495417 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		Scene_t1684909666  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m3061904506(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m3061904506_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern Il2CppClass* SceneManager_t90660965_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m670567184_MethodInfo_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m1162592635_MetadataUsageId;
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m1162592635 (Il2CppObject * __this /* static, unused */, Scene_t1684909666  ___previousActiveScene0, Scene_t1684909666  ___newActiveScene1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m1162592635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_2_t606618774 * L_0 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityAction_2_t606618774 * L_1 = ((SceneManager_t90660965_StaticFields*)SceneManager_t90660965_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		Scene_t1684909666  L_2 = ___previousActiveScene0;
		Scene_t1684909666  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m670567184(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m670567184_MethodInfo_var);
	}

IL_0016:
	{
		return;
	}
}
// UnityEngine.Resolution[] UnityEngine.Screen::get_resolutions()
extern "C"  ResolutionU5BU5D_t665107673* Screen_get_resolutions_m3563081577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef ResolutionU5BU5D_t665107673* (*Screen_get_resolutions_m3563081577_ftn) ();
	static Screen_get_resolutions_m3563081577_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_resolutions_m3563081577_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_resolutions()");
	return _il2cpp_icall_func();
}
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t3693662728  Screen_get_currentResolution_m2361090437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Resolution_t3693662728  (*Screen_get_currentResolution_m2361090437_ftn) ();
	static Screen_get_currentResolution_m2361090437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_currentResolution_m2361090437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_currentResolution()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)
extern "C"  void Screen_SetResolution_m2850169807 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, bool ___fullscreen2, int32_t ___preferredRefreshRate3, const MethodInfo* method)
{
	typedef void (*Screen_SetResolution_m2850169807_ftn) (int32_t, int32_t, bool, int32_t);
	static Screen_SetResolution_m2850169807_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_SetResolution_m2850169807_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)");
	_il2cpp_icall_func(___width0, ___height1, ___fullscreen2, ___preferredRefreshRate3);
}
// System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean)
extern "C"  void Screen_SetResolution_m55027544 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, bool ___fullscreen2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		bool L_2 = ___fullscreen2;
		int32_t L_3 = V_0;
		Screen_SetResolution_m2850169807(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m41137238_ftn) ();
	static Screen_get_width_m41137238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m41137238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1051800773_ftn) ();
	static Screen_get_height_m1051800773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1051800773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m3345126327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m3345126327_ftn) ();
	static Screen_get_dpi_m3345126327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m3345126327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_sleepTimeout()
extern "C"  int32_t Screen_get_sleepTimeout_m405361946 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_sleepTimeout_m405361946_ftn) ();
	static Screen_get_sleepTimeout_m405361946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_sleepTimeout_m405361946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_sleepTimeout()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject__ctor_m2671490429_MetadataUsageId;
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m2671490429_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m1778903390(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m1778903390 (Il2CppObject * __this /* static, unused */, ScriptableObject_t1975622470 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn) (ScriptableObject_t1975622470 *);
	static ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m1778903390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstance_m3921674852 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t1975622470 * (*ScriptableObject_CreateInstance_m3921674852_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m3921674852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m3921674852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstance_m3271154163 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t1975622470 * L_1 = ScriptableObject_CreateInstanceFromType_m4271875689(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t1975622470 * ScriptableObject_CreateInstanceFromType_m4271875689 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t1975622470 * (*ScriptableObject_CreateInstanceFromType_m4271875689_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m4271875689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m4271875689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke(const ScriptableObject_t1975622470& unmarshaled, ScriptableObject_t1975622470_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_back(const ScriptableObject_t1975622470_marshaled_pinvoke& marshaled, ScriptableObject_t1975622470& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_pinvoke_cleanup(ScriptableObject_t1975622470_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_com(const ScriptableObject_t1975622470& unmarshaled, ScriptableObject_t1975622470_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t1975622470_marshal_com_back(const ScriptableObject_t1975622470_marshaled_com& marshaled, ScriptableObject_t1975622470& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_CachedPtr_0));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t1975622470_marshal_com_cleanup(ScriptableObject_t1975622470_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m2437378488 (PreserveAttribute_t4182602970 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m2374853658 (RequiredByNativeCodeAttribute_t1913052472 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m2459832290 (UsedByNativeCodeAttribute_t3212052468 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m1487697870 (SelectionBaseAttribute_t936505999 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t1761367055_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents__cctor_m1655934720_MetadataUsageId;
extern "C"  void SendMouseEvents__cctor_m1655934720 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents__cctor_m1655934720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t1761367055  V_0;
	memset(&V_0, 0, sizeof(V_0));
	HitInfo_t1761367055  V_1;
	memset(&V_1, 0, sizeof(V_1));
	HitInfo_t1761367055  V_2;
	memset(&V_2, 0, sizeof(V_2));
	HitInfo_t1761367055  V_3;
	memset(&V_3, 0, sizeof(V_3));
	HitInfo_t1761367055  V_4;
	memset(&V_4, 0, sizeof(V_4));
	HitInfo_t1761367055  V_5;
	memset(&V_5, 0, sizeof(V_5));
	HitInfo_t1761367055  V_6;
	memset(&V_6, 0, sizeof(V_6));
	HitInfo_t1761367055  V_7;
	memset(&V_7, 0, sizeof(V_7));
	HitInfo_t1761367055  V_8;
	memset(&V_8, 0, sizeof(V_8));
	{
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		HitInfoU5BU5D_t934504150* L_0 = ((HitInfoU5BU5D_t934504150*)SZArrayNew(HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_0));
		HitInfo_t1761367055  L_1 = V_0;
		(*(HitInfo_t1761367055 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		HitInfoU5BU5D_t934504150* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_1));
		HitInfo_t1761367055  L_3 = V_1;
		(*(HitInfo_t1761367055 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		HitInfoU5BU5D_t934504150* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1761367055  L_5 = V_2;
		(*(HitInfo_t1761367055 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_LastHit_1(L_4);
		HitInfoU5BU5D_t934504150* L_6 = ((HitInfoU5BU5D_t934504150*)SZArrayNew(HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_3));
		HitInfo_t1761367055  L_7 = V_3;
		(*(HitInfo_t1761367055 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_7;
		HitInfoU5BU5D_t934504150* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_4));
		HitInfo_t1761367055  L_9 = V_4;
		(*(HitInfo_t1761367055 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_9;
		HitInfoU5BU5D_t934504150* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_5));
		HitInfo_t1761367055  L_11 = V_5;
		(*(HitInfo_t1761367055 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_11;
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_MouseDownHit_2(L_10);
		HitInfoU5BU5D_t934504150* L_12 = ((HitInfoU5BU5D_t934504150*)SZArrayNew(HitInfoU5BU5D_t934504150_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_6));
		HitInfo_t1761367055  L_13 = V_6;
		(*(HitInfo_t1761367055 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_13;
		HitInfoU5BU5D_t934504150* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_7));
		HitInfo_t1761367055  L_15 = V_7;
		(*(HitInfo_t1761367055 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_15;
		HitInfoU5BU5D_t934504150* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_8));
		HitInfo_t1761367055  L_17 = V_8;
		(*(HitInfo_t1761367055 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_17;
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_CurrentHit_3(L_16);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SetMouseMoved()
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern const uint32_t SendMouseEvents_SetMouseMoved_m532965689_MetadataUsageId;
extern "C"  void SendMouseEvents_SetMouseMoved_m532965689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SetMouseMoved_m532965689_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraU5BU5D_t3079764780_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t1761367055_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUILayer_t3254902478_m2431610297_MethodInfo_var;
extern const uint32_t SendMouseEvents_DoSendMouseEvents_m701697135_MetadataUsageId;
extern "C"  void SendMouseEvents_DoSendMouseEvents_m701697135 (Il2CppObject * __this /* static, unused */, int32_t ___skipRTCameras0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_DoSendMouseEvents_m701697135_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Camera_t189460977 * V_3 = NULL;
	CameraU5BU5D_t3079764780* V_4 = NULL;
	int32_t V_5 = 0;
	Rect_t3681755626  V_6;
	memset(&V_6, 0, sizeof(V_6));
	GUILayer_t3254902478 * V_7 = NULL;
	GUIElement_t3381083099 * V_8 = NULL;
	Ray_t2469606224  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	GameObject_t1756533147 * V_12 = NULL;
	GameObject_t1756533147 * V_13 = NULL;
	int32_t V_14 = 0;
	HitInfo_t1761367055  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t2243707580  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float G_B23_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Camera_get_allCamerasCount_m989474043(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_2 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_3 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		NullCheck(L_3);
		int32_t L_4 = V_1;
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) == ((int32_t)L_4)))
		{
			goto IL_002e;
		}
	}

IL_0023:
	{
		int32_t L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_m_Cameras_4(((CameraU5BU5D_t3079764780*)SZArrayNew(CameraU5BU5D_t3079764780_il2cpp_TypeInfo_var, (uint32_t)L_5)));
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_6 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		Camera_GetAllCameras_m2922515227(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_005e;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_7 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_15));
		HitInfo_t1761367055  L_9 = V_15;
		(*(HitInfo_t1761367055 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
		int32_t L_10 = V_2;
		V_2 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_12 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		bool L_13 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_s_MouseUsed_0();
		if (L_13)
		{
			goto IL_02c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		CameraU5BU5D_t3079764780* L_14 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_Cameras_4();
		V_4 = L_14;
		V_5 = 0;
		goto IL_02b8;
	}

IL_0084:
	{
		CameraU5BU5D_t3079764780* L_15 = V_4;
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Camera_t189460977 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = L_18;
		Camera_t189460977 * L_19 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_19, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00ad;
		}
	}
	{
		int32_t L_21 = ___skipRTCameras0;
		if (!L_21)
		{
			goto IL_00b2;
		}
	}
	{
		Camera_t189460977 * L_22 = V_3;
		NullCheck(L_22);
		RenderTexture_t2666733923 * L_23 = Camera_get_targetTexture_m705925974(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_23, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b2;
		}
	}

IL_00ad:
	{
		goto IL_02b2;
	}

IL_00b2:
	{
		Camera_t189460977 * L_25 = V_3;
		NullCheck(L_25);
		Rect_t3681755626  L_26 = Camera_get_pixelRect_m2084185953(L_25, /*hidden argument*/NULL);
		V_6 = L_26;
		Vector3_t2243707580  L_27 = V_0;
		bool L_28 = Rect_Contains_m1334685291((&V_6), L_27, /*hidden argument*/NULL);
		if (L_28)
		{
			goto IL_00cc;
		}
	}
	{
		goto IL_02b2;
	}

IL_00cc:
	{
		Camera_t189460977 * L_29 = V_3;
		NullCheck(L_29);
		GUILayer_t3254902478 * L_30 = Component_GetComponent_TisGUILayer_t3254902478_m2431610297(L_29, /*hidden argument*/Component_GetComponent_TisGUILayer_t3254902478_m2431610297_MethodInfo_var);
		V_7 = L_30;
		GUILayer_t3254902478 * L_31 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0145;
		}
	}
	{
		GUILayer_t3254902478 * L_33 = V_7;
		Vector3_t2243707580  L_34 = V_0;
		NullCheck(L_33);
		GUIElement_t3381083099 * L_35 = GUILayer_HitTest_m2960428006(L_33, L_34, /*hidden argument*/NULL);
		V_8 = L_35;
		GUIElement_t3381083099 * L_36 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_37 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_38 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		GUIElement_t3381083099 * L_39 = V_8;
		NullCheck(L_39);
		GameObject_t1756533147 * L_40 = Component_get_gameObject_m3105766835(L_39, /*hidden argument*/NULL);
		((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0(L_40);
		HitInfoU5BU5D_t934504150* L_41 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 0);
		Camera_t189460977 * L_42 = V_3;
		((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1(L_42);
		goto IL_0145;
	}

IL_0123:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_43 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_target_0((GameObject_t1756533147 *)NULL);
		HitInfoU5BU5D_t934504150* L_44 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 0);
		((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_camera_1((Camera_t189460977 *)NULL);
	}

IL_0145:
	{
		Camera_t189460977 * L_45 = V_3;
		NullCheck(L_45);
		int32_t L_46 = Camera_get_eventMask_m4241372419(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_0155;
		}
	}
	{
		goto IL_02b2;
	}

IL_0155:
	{
		Camera_t189460977 * L_47 = V_3;
		Vector3_t2243707580  L_48 = V_0;
		NullCheck(L_47);
		Ray_t2469606224  L_49 = Camera_ScreenPointToRay_m614889538(L_47, L_48, /*hidden argument*/NULL);
		V_9 = L_49;
		Vector3_t2243707580  L_50 = Ray_get_direction_m4059191533((&V_9), /*hidden argument*/NULL);
		V_16 = L_50;
		float L_51 = (&V_16)->get_z_3();
		V_10 = L_51;
		float L_52 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		bool L_53 = Mathf_Approximately_m1064446634(NULL /*static, unused*/, (0.0f), L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_018b;
		}
	}
	{
		G_B23_0 = (std::numeric_limits<float>::infinity());
		goto IL_01a0;
	}

IL_018b:
	{
		Camera_t189460977 * L_54 = V_3;
		NullCheck(L_54);
		float L_55 = Camera_get_farClipPlane_m3137713566(L_54, /*hidden argument*/NULL);
		Camera_t189460977 * L_56 = V_3;
		NullCheck(L_56);
		float L_57 = Camera_get_nearClipPlane_m3536967407(L_56, /*hidden argument*/NULL);
		float L_58 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_59 = fabsf(((float)((float)((float)((float)L_55-(float)L_57))/(float)L_58)));
		G_B23_0 = L_59;
	}

IL_01a0:
	{
		V_11 = G_B23_0;
		Camera_t189460977 * L_60 = V_3;
		Ray_t2469606224  L_61 = V_9;
		float L_62 = V_11;
		Camera_t189460977 * L_63 = V_3;
		NullCheck(L_63);
		int32_t L_64 = Camera_get_cullingMask_m73686965(L_63, /*hidden argument*/NULL);
		Camera_t189460977 * L_65 = V_3;
		NullCheck(L_65);
		int32_t L_66 = Camera_get_eventMask_m4241372419(L_65, /*hidden argument*/NULL);
		NullCheck(L_60);
		GameObject_t1756533147 * L_67 = Camera_RaycastTry_m3412198936(L_60, L_61, L_62, ((int32_t)((int32_t)L_64&(int32_t)L_66)), /*hidden argument*/NULL);
		V_12 = L_67;
		GameObject_t1756533147 * L_68 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_68, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_01f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_70 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 1);
		GameObject_t1756533147 * L_71 = V_12;
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0(L_71);
		HitInfoU5BU5D_t934504150* L_72 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		Camera_t189460977 * L_73 = V_3;
		((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1(L_73);
		goto IL_022a;
	}

IL_01f0:
	{
		Camera_t189460977 * L_74 = V_3;
		NullCheck(L_74);
		int32_t L_75 = Camera_get_clearFlags_m1743144302(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) == ((int32_t)1)))
		{
			goto IL_0208;
		}
	}
	{
		Camera_t189460977 * L_76 = V_3;
		NullCheck(L_76);
		int32_t L_77 = Camera_get_clearFlags_m1743144302(L_76, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_77) == ((uint32_t)2))))
		{
			goto IL_022a;
		}
	}

IL_0208:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_78 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 1);
		((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_target_0((GameObject_t1756533147 *)NULL);
		HitInfoU5BU5D_t934504150* L_79 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, 1);
		((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_camera_1((Camera_t189460977 *)NULL);
	}

IL_022a:
	{
		Camera_t189460977 * L_80 = V_3;
		Ray_t2469606224  L_81 = V_9;
		float L_82 = V_11;
		Camera_t189460977 * L_83 = V_3;
		NullCheck(L_83);
		int32_t L_84 = Camera_get_cullingMask_m73686965(L_83, /*hidden argument*/NULL);
		Camera_t189460977 * L_85 = V_3;
		NullCheck(L_85);
		int32_t L_86 = Camera_get_eventMask_m4241372419(L_85, /*hidden argument*/NULL);
		NullCheck(L_80);
		GameObject_t1756533147 * L_87 = Camera_RaycastTry2D_m755036866(L_80, L_81, L_82, ((int32_t)((int32_t)L_84&(int32_t)L_86)), /*hidden argument*/NULL);
		V_13 = L_87;
		GameObject_t1756533147 * L_88 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_88, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_0278;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_90 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 2);
		GameObject_t1756533147 * L_91 = V_13;
		((L_90)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0(L_91);
		HitInfoU5BU5D_t934504150* L_92 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 2);
		Camera_t189460977 * L_93 = V_3;
		((L_92)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1(L_93);
		goto IL_02b2;
	}

IL_0278:
	{
		Camera_t189460977 * L_94 = V_3;
		NullCheck(L_94);
		int32_t L_95 = Camera_get_clearFlags_m1743144302(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) == ((int32_t)1)))
		{
			goto IL_0290;
		}
	}
	{
		Camera_t189460977 * L_96 = V_3;
		NullCheck(L_96);
		int32_t L_97 = Camera_get_clearFlags_m1743144302(L_96, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_97) == ((uint32_t)2))))
		{
			goto IL_02b2;
		}
	}

IL_0290:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_98 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 2);
		((L_98)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_target_0((GameObject_t1756533147 *)NULL);
		HitInfoU5BU5D_t934504150* L_99 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, 2);
		((L_99)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_camera_1((Camera_t189460977 *)NULL);
	}

IL_02b2:
	{
		int32_t L_100 = V_5;
		V_5 = ((int32_t)((int32_t)L_100+(int32_t)1));
	}

IL_02b8:
	{
		int32_t L_101 = V_5;
		CameraU5BU5D_t3079764780* L_102 = V_4;
		NullCheck(L_102);
		if ((((int32_t)L_101) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_102)->max_length)))))))
		{
			goto IL_0084;
		}
	}

IL_02c3:
	{
		V_14 = 0;
		goto IL_02e9;
	}

IL_02cb:
	{
		int32_t L_103 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_104 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		int32_t L_105 = V_14;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		SendMouseEvents_SendEvents_m2738043830(NULL /*static, unused*/, L_103, (*(HitInfo_t1761367055 *)((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_105)))), /*hidden argument*/NULL);
		int32_t L_106 = V_14;
		V_14 = ((int32_t)((int32_t)L_106+(int32_t)1));
	}

IL_02e9:
	{
		int32_t L_107 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_108 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_CurrentHit_3();
		NullCheck(L_108);
		if ((((int32_t)L_107) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_108)->max_length)))))))
		{
			goto IL_02cb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->set_s_MouseUsed_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* SendMouseEvents_t3505065032_il2cpp_TypeInfo_var;
extern Il2CppClass* HitInfo_t1761367055_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4145672138;
extern Il2CppCodeGenString* _stringLiteral1449131165;
extern Il2CppCodeGenString* _stringLiteral3280802065;
extern Il2CppCodeGenString* _stringLiteral301042844;
extern Il2CppCodeGenString* _stringLiteral368299876;
extern Il2CppCodeGenString* _stringLiteral85975202;
extern Il2CppCodeGenString* _stringLiteral463234816;
extern const uint32_t SendMouseEvents_SendEvents_m2738043830_MetadataUsageId;
extern "C"  void SendMouseEvents_SendEvents_m2738043830 (Il2CppObject * __this /* static, unused */, int32_t ___i0, HitInfo_t1761367055  ___hit1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SendMouseEvents_SendEvents_m2738043830_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	HitInfo_t1761367055  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		HitInfo_t1761367055  L_3 = ___hit1;
		bool L_4 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_5 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_6 = ___i0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		HitInfo_t1761367055  L_7 = ___hit1;
		(*(HitInfo_t1761367055 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		HitInfoU5BU5D_t934504150* L_8 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_9 = ___i0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		HitInfo_SendMessage_m3368777144(((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9))), _stringLiteral4145672138, /*hidden argument*/NULL);
	}

IL_0045:
	{
		goto IL_00fc;
	}

IL_004a:
	{
		bool L_10 = V_1;
		if (L_10)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_11 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_12 = ___i0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		bool L_13 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, (*(HitInfo_t1761367055 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))), /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00c8;
		}
	}
	{
		HitInfo_t1761367055  L_14 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_15 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_16 = ___i0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		bool L_17 = HitInfo_Compare_m4272872794(NULL /*static, unused*/, L_14, (*(HitInfo_t1761367055 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_18 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_19 = ___i0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		HitInfo_SendMessage_m3368777144(((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19))), _stringLiteral1449131165, /*hidden argument*/NULL);
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_20 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_21 = ___i0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		HitInfo_SendMessage_m3368777144(((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_21))), _stringLiteral3280802065, /*hidden argument*/NULL);
		HitInfoU5BU5D_t934504150* L_22 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_23 = ___i0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		Initobj (HitInfo_t1761367055_il2cpp_TypeInfo_var, (&V_2));
		HitInfo_t1761367055  L_24 = V_2;
		(*(HitInfo_t1761367055 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))) = L_24;
	}

IL_00c8:
	{
		goto IL_00fc;
	}

IL_00cd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_25 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_26 = ___i0;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		bool L_27 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, (*(HitInfo_t1761367055 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_28 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_MouseDownHit_2();
		int32_t L_29 = ___i0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		HitInfo_SendMessage_m3368777144(((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29))), _stringLiteral301042844, /*hidden argument*/NULL);
	}

IL_00fc:
	{
		HitInfo_t1761367055  L_30 = ___hit1;
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_31 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_32 = ___i0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		bool L_33 = HitInfo_Compare_m4272872794(NULL /*static, unused*/, L_30, (*(HitInfo_t1761367055 *)((L_31)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_32)))), /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0133;
		}
	}
	{
		HitInfo_t1761367055  L_34 = ___hit1;
		bool L_35 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012e;
		}
	}
	{
		HitInfo_SendMessage_m3368777144((&___hit1), _stringLiteral368299876, /*hidden argument*/NULL);
	}

IL_012e:
	{
		goto IL_0185;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_36 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_37 = ___i0;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		bool L_38 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, (*(HitInfo_t1761367055 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))), /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0162;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_39 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_40 = ___i0;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		HitInfo_SendMessage_m3368777144(((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40))), _stringLiteral85975202, /*hidden argument*/NULL);
	}

IL_0162:
	{
		HitInfo_t1761367055  L_41 = ___hit1;
		bool L_42 = HitInfo_op_Implicit_m1583347317(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0185;
		}
	}
	{
		HitInfo_SendMessage_m3368777144((&___hit1), _stringLiteral463234816, /*hidden argument*/NULL);
		HitInfo_SendMessage_m3368777144((&___hit1), _stringLiteral368299876, /*hidden argument*/NULL);
	}

IL_0185:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SendMouseEvents_t3505065032_il2cpp_TypeInfo_var);
		HitInfoU5BU5D_t934504150* L_43 = ((SendMouseEvents_t3505065032_StaticFields*)SendMouseEvents_t3505065032_il2cpp_TypeInfo_var->static_fields)->get_m_LastHit_1();
		int32_t L_44 = ___i0;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		HitInfo_t1761367055  L_45 = ___hit1;
		(*(HitInfo_t1761367055 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_44)))) = L_45;
		return;
	}
}
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern "C"  void HitInfo_SendMessage_m3368777144 (HitInfo_t1761367055 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_target_0();
		String_t* L_1 = ___name0;
		NullCheck(L_0);
		GameObject_SendMessage_m71956653(L_0, L_1, NULL, 1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void HitInfo_SendMessage_m3368777144_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	HitInfo_t1761367055 * _thisAdjusted = reinterpret_cast<HitInfo_t1761367055 *>(__this + 1);
	HitInfo_SendMessage_m3368777144(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HitInfo_Compare_m4272872794_MetadataUsageId;
extern "C"  bool HitInfo_Compare_m4272872794 (Il2CppObject * __this /* static, unused */, HitInfo_t1761367055  ___lhs0, HitInfo_t1761367055  ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_Compare_m4272872794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		GameObject_t1756533147 * L_0 = (&___lhs0)->get_target_0();
		GameObject_t1756533147 * L_1 = (&___rhs1)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Camera_t189460977 * L_3 = (&___lhs0)->get_camera_1();
		Camera_t189460977 * L_4 = (&___rhs1)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t HitInfo_op_Implicit_m1583347317_MetadataUsageId;
extern "C"  bool HitInfo_op_Implicit_m1583347317 (Il2CppObject * __this /* static, unused */, HitInfo_t1761367055  ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitInfo_op_Implicit_m1583347317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		GameObject_t1756533147 * L_0 = (&___exists0)->get_target_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Camera_t189460977 * L_2 = (&___exists0)->get_camera_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_pinvoke(const HitInfo_t1761367055& unmarshaled, HitInfo_t1761367055_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t1761367055_marshal_pinvoke_back(const HitInfo_t1761367055_marshaled_pinvoke& marshaled, HitInfo_t1761367055& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_pinvoke_cleanup(HitInfo_t1761367055_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_com(const HitInfo_t1761367055& unmarshaled, HitInfo_t1761367055_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
extern "C" void HitInfo_t1761367055_marshal_com_back(const HitInfo_t1761367055_marshaled_com& marshaled, HitInfo_t1761367055& unmarshaled)
{
	Il2CppCodeGenException* ___target_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'target' of type 'HitInfo': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___target_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.SendMouseEvents/HitInfo
extern "C" void HitInfo_t1761367055_marshal_com_cleanup(HitInfo_t1761367055_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern "C"  void FormerlySerializedAsAttribute__ctor_m3551035707 (FormerlySerializedAsAttribute_t3673080018 * __this, String_t* ___oldName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___oldName0;
		__this->set_m_oldName_0(L_0);
		return;
	}
}
// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::get_oldName()
extern "C"  String_t* FormerlySerializedAsAttribute_get_oldName_m3225463145 (FormerlySerializedAsAttribute_t3673080018 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_oldName_0();
		return L_0;
	}
}
// System.Void UnityEngine.SerializeField::.ctor()
extern "C"  void SerializeField__ctor_m994129777 (SerializeField_t3073427462 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C"  void SerializePrivateVariables__ctor_m806793207 (SerializePrivateVariables_t2241034664 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral576534231;
extern Il2CppCodeGenString* _stringLiteral1994281847;
extern const uint32_t SetupCoroutine_InvokeMoveNext_m2975616245_MetadataUsageId;
extern "C"  void SetupCoroutine_InvokeMoveNext_m2975616245 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, IntPtr_t ___returnValueAddress1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMoveNext_m2975616245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___returnValueAddress1;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m1573482188(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		ArgumentException_t3259014390 * L_3 = (ArgumentException_t3259014390 *)il2cpp_codegen_object_new(ArgumentException_t3259014390_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m544251339(L_3, _stringLiteral576534231, _stringLiteral1994281847, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0020:
	{
		IntPtr_t L_4 = ___returnValueAddress1;
		void* L_5 = IntPtr_op_Explicit_m1073656736(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Il2CppObject * L_6 = ___enumerator0;
		NullCheck(L_6);
		bool L_7 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_6);
		*((int8_t*)(L_5)) = (int8_t)L_7;
		return;
	}
}
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t SetupCoroutine_InvokeMember_m1481430263_MetadataUsageId;
extern "C"  Il2CppObject * SetupCoroutine_InvokeMember_m1481430263 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___behaviour0, String_t* ___name1, Il2CppObject * ___variable2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SetupCoroutine_InvokeMember_m1481430263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	{
		V_0 = (ObjectU5BU5D_t3614634134*)NULL;
		Il2CppObject * L_0 = ___variable2;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		ObjectU5BU5D_t3614634134* L_1 = V_0;
		Il2CppObject * L_2 = ___variable2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_2);
	}

IL_0013:
	{
		Il2CppObject * L_3 = ___behaviour0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		String_t* L_5 = ___name1;
		Il2CppObject * L_6 = ___behaviour0;
		ObjectU5BU5D_t3614634134* L_7 = V_0;
		NullCheck(L_4);
		Il2CppObject * L_8 = VirtFuncInvoker8< Il2CppObject *, String_t*, int32_t, Binder_t3404612058 *, Il2CppObject *, ObjectU5BU5D_t3614634134*, ParameterModifierU5BU5D_t963192633*, CultureInfo_t3500843524 *, StringU5BU5D_t1642385972* >::Invoke(76 /* System.Object System.Type::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[]) */, L_4, L_5, ((int32_t)308), (Binder_t3404612058 *)NULL, L_6, L_7, (ParameterModifierU5BU5D_t963192633*)(ParameterModifierU5BU5D_t963192633*)NULL, (CultureInfo_t3500843524 *)NULL, (StringU5BU5D_t1642385972*)(StringU5BU5D_t1642385972*)NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C"  int32_t Shader_PropertyToID_m678579425 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m678579425_ftn) (String_t*);
	static Shader_PropertyToID_m678579425_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m678579425_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern "C"  void SharedBetweenAnimatorsAttribute__ctor_m1221241062 (SharedBetweenAnimatorsAttribute_t1565472209 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_pinvoke(const SkeletonBone_t345082847& unmarshaled, SkeletonBone_t345082847_marshaled_pinvoke& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.get_name_0());
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t4030073918_marshal_pinvoke(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_back(const SkeletonBone_t345082847_marshaled_pinvoke& marshaled, SkeletonBone_t345082847& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_string_result(marshaled.___name_0));
	Vector3_t2243707580  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t4030073918  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t4030073918_marshal_pinvoke_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t2243707580  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_pinvoke_cleanup(SkeletonBone_t345082847_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___position_1);
	Quaternion_t4030073918_marshal_pinvoke_cleanup(marshaled.___rotation_2);
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___scale_3);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_com(const SkeletonBone_t345082847& unmarshaled, SkeletonBone_t345082847_marshaled_com& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_name_0());
	Vector3_t2243707580_marshal_com(unmarshaled.get_position_1(), marshaled.___position_1);
	Quaternion_t4030073918_marshal_com(unmarshaled.get_rotation_2(), marshaled.___rotation_2);
	Vector3_t2243707580_marshal_com(unmarshaled.get_scale_3(), marshaled.___scale_3);
	marshaled.___transformModified_4 = unmarshaled.get_transformModified_4();
}
extern "C" void SkeletonBone_t345082847_marshal_com_back(const SkeletonBone_t345082847_marshaled_com& marshaled, SkeletonBone_t345082847& unmarshaled)
{
	unmarshaled.set_name_0(il2cpp_codegen_marshal_bstring_result(marshaled.___name_0));
	Vector3_t2243707580  unmarshaled_position_temp_1;
	memset(&unmarshaled_position_temp_1, 0, sizeof(unmarshaled_position_temp_1));
	Vector3_t2243707580_marshal_com_back(marshaled.___position_1, unmarshaled_position_temp_1);
	unmarshaled.set_position_1(unmarshaled_position_temp_1);
	Quaternion_t4030073918  unmarshaled_rotation_temp_2;
	memset(&unmarshaled_rotation_temp_2, 0, sizeof(unmarshaled_rotation_temp_2));
	Quaternion_t4030073918_marshal_com_back(marshaled.___rotation_2, unmarshaled_rotation_temp_2);
	unmarshaled.set_rotation_2(unmarshaled_rotation_temp_2);
	Vector3_t2243707580  unmarshaled_scale_temp_3;
	memset(&unmarshaled_scale_temp_3, 0, sizeof(unmarshaled_scale_temp_3));
	Vector3_t2243707580_marshal_com_back(marshaled.___scale_3, unmarshaled_scale_temp_3);
	unmarshaled.set_scale_3(unmarshaled_scale_temp_3);
	int32_t unmarshaled_transformModified_temp_4 = 0;
	unmarshaled_transformModified_temp_4 = marshaled.___transformModified_4;
	unmarshaled.set_transformModified_4(unmarshaled_transformModified_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t345082847_marshal_com_cleanup(SkeletonBone_t345082847_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___name_0);
	marshaled.___name_0 = NULL;
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___position_1);
	Quaternion_t4030073918_marshal_com_cleanup(marshaled.___rotation_2);
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___scale_3);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
