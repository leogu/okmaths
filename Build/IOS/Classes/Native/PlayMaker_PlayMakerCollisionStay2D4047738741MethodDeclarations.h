﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionStay2D
struct PlayMakerCollisionStay2D_t4047738741;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"

// System.Void PlayMakerCollisionStay2D::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionStay2D_OnCollisionStay2D_m3480195773 (PlayMakerCollisionStay2D_t4047738741 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionStay2D::.ctor()
extern "C"  void PlayMakerCollisionStay2D__ctor_m660356446 (PlayMakerCollisionStay2D_t4047738741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
