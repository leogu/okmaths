﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveX
struct DOTweenRigidbodyMoveX_t684693156;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveX::.ctor()
extern "C"  void DOTweenRigidbodyMoveX__ctor_m2117606552 (DOTweenRigidbodyMoveX_t684693156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveX::Reset()
extern "C"  void DOTweenRigidbodyMoveX_Reset_m123912893 (DOTweenRigidbodyMoveX_t684693156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveX::OnEnter()
extern "C"  void DOTweenRigidbodyMoveX_OnEnter_m1859680565 (DOTweenRigidbodyMoveX_t684693156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveX::<OnEnter>m__8A()
extern "C"  void DOTweenRigidbodyMoveX_U3COnEnterU3Em__8A_m356800003 (DOTweenRigidbodyMoveX_t684693156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveX::<OnEnter>m__8B()
extern "C"  void DOTweenRigidbodyMoveX_U3COnEnterU3Em__8B_m356799974 (DOTweenRigidbodyMoveX_t684693156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
