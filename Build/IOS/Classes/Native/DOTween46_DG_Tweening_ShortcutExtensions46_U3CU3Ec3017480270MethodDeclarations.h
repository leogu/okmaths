﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_t3017480270;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass36_0__ctor_m1714853417 (U3CU3Ec__DisplayClass36_0_t3017480270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0::<DOBlendableColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__0_m2823944853 (U3CU3Ec__DisplayClass36_0_t3017480270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__1_m1854242535 (U3CU3Ec__DisplayClass36_0_t3017480270 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
