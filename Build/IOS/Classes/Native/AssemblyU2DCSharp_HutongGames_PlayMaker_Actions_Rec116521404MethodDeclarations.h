﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax
struct RectTransformSetAnchorMinAndMax_t116521404;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::.ctor()
extern "C"  void RectTransformSetAnchorMinAndMax__ctor_m2453996558 (RectTransformSetAnchorMinAndMax_t116521404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::Reset()
extern "C"  void RectTransformSetAnchorMinAndMax_Reset_m487384785 (RectTransformSetAnchorMinAndMax_t116521404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::OnEnter()
extern "C"  void RectTransformSetAnchorMinAndMax_OnEnter_m3107726641 (RectTransformSetAnchorMinAndMax_t116521404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::OnActionUpdate()
extern "C"  void RectTransformSetAnchorMinAndMax_OnActionUpdate_m2385955170 (RectTransformSetAnchorMinAndMax_t116521404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMinAndMax::DoSetAnchorMax()
extern "C"  void RectTransformSetAnchorMinAndMax_DoSetAnchorMax_m2552076396 (RectTransformSetAnchorMinAndMax_t116521404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
