﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFlareStrength
struct SetFlareStrength_t1646161835;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::.ctor()
extern "C"  void SetFlareStrength__ctor_m1790257981 (SetFlareStrength_t1646161835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::Reset()
extern "C"  void SetFlareStrength_Reset_m1608230324 (SetFlareStrength_t1646161835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::OnEnter()
extern "C"  void SetFlareStrength_OnEnter_m709822310 (SetFlareStrength_t1646161835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::OnUpdate()
extern "C"  void SetFlareStrength_OnUpdate_m1591378545 (SetFlareStrength_t1646161835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFlareStrength::DoSetFlareStrength()
extern "C"  void SetFlareStrength_DoSetFlareStrength_m89931073 (SetFlareStrength_t1646161835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
