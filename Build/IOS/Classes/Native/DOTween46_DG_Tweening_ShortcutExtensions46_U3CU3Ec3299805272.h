﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t3299805272  : public Il2CppObject
{
public:
	// UnityEngine.UI.Slider DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::target
	Slider_t297367283 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t3299805272, ___target_0)); }
	inline Slider_t297367283 * get_target_0() const { return ___target_0; }
	inline Slider_t297367283 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Slider_t297367283 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
