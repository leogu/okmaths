﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InputNavigator
struct InputNavigator_t3938593723;

#include "codegen/il2cpp-codegen.h"

// System.Void InputNavigator::.ctor()
extern "C"  void InputNavigator__ctor_m2708649228 (InputNavigator_t3938593723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputNavigator::Start()
extern "C"  void InputNavigator_Start_m2302521928 (InputNavigator_t3938593723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InputNavigator::Update()
extern "C"  void InputNavigator_Update_m209879515 (InputNavigator_t3938593723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
