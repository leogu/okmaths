﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable
struct PlayMakerFsmVariable_t147113364;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget
struct PlayMakerFsmVariableTarget_t681224443;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_4024510117.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_U681224443.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::.ctor()
extern "C"  void PlayMakerFsmVariable__ctor_m1936900548 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::.ctor(HutongGames.PlayMaker.Ecosystem.Utils.VariableSelectionChoice)
extern "C"  void PlayMakerFsmVariable__ctor_m3420887324 (PlayMakerFsmVariable_t147113364 * __this, int32_t ___variableSelectionChoice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::.ctor(System.String)
extern "C"  void PlayMakerFsmVariable__ctor_m4074810126 (PlayMakerFsmVariable_t147113364 * __this, String_t* ___defaultVariableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::.ctor(HutongGames.PlayMaker.Ecosystem.Utils.VariableSelectionChoice,System.String)
extern "C"  void PlayMakerFsmVariable__ctor_m3177902728 (PlayMakerFsmVariable_t147113364 * __this, int32_t ___variableSelectionChoice0, String_t* ___defaultVariableName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_namedVariable()
extern "C"  NamedVariable_t3026441313 * PlayMakerFsmVariable_get_namedVariable_m3505898742 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmFloat()
extern "C"  FsmFloat_t937133978 * PlayMakerFsmVariable_get_FsmFloat_m2448092858 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmInt()
extern "C"  FsmInt_t1273009179 * PlayMakerFsmVariable_get_FsmInt_m741012896 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmBool()
extern "C"  FsmBool_t664485696 * PlayMakerFsmVariable_get_FsmBool_m1601123638 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmGameObject()
extern "C"  FsmGameObject_t3097142863 * PlayMakerFsmVariable_get_FsmGameObject_m1055186070 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmColor()
extern "C"  FsmColor_t118301965 * PlayMakerFsmVariable_get_FsmColor_m1497619860 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmMaterial()
extern "C"  FsmMaterial_t1421632035 * PlayMakerFsmVariable_get_FsmMaterial_m2177885494 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmObject()
extern "C"  FsmObject_t2785794313 * PlayMakerFsmVariable_get_FsmObject_m4134659350 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmQuaternion()
extern "C"  FsmQuaternion_t878438756 * PlayMakerFsmVariable_get_FsmQuaternion_m2001793270 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmRect()
extern "C"  FsmRect_t19023354 * PlayMakerFsmVariable_get_FsmRect_m3795318710 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmString()
extern "C"  FsmString_t2414474701 * PlayMakerFsmVariable_get_FsmString_m3755608086 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmTexture()
extern "C"  FsmTexture_t3372293163 * PlayMakerFsmVariable_get_FsmTexture_m1698348000 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmVector2()
extern "C"  FsmVector2_t2430450063 * PlayMakerFsmVariable_get_FsmVector2_m1118051056 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::get_FsmVector3()
extern "C"  FsmVector3_t3996534004 * PlayMakerFsmVariable_get_FsmVector3_m3208085174 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::GetVariable(HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget)
extern "C"  bool PlayMakerFsmVariable_GetVariable_m1569560338 (PlayMakerFsmVariable_t147113364 * __this, PlayMakerFsmVariableTarget_t681224443 * ___variableTarget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::ToString()
extern "C"  String_t* PlayMakerFsmVariable_ToString_m583716251 (PlayMakerFsmVariable_t147113364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::GetTypeFromChoice(HutongGames.PlayMaker.Ecosystem.Utils.VariableSelectionChoice)
extern "C"  int32_t PlayMakerFsmVariable_GetTypeFromChoice_m2246309168 (Il2CppObject * __this /* static, unused */, int32_t ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
