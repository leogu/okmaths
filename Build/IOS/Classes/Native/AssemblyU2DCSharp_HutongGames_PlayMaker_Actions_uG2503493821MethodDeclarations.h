﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder
struct uGuiInputFieldGetPlaceHolder_t2503493821;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::.ctor()
extern "C"  void uGuiInputFieldGetPlaceHolder__ctor_m935964321 (uGuiInputFieldGetPlaceHolder_t2503493821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::Reset()
extern "C"  void uGuiInputFieldGetPlaceHolder_Reset_m2376139522 (uGuiInputFieldGetPlaceHolder_t2503493821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::OnEnter()
extern "C"  void uGuiInputFieldGetPlaceHolder_OnEnter_m2884237768 (uGuiInputFieldGetPlaceHolder_t2503493821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::DoGetValue()
extern "C"  void uGuiInputFieldGetPlaceHolder_DoGetValue_m1588845203 (uGuiInputFieldGetPlaceHolder_t2503493821 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
