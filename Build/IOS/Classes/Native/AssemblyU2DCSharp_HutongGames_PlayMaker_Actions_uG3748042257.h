﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties
struct  uGuiNavigationExplicitGetProperties_t3748042257  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::selectOnDown
	FsmGameObject_t3097142863 * ___selectOnDown_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::selectOnUp
	FsmGameObject_t3097142863 * ___selectOnUp_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::selectOnLeft
	FsmGameObject_t3097142863 * ___selectOnLeft_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::selectOnRight
	FsmGameObject_t3097142863 * ___selectOnRight_15;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::_selectable
	Selectable_t1490392188 * ____selectable_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitGetProperties_t3748042257, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_selectOnDown_12() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitGetProperties_t3748042257, ___selectOnDown_12)); }
	inline FsmGameObject_t3097142863 * get_selectOnDown_12() const { return ___selectOnDown_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnDown_12() { return &___selectOnDown_12; }
	inline void set_selectOnDown_12(FsmGameObject_t3097142863 * value)
	{
		___selectOnDown_12 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnDown_12, value);
	}

	inline static int32_t get_offset_of_selectOnUp_13() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitGetProperties_t3748042257, ___selectOnUp_13)); }
	inline FsmGameObject_t3097142863 * get_selectOnUp_13() const { return ___selectOnUp_13; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnUp_13() { return &___selectOnUp_13; }
	inline void set_selectOnUp_13(FsmGameObject_t3097142863 * value)
	{
		___selectOnUp_13 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnUp_13, value);
	}

	inline static int32_t get_offset_of_selectOnLeft_14() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitGetProperties_t3748042257, ___selectOnLeft_14)); }
	inline FsmGameObject_t3097142863 * get_selectOnLeft_14() const { return ___selectOnLeft_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnLeft_14() { return &___selectOnLeft_14; }
	inline void set_selectOnLeft_14(FsmGameObject_t3097142863 * value)
	{
		___selectOnLeft_14 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnLeft_14, value);
	}

	inline static int32_t get_offset_of_selectOnRight_15() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitGetProperties_t3748042257, ___selectOnRight_15)); }
	inline FsmGameObject_t3097142863 * get_selectOnRight_15() const { return ___selectOnRight_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnRight_15() { return &___selectOnRight_15; }
	inline void set_selectOnRight_15(FsmGameObject_t3097142863 * value)
	{
		___selectOnRight_15 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnRight_15, value);
	}

	inline static int32_t get_offset_of__selectable_16() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitGetProperties_t3748042257, ____selectable_16)); }
	inline Selectable_t1490392188 * get__selectable_16() const { return ____selectable_16; }
	inline Selectable_t1490392188 ** get_address_of__selectable_16() { return &____selectable_16; }
	inline void set__selectable_16(Selectable_t1490392188 * value)
	{
		____selectable_16 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
