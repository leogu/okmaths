﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiImageSetSprite
struct  uGuiImageSetSprite_t842539340  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiImageSetSprite::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.uGuiImageSetSprite::sprite
	FsmObject_t2785794313 * ___sprite_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiImageSetSprite::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Image HutongGames.PlayMaker.Actions.uGuiImageSetSprite::_image
	Image_t2042527209 * ____image_14;
	// UnityEngine.Sprite HutongGames.PlayMaker.Actions.uGuiImageSetSprite::_originalSprite
	Sprite_t309593783 * ____originalSprite_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiImageSetSprite_t842539340, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_sprite_12() { return static_cast<int32_t>(offsetof(uGuiImageSetSprite_t842539340, ___sprite_12)); }
	inline FsmObject_t2785794313 * get_sprite_12() const { return ___sprite_12; }
	inline FsmObject_t2785794313 ** get_address_of_sprite_12() { return &___sprite_12; }
	inline void set_sprite_12(FsmObject_t2785794313 * value)
	{
		___sprite_12 = value;
		Il2CppCodeGenWriteBarrier(&___sprite_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiImageSetSprite_t842539340, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__image_14() { return static_cast<int32_t>(offsetof(uGuiImageSetSprite_t842539340, ____image_14)); }
	inline Image_t2042527209 * get__image_14() const { return ____image_14; }
	inline Image_t2042527209 ** get_address_of__image_14() { return &____image_14; }
	inline void set__image_14(Image_t2042527209 * value)
	{
		____image_14 = value;
		Il2CppCodeGenWriteBarrier(&____image_14, value);
	}

	inline static int32_t get_offset_of__originalSprite_15() { return static_cast<int32_t>(offsetof(uGuiImageSetSprite_t842539340, ____originalSprite_15)); }
	inline Sprite_t309593783 * get__originalSprite_15() const { return ____originalSprite_15; }
	inline Sprite_t309593783 ** get_address_of__originalSprite_15() { return &____originalSprite_15; }
	inline void set__originalSprite_15(Sprite_t309593783 * value)
	{
		____originalSprite_15 = value;
		Il2CppCodeGenWriteBarrier(&____originalSprite_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
