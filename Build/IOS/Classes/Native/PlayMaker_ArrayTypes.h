﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// HutongGames.PlayMaker.INameable
struct INameable_t2561379662;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t4287019078;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// HutongGames.PlayMaker.ActionReport
struct ActionReport_t4101412914;
// HutongGames.PlayMaker.FsmVarOverride
struct FsmVarOverride_t639182869;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t2754669930;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t924276959;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t786753495;
// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t3699592477;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// HutongGames.PlayMaker.IFsmStateAction
struct IFsmStateAction_t2932019468;
// HutongGames.PlayMaker.DelayedEvent
struct DelayedEvent_t1624700828;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmLogEntry
struct FsmLogEntry_t865277298;
// HutongGames.PlayMaker.FsmLog
struct FsmLog_t3672513366;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;

#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_HutongGames_Extensions_TextureExtensions_627548518.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1273009179.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport4101412914.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride639182869.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject3097142863.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmOwnerDefault2023674184.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmAnimationCurve326747561.h"
#include "PlayMaker_HutongGames_PlayMaker_FunctionCall2754669930.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl924276959.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmProperty786753495.h"
#include "PlayMaker_HutongGames_PlayMaker_LayoutOption3699592477.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString2414474701.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmObject2785794313.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar2872592513.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmArray527459893.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEnum2808516103.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool664485696.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector22430450063.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector33996534004.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor118301965.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect19023354.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion878438756.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_HutongGames_PlayMaker_DelayedEvent1624700828.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry865277298.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog3672513366.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmMaterial1421632035.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTexture3372293163.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

#pragma once
// HutongGames.Extensions.TextureExtensions/Point[]
struct PointU5BU5D_t1499831875  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Point_t627548518  m_Items[1];

public:
	inline Point_t627548518  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Point_t627548518 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Point_t627548518  value)
	{
		m_Items[index] = value;
	}
};
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t623924777  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayMakerFSM_t437737208 * m_Items[1];

public:
	inline PlayMakerFSM_t437737208 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PlayMakerFSM_t437737208 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PlayMakerFSM_t437737208 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t4177556671  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmFloat_t937133978 * m_Items[1];

public:
	inline FsmFloat_t937133978 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmFloat_t937133978 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmFloat_t937133978 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.NamedVariable[]
struct NamedVariableU5BU5D_t2156269820  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) NamedVariable_t3026441313 * m_Items[1];

public:
	inline NamedVariable_t3026441313 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline NamedVariable_t3026441313 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, NamedVariable_t3026441313 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.INameable[]
struct INameableU5BU5D_t2958638523  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.INamedVariable[]
struct INamedVariableU5BU5D_t1966600163  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmInt_t1273009179 * m_Items[1];

public:
	inline FsmInt_t1273009179 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmInt_t1273009179 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmInt_t1273009179 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t1091630918  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTransition_t1534990431 * m_Items[1];

public:
	inline FsmTransition_t1534990431 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmTransition_t1534990431 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmTransition_t1534990431 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.ActionReport[]
struct ActionReportU5BU5D_t1184356039  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ActionReport_t4101412914 * m_Items[1];

public:
	inline ActionReport_t4101412914 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ActionReport_t4101412914 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ActionReport_t4101412914 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVarOverride[]
struct FsmVarOverrideU5BU5D_t4083454968  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVarOverride_t639182869 * m_Items[1];

public:
	inline FsmVarOverride_t639182869 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVarOverride_t639182869 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVarOverride_t639182869 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.Fsm[]
struct FsmU5BU5D_t2705153437  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Fsm_t917886356 * m_Items[1];

public:
	inline Fsm_t917886356 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Fsm_t917886356 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Fsm_t917886356 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmGameObject[]
struct FsmGameObjectU5BU5D_t3601875862  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmGameObject_t3097142863 * m_Items[1];

public:
	inline FsmGameObject_t3097142863 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmGameObject_t3097142863 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmGameObject_t3097142863 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmOwnerDefault[]
struct FsmOwnerDefaultU5BU5D_t4036794521  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmOwnerDefault_t2023674184 * m_Items[1];

public:
	inline FsmOwnerDefault_t2023674184 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmOwnerDefault_t2023674184 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmOwnerDefault_t2023674184 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmAnimationCurve[]
struct FsmAnimationCurveU5BU5D_t994773396  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmAnimationCurve_t326747561 * m_Items[1];

public:
	inline FsmAnimationCurve_t326747561 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmAnimationCurve_t326747561 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmAnimationCurve_t326747561 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FunctionCall[]
struct FunctionCallU5BU5D_t3718000047  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FunctionCall_t2754669930 * m_Items[1];

public:
	inline FunctionCall_t2754669930 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FunctionCall_t2754669930 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FunctionCall_t2754669930 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTemplateControl[]
struct FsmTemplateControlU5BU5D_t352068806  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTemplateControl_t924276959 * m_Items[1];

public:
	inline FsmTemplateControl_t924276959 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmTemplateControl_t924276959 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmTemplateControl_t924276959 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEventTarget[]
struct FsmEventTargetU5BU5D_t1214459052  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEventTarget_t172293745 * m_Items[1];

public:
	inline FsmEventTarget_t172293745 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmEventTarget_t172293745 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmEventTarget_t172293745 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmProperty[]
struct FsmPropertyU5BU5D_t3833488110  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmProperty_t786753495 * m_Items[1];

public:
	inline FsmProperty_t786753495 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmProperty_t786753495 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmProperty_t786753495 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.LayoutOption[]
struct LayoutOptionU5BU5D_t896618960  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LayoutOption_t3699592477 * m_Items[1];

public:
	inline LayoutOption_t3699592477 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline LayoutOption_t3699592477 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, LayoutOption_t3699592477 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmString_t2414474701 * m_Items[1];

public:
	inline FsmString_t2414474701 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmString_t2414474701 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmString_t2414474701 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmObject[]
struct FsmObjectU5BU5D_t2051300532  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmObject_t2785794313 * m_Items[1];

public:
	inline FsmObject_t2785794313 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmObject_t2785794313 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmObject_t2785794313 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t16885852  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVar_t2872592513 * m_Items[1];

public:
	inline FsmVar_t2872592513 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVar_t2872592513 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVar_t2872592513 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmArray[]
struct FsmArrayU5BU5D_t2711026008  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmArray_t527459893 * m_Items[1];

public:
	inline FsmArray_t527459893 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmArray_t527459893 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmArray_t527459893 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEnum[]
struct FsmEnumU5BU5D_t2440162814  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEnum_t2808516103 * m_Items[1];

public:
	inline FsmEnum_t2808516103 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmEnum_t2808516103 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmEnum_t2808516103 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3830815681  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmBool_t664485696 * m_Items[1];

public:
	inline FsmBool_t664485696 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmBool_t664485696 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmBool_t664485696 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t381696854  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVector2_t2430450063 * m_Items[1];

public:
	inline FsmVector2_t2430450063 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVector2_t2430450063 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVector2_t2430450063 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t643261629  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVector3_t3996534004 * m_Items[1];

public:
	inline FsmVector3_t3996534004 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmVector3_t3996534004 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmVector3_t3996534004 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t4100123680  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmColor_t118301965 * m_Items[1];

public:
	inline FsmColor_t118301965 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmColor_t118301965 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmColor_t118301965 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmRect[]
struct FsmRectU5BU5D_t1455296735  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmRect_t19023354 * m_Items[1];

public:
	inline FsmRect_t19023354 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmRect_t19023354 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmRect_t19023354 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t3489263757  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmQuaternion_t878438756 * m_Items[1];

public:
	inline FsmQuaternion_t878438756 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmQuaternion_t878438756 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmQuaternion_t878438756 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t835534274  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1497896004  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmStateAction_t2862378169 * m_Items[1];

public:
	inline FsmStateAction_t2862378169 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmStateAction_t2862378169 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmStateAction_t2862378169 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.IFsmStateAction[]
struct IFsmStateActionU5BU5D_t3881905477  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.DelayedEvent[]
struct DelayedEventU5BU5D_t3203304053  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DelayedEvent_t1624700828 * m_Items[1];

public:
	inline DelayedEvent_t1624700828 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DelayedEvent_t1624700828 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DelayedEvent_t1624700828 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t1586422282  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmState_t1643911659 * m_Items[1];

public:
	inline FsmState_t1643911659 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmState_t1643911659 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmState_t1643911659 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t287863993  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEvent_t1258573736 * m_Items[1];

public:
	inline FsmEvent_t1258573736 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmEvent_t1258573736 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmEvent_t1258573736 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmLogEntry[]
struct FsmLogEntryU5BU5D_t2612624519  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmLogEntry_t865277298 * m_Items[1];

public:
	inline FsmLogEntry_t865277298 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmLogEntry_t865277298 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmLogEntry_t865277298 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmLog[]
struct FsmLogU5BU5D_t4196519571  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmLog_t3672513366 * m_Items[1];

public:
	inline FsmLog_t3672513366 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmLog_t3672513366 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmLog_t3672513366 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1567627762  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmMaterial_t1421632035 * m_Items[1];

public:
	inline FsmMaterial_t1421632035 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmMaterial_t1421632035 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmMaterial_t1421632035 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTexture[]
struct FsmTextureU5BU5D_t3720629962  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTexture_t3372293163 * m_Items[1];

public:
	inline FsmTexture_t3372293163 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FsmTexture_t3372293163 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FsmTexture_t3372293163 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.VariableType[]
struct VariableTypeU5BU5D_t4294461311  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline int32_t* GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
