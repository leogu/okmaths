﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit
struct  uGuiInputFieldGetCharacterLimit_t402521518  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::characterLimit
	FsmInt_t1273009179 * ___characterLimit_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::hasNoLimitEvent
	FsmEvent_t1258573736 * ___hasNoLimitEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::isLimitedEvent
	FsmEvent_t1258573736 * ___isLimitedEvent_14;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetCharacterLimit::_inputField
	InputField_t1631627530 * ____inputField_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCharacterLimit_t402521518, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_characterLimit_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCharacterLimit_t402521518, ___characterLimit_12)); }
	inline FsmInt_t1273009179 * get_characterLimit_12() const { return ___characterLimit_12; }
	inline FsmInt_t1273009179 ** get_address_of_characterLimit_12() { return &___characterLimit_12; }
	inline void set_characterLimit_12(FsmInt_t1273009179 * value)
	{
		___characterLimit_12 = value;
		Il2CppCodeGenWriteBarrier(&___characterLimit_12, value);
	}

	inline static int32_t get_offset_of_hasNoLimitEvent_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCharacterLimit_t402521518, ___hasNoLimitEvent_13)); }
	inline FsmEvent_t1258573736 * get_hasNoLimitEvent_13() const { return ___hasNoLimitEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_hasNoLimitEvent_13() { return &___hasNoLimitEvent_13; }
	inline void set_hasNoLimitEvent_13(FsmEvent_t1258573736 * value)
	{
		___hasNoLimitEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___hasNoLimitEvent_13, value);
	}

	inline static int32_t get_offset_of_isLimitedEvent_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCharacterLimit_t402521518, ___isLimitedEvent_14)); }
	inline FsmEvent_t1258573736 * get_isLimitedEvent_14() const { return ___isLimitedEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isLimitedEvent_14() { return &___isLimitedEvent_14; }
	inline void set_isLimitedEvent_14(FsmEvent_t1258573736 * value)
	{
		___isLimitedEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isLimitedEvent_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCharacterLimit_t402521518, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of__inputField_16() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetCharacterLimit_t402521518, ____inputField_16)); }
	inline InputField_t1631627530 * get__inputField_16() const { return ____inputField_16; }
	inline InputField_t1631627530 ** get_address_of__inputField_16() { return &____inputField_16; }
	inline void set__inputField_16(InputField_t1631627530 * value)
	{
		____inputField_16 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
