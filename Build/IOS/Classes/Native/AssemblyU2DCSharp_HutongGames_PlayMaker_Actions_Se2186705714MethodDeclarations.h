﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetVector2XY
struct SetVector2XY_t2186705714;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::.ctor()
extern "C"  void SetVector2XY__ctor_m1690221304 (SetVector2XY_t2186705714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::Reset()
extern "C"  void SetVector2XY_Reset_m2758742847 (SetVector2XY_t2186705714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::OnEnter()
extern "C"  void SetVector2XY_OnEnter_m4174067887 (SetVector2XY_t2186705714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::OnUpdate()
extern "C"  void SetVector2XY_OnUpdate_m4216621134 (SetVector2XY_t2186705714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetVector2XY::DoSetVector2XYZ()
extern "C"  void SetVector2XY_DoSetVector2XYZ_m3211287799 (SetVector2XY_t2186705714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
