﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGravity2d
struct SetGravity2d_t2684819180;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::.ctor()
extern "C"  void SetGravity2d__ctor_m640674016 (SetGravity2d_t2684819180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::Reset()
extern "C"  void SetGravity2d_Reset_m821923897 (SetGravity2d_t2684819180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::OnEnter()
extern "C"  void SetGravity2d_OnEnter_m277733089 (SetGravity2d_t2684819180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::OnUpdate()
extern "C"  void SetGravity2d_OnUpdate_m2703578550 (SetGravity2d_t2684819180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGravity2d::DoSetGravity()
extern "C"  void SetGravity2d_DoSetGravity_m2826145855 (SetGravity2d_t2684819180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
