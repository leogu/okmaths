﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t4101825636;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// FsmTemplate
struct FsmTemplate_t1285897084;
// UnityEngine.GUITexture
struct GUITexture_t1909122990;
// UnityEngine.GUIText
struct GUIText_t2411476300;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.BitStream
struct BitStream_t1979465639;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t1586422282;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t287863993;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t1091630918;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GUITexture1909122990.h"
#include "UnityEngine_UnityEngine_GUIText2411476300.h"
#include "PlayMaker_FsmTemplate1285897084.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection45590380.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1607866742.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059.h"
#include "UnityEngine_UnityEngine_BitStream1979465639.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2097711603.h"

// System.String PlayMakerFSM::get_VersionNotes()
extern "C"  String_t* PlayMakerFSM_get_VersionNotes_m805927392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_VersionLabel()
extern "C"  String_t* PlayMakerFSM_get_VersionLabel_m1627028815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern "C"  List_1_t4101825636 * PlayMakerFSM_get_FsmList_m5507309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM PlayMakerFSM::FindFsmOnGameObject(UnityEngine.GameObject,System.String)
extern "C"  PlayMakerFSM_t437737208 * PlayMakerFSM_FindFsmOnGameObject_m2273894409 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FsmTemplate PlayMakerFSM::get_FsmTemplate()
extern "C"  FsmTemplate_t1285897084 * PlayMakerFSM_get_FsmTemplate_m3817745221 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C"  GUITexture_t1909122990 * PlayMakerFSM_get_GuiTexture_m2496936866 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_GuiTexture(UnityEngine.GUITexture)
extern "C"  void PlayMakerFSM_set_GuiTexture_m2916730005 (PlayMakerFSM_t437737208 * __this, GUITexture_t1909122990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C"  GUIText_t2411476300 * PlayMakerFSM_get_GuiText_m3709088712 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_GuiText(UnityEngine.GUIText)
extern "C"  void PlayMakerFSM_set_GuiText_m190415149 (PlayMakerFSM_t437737208 * __this, GUIText_t2411476300 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerFSM::get_DrawGizmos()
extern "C"  bool PlayMakerFSM_get_DrawGizmos_m1038294635 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_DrawGizmos(System.Boolean)
extern "C"  void PlayMakerFSM_set_DrawGizmos_m2543964976 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Reset()
extern "C"  void PlayMakerFSM_Reset_m222573198 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Awake()
extern "C"  void PlayMakerFSM_Awake_m2941374002 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Preprocess()
extern "C"  void PlayMakerFSM_Preprocess_m1370866543 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Init()
extern "C"  void PlayMakerFSM_Init_m2906566981 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::InitTemplate()
extern "C"  void PlayMakerFSM_InitTemplate_m3833177745 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::InitFsm()
extern "C"  void PlayMakerFSM_InitFsm_m3268071385 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponents()
extern "C"  void PlayMakerFSM_AddEventHandlerComponents_m3901157882 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SetFsmTemplate(FsmTemplate)
extern "C"  void PlayMakerFSM_SetFsmTemplate_m648066017 (PlayMakerFSM_t437737208 * __this, FsmTemplate_t1285897084 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Start()
extern "C"  void PlayMakerFSM_Start_m3186900003 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnEnable()
extern "C"  void PlayMakerFSM_OnEnable_m739653507 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Update()
extern "C"  void PlayMakerFSM_Update_m2935406016 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::LateUpdate()
extern "C"  void PlayMakerFSM_LateUpdate_m1309297244 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayMakerFSM::DoCoroutine(System.Collections.IEnumerator)
extern "C"  Il2CppObject * PlayMakerFSM_DoCoroutine_m2070243337 (PlayMakerFSM_t437737208 * __this, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDisable()
extern "C"  void PlayMakerFSM_OnDisable_m1788428118 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDestroy()
extern "C"  void PlayMakerFSM_OnDestroy_m924941156 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnApplicationQuit()
extern "C"  void PlayMakerFSM_OnApplicationQuit_m1000198585 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDrawGizmos()
extern "C"  void PlayMakerFSM_OnDrawGizmos_m1328164963 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SetState(System.String)
extern "C"  void PlayMakerFSM_SetState_m3531642678 (PlayMakerFSM_t437737208 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::ChangeState(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_ChangeState_m4048370620 (PlayMakerFSM_t437737208 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::ChangeState(System.String)
extern "C"  void PlayMakerFSM_ChangeState_m3369177710 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SendEvent(System.String)
extern "C"  void PlayMakerFSM_SendEvent_m2107076673 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SendRemoteFsmEvent(System.String)
extern "C"  void PlayMakerFSM_SendRemoteFsmEvent_m2943015349 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::SendRemoteFsmEventWithData(System.String,System.String)
extern "C"  void PlayMakerFSM_SendRemoteFsmEventWithData_m2864874037 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, String_t* ___eventData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::BroadcastEvent(System.String)
extern "C"  void PlayMakerFSM_BroadcastEvent_m1710776222 (Il2CppObject * __this /* static, unused */, String_t* ___fsmEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_BroadcastEvent_m583362320 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnBecameVisible()
extern "C"  void PlayMakerFSM_OnBecameVisible_m2356140445 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnBecameInvisible()
extern "C"  void PlayMakerFSM_OnBecameInvisible_m51942020 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnLevelWasLoaded()
extern "C"  void PlayMakerFSM_OnLevelWasLoaded_m434828254 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern "C"  void PlayMakerFSM_OnPlayerConnected_m1919487554 (PlayMakerFSM_t437737208 * __this, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnServerInitialized()
extern "C"  void PlayMakerFSM_OnServerInitialized_m897882395 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnConnectedToServer()
extern "C"  void PlayMakerFSM_OnConnectedToServer_m1688810999 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern "C"  void PlayMakerFSM_OnPlayerDisconnected_m1152869810 (PlayMakerFSM_t437737208 * __this, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern "C"  void PlayMakerFSM_OnDisconnectedFromServer_m3587986053 (PlayMakerFSM_t437737208 * __this, int32_t ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern "C"  void PlayMakerFSM_OnFailedToConnect_m3759082883 (PlayMakerFSM_t437737208 * __this, int32_t ___error0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern "C"  void PlayMakerFSM_OnNetworkInstantiate_m1787303400 (PlayMakerFSM_t437737208 * __this, NetworkMessageInfo_t614064059  ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern "C"  void PlayMakerFSM_OnSerializeNetworkView_m297962887 (PlayMakerFSM_t437737208 * __this, BitStream_t1979465639 * ___stream0, NetworkMessageInfo_t614064059  ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::NetworkSyncVariables(UnityEngine.BitStream,HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerFSM_NetworkSyncVariables_m1890220694 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___stream0, FsmVariables_t630687169 * ___variables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern "C"  void PlayMakerFSM_OnMasterServerEvent_m4091791057 (PlayMakerFSM_t437737208 * __this, int32_t ___masterServerEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C"  Fsm_t917886356 * PlayMakerFSM_get_Fsm_m3359411247 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_FsmName()
extern "C"  String_t* PlayMakerFSM_get_FsmName_m2713674740 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_FsmName(System.String)
extern "C"  void PlayMakerFSM_set_FsmName_m3291949591 (PlayMakerFSM_t437737208 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_FsmDescription()
extern "C"  String_t* PlayMakerFSM_get_FsmDescription_m3515647765 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::set_FsmDescription(System.String)
extern "C"  void PlayMakerFSM_set_FsmDescription_m783281822 (PlayMakerFSM_t437737208 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerFSM::get_Active()
extern "C"  bool PlayMakerFSM_get_Active_m2477371780 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_ActiveStateName()
extern "C"  String_t* PlayMakerFSM_get_ActiveStateName_m1650919959 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState[] PlayMakerFSM::get_FsmStates()
extern "C"  FsmStateU5BU5D_t1586422282* PlayMakerFSM_get_FsmStates_m1657585614 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent[] PlayMakerFSM::get_FsmEvents()
extern "C"  FsmEventU5BU5D_t287863993* PlayMakerFSM_get_FsmEvents_m601072828 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] PlayMakerFSM::get_FsmGlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t1091630918* PlayMakerFSM_get_FsmGlobalTransitions_m1961813325 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables PlayMakerFSM::get_FsmVariables()
extern "C"  FsmVariables_t630687169 * PlayMakerFSM_get_FsmVariables_m2623807583 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerFSM::get_UsesTemplate()
extern "C"  bool PlayMakerFSM_get_UsesTemplate_m3598466416 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnBeforeSerialize()
extern "C"  void PlayMakerFSM_OnBeforeSerialize_m2468983921 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::OnAfterDeserialize()
extern "C"  void PlayMakerFSM_OnAfterDeserialize_m3821310365 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::.ctor()
extern "C"  void PlayMakerFSM__ctor_m1051306911 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::.cctor()
extern "C"  void PlayMakerFSM__cctor_m2888832784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
