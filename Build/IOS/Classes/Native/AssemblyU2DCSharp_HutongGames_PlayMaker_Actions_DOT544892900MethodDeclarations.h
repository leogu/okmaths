﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateVector2
struct DOTweenAnimateVector2_t544892900;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::.ctor()
extern "C"  void DOTweenAnimateVector2__ctor_m2593721700 (DOTweenAnimateVector2_t544892900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::Reset()
extern "C"  void DOTweenAnimateVector2_Reset_m587179833 (DOTweenAnimateVector2_t544892900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::OnEnter()
extern "C"  void DOTweenAnimateVector2_OnEnter_m4075731761 (DOTweenAnimateVector2_t544892900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::<OnEnter>m__14()
extern "C"  Vector2_t2243707579  DOTweenAnimateVector2_U3COnEnterU3Em__14_m1818520400 (DOTweenAnimateVector2_t544892900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::<OnEnter>m__15(UnityEngine.Vector2)
extern "C"  void DOTweenAnimateVector2_U3COnEnterU3Em__15_m706096830 (DOTweenAnimateVector2_t544892900 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::<OnEnter>m__16()
extern "C"  void DOTweenAnimateVector2_U3COnEnterU3Em__16_m2886332161 (DOTweenAnimateVector2_t544892900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateVector2::<OnEnter>m__17()
extern "C"  void DOTweenAnimateVector2_U3COnEnterU3Em__17_m2886332128 (DOTweenAnimateVector2_t544892900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
