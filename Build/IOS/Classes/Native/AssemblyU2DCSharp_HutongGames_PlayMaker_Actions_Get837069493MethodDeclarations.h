﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetJointBreak2dInfo
struct GetJointBreak2dInfo_t837069493;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::.ctor()
extern "C"  void GetJointBreak2dInfo__ctor_m4075337409 (GetJointBreak2dInfo_t837069493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::Reset()
extern "C"  void GetJointBreak2dInfo_Reset_m2022400342 (GetJointBreak2dInfo_t837069493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::StoreInfo()
extern "C"  void GetJointBreak2dInfo_StoreInfo_m2671939188 (GetJointBreak2dInfo_t837069493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetJointBreak2dInfo::OnEnter()
extern "C"  void GetJointBreak2dInfo_OnEnter_m2984339988 (GetJointBreak2dInfo_t837069493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
