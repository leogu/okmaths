﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TransformDirection
struct TransformDirection_t111371447;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TransformDirection::.ctor()
extern "C"  void TransformDirection__ctor_m3072317927 (TransformDirection_t111371447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::Reset()
extern "C"  void TransformDirection_Reset_m1998105728 (TransformDirection_t111371447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::OnEnter()
extern "C"  void TransformDirection_OnEnter_m3620140870 (TransformDirection_t111371447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::OnUpdate()
extern "C"  void TransformDirection_OnUpdate_m2177794759 (TransformDirection_t111371447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformDirection::DoTransformDirection()
extern "C"  void TransformDirection_DoTransformDirection_m3754428033 (TransformDirection_t111371447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
