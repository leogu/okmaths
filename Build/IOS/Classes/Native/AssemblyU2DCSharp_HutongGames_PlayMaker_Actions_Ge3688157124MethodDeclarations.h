﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorSpeed
struct GetAnimatorSpeed_t3688157124;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::.ctor()
extern "C"  void GetAnimatorSpeed__ctor_m432733060 (GetAnimatorSpeed_t3688157124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::Reset()
extern "C"  void GetAnimatorSpeed_Reset_m2156358445 (GetAnimatorSpeed_t3688157124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::OnEnter()
extern "C"  void GetAnimatorSpeed_OnEnter_m2325754493 (GetAnimatorSpeed_t3688157124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::OnActionUpdate()
extern "C"  void GetAnimatorSpeed_OnActionUpdate_m3526907000 (GetAnimatorSpeed_t3688157124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorSpeed::GetPlaybackSpeed()
extern "C"  void GetAnimatorSpeed_GetPlaybackSpeed_m3355566050 (GetAnimatorSpeed_t3688157124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
