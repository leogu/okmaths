﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetName
struct SetName_t1651919227;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetName::.ctor()
extern "C"  void SetName__ctor_m1636195175 (SetName_t1651919227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetName::Reset()
extern "C"  void SetName_Reset_m614426492 (SetName_t1651919227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetName::OnEnter()
extern "C"  void SetName_OnEnter_m1151358634 (SetName_t1651919227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetName::DoSetLayer()
extern "C"  void SetName_DoSetLayer_m4020328757 (SetName_t1651919227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
