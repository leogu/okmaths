﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SplitTextToArrayList
struct SplitTextToArrayList_t355753733;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SplitTextToArrayList::.ctor()
extern "C"  void SplitTextToArrayList__ctor_m3888607985 (SplitTextToArrayList_t355753733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SplitTextToArrayList::Reset()
extern "C"  void SplitTextToArrayList_Reset_m1558417266 (SplitTextToArrayList_t355753733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SplitTextToArrayList::OnEnter()
extern "C"  void SplitTextToArrayList_OnEnter_m2481155648 (SplitTextToArrayList_t355753733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SplitTextToArrayList::splitText()
extern "C"  void SplitTextToArrayList_splitText_m2998747206 (SplitTextToArrayList_t355753733 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
