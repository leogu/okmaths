﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "PlayMaker_HutongGames_PlayMaker_OwnerDefaultOption3848444473.h"
#include "AssemblyU2DCSharp_PlayMakerUGuiComponentProxy_Acti2766562384.h"
#include "AssemblyU2DCSharp_PlayMakerUGuiComponentProxy_FsmV2450860713.h"
#include "AssemblyU2DCSharp_PlayMakerUGuiComponentProxy_FsmE2719220363.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiComponentProxy
struct  PlayMakerUGuiComponentProxy_t3753606439  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerUGuiComponentProxy::debug
	bool ___debug_2;
	// System.String PlayMakerUGuiComponentProxy::error
	String_t* ___error_3;
	// HutongGames.PlayMaker.OwnerDefaultOption PlayMakerUGuiComponentProxy::UiTargetOption
	int32_t ___UiTargetOption_4;
	// UnityEngine.GameObject PlayMakerUGuiComponentProxy::UiTarget
	GameObject_t1756533147 * ___UiTarget_5;
	// PlayMakerUGuiComponentProxy/ActionType PlayMakerUGuiComponentProxy::action
	int32_t ___action_6;
	// PlayMakerUGuiComponentProxy/FsmVariableSetup PlayMakerUGuiComponentProxy::fsmVariableSetup
	FsmVariableSetup_t2450860713  ___fsmVariableSetup_7;
	// HutongGames.PlayMaker.FsmFloat PlayMakerUGuiComponentProxy::fsmFloatTarget
	FsmFloat_t937133978 * ___fsmFloatTarget_8;
	// HutongGames.PlayMaker.FsmBool PlayMakerUGuiComponentProxy::fsmBoolTarget
	FsmBool_t664485696 * ___fsmBoolTarget_9;
	// HutongGames.PlayMaker.FsmVector2 PlayMakerUGuiComponentProxy::fsmVector2Target
	FsmVector2_t2430450063 * ___fsmVector2Target_10;
	// HutongGames.PlayMaker.FsmString PlayMakerUGuiComponentProxy::fsmStringTarget
	FsmString_t2414474701 * ___fsmStringTarget_11;
	// HutongGames.PlayMaker.FsmInt PlayMakerUGuiComponentProxy::fsmIntTarget
	FsmInt_t1273009179 * ___fsmIntTarget_12;
	// PlayMakerUGuiComponentProxy/FsmEventSetup PlayMakerUGuiComponentProxy::fsmEventSetup
	FsmEventSetup_t2719220363  ___fsmEventSetup_13;
	// HutongGames.PlayMaker.FsmEventTarget PlayMakerUGuiComponentProxy::fsmEventTarget
	FsmEventTarget_t172293745 * ___fsmEventTarget_14;
	// System.Boolean PlayMakerUGuiComponentProxy::WatchInputField
	bool ___WatchInputField_15;
	// UnityEngine.UI.InputField PlayMakerUGuiComponentProxy::inputField
	InputField_t1631627530 * ___inputField_16;
	// System.String PlayMakerUGuiComponentProxy::lastInputFieldValue
	String_t* ___lastInputFieldValue_17;

public:
	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_error_3() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___error_3)); }
	inline String_t* get_error_3() const { return ___error_3; }
	inline String_t** get_address_of_error_3() { return &___error_3; }
	inline void set_error_3(String_t* value)
	{
		___error_3 = value;
		Il2CppCodeGenWriteBarrier(&___error_3, value);
	}

	inline static int32_t get_offset_of_UiTargetOption_4() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___UiTargetOption_4)); }
	inline int32_t get_UiTargetOption_4() const { return ___UiTargetOption_4; }
	inline int32_t* get_address_of_UiTargetOption_4() { return &___UiTargetOption_4; }
	inline void set_UiTargetOption_4(int32_t value)
	{
		___UiTargetOption_4 = value;
	}

	inline static int32_t get_offset_of_UiTarget_5() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___UiTarget_5)); }
	inline GameObject_t1756533147 * get_UiTarget_5() const { return ___UiTarget_5; }
	inline GameObject_t1756533147 ** get_address_of_UiTarget_5() { return &___UiTarget_5; }
	inline void set_UiTarget_5(GameObject_t1756533147 * value)
	{
		___UiTarget_5 = value;
		Il2CppCodeGenWriteBarrier(&___UiTarget_5, value);
	}

	inline static int32_t get_offset_of_action_6() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___action_6)); }
	inline int32_t get_action_6() const { return ___action_6; }
	inline int32_t* get_address_of_action_6() { return &___action_6; }
	inline void set_action_6(int32_t value)
	{
		___action_6 = value;
	}

	inline static int32_t get_offset_of_fsmVariableSetup_7() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmVariableSetup_7)); }
	inline FsmVariableSetup_t2450860713  get_fsmVariableSetup_7() const { return ___fsmVariableSetup_7; }
	inline FsmVariableSetup_t2450860713 * get_address_of_fsmVariableSetup_7() { return &___fsmVariableSetup_7; }
	inline void set_fsmVariableSetup_7(FsmVariableSetup_t2450860713  value)
	{
		___fsmVariableSetup_7 = value;
	}

	inline static int32_t get_offset_of_fsmFloatTarget_8() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmFloatTarget_8)); }
	inline FsmFloat_t937133978 * get_fsmFloatTarget_8() const { return ___fsmFloatTarget_8; }
	inline FsmFloat_t937133978 ** get_address_of_fsmFloatTarget_8() { return &___fsmFloatTarget_8; }
	inline void set_fsmFloatTarget_8(FsmFloat_t937133978 * value)
	{
		___fsmFloatTarget_8 = value;
		Il2CppCodeGenWriteBarrier(&___fsmFloatTarget_8, value);
	}

	inline static int32_t get_offset_of_fsmBoolTarget_9() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmBoolTarget_9)); }
	inline FsmBool_t664485696 * get_fsmBoolTarget_9() const { return ___fsmBoolTarget_9; }
	inline FsmBool_t664485696 ** get_address_of_fsmBoolTarget_9() { return &___fsmBoolTarget_9; }
	inline void set_fsmBoolTarget_9(FsmBool_t664485696 * value)
	{
		___fsmBoolTarget_9 = value;
		Il2CppCodeGenWriteBarrier(&___fsmBoolTarget_9, value);
	}

	inline static int32_t get_offset_of_fsmVector2Target_10() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmVector2Target_10)); }
	inline FsmVector2_t2430450063 * get_fsmVector2Target_10() const { return ___fsmVector2Target_10; }
	inline FsmVector2_t2430450063 ** get_address_of_fsmVector2Target_10() { return &___fsmVector2Target_10; }
	inline void set_fsmVector2Target_10(FsmVector2_t2430450063 * value)
	{
		___fsmVector2Target_10 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVector2Target_10, value);
	}

	inline static int32_t get_offset_of_fsmStringTarget_11() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmStringTarget_11)); }
	inline FsmString_t2414474701 * get_fsmStringTarget_11() const { return ___fsmStringTarget_11; }
	inline FsmString_t2414474701 ** get_address_of_fsmStringTarget_11() { return &___fsmStringTarget_11; }
	inline void set_fsmStringTarget_11(FsmString_t2414474701 * value)
	{
		___fsmStringTarget_11 = value;
		Il2CppCodeGenWriteBarrier(&___fsmStringTarget_11, value);
	}

	inline static int32_t get_offset_of_fsmIntTarget_12() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmIntTarget_12)); }
	inline FsmInt_t1273009179 * get_fsmIntTarget_12() const { return ___fsmIntTarget_12; }
	inline FsmInt_t1273009179 ** get_address_of_fsmIntTarget_12() { return &___fsmIntTarget_12; }
	inline void set_fsmIntTarget_12(FsmInt_t1273009179 * value)
	{
		___fsmIntTarget_12 = value;
		Il2CppCodeGenWriteBarrier(&___fsmIntTarget_12, value);
	}

	inline static int32_t get_offset_of_fsmEventSetup_13() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmEventSetup_13)); }
	inline FsmEventSetup_t2719220363  get_fsmEventSetup_13() const { return ___fsmEventSetup_13; }
	inline FsmEventSetup_t2719220363 * get_address_of_fsmEventSetup_13() { return &___fsmEventSetup_13; }
	inline void set_fsmEventSetup_13(FsmEventSetup_t2719220363  value)
	{
		___fsmEventSetup_13 = value;
	}

	inline static int32_t get_offset_of_fsmEventTarget_14() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___fsmEventTarget_14)); }
	inline FsmEventTarget_t172293745 * get_fsmEventTarget_14() const { return ___fsmEventTarget_14; }
	inline FsmEventTarget_t172293745 ** get_address_of_fsmEventTarget_14() { return &___fsmEventTarget_14; }
	inline void set_fsmEventTarget_14(FsmEventTarget_t172293745 * value)
	{
		___fsmEventTarget_14 = value;
		Il2CppCodeGenWriteBarrier(&___fsmEventTarget_14, value);
	}

	inline static int32_t get_offset_of_WatchInputField_15() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___WatchInputField_15)); }
	inline bool get_WatchInputField_15() const { return ___WatchInputField_15; }
	inline bool* get_address_of_WatchInputField_15() { return &___WatchInputField_15; }
	inline void set_WatchInputField_15(bool value)
	{
		___WatchInputField_15 = value;
	}

	inline static int32_t get_offset_of_inputField_16() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___inputField_16)); }
	inline InputField_t1631627530 * get_inputField_16() const { return ___inputField_16; }
	inline InputField_t1631627530 ** get_address_of_inputField_16() { return &___inputField_16; }
	inline void set_inputField_16(InputField_t1631627530 * value)
	{
		___inputField_16 = value;
		Il2CppCodeGenWriteBarrier(&___inputField_16, value);
	}

	inline static int32_t get_offset_of_lastInputFieldValue_17() { return static_cast<int32_t>(offsetof(PlayMakerUGuiComponentProxy_t3753606439, ___lastInputFieldValue_17)); }
	inline String_t* get_lastInputFieldValue_17() const { return ___lastInputFieldValue_17; }
	inline String_t** get_address_of_lastInputFieldValue_17() { return &___lastInputFieldValue_17; }
	inline void set_lastInputFieldValue_17(String_t* value)
	{
		___lastInputFieldValue_17 = value;
		Il2CppCodeGenWriteBarrier(&___lastInputFieldValue_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
