﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionReport/<>c__DisplayClass2
struct U3CU3Ec__DisplayClass2_t3039022016;
// HutongGames.PlayMaker.ActionReport
struct ActionReport_t4101412914;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport4101412914.h"

// System.Void HutongGames.PlayMaker.ActionReport/<>c__DisplayClass2::.ctor()
extern "C"  void U3CU3Ec__DisplayClass2__ctor_m731932289 (U3CU3Ec__DisplayClass2_t3039022016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionReport/<>c__DisplayClass2::<Remove>b__1(HutongGames.PlayMaker.ActionReport)
extern "C"  bool U3CU3Ec__DisplayClass2_U3CRemoveU3Eb__1_m3094630184 (U3CU3Ec__DisplayClass2_t3039022016 * __this, ActionReport_t4101412914 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
