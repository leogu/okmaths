﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t3395645806;

#include "codegen/il2cpp-codegen.h"

// System.Void DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m1183603446 (U3CU3Ec__DisplayClass0_0_t3395645806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::<DOSetFloat>b__0()
extern "C"  float U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__0_m1497172051 (U3CU3Ec__DisplayClass0_0_t3395645806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions50/<>c__DisplayClass0_0::<DOSetFloat>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass0_0_U3CDOSetFloatU3Eb__1_m4215540369 (U3CU3Ec__DisplayClass0_0_t3395645806 * __this, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
