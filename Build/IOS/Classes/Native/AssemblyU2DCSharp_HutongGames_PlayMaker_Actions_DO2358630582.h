﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition
struct  DOTweenRigidbodyLookAtPosition_t2358630582  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::position
	FsmVector3_t3996534004 * ___position_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::setRelative
	FsmBool_t664485696 * ___setRelative_13;
	// DG.Tweening.AxisConstraint HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::axisConstraint
	int32_t ___axisConstraint_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::up
	FsmVector3_t3996534004 * ___up_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::duration
	FsmFloat_t937133978 * ___duration_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::startDelay
	FsmFloat_t937133978 * ___startDelay_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::playInReverse
	FsmBool_t664485696 * ___playInReverse_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::setReverseRelative
	FsmBool_t664485696 * ___setReverseRelative_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::startEvent
	FsmEvent_t1258573736 * ___startEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_23;
	// System.String HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::tweenIdDescription
	String_t* ___tweenIdDescription_24;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::tweenIdType
	int32_t ___tweenIdType_25;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::stringAsId
	FsmString_t2414474701 * ___stringAsId_26;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::tagAsId
	FsmString_t2414474701 * ___tagAsId_27;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::selectedEase
	int32_t ___selectedEase_28;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::easeType
	int32_t ___easeType_29;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_30;
	// System.String HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_31;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::loops
	FsmInt_t1273009179 * ___loops_32;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::loopType
	int32_t ___loopType_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::recyclable
	FsmBool_t664485696 * ___recyclable_35;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::updateType
	int32_t ___updateType_36;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_37;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::debugThis
	FsmBool_t664485696 * ___debugThis_38;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenRigidbodyLookAtPosition::tweener
	Tweener_t760404022 * ___tweener_39;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_position_12() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___position_12)); }
	inline FsmVector3_t3996534004 * get_position_12() const { return ___position_12; }
	inline FsmVector3_t3996534004 ** get_address_of_position_12() { return &___position_12; }
	inline void set_position_12(FsmVector3_t3996534004 * value)
	{
		___position_12 = value;
		Il2CppCodeGenWriteBarrier(&___position_12, value);
	}

	inline static int32_t get_offset_of_setRelative_13() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___setRelative_13)); }
	inline FsmBool_t664485696 * get_setRelative_13() const { return ___setRelative_13; }
	inline FsmBool_t664485696 ** get_address_of_setRelative_13() { return &___setRelative_13; }
	inline void set_setRelative_13(FsmBool_t664485696 * value)
	{
		___setRelative_13 = value;
		Il2CppCodeGenWriteBarrier(&___setRelative_13, value);
	}

	inline static int32_t get_offset_of_axisConstraint_14() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___axisConstraint_14)); }
	inline int32_t get_axisConstraint_14() const { return ___axisConstraint_14; }
	inline int32_t* get_address_of_axisConstraint_14() { return &___axisConstraint_14; }
	inline void set_axisConstraint_14(int32_t value)
	{
		___axisConstraint_14 = value;
	}

	inline static int32_t get_offset_of_up_15() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___up_15)); }
	inline FsmVector3_t3996534004 * get_up_15() const { return ___up_15; }
	inline FsmVector3_t3996534004 ** get_address_of_up_15() { return &___up_15; }
	inline void set_up_15(FsmVector3_t3996534004 * value)
	{
		___up_15 = value;
		Il2CppCodeGenWriteBarrier(&___up_15, value);
	}

	inline static int32_t get_offset_of_duration_16() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___duration_16)); }
	inline FsmFloat_t937133978 * get_duration_16() const { return ___duration_16; }
	inline FsmFloat_t937133978 ** get_address_of_duration_16() { return &___duration_16; }
	inline void set_duration_16(FsmFloat_t937133978 * value)
	{
		___duration_16 = value;
		Il2CppCodeGenWriteBarrier(&___duration_16, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_17() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___setSpeedBased_17)); }
	inline FsmBool_t664485696 * get_setSpeedBased_17() const { return ___setSpeedBased_17; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_17() { return &___setSpeedBased_17; }
	inline void set_setSpeedBased_17(FsmBool_t664485696 * value)
	{
		___setSpeedBased_17 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_17, value);
	}

	inline static int32_t get_offset_of_startDelay_18() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___startDelay_18)); }
	inline FsmFloat_t937133978 * get_startDelay_18() const { return ___startDelay_18; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_18() { return &___startDelay_18; }
	inline void set_startDelay_18(FsmFloat_t937133978 * value)
	{
		___startDelay_18 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_18, value);
	}

	inline static int32_t get_offset_of_playInReverse_19() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___playInReverse_19)); }
	inline FsmBool_t664485696 * get_playInReverse_19() const { return ___playInReverse_19; }
	inline FsmBool_t664485696 ** get_address_of_playInReverse_19() { return &___playInReverse_19; }
	inline void set_playInReverse_19(FsmBool_t664485696 * value)
	{
		___playInReverse_19 = value;
		Il2CppCodeGenWriteBarrier(&___playInReverse_19, value);
	}

	inline static int32_t get_offset_of_setReverseRelative_20() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___setReverseRelative_20)); }
	inline FsmBool_t664485696 * get_setReverseRelative_20() const { return ___setReverseRelative_20; }
	inline FsmBool_t664485696 ** get_address_of_setReverseRelative_20() { return &___setReverseRelative_20; }
	inline void set_setReverseRelative_20(FsmBool_t664485696 * value)
	{
		___setReverseRelative_20 = value;
		Il2CppCodeGenWriteBarrier(&___setReverseRelative_20, value);
	}

	inline static int32_t get_offset_of_startEvent_21() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___startEvent_21)); }
	inline FsmEvent_t1258573736 * get_startEvent_21() const { return ___startEvent_21; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_21() { return &___startEvent_21; }
	inline void set_startEvent_21(FsmEvent_t1258573736 * value)
	{
		___startEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_21, value);
	}

	inline static int32_t get_offset_of_finishEvent_22() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___finishEvent_22)); }
	inline FsmEvent_t1258573736 * get_finishEvent_22() const { return ___finishEvent_22; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_22() { return &___finishEvent_22; }
	inline void set_finishEvent_22(FsmEvent_t1258573736 * value)
	{
		___finishEvent_22 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_22, value);
	}

	inline static int32_t get_offset_of_finishImmediately_23() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___finishImmediately_23)); }
	inline FsmBool_t664485696 * get_finishImmediately_23() const { return ___finishImmediately_23; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_23() { return &___finishImmediately_23; }
	inline void set_finishImmediately_23(FsmBool_t664485696 * value)
	{
		___finishImmediately_23 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_23, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_24() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___tweenIdDescription_24)); }
	inline String_t* get_tweenIdDescription_24() const { return ___tweenIdDescription_24; }
	inline String_t** get_address_of_tweenIdDescription_24() { return &___tweenIdDescription_24; }
	inline void set_tweenIdDescription_24(String_t* value)
	{
		___tweenIdDescription_24 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_24, value);
	}

	inline static int32_t get_offset_of_tweenIdType_25() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___tweenIdType_25)); }
	inline int32_t get_tweenIdType_25() const { return ___tweenIdType_25; }
	inline int32_t* get_address_of_tweenIdType_25() { return &___tweenIdType_25; }
	inline void set_tweenIdType_25(int32_t value)
	{
		___tweenIdType_25 = value;
	}

	inline static int32_t get_offset_of_stringAsId_26() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___stringAsId_26)); }
	inline FsmString_t2414474701 * get_stringAsId_26() const { return ___stringAsId_26; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_26() { return &___stringAsId_26; }
	inline void set_stringAsId_26(FsmString_t2414474701 * value)
	{
		___stringAsId_26 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_26, value);
	}

	inline static int32_t get_offset_of_tagAsId_27() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___tagAsId_27)); }
	inline FsmString_t2414474701 * get_tagAsId_27() const { return ___tagAsId_27; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_27() { return &___tagAsId_27; }
	inline void set_tagAsId_27(FsmString_t2414474701 * value)
	{
		___tagAsId_27 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_27, value);
	}

	inline static int32_t get_offset_of_selectedEase_28() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___selectedEase_28)); }
	inline int32_t get_selectedEase_28() const { return ___selectedEase_28; }
	inline int32_t* get_address_of_selectedEase_28() { return &___selectedEase_28; }
	inline void set_selectedEase_28(int32_t value)
	{
		___selectedEase_28 = value;
	}

	inline static int32_t get_offset_of_easeType_29() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___easeType_29)); }
	inline int32_t get_easeType_29() const { return ___easeType_29; }
	inline int32_t* get_address_of_easeType_29() { return &___easeType_29; }
	inline void set_easeType_29(int32_t value)
	{
		___easeType_29 = value;
	}

	inline static int32_t get_offset_of_animationCurve_30() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___animationCurve_30)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_30() const { return ___animationCurve_30; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_30() { return &___animationCurve_30; }
	inline void set_animationCurve_30(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_30 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_30, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_31() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___loopsDescriptionArea_31)); }
	inline String_t* get_loopsDescriptionArea_31() const { return ___loopsDescriptionArea_31; }
	inline String_t** get_address_of_loopsDescriptionArea_31() { return &___loopsDescriptionArea_31; }
	inline void set_loopsDescriptionArea_31(String_t* value)
	{
		___loopsDescriptionArea_31 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_31, value);
	}

	inline static int32_t get_offset_of_loops_32() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___loops_32)); }
	inline FsmInt_t1273009179 * get_loops_32() const { return ___loops_32; }
	inline FsmInt_t1273009179 ** get_address_of_loops_32() { return &___loops_32; }
	inline void set_loops_32(FsmInt_t1273009179 * value)
	{
		___loops_32 = value;
		Il2CppCodeGenWriteBarrier(&___loops_32, value);
	}

	inline static int32_t get_offset_of_loopType_33() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___loopType_33)); }
	inline int32_t get_loopType_33() const { return ___loopType_33; }
	inline int32_t* get_address_of_loopType_33() { return &___loopType_33; }
	inline void set_loopType_33(int32_t value)
	{
		___loopType_33 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_34() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___autoKillOnCompletion_34)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_34() const { return ___autoKillOnCompletion_34; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_34() { return &___autoKillOnCompletion_34; }
	inline void set_autoKillOnCompletion_34(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_34 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_34, value);
	}

	inline static int32_t get_offset_of_recyclable_35() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___recyclable_35)); }
	inline FsmBool_t664485696 * get_recyclable_35() const { return ___recyclable_35; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_35() { return &___recyclable_35; }
	inline void set_recyclable_35(FsmBool_t664485696 * value)
	{
		___recyclable_35 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_35, value);
	}

	inline static int32_t get_offset_of_updateType_36() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___updateType_36)); }
	inline int32_t get_updateType_36() const { return ___updateType_36; }
	inline int32_t* get_address_of_updateType_36() { return &___updateType_36; }
	inline void set_updateType_36(int32_t value)
	{
		___updateType_36 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_37() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___isIndependentUpdate_37)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_37() const { return ___isIndependentUpdate_37; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_37() { return &___isIndependentUpdate_37; }
	inline void set_isIndependentUpdate_37(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_37 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_37, value);
	}

	inline static int32_t get_offset_of_debugThis_38() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___debugThis_38)); }
	inline FsmBool_t664485696 * get_debugThis_38() const { return ___debugThis_38; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_38() { return &___debugThis_38; }
	inline void set_debugThis_38(FsmBool_t664485696 * value)
	{
		___debugThis_38 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_38, value);
	}

	inline static int32_t get_offset_of_tweener_39() { return static_cast<int32_t>(offsetof(DOTweenRigidbodyLookAtPosition_t2358630582, ___tweener_39)); }
	inline Tweener_t760404022 * get_tweener_39() const { return ___tweener_39; }
	inline Tweener_t760404022 ** get_address_of_tweener_39() { return &___tweener_39; }
	inline void set_tweener_39(Tweener_t760404022 * value)
	{
		___tweener_39 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
