﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetColorRGBA
struct SetColorRGBA_t2500887559;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::.ctor()
extern "C"  void SetColorRGBA__ctor_m2502055023 (SetColorRGBA_t2500887559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::Reset()
extern "C"  void SetColorRGBA_Reset_m3296107832 (SetColorRGBA_t2500887559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::OnEnter()
extern "C"  void SetColorRGBA_OnEnter_m2051699734 (SetColorRGBA_t2500887559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::OnUpdate()
extern "C"  void SetColorRGBA_OnUpdate_m2105480183 (SetColorRGBA_t2500887559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorRGBA::DoSetColorRGBA()
extern "C"  void SetColorRGBA_DoSetColorRGBA_m769674369 (SetColorRGBA_t2500887559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
