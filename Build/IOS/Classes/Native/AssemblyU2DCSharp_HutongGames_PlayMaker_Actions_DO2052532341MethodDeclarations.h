﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformPunchAnchorPos
struct DOTweenRectTransformPunchAnchorPos_t2052532341;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformPunchAnchorPos::.ctor()
extern "C"  void DOTweenRectTransformPunchAnchorPos__ctor_m2442893981 (DOTweenRectTransformPunchAnchorPos_t2052532341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformPunchAnchorPos::Reset()
extern "C"  void DOTweenRectTransformPunchAnchorPos_Reset_m4161065950 (DOTweenRectTransformPunchAnchorPos_t2052532341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformPunchAnchorPos::OnEnter()
extern "C"  void DOTweenRectTransformPunchAnchorPos_OnEnter_m3415220 (DOTweenRectTransformPunchAnchorPos_t2052532341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformPunchAnchorPos::<OnEnter>m__72()
extern "C"  void DOTweenRectTransformPunchAnchorPos_U3COnEnterU3Em__72_m132630522 (DOTweenRectTransformPunchAnchorPos_t2052532341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformPunchAnchorPos::<OnEnter>m__73()
extern "C"  void DOTweenRectTransformPunchAnchorPos_U3COnEnterU3Em__73_m273793023 (DOTweenRectTransformPunchAnchorPos_t2052532341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
