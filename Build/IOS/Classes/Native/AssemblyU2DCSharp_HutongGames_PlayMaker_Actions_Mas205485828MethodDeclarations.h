﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetProperties
struct MasterServerGetProperties_t205485828;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::.ctor()
extern "C"  void MasterServerGetProperties__ctor_m163360248 (MasterServerGetProperties_t205485828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::Reset()
extern "C"  void MasterServerGetProperties_Reset_m855119421 (MasterServerGetProperties_t205485828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::OnEnter()
extern "C"  void MasterServerGetProperties_OnEnter_m1416772805 (MasterServerGetProperties_t205485828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetProperties::GetMasterServerProperties()
extern "C"  void MasterServerGetProperties_GetMasterServerProperties_m4121447740 (MasterServerGetProperties_t205485828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
