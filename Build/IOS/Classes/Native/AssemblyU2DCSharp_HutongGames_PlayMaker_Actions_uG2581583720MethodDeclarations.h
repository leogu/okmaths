﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetHideMobileInput
struct uGuiInputFieldSetHideMobileInput_t2581583720;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetHideMobileInput::.ctor()
extern "C"  void uGuiInputFieldSetHideMobileInput__ctor_m671485848 (uGuiInputFieldSetHideMobileInput_t2581583720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetHideMobileInput::Reset()
extern "C"  void uGuiInputFieldSetHideMobileInput_Reset_m3518153945 (uGuiInputFieldSetHideMobileInput_t2581583720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetHideMobileInput::OnEnter()
extern "C"  void uGuiInputFieldSetHideMobileInput_OnEnter_m907477841 (uGuiInputFieldSetHideMobileInput_t2581583720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetHideMobileInput::DoSetValue()
extern "C"  void uGuiInputFieldSetHideMobileInput_DoSetValue_m1008066642 (uGuiInputFieldSetHideMobileInput_t2581583720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetHideMobileInput::OnExit()
extern "C"  void uGuiInputFieldSetHideMobileInput_OnExit_m2093624037 (uGuiInputFieldSetHideMobileInput_t2581583720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
