﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch
struct NetworkPeerTypeSwitch_t3158774876;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::.ctor()
extern "C"  void NetworkPeerTypeSwitch__ctor_m4234856768 (NetworkPeerTypeSwitch_t3158774876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::Reset()
extern "C"  void NetworkPeerTypeSwitch_Reset_m631656597 (NetworkPeerTypeSwitch_t3158774876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::OnEnter()
extern "C"  void NetworkPeerTypeSwitch_OnEnter_m2632995485 (NetworkPeerTypeSwitch_t3158774876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::OnUpdate()
extern "C"  void NetworkPeerTypeSwitch_OnUpdate_m2765316582 (NetworkPeerTypeSwitch_t3158774876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkPeerTypeSwitch::DoNetworkPeerTypeSwitch()
extern "C"  void NetworkPeerTypeSwitch_DoNetworkPeerTypeSwitch_m2765258925 (NetworkPeerTypeSwitch_t3158774876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
