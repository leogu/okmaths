﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToById
struct DOTweenControlMethodsGoToById_t2684174402;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToById::.ctor()
extern "C"  void DOTweenControlMethodsGoToById__ctor_m1483689096 (DOTweenControlMethodsGoToById_t2684174402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToById::Reset()
extern "C"  void DOTweenControlMethodsGoToById_Reset_m3632682875 (DOTweenControlMethodsGoToById_t2684174402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsGoToById::OnEnter()
extern "C"  void DOTweenControlMethodsGoToById_OnEnter_m4056462955 (DOTweenControlMethodsGoToById_t2684174402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
