﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetBackgroundColor
struct SetBackgroundColor_t423384025;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::.ctor()
extern "C"  void SetBackgroundColor__ctor_m2311639891 (SetBackgroundColor_t423384025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::Reset()
extern "C"  void SetBackgroundColor_Reset_m3106206410 (SetBackgroundColor_t423384025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::OnEnter()
extern "C"  void SetBackgroundColor_OnEnter_m676937668 (SetBackgroundColor_t423384025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::OnUpdate()
extern "C"  void SetBackgroundColor_OnUpdate_m2935948035 (SetBackgroundColor_t423384025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetBackgroundColor::DoSetBackgroundColor()
extern "C"  void SetBackgroundColor_DoSetBackgroundColor_m872035457 (SetBackgroundColor_t423384025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
