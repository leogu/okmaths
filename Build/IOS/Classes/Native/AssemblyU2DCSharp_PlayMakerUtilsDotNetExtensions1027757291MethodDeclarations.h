﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.VariableType[]
struct VariableTypeU5BU5D_t4294461311;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Boolean PlayMakerUtilsDotNetExtensions::Contains(HutongGames.PlayMaker.VariableType[],HutongGames.PlayMaker.VariableType)
extern "C"  bool PlayMakerUtilsDotNetExtensions_Contains_m3716285363 (Il2CppObject * __this /* static, unused */, VariableTypeU5BU5D_t4294461311* ___target0, int32_t ___vType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
