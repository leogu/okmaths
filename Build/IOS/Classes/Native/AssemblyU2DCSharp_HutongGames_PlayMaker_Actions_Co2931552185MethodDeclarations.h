﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CommonDivisorMultiple
struct CommonDivisorMultiple_t2931552185;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CommonDivisorMultiple::.ctor()
extern "C"  void CommonDivisorMultiple__ctor_m1237629327 (CommonDivisorMultiple_t2931552185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CommonDivisorMultiple::Reset()
extern "C"  void CommonDivisorMultiple_Reset_m3565980626 (CommonDivisorMultiple_t2931552185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CommonDivisorMultiple::OnEnter()
extern "C"  void CommonDivisorMultiple_OnEnter_m1621821716 (CommonDivisorMultiple_t2931552185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CommonDivisorMultiple::OnUpdate()
extern "C"  void CommonDivisorMultiple_OnUpdate_m3610067071 (CommonDivisorMultiple_t2931552185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CommonDivisorMultiple::DoCal()
extern "C"  void CommonDivisorMultiple_DoCal_m750268530 (CommonDivisorMultiple_t2931552185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
