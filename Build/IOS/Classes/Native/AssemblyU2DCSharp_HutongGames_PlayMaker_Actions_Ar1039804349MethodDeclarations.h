﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayAddRange
struct ArrayAddRange_t1039804349;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::.ctor()
extern "C"  void ArrayAddRange__ctor_m162800897 (ArrayAddRange_t1039804349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::Reset()
extern "C"  void ArrayAddRange_Reset_m2464075030 (ArrayAddRange_t1039804349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::OnEnter()
extern "C"  void ArrayAddRange_OnEnter_m1775271724 (ArrayAddRange_t1039804349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayAddRange::DoAddRange()
extern "C"  void ArrayAddRange_DoAddRange_m2304842660 (ArrayAddRange_t1039804349 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
