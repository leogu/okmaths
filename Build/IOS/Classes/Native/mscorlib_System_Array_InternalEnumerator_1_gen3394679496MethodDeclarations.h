﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3394679496.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Cu2535927234.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1110309857_gshared (InternalEnumerator_1_t3394679496 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1110309857(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3394679496 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1110309857_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m78983221_gshared (InternalEnumerator_1_t3394679496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m78983221(__this, method) ((  void (*) (InternalEnumerator_1_t3394679496 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m78983221_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m351715741_gshared (InternalEnumerator_1_t3394679496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m351715741(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3394679496 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m351715741_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1785947314_gshared (InternalEnumerator_1_t3394679496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1785947314(__this, method) ((  void (*) (InternalEnumerator_1_t3394679496 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1785947314_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m185515549_gshared (InternalEnumerator_1_t3394679496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m185515549(__this, method) ((  bool (*) (InternalEnumerator_1_t3394679496 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m185515549_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.Actions.CurveFsmAction/Calculation>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m4111881272_gshared (InternalEnumerator_1_t3394679496 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4111881272(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3394679496 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4111881272_gshared)(__this, method)
