﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLightBlendableColor
struct DOTweenLightBlendableColor_t3983294788;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLightBlendableColor::.ctor()
extern "C"  void DOTweenLightBlendableColor__ctor_m2050827790 (DOTweenLightBlendableColor_t3983294788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightBlendableColor::Reset()
extern "C"  void DOTweenLightBlendableColor_Reset_m1251088813 (DOTweenLightBlendableColor_t3983294788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightBlendableColor::OnEnter()
extern "C"  void DOTweenLightBlendableColor_OnEnter_m1906354693 (DOTweenLightBlendableColor_t3983294788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightBlendableColor::<OnEnter>m__46()
extern "C"  void DOTweenLightBlendableColor_U3COnEnterU3Em__46_m1119215288 (DOTweenLightBlendableColor_t3983294788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLightBlendableColor::<OnEnter>m__47()
extern "C"  void DOTweenLightBlendableColor_U3COnEnterU3Em__47_m1260377789 (DOTweenLightBlendableColor_t3983294788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
