﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2LowPassFilter
struct Vector2LowPassFilter_t2408986664;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::.ctor()
extern "C"  void Vector2LowPassFilter__ctor_m1649936856 (Vector2LowPassFilter_t2408986664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::Reset()
extern "C"  void Vector2LowPassFilter_Reset_m2719825465 (Vector2LowPassFilter_t2408986664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::OnEnter()
extern "C"  void Vector2LowPassFilter_OnEnter_m2629696593 (Vector2LowPassFilter_t2408986664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2LowPassFilter::OnUpdate()
extern "C"  void Vector2LowPassFilter_OnUpdate_m1218939766 (Vector2LowPassFilter_t2408986664 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
