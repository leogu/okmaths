﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformScale
struct DOTweenTransformScale_t3800860112;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScale::.ctor()
extern "C"  void DOTweenTransformScale__ctor_m37410232 (DOTweenTransformScale_t3800860112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScale::Reset()
extern "C"  void DOTweenTransformScale_Reset_m3994265637 (DOTweenTransformScale_t3800860112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScale::OnEnter()
extern "C"  void DOTweenTransformScale_OnEnter_m369594717 (DOTweenTransformScale_t3800860112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScale::<OnEnter>m__D6()
extern "C"  void DOTweenTransformScale_U3COnEnterU3Em__D6_m814826966 (DOTweenTransformScale_t3800860112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScale::<OnEnter>m__D7()
extern "C"  void DOTweenTransformScale_U3COnEnterU3Em__D7_m814827061 (DOTweenTransformScale_t3800860112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
