﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVarOverride
struct FsmVarOverride_t639182869;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVarOverride639182869.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"

// System.Void HutongGames.PlayMaker.FsmVarOverride::.ctor(HutongGames.PlayMaker.FsmVarOverride)
extern "C"  void FsmVarOverride__ctor_m567747291 (FsmVarOverride_t639182869 * __this, FsmVarOverride_t639182869 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVarOverride::.ctor(HutongGames.PlayMaker.NamedVariable)
extern "C"  void FsmVarOverride__ctor_m3063307363 (FsmVarOverride_t639182869 * __this, NamedVariable_t3026441313 * ___namedVar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVarOverride::Apply(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVarOverride_Apply_m1056884487 (FsmVarOverride_t639182869 * __this, FsmVariables_t630687169 * ___variables0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
