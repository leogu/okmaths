﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CreateObject
struct CreateObject_t1397922645;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CreateObject::.ctor()
extern "C"  void CreateObject__ctor_m825704363 (CreateObject_t1397922645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateObject::Reset()
extern "C"  void CreateObject_Reset_m1901844258 (CreateObject_t1397922645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CreateObject::OnEnter()
extern "C"  void CreateObject_OnEnter_m1431193228 (CreateObject_t1397922645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
