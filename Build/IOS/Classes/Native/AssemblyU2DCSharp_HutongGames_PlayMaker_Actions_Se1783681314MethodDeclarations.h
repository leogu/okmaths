﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightCookie
struct SetLightCookie_t1783681314;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::.ctor()
extern "C"  void SetLightCookie__ctor_m121768530 (SetLightCookie_t1783681314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::Reset()
extern "C"  void SetLightCookie_Reset_m1567908811 (SetLightCookie_t1783681314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::OnEnter()
extern "C"  void SetLightCookie_OnEnter_m2944340955 (SetLightCookie_t1783681314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightCookie::DoSetLightCookie()
extern "C"  void SetLightCookie_DoSetLightCookie_m144910113 (SetLightCookie_t1783681314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
