﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadRequestedScene
struct LoadRequestedScene_t2989246692;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadRequestedScene::.ctor()
extern "C"  void LoadRequestedScene__ctor_m570664411 (LoadRequestedScene_t2989246692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadRequestedScene::Start()
extern "C"  void LoadRequestedScene_Start_m2254282255 (LoadRequestedScene_t2989246692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadRequestedScene::LoadRequestedScenee()
extern "C"  void LoadRequestedScene_LoadRequestedScenee_m2889935500 (LoadRequestedScene_t2989246692 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
