﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EnableFog
struct EnableFog_t18595813;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EnableFog::.ctor()
extern "C"  void EnableFog__ctor_m2522865405 (EnableFog_t18595813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFog::Reset()
extern "C"  void EnableFog_Reset_m2340040994 (EnableFog_t18595813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFog::OnEnter()
extern "C"  void EnableFog_OnEnter_m852669648 (EnableFog_t18595813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EnableFog::OnUpdate()
extern "C"  void EnableFog_OnUpdate_m2431011681 (EnableFog_t18595813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
