﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScreenToWorldPoint
struct ScreenToWorldPoint_t1519632141;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::.ctor()
extern "C"  void ScreenToWorldPoint__ctor_m2185184219 (ScreenToWorldPoint_t1519632141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::Reset()
extern "C"  void ScreenToWorldPoint_Reset_m221764338 (ScreenToWorldPoint_t1519632141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::OnEnter()
extern "C"  void ScreenToWorldPoint_OnEnter_m1356141828 (ScreenToWorldPoint_t1519632141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::OnUpdate()
extern "C"  void ScreenToWorldPoint_OnUpdate_m1745062579 (ScreenToWorldPoint_t1519632141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScreenToWorldPoint::DoScreenToWorldPoint()
extern "C"  void ScreenToWorldPoint_DoScreenToWorldPoint_m3535202433 (ScreenToWorldPoint_t1519632141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
