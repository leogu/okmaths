﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiTextSetText
struct  uGuiTextSetText_t2405364386  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiTextSetText::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiTextSetText::text
	FsmString_t2414474701 * ___text_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiTextSetText::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiTextSetText::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.UI.Text HutongGames.PlayMaker.Actions.uGuiTextSetText::_text
	Text_t356221433 * ____text_15;
	// System.String HutongGames.PlayMaker.Actions.uGuiTextSetText::_originalString
	String_t* ____originalString_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiTextSetText_t2405364386, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(uGuiTextSetText_t2405364386, ___text_12)); }
	inline FsmString_t2414474701 * get_text_12() const { return ___text_12; }
	inline FsmString_t2414474701 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(FsmString_t2414474701 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier(&___text_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiTextSetText_t2405364386, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiTextSetText_t2405364386, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__text_15() { return static_cast<int32_t>(offsetof(uGuiTextSetText_t2405364386, ____text_15)); }
	inline Text_t356221433 * get__text_15() const { return ____text_15; }
	inline Text_t356221433 ** get_address_of__text_15() { return &____text_15; }
	inline void set__text_15(Text_t356221433 * value)
	{
		____text_15 = value;
		Il2CppCodeGenWriteBarrier(&____text_15, value);
	}

	inline static int32_t get_offset_of__originalString_16() { return static_cast<int32_t>(offsetof(uGuiTextSetText_t2405364386, ____originalString_16)); }
	inline String_t* get__originalString_16() const { return ____originalString_16; }
	inline String_t** get_address_of__originalString_16() { return &____originalString_16; }
	inline void set__originalString_16(String_t* value)
	{
		____originalString_16 = value;
		Il2CppCodeGenWriteBarrier(&____originalString_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
