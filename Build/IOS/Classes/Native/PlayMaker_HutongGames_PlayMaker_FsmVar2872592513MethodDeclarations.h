﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// System.Type
struct Type_t;
// System.Enum
struct Enum_t2459695545;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Texture
struct Texture_t2243626319;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t4287019078;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Enum2459695545.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar2872592513.h"
#include "mscorlib_System_Object2689449295.h"

// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmVar::get_NamedVar()
extern "C"  NamedVariable_t3026441313 * FsmVar_get_NamedVar_m4159647311 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_NamedVar(HutongGames.PlayMaker.NamedVariable)
extern "C"  void FsmVar_set_NamedVar_m3931525694 (FsmVar_t2872592513 * __this, NamedVariable_t3026441313 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmVar::get_NamedVarType()
extern "C"  Type_t * FsmVar_get_NamedVarType_m3150530715 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmVar::get_EnumType()
extern "C"  Type_t * FsmVar_get_EnumType_m2697052258 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_EnumType(System.Type)
extern "C"  void FsmVar_set_EnumType_m2738954053 (FsmVar_t2872592513 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Enum HutongGames.PlayMaker.FsmVar::get_EnumValue()
extern "C"  Enum_t2459695545 * FsmVar_get_EnumValue_m783618248 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_EnumValue(System.Enum)
extern "C"  void FsmVar_set_EnumValue_m2225023937 (FsmVar_t2872592513 * __this, Enum_t2459695545 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmVar::get_ObjectType()
extern "C"  Type_t * FsmVar_get_ObjectType_m167428156 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_ObjectType(System.Type)
extern "C"  void FsmVar_set_ObjectType_m2728052267 (FsmVar_t2872592513 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVar::get_Type()
extern "C"  int32_t FsmVar_get_Type_m3920234816 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_Type(HutongGames.PlayMaker.VariableType)
extern "C"  void FsmVar_set_Type_m3847714689 (FsmVar_t2872592513 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmVar::get_RealType()
extern "C"  Type_t * FsmVar_get_RealType_m694863411 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVar::get_IsNone()
extern "C"  bool FsmVar_get_IsNone_m3995832317 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVar::get_vector2Value()
extern "C"  Vector2_t2243707579  FsmVar_get_vector2Value_m1379354294 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_vector2Value(UnityEngine.Vector2)
extern "C"  void FsmVar_set_vector2Value_m1001412473 (FsmVar_t2872592513 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVar::get_vector3Value()
extern "C"  Vector3_t2243707580  FsmVar_get_vector3Value_m3479384598 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_vector3Value(UnityEngine.Vector3)
extern "C"  void FsmVar_set_vector3Value_m545925437 (FsmVar_t2872592513 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.FsmVar::get_colorValue()
extern "C"  Color_t2020392075  FsmVar_get_colorValue_m571237974 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_colorValue(UnityEngine.Color)
extern "C"  void FsmVar_set_colorValue_m3612131293 (FsmVar_t2872592513 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.FsmVar::get_rectValue()
extern "C"  Rect_t3681755626  FsmVar_get_rectValue_m997711486 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_rectValue(UnityEngine.Rect)
extern "C"  void FsmVar_set_rectValue_m909927937 (FsmVar_t2872592513 * __this, Rect_t3681755626  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion HutongGames.PlayMaker.FsmVar::get_quaternionValue()
extern "C"  Quaternion_t4030073918  FsmVar_get_quaternionValue_m4158843358 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_quaternionValue(UnityEngine.Quaternion)
extern "C"  void FsmVar_set_quaternionValue_m3859005889 (FsmVar_t2872592513 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.FsmVar::get_gameObjectValue()
extern "C"  GameObject_t1756533147 * FsmVar_get_gameObjectValue_m4097298956 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_gameObjectValue(UnityEngine.GameObject)
extern "C"  void FsmVar_set_gameObjectValue_m203872577 (FsmVar_t2872592513 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material HutongGames.PlayMaker.FsmVar::get_materialValue()
extern "C"  Material_t193706927 * FsmVar_get_materialValue_m2480928972 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_materialValue(UnityEngine.Material)
extern "C"  void FsmVar_set_materialValue_m1792017953 (FsmVar_t2872592513 * __this, Material_t193706927 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture HutongGames.PlayMaker.FsmVar::get_textureValue()
extern "C"  Texture_t2243626319 * FsmVar_get_textureValue_m1170451510 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::set_textureValue(UnityEngine.Texture)
extern "C"  void FsmVar_set_textureValue_m3216530089 (FsmVar_t2872592513 * __this, Texture_t2243626319 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor()
extern "C"  void FsmVar__ctor_m1093947946 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor(System.Type)
extern "C"  void FsmVar__ctor_m2397726347 (FsmVar_t2872592513 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor(HutongGames.PlayMaker.FsmVar)
extern "C"  void FsmVar__ctor_m616380443 (FsmVar_t2872592513 * __this, FsmVar_t2872592513 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::.ctor(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar__ctor_m83418448 (FsmVar_t2872592513 * __this, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::Init(HutongGames.PlayMaker.NamedVariable)
extern "C"  void FsmVar_Init_m1052285597 (FsmVar_t2872592513 * __this, NamedVariable_t3026441313 * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::UpdateType(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_UpdateType_m2054010999 (FsmVar_t2872592513 * __this, Il2CppObject * ___sourceVar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::InitNamedVar()
extern "C"  void FsmVar_InitNamedVar_m3600674168 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::InitEnumType()
extern "C"  void FsmVar_InitEnumType_m3602580133 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmVar::GetValue()
extern "C"  Il2CppObject * FsmVar_GetValue_m2514959358 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::GetValueFrom(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_GetValueFrom_m4288313497 (FsmVar_t2872592513 * __this, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::UpdateValue()
extern "C"  void FsmVar_UpdateValue_m4265045260 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::ApplyValueTo(HutongGames.PlayMaker.INamedVariable)
extern "C"  void FsmVar_ApplyValueTo_m3651631394 (FsmVar_t2872592513 * __this, Il2CppObject * ___targetVariable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVar::DebugString()
extern "C"  String_t* FsmVar_DebugString_m253057497 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmVar::ToString()
extern "C"  String_t* FsmVar_ToString_m90515791 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::SetValue(System.Object)
extern "C"  void FsmVar_SetValue_m2315358391 (FsmVar_t2872592513 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVar::DebugLog()
extern "C"  void FsmVar_DebugLog_m705720533 (FsmVar_t2872592513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmVar::GetVariableType(System.Type)
extern "C"  int32_t FsmVar_GetVariableType_m195376106 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
