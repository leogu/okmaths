﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScaleGUI
struct ScaleGUI_t2599920435;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::.ctor()
extern "C"  void ScaleGUI__ctor_m3763331221 (ScaleGUI_t2599920435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::Reset()
extern "C"  void ScaleGUI_Reset_m1187666236 (ScaleGUI_t2599920435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::OnGUI()
extern "C"  void ScaleGUI_OnGUI_m240692131 (ScaleGUI_t2599920435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleGUI::OnUpdate()
extern "C"  void ScaleGUI_OnUpdate_m1779581033 (ScaleGUI_t2599920435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
