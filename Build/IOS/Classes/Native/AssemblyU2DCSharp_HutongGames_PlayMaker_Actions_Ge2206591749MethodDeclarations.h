﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmTexture
struct GetFsmTexture_t2206591749;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::.ctor()
extern "C"  void GetFsmTexture__ctor_m2976335415 (GetFsmTexture_t2206591749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::Reset()
extern "C"  void GetFsmTexture_Reset_m3552732250 (GetFsmTexture_t2206591749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::OnEnter()
extern "C"  void GetFsmTexture_OnEnter_m497116620 (GetFsmTexture_t2206591749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::OnUpdate()
extern "C"  void GetFsmTexture_OnUpdate_m2194137271 (GetFsmTexture_t2206591749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmTexture::DoGetFsmVariable()
extern "C"  void GetFsmTexture_DoGetFsmVariable_m3286165478 (GetFsmTexture_t2206591749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
