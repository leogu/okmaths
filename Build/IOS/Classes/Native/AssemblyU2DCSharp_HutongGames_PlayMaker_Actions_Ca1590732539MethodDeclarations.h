﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CameraFadeOut
struct CameraFadeOut_t1590732539;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::.ctor()
extern "C"  void CameraFadeOut__ctor_m1320484067 (CameraFadeOut_t1590732539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::Reset()
extern "C"  void CameraFadeOut_Reset_m1871311568 (CameraFadeOut_t1590732539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::OnEnter()
extern "C"  void CameraFadeOut_OnEnter_m3117888614 (CameraFadeOut_t1590732539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::OnUpdate()
extern "C"  void CameraFadeOut_OnUpdate_m2808132203 (CameraFadeOut_t1590732539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CameraFadeOut::OnGUI()
extern "C"  void CameraFadeOut_OnGUI_m118602797 (CameraFadeOut_t1590732539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
