﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmObject
struct SetFsmObject_t3286560103;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::.ctor()
extern "C"  void SetFsmObject__ctor_m2139089017 (SetFsmObject_t3286560103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::Reset()
extern "C"  void SetFsmObject_Reset_m3824522488 (SetFsmObject_t3286560103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::OnEnter()
extern "C"  void SetFsmObject_OnEnter_m3146556578 (SetFsmObject_t3286560103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::DoSetFsmBool()
extern "C"  void SetFsmObject_DoSetFsmBool_m2735063900 (SetFsmObject_t3286560103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmObject::OnUpdate()
extern "C"  void SetFsmObject_OnUpdate_m2831418325 (SetFsmObject_t3286560103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
