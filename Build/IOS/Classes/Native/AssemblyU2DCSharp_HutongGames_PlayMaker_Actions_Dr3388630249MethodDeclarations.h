﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawStateLabel
struct DrawStateLabel_t3388630249;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawStateLabel::.ctor()
extern "C"  void DrawStateLabel__ctor_m2926476675 (DrawStateLabel_t3388630249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawStateLabel::Reset()
extern "C"  void DrawStateLabel_Reset_m594149434 (DrawStateLabel_t3388630249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawStateLabel::OnEnter()
extern "C"  void DrawStateLabel_OnEnter_m498903972 (DrawStateLabel_t3388630249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
