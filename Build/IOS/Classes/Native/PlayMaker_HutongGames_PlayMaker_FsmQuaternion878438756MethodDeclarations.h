﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion878438756.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.Quaternion HutongGames.PlayMaker.FsmQuaternion::get_Value()
extern "C"  Quaternion_t4030073918  FsmQuaternion_get_Value_m2629833107 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::set_Value(UnityEngine.Quaternion)
extern "C"  void FsmQuaternion_set_Value_m1061544654 (FsmQuaternion_t878438756 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmQuaternion::get_RawValue()
extern "C"  Il2CppObject * FsmQuaternion_get_RawValue_m156649094 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::set_RawValue(System.Object)
extern "C"  void FsmQuaternion_set_RawValue_m1281778201 (FsmQuaternion_t878438756 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::.ctor()
extern "C"  void FsmQuaternion__ctor_m3078120561 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::.ctor(System.String)
extern "C"  void FsmQuaternion__ctor_m2418249539 (FsmQuaternion_t878438756 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::.ctor(HutongGames.PlayMaker.FsmQuaternion)
extern "C"  void FsmQuaternion__ctor_m1293044995 (FsmQuaternion_t878438756 * __this, FsmQuaternion_t878438756 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmQuaternion::Clone()
extern "C"  NamedVariable_t3026441313 * FsmQuaternion_Clone_m2300317292 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmQuaternion::get_VariableType()
extern "C"  int32_t FsmQuaternion_get_VariableType_m2736361829 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmQuaternion::ToString()
extern "C"  String_t* FsmQuaternion_ToString_m18655502 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmQuaternion::op_Implicit(UnityEngine.Quaternion)
extern "C"  FsmQuaternion_t878438756 * FsmQuaternion_op_Implicit_m1876528298 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
