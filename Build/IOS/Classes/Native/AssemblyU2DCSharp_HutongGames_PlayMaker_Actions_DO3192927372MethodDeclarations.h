﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraShakePosition
struct DOTweenCameraShakePosition_t3192927372;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakePosition::.ctor()
extern "C"  void DOTweenCameraShakePosition__ctor_m1793524480 (DOTweenCameraShakePosition_t3192927372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakePosition::Reset()
extern "C"  void DOTweenCameraShakePosition_Reset_m3759679897 (DOTweenCameraShakePosition_t3192927372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakePosition::OnEnter()
extern "C"  void DOTweenCameraShakePosition_OnEnter_m1847913281 (DOTweenCameraShakePosition_t3192927372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakePosition::<OnEnter>m__32()
extern "C"  void DOTweenCameraShakePosition_U3COnEnterU3Em__32_m3913828103 (DOTweenCameraShakePosition_t3192927372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraShakePosition::<OnEnter>m__33()
extern "C"  void DOTweenCameraShakePosition_U3COnEnterU3Em__33_m3772665602 (DOTweenCameraShakePosition_t3192927372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
