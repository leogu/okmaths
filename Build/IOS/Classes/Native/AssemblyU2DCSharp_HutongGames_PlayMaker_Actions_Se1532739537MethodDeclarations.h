﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRectValue
struct SetRectValue_t1532739537;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRectValue::.ctor()
extern "C"  void SetRectValue__ctor_m505973987 (SetRectValue_t1532739537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::Reset()
extern "C"  void SetRectValue_Reset_m1947791194 (SetRectValue_t1532739537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::OnEnter()
extern "C"  void SetRectValue_OnEnter_m2595350732 (SetRectValue_t1532739537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectValue::OnUpdate()
extern "C"  void SetRectValue_OnUpdate_m3392731339 (SetRectValue_t1532739537 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
