﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatDivide
struct FloatDivide_t551501307;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatDivide::.ctor()
extern "C"  void FloatDivide__ctor_m450386373 (FloatDivide_t551501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::Reset()
extern "C"  void FloatDivide_Reset_m3473464760 (FloatDivide_t551501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::OnEnter()
extern "C"  void FloatDivide_OnEnter_m185313026 (FloatDivide_t551501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatDivide::OnUpdate()
extern "C"  void FloatDivide_OnUpdate_m556311777 (FloatDivide_t551501307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
