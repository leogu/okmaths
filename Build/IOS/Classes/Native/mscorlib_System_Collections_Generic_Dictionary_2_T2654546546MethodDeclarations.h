﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2822338421MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m1469573638(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2654546546 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2641568392_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1301435150(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2654546546 *, Fsm_t917886356 *, RaycastHit2D_t4063908774 , const MethodInfo*))Transform_1_Invoke_m636128608_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2189584679(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2654546546 *, Fsm_t917886356 *, RaycastHit2D_t4063908774 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2662683403_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2206175688(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t2654546546 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2124733066_gshared)(__this, ___result0, method)
