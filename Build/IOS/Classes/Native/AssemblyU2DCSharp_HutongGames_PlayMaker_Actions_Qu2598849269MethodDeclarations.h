﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionLowPassFilter
struct QuaternionLowPassFilter_t2598849269;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::.ctor()
extern "C"  void QuaternionLowPassFilter__ctor_m4015493771 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::Reset()
extern "C"  void QuaternionLowPassFilter_Reset_m2111646902 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnEnter()
extern "C"  void QuaternionLowPassFilter_OnEnter_m1685750224 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnUpdate()
extern "C"  void QuaternionLowPassFilter_OnUpdate_m3807608419 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnLateUpdate()
extern "C"  void QuaternionLowPassFilter_OnLateUpdate_m1695843495 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::OnFixedUpdate()
extern "C"  void QuaternionLowPassFilter_OnFixedUpdate_m4043056181 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionLowPassFilter::DoQuatLowPassFilter()
extern "C"  void QuaternionLowPassFilter_DoQuatLowPassFilter_m950652094 (QuaternionLowPassFilter_t2598849269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
