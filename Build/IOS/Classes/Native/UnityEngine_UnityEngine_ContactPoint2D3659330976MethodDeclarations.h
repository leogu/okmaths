﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ContactPoint2D
struct ContactPoint2D_t3659330976;
struct ContactPoint2D_t3659330976_marshaled_pinvoke;
struct ContactPoint2D_t3659330976_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
extern "C"  Vector2_t2243707579  ContactPoint2D_get_point_m2343925229 (ContactPoint2D_t3659330976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_normal()
extern "C"  Vector2_t2243707579  ContactPoint2D_get_normal_m3786302012 (ContactPoint2D_t3659330976 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ContactPoint2D_t3659330976;
struct ContactPoint2D_t3659330976_marshaled_pinvoke;

extern "C" void ContactPoint2D_t3659330976_marshal_pinvoke(const ContactPoint2D_t3659330976& unmarshaled, ContactPoint2D_t3659330976_marshaled_pinvoke& marshaled);
extern "C" void ContactPoint2D_t3659330976_marshal_pinvoke_back(const ContactPoint2D_t3659330976_marshaled_pinvoke& marshaled, ContactPoint2D_t3659330976& unmarshaled);
extern "C" void ContactPoint2D_t3659330976_marshal_pinvoke_cleanup(ContactPoint2D_t3659330976_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ContactPoint2D_t3659330976;
struct ContactPoint2D_t3659330976_marshaled_com;

extern "C" void ContactPoint2D_t3659330976_marshal_com(const ContactPoint2D_t3659330976& unmarshaled, ContactPoint2D_t3659330976_marshaled_com& marshaled);
extern "C" void ContactPoint2D_t3659330976_marshal_com_back(const ContactPoint2D_t3659330976_marshaled_com& marshaled, ContactPoint2D_t3659330976& unmarshaled);
extern "C" void ContactPoint2D_t3659330976_marshal_com_cleanup(ContactPoint2D_t3659330976_marshaled_com& marshaled);
