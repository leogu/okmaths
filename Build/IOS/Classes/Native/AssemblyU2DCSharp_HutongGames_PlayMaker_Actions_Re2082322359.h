﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta
struct  RectTransformGetSizeDelta_t2082322359  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::sizeDelta
	FsmVector2_t2430450063 * ___sizeDelta_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::width
	FsmFloat_t937133978 * ___width_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::height
	FsmFloat_t937133978 * ___height_16;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::_rt
	RectTransform_t3349966182 * ____rt_17;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t2082322359, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_sizeDelta_14() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t2082322359, ___sizeDelta_14)); }
	inline FsmVector2_t2430450063 * get_sizeDelta_14() const { return ___sizeDelta_14; }
	inline FsmVector2_t2430450063 ** get_address_of_sizeDelta_14() { return &___sizeDelta_14; }
	inline void set_sizeDelta_14(FsmVector2_t2430450063 * value)
	{
		___sizeDelta_14 = value;
		Il2CppCodeGenWriteBarrier(&___sizeDelta_14, value);
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t2082322359, ___width_15)); }
	inline FsmFloat_t937133978 * get_width_15() const { return ___width_15; }
	inline FsmFloat_t937133978 ** get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(FsmFloat_t937133978 * value)
	{
		___width_15 = value;
		Il2CppCodeGenWriteBarrier(&___width_15, value);
	}

	inline static int32_t get_offset_of_height_16() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t2082322359, ___height_16)); }
	inline FsmFloat_t937133978 * get_height_16() const { return ___height_16; }
	inline FsmFloat_t937133978 ** get_address_of_height_16() { return &___height_16; }
	inline void set_height_16(FsmFloat_t937133978 * value)
	{
		___height_16 = value;
		Il2CppCodeGenWriteBarrier(&___height_16, value);
	}

	inline static int32_t get_offset_of__rt_17() { return static_cast<int32_t>(offsetof(RectTransformGetSizeDelta_t2082322359, ____rt_17)); }
	inline RectTransform_t3349966182 * get__rt_17() const { return ____rt_17; }
	inline RectTransform_t3349966182 ** get_address_of__rt_17() { return &____rt_17; }
	inline void set__rt_17(RectTransform_t3349966182 * value)
	{
		____rt_17 = value;
		Il2CppCodeGenWriteBarrier(&____rt_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
