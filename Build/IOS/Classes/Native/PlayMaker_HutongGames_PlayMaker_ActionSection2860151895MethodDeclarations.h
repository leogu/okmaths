﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionSection
struct ActionSection_t2860151895;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String HutongGames.PlayMaker.ActionSection::get_Section()
extern "C"  String_t* ActionSection_get_Section_m3227412155 (ActionSection_t2860151895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionSection::.ctor(System.String)
extern "C"  void ActionSection__ctor_m2599098326 (ActionSection_t2860151895 * __this, String_t* ___section0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
