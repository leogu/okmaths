﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUISkin
struct SetGUISkin_t1170183820;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::.ctor()
extern "C"  void SetGUISkin__ctor_m2552864778 (SetGUISkin_t1170183820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::Reset()
extern "C"  void SetGUISkin_Reset_m835200121 (SetGUISkin_t1170183820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUISkin::OnGUI()
extern "C"  void SetGUISkin_OnGUI_m598230426 (SetGUISkin_t1170183820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
