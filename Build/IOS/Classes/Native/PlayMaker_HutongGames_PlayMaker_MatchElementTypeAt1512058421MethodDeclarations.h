﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.MatchElementTypeAttribute
struct MatchElementTypeAttribute_t1512058421;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String HutongGames.PlayMaker.MatchElementTypeAttribute::get_FieldName()
extern "C"  String_t* MatchElementTypeAttribute_get_FieldName_m2139720489 (MatchElementTypeAttribute_t1512058421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.MatchElementTypeAttribute::.ctor(System.String)
extern "C"  void MatchElementTypeAttribute__ctor_m2707542418 (MatchElementTypeAttribute_t1512058421 * __this, String_t* ___fieldName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
