﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayDeleteAt
struct ArrayDeleteAt_t3892309371;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::.ctor()
extern "C"  void ArrayDeleteAt__ctor_m2866431111 (ArrayDeleteAt_t3892309371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::Reset()
extern "C"  void ArrayDeleteAt_Reset_m79850812 (ArrayDeleteAt_t3892309371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::OnEnter()
extern "C"  void ArrayDeleteAt_OnEnter_m3589948890 (ArrayDeleteAt_t3892309371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayDeleteAt::DoDeleteAt()
extern "C"  void ArrayDeleteAt_DoDeleteAt_m916478500 (ArrayDeleteAt_t3892309371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
