﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.KillDelayedEvents
struct KillDelayedEvents_t3385780931;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.KillDelayedEvents::.ctor()
extern "C"  void KillDelayedEvents__ctor_m964650739 (KillDelayedEvents_t3385780931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.KillDelayedEvents::OnEnter()
extern "C"  void KillDelayedEvents_OnEnter_m2872468462 (KillDelayedEvents_t3385780931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
