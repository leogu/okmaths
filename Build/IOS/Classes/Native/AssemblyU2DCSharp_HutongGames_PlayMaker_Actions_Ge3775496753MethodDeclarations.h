﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed
struct GetAnimatorPlayBackSpeed_t3775496753;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::.ctor()
extern "C"  void GetAnimatorPlayBackSpeed__ctor_m2251414007 (GetAnimatorPlayBackSpeed_t3775496753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::Reset()
extern "C"  void GetAnimatorPlayBackSpeed_Reset_m3970662102 (GetAnimatorPlayBackSpeed_t3775496753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::OnEnter()
extern "C"  void GetAnimatorPlayBackSpeed_OnEnter_m2140183640 (GetAnimatorPlayBackSpeed_t3775496753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::OnUpdate()
extern "C"  void GetAnimatorPlayBackSpeed_OnUpdate_m1482976847 (GetAnimatorPlayBackSpeed_t3775496753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPlayBackSpeed::GetPlayBackSpeed()
extern "C"  void GetAnimatorPlayBackSpeed_GetPlayBackSpeed_m911770741 (GetAnimatorPlayBackSpeed_t3775496753 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
