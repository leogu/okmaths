﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue
struct  uGuiSliderSetNormalizedValue_t4221362059  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::value
	FsmFloat_t937133978 * ___value_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::_slider
	Slider_t297367283 * ____slider_15;
	// System.Single HutongGames.PlayMaker.Actions.uGuiSliderSetNormalizedValue::_originalValue
	float ____originalValue_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderSetNormalizedValue_t4221362059, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiSliderSetNormalizedValue_t4221362059, ___value_12)); }
	inline FsmFloat_t937133978 * get_value_12() const { return ___value_12; }
	inline FsmFloat_t937133978 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmFloat_t937133978 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiSliderSetNormalizedValue_t4221362059, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiSliderSetNormalizedValue_t4221362059, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__slider_15() { return static_cast<int32_t>(offsetof(uGuiSliderSetNormalizedValue_t4221362059, ____slider_15)); }
	inline Slider_t297367283 * get__slider_15() const { return ____slider_15; }
	inline Slider_t297367283 ** get_address_of__slider_15() { return &____slider_15; }
	inline void set__slider_15(Slider_t297367283 * value)
	{
		____slider_15 = value;
		Il2CppCodeGenWriteBarrier(&____slider_15, value);
	}

	inline static int32_t get_offset_of__originalValue_16() { return static_cast<int32_t>(offsetof(uGuiSliderSetNormalizedValue_t4221362059, ____originalValue_16)); }
	inline float get__originalValue_16() const { return ____originalValue_16; }
	inline float* get_address_of__originalValue_16() { return &____originalValue_16; }
	inline void set__originalValue_16(float value)
	{
		____originalValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
