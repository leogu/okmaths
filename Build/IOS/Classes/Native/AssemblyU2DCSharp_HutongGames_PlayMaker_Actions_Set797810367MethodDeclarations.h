﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightColor
struct SetLightColor_t797810367;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightColor::.ctor()
extern "C"  void SetLightColor__ctor_m2738820771 (SetLightColor_t797810367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::Reset()
extern "C"  void SetLightColor_Reset_m2386352608 (SetLightColor_t797810367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::OnEnter()
extern "C"  void SetLightColor_OnEnter_m2219020382 (SetLightColor_t797810367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::OnUpdate()
extern "C"  void SetLightColor_OnUpdate_m2538468387 (SetLightColor_t797810367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightColor::DoSetLightColor()
extern "C"  void SetLightColor_DoSetLightColor_m3124768461 (SetLightColor_t797810367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
