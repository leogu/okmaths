﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PlayMakerHashTableProxy
struct PlayMakerHashTableProxy_t3073922234;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1918293222.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableActions
struct  HashTableActions_t3659156725  : public CollectionsActions_t1918293222
{
public:
	// PlayMakerHashTableProxy HutongGames.PlayMaker.Actions.HashTableActions::proxy
	PlayMakerHashTableProxy_t3073922234 * ___proxy_11;

public:
	inline static int32_t get_offset_of_proxy_11() { return static_cast<int32_t>(offsetof(HashTableActions_t3659156725, ___proxy_11)); }
	inline PlayMakerHashTableProxy_t3073922234 * get_proxy_11() const { return ___proxy_11; }
	inline PlayMakerHashTableProxy_t3073922234 ** get_address_of_proxy_11() { return &___proxy_11; }
	inline void set_proxy_11(PlayMakerHashTableProxy_t3073922234 * value)
	{
		___proxy_11 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
