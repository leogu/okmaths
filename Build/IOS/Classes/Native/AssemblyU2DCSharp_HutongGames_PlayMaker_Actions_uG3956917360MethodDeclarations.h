﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties
struct uGuiCanvasGroupSetProperties_t3956917360;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::.ctor()
extern "C"  void uGuiCanvasGroupSetProperties__ctor_m2901615042 (uGuiCanvasGroupSetProperties_t3956917360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::Reset()
extern "C"  void uGuiCanvasGroupSetProperties_Reset_m2104319257 (uGuiCanvasGroupSetProperties_t3956917360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::OnEnter()
extern "C"  void uGuiCanvasGroupSetProperties_OnEnter_m2003116977 (uGuiCanvasGroupSetProperties_t3956917360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::OnUpdate()
extern "C"  void uGuiCanvasGroupSetProperties_OnUpdate_m1922933132 (uGuiCanvasGroupSetProperties_t3956917360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::DoAction()
extern "C"  void uGuiCanvasGroupSetProperties_DoAction_m1147065325 (uGuiCanvasGroupSetProperties_t3956917360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetProperties::OnExit()
extern "C"  void uGuiCanvasGroupSetProperties_OnExit_m2441536381 (uGuiCanvasGroupSetProperties_t3956917360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
