﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugVector3
struct DebugVector3_t3987895819;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugVector3::.ctor()
extern "C"  void DebugVector3__ctor_m1607209373 (DebugVector3_t3987895819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugVector3::Reset()
extern "C"  void DebugVector3_Reset_m1420852116 (DebugVector3_t3987895819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugVector3::OnEnter()
extern "C"  void DebugVector3_OnEnter_m4188261254 (DebugVector3_t3987895819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
