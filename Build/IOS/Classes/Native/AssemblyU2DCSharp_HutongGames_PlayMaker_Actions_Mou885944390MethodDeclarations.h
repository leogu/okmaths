﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MousePickEvent
struct MousePickEvent_t885944390;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::.ctor()
extern "C"  void MousePickEvent__ctor_m2114644974 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::Reset()
extern "C"  void MousePickEvent_Reset_m1038801391 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::OnEnter()
extern "C"  void MousePickEvent_OnEnter_m338535231 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::OnUpdate()
extern "C"  void MousePickEvent_OnUpdate_m437362264 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MousePickEvent::DoMousePickEvent()
extern "C"  void MousePickEvent_DoMousePickEvent_m3750810849 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.MousePickEvent::DoRaycast()
extern "C"  bool MousePickEvent_DoRaycast_m734196572 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.MousePickEvent::ErrorCheck()
extern "C"  String_t* MousePickEvent_ErrorCheck_m415011801 (MousePickEvent_t885944390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
