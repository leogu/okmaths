﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorHumanScale
struct GetAnimatorHumanScale_t2919277580;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::.ctor()
extern "C"  void GetAnimatorHumanScale__ctor_m3844296222 (GetAnimatorHumanScale_t2919277580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::Reset()
extern "C"  void GetAnimatorHumanScale_Reset_m1877684449 (GetAnimatorHumanScale_t2919277580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::OnEnter()
extern "C"  void GetAnimatorHumanScale_OnEnter_m1742975297 (GetAnimatorHumanScale_t2919277580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorHumanScale::DoGetHumanScale()
extern "C"  void GetAnimatorHumanScale_DoGetHumanScale_m1731351956 (GetAnimatorHumanScale_t2919277580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
