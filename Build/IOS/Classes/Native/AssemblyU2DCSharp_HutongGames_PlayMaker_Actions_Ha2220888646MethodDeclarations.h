﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableKeys
struct HashTableKeys_t2220888646;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableKeys::.ctor()
extern "C"  void HashTableKeys__ctor_m2046377058 (HashTableKeys_t2220888646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableKeys::Reset()
extern "C"  void HashTableKeys_Reset_m1745406367 (HashTableKeys_t2220888646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableKeys::OnEnter()
extern "C"  void HashTableKeys_OnEnter_m286881047 (HashTableKeys_t2220888646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableKeys::doHashTableKeys()
extern "C"  void HashTableKeys_doHashTableKeys_m2882685865 (HashTableKeys_t2220888646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
