﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin
struct RectTransformSetOffsetMin_t3430680583;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::.ctor()
extern "C"  void RectTransformSetOffsetMin__ctor_m2589954083 (RectTransformSetOffsetMin_t3430680583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::Reset()
extern "C"  void RectTransformSetOffsetMin_Reset_m1412896928 (RectTransformSetOffsetMin_t3430680583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::OnEnter()
extern "C"  void RectTransformSetOffsetMin_OnEnter_m2494505910 (RectTransformSetOffsetMin_t3430680583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::OnActionUpdate()
extern "C"  void RectTransformSetOffsetMin_OnActionUpdate_m2011070589 (RectTransformSetOffsetMin_t3430680583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMin::DoSetOffsetMin()
extern "C"  void RectTransformSetOffsetMin_DoSetOffsetMin_m1140879605 (RectTransformSetOffsetMin_t3430680583 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
