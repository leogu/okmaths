﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetVertexPositions
struct ArrayListGetVertexPositions_t1995749503;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexPositions::.ctor()
extern "C"  void ArrayListGetVertexPositions__ctor_m3562645953 (ArrayListGetVertexPositions_t1995749503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexPositions::Reset()
extern "C"  void ArrayListGetVertexPositions_Reset_m4020411196 (ArrayListGetVertexPositions_t1995749503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexPositions::OnEnter()
extern "C"  void ArrayListGetVertexPositions_OnEnter_m2522650582 (ArrayListGetVertexPositions_t1995749503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetVertexPositions::getVertexPositions()
extern "C"  void ArrayListGetVertexPositions_getVertexPositions_m3954603293 (ArrayListGetVertexPositions_t1995749503 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
