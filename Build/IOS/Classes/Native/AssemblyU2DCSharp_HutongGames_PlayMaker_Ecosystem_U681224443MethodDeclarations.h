﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget
struct PlayMakerFsmVariableTarget_t681224443;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_3345575873.h"
#include "PlayMaker_PlayMakerFSM437737208.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::.ctor()
extern "C"  void PlayMakerFsmVariableTarget__ctor_m1365327241 (PlayMakerFsmVariableTarget_t681224443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::.ctor(HutongGames.PlayMaker.Ecosystem.Utils.ProxyFsmVariableTarget)
extern "C"  void PlayMakerFsmVariableTarget__ctor_m1026113847 (PlayMakerFsmVariableTarget_t681224443 * __this, int32_t ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::get_isTargetAvailable()
extern "C"  bool PlayMakerFsmVariableTarget_get_isTargetAvailable_m2492547838 (PlayMakerFsmVariableTarget_t681224443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::get_FsmVariables()
extern "C"  FsmVariables_t630687169 * PlayMakerFsmVariableTarget_get_FsmVariables_m3408788517 (PlayMakerFsmVariableTarget_t681224443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::get_fsmComponent()
extern "C"  PlayMakerFSM_t437737208 * PlayMakerFsmVariableTarget_get_fsmComponent_m1527136640 (PlayMakerFsmVariableTarget_t681224443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::set_fsmComponent(PlayMakerFSM)
extern "C"  void PlayMakerFsmVariableTarget_set_fsmComponent_m3384835891 (PlayMakerFsmVariableTarget_t681224443 * __this, PlayMakerFSM_t437737208 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::Initialize(System.Boolean)
extern "C"  void PlayMakerFsmVariableTarget_Initialize_m1909290904 (PlayMakerFsmVariableTarget_t681224443 * __this, bool ___forceRefresh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::ToString()
extern "C"  String_t* PlayMakerFsmVariableTarget_ToString_m1471310178 (PlayMakerFsmVariableTarget_t681224443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
