﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetDeviceRoll
struct GetDeviceRoll_t3058231729;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::.ctor()
extern "C"  void GetDeviceRoll__ctor_m3132056657 (GetDeviceRoll_t3058231729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::Reset()
extern "C"  void GetDeviceRoll_Reset_m1219586542 (GetDeviceRoll_t3058231729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::OnEnter()
extern "C"  void GetDeviceRoll_OnEnter_m1064399820 (GetDeviceRoll_t3058231729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::OnUpdate()
extern "C"  void GetDeviceRoll_OnUpdate_m411152405 (GetDeviceRoll_t3058231729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDeviceRoll::DoGetDeviceRoll()
extern "C"  void GetDeviceRoll_DoGetDeviceRoll_m4133049377 (GetDeviceRoll_t3058231729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
