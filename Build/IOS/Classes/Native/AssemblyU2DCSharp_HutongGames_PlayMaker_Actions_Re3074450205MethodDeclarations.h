﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax
struct RectTransformSetOffsetMax_t3074450205;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::.ctor()
extern "C"  void RectTransformSetOffsetMax__ctor_m507134053 (RectTransformSetOffsetMax_t3074450205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::Reset()
extern "C"  void RectTransformSetOffsetMax_Reset_m3600961978 (RectTransformSetOffsetMax_t3074450205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::OnEnter()
extern "C"  void RectTransformSetOffsetMax_OnEnter_m1157023544 (RectTransformSetOffsetMax_t3074450205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::OnActionUpdate()
extern "C"  void RectTransformSetOffsetMax_OnActionUpdate_m2257418099 (RectTransformSetOffsetMax_t3074450205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetOffsetMax::DoSetOffsetMax()
extern "C"  void RectTransformSetOffsetMax_DoSetOffsetMax_m2483386485 (RectTransformSetOffsetMax_t3074450205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
