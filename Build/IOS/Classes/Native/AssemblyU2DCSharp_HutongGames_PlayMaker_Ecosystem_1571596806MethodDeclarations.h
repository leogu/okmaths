﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct PlayMakerEvent_t1571596806;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_2104288969.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::.ctor()
extern "C"  void PlayMakerEvent__ctor_m1139841202 (PlayMakerEvent_t1571596806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::.ctor(System.String)
extern "C"  void PlayMakerEvent__ctor_m116535804 (PlayMakerEvent_t1571596806 * __this, String_t* ___defaultEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::SendEvent(PlayMakerFSM,HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget)
extern "C"  bool PlayMakerEvent_SendEvent_m2431361578 (PlayMakerEvent_t1571596806 * __this, PlayMakerFSM_t437737208 * ___fromFsm0, PlayMakerEventTarget_t2104288969 * ___eventTarget1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::ToString()
extern "C"  String_t* PlayMakerEvent_ToString_m990115885 (PlayMakerEvent_t1571596806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
