﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerParticleCollision
struct PlayMakerParticleCollision_t1230092432;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void PlayMakerParticleCollision::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void PlayMakerParticleCollision_OnParticleCollision_m2510204134 (PlayMakerParticleCollision_t1230092432 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerParticleCollision::.ctor()
extern "C"  void PlayMakerParticleCollision__ctor_m2323843317 (PlayMakerParticleCollision_t1230092432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
