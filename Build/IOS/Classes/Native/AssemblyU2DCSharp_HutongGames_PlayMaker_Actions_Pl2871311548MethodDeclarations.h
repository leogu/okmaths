﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsSetString
struct PlayerPrefsSetString_t2871311548;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetString::.ctor()
extern "C"  void PlayerPrefsSetString__ctor_m860027578 (PlayerPrefsSetString_t2871311548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetString::Reset()
extern "C"  void PlayerPrefsSetString_Reset_m3429204425 (PlayerPrefsSetString_t2871311548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsSetString::OnEnter()
extern "C"  void PlayerPrefsSetString_OnEnter_m2488920569 (PlayerPrefsSetString_t2871311548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
