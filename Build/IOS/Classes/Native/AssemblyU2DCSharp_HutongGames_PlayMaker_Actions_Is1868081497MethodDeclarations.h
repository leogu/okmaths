﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsKinematic
struct IsKinematic_t1868081497;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsKinematic::.ctor()
extern "C"  void IsKinematic__ctor_m3659739567 (IsKinematic_t1868081497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::Reset()
extern "C"  void IsKinematic_Reset_m1818460434 (IsKinematic_t1868081497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::OnEnter()
extern "C"  void IsKinematic_OnEnter_m3761232948 (IsKinematic_t1868081497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::OnUpdate()
extern "C"  void IsKinematic_OnUpdate_m3818180799 (IsKinematic_t1868081497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsKinematic::DoIsKinematic()
extern "C"  void IsKinematic_DoIsKinematic_m2752388589 (IsKinematic_t1868081497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
