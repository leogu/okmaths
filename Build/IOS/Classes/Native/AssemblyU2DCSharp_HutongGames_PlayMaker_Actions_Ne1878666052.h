﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix
struct  NetworkSetLevelPrefix_t1878666052  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix::levelPrefix
	FsmInt_t1273009179 * ___levelPrefix_11;

public:
	inline static int32_t get_offset_of_levelPrefix_11() { return static_cast<int32_t>(offsetof(NetworkSetLevelPrefix_t1878666052, ___levelPrefix_11)); }
	inline FsmInt_t1273009179 * get_levelPrefix_11() const { return ___levelPrefix_11; }
	inline FsmInt_t1273009179 ** get_address_of_levelPrefix_11() { return &___levelPrefix_11; }
	inline void set_levelPrefix_11(FsmInt_t1273009179 * value)
	{
		___levelPrefix_11 = value;
		Il2CppCodeGenWriteBarrier(&___levelPrefix_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
