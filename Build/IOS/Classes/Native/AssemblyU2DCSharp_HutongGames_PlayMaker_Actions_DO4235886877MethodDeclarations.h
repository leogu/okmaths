﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraNearClipPlane
struct DOTweenCameraNearClipPlane_t4235886877;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraNearClipPlane::.ctor()
extern "C"  void DOTweenCameraNearClipPlane__ctor_m985842069 (DOTweenCameraNearClipPlane_t4235886877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraNearClipPlane::Reset()
extern "C"  void DOTweenCameraNearClipPlane_Reset_m188836454 (DOTweenCameraNearClipPlane_t4235886877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraNearClipPlane::OnEnter()
extern "C"  void DOTweenCameraNearClipPlane_OnEnter_m1195441644 (DOTweenCameraNearClipPlane_t4235886877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraNearClipPlane::<OnEnter>m__2A()
extern "C"  void DOTweenCameraNearClipPlane_U3COnEnterU3Em__2A_m1696565850 (DOTweenCameraNearClipPlane_t4235886877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraNearClipPlane::<OnEnter>m__2B()
extern "C"  void DOTweenCameraNearClipPlane_U3COnEnterU3Em__2B_m433009887 (DOTweenCameraNearClipPlane_t4235886877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
