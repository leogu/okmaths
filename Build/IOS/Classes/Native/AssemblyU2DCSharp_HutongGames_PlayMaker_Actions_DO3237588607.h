﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// DG.Tweening.Sequence
struct Sequence_t110643099;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump
struct  DOTweenTransformLocalJump_t3237588607  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::to
	FsmVector3_t3996534004 * ___to_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::setRelative
	FsmBool_t664485696 * ___setRelative_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::snapping
	FsmBool_t664485696 * ___snapping_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::jumpPower
	FsmFloat_t937133978 * ___jumpPower_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::numJumps
	FsmInt_t1273009179 * ___numJumps_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::duration
	FsmFloat_t937133978 * ___duration_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::startDelay
	FsmFloat_t937133978 * ___startDelay_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::playInReverse
	FsmBool_t664485696 * ___playInReverse_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::setReverseRelative
	FsmBool_t664485696 * ___setReverseRelative_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::startEvent
	FsmEvent_t1258573736 * ___startEvent_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_23;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::tweenIdDescription
	String_t* ___tweenIdDescription_24;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::tweenIdType
	int32_t ___tweenIdType_25;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::stringAsId
	FsmString_t2414474701 * ___stringAsId_26;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::tagAsId
	FsmString_t2414474701 * ___tagAsId_27;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::selectedEase
	int32_t ___selectedEase_28;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::easeType
	int32_t ___easeType_29;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_30;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_31;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::loops
	FsmInt_t1273009179 * ___loops_32;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::loopType
	int32_t ___loopType_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::recyclable
	FsmBool_t664485696 * ___recyclable_35;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::updateType
	int32_t ___updateType_36;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_37;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::debugThis
	FsmBool_t664485696 * ___debugThis_38;
	// DG.Tweening.Sequence HutongGames.PlayMaker.Actions.DOTweenTransformLocalJump::sequence
	Sequence_t110643099 * ___sequence_39;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_to_12() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___to_12)); }
	inline FsmVector3_t3996534004 * get_to_12() const { return ___to_12; }
	inline FsmVector3_t3996534004 ** get_address_of_to_12() { return &___to_12; }
	inline void set_to_12(FsmVector3_t3996534004 * value)
	{
		___to_12 = value;
		Il2CppCodeGenWriteBarrier(&___to_12, value);
	}

	inline static int32_t get_offset_of_setRelative_13() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___setRelative_13)); }
	inline FsmBool_t664485696 * get_setRelative_13() const { return ___setRelative_13; }
	inline FsmBool_t664485696 ** get_address_of_setRelative_13() { return &___setRelative_13; }
	inline void set_setRelative_13(FsmBool_t664485696 * value)
	{
		___setRelative_13 = value;
		Il2CppCodeGenWriteBarrier(&___setRelative_13, value);
	}

	inline static int32_t get_offset_of_snapping_14() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___snapping_14)); }
	inline FsmBool_t664485696 * get_snapping_14() const { return ___snapping_14; }
	inline FsmBool_t664485696 ** get_address_of_snapping_14() { return &___snapping_14; }
	inline void set_snapping_14(FsmBool_t664485696 * value)
	{
		___snapping_14 = value;
		Il2CppCodeGenWriteBarrier(&___snapping_14, value);
	}

	inline static int32_t get_offset_of_jumpPower_15() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___jumpPower_15)); }
	inline FsmFloat_t937133978 * get_jumpPower_15() const { return ___jumpPower_15; }
	inline FsmFloat_t937133978 ** get_address_of_jumpPower_15() { return &___jumpPower_15; }
	inline void set_jumpPower_15(FsmFloat_t937133978 * value)
	{
		___jumpPower_15 = value;
		Il2CppCodeGenWriteBarrier(&___jumpPower_15, value);
	}

	inline static int32_t get_offset_of_numJumps_16() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___numJumps_16)); }
	inline FsmInt_t1273009179 * get_numJumps_16() const { return ___numJumps_16; }
	inline FsmInt_t1273009179 ** get_address_of_numJumps_16() { return &___numJumps_16; }
	inline void set_numJumps_16(FsmInt_t1273009179 * value)
	{
		___numJumps_16 = value;
		Il2CppCodeGenWriteBarrier(&___numJumps_16, value);
	}

	inline static int32_t get_offset_of_duration_17() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___duration_17)); }
	inline FsmFloat_t937133978 * get_duration_17() const { return ___duration_17; }
	inline FsmFloat_t937133978 ** get_address_of_duration_17() { return &___duration_17; }
	inline void set_duration_17(FsmFloat_t937133978 * value)
	{
		___duration_17 = value;
		Il2CppCodeGenWriteBarrier(&___duration_17, value);
	}

	inline static int32_t get_offset_of_startDelay_18() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___startDelay_18)); }
	inline FsmFloat_t937133978 * get_startDelay_18() const { return ___startDelay_18; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_18() { return &___startDelay_18; }
	inline void set_startDelay_18(FsmFloat_t937133978 * value)
	{
		___startDelay_18 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_18, value);
	}

	inline static int32_t get_offset_of_playInReverse_19() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___playInReverse_19)); }
	inline FsmBool_t664485696 * get_playInReverse_19() const { return ___playInReverse_19; }
	inline FsmBool_t664485696 ** get_address_of_playInReverse_19() { return &___playInReverse_19; }
	inline void set_playInReverse_19(FsmBool_t664485696 * value)
	{
		___playInReverse_19 = value;
		Il2CppCodeGenWriteBarrier(&___playInReverse_19, value);
	}

	inline static int32_t get_offset_of_setReverseRelative_20() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___setReverseRelative_20)); }
	inline FsmBool_t664485696 * get_setReverseRelative_20() const { return ___setReverseRelative_20; }
	inline FsmBool_t664485696 ** get_address_of_setReverseRelative_20() { return &___setReverseRelative_20; }
	inline void set_setReverseRelative_20(FsmBool_t664485696 * value)
	{
		___setReverseRelative_20 = value;
		Il2CppCodeGenWriteBarrier(&___setReverseRelative_20, value);
	}

	inline static int32_t get_offset_of_startEvent_21() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___startEvent_21)); }
	inline FsmEvent_t1258573736 * get_startEvent_21() const { return ___startEvent_21; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_21() { return &___startEvent_21; }
	inline void set_startEvent_21(FsmEvent_t1258573736 * value)
	{
		___startEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_21, value);
	}

	inline static int32_t get_offset_of_finishEvent_22() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___finishEvent_22)); }
	inline FsmEvent_t1258573736 * get_finishEvent_22() const { return ___finishEvent_22; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_22() { return &___finishEvent_22; }
	inline void set_finishEvent_22(FsmEvent_t1258573736 * value)
	{
		___finishEvent_22 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_22, value);
	}

	inline static int32_t get_offset_of_finishImmediately_23() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___finishImmediately_23)); }
	inline FsmBool_t664485696 * get_finishImmediately_23() const { return ___finishImmediately_23; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_23() { return &___finishImmediately_23; }
	inline void set_finishImmediately_23(FsmBool_t664485696 * value)
	{
		___finishImmediately_23 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_23, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_24() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___tweenIdDescription_24)); }
	inline String_t* get_tweenIdDescription_24() const { return ___tweenIdDescription_24; }
	inline String_t** get_address_of_tweenIdDescription_24() { return &___tweenIdDescription_24; }
	inline void set_tweenIdDescription_24(String_t* value)
	{
		___tweenIdDescription_24 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_24, value);
	}

	inline static int32_t get_offset_of_tweenIdType_25() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___tweenIdType_25)); }
	inline int32_t get_tweenIdType_25() const { return ___tweenIdType_25; }
	inline int32_t* get_address_of_tweenIdType_25() { return &___tweenIdType_25; }
	inline void set_tweenIdType_25(int32_t value)
	{
		___tweenIdType_25 = value;
	}

	inline static int32_t get_offset_of_stringAsId_26() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___stringAsId_26)); }
	inline FsmString_t2414474701 * get_stringAsId_26() const { return ___stringAsId_26; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_26() { return &___stringAsId_26; }
	inline void set_stringAsId_26(FsmString_t2414474701 * value)
	{
		___stringAsId_26 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_26, value);
	}

	inline static int32_t get_offset_of_tagAsId_27() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___tagAsId_27)); }
	inline FsmString_t2414474701 * get_tagAsId_27() const { return ___tagAsId_27; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_27() { return &___tagAsId_27; }
	inline void set_tagAsId_27(FsmString_t2414474701 * value)
	{
		___tagAsId_27 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_27, value);
	}

	inline static int32_t get_offset_of_selectedEase_28() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___selectedEase_28)); }
	inline int32_t get_selectedEase_28() const { return ___selectedEase_28; }
	inline int32_t* get_address_of_selectedEase_28() { return &___selectedEase_28; }
	inline void set_selectedEase_28(int32_t value)
	{
		___selectedEase_28 = value;
	}

	inline static int32_t get_offset_of_easeType_29() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___easeType_29)); }
	inline int32_t get_easeType_29() const { return ___easeType_29; }
	inline int32_t* get_address_of_easeType_29() { return &___easeType_29; }
	inline void set_easeType_29(int32_t value)
	{
		___easeType_29 = value;
	}

	inline static int32_t get_offset_of_animationCurve_30() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___animationCurve_30)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_30() const { return ___animationCurve_30; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_30() { return &___animationCurve_30; }
	inline void set_animationCurve_30(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_30 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_30, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_31() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___loopsDescriptionArea_31)); }
	inline String_t* get_loopsDescriptionArea_31() const { return ___loopsDescriptionArea_31; }
	inline String_t** get_address_of_loopsDescriptionArea_31() { return &___loopsDescriptionArea_31; }
	inline void set_loopsDescriptionArea_31(String_t* value)
	{
		___loopsDescriptionArea_31 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_31, value);
	}

	inline static int32_t get_offset_of_loops_32() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___loops_32)); }
	inline FsmInt_t1273009179 * get_loops_32() const { return ___loops_32; }
	inline FsmInt_t1273009179 ** get_address_of_loops_32() { return &___loops_32; }
	inline void set_loops_32(FsmInt_t1273009179 * value)
	{
		___loops_32 = value;
		Il2CppCodeGenWriteBarrier(&___loops_32, value);
	}

	inline static int32_t get_offset_of_loopType_33() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___loopType_33)); }
	inline int32_t get_loopType_33() const { return ___loopType_33; }
	inline int32_t* get_address_of_loopType_33() { return &___loopType_33; }
	inline void set_loopType_33(int32_t value)
	{
		___loopType_33 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_34() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___autoKillOnCompletion_34)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_34() const { return ___autoKillOnCompletion_34; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_34() { return &___autoKillOnCompletion_34; }
	inline void set_autoKillOnCompletion_34(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_34 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_34, value);
	}

	inline static int32_t get_offset_of_recyclable_35() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___recyclable_35)); }
	inline FsmBool_t664485696 * get_recyclable_35() const { return ___recyclable_35; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_35() { return &___recyclable_35; }
	inline void set_recyclable_35(FsmBool_t664485696 * value)
	{
		___recyclable_35 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_35, value);
	}

	inline static int32_t get_offset_of_updateType_36() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___updateType_36)); }
	inline int32_t get_updateType_36() const { return ___updateType_36; }
	inline int32_t* get_address_of_updateType_36() { return &___updateType_36; }
	inline void set_updateType_36(int32_t value)
	{
		___updateType_36 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_37() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___isIndependentUpdate_37)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_37() const { return ___isIndependentUpdate_37; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_37() { return &___isIndependentUpdate_37; }
	inline void set_isIndependentUpdate_37(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_37 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_37, value);
	}

	inline static int32_t get_offset_of_debugThis_38() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___debugThis_38)); }
	inline FsmBool_t664485696 * get_debugThis_38() const { return ___debugThis_38; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_38() { return &___debugThis_38; }
	inline void set_debugThis_38(FsmBool_t664485696 * value)
	{
		___debugThis_38 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_38, value);
	}

	inline static int32_t get_offset_of_sequence_39() { return static_cast<int32_t>(offsetof(DOTweenTransformLocalJump_t3237588607, ___sequence_39)); }
	inline Sequence_t110643099 * get_sequence_39() const { return ___sequence_39; }
	inline Sequence_t110643099 ** get_address_of_sequence_39() { return &___sequence_39; }
	inline void set_sequence_39(Sequence_t110643099 * value)
	{
		___sequence_39 = value;
		Il2CppCodeGenWriteBarrier(&___sequence_39, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
