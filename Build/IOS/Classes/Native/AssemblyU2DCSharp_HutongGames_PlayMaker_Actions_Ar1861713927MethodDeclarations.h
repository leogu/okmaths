﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject
struct ArrayListGetClosestGameObject_t1861713927;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::.ctor()
extern "C"  void ArrayListGetClosestGameObject__ctor_m2004406777 (ArrayListGetClosestGameObject_t1861713927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::Reset()
extern "C"  void ArrayListGetClosestGameObject_Reset_m4153379204 (ArrayListGetClosestGameObject_t1861713927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::OnEnter()
extern "C"  void ArrayListGetClosestGameObject_OnEnter_m4060064078 (ArrayListGetClosestGameObject_t1861713927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::OnUpdate()
extern "C"  void ArrayListGetClosestGameObject_OnUpdate_m1974138709 (ArrayListGetClosestGameObject_t1861713927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::DoFindClosestGo()
extern "C"  void ArrayListGetClosestGameObject_DoFindClosestGo_m4083689540 (ArrayListGetClosestGameObject_t1861713927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
