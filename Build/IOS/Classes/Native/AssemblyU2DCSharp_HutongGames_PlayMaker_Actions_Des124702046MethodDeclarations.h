﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyObjects
struct DestroyObjects_t124702046;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyObjects::.ctor()
extern "C"  void DestroyObjects__ctor_m1100891446 (DestroyObjects_t124702046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObjects::Reset()
extern "C"  void DestroyObjects_Reset_m2548621959 (DestroyObjects_t124702046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyObjects::OnEnter()
extern "C"  void DestroyObjects_OnEnter_m581152455 (DestroyObjects_t124702046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
