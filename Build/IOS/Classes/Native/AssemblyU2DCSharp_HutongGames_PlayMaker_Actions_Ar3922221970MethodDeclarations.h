﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex
struct ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::.ctor()
extern "C"  void ArrayListGetGameobjectMaxFsmFloatIndex__ctor_m1973378124 (ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::Reset()
extern "C"  void ArrayListGetGameobjectMaxFsmFloatIndex_Reset_m3420827675 (ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::OnEnter()
extern "C"  void ArrayListGetGameobjectMaxFsmFloatIndex_OnEnter_m2988349651 (ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::OnUpdate()
extern "C"  void ArrayListGetGameobjectMaxFsmFloatIndex_OnUpdate_m507810826 (ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::DoFindMaxGo()
extern "C"  void ArrayListGetGameobjectMaxFsmFloatIndex_DoFindMaxGo_m2033122382 (ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
