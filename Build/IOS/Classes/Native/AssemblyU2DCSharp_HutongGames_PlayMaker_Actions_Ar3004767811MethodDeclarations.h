﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight
struct ArrayListGetFarthestGameObjectInSight_t3004767811;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::.ctor()
extern "C"  void ArrayListGetFarthestGameObjectInSight__ctor_m3599440861 (ArrayListGetFarthestGameObjectInSight_t3004767811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::Reset()
extern "C"  void ArrayListGetFarthestGameObjectInSight_Reset_m145309536 (ArrayListGetFarthestGameObjectInSight_t3004767811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::OnEnter()
extern "C"  void ArrayListGetFarthestGameObjectInSight_OnEnter_m3815557914 (ArrayListGetFarthestGameObjectInSight_t3004767811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::OnUpdate()
extern "C"  void ArrayListGetFarthestGameObjectInSight_OnUpdate_m1571905721 (ArrayListGetFarthestGameObjectInSight_t3004767811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::DoFindFarthestGo()
extern "C"  void ArrayListGetFarthestGameObjectInSight_DoFindFarthestGo_m983662152 (ArrayListGetFarthestGameObjectInSight_t3004767811 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetFarthestGameObjectInSight::DoLineCast(UnityEngine.GameObject)
extern "C"  bool ArrayListGetFarthestGameObjectInSight_DoLineCast_m2320210809 (ArrayListGetFarthestGameObjectInSight_t3004767811 * __this, GameObject_t1756533147 * ___toGameObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
