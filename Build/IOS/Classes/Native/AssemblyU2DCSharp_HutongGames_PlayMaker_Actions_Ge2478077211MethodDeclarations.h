﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAtan2FromVector2
struct GetAtan2FromVector2_t2478077211;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::.ctor()
extern "C"  void GetAtan2FromVector2__ctor_m2287476133 (GetAtan2FromVector2_t2478077211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::Reset()
extern "C"  void GetAtan2FromVector2_Reset_m3104233848 (GetAtan2FromVector2_t2478077211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::OnEnter()
extern "C"  void GetAtan2FromVector2_OnEnter_m2146488290 (GetAtan2FromVector2_t2478077211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::OnUpdate()
extern "C"  void GetAtan2FromVector2_OnUpdate_m1210802081 (GetAtan2FromVector2_t2478077211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAtan2FromVector2::DoATan()
extern "C"  void GetAtan2FromVector2_DoATan_m1909649524 (GetAtan2FromVector2_t2478077211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
