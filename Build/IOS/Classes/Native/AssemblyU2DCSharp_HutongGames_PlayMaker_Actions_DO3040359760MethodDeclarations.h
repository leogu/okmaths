﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyRotate
struct DOTweenRigidbodyRotate_t3040359760;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyRotate::.ctor()
extern "C"  void DOTweenRigidbodyRotate__ctor_m548866552 (DOTweenRigidbodyRotate_t3040359760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyRotate::Reset()
extern "C"  void DOTweenRigidbodyRotate_Reset_m2269473785 (DOTweenRigidbodyRotate_t3040359760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyRotate::OnEnter()
extern "C"  void DOTweenRigidbodyRotate_OnEnter_m3183954169 (DOTweenRigidbodyRotate_t3040359760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyRotate::<OnEnter>m__90()
extern "C"  void DOTweenRigidbodyRotate_U3COnEnterU3Em__90_m1707680251 (DOTweenRigidbodyRotate_t3040359760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyRotate::<OnEnter>m__91()
extern "C"  void DOTweenRigidbodyRotate_U3COnEnterU3Em__91_m161799286 (DOTweenRigidbodyRotate_t3040359760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
