﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionEnter
struct PlayMakerCollisionEnter_t204200476;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void PlayMakerCollisionEnter::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionEnter_OnCollisionEnter_m40305123 (PlayMakerCollisionEnter_t204200476 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionEnter::.ctor()
extern "C"  void PlayMakerCollisionEnter__ctor_m3411485885 (PlayMakerCollisionEnter_t204200476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
