﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder
struct  uGuiInputFieldGetPlaceHolder_t2503493821  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::placeHolder
	FsmGameObject_t3097142863 * ___placeHolder_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::placeHolderDefined
	FsmBool_t664485696 * ___placeHolderDefined_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::foundEvent
	FsmEvent_t1258573736 * ___foundEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::notFoundEvent
	FsmEvent_t1258573736 * ___notFoundEvent_15;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetPlaceHolder::_inputField
	InputField_t1631627530 * ____inputField_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetPlaceHolder_t2503493821, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_placeHolder_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetPlaceHolder_t2503493821, ___placeHolder_12)); }
	inline FsmGameObject_t3097142863 * get_placeHolder_12() const { return ___placeHolder_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_placeHolder_12() { return &___placeHolder_12; }
	inline void set_placeHolder_12(FsmGameObject_t3097142863 * value)
	{
		___placeHolder_12 = value;
		Il2CppCodeGenWriteBarrier(&___placeHolder_12, value);
	}

	inline static int32_t get_offset_of_placeHolderDefined_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetPlaceHolder_t2503493821, ___placeHolderDefined_13)); }
	inline FsmBool_t664485696 * get_placeHolderDefined_13() const { return ___placeHolderDefined_13; }
	inline FsmBool_t664485696 ** get_address_of_placeHolderDefined_13() { return &___placeHolderDefined_13; }
	inline void set_placeHolderDefined_13(FsmBool_t664485696 * value)
	{
		___placeHolderDefined_13 = value;
		Il2CppCodeGenWriteBarrier(&___placeHolderDefined_13, value);
	}

	inline static int32_t get_offset_of_foundEvent_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetPlaceHolder_t2503493821, ___foundEvent_14)); }
	inline FsmEvent_t1258573736 * get_foundEvent_14() const { return ___foundEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_foundEvent_14() { return &___foundEvent_14; }
	inline void set_foundEvent_14(FsmEvent_t1258573736 * value)
	{
		___foundEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___foundEvent_14, value);
	}

	inline static int32_t get_offset_of_notFoundEvent_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetPlaceHolder_t2503493821, ___notFoundEvent_15)); }
	inline FsmEvent_t1258573736 * get_notFoundEvent_15() const { return ___notFoundEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_notFoundEvent_15() { return &___notFoundEvent_15; }
	inline void set_notFoundEvent_15(FsmEvent_t1258573736 * value)
	{
		___notFoundEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___notFoundEvent_15, value);
	}

	inline static int32_t get_offset_of__inputField_16() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetPlaceHolder_t2503493821, ____inputField_16)); }
	inline InputField_t1631627530 * get__inputField_16() const { return ____inputField_16; }
	inline InputField_t1631627530 ** get_address_of__inputField_16() { return &____inputField_16; }
	inline void set__inputField_16(InputField_t1631627530 * value)
	{
		____inputField_16 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
