﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableContainsKey
struct HashTableContainsKey_t2348818626;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableContainsKey::.ctor()
extern "C"  void HashTableContainsKey__ctor_m4145145608 (HashTableContainsKey_t2348818626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContainsKey::Reset()
extern "C"  void HashTableContainsKey_Reset_m2697644143 (HashTableContainsKey_t2348818626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContainsKey::OnEnter()
extern "C"  void HashTableContainsKey_OnEnter_m3248372479 (HashTableContainsKey_t2348818626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableContainsKey::checkIfContainsKey()
extern "C"  void HashTableContainsKey_checkIfContainsKey_m3636782043 (HashTableContainsKey_t2348818626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
