﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter
struct SetAnimatorLayersAffectMassCenter_t195665531;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::.ctor()
extern "C"  void SetAnimatorLayersAffectMassCenter__ctor_m3811226727 (SetAnimatorLayersAffectMassCenter_t195665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::Reset()
extern "C"  void SetAnimatorLayersAffectMassCenter_Reset_m3510256796 (SetAnimatorLayersAffectMassCenter_t195665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::OnEnter()
extern "C"  void SetAnimatorLayersAffectMassCenter_OnEnter_m2691460330 (SetAnimatorLayersAffectMassCenter_t195665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorLayersAffectMassCenter::SetAffectMassCenter()
extern "C"  void SetAnimatorLayersAffectMassCenter_SetAffectMassCenter_m2768642441 (SetAnimatorLayersAffectMassCenter_t195665531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
