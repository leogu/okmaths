﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiGraphicSetColor
struct uGuiGraphicSetColor_t2830131109;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::.ctor()
extern "C"  void uGuiGraphicSetColor__ctor_m3570122725 (uGuiGraphicSetColor_t2830131109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::Reset()
extern "C"  void uGuiGraphicSetColor_Reset_m842782106 (uGuiGraphicSetColor_t2830131109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::OnEnter()
extern "C"  void uGuiGraphicSetColor_OnEnter_m3453890576 (uGuiGraphicSetColor_t2830131109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::OnUpdate()
extern "C"  void uGuiGraphicSetColor_OnUpdate_m2085311905 (uGuiGraphicSetColor_t2830131109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::DoSetColorValue()
extern "C"  void uGuiGraphicSetColor_DoSetColorValue_m2897975198 (uGuiGraphicSetColor_t2830131109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGraphicSetColor::OnExit()
extern "C"  void uGuiGraphicSetColor_OnExit_m2429613724 (uGuiGraphicSetColor_t2830131109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
