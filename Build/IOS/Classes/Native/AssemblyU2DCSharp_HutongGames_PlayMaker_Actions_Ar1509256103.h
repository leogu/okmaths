﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag
struct  ArrayListFindGameObjectsByTag_t1509256103  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListFindGameObjectsByTag::tag
	FsmString_t2414474701 * ___tag_14;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListFindGameObjectsByTag_t1509256103, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListFindGameObjectsByTag_t1509256103, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_tag_14() { return static_cast<int32_t>(offsetof(ArrayListFindGameObjectsByTag_t1509256103, ___tag_14)); }
	inline FsmString_t2414474701 * get_tag_14() const { return ___tag_14; }
	inline FsmString_t2414474701 ** get_address_of_tag_14() { return &___tag_14; }
	inline void set_tag_14(FsmString_t2414474701 * value)
	{
		___tag_14 = value;
		Il2CppCodeGenWriteBarrier(&___tag_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
