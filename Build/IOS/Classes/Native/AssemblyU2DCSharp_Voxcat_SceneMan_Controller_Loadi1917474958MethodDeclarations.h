﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Voxcat.SceneMan.Controller.LoadingSceneController
struct LoadingSceneController_t1917474958;

#include "codegen/il2cpp-codegen.h"

// System.Void Voxcat.SceneMan.Controller.LoadingSceneController::.ctor()
extern "C"  void LoadingSceneController__ctor_m2086779514 (LoadingSceneController_t1917474958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Voxcat.SceneMan.Controller.LoadingSceneController::LoadScene()
extern "C"  void LoadingSceneController_LoadScene_m3981768374 (LoadingSceneController_t1917474958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
