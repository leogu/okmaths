﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetDistance
struct GetDistance_t829290575;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetDistance::.ctor()
extern "C"  void GetDistance__ctor_m2871168655 (GetDistance_t829290575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDistance::Reset()
extern "C"  void GetDistance_Reset_m1753566180 (GetDistance_t829290575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDistance::OnEnter()
extern "C"  void GetDistance_OnEnter_m346784186 (GetDistance_t829290575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDistance::OnUpdate()
extern "C"  void GetDistance_OnUpdate_m3597038551 (GetDistance_t829290575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetDistance::DoGetDistance()
extern "C"  void GetDistance_DoGetDistance_m999552221 (GetDistance_t829290575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
