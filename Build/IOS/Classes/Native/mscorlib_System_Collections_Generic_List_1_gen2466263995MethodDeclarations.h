﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::.ctor()
#define List_1__ctor_m2345916416(__this, method) ((  void (*) (List_1_t2466263995 *, const MethodInfo*))List_1__ctor_m365405030_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m586319554(__this, ___collection0, method) ((  void (*) (List_1_t2466263995 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1612406893_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::.ctor(System.Int32)
#define List_1__ctor_m211815424(__this, ___capacity0, method) ((  void (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1__ctor_m136460305_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::.cctor()
#define List_1__cctor_m2421025560(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m138621019_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m917982975(__this, method) ((  Il2CppObject* (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m154161632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m1131903123(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2466263995 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2020941110_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1001200542(__this, method) ((  Il2CppObject * (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3552870393_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2267230459(__this, ___item0, method) ((  int32_t (*) (List_1_t2466263995 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1765626550_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m3212484843(__this, ___item0, method) ((  bool (*) (List_1_t2466263995 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m149594880_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1856655465(__this, ___item0, method) ((  int32_t (*) (List_1_t2466263995 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m406088260_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m2824493916(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2466263995 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3961795241_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m2495844482(__this, ___item0, method) ((  void (*) (List_1_t2466263995 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3415450529_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2540589906(__this, method) ((  bool (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2131934397_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2923756051(__this, method) ((  bool (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m418560222_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m528457283(__this, method) ((  Il2CppObject * (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1594235606_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1791763752(__this, method) ((  bool (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2120144013_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2410552951(__this, method) ((  bool (*) (List_1_t2466263995 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m257950146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1123200156(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m936612973_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m1771114529(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2466263995 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m162109184_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Add(T)
#define List_1_Add_m1303823236(__this, ___item0, method) ((  void (*) (List_1_t2466263995 *, FsmGameObject_t3097142863 *, const MethodInfo*))List_1_Add_m567051994_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m660781487(__this, ___newCount0, method) ((  void (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m185971996_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3369884032(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2466263995 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1904903857_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3843055375(__this, ___collection0, method) ((  void (*) (List_1_t2466263995 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m1580067148_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m1799592383(__this, ___enumerable0, method) ((  void (*) (List_1_t2466263995 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2489692396_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3204301094(__this, ___collection0, method) ((  void (*) (List_1_t2466263995 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3614127065_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::AsReadOnly()
#define List_1_AsReadOnly_m1853542703(__this, method) ((  ReadOnlyCollection_1_t3282928555 * (*) (List_1_t2466263995 *, const MethodInfo*))List_1_AsReadOnly_m2563000362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Clear()
#define List_1_Clear_m2483040172(__this, method) ((  void (*) (List_1_t2466263995 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Contains(T)
#define List_1_Contains_m2111680406(__this, ___item0, method) ((  bool (*) (List_1_t2466263995 *, FsmGameObject_t3097142863 *, const MethodInfo*))List_1_Contains_m2577748987_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m3019888068(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2466263995 *, FsmGameObjectU5BU5D_t3601875862*, int32_t, const MethodInfo*))List_1_CopyTo_m1758262197_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Find(System.Predicate`1<T>)
#define List_1_Find_m791692024(__this, ___match0, method) ((  FsmGameObject_t3097142863 * (*) (List_1_t2466263995 *, Predicate_1_t1540112978 *, const MethodInfo*))List_1_Find_m1725159095_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1308043659(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1540112978 *, const MethodInfo*))List_1_CheckMatch_m1196994270_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m99196568(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2466263995 *, int32_t, int32_t, Predicate_1_t1540112978 *, const MethodInfo*))List_1_GetIndex_m3409004147_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::GetEnumerator()
#define List_1_GetEnumerator_m3024270061(__this, method) ((  Enumerator_t2000993669  (*) (List_1_t2466263995 *, const MethodInfo*))List_1_GetEnumerator_m3294992758_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::IndexOf(T)
#define List_1_IndexOf_m659803394(__this, ___item0, method) ((  int32_t (*) (List_1_t2466263995 *, FsmGameObject_t3097142863 *, const MethodInfo*))List_1_IndexOf_m2070479489_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3447353415(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2466263995 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3137156970_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m3502821684(__this, ___index0, method) ((  void (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1_CheckIndex_m524615377_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Insert(System.Int32,T)
#define List_1_Insert_m3928411277(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2466263995 *, int32_t, FsmGameObject_t3097142863 *, const MethodInfo*))List_1_Insert_m283311118_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1015510490(__this, ___collection0, method) ((  void (*) (List_1_t2466263995 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3968030679_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Remove(T)
#define List_1_Remove_m2874067717(__this, ___item0, method) ((  bool (*) (List_1_t2466263995 *, FsmGameObject_t3097142863 *, const MethodInfo*))List_1_Remove_m1271859478_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1000860667(__this, ___match0, method) ((  int32_t (*) (List_1_t2466263995 *, Predicate_1_t1540112978 *, const MethodInfo*))List_1_RemoveAll_m2972055270_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3371609297(__this, ___index0, method) ((  void (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2639322385_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m1352588336(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2466263995 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3877627853_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Reverse()
#define List_1_Reverse_m1309115927(__this, method) ((  void (*) (List_1_t2466263995 *, const MethodInfo*))List_1_Reverse_m3280715839_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Sort()
#define List_1_Sort_m655568221(__this, method) ((  void (*) (List_1_t2466263995 *, const MethodInfo*))List_1_Sort_m1888745365_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m1747060096(__this, ___comparison0, method) ((  void (*) (List_1_t2466263995 *, Comparison_1_t63914418 *, const MethodInfo*))List_1_Sort_m785723827_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::ToArray()
#define List_1_ToArray_m2430552744(__this, method) ((  FsmGameObjectU5BU5D_t3601875862* (*) (List_1_t2466263995 *, const MethodInfo*))List_1_ToArray_m3072408725_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::TrimExcess()
#define List_1_TrimExcess_m2893166934(__this, method) ((  void (*) (List_1_t2466263995 *, const MethodInfo*))List_1_TrimExcess_m1944241237_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::get_Capacity()
#define List_1_get_Capacity_m1564540636(__this, method) ((  int32_t (*) (List_1_t2466263995 *, const MethodInfo*))List_1_get_Capacity_m3133733835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2503977039(__this, ___value0, method) ((  void (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1_set_Capacity_m491101164_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::get_Count()
#define List_1_get_Count_m3267368482(__this, method) ((  int32_t (*) (List_1_t2466263995 *, const MethodInfo*))List_1_get_Count_m1012630527_gshared)(__this, method)
// T System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::get_Item(System.Int32)
#define List_1_get_Item_m2594534015(__this, ___index0, method) ((  FsmGameObject_t3097142863 * (*) (List_1_t2466263995 *, int32_t, const MethodInfo*))List_1_get_Item_m1650084162_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>::set_Item(System.Int32,T)
#define List_1_set_Item_m3996883046(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2466263995 *, int32_t, FsmGameObject_t3097142863 *, const MethodInfo*))List_1_set_Item_m1410262499_gshared)(__this, ___index0, ___value1, method)
