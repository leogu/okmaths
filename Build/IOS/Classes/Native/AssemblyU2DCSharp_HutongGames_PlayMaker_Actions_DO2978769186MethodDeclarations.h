﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveZ
struct DOTweenTransformLocalMoveZ_t2978769186;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveZ::.ctor()
extern "C"  void DOTweenTransformLocalMoveZ__ctor_m3063005556 (DOTweenTransformLocalMoveZ_t2978769186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveZ::Reset()
extern "C"  void DOTweenTransformLocalMoveZ_Reset_m3247487283 (DOTweenTransformLocalMoveZ_t2978769186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveZ::OnEnter()
extern "C"  void DOTweenTransformLocalMoveZ_OnEnter_m1494440739 (DOTweenTransformLocalMoveZ_t2978769186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveZ::<OnEnter>m__BA()
extern "C"  void DOTweenTransformLocalMoveZ_U3COnEnterU3Em__BA_m1218648987 (DOTweenTransformLocalMoveZ_t2978769186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformLocalMoveZ::<OnEnter>m__BB()
extern "C"  void DOTweenTransformLocalMoveZ_U3COnEnterU3Em__BB_m3685410316 (DOTweenTransformLocalMoveZ_t2978769186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
