﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawTexture
struct DrawTexture_t3267812629;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawTexture::.ctor()
extern "C"  void DrawTexture__ctor_m4079712621 (DrawTexture_t3267812629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::Reset()
extern "C"  void DrawTexture_Reset_m3896888210 (DrawTexture_t3267812629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawTexture::OnGUI()
extern "C"  void DrawTexture_OnGUI_m632521183 (DrawTexture_t3267812629 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
