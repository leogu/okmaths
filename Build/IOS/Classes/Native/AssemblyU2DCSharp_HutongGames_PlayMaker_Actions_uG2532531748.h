﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiGetIsInteractable
struct  uGuiGetIsInteractable_t2532531748  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::isInteractable
	FsmBool_t664485696 * ___isInteractable_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::isInteractableEvent
	FsmEvent_t1258573736 * ___isInteractableEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::isNotInteractableEvent
	FsmEvent_t1258573736 * ___isNotInteractableEvent_14;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::_selectable
	Selectable_t1490392188 * ____selectable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::_originalState
	bool ____originalState_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiGetIsInteractable_t2532531748, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isInteractable_12() { return static_cast<int32_t>(offsetof(uGuiGetIsInteractable_t2532531748, ___isInteractable_12)); }
	inline FsmBool_t664485696 * get_isInteractable_12() const { return ___isInteractable_12; }
	inline FsmBool_t664485696 ** get_address_of_isInteractable_12() { return &___isInteractable_12; }
	inline void set_isInteractable_12(FsmBool_t664485696 * value)
	{
		___isInteractable_12 = value;
		Il2CppCodeGenWriteBarrier(&___isInteractable_12, value);
	}

	inline static int32_t get_offset_of_isInteractableEvent_13() { return static_cast<int32_t>(offsetof(uGuiGetIsInteractable_t2532531748, ___isInteractableEvent_13)); }
	inline FsmEvent_t1258573736 * get_isInteractableEvent_13() const { return ___isInteractableEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isInteractableEvent_13() { return &___isInteractableEvent_13; }
	inline void set_isInteractableEvent_13(FsmEvent_t1258573736 * value)
	{
		___isInteractableEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isInteractableEvent_13, value);
	}

	inline static int32_t get_offset_of_isNotInteractableEvent_14() { return static_cast<int32_t>(offsetof(uGuiGetIsInteractable_t2532531748, ___isNotInteractableEvent_14)); }
	inline FsmEvent_t1258573736 * get_isNotInteractableEvent_14() const { return ___isNotInteractableEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotInteractableEvent_14() { return &___isNotInteractableEvent_14; }
	inline void set_isNotInteractableEvent_14(FsmEvent_t1258573736 * value)
	{
		___isNotInteractableEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isNotInteractableEvent_14, value);
	}

	inline static int32_t get_offset_of__selectable_15() { return static_cast<int32_t>(offsetof(uGuiGetIsInteractable_t2532531748, ____selectable_15)); }
	inline Selectable_t1490392188 * get__selectable_15() const { return ____selectable_15; }
	inline Selectable_t1490392188 ** get_address_of__selectable_15() { return &____selectable_15; }
	inline void set__selectable_15(Selectable_t1490392188 * value)
	{
		____selectable_15 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_15, value);
	}

	inline static int32_t get_offset_of__originalState_16() { return static_cast<int32_t>(offsetof(uGuiGetIsInteractable_t2532531748, ____originalState_16)); }
	inline bool get__originalState_16() const { return ____originalState_16; }
	inline bool* get_address_of__originalState_16() { return &____originalState_16; }
	inline void set__originalState_16(bool value)
	{
		____originalState_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
