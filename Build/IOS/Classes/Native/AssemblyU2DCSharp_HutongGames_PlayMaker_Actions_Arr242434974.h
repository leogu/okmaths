﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects
struct  ArrayListSendEventToGameObjects_t242434974  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::eventTarget
	FsmEventTarget_t172293745 * ___eventTarget_12;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::reference
	FsmString_t2414474701 * ___reference_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::sendEvent
	FsmEvent_t1258573736 * ___sendEvent_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::excludeSelf
	FsmBool_t664485696 * ___excludeSelf_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListSendEventToGameObjects::sendToChildren
	FsmBool_t664485696 * ___sendToChildren_17;

public:
	inline static int32_t get_offset_of_eventTarget_12() { return static_cast<int32_t>(offsetof(ArrayListSendEventToGameObjects_t242434974, ___eventTarget_12)); }
	inline FsmEventTarget_t172293745 * get_eventTarget_12() const { return ___eventTarget_12; }
	inline FsmEventTarget_t172293745 ** get_address_of_eventTarget_12() { return &___eventTarget_12; }
	inline void set_eventTarget_12(FsmEventTarget_t172293745 * value)
	{
		___eventTarget_12 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_12, value);
	}

	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(ArrayListSendEventToGameObjects_t242434974, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_reference_14() { return static_cast<int32_t>(offsetof(ArrayListSendEventToGameObjects_t242434974, ___reference_14)); }
	inline FsmString_t2414474701 * get_reference_14() const { return ___reference_14; }
	inline FsmString_t2414474701 ** get_address_of_reference_14() { return &___reference_14; }
	inline void set_reference_14(FsmString_t2414474701 * value)
	{
		___reference_14 = value;
		Il2CppCodeGenWriteBarrier(&___reference_14, value);
	}

	inline static int32_t get_offset_of_sendEvent_15() { return static_cast<int32_t>(offsetof(ArrayListSendEventToGameObjects_t242434974, ___sendEvent_15)); }
	inline FsmEvent_t1258573736 * get_sendEvent_15() const { return ___sendEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_sendEvent_15() { return &___sendEvent_15; }
	inline void set_sendEvent_15(FsmEvent_t1258573736 * value)
	{
		___sendEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_15, value);
	}

	inline static int32_t get_offset_of_excludeSelf_16() { return static_cast<int32_t>(offsetof(ArrayListSendEventToGameObjects_t242434974, ___excludeSelf_16)); }
	inline FsmBool_t664485696 * get_excludeSelf_16() const { return ___excludeSelf_16; }
	inline FsmBool_t664485696 ** get_address_of_excludeSelf_16() { return &___excludeSelf_16; }
	inline void set_excludeSelf_16(FsmBool_t664485696 * value)
	{
		___excludeSelf_16 = value;
		Il2CppCodeGenWriteBarrier(&___excludeSelf_16, value);
	}

	inline static int32_t get_offset_of_sendToChildren_17() { return static_cast<int32_t>(offsetof(ArrayListSendEventToGameObjects_t242434974, ___sendToChildren_17)); }
	inline FsmBool_t664485696 * get_sendToChildren_17() const { return ___sendToChildren_17; }
	inline FsmBool_t664485696 ** get_address_of_sendToChildren_17() { return &___sendToChildren_17; }
	inline void set_sendToChildren_17(FsmBool_t664485696 * value)
	{
		___sendToChildren_17 = value;
		Il2CppCodeGenWriteBarrier(&___sendToChildren_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
