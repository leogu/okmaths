﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MouseLook
struct MouseLook_t1032090344;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"

// System.Void HutongGames.PlayMaker.Actions.MouseLook::.ctor()
extern "C"  void MouseLook__ctor_m1371025114 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::Reset()
extern "C"  void MouseLook_Reset_m3800630565 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::OnEnter()
extern "C"  void MouseLook_OnEnter_m4219644365 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::OnUpdate()
extern "C"  void MouseLook_OnUpdate_m975518580 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MouseLook::DoMouseLook()
extern "C"  void MouseLook_DoMouseLook_m4185814061 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook::GetXRotation()
extern "C"  float MouseLook_GetXRotation_m2134992052 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook::GetYRotation()
extern "C"  float MouseLook_GetYRotation_m2100596053 (MouseLook_t1032090344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.MouseLook::ClampAngle(System.Single,HutongGames.PlayMaker.FsmFloat,HutongGames.PlayMaker.FsmFloat)
extern "C"  float MouseLook_ClampAngle_m4221150413 (Il2CppObject * __this /* static, unused */, float ___angle0, FsmFloat_t937133978 * ___min1, FsmFloat_t937133978 * ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
