﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues
struct uGuiLayoutElementSetValues_t2474063466;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::.ctor()
extern "C"  void uGuiLayoutElementSetValues__ctor_m3745175498 (uGuiLayoutElementSetValues_t2474063466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::Reset()
extern "C"  void uGuiLayoutElementSetValues_Reset_m893608851 (uGuiLayoutElementSetValues_t2474063466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::OnEnter()
extern "C"  void uGuiLayoutElementSetValues_OnEnter_m1800577747 (uGuiLayoutElementSetValues_t2474063466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::OnUpdate()
extern "C"  void uGuiLayoutElementSetValues_OnUpdate_m71171700 (uGuiLayoutElementSetValues_t2474063466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiLayoutElementSetValues::DoSetValues()
extern "C"  void uGuiLayoutElementSetValues_DoSetValues_m1947964619 (uGuiLayoutElementSetValues_t2474063466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
