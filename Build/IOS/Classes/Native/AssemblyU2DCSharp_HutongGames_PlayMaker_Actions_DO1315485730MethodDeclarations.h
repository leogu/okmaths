﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById
struct DOTweenControlMethodsRestartById_t1315485730;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::.ctor()
extern "C"  void DOTweenControlMethodsRestartById__ctor_m2124373812 (DOTweenControlMethodsRestartById_t1315485730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::Reset()
extern "C"  void DOTweenControlMethodsRestartById_Reset_m2919793811 (DOTweenControlMethodsRestartById_t1315485730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsRestartById::OnEnter()
extern "C"  void DOTweenControlMethodsRestartById_OnEnter_m2467423155 (DOTweenControlMethodsRestartById_t1315485730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
