﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmVector3
struct  FsmVector3_t3996534004  : public NamedVariable_t3026441313
{
public:
	// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVector3::value
	Vector3_t2243707580  ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmVector3_t3996534004, ___value_5)); }
	inline Vector3_t2243707580  get_value_5() const { return ___value_5; }
	inline Vector3_t2243707580 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Vector3_t2243707580  value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
