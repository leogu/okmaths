﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SampleCurve
struct SampleCurve_t702362601;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SampleCurve::.ctor()
extern "C"  void SampleCurve__ctor_m423861119 (SampleCurve_t702362601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SampleCurve::Reset()
extern "C"  void SampleCurve_Reset_m2814989730 (SampleCurve_t702362601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SampleCurve::OnEnter()
extern "C"  void SampleCurve_OnEnter_m3421227332 (SampleCurve_t702362601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SampleCurve::OnUpdate()
extern "C"  void SampleCurve_OnUpdate_m458739823 (SampleCurve_t702362601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SampleCurve::DoSampleCurve()
extern "C"  void SampleCurve_DoSampleCurve_m2326468173 (SampleCurve_t702362601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
