﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutIntLabel
struct GUILayoutIntLabel_t2993446;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::.ctor()
extern "C"  void GUILayoutIntLabel__ctor_m93507566 (GUILayoutIntLabel_t2993446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::Reset()
extern "C"  void GUILayoutIntLabel_Reset_m775535843 (GUILayoutIntLabel_t2993446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntLabel::OnGUI()
extern "C"  void GUILayoutIntLabel_OnGUI_m981562674 (GUILayoutIntLabel_t2993446 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
