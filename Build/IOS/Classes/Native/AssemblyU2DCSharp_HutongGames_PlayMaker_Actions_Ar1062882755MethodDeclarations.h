﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayForEach
struct ArrayForEach_t1062882755;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::.ctor()
extern "C"  void ArrayForEach__ctor_m1757871667 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::Reset()
extern "C"  void ArrayForEach_Reset_m2553285396 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::Awake()
extern "C"  void ArrayForEach_Awake_m2387589848 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnEnter()
extern "C"  void ArrayForEach_OnEnter_m2706169234 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnUpdate()
extern "C"  void ArrayForEach_OnUpdate_m2167903195 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnFixedUpdate()
extern "C"  void ArrayForEach_OnFixedUpdate_m1625273593 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::OnLateUpdate()
extern "C"  void ArrayForEach_OnLateUpdate_m4033267983 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::StartNextFsm()
extern "C"  void ArrayForEach_StartNextFsm_m3547426710 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::StartFsm()
extern "C"  void ArrayForEach_StartFsm_m2982223735 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::DoStartFsm()
extern "C"  void ArrayForEach_DoStartFsm_m1906545692 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayForEach::CheckIfFinished()
extern "C"  void ArrayForEach_CheckIfFinished_m632596150 (ArrayForEach_t1062882755 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
