﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue
struct uGuiScrollbarGetValue_t2028754341;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::.ctor()
extern "C"  void uGuiScrollbarGetValue__ctor_m3128579161 (uGuiScrollbarGetValue_t2028754341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::Reset()
extern "C"  void uGuiScrollbarGetValue_Reset_m3820338334 (uGuiScrollbarGetValue_t2028754341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::OnEnter()
extern "C"  void uGuiScrollbarGetValue_OnEnter_m484958148 (uGuiScrollbarGetValue_t2028754341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::OnUpdate()
extern "C"  void uGuiScrollbarGetValue_OnUpdate_m2297445661 (uGuiScrollbarGetValue_t2028754341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarGetValue::DoGetValue()
extern "C"  void uGuiScrollbarGetValue_DoGetValue_m94984479 (uGuiScrollbarGetValue_t2028754341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
