﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetTransformParent
struct SetTransformParent_t806690980;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetTransformParent::.ctor()
extern "C"  void SetTransformParent__ctor_m874798308 (SetTransformParent_t806690980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTransformParent::Reset()
extern "C"  void SetTransformParent_Reset_m83761997 (SetTransformParent_t806690980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetTransformParent::OnEnter()
extern "C"  void SetTransformParent_OnEnter_m3531440157 (SetTransformParent_t806690980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
