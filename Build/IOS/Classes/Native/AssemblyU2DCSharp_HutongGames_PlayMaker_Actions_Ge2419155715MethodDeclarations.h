﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetEventProperties
struct GetEventProperties_t2419155715;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetEventProperties::.ctor()
extern "C"  void GetEventProperties__ctor_m4066043153 (GetEventProperties_t2419155715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetEventProperties::Reset()
extern "C"  void GetEventProperties_Reset_m1212610056 (GetEventProperties_t2419155715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetEventProperties::OnEnter()
extern "C"  void GetEventProperties_OnEnter_m3716197498 (GetEventProperties_t2419155715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
