﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate
struct uGuiInputFieldGetCaretBlinkRate_t1105302691;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::.ctor()
extern "C"  void uGuiInputFieldGetCaretBlinkRate__ctor_m2949623999 (uGuiInputFieldGetCaretBlinkRate_t1105302691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::Reset()
extern "C"  void uGuiInputFieldGetCaretBlinkRate_Reset_m31842692 (uGuiInputFieldGetCaretBlinkRate_t1105302691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::OnEnter()
extern "C"  void uGuiInputFieldGetCaretBlinkRate_OnEnter_m3297582994 (uGuiInputFieldGetCaretBlinkRate_t1105302691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::OnUpdate()
extern "C"  void uGuiInputFieldGetCaretBlinkRate_OnUpdate_m3387316159 (uGuiInputFieldGetCaretBlinkRate_t1105302691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetCaretBlinkRate::DoGetValue()
extern "C"  void uGuiInputFieldGetCaretBlinkRate_DoGetValue_m2152939833 (uGuiInputFieldGetCaretBlinkRate_t1105302691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
