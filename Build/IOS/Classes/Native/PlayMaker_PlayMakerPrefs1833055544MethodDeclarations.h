﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerPrefs
struct PlayMakerPrefs_t1833055544;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"

// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern "C"  PlayMakerPrefs_t1833055544 * PlayMakerPrefs_get_Instance_m688943076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern "C"  ColorU5BU5D_t672350442* PlayMakerPrefs_get_Colors_m3543534341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::set_Colors(UnityEngine.Color[])
extern "C"  void PlayMakerPrefs_set_Colors_m3809722884 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PlayMakerPrefs::get_ColorNames()
extern "C"  StringU5BU5D_t1642385972* PlayMakerPrefs_get_ColorNames_m494908428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::set_ColorNames(System.String[])
extern "C"  void PlayMakerPrefs_set_ColorNames_m3843271929 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::ResetDefaultColors()
extern "C"  void PlayMakerPrefs_ResetDefaultColors_m993607445 (PlayMakerPrefs_t1833055544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayMakerPrefs::get_MinimapColors()
extern "C"  ColorU5BU5D_t672350442* PlayMakerPrefs_get_MinimapColors_m2639067296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::SaveChanges()
extern "C"  void PlayMakerPrefs_SaveChanges_m3585553949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern "C"  void PlayMakerPrefs_UpdateMinimapColors_m1326282697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::.ctor()
extern "C"  void PlayMakerPrefs__ctor_m1864640893 (PlayMakerPrefs_t1833055544 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::.cctor()
extern "C"  void PlayMakerPrefs__cctor_m2817732924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
