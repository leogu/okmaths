﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m757112303(__this, ___l0, method) ((  void (*) (Enumerator_t3863303798 *, List_1_t33606828 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m769523899(__this, method) ((  void (*) (Enumerator_t3863303798 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3707385111(__this, method) ((  Il2CppObject * (*) (Enumerator_t3863303798 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::Dispose()
#define Enumerator_Dispose_m753619564(__this, method) ((  void (*) (Enumerator_t3863303798 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::VerifyState()
#define Enumerator_VerifyState_m4059010309(__this, method) ((  void (*) (Enumerator_t3863303798 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::MoveNext()
#define Enumerator_MoveNext_m1838688394(__this, method) ((  bool (*) (Enumerator_t3863303798 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmBool>::get_Current()
#define Enumerator_get_Current_m2361709928(__this, method) ((  FsmBool_t664485696 * (*) (Enumerator_t3863303798 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
