﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// SimpleJson.JsonArray
struct JsonArray_t1679500587;
// System.String
struct String_t;
// SimpleJson.JsonObject
struct JsonObject_t2300545015;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t3332939458;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct IEnumerator_1_t4132064902;
// SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t2810850750;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t3084043859;
// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t266144316;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct IDictionary_2_t3814930911;
// System.Enum
struct Enum_t2459695545;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>
struct IEnumerable_1_t3143943587;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t2851816542;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t2545856110;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t547167195;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t352281633;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t4206365109;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey2
struct U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey4
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey6
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t209712766;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t1099699699;
// UnityEngine.Advertisements.UnityAdsDelegate
struct UnityAdsDelegate_t3613839672;
// UnityEngine.Advertisements.UnityAdsInternal
struct UnityAdsInternal_t2792527316;
// UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>
struct UnityAdsDelegate_2_t1684806294;
// UnityEngine.Animation
struct Animation_t2068071072;
// UnityEngine.AnimationState
struct AnimationState_t1303741697;
// UnityEngine.AnimationClip
struct AnimationClip_t3510324950;
// UnityEngine.Animation/Enumerator
struct Enumerator_t59479314;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// UnityEngine.AnimationEvent
struct AnimationEvent_t2428323300;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t670468573;
// UnityEngine.Application/LogCallback
struct LogCallback_t1867914413;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t1557026495;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t1038783543;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t2674559435;
// UnityEngine.Object[]
struct ObjectU5BU5D_t4217747464;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t3007145346;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t421863554;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t3743753033;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// UnityEngine.BitStream
struct BitStream_t1979465639;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3522132132;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t578636151;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.PhysicsMaterial2D
struct PhysicsMaterial2D_t851691520;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t1084937515;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t1215651809;
// UnityEngine.Component[]
struct ComponentU5BU5D_t4136971630;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3188497603;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.CullingGroup
struct CullingGroup_t1091689465;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t2480912210;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t1786092740;
// UnityEngine.ILogger
struct ILogger_t1425954571;
// System.Exception
struct Exception_t1927440687;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t865810509;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t2656950;
// UnityEngine.Display
struct Display_t3666191348;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t3423469815;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Event
struct Event_t3028476042;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t4810721;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2229564840;
// System.Delegate
struct Delegate_t3022476291;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t2183506063;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t3793436469;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t828812576;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_U3CModuleU3E3783534214.h"
#include "UnityEngine_U3CModuleU3E3783534214MethodDeclarations.h"
#include "UnityEngine_SimpleJson_JsonArray1679500587.h"
#include "UnityEngine_SimpleJson_JsonArray1679500587MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_SimpleJson_SimpleJson3569903358MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_SimpleJson_JsonObject2300545015.h"
#include "UnityEngine_SimpleJson_JsonObject2300545015MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge309261261.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1629285963.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2792759032.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3307288400.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22361573779.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22361573779MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_ArgumentNullException628810857MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException628810857.h"
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy2810850750.h"
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy2810850750MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3698747442MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4203819763MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T880847899MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1385920220MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T134667198MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T639739519MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3084043859.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3698747442.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4203819763.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T880847899.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1385920220.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T134667198.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T639739519.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils3032483338MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2267060895MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2267060895.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase904190842.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_G352281633.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1520880194MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1520880194.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4206365109.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23901068228.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23901068228MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo3500843524.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_DateTimeOffset1362988906.h"
#include "mscorlib_System_Guid2533601593.h"
#include "mscorlib_System_DateTime693205669MethodDeclarations.h"
#include "mscorlib_System_DateTimeOffset1362988906MethodDeclarations.h"
#include "mscorlib_System_Guid2533601593MethodDeclarations.h"
#include "System_System_Uri19570940.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_G352281633MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g24406117.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g24406117MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils3032483338.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075.h"
#include "mscorlib_System_Reflection_ParameterInfo2249040075MethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1606021565MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3084043859MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1606021565.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1342095133MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1342095133.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3264409434MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_3264409434.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_2636512595MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4206365109MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_2636512595.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4202596536MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_4202596536.h"
#include "mscorlib_System_Reflection_ConstructorInfo2851816542MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "UnityEngine_SimpleJson_SimpleJson3569903358.h"
#include "mscorlib_System_Runtime_Serialization_Serialization753258759MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serialization753258759.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "mscorlib_System_UInt322149682021MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Globalization_NumberStyles3408984435.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Double4078015681MethodDeclarations.h"
#include "mscorlib_System_Int64909078037MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_StringComparison2376310518.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_UInt642909196914MethodDeclarations.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077MethodDeclarations.h"
#include "mscorlib_System_Single2076509932MethodDeclarations.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_UInt16986882611.h"
#include "UnityEngine_UnityEngine_AddComponentMenu1099699699.h"
#include "UnityEngine_UnityEngine_AddComponentMenu1099699699MethodDeclarations.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3613839672.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3613839672MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInt2792527316.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInt2792527316MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel1684806294.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel1684806294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsMan1959728304.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsMan1959728304MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_AnchoredJoint2D4245992618.h"
#include "UnityEngine_UnityEngine_AnchoredJoint2D4245992618MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "UnityEngine_UnityEngine_Animation2068071072MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator59479314MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator59479314.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode3402232318.h"
#include "UnityEngine_UnityEngine_AnimationBlendMode3402232318MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Motion2415020824MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_Animator69676727MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal3089653344.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_AvatarTarget193923574.h"
#include "UnityEngine_UnityEngine_MatchTargetWeightMask296470556.h"
#include "UnityEngine_UnityEngine_HumanBodyBones1322940928.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode258421960.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_AnimatorCullingMode258421960MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application_LogCallback1867914413.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly1557026495MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754.h"
#include "UnityEngine_UnityEngine_AssetBundle2054978754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest1038783543MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest_C2509986555.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest_C2509986555MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest2674559435MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine958797062MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3043633143.h"
#include "UnityEngine_UnityEngine_RequireComponent864575032.h"
#include "System_System_Collections_Generic_Stack_1_gen2391531380MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen2391531380.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672924358MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen672924358.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixer3244290001.h"
#include "UnityEngine_UnityEngine_Audio_AudioMixer3244290001MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"
#include "UnityEngine_UnityEngine_AudioListener1996719162.h"
#include "UnityEngine_UnityEngine_AudioListener1996719162MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_AudioType4076847944.h"
#include "UnityEngine_UnityEngine_AudioType4076847944MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AvatarIKGoal3089653344MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AvatarTarget193923574MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BitStream1979465639.h"
#include "UnityEngine_UnityEngine_BitStream1979465639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NetworkViewID3942988548.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Bounds3033363703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UnityString276356480MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705.h"
#include "UnityEngine_UnityEngine_Ray2469606224.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback834278767.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_CameraClearFlags452084705MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Canvas209405766MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_Math2022911894MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "mscorlib_System_Collections_Generic_List_1_gen573379950.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "mscorlib_System_Collections_Generic_List_1_gen243638650.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828713.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_Collider3497673348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_PhysicMaterial578636151.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_Collider2D646061738MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_PhysicsMaterial2D851691520.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collision2876846408MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Color32874517518MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3188497603.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465.h"
#include "UnityEngine_UnityEngine_CullingGroup1091689465MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2480912210.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1057617917MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Cursor873194084.h"
#include "UnityEngine_UnityEngine_Cursor873194084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CursorLockMode3372615096.h"
#include "UnityEngine_UnityEngine_CursorLockMode3372615096MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1786092740.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1786092740MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1368543263.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Logger3328995178MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugLogHandler865810509.h"
#include "UnityEngine_UnityEngine_Logger3328995178.h"
#include "mscorlib_System_Exception1927440687.h"
#include "UnityEngine_UnityEngine_DeviceOrientation895964084.h"
#include "UnityEngine_UnityEngine_DeviceOrientation895964084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent2656950MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display3666191348.h"
#include "UnityEngine_UnityEngine_Display3666191348MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDel3423469815MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker154385424.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker154385424MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties2488747555.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties2488747555MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_Event3028476042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3986656710.h"
#include "mscorlib_System_ArgumentException3259014390.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache4810721MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2229564840MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall2183506063MethodDeclarations.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4275971970MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511.h"
#include "UnityEngine_UnityEngine_Events_UnityAction4025899511MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2295673753MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1598685972MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1598685972.h"
#include "mscorlib_System_Predicate_1_gen672534955MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen672534955.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3793436469MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3420894182.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase828812576MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_822770124MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_870059823.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_865427339.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_822770124.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2619124609.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup339478082MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3162557601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3162557601.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2697287275.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2697287275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo857969000MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SimpleJson.JsonArray::.ctor()
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m365405030_MethodInfo_var;
extern const uint32_t JsonArray__ctor_m33515755_MetadataUsageId;
extern "C"  void JsonArray__ctor_m33515755 (JsonArray_t1679500587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonArray__ctor_m33515755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m365405030(__this, /*hidden argument*/List_1__ctor_m365405030_MethodInfo_var);
		return;
	}
}
// System.String SimpleJson.JsonArray::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonArray_ToString_m1666534930_MetadataUsageId;
extern "C"  String_t* JsonArray_ToString_m1666534930 (JsonArray_t1679500587 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonArray_ToString_m1666534930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = SimpleJson_SerializeObject_m478839316(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B2_0 = L_2;
	}

IL_0012:
	{
		return G_B2_0;
	}
}
// System.Void SimpleJson.JsonObject::.ctor()
extern Il2CppClass* Dictionary_2_t309261261_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3188644741_MethodInfo_var;
extern const uint32_t JsonObject__ctor_m2062444459_MetadataUsageId;
extern "C"  void JsonObject__ctor_m2062444459 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject__ctor_m2062444459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Dictionary_2_t309261261 * L_0 = (Dictionary_2_t309261261 *)il2cpp_codegen_object_new(Dictionary_2_t309261261_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3188644741(L_0, /*hidden argument*/Dictionary_2__ctor_m3188644741_MethodInfo_var);
		__this->set__members_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator SimpleJson.JsonObject::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* Enumerator_t1629285963_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m4290581089_MethodInfo_var;
extern const uint32_t JsonObject_System_Collections_IEnumerable_GetEnumerator_m763605844_MetadataUsageId;
extern "C"  Il2CppObject * JsonObject_System_Collections_IEnumerable_GetEnumerator_m763605844 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_System_Collections_IEnumerable_GetEnumerator_m763605844_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		Enumerator_t1629285963  L_1 = Dictionary_2_GetEnumerator_m4290581089(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m4290581089_MethodInfo_var);
		Enumerator_t1629285963  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t1629285963_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.Void SimpleJson.JsonObject::Add(System.String,System.Object)
extern const MethodInfo* Dictionary_2_Add_m1128763565_MethodInfo_var;
extern const uint32_t JsonObject_Add_m3402027904_MetadataUsageId;
extern "C"  void JsonObject_Add_m3402027904 (JsonObject_t2300545015 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Add_m3402027904_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		Dictionary_2_Add_m1128763565(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		return;
	}
}
// System.Boolean SimpleJson.JsonObject::ContainsKey(System.String)
extern const MethodInfo* Dictionary_2_ContainsKey_m1533770720_MethodInfo_var;
extern const uint32_t JsonObject_ContainsKey_m567001925_MetadataUsageId;
extern "C"  bool JsonObject_ContainsKey_m567001925 (JsonObject_t2300545015 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_ContainsKey_m567001925_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1533770720(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<System.String> SimpleJson.JsonObject::get_Keys()
extern const MethodInfo* Dictionary_2_get_Keys_m660840187_MethodInfo_var;
extern const uint32_t JsonObject_get_Keys_m98057106_MetadataUsageId;
extern "C"  Il2CppObject* JsonObject_get_Keys_m98057106 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_get_Keys_m98057106_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		KeyCollection_t2792759032 * L_1 = Dictionary_2_get_Keys_m660840187(L_0, /*hidden argument*/Dictionary_2_get_Keys_m660840187_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.ICollection`1<System.Object> SimpleJson.JsonObject::get_Values()
extern const MethodInfo* Dictionary_2_get_Values_m139345435_MethodInfo_var;
extern const uint32_t JsonObject_get_Values_m3342885038_MetadataUsageId;
extern "C"  Il2CppObject* JsonObject_get_Values_m3342885038 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_get_Values_m3342885038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		ValueCollection_t3307288400 * L_1 = Dictionary_2_get_Values_m139345435(L_0, /*hidden argument*/Dictionary_2_get_Values_m139345435_MethodInfo_var);
		return L_1;
	}
}
// System.Object SimpleJson.JsonObject::get_Item(System.String)
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const uint32_t JsonObject_get_Item_m1018841170_MetadataUsageId;
extern "C"  Il2CppObject * JsonObject_get_Item_m1018841170 (JsonObject_t2300545015 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_get_Item_m1018841170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		Il2CppObject * L_2 = Dictionary_2_get_Item_m464793699(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		return L_2;
	}
}
// System.Void SimpleJson.JsonObject::set_Item(System.String,System.Object)
extern const MethodInfo* Dictionary_2_set_Item_m4132139590_MethodInfo_var;
extern const uint32_t JsonObject_set_Item_m475151339_MetadataUsageId;
extern "C"  void JsonObject_set_Item_m475151339 (JsonObject_t2300545015 * __this, String_t* ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_set_Item_m475151339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = ___key0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		Dictionary_2_set_Item_m4132139590(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_set_Item_m4132139590_MethodInfo_var);
		return;
	}
}
// System.Void SimpleJson.JsonObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m1313755691_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3217213384_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1128763565_MethodInfo_var;
extern const uint32_t JsonObject_Add_m534223875_MetadataUsageId;
extern "C"  void JsonObject_Add_m534223875 (JsonObject_t2300545015 * __this, KeyValuePair_2_t2361573779  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Add_m534223875_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1313755691((&___item0), /*hidden argument*/KeyValuePair_2_get_Key_m1313755691_MethodInfo_var);
		Il2CppObject * L_2 = KeyValuePair_2_get_Value_m3217213384((&___item0), /*hidden argument*/KeyValuePair_2_get_Value_m3217213384_MethodInfo_var);
		NullCheck(L_0);
		Dictionary_2_Add_m1128763565(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_Add_m1128763565_MethodInfo_var);
		return;
	}
}
// System.Void SimpleJson.JsonObject::Clear()
extern const MethodInfo* Dictionary_2_Clear_m3103846974_MethodInfo_var;
extern const uint32_t JsonObject_Clear_m2413527334_MetadataUsageId;
extern "C"  void JsonObject_Clear_m2413527334 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Clear_m2413527334_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		Dictionary_2_Clear_m3103846974(L_0, /*hidden argument*/Dictionary_2_Clear_m3103846974_MethodInfo_var);
		return;
	}
}
// System.Boolean SimpleJson.JsonObject::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m1313755691_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1533770720_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m464793699_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3217213384_MethodInfo_var;
extern const uint32_t JsonObject_Contains_m3389598869_MetadataUsageId;
extern "C"  bool JsonObject_Contains_m3389598869 (JsonObject_t2300545015 * __this, KeyValuePair_2_t2361573779  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Contains_m3389598869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1313755691((&___item0), /*hidden argument*/KeyValuePair_2_get_Key_m1313755691_MethodInfo_var);
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1533770720(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1533770720_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		Dictionary_2_t309261261 * L_3 = __this->get__members_0();
		String_t* L_4 = KeyValuePair_2_get_Key_m1313755691((&___item0), /*hidden argument*/KeyValuePair_2_get_Key_m1313755691_MethodInfo_var);
		NullCheck(L_3);
		Il2CppObject * L_5 = Dictionary_2_get_Item_m464793699(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m464793699_MethodInfo_var);
		Il2CppObject * L_6 = KeyValuePair_2_get_Value_m3217213384((&___item0), /*hidden argument*/KeyValuePair_2_get_Value_m3217213384_MethodInfo_var);
		G_B3_0 = ((((Il2CppObject*)(Il2CppObject *)L_5) == ((Il2CppObject*)(Il2CppObject *)L_6))? 1 : 0);
		goto IL_0035;
	}

IL_0034:
	{
		G_B3_0 = 0;
	}

IL_0035:
	{
		return (bool)G_B3_0;
	}
}
// System.Void SimpleJson.JsonObject::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[],System.Int32)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4132064902_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1185213181;
extern const uint32_t JsonObject_CopyTo_m4217013231_MetadataUsageId;
extern "C"  void JsonObject_CopyTo_m4217013231 (JsonObject_t2300545015 * __this, KeyValuePair_2U5BU5D_t3332939458* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_CopyTo_m4217013231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t2361573779  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Il2CppObject* V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		KeyValuePair_2U5BU5D_t3332939458* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1185213181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = JsonObject_get_Count_m1372513591(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		Il2CppObject* L_3 = JsonObject_GetEnumerator_m1868414604(__this, /*hidden argument*/NULL);
		V_2 = L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0024:
		{
			Il2CppObject* L_4 = V_2;
			NullCheck(L_4);
			KeyValuePair_2_t2361573779  L_5 = InterfaceFuncInvoker0< KeyValuePair_2_t2361573779  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::get_Current() */, IEnumerator_1_t4132064902_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			KeyValuePair_2U5BU5D_t3332939458* L_6 = ___array0;
			int32_t L_7 = ___arrayIndex1;
			int32_t L_8 = L_7;
			___arrayIndex1 = ((int32_t)((int32_t)L_8+(int32_t)1));
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
			KeyValuePair_2_t2361573779  L_9 = V_1;
			(*(KeyValuePair_2_t2361573779 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))) = L_9;
			int32_t L_10 = V_0;
			int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
			V_0 = L_11;
			if ((((int32_t)L_11) > ((int32_t)0)))
			{
				goto IL_004d;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}

IL_004d:
		{
			Il2CppObject* L_12 = V_2;
			NullCheck(L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0024;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Il2CppObject* L_15 = V_2;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Int32 SimpleJson.JsonObject::get_Count()
extern const MethodInfo* Dictionary_2_get_Count_m1801589293_MethodInfo_var;
extern const uint32_t JsonObject_get_Count_m1372513591_MetadataUsageId;
extern "C"  int32_t JsonObject_get_Count_m1372513591 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_get_Count_m1372513591_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m1801589293(L_0, /*hidden argument*/Dictionary_2_get_Count_m1801589293_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean SimpleJson.JsonObject::get_IsReadOnly()
extern "C"  bool JsonObject_get_IsReadOnly_m2070569848 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean SimpleJson.JsonObject::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m1313755691_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m3100271268_MethodInfo_var;
extern const uint32_t JsonObject_Remove_m968288636_MetadataUsageId;
extern "C"  bool JsonObject_Remove_m968288636 (JsonObject_t2300545015 * __this, KeyValuePair_2_t2361573779  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_Remove_m968288636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		String_t* L_1 = KeyValuePair_2_get_Key_m1313755691((&___item0), /*hidden argument*/KeyValuePair_2_get_Key_m1313755691_MethodInfo_var);
		NullCheck(L_0);
		bool L_2 = Dictionary_2_Remove_m3100271268(L_0, L_1, /*hidden argument*/Dictionary_2_Remove_m3100271268_MethodInfo_var);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> SimpleJson.JsonObject::GetEnumerator()
extern Il2CppClass* Enumerator_t1629285963_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m4290581089_MethodInfo_var;
extern const uint32_t JsonObject_GetEnumerator_m1868414604_MetadataUsageId;
extern "C"  Il2CppObject* JsonObject_GetEnumerator_m1868414604 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonObject_GetEnumerator_m1868414604_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t309261261 * L_0 = __this->get__members_0();
		NullCheck(L_0);
		Enumerator_t1629285963  L_1 = Dictionary_2_GetEnumerator_m4290581089(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m4290581089_MethodInfo_var);
		Enumerator_t1629285963  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t1629285963_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.String SimpleJson.JsonObject::ToString()
extern "C"  String_t* JsonObject_ToString_m1270529244 (JsonObject_t2300545015 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = SimpleJson_SerializeObject_m478839316(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::.ctor()
extern Il2CppClass* ThreadSafeDictionaryValueFactory_2_t3698747442_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadSafeDictionary_2_t4203819763_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadSafeDictionaryValueFactory_2_t880847899_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadSafeDictionary_2_t1385920220_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadSafeDictionaryValueFactory_2_t134667198_il2cpp_TypeInfo_var;
extern Il2CppClass* ThreadSafeDictionary_2_t639739519_il2cpp_TypeInfo_var;
extern const MethodInfo* ThreadSafeDictionaryValueFactory_2__ctor_m1401110360_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionary_2__ctor_m854375879_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionaryValueFactory_2__ctor_m1329292246_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionary_2__ctor_m871953061_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionaryValueFactory_2__ctor_m1894899048_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionary_2__ctor_m3536678423_MethodInfo_var;
extern const uint32_t PocoJsonSerializerStrategy__ctor_m679193024_MetadataUsageId;
extern "C"  void PocoJsonSerializerStrategy__ctor_m679193024 (PocoJsonSerializerStrategy_t2810850750 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy__ctor_m679193024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 6));
		ThreadSafeDictionaryValueFactory_2_t3698747442 * L_1 = (ThreadSafeDictionaryValueFactory_2_t3698747442 *)il2cpp_codegen_object_new(ThreadSafeDictionaryValueFactory_2_t3698747442_il2cpp_TypeInfo_var);
		ThreadSafeDictionaryValueFactory_2__ctor_m1401110360(L_1, __this, L_0, /*hidden argument*/ThreadSafeDictionaryValueFactory_2__ctor_m1401110360_MethodInfo_var);
		ThreadSafeDictionary_2_t4203819763 * L_2 = (ThreadSafeDictionary_2_t4203819763 *)il2cpp_codegen_object_new(ThreadSafeDictionary_2_t4203819763_il2cpp_TypeInfo_var);
		ThreadSafeDictionary_2__ctor_m854375879(L_2, L_1, /*hidden argument*/ThreadSafeDictionary_2__ctor_m854375879_MethodInfo_var);
		__this->set_ConstructorCache_0(L_2);
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 7));
		ThreadSafeDictionaryValueFactory_2_t880847899 * L_4 = (ThreadSafeDictionaryValueFactory_2_t880847899 *)il2cpp_codegen_object_new(ThreadSafeDictionaryValueFactory_2_t880847899_il2cpp_TypeInfo_var);
		ThreadSafeDictionaryValueFactory_2__ctor_m1329292246(L_4, __this, L_3, /*hidden argument*/ThreadSafeDictionaryValueFactory_2__ctor_m1329292246_MethodInfo_var);
		ThreadSafeDictionary_2_t1385920220 * L_5 = (ThreadSafeDictionary_2_t1385920220 *)il2cpp_codegen_object_new(ThreadSafeDictionary_2_t1385920220_il2cpp_TypeInfo_var);
		ThreadSafeDictionary_2__ctor_m871953061(L_5, L_4, /*hidden argument*/ThreadSafeDictionary_2__ctor_m871953061_MethodInfo_var);
		__this->set_GetCache_1(L_5);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 8));
		ThreadSafeDictionaryValueFactory_2_t134667198 * L_7 = (ThreadSafeDictionaryValueFactory_2_t134667198 *)il2cpp_codegen_object_new(ThreadSafeDictionaryValueFactory_2_t134667198_il2cpp_TypeInfo_var);
		ThreadSafeDictionaryValueFactory_2__ctor_m1894899048(L_7, __this, L_6, /*hidden argument*/ThreadSafeDictionaryValueFactory_2__ctor_m1894899048_MethodInfo_var);
		ThreadSafeDictionary_2_t639739519 * L_8 = (ThreadSafeDictionary_2_t639739519 *)il2cpp_codegen_object_new(ThreadSafeDictionary_2_t639739519_il2cpp_TypeInfo_var);
		ThreadSafeDictionary_2__ctor_m3536678423(L_8, L_7, /*hidden argument*/ThreadSafeDictionary_2__ctor_m3536678423_MethodInfo_var);
		__this->set_SetCache_2(L_8);
		return;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::.cctor()
extern const Il2CppType* Int32_t2071877448_0_0_0_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3879389748;
extern Il2CppCodeGenString* _stringLiteral3846594588;
extern Il2CppCodeGenString* _stringLiteral87001591;
extern const uint32_t PocoJsonSerializerStrategy__cctor_m3858986209_MetadataUsageId;
extern "C"  void PocoJsonSerializerStrategy__cctor_m3858986209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy__cctor_m3858986209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->set_EmptyTypes_3(((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)0)));
		TypeU5BU5D_t1664964607* L_0 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Int32_t2071877448_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_1);
		((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->set_ArrayConstructorParameterTypes_4(L_0);
		StringU5BU5D_t1642385972* L_2 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, _stringLiteral3879389748);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3879389748);
		StringU5BU5D_t1642385972* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, _stringLiteral3846594588);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3846594588);
		StringU5BU5D_t1642385972* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral87001591);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral87001591);
		((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->set_Iso8601Format_5(L_4);
		return;
	}
}
// System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String)
extern "C"  String_t* PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m1498630973 (PocoJsonSerializerStrategy_t2810850750 * __this, String_t* ___clrPropertyName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___clrPropertyName0;
		return L_0;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.PocoJsonSerializerStrategy::ContructorDelegateFactory(System.Type)
extern Il2CppClass* PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_ContructorDelegateFactory_m2249307191_MetadataUsageId;
extern "C"  ConstructorDelegate_t3084043859 * PocoJsonSerializerStrategy_ContructorDelegateFactory_m2249307191 (PocoJsonSerializerStrategy_t2810850750 * __this, Type_t * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_ContructorDelegateFactory_m2249307191_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * G_B2_0 = NULL;
	Type_t * G_B1_0 = NULL;
	TypeU5BU5D_t1664964607* G_B3_0 = NULL;
	Type_t * G_B3_1 = NULL;
	{
		Type_t * L_0 = ___key0;
		Type_t * L_1 = ___key0;
		NullCheck(L_1);
		bool L_2 = Type_get_IsArray_m811277129(L_1, /*hidden argument*/NULL);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var);
		TypeU5BU5D_t1664964607* L_3 = ((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->get_ArrayConstructorParameterTypes_4();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var);
		TypeU5BU5D_t1664964607* L_4 = ((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->get_EmptyTypes_3();
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		ConstructorDelegate_t3084043859 * L_5 = ReflectionUtils_GetContructor_m2213599410(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate> SimpleJson.PocoJsonSerializerStrategy::GetterValueFactory(System.Type)
extern Il2CppClass* Dictionary_2_t2267060895_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2545856110_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4024220188_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t266144316_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t547167195_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2025531273_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1845667710_MethodInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_GetterValueFactory_m3591081731_MetadataUsageId;
extern "C"  Il2CppObject* PocoJsonSerializerStrategy_GetterValueFactory_m3591081731 (PocoJsonSerializerStrategy_t2810850750 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_GetterValueFactory_m3591081731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	FieldInfo_t * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t2267060895 * L_0 = (Dictionary_2_t2267060895 *)il2cpp_codegen_object_new(Dictionary_2_t2267060895_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1845667710(L_0, /*hidden argument*/Dictionary_2__ctor_m1845667710_MethodInfo_var);
		V_0 = L_0;
		Type_t * L_1 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ReflectionUtils_GetProperties_m1086308370(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator() */, IEnumerable_1_t2545856110_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0063;
		}

IL_0017:
		{
			Il2CppObject* L_4 = V_2;
			NullCheck(L_4);
			PropertyInfo_t * L_5 = InterfaceFuncInvoker0< PropertyInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current() */, IEnumerator_1_t4024220188_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			PropertyInfo_t * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_6);
			if (!L_7)
			{
				goto IL_0063;
			}
		}

IL_0029:
		{
			PropertyInfo_t * L_8 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
			MethodInfo_t * L_9 = ReflectionUtils_GetGetterMethodInfo_m1539519564(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_3 = L_9;
			MethodInfo_t * L_10 = V_3;
			NullCheck(L_10);
			bool L_11 = MethodBase_get_IsStatic_m1015686807(L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0046;
			}
		}

IL_003b:
		{
			MethodInfo_t * L_12 = V_3;
			NullCheck(L_12);
			bool L_13 = MethodBase_get_IsPublic_m479656180(L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_004b;
			}
		}

IL_0046:
		{
			goto IL_0063;
		}

IL_004b:
		{
			Il2CppObject* L_14 = V_0;
			PropertyInfo_t * L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_15);
			String_t* L_17 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_16);
			PropertyInfo_t * L_18 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
			GetDelegate_t352281633 * L_19 = ReflectionUtils_GetGetMethod_m3146646939(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			NullCheck(L_14);
			InterfaceActionInvoker2< String_t*, GetDelegate_t352281633 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(!0,!1) */, IDictionary_2_t266144316_il2cpp_TypeInfo_var, L_14, L_17, L_19);
		}

IL_0063:
		{
			Il2CppObject* L_20 = V_2;
			NullCheck(L_20);
			bool L_21 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0017;
			}
		}

IL_006e:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0073);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0073;
	}

FINALLY_0073:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_22 = V_2;
			if (L_22)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(115)
		}

IL_0077:
		{
			Il2CppObject* L_23 = V_2;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_23);
			IL2CPP_END_FINALLY(115)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(115)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007e:
	{
		Type_t * L_24 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		Il2CppObject* L_25 = ReflectionUtils_GetFields_m771042561(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Il2CppObject* L_26 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator() */, IEnumerable_1_t547167195_il2cpp_TypeInfo_var, L_25);
		V_5 = L_26;
	}

IL_008b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00d0;
		}

IL_0090:
		{
			Il2CppObject* L_27 = V_5;
			NullCheck(L_27);
			FieldInfo_t * L_28 = InterfaceFuncInvoker0< FieldInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current() */, IEnumerator_1_t2025531273_il2cpp_TypeInfo_var, L_27);
			V_4 = L_28;
			FieldInfo_t * L_29 = V_4;
			NullCheck(L_29);
			bool L_30 = FieldInfo_get_IsStatic_m1411173225(L_29, /*hidden argument*/NULL);
			if (L_30)
			{
				goto IL_00b1;
			}
		}

IL_00a5:
		{
			FieldInfo_t * L_31 = V_4;
			NullCheck(L_31);
			bool L_32 = FieldInfo_get_IsPublic_m618896610(L_31, /*hidden argument*/NULL);
			if (L_32)
			{
				goto IL_00b6;
			}
		}

IL_00b1:
		{
			goto IL_00d0;
		}

IL_00b6:
		{
			Il2CppObject* L_33 = V_0;
			FieldInfo_t * L_34 = V_4;
			NullCheck(L_34);
			String_t* L_35 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_34);
			String_t* L_36 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_35);
			FieldInfo_t * L_37 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
			GetDelegate_t352281633 * L_38 = ReflectionUtils_GetGetMethod_m2453322602(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			NullCheck(L_33);
			InterfaceActionInvoker2< String_t*, GetDelegate_t352281633 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(!0,!1) */, IDictionary_2_t266144316_il2cpp_TypeInfo_var, L_33, L_36, L_38);
		}

IL_00d0:
		{
			Il2CppObject* L_39 = V_5;
			NullCheck(L_39);
			bool L_40 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_39);
			if (L_40)
			{
				goto IL_0090;
			}
		}

IL_00dc:
		{
			IL2CPP_LEAVE(0xEE, FINALLY_00e1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00e1;
	}

FINALLY_00e1:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_41 = V_5;
			if (L_41)
			{
				goto IL_00e6;
			}
		}

IL_00e5:
		{
			IL2CPP_END_FINALLY(225)
		}

IL_00e6:
		{
			Il2CppObject* L_42 = V_5;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_42);
			IL2CPP_END_FINALLY(225)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(225)
	{
		IL2CPP_JUMP_TBL(0xEE, IL_00ee)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00ee:
	{
		Il2CppObject* L_43 = V_0;
		return L_43;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>> SimpleJson.PocoJsonSerializerStrategy::SetterValueFactory(System.Type)
extern Il2CppClass* Dictionary_2_t1520880194_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2545856110_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t4024220188_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t3814930911_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t547167195_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t2025531273_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2749652828_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m3394507420_MethodInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_SetterValueFactory_m4054347921_MetadataUsageId;
extern "C"  Il2CppObject* PocoJsonSerializerStrategy_SetterValueFactory_m4054347921 (PocoJsonSerializerStrategy_t2810850750 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_SetterValueFactory_m4054347921_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	PropertyInfo_t * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	MethodInfo_t * V_3 = NULL;
	FieldInfo_t * V_4 = NULL;
	Il2CppObject* V_5 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1520880194 * L_0 = (Dictionary_2_t1520880194 *)il2cpp_codegen_object_new(Dictionary_2_t1520880194_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2749652828(L_0, /*hidden argument*/Dictionary_2__ctor_m2749652828_MethodInfo_var);
		V_0 = L_0;
		Type_t * L_1 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		Il2CppObject* L_2 = ReflectionUtils_GetProperties_m1086308370(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator() */, IEnumerable_1_t2545856110_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0017:
		{
			Il2CppObject* L_4 = V_2;
			NullCheck(L_4);
			PropertyInfo_t * L_5 = InterfaceFuncInvoker0< PropertyInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current() */, IEnumerator_1_t4024220188_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			PropertyInfo_t * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_6);
			if (!L_7)
			{
				goto IL_006e;
			}
		}

IL_0029:
		{
			PropertyInfo_t * L_8 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
			MethodInfo_t * L_9 = ReflectionUtils_GetSetterMethodInfo_m1933298824(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_3 = L_9;
			MethodInfo_t * L_10 = V_3;
			NullCheck(L_10);
			bool L_11 = MethodBase_get_IsStatic_m1015686807(L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0046;
			}
		}

IL_003b:
		{
			MethodInfo_t * L_12 = V_3;
			NullCheck(L_12);
			bool L_13 = MethodBase_get_IsPublic_m479656180(L_12, /*hidden argument*/NULL);
			if (L_13)
			{
				goto IL_004b;
			}
		}

IL_0046:
		{
			goto IL_006e;
		}

IL_004b:
		{
			Il2CppObject* L_14 = V_0;
			PropertyInfo_t * L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_15);
			String_t* L_17 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_16);
			PropertyInfo_t * L_18 = V_1;
			NullCheck(L_18);
			Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_18);
			PropertyInfo_t * L_20 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
			SetDelegate_t4206365109 * L_21 = ReflectionUtils_GetSetMethod_m2416528667(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			KeyValuePair_2_t3901068228  L_22;
			memset(&L_22, 0, sizeof(L_22));
			KeyValuePair_2__ctor_m3394507420(&L_22, L_19, L_21, /*hidden argument*/KeyValuePair_2__ctor_m3394507420_MethodInfo_var);
			NullCheck(L_14);
			InterfaceActionInvoker2< String_t*, KeyValuePair_2_t3901068228  >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Item(!0,!1) */, IDictionary_2_t3814930911_il2cpp_TypeInfo_var, L_14, L_17, L_22);
		}

IL_006e:
		{
			Il2CppObject* L_23 = V_2;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_0017;
			}
		}

IL_0079:
		{
			IL2CPP_LEAVE(0x89, FINALLY_007e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007e;
	}

FINALLY_007e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_25 = V_2;
			if (L_25)
			{
				goto IL_0082;
			}
		}

IL_0081:
		{
			IL2CPP_END_FINALLY(126)
		}

IL_0082:
		{
			Il2CppObject* L_26 = V_2;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_26);
			IL2CPP_END_FINALLY(126)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(126)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0089:
	{
		Type_t * L_27 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		Il2CppObject* L_28 = ReflectionUtils_GetFields_m771042561(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Il2CppObject* L_29 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator() */, IEnumerable_1_t547167195_il2cpp_TypeInfo_var, L_28);
		V_5 = L_29;
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f3;
		}

IL_009b:
		{
			Il2CppObject* L_30 = V_5;
			NullCheck(L_30);
			FieldInfo_t * L_31 = InterfaceFuncInvoker0< FieldInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current() */, IEnumerator_1_t2025531273_il2cpp_TypeInfo_var, L_30);
			V_4 = L_31;
			FieldInfo_t * L_32 = V_4;
			NullCheck(L_32);
			bool L_33 = FieldInfo_get_IsInitOnly_m3059790683(L_32, /*hidden argument*/NULL);
			if (L_33)
			{
				goto IL_00c8;
			}
		}

IL_00b0:
		{
			FieldInfo_t * L_34 = V_4;
			NullCheck(L_34);
			bool L_35 = FieldInfo_get_IsStatic_m1411173225(L_34, /*hidden argument*/NULL);
			if (L_35)
			{
				goto IL_00c8;
			}
		}

IL_00bc:
		{
			FieldInfo_t * L_36 = V_4;
			NullCheck(L_36);
			bool L_37 = FieldInfo_get_IsPublic_m618896610(L_36, /*hidden argument*/NULL);
			if (L_37)
			{
				goto IL_00cd;
			}
		}

IL_00c8:
		{
			goto IL_00f3;
		}

IL_00cd:
		{
			Il2CppObject* L_38 = V_0;
			FieldInfo_t * L_39 = V_4;
			NullCheck(L_39);
			String_t* L_40 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_39);
			String_t* L_41 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_40);
			FieldInfo_t * L_42 = V_4;
			NullCheck(L_42);
			Type_t * L_43 = VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_42);
			FieldInfo_t * L_44 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
			SetDelegate_t4206365109 * L_45 = ReflectionUtils_GetSetMethod_m3627950826(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
			KeyValuePair_2_t3901068228  L_46;
			memset(&L_46, 0, sizeof(L_46));
			KeyValuePair_2__ctor_m3394507420(&L_46, L_43, L_45, /*hidden argument*/KeyValuePair_2__ctor_m3394507420_MethodInfo_var);
			NullCheck(L_38);
			InterfaceActionInvoker2< String_t*, KeyValuePair_2_t3901068228  >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Item(!0,!1) */, IDictionary_2_t3814930911_il2cpp_TypeInfo_var, L_38, L_41, L_46);
		}

IL_00f3:
		{
			Il2CppObject* L_47 = V_5;
			NullCheck(L_47);
			bool L_48 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_47);
			if (L_48)
			{
				goto IL_009b;
			}
		}

IL_00ff:
		{
			IL2CPP_LEAVE(0x111, FINALLY_0104);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0104;
	}

FINALLY_0104:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_49 = V_5;
			if (L_49)
			{
				goto IL_0109;
			}
		}

IL_0108:
		{
			IL2CPP_END_FINALLY(260)
		}

IL_0109:
		{
			Il2CppObject* L_50 = V_5;
			NullCheck(L_50);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_50);
			IL2CPP_END_FINALLY(260)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(260)
	{
		IL2CPP_JUMP_TBL(0x111, IL_0111)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0111:
	{
		Il2CppObject* L_51 = V_0;
		return L_51;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&)
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m741807998 (PocoJsonSerializerStrategy_t2810850750 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Il2CppObject * L_0 = ___input0;
		Il2CppObject ** L_1 = ___output1;
		bool L_2 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(11 /* System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&) */, __this, L_0, L_1);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_3 = ___input0;
		Il2CppObject ** L_4 = ___output1;
		bool L_5 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(12 /* System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&) */, __this, L_3, L_4);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t PocoJsonSerializerStrategy_SerializeEnum_m387099298_MetadataUsageId;
extern "C"  Il2CppObject * PocoJsonSerializerStrategy_SerializeEnum_m387099298 (PocoJsonSerializerStrategy_t2810850750 * __this, Enum_t2459695545 * ___p0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_SerializeEnum_m387099298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enum_t2459695545 * L_0 = ___p0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_1 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_2 = Convert_ToDouble_m574888941(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		double L_3 = L_2;
		Il2CppObject * L_4 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern Il2CppClass* DateTime_t693205669_il2cpp_TypeInfo_var;
extern Il2CppClass* PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* DateTimeOffset_t1362988906_il2cpp_TypeInfo_var;
extern Il2CppClass* Guid_t2533601593_il2cpp_TypeInfo_var;
extern Il2CppClass* Uri_t19570940_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029402;
extern const uint32_t PocoJsonSerializerStrategy_TrySerializeKnownTypes_m1354296985_MetadataUsageId;
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeKnownTypes_m1354296985 (PocoJsonSerializerStrategy_t2810850750 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_TrySerializeKnownTypes_m1354296985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Enum_t2459695545 * V_1 = NULL;
	DateTime_t693205669  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t693205669  V_3;
	memset(&V_3, 0, sizeof(V_3));
	DateTimeOffset_t1362988906  V_4;
	memset(&V_4, 0, sizeof(V_4));
	DateTimeOffset_t1362988906  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Guid_t2533601593  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		V_0 = (bool)1;
		Il2CppObject * L_0 = ___input0;
		if (!((Il2CppObject *)IsInstSealed(L_0, DateTime_t693205669_il2cpp_TypeInfo_var)))
		{
			goto IL_0036;
		}
	}
	{
		Il2CppObject ** L_1 = ___output1;
		Il2CppObject * L_2 = ___input0;
		V_2 = ((*(DateTime_t693205669 *)((DateTime_t693205669 *)UnBox (L_2, DateTime_t693205669_il2cpp_TypeInfo_var))));
		DateTime_t693205669  L_3 = DateTime_ToUniversalTime_m1815024752((&V_2), /*hidden argument*/NULL);
		V_3 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var);
		StringU5BU5D_t1642385972* L_4 = ((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->get_Iso8601Format_5();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		String_t* L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_7 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = DateTime_ToString_m2276663647((&V_3), L_6, L_7, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_1)) = (Il2CppObject *)L_8;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_1), (Il2CppObject *)L_8);
		goto IL_00ca;
	}

IL_0036:
	{
		Il2CppObject * L_9 = ___input0;
		if (!((Il2CppObject *)IsInstSealed(L_9, DateTimeOffset_t1362988906_il2cpp_TypeInfo_var)))
		{
			goto IL_006c;
		}
	}
	{
		Il2CppObject ** L_10 = ___output1;
		Il2CppObject * L_11 = ___input0;
		V_4 = ((*(DateTimeOffset_t1362988906 *)((DateTimeOffset_t1362988906 *)UnBox (L_11, DateTimeOffset_t1362988906_il2cpp_TypeInfo_var))));
		DateTimeOffset_t1362988906  L_12 = DateTimeOffset_ToUniversalTime_m3836057040((&V_4), /*hidden argument*/NULL);
		V_5 = L_12;
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var);
		StringU5BU5D_t1642385972* L_13 = ((PocoJsonSerializerStrategy_t2810850750_StaticFields*)PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var->static_fields)->get_Iso8601Format_5();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		String_t* L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_16 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_17 = DateTimeOffset_ToString_m2121173678((&V_5), L_15, L_16, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_10)) = (Il2CppObject *)L_17;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_10), (Il2CppObject *)L_17);
		goto IL_00ca;
	}

IL_006c:
	{
		Il2CppObject * L_18 = ___input0;
		if (!((Il2CppObject *)IsInstSealed(L_18, Guid_t2533601593_il2cpp_TypeInfo_var)))
		{
			goto IL_0092;
		}
	}
	{
		Il2CppObject ** L_19 = ___output1;
		Il2CppObject * L_20 = ___input0;
		V_6 = ((*(Guid_t2533601593 *)((Guid_t2533601593 *)UnBox (L_20, Guid_t2533601593_il2cpp_TypeInfo_var))));
		String_t* L_21 = Guid_ToString_m51661589((&V_6), _stringLiteral372029402, /*hidden argument*/NULL);
		*((Il2CppObject **)(L_19)) = (Il2CppObject *)L_21;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_19), (Il2CppObject *)L_21);
		goto IL_00ca;
	}

IL_0092:
	{
		Il2CppObject * L_22 = ___input0;
		if (!((Uri_t19570940 *)IsInstClass(L_22, Uri_t19570940_il2cpp_TypeInfo_var)))
		{
			goto IL_00aa;
		}
	}
	{
		Il2CppObject ** L_23 = ___output1;
		Il2CppObject * L_24 = ___input0;
		NullCheck(L_24);
		String_t* L_25 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		*((Il2CppObject **)(L_23)) = (Il2CppObject *)L_25;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_23), (Il2CppObject *)L_25);
		goto IL_00ca;
	}

IL_00aa:
	{
		Il2CppObject * L_26 = ___input0;
		V_1 = ((Enum_t2459695545 *)IsInstClass(L_26, Enum_t2459695545_il2cpp_TypeInfo_var));
		Enum_t2459695545 * L_27 = V_1;
		if (!L_27)
		{
			goto IL_00c5;
		}
	}
	{
		Il2CppObject ** L_28 = ___output1;
		Enum_t2459695545 * L_29 = V_1;
		Il2CppObject * L_30 = VirtFuncInvoker1< Il2CppObject *, Enum_t2459695545 * >::Invoke(10 /* System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum) */, __this, L_29);
		*((Il2CppObject **)(L_28)) = (Il2CppObject *)L_30;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_28), (Il2CppObject *)L_30);
		goto IL_00ca;
	}

IL_00c5:
	{
		V_0 = (bool)0;
		Il2CppObject ** L_31 = ___output1;
		*((Il2CppObject **)(L_31)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_31), (Il2CppObject *)NULL);
	}

IL_00ca:
	{
		bool L_32 = V_0;
		return L_32;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppClass* JsonObject_t2300545015_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t202585634_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t316533162_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1794897240_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m360278047_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m4275643388_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral748179678;
extern const uint32_t PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4062772870_MetadataUsageId;
extern "C"  bool PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4062772870 (PocoJsonSerializerStrategy_t2810850750 * __this, Il2CppObject * ___input0, Il2CppObject ** ___output1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m4062772870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	KeyValuePair_2_t24406117  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Il2CppObject* V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject * L_0 = ___input0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral748179678, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Il2CppObject ** L_2 = ___output1;
		*((Il2CppObject **)(L_2)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_2), (Il2CppObject *)NULL);
		Il2CppObject * L_3 = ___input0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Type_t * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_5);
		if (L_6)
		{
			goto IL_0028;
		}
	}
	{
		return (bool)0;
	}

IL_0028:
	{
		JsonObject_t2300545015 * L_7 = (JsonObject_t2300545015 *)il2cpp_codegen_object_new(JsonObject_t2300545015_il2cpp_TypeInfo_var);
		JsonObject__ctor_m2062444459(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		Il2CppObject* L_8 = __this->get_GetCache_1();
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		Il2CppObject* L_10 = InterfaceFuncInvoker1< Il2CppObject*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Item(!0) */, IDictionary_2_t202585634_il2cpp_TypeInfo_var, L_8, L_9);
		V_2 = L_10;
		Il2CppObject* L_11 = V_2;
		NullCheck(L_11);
		Il2CppObject* L_12 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::GetEnumerator() */, IEnumerable_1_t316533162_il2cpp_TypeInfo_var, L_11);
		V_4 = L_12;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007c;
		}

IL_0048:
		{
			Il2CppObject* L_13 = V_4;
			NullCheck(L_13);
			KeyValuePair_2_t24406117  L_14 = InterfaceFuncInvoker0< KeyValuePair_2_t24406117  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Current() */, IEnumerator_1_t1794897240_il2cpp_TypeInfo_var, L_13);
			V_3 = L_14;
			GetDelegate_t352281633 * L_15 = KeyValuePair_2_get_Value_m360278047((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m360278047_MethodInfo_var);
			if (!L_15)
			{
				goto IL_007c;
			}
		}

IL_005c:
		{
			Il2CppObject* L_16 = V_1;
			String_t* L_17 = KeyValuePair_2_get_Key_m4275643388((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m4275643388_MethodInfo_var);
			String_t* L_18 = VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_17);
			GetDelegate_t352281633 * L_19 = KeyValuePair_2_get_Value_m360278047((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m360278047_MethodInfo_var);
			Il2CppObject * L_20 = ___input0;
			NullCheck(L_19);
			Il2CppObject * L_21 = GetDelegate_Invoke_m528436722(L_19, L_20, /*hidden argument*/NULL);
			NullCheck(L_16);
			InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::Add(!0,!1) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_16, L_18, L_21);
		}

IL_007c:
		{
			Il2CppObject* L_22 = V_4;
			NullCheck(L_22);
			bool L_23 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_22);
			if (L_23)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0x9A, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_24 = V_4;
			if (L_24)
			{
				goto IL_0092;
			}
		}

IL_0091:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0092:
		{
			Il2CppObject* L_25 = V_4;
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_25);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_009a:
	{
		Il2CppObject ** L_26 = ___output1;
		Il2CppObject* L_27 = V_1;
		*((Il2CppObject **)(L_26)) = (Il2CppObject *)L_27;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_26), (Il2CppObject *)L_27);
		return (bool)1;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils::.cctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils__cctor_m3529209954_MetadataUsageId;
extern "C"  void ReflectionUtils__cctor_m3529209954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils__cctor_m3529209954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ReflectionUtils_t3032483338_StaticFields*)ReflectionUtils_t3032483338_il2cpp_TypeInfo_var->static_fields)->set_EmptyObjects_0(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo> SimpleJson.Reflection.ReflectionUtils::GetConstructors(System.Type)
extern "C"  Il2CppObject* ReflectionUtils_GetConstructors_m2419931237 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		ConstructorInfoU5BU5D_t1996683371* L_1 = Type_GetConstructors_m52202211(L_0, /*hidden argument*/NULL);
		return (Il2CppObject*)L_1;
	}
}
// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils::GetConstructorInfo(System.Type,System.Type[])
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t3143943587_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t327340369_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetConstructorInfo_m577110104_MetadataUsageId;
extern "C"  ConstructorInfo_t2851816542 * ReflectionUtils_GetConstructorInfo_m577110104 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t1664964607* ___argsType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetConstructorInfo_m577110104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	ConstructorInfo_t2851816542 * V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	ParameterInfoU5BU5D_t2275869610* V_5 = NULL;
	ParameterInfo_t2249040075 * V_6 = NULL;
	ParameterInfoU5BU5D_t2275869610* V_7 = NULL;
	int32_t V_8 = 0;
	ConstructorInfo_t2851816542 * V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		Il2CppObject* L_1 = ReflectionUtils_GetConstructors_m2419931237(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject* L_2 = V_0;
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>::GetEnumerator() */, IEnumerable_1_t3143943587_il2cpp_TypeInfo_var, L_2);
		V_4 = L_3;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0085;
		}

IL_0014:
		{
			Il2CppObject* L_4 = V_4;
			NullCheck(L_4);
			ConstructorInfo_t2851816542 * L_5 = InterfaceFuncInvoker0< ConstructorInfo_t2851816542 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.ConstructorInfo>::get_Current() */, IEnumerator_1_t327340369_il2cpp_TypeInfo_var, L_4);
			V_3 = L_5;
			ConstructorInfo_t2851816542 * L_6 = V_3;
			NullCheck(L_6);
			ParameterInfoU5BU5D_t2275869610* L_7 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_6);
			V_5 = L_7;
			TypeU5BU5D_t1664964607* L_8 = ___argsType1;
			NullCheck(L_8);
			ParameterInfoU5BU5D_t2275869610* L_9 = V_5;
			NullCheck(L_9);
			if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
			{
				goto IL_0035;
			}
		}

IL_0030:
		{
			goto IL_0085;
		}

IL_0035:
		{
			V_1 = 0;
			V_2 = (bool)1;
			ConstructorInfo_t2851816542 * L_10 = V_3;
			NullCheck(L_10);
			ParameterInfoU5BU5D_t2275869610* L_11 = VirtFuncInvoker0< ParameterInfoU5BU5D_t2275869610* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_10);
			V_7 = L_11;
			V_8 = 0;
			goto IL_006c;
		}

IL_0049:
		{
			ParameterInfoU5BU5D_t2275869610* L_12 = V_7;
			int32_t L_13 = V_8;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
			int32_t L_14 = L_13;
			ParameterInfo_t2249040075 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
			V_6 = L_15;
			ParameterInfo_t2249040075 * L_16 = V_6;
			NullCheck(L_16);
			Type_t * L_17 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_16);
			TypeU5BU5D_t1664964607* L_18 = ___argsType1;
			int32_t L_19 = V_1;
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
			int32_t L_20 = L_19;
			Type_t * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
			if ((((Il2CppObject*)(Type_t *)L_17) == ((Il2CppObject*)(Type_t *)L_21)))
			{
				goto IL_0066;
			}
		}

IL_005f:
		{
			V_2 = (bool)0;
			goto IL_0077;
		}

IL_0066:
		{
			int32_t L_22 = V_8;
			V_8 = ((int32_t)((int32_t)L_22+(int32_t)1));
		}

IL_006c:
		{
			int32_t L_23 = V_8;
			ParameterInfoU5BU5D_t2275869610* L_24 = V_7;
			NullCheck(L_24);
			if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length)))))))
			{
				goto IL_0049;
			}
		}

IL_0077:
		{
			bool L_25 = V_2;
			if (!L_25)
			{
				goto IL_0085;
			}
		}

IL_007d:
		{
			ConstructorInfo_t2851816542 * L_26 = V_3;
			V_9 = L_26;
			IL2CPP_LEAVE(0xA5, FINALLY_0096);
		}

IL_0085:
		{
			Il2CppObject* L_27 = V_4;
			NullCheck(L_27);
			bool L_28 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_27);
			if (L_28)
			{
				goto IL_0014;
			}
		}

IL_0091:
		{
			IL2CPP_LEAVE(0xA3, FINALLY_0096);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0096;
	}

FINALLY_0096:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_29 = V_4;
			if (L_29)
			{
				goto IL_009b;
			}
		}

IL_009a:
		{
			IL2CPP_END_FINALLY(150)
		}

IL_009b:
		{
			Il2CppObject* L_30 = V_4;
			NullCheck(L_30);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_30);
			IL2CPP_END_FINALLY(150)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(150)
	{
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_JUMP_TBL(0xA3, IL_00a3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a3:
	{
		return (ConstructorInfo_t2851816542 *)NULL;
	}

IL_00a5:
	{
		ConstructorInfo_t2851816542 * L_31 = V_9;
		return L_31;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> SimpleJson.Reflection.ReflectionUtils::GetProperties(System.Type)
extern "C"  Il2CppObject* ReflectionUtils_GetProperties_m1086308370 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		PropertyInfoU5BU5D_t1736152084* L_1 = VirtFuncInvoker1< PropertyInfoU5BU5D_t1736152084*, int32_t >::Invoke(54 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_0, ((int32_t)60));
		return (Il2CppObject*)L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> SimpleJson.Reflection.ReflectionUtils::GetFields(System.Type)
extern "C"  Il2CppObject* ReflectionUtils_GetFields_m771042561 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		NullCheck(L_0);
		FieldInfoU5BU5D_t125053523* L_1 = VirtFuncInvoker1< FieldInfoU5BU5D_t125053523*, int32_t >::Invoke(46 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_0, ((int32_t)60));
		return (Il2CppObject*)L_1;
	}
}
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetGetterMethodInfo(System.Reflection.PropertyInfo)
extern "C"  MethodInfo_t * ReflectionUtils_GetGetterMethodInfo_m1539519564 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = ___propertyInfo0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(19 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_0, (bool)1);
		return L_1;
	}
}
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetSetterMethodInfo(System.Reflection.PropertyInfo)
extern "C"  MethodInfo_t * ReflectionUtils_GetSetterMethodInfo_m1933298824 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = ___propertyInfo0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(21 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_0, (bool)1);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetContructor(System.Type,System.Type[])
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetContructor_m2213599410_MetadataUsageId;
extern "C"  ConstructorDelegate_t3084043859 * ReflectionUtils_GetContructor_m2213599410 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t1664964607* ___argsType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetContructor_m2213599410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type0;
		TypeU5BU5D_t1664964607* L_1 = ___argsType1;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		ConstructorDelegate_t3084043859 * L_2 = ReflectionUtils_GetConstructorByReflection_m2587709881(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Reflection.ConstructorInfo)
extern Il2CppClass* U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstructorDelegate_t3084043859_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetConstructorByReflectionU3Ec__AnonStorey2_U3CU3Em__6_m3432422969_MethodInfo_var;
extern const uint32_t ReflectionUtils_GetConstructorByReflection_m2214455175_MetadataUsageId;
extern "C"  ConstructorDelegate_t3084043859 * ReflectionUtils_GetConstructorByReflection_m2214455175 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t2851816542 * ___constructorInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetConstructorByReflection_m2214455175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * V_0 = NULL;
	{
		U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * L_0 = (U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 *)il2cpp_codegen_object_new(U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565_il2cpp_TypeInfo_var);
		U3CGetConstructorByReflectionU3Ec__AnonStorey2__ctor_m3579372293(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * L_1 = V_0;
		ConstructorInfo_t2851816542 * L_2 = ___constructorInfo0;
		NullCheck(L_1);
		L_1->set_constructorInfo_0(L_2);
		U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CGetConstructorByReflectionU3Ec__AnonStorey2_U3CU3Em__6_m3432422969_MethodInfo_var);
		ConstructorDelegate_t3084043859 * L_5 = (ConstructorDelegate_t3084043859 *)il2cpp_codegen_object_new(ConstructorDelegate_t3084043859_il2cpp_TypeInfo_var);
		ConstructorDelegate__ctor_m2500175747(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Type,System.Type[])
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetConstructorByReflection_m2587709881_MetadataUsageId;
extern "C"  ConstructorDelegate_t3084043859 * ReflectionUtils_GetConstructorByReflection_m2587709881 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, TypeU5BU5D_t1664964607* ___argsType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetConstructorByReflection_m2587709881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfo_t2851816542 * V_0 = NULL;
	ConstructorDelegate_t3084043859 * G_B3_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		TypeU5BU5D_t1664964607* L_1 = ___argsType1;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		ConstructorInfo_t2851816542 * L_2 = ReflectionUtils_GetConstructorInfo_m577110104(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ConstructorInfo_t2851816542 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0014;
		}
	}
	{
		G_B3_0 = ((ConstructorDelegate_t3084043859 *)(NULL));
		goto IL_001a;
	}

IL_0014:
	{
		ConstructorInfo_t2851816542 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		ConstructorDelegate_t3084043859 * L_5 = ReflectionUtils_GetConstructorByReflection_m2214455175(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.PropertyInfo)
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetGetMethod_m3146646939_MetadataUsageId;
extern "C"  GetDelegate_t352281633 * ReflectionUtils_GetGetMethod_m3146646939 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetGetMethod_m3146646939_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PropertyInfo_t * L_0 = ___propertyInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		GetDelegate_t352281633 * L_1 = ReflectionUtils_GetGetMethodByReflection_m436060527(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.FieldInfo)
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetGetMethod_m2453322602_MetadataUsageId;
extern "C"  GetDelegate_t352281633 * ReflectionUtils_GetGetMethod_m2453322602 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetGetMethod_m2453322602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FieldInfo_t * L_0 = ___fieldInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		GetDelegate_t352281633 * L_1 = ReflectionUtils_GetGetMethodByReflection_m4176955894(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.PropertyInfo)
extern Il2CppClass* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern Il2CppClass* GetDelegate_t352281633_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__7_m3800765770_MethodInfo_var;
extern const uint32_t ReflectionUtils_GetGetMethodByReflection_m436060527_MetadataUsageId;
extern "C"  GetDelegate_t352281633 * ReflectionUtils_GetGetMethodByReflection_m436060527 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetGetMethodByReflection_m436060527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 * V_0 = NULL;
	{
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 * L_0 = (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 *)il2cpp_codegen_object_new(U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133_il2cpp_TypeInfo_var);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m3347821541(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 * L_1 = V_0;
		PropertyInfo_t * L_2 = ___propertyInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		MethodInfo_t * L_3 = ReflectionUtils_GetGetterMethodInfo_m1539519564(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_methodInfo_0(L_3);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__7_m3800765770_MethodInfo_var);
		GetDelegate_t352281633 * L_6 = (GetDelegate_t352281633 *)il2cpp_codegen_object_new(GetDelegate_t352281633_il2cpp_TypeInfo_var);
		GetDelegate__ctor_m2792866377(L_6, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.FieldInfo)
extern Il2CppClass* U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434_il2cpp_TypeInfo_var;
extern Il2CppClass* GetDelegate_t352281633_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__8_m2501469858_MethodInfo_var;
extern const uint32_t ReflectionUtils_GetGetMethodByReflection_m4176955894_MetadataUsageId;
extern "C"  GetDelegate_t352281633 * ReflectionUtils_GetGetMethodByReflection_m4176955894 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetGetMethodByReflection_m4176955894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * V_0 = NULL;
	{
		U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * L_0 = (U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 *)il2cpp_codegen_object_new(U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434_il2cpp_TypeInfo_var);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey4__ctor_m555912566(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * L_1 = V_0;
		FieldInfo_t * L_2 = ___fieldInfo0;
		NullCheck(L_1);
		L_1->set_fieldInfo_0(L_2);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CGetGetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__8_m2501469858_MethodInfo_var);
		GetDelegate_t352281633 * L_5 = (GetDelegate_t352281633 *)il2cpp_codegen_object_new(GetDelegate_t352281633_il2cpp_TypeInfo_var);
		GetDelegate__ctor_m2792866377(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.PropertyInfo)
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetSetMethod_m2416528667_MetadataUsageId;
extern "C"  SetDelegate_t4206365109 * ReflectionUtils_GetSetMethod_m2416528667 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetSetMethod_m2416528667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PropertyInfo_t * L_0 = ___propertyInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		SetDelegate_t4206365109 * L_1 = ReflectionUtils_GetSetMethodByReflection_m45132015(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.FieldInfo)
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t ReflectionUtils_GetSetMethod_m3627950826_MetadataUsageId;
extern "C"  SetDelegate_t4206365109 * ReflectionUtils_GetSetMethod_m3627950826 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetSetMethod_m3627950826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		FieldInfo_t * L_0 = ___fieldInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		SetDelegate_t4206365109 * L_1 = ReflectionUtils_GetSetMethodByReflection_m2697613686(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.PropertyInfo)
extern Il2CppClass* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595_il2cpp_TypeInfo_var;
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern Il2CppClass* SetDelegate_t4206365109_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__9_m1719902469_MethodInfo_var;
extern const uint32_t ReflectionUtils_GetSetMethodByReflection_m45132015_MetadataUsageId;
extern "C"  SetDelegate_t4206365109 * ReflectionUtils_GetSetMethodByReflection_m45132015 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetSetMethodByReflection_m45132015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 * V_0 = NULL;
	{
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 * L_0 = (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 *)il2cpp_codegen_object_new(U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595_il2cpp_TypeInfo_var);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m2089198215(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 * L_1 = V_0;
		PropertyInfo_t * L_2 = ___propertyInfo0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		MethodInfo_t * L_3 = ReflectionUtils_GetSetterMethodInfo_m1933298824(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_methodInfo_0(L_3);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__9_m1719902469_MethodInfo_var);
		SetDelegate_t4206365109 * L_6 = (SetDelegate_t4206365109 *)il2cpp_codegen_object_new(SetDelegate_t4206365109_il2cpp_TypeInfo_var);
		SetDelegate__ctor_m3526263997(L_6, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.FieldInfo)
extern Il2CppClass* U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536_il2cpp_TypeInfo_var;
extern Il2CppClass* SetDelegate_t4206365109_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey6_U3CU3Em__A_m2382901294_MethodInfo_var;
extern const uint32_t ReflectionUtils_GetSetMethodByReflection_m2697613686_MetadataUsageId;
extern "C"  SetDelegate_t4206365109 * ReflectionUtils_GetSetMethodByReflection_m2697613686 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___fieldInfo0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReflectionUtils_GetSetMethodByReflection_m2697613686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * V_0 = NULL;
	{
		U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * L_0 = (U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 *)il2cpp_codegen_object_new(U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536_il2cpp_TypeInfo_var);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey6__ctor_m757389080(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * L_1 = V_0;
		FieldInfo_t * L_2 = ___fieldInfo0;
		NullCheck(L_1);
		L_1->set_fieldInfo_0(L_2);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CGetSetMethodByReflectionU3Ec__AnonStorey6_U3CU3Em__A_m2382901294_MethodInfo_var);
		SetDelegate_t4206365109 * L_5 = (SetDelegate_t4206365109 *)il2cpp_codegen_object_new(SetDelegate_t4206365109_il2cpp_TypeInfo_var);
		SetDelegate__ctor_m3526263997(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey2::.ctor()
extern "C"  void U3CGetConstructorByReflectionU3Ec__AnonStorey2__ctor_m3579372293 (U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey2::<>m__6(System.Object[])
extern "C"  Il2CppObject * U3CGetConstructorByReflectionU3Ec__AnonStorey2_U3CU3Em__6_m3432422969 (U3CGetConstructorByReflectionU3Ec__AnonStorey2_t1606021565 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		ConstructorInfo_t2851816542 * L_0 = __this->get_constructorInfo_0();
		ObjectU5BU5D_t3614634134* L_1 = ___args0;
		NullCheck(L_0);
		Il2CppObject * L_2 = ConstructorInfo_Invoke_m2144827141(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::.ctor()
extern "C"  void U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m3347821541 (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::<>m__7(System.Object)
extern Il2CppClass* ReflectionUtils_t3032483338_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__7_m3800765770_MetadataUsageId;
extern "C"  Il2CppObject * U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__7_m3800765770 (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1342095133 * __this, Il2CppObject * ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__7_m3800765770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MethodInfo_t * L_0 = __this->get_methodInfo_0();
		Il2CppObject * L_1 = ___source0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t3032483338_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t3614634134* L_2 = ((ReflectionUtils_t3032483338_StaticFields*)ReflectionUtils_t3032483338_il2cpp_TypeInfo_var->static_fields)->get_EmptyObjects_0();
		NullCheck(L_0);
		Il2CppObject * L_3 = MethodBase_Invoke_m1075809207(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey4::.ctor()
extern "C"  void U3CGetGetMethodByReflectionU3Ec__AnonStorey4__ctor_m555912566 (U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey4::<>m__8(System.Object)
extern "C"  Il2CppObject * U3CGetGetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__8_m2501469858 (U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * __this, Il2CppObject * ___source0, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = __this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___source0;
		NullCheck(L_0);
		Il2CppObject * L_2 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::.ctor()
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m2089198215 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::<>m__9(System.Object,System.Object)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__9_m1719902469_MetadataUsageId;
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__9_m1719902469 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t2636512595 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__9_m1719902469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MethodInfo_t * L_0 = __this->get_methodInfo_0();
		Il2CppObject * L_1 = ___source0;
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		Il2CppObject * L_3 = ___value1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		NullCheck(L_0);
		MethodBase_Invoke_m1075809207(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey6::.ctor()
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey6__ctor_m757389080 (U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey6::<>m__A(System.Object,System.Object)
extern "C"  void U3CGetSetMethodByReflectionU3Ec__AnonStorey6_U3CU3Em__A_m2382901294 (U3CGetSetMethodByReflectionU3Ec__AnonStorey6_t4202596536 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = __this->get_fieldInfo_0();
		Il2CppObject * L_1 = ___source0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		FieldInfo_SetValue_m2504255891(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void ConstructorDelegate__ctor_m2500175747 (ConstructorDelegate_t3084043859 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::Invoke(System.Object[])
extern "C"  Il2CppObject * ConstructorDelegate_Invoke_m2493638242 (ConstructorDelegate_t3084043859 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ConstructorDelegate_Invoke_m2493638242((ConstructorDelegate_t3084043859 *)__this->get_prev_9(),___args0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___args0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ConstructorDelegate_BeginInvoke_m2710763296 (ConstructorDelegate_t3084043859 * __this, ObjectU5BU5D_t3614634134* ___args0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ConstructorDelegate_EndInvoke_m3645696558 (ConstructorDelegate_t3084043859 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void SimpleJson.Reflection.ReflectionUtils/GetDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GetDelegate__ctor_m2792866377 (GetDelegate_t352281633 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::Invoke(System.Object)
extern "C"  Il2CppObject * GetDelegate_Invoke_m528436722 (GetDelegate_t352281633 * __this, Il2CppObject * ___source0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		GetDelegate_Invoke_m528436722((GetDelegate_t352281633 *)__this->get_prev_9(),___source0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___source0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___source0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___source0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___source0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___source0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/GetDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetDelegate_BeginInvoke_m2948557404 (GetDelegate_t352281633 * __this, Il2CppObject * ___source0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___source0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * GetDelegate_EndInvoke_m1967267126 (GetDelegate_t352281633 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void SetDelegate__ctor_m3526263997 (SetDelegate_t4206365109 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::Invoke(System.Object,System.Object)
extern "C"  void SetDelegate_Invoke_m1756397093 (SetDelegate_t4206365109 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SetDelegate_Invoke_m1756397093((SetDelegate_t4206365109 *)__this->get_prev_9(),___source0, ___value1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___source0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___source0, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___source0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___source0, ___value1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/SetDelegate::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SetDelegate_BeginInvoke_m2186054362 (SetDelegate_t4206365109 * __this, Il2CppObject * ___source0, Il2CppObject * ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___source0;
	__d_args[1] = ___value1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void SetDelegate_EndInvoke_m382620167 (SetDelegate_t4206365109 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Object SimpleJson.SimpleJson::DeserializeObject(System.String)
extern Il2CppClass* SerializationException_t753258759_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3720450562;
extern const uint32_t SimpleJson_DeserializeObject_m4008938349_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_DeserializeObject_m4008938349 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_DeserializeObject_m4008938349_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		String_t* L_0 = ___json0;
		bool L_1 = SimpleJson_TryDeserializeObject_m2024713247(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}

IL_000f:
	{
		SerializationException_t753258759 * L_3 = (SerializationException_t753258759 *)il2cpp_codegen_object_new(SerializationException_t753258759_il2cpp_TypeInfo_var);
		SerializationException__ctor_m1019897788(L_3, _stringLiteral3720450562, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Boolean SimpleJson.SimpleJson::TryDeserializeObject(System.String,System.Object&)
extern "C"  bool SimpleJson_TryDeserializeObject_m2024713247 (Il2CppObject * __this /* static, unused */, String_t* ___json0, Il2CppObject ** ___obj1, const MethodInfo* method)
{
	bool V_0 = false;
	CharU5BU5D_t1328083999* V_1 = NULL;
	int32_t V_2 = 0;
	{
		V_0 = (bool)1;
		String_t* L_0 = ___json0;
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_1 = ___json0;
		NullCheck(L_1);
		CharU5BU5D_t1328083999* L_2 = String_ToCharArray_m870309954(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		V_2 = 0;
		Il2CppObject ** L_3 = ___obj1;
		CharU5BU5D_t1328083999* L_4 = V_1;
		Il2CppObject * L_5 = SimpleJson_ParseValue_m1002395532(NULL /*static, unused*/, L_4, (&V_2), (&V_0), /*hidden argument*/NULL);
		*((Il2CppObject **)(L_3)) = (Il2CppObject *)L_5;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_3), (Il2CppObject *)L_5);
		goto IL_0025;
	}

IL_0022:
	{
		Il2CppObject ** L_6 = ___obj1;
		*((Il2CppObject **)(L_6)) = (Il2CppObject *)NULL;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)(L_6), (Il2CppObject *)NULL);
	}

IL_0025:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object,SimpleJson.IJsonSerializerStrategy)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_SerializeObject_m3389162840_MetadataUsageId;
extern "C"  String_t* SimpleJson_SerializeObject_m3389162840 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, Il2CppObject * ___jsonSerializerStrategy1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeObject_m3389162840_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	bool V_1 = false;
	String_t* G_B3_0 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = ___jsonSerializerStrategy1;
		Il2CppObject * L_2 = ___json0;
		StringBuilder_t1221177846 * L_3 = V_0;
		bool L_4 = SimpleJson_SerializeValue_m2700795577(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0025;
		}
	}
	{
		StringBuilder_t1221177846 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = StringBuilder_ToString_m1507807375(L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object)
extern "C"  String_t* SimpleJson_SerializeObject_m478839316 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___json0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___json0;
		Il2CppObject * L_1 = SimpleJson_get_CurrentJsonSerializerStrategy_m3073481146(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = SimpleJson_SerializeObject_m3389162840(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.Object> SimpleJson.SimpleJson::ParseObject(System.Char[],System.Int32&,System.Boolean&)
extern Il2CppClass* JsonObject_t2300545015_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_ParseObject_m2464058880_MetadataUsageId;
extern "C"  Il2CppObject* SimpleJson_ParseObject_m2464058880 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseObject_m2464058880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		JsonObject_t2300545015 * L_0 = (JsonObject_t2300545015 *)il2cpp_codegen_object_new(JsonObject_t2300545015_il2cpp_TypeInfo_var);
		JsonObject__ctor_m2062444459(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t1328083999* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_2 = (bool)0;
		goto IL_0096;
	}

IL_0015:
	{
		CharU5BU5D_t1328083999* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		int32_t L_5 = SimpleJson_LookAhead_m2426341548(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		bool* L_7 = ___success2;
		*((int8_t*)(L_7)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0029:
	{
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_003d;
		}
	}
	{
		CharU5BU5D_t1328083999* L_9 = ___json0;
		int32_t* L_10 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_0096;
	}

IL_003d:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_004e;
		}
	}
	{
		CharU5BU5D_t1328083999* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Il2CppObject* L_14 = V_0;
		return L_14;
	}

IL_004e:
	{
		CharU5BU5D_t1328083999* L_15 = ___json0;
		int32_t* L_16 = ___index1;
		bool* L_17 = ___success2;
		String_t* L_18 = SimpleJson_ParseString_m1134805156(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		bool* L_19 = ___success2;
		if ((*((int8_t*)L_19)))
		{
			goto IL_0063;
		}
	}
	{
		bool* L_20 = ___success2;
		*((int8_t*)(L_20)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0063:
	{
		CharU5BU5D_t1328083999* L_21 = ___json0;
		int32_t* L_22 = ___index1;
		int32_t L_23 = SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)5)))
		{
			goto IL_0077;
		}
	}
	{
		bool* L_25 = ___success2;
		*((int8_t*)(L_25)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_0077:
	{
		CharU5BU5D_t1328083999* L_26 = ___json0;
		int32_t* L_27 = ___index1;
		bool* L_28 = ___success2;
		Il2CppObject * L_29 = SimpleJson_ParseValue_m1002395532(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		bool* L_30 = ___success2;
		if ((*((int8_t*)L_30)))
		{
			goto IL_008d;
		}
	}
	{
		bool* L_31 = ___success2;
		*((int8_t*)(L_31)) = (int8_t)0;
		return (Il2CppObject*)NULL;
	}

IL_008d:
	{
		Il2CppObject* L_32 = V_0;
		String_t* L_33 = V_3;
		Il2CppObject * L_34 = V_4;
		NullCheck(L_32);
		InterfaceActionInvoker2< String_t*, Il2CppObject * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(!0,!1) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_32, L_33, L_34);
	}

IL_0096:
	{
		bool L_35 = V_2;
		if (!L_35)
		{
			goto IL_0015;
		}
	}
	{
		Il2CppObject* L_36 = V_0;
		return L_36;
	}
}
// SimpleJson.JsonArray SimpleJson.SimpleJson::ParseArray(System.Char[],System.Int32&,System.Boolean&)
extern Il2CppClass* JsonArray_t1679500587_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m567051994_MethodInfo_var;
extern const uint32_t SimpleJson_ParseArray_m3513734753_MetadataUsageId;
extern "C"  JsonArray_t1679500587 * SimpleJson_ParseArray_m3513734753 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseArray_m3513734753_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	JsonArray_t1679500587 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	{
		JsonArray_t1679500587 * L_0 = (JsonArray_t1679500587 *)il2cpp_codegen_object_new(JsonArray_t1679500587_il2cpp_TypeInfo_var);
		JsonArray__ctor_m33515755(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t1328083999* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = (bool)0;
		goto IL_006a;
	}

IL_0015:
	{
		CharU5BU5D_t1328083999* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		int32_t L_5 = SimpleJson_LookAhead_m2426341548(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		bool* L_7 = ___success2;
		*((int8_t*)(L_7)) = (int8_t)0;
		return (JsonArray_t1679500587 *)NULL;
	}

IL_0029:
	{
		int32_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_003d;
		}
	}
	{
		CharU5BU5D_t1328083999* L_9 = ___json0;
		int32_t* L_10 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003d:
	{
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)4))))
		{
			goto IL_0051;
		}
	}
	{
		CharU5BU5D_t1328083999* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0051:
	{
		CharU5BU5D_t1328083999* L_14 = ___json0;
		int32_t* L_15 = ___index1;
		bool* L_16 = ___success2;
		Il2CppObject * L_17 = SimpleJson_ParseValue_m1002395532(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		bool* L_18 = ___success2;
		if ((*((int8_t*)L_18)))
		{
			goto IL_0063;
		}
	}
	{
		return (JsonArray_t1679500587 *)NULL;
	}

IL_0063:
	{
		JsonArray_t1679500587 * L_19 = V_0;
		Il2CppObject * L_20 = V_3;
		NullCheck(L_19);
		List_1_Add_m567051994(L_19, L_20, /*hidden argument*/List_1_Add_m567051994_MethodInfo_var);
	}

IL_006a:
	{
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_0015;
		}
	}

IL_0070:
	{
		JsonArray_t1679500587 * L_22 = V_0;
		return L_22;
	}
}
// System.Object SimpleJson.SimpleJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_ParseValue_m1002395532_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_ParseValue_m1002395532 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseValue_m1002395532_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t1328083999* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		int32_t L_2 = SimpleJson_LookAhead_m2426341548(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_0090;
		}
		if (L_3 == 1)
		{
			goto IL_0056;
		}
		if (L_3 == 2)
		{
			goto IL_0095;
		}
		if (L_3 == 3)
		{
			goto IL_005f;
		}
		if (L_3 == 4)
		{
			goto IL_0095;
		}
		if (L_3 == 5)
		{
			goto IL_0095;
		}
		if (L_3 == 6)
		{
			goto IL_0095;
		}
		if (L_3 == 7)
		{
			goto IL_0044;
		}
		if (L_3 == 8)
		{
			goto IL_004d;
		}
		if (L_3 == 9)
		{
			goto IL_0068;
		}
		if (L_3 == 10)
		{
			goto IL_0077;
		}
		if (L_3 == 11)
		{
			goto IL_0086;
		}
	}
	{
		goto IL_0095;
	}

IL_0044:
	{
		CharU5BU5D_t1328083999* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		bool* L_6 = ___success2;
		String_t* L_7 = SimpleJson_ParseString_m1134805156(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_004d:
	{
		CharU5BU5D_t1328083999* L_8 = ___json0;
		int32_t* L_9 = ___index1;
		bool* L_10 = ___success2;
		Il2CppObject * L_11 = SimpleJson_ParseNumber_m2704962974(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0056:
	{
		CharU5BU5D_t1328083999* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		bool* L_14 = ___success2;
		Il2CppObject* L_15 = SimpleJson_ParseObject_m2464058880(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_005f:
	{
		CharU5BU5D_t1328083999* L_16 = ___json0;
		int32_t* L_17 = ___index1;
		bool* L_18 = ___success2;
		JsonArray_t1679500587 * L_19 = SimpleJson_ParseArray_m3513734753(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_0068:
	{
		CharU5BU5D_t1328083999* L_20 = ___json0;
		int32_t* L_21 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		bool L_22 = ((bool)1);
		Il2CppObject * L_23 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_22);
		return L_23;
	}

IL_0077:
	{
		CharU5BU5D_t1328083999* L_24 = ___json0;
		int32_t* L_25 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		bool L_26 = ((bool)0);
		Il2CppObject * L_27 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0086:
	{
		CharU5BU5D_t1328083999* L_28 = ___json0;
		int32_t* L_29 = ___index1;
		SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		return NULL;
	}

IL_0090:
	{
		goto IL_0095;
	}

IL_0095:
	{
		bool* L_30 = ___success2;
		*((int8_t*)(L_30)) = (int8_t)0;
		return NULL;
	}
}
// System.String SimpleJson.SimpleJson::ParseString(System.Char[],System.Int32&,System.Boolean&)
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2303484169;
extern const uint32_t SimpleJson_ParseString_m1134805156_MetadataUsageId;
extern "C"  String_t* SimpleJson_ParseString_m1134805156 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseString_m1134805156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t1221177846 * V_0 = NULL;
	Il2CppChar V_1 = 0x0;
	bool V_2 = false;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	int32_t V_6 = 0;
	bool V_7 = false;
	{
		StringBuilder_t1221177846 * L_0 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m536337337(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t1328083999* L_1 = ___json0;
		int32_t* L_2 = ___index1;
		SimpleJson_EatWhitespace_m481181633(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_3 = ___json0;
		int32_t* L_4 = ___index1;
		int32_t* L_5 = ___index1;
		int32_t L_6 = (*((int32_t*)L_5));
		V_6 = L_6;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = V_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_7);
		int32_t L_8 = L_7;
		uint16_t L_9 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		V_2 = (bool)0;
		goto IL_0239;
	}

IL_0027:
	{
		int32_t* L_10 = ___index1;
		CharU5BU5D_t1328083999* L_11 = ___json0;
		NullCheck(L_11);
		if ((!(((uint32_t)(*((int32_t*)L_10))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_023f;
	}

IL_0036:
	{
		CharU5BU5D_t1328083999* L_12 = ___json0;
		int32_t* L_13 = ___index1;
		int32_t* L_14 = ___index1;
		int32_t L_15 = (*((int32_t*)L_14));
		V_6 = L_15;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		int32_t L_16 = V_6;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_16);
		int32_t L_17 = L_16;
		uint16_t L_18 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_1 = L_18;
		Il2CppChar L_19 = V_1;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0053;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_023f;
	}

IL_0053:
	{
		Il2CppChar L_20 = V_1;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0231;
		}
	}
	{
		int32_t* L_21 = ___index1;
		CharU5BU5D_t1328083999* L_22 = ___json0;
		NullCheck(L_22);
		if ((!(((uint32_t)(*((int32_t*)L_21))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_22)->max_length))))))))
		{
			goto IL_006a;
		}
	}
	{
		goto IL_023f;
	}

IL_006a:
	{
		CharU5BU5D_t1328083999* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		int32_t* L_25 = ___index1;
		int32_t L_26 = (*((int32_t*)L_25));
		V_6 = L_26;
		*((int32_t*)(L_24)) = (int32_t)((int32_t)((int32_t)L_26+(int32_t)1));
		int32_t L_27 = V_6;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_27);
		int32_t L_28 = L_27;
		uint16_t L_29 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_1 = L_29;
		Il2CppChar L_30 = V_1;
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_008e;
		}
	}
	{
		StringBuilder_t1221177846 * L_31 = V_0;
		NullCheck(L_31);
		StringBuilder_Append_m3618697540(L_31, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_008e:
	{
		Il2CppChar L_32 = V_1;
		if ((!(((uint32_t)L_32) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00a4;
		}
	}
	{
		StringBuilder_t1221177846 * L_33 = V_0;
		NullCheck(L_33);
		StringBuilder_Append_m3618697540(L_33, ((int32_t)92), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00a4:
	{
		Il2CppChar L_34 = V_1;
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00ba;
		}
	}
	{
		StringBuilder_t1221177846 * L_35 = V_0;
		NullCheck(L_35);
		StringBuilder_Append_m3618697540(L_35, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00ba:
	{
		Il2CppChar L_36 = V_1;
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t1221177846 * L_37 = V_0;
		NullCheck(L_37);
		StringBuilder_Append_m3618697540(L_37, 8, /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00cf:
	{
		Il2CppChar L_38 = V_1;
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00e5;
		}
	}
	{
		StringBuilder_t1221177846 * L_39 = V_0;
		NullCheck(L_39);
		StringBuilder_Append_m3618697540(L_39, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00e5:
	{
		Il2CppChar L_40 = V_1;
		if ((!(((uint32_t)L_40) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_00fb;
		}
	}
	{
		StringBuilder_t1221177846 * L_41 = V_0;
		NullCheck(L_41);
		StringBuilder_Append_m3618697540(L_41, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00fb:
	{
		Il2CppChar L_42 = V_1;
		if ((!(((uint32_t)L_42) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0111;
		}
	}
	{
		StringBuilder_t1221177846 * L_43 = V_0;
		NullCheck(L_43);
		StringBuilder_Append_m3618697540(L_43, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_0111:
	{
		Il2CppChar L_44 = V_1;
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0127;
		}
	}
	{
		StringBuilder_t1221177846 * L_45 = V_0;
		NullCheck(L_45);
		StringBuilder_Append_m3618697540(L_45, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_0127:
	{
		Il2CppChar L_46 = V_1;
		if ((!(((uint32_t)L_46) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_022c;
		}
	}
	{
		CharU5BU5D_t1328083999* L_47 = ___json0;
		NullCheck(L_47);
		int32_t* L_48 = ___index1;
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length))))-(int32_t)(*((int32_t*)L_48))));
		int32_t L_49 = V_3;
		if ((((int32_t)L_49) < ((int32_t)4)))
		{
			goto IL_0227;
		}
	}
	{
		bool* L_50 = ___success2;
		CharU5BU5D_t1328083999* L_51 = ___json0;
		int32_t* L_52 = ___index1;
		String_t* L_53 = String_CreateString_m2448464375(NULL, L_51, (*((int32_t*)L_52)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_54 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_55 = UInt32_TryParse_m3987111861(NULL /*static, unused*/, L_53, ((int32_t)515), L_54, (&V_4), /*hidden argument*/NULL);
		bool L_56 = L_55;
		V_7 = L_56;
		*((int8_t*)(L_50)) = (int8_t)L_56;
		bool L_57 = V_7;
		if (L_57)
		{
			goto IL_0169;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_58 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_58;
	}

IL_0169:
	{
		uint32_t L_59 = V_4;
		if ((!(((uint32_t)((int32_t)55296)) <= ((uint32_t)L_59))))
		{
			goto IL_020e;
		}
	}
	{
		uint32_t L_60 = V_4;
		if ((!(((uint32_t)L_60) <= ((uint32_t)((int32_t)56319)))))
		{
			goto IL_020e;
		}
	}
	{
		int32_t* L_61 = ___index1;
		int32_t* L_62 = ___index1;
		*((int32_t*)(L_61)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_62))+(int32_t)4));
		CharU5BU5D_t1328083999* L_63 = ___json0;
		NullCheck(L_63);
		int32_t* L_64 = ___index1;
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))))-(int32_t)(*((int32_t*)L_64))));
		int32_t L_65 = V_3;
		if ((((int32_t)L_65) < ((int32_t)6)))
		{
			goto IL_0205;
		}
	}
	{
		CharU5BU5D_t1328083999* L_66 = ___json0;
		int32_t* L_67 = ___index1;
		String_t* L_68 = String_CreateString_m2448464375(NULL, L_66, (*((int32_t*)L_67)), 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_69 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_68, _stringLiteral2303484169, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_0205;
		}
	}
	{
		CharU5BU5D_t1328083999* L_70 = ___json0;
		int32_t* L_71 = ___index1;
		String_t* L_72 = String_CreateString_m2448464375(NULL, L_70, ((int32_t)((int32_t)(*((int32_t*)L_71))+(int32_t)2)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_73 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_74 = UInt32_TryParse_m3987111861(NULL /*static, unused*/, L_72, ((int32_t)515), L_73, (&V_5), /*hidden argument*/NULL);
		if (!L_74)
		{
			goto IL_0205;
		}
	}
	{
		uint32_t L_75 = V_5;
		if ((!(((uint32_t)((int32_t)56320)) <= ((uint32_t)L_75))))
		{
			goto IL_0205;
		}
	}
	{
		uint32_t L_76 = V_5;
		if ((!(((uint32_t)L_76) <= ((uint32_t)((int32_t)57343)))))
		{
			goto IL_0205;
		}
	}
	{
		StringBuilder_t1221177846 * L_77 = V_0;
		uint32_t L_78 = V_4;
		NullCheck(L_77);
		StringBuilder_Append_m3618697540(L_77, (((int32_t)((uint16_t)L_78))), /*hidden argument*/NULL);
		StringBuilder_t1221177846 * L_79 = V_0;
		uint32_t L_80 = V_5;
		NullCheck(L_79);
		StringBuilder_Append_m3618697540(L_79, (((int32_t)((uint16_t)L_80))), /*hidden argument*/NULL);
		int32_t* L_81 = ___index1;
		int32_t* L_82 = ___index1;
		*((int32_t*)(L_81)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_82))+(int32_t)6));
		goto IL_0239;
	}

IL_0205:
	{
		bool* L_83 = ___success2;
		*((int8_t*)(L_83)) = (int8_t)0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_84 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_84;
	}

IL_020e:
	{
		StringBuilder_t1221177846 * L_85 = V_0;
		uint32_t L_86 = V_4;
		String_t* L_87 = SimpleJson_ConvertFromUtf32_m799444971(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		StringBuilder_Append_m3636508479(L_85, L_87, /*hidden argument*/NULL);
		int32_t* L_88 = ___index1;
		int32_t* L_89 = ___index1;
		*((int32_t*)(L_88)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_89))+(int32_t)4));
		goto IL_022c;
	}

IL_0227:
	{
		goto IL_023f;
	}

IL_022c:
	{
		goto IL_0239;
	}

IL_0231:
	{
		StringBuilder_t1221177846 * L_90 = V_0;
		Il2CppChar L_91 = V_1;
		NullCheck(L_90);
		StringBuilder_Append_m3618697540(L_90, L_91, /*hidden argument*/NULL);
	}

IL_0239:
	{
		bool L_92 = V_2;
		if (!L_92)
		{
			goto IL_0027;
		}
	}

IL_023f:
	{
		bool L_93 = V_2;
		if (L_93)
		{
			goto IL_024a;
		}
	}
	{
		bool* L_94 = ___success2;
		*((int8_t*)(L_94)) = (int8_t)0;
		return (String_t*)NULL;
	}

IL_024a:
	{
		StringBuilder_t1221177846 * L_95 = V_0;
		NullCheck(L_95);
		String_t* L_96 = StringBuilder_ToString_m1507807375(L_95, /*hidden argument*/NULL);
		return L_96;
	}
}
// System.String SimpleJson.SimpleJson::ConvertFromUtf32(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2737755086;
extern Il2CppCodeGenString* _stringLiteral436072782;
extern Il2CppCodeGenString* _stringLiteral107918005;
extern const uint32_t SimpleJson_ConvertFromUtf32_m799444971_MetadataUsageId;
extern "C"  String_t* SimpleJson_ConvertFromUtf32_m799444971 (Il2CppObject * __this /* static, unused */, int32_t ___utf320, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ConvertFromUtf32_m799444971_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___utf320;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___utf320;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)1114111))))
		{
			goto IL_0022;
		}
	}

IL_0012:
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_2, _stringLiteral2737755086, _stringLiteral436072782, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		int32_t L_3 = ___utf320;
		if ((((int32_t)((int32_t)55296)) > ((int32_t)L_3)))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_4 = ___utf320;
		if ((((int32_t)L_4) > ((int32_t)((int32_t)57343))))
		{
			goto IL_0048;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_5 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m4234257711(L_5, _stringLiteral2737755086, _stringLiteral107918005, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0048:
	{
		int32_t L_6 = ___utf320;
		if ((((int32_t)L_6) >= ((int32_t)((int32_t)65536))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_7 = ___utf320;
		String_t* L_8 = String_CreateString_m2556700934(NULL, (((int32_t)((uint16_t)L_7))), 1, /*hidden argument*/NULL);
		return L_8;
	}

IL_005c:
	{
		int32_t L_9 = ___utf320;
		___utf320 = ((int32_t)((int32_t)L_9-(int32_t)((int32_t)65536)));
		CharU5BU5D_t1328083999* L_10 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_11 = ___utf320;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11>>(int32_t)((int32_t)10)))+(int32_t)((int32_t)55296)))))));
		CharU5BU5D_t1328083999* L_12 = L_10;
		int32_t L_13 = ___utf320;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppChar)(((int32_t)((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_13%(int32_t)((int32_t)1024)))+(int32_t)((int32_t)56320)))))));
		String_t* L_14 = String_CreateString_m3818307083(NULL, L_12, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Object SimpleJson.SimpleJson::ParseNumber(System.Char[],System.Int32&,System.Boolean&)
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral372029369;
extern const uint32_t SimpleJson_ParseNumber_m2704962974_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_ParseNumber_m2704962974 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, bool* ___success2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_ParseNumber_m2704962974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	String_t* V_3 = NULL;
	double V_4 = 0.0;
	int64_t V_5 = 0;
	{
		CharU5BU5D_t1328083999* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		SimpleJson_EatWhitespace_m481181633(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t1328083999* L_2 = ___json0;
		int32_t* L_3 = ___index1;
		int32_t L_4 = SimpleJson_GetLastIndexOfNumber_m3446027218(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index1;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)(*((int32_t*)L_6))))+(int32_t)1));
		CharU5BU5D_t1328083999* L_7 = ___json0;
		int32_t* L_8 = ___index1;
		int32_t L_9 = V_1;
		String_t* L_10 = String_CreateString_m2448464375(NULL, L_7, (*((int32_t*)L_8)), L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		String_t* L_11 = V_3;
		NullCheck(L_11);
		int32_t L_12 = String_IndexOf_m570401060(L_11, _stringLiteral372029316, 5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_13 = V_3;
		NullCheck(L_13);
		int32_t L_14 = String_IndexOf_m570401060(L_13, _stringLiteral372029369, 5, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_006e;
		}
	}

IL_0045:
	{
		bool* L_15 = ___success2;
		CharU5BU5D_t1328083999* L_16 = ___json0;
		int32_t* L_17 = ___index1;
		int32_t L_18 = V_1;
		String_t* L_19 = String_CreateString_m2448464375(NULL, L_16, (*((int32_t*)L_17)), L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_20 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_21 = Double_TryParse_m815528105(NULL /*static, unused*/, L_19, ((int32_t)511), L_20, (&V_4), /*hidden argument*/NULL);
		*((int8_t*)(L_15)) = (int8_t)L_21;
		double L_22 = V_4;
		double L_23 = L_22;
		Il2CppObject * L_24 = Box(Double_t4078015681_il2cpp_TypeInfo_var, &L_23);
		V_2 = L_24;
		goto IL_0092;
	}

IL_006e:
	{
		bool* L_25 = ___success2;
		CharU5BU5D_t1328083999* L_26 = ___json0;
		int32_t* L_27 = ___index1;
		int32_t L_28 = V_1;
		String_t* L_29 = String_CreateString_m2448464375(NULL, L_26, (*((int32_t*)L_27)), L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_30 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_31 = Int64_TryParse_m3093198325(NULL /*static, unused*/, L_29, ((int32_t)511), L_30, (&V_5), /*hidden argument*/NULL);
		*((int8_t*)(L_25)) = (int8_t)L_31;
		int64_t L_32 = V_5;
		int64_t L_33 = L_32;
		Il2CppObject * L_34 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_33);
		V_2 = L_34;
	}

IL_0092:
	{
		int32_t* L_35 = ___index1;
		int32_t L_36 = V_0;
		*((int32_t*)(L_35)) = (int32_t)((int32_t)((int32_t)L_36+(int32_t)1));
		Il2CppObject * L_37 = V_2;
		return L_37;
	}
}
// System.Int32 SimpleJson.SimpleJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern Il2CppCodeGenString* _stringLiteral4282166785;
extern const uint32_t SimpleJson_GetLastIndexOfNumber_m3446027218_MetadataUsageId;
extern "C"  int32_t SimpleJson_GetLastIndexOfNumber_m3446027218 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_GetLastIndexOfNumber_m3446027218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		goto IL_0023;
	}

IL_0007:
	{
		CharU5BU5D_t1328083999* L_1 = ___json0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(_stringLiteral4282166785);
		int32_t L_5 = String_IndexOf_m2358239236(_stringLiteral4282166785, L_4, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002c;
	}

IL_001f:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_0;
		CharU5BU5D_t1328083999* L_8 = ___json0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0007;
		}
	}

IL_002c:
	{
		int32_t L_9 = V_0;
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}
}
// System.Void SimpleJson.SimpleJson::EatWhitespace(System.Char[],System.Int32&)
extern Il2CppCodeGenString* _stringLiteral96257128;
extern const uint32_t SimpleJson_EatWhitespace_m481181633_MetadataUsageId;
extern "C"  void SimpleJson_EatWhitespace_m481181633 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_EatWhitespace_m481181633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0024;
	}

IL_0005:
	{
		CharU5BU5D_t1328083999* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, (*((int32_t*)L_1)));
		int32_t L_2 = (*((int32_t*)L_1));
		uint16_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(_stringLiteral96257128);
		int32_t L_4 = String_IndexOf_m2358239236(_stringLiteral96257128, L_3, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_002e;
	}

IL_001e:
	{
		int32_t* L_5 = ___index1;
		int32_t* L_6 = ___index1;
		*((int32_t*)(L_5)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_6))+(int32_t)1));
	}

IL_0024:
	{
		int32_t* L_7 = ___index1;
		CharU5BU5D_t1328083999* L_8 = ___json0;
		NullCheck(L_8);
		if ((((int32_t)(*((int32_t*)L_7))) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0005;
		}
	}

IL_002e:
	{
		return;
	}
}
// System.Int32 SimpleJson.SimpleJson::LookAhead(System.Char[],System.Int32)
extern "C"  int32_t SimpleJson_LookAhead_m2426341548 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t ___index1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index1;
		V_0 = L_0;
		CharU5BU5D_t1328083999* L_1 = ___json0;
		int32_t L_2 = SimpleJson_NextToken_m3906376762(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 SimpleJson.SimpleJson::NextToken(System.Char[],System.Int32&)
extern "C"  int32_t SimpleJson_NextToken_m3906376762 (Il2CppObject * __this /* static, unused */, CharU5BU5D_t1328083999* ___json0, int32_t* ___index1, const MethodInfo* method)
{
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		CharU5BU5D_t1328083999* L_0 = ___json0;
		int32_t* L_1 = ___index1;
		SimpleJson_EatWhitespace_m481181633(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index1;
		CharU5BU5D_t1328083999* L_3 = ___json0;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))))))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		CharU5BU5D_t1328083999* L_4 = ___json0;
		int32_t* L_5 = ___index1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (*((int32_t*)L_5)));
		int32_t L_6 = (*((int32_t*)L_5));
		uint16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		int32_t* L_8 = ___index1;
		int32_t* L_9 = ___index1;
		*((int32_t*)(L_8)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_9))+(int32_t)1));
		Il2CppChar L_10 = V_0;
		V_2 = L_10;
		Il2CppChar L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00c4;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ca;
		}
	}

IL_008d:
	{
		Il2CppChar L_12 = V_2;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00c2;
		}
	}

IL_00a2:
	{
		Il2CppChar L_13 = V_2;
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_13-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00be;
		}
	}
	{
		goto IL_00cc;
	}

IL_00bc:
	{
		return 1;
	}

IL_00be:
	{
		return 2;
	}

IL_00c0:
	{
		return 3;
	}

IL_00c2:
	{
		return 4;
	}

IL_00c4:
	{
		return 6;
	}

IL_00c6:
	{
		return 7;
	}

IL_00c8:
	{
		return 8;
	}

IL_00ca:
	{
		return 5;
	}

IL_00cc:
	{
		int32_t* L_14 = ___index1;
		int32_t* L_15 = ___index1;
		*((int32_t*)(L_14)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_15))-(int32_t)1));
		CharU5BU5D_t1328083999* L_16 = ___json0;
		NullCheck(L_16);
		int32_t* L_17 = ___index1;
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))-(int32_t)(*((int32_t*)L_17))));
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) < ((int32_t)5)))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t1328083999* L_19 = ___json0;
		int32_t* L_20 = ___index1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, (*((int32_t*)L_20)));
		int32_t L_21 = (*((int32_t*)L_20));
		uint16_t L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t1328083999* L_23 = ___json0;
		int32_t* L_24 = ___index1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)((int32_t)(*((int32_t*)L_24))+(int32_t)1)));
		int32_t L_25 = ((int32_t)((int32_t)(*((int32_t*)L_24))+(int32_t)1));
		uint16_t L_26 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		if ((!(((uint32_t)L_26) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t1328083999* L_27 = ___json0;
		int32_t* L_28 = ___index1;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)2)));
		int32_t L_29 = ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)2));
		uint16_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		if ((!(((uint32_t)L_30) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t1328083999* L_31 = ___json0;
		int32_t* L_32 = ___index1;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)(*((int32_t*)L_32))+(int32_t)3)));
		int32_t L_33 = ((int32_t)((int32_t)(*((int32_t*)L_32))+(int32_t)3));
		uint16_t L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		if ((!(((uint32_t)L_34) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t1328083999* L_35 = ___json0;
		int32_t* L_36 = ___index1;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)((int32_t)(*((int32_t*)L_36))+(int32_t)4)));
		int32_t L_37 = ((int32_t)((int32_t)(*((int32_t*)L_36))+(int32_t)4));
		uint16_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		if ((!(((uint32_t)L_38) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0128;
		}
	}
	{
		int32_t* L_39 = ___index1;
		int32_t* L_40 = ___index1;
		*((int32_t*)(L_39)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)5));
		return ((int32_t)10);
	}

IL_0128:
	{
		int32_t L_41 = V_1;
		if ((((int32_t)L_41) < ((int32_t)4)))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t1328083999* L_42 = ___json0;
		int32_t* L_43 = ___index1;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, (*((int32_t*)L_43)));
		int32_t L_44 = (*((int32_t*)L_43));
		uint16_t L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		if ((!(((uint32_t)L_45) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t1328083999* L_46 = ___json0;
		int32_t* L_47 = ___index1;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)((int32_t)(*((int32_t*)L_47))+(int32_t)1)));
		int32_t L_48 = ((int32_t)((int32_t)(*((int32_t*)L_47))+(int32_t)1));
		uint16_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		if ((!(((uint32_t)L_49) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t1328083999* L_50 = ___json0;
		int32_t* L_51 = ___index1;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)((int32_t)(*((int32_t*)L_51))+(int32_t)2)));
		int32_t L_52 = ((int32_t)((int32_t)(*((int32_t*)L_51))+(int32_t)2));
		uint16_t L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		if ((!(((uint32_t)L_53) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t1328083999* L_54 = ___json0;
		int32_t* L_55 = ___index1;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)3)));
		int32_t L_56 = ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)3));
		uint16_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		if ((!(((uint32_t)L_57) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_016a;
		}
	}
	{
		int32_t* L_58 = ___index1;
		int32_t* L_59 = ___index1;
		*((int32_t*)(L_58)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_59))+(int32_t)4));
		return ((int32_t)9);
	}

IL_016a:
	{
		int32_t L_60 = V_1;
		if ((((int32_t)L_60) < ((int32_t)4)))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t1328083999* L_61 = ___json0;
		int32_t* L_62 = ___index1;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, (*((int32_t*)L_62)));
		int32_t L_63 = (*((int32_t*)L_62));
		uint16_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		if ((!(((uint32_t)L_64) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t1328083999* L_65 = ___json0;
		int32_t* L_66 = ___index1;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)((int32_t)(*((int32_t*)L_66))+(int32_t)1)));
		int32_t L_67 = ((int32_t)((int32_t)(*((int32_t*)L_66))+(int32_t)1));
		uint16_t L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		if ((!(((uint32_t)L_68) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t1328083999* L_69 = ___json0;
		int32_t* L_70 = ___index1;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, ((int32_t)((int32_t)(*((int32_t*)L_70))+(int32_t)2)));
		int32_t L_71 = ((int32_t)((int32_t)(*((int32_t*)L_70))+(int32_t)2));
		uint16_t L_72 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		if ((!(((uint32_t)L_72) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t1328083999* L_73 = ___json0;
		int32_t* L_74 = ___index1;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)((int32_t)(*((int32_t*)L_74))+(int32_t)3)));
		int32_t L_75 = ((int32_t)((int32_t)(*((int32_t*)L_74))+(int32_t)3));
		uint16_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		if ((!(((uint32_t)L_76) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		int32_t* L_77 = ___index1;
		int32_t* L_78 = ___index1;
		*((int32_t*)(L_77)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_78))+(int32_t)4));
		return ((int32_t)11);
	}

IL_01ac:
	{
		return 0;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeValue(SimpleJson.IJsonSerializerStrategy,System.Object,System.Text.StringBuilder)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t1943082916_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* IJsonSerializerStrategy_t209712766_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3323263070;
extern Il2CppCodeGenString* _stringLiteral2609877245;
extern Il2CppCodeGenString* _stringLiteral1743624307;
extern const uint32_t SimpleJson_SerializeValue_m2700795577_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeValue_m2700795577 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy0, Il2CppObject * ___value1, StringBuilder_t1221177846 * ___builder2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeValue_m2700795577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Il2CppObject * V_5 = NULL;
	StringBuilder_t1221177846 * G_B13_0 = NULL;
	StringBuilder_t1221177846 * G_B12_0 = NULL;
	String_t* G_B14_0 = NULL;
	StringBuilder_t1221177846 * G_B14_1 = NULL;
	{
		V_0 = (bool)1;
		Il2CppObject * L_0 = ___value1;
		V_1 = ((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = V_1;
		StringBuilder_t1221177846 * L_3 = ___builder2;
		bool L_4 = SimpleJson_SerializeString_m1750369917(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0100;
	}

IL_001c:
	{
		Il2CppObject * L_5 = ___value1;
		V_2 = ((Il2CppObject*)IsInst(L_5, IDictionary_2_t2603311978_il2cpp_TypeInfo_var));
		Il2CppObject* L_6 = V_2;
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Il2CppObject * L_7 = ___jsonSerializerStrategy0;
		Il2CppObject* L_8 = V_2;
		NullCheck(L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Keys() */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_8);
		Il2CppObject* L_10 = V_2;
		NullCheck(L_10);
		Il2CppObject* L_11 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Values() */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_10);
		StringBuilder_t1221177846 * L_12 = ___builder2;
		bool L_13 = SimpleJson_SerializeObject_m3504199269(NULL /*static, unused*/, L_7, L_9, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_0100;
	}

IL_0042:
	{
		Il2CppObject * L_14 = ___value1;
		V_3 = ((Il2CppObject*)IsInst(L_14, IDictionary_2_t1943082916_il2cpp_TypeInfo_var));
		Il2CppObject* L_15 = V_3;
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		Il2CppObject * L_16 = ___jsonSerializerStrategy0;
		Il2CppObject* L_17 = V_3;
		NullCheck(L_17);
		Il2CppObject* L_18 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(4 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Keys() */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_17);
		Il2CppObject* L_19 = V_3;
		NullCheck(L_19);
		Il2CppObject* L_20 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Values() */, IDictionary_2_t1943082916_il2cpp_TypeInfo_var, L_19);
		StringBuilder_t1221177846 * L_21 = ___builder2;
		bool L_22 = SimpleJson_SerializeObject_m3504199269(NULL /*static, unused*/, L_16, L_18, L_20, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_0100;
	}

IL_0068:
	{
		Il2CppObject * L_23 = ___value1;
		V_4 = ((Il2CppObject *)IsInst(L_23, IEnumerable_t2911409499_il2cpp_TypeInfo_var));
		Il2CppObject * L_24 = V_4;
		if (!L_24)
		{
			goto IL_0086;
		}
	}
	{
		Il2CppObject * L_25 = ___jsonSerializerStrategy0;
		Il2CppObject * L_26 = V_4;
		StringBuilder_t1221177846 * L_27 = ___builder2;
		bool L_28 = SimpleJson_SerializeArray_m779157756(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		goto IL_0100;
	}

IL_0086:
	{
		Il2CppObject * L_29 = ___value1;
		bool L_30 = SimpleJson_IsNumeric_m810085359(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_009e;
		}
	}
	{
		Il2CppObject * L_31 = ___value1;
		StringBuilder_t1221177846 * L_32 = ___builder2;
		bool L_33 = SimpleJson_SerializeNumber_m1195210771(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_0100;
	}

IL_009e:
	{
		Il2CppObject * L_34 = ___value1;
		if (!((Il2CppObject *)IsInstSealed(L_34, Boolean_t3825574718_il2cpp_TypeInfo_var)))
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t1221177846 * L_35 = ___builder2;
		Il2CppObject * L_36 = ___value1;
		G_B12_0 = L_35;
		if (!((*(bool*)((bool*)UnBox (L_36, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
		{
			G_B13_0 = L_35;
			goto IL_00bf;
		}
	}
	{
		G_B14_0 = _stringLiteral3323263070;
		G_B14_1 = G_B12_0;
		goto IL_00c4;
	}

IL_00bf:
	{
		G_B14_0 = _stringLiteral2609877245;
		G_B14_1 = G_B13_0;
	}

IL_00c4:
	{
		NullCheck(G_B14_1);
		StringBuilder_Append_m3636508479(G_B14_1, G_B14_0, /*hidden argument*/NULL);
		goto IL_0100;
	}

IL_00cf:
	{
		Il2CppObject * L_37 = ___value1;
		if (L_37)
		{
			goto IL_00e6;
		}
	}
	{
		StringBuilder_t1221177846 * L_38 = ___builder2;
		NullCheck(L_38);
		StringBuilder_Append_m3636508479(L_38, _stringLiteral1743624307, /*hidden argument*/NULL);
		goto IL_0100;
	}

IL_00e6:
	{
		Il2CppObject * L_39 = ___jsonSerializerStrategy0;
		Il2CppObject * L_40 = ___value1;
		NullCheck(L_39);
		bool L_41 = InterfaceFuncInvoker2< bool, Il2CppObject *, Il2CppObject ** >::Invoke(0 /* System.Boolean SimpleJson.IJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&) */, IJsonSerializerStrategy_t209712766_il2cpp_TypeInfo_var, L_39, L_40, (&V_5));
		V_0 = L_41;
		bool L_42 = V_0;
		if (!L_42)
		{
			goto IL_0100;
		}
	}
	{
		Il2CppObject * L_43 = ___jsonSerializerStrategy0;
		Il2CppObject * L_44 = V_5;
		StringBuilder_t1221177846 * L_45 = ___builder2;
		SimpleJson_SerializeValue_m2700795577(NULL /*static, unused*/, L_43, L_44, L_45, /*hidden argument*/NULL);
	}

IL_0100:
	{
		bool L_46 = V_0;
		return L_46;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeObject(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Collections.IEnumerable,System.Text.StringBuilder)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029399;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral372029393;
extern const uint32_t SimpleJson_SerializeObject_m3504199269_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeObject_m3504199269 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy0, Il2CppObject * ___keys1, Il2CppObject * ___values2, StringBuilder_t1221177846 * ___builder3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeObject_m3504199269_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	String_t* V_5 = NULL;
	{
		StringBuilder_t1221177846 * L_0 = ___builder3;
		NullCheck(L_0);
		StringBuilder_Append_m3636508479(L_0, _stringLiteral372029399, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___keys1;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		Il2CppObject * L_3 = ___values2;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_3);
		V_1 = L_4;
		V_2 = (bool)1;
		goto IL_008d;
	}

IL_0021:
	{
		Il2CppObject * L_5 = V_0;
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_5);
		V_3 = L_6;
		Il2CppObject * L_7 = V_1;
		NullCheck(L_7);
		Il2CppObject * L_8 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_7);
		V_4 = L_8;
		bool L_9 = V_2;
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		StringBuilder_t1221177846 * L_10 = ___builder3;
		NullCheck(L_10);
		StringBuilder_Append_m3636508479(L_10, _stringLiteral372029314, /*hidden argument*/NULL);
	}

IL_0042:
	{
		Il2CppObject * L_11 = V_3;
		V_5 = ((String_t*)IsInstSealed(L_11, String_t_il2cpp_TypeInfo_var));
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_13 = V_5;
		StringBuilder_t1221177846 * L_14 = ___builder3;
		SimpleJson_SerializeString_m1750369917(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		goto IL_006f;
	}

IL_005f:
	{
		Il2CppObject * L_15 = ___jsonSerializerStrategy0;
		Il2CppObject * L_16 = V_4;
		StringBuilder_t1221177846 * L_17 = ___builder3;
		bool L_18 = SimpleJson_SerializeValue_m2700795577(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_006f;
		}
	}
	{
		return (bool)0;
	}

IL_006f:
	{
		StringBuilder_t1221177846 * L_19 = ___builder3;
		NullCheck(L_19);
		StringBuilder_Append_m3636508479(L_19, _stringLiteral372029336, /*hidden argument*/NULL);
		Il2CppObject * L_20 = ___jsonSerializerStrategy0;
		Il2CppObject * L_21 = V_4;
		StringBuilder_t1221177846 * L_22 = ___builder3;
		bool L_23 = SimpleJson_SerializeValue_m2700795577(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_008b;
		}
	}
	{
		return (bool)0;
	}

IL_008b:
	{
		V_2 = (bool)0;
	}

IL_008d:
	{
		Il2CppObject * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_24);
		if (!L_25)
		{
			goto IL_00a3;
		}
	}
	{
		Il2CppObject * L_26 = V_1;
		NullCheck(L_26);
		bool L_27 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_26);
		if (L_27)
		{
			goto IL_0021;
		}
	}

IL_00a3:
	{
		StringBuilder_t1221177846 * L_28 = ___builder3;
		NullCheck(L_28);
		StringBuilder_Append_m3636508479(L_28, _stringLiteral372029393, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeArray(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Text.StringBuilder)
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029431;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029425;
extern const uint32_t SimpleJson_SerializeArray_m779157756_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeArray_m779157756 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___jsonSerializerStrategy0, Il2CppObject * ___anArray1, StringBuilder_t1221177846 * ___builder2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeArray_m779157756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t1221177846 * L_0 = ___builder2;
		NullCheck(L_0);
		StringBuilder_Append_m3636508479(L_0, _stringLiteral372029431, /*hidden argument*/NULL);
		V_0 = (bool)1;
		Il2CppObject * L_1 = ___anArray1;
		NullCheck(L_1);
		Il2CppObject * L_2 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0049;
		}

IL_001a:
		{
			Il2CppObject * L_3 = V_2;
			NullCheck(L_3);
			Il2CppObject * L_4 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_0033;
			}
		}

IL_0027:
		{
			StringBuilder_t1221177846 * L_6 = ___builder2;
			NullCheck(L_6);
			StringBuilder_Append_m3636508479(L_6, _stringLiteral372029314, /*hidden argument*/NULL);
		}

IL_0033:
		{
			Il2CppObject * L_7 = ___jsonSerializerStrategy0;
			Il2CppObject * L_8 = V_1;
			StringBuilder_t1221177846 * L_9 = ___builder2;
			bool L_10 = SimpleJson_SerializeValue_m2700795577(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_0047;
			}
		}

IL_0040:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0x7C, FINALLY_0059);
		}

IL_0047:
		{
			V_0 = (bool)0;
		}

IL_0049:
		{
			Il2CppObject * L_11 = V_2;
			NullCheck(L_11);
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_001a;
			}
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x6E, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_13 = V_2;
			V_4 = ((Il2CppObject *)IsInst(L_13, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_14 = V_4;
			if (L_14)
			{
				goto IL_0066;
			}
		}

IL_0065:
		{
			IL2CPP_END_FINALLY(89)
		}

IL_0066:
		{
			Il2CppObject * L_15 = V_4;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(89)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x7C, IL_007c)
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006e:
	{
		StringBuilder_t1221177846 * L_16 = ___builder2;
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, _stringLiteral372029425, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_007c:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeString(System.String,System.Text.StringBuilder)
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral3943473468;
extern Il2CppCodeGenString* _stringLiteral2088416310;
extern Il2CppCodeGenString* _stringLiteral1093630588;
extern Il2CppCodeGenString* _stringLiteral3419229416;
extern Il2CppCodeGenString* _stringLiteral3062999056;
extern Il2CppCodeGenString* _stringLiteral381169868;
extern Il2CppCodeGenString* _stringLiteral3869568110;
extern const uint32_t SimpleJson_SerializeString_m1750369917_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeString_m1750369917 (Il2CppObject * __this /* static, unused */, String_t* ___aString0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeString_m1750369917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t1328083999* V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		StringBuilder_t1221177846 * L_0 = ___builder1;
		NullCheck(L_0);
		StringBuilder_Append_m3636508479(L_0, _stringLiteral372029312, /*hidden argument*/NULL);
		String_t* L_1 = ___aString0;
		NullCheck(L_1);
		CharU5BU5D_t1328083999* L_2 = String_ToCharArray_m870309954(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_00d8;
	}

IL_001a:
	{
		CharU5BU5D_t1328083999* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		uint16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		Il2CppChar L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0037;
		}
	}
	{
		StringBuilder_t1221177846 * L_8 = ___builder1;
		NullCheck(L_8);
		StringBuilder_Append_m3636508479(L_8, _stringLiteral3943473468, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0037:
	{
		Il2CppChar L_9 = V_2;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0050;
		}
	}
	{
		StringBuilder_t1221177846 * L_10 = ___builder1;
		NullCheck(L_10);
		StringBuilder_Append_m3636508479(L_10, _stringLiteral2088416310, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0050:
	{
		Il2CppChar L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		StringBuilder_t1221177846 * L_12 = ___builder1;
		NullCheck(L_12);
		StringBuilder_Append_m3636508479(L_12, _stringLiteral1093630588, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0068:
	{
		Il2CppChar L_13 = V_2;
		if ((!(((uint32_t)L_13) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0081;
		}
	}
	{
		StringBuilder_t1221177846 * L_14 = ___builder1;
		NullCheck(L_14);
		StringBuilder_Append_m3636508479(L_14, _stringLiteral3419229416, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0081:
	{
		Il2CppChar L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_009a;
		}
	}
	{
		StringBuilder_t1221177846 * L_16 = ___builder1;
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, _stringLiteral3062999056, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_009a:
	{
		Il2CppChar L_17 = V_2;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00b3;
		}
	}
	{
		StringBuilder_t1221177846 * L_18 = ___builder1;
		NullCheck(L_18);
		StringBuilder_Append_m3636508479(L_18, _stringLiteral381169868, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_00b3:
	{
		Il2CppChar L_19 = V_2;
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00cc;
		}
	}
	{
		StringBuilder_t1221177846 * L_20 = ___builder1;
		NullCheck(L_20);
		StringBuilder_Append_m3636508479(L_20, _stringLiteral3869568110, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_00cc:
	{
		StringBuilder_t1221177846 * L_21 = ___builder1;
		Il2CppChar L_22 = V_2;
		NullCheck(L_21);
		StringBuilder_Append_m3618697540(L_21, L_22, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00d8:
	{
		int32_t L_24 = V_1;
		CharU5BU5D_t1328083999* L_25 = V_0;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t1221177846 * L_26 = ___builder1;
		NullCheck(L_26);
		StringBuilder_Append_m3636508479(L_26, _stringLiteral372029312, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeNumber(System.Object,System.Text.StringBuilder)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029392;
extern const uint32_t SimpleJson_SerializeNumber_m1195210771_MetadataUsageId;
extern "C"  bool SimpleJson_SerializeNumber_m1195210771 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___number0, StringBuilder_t1221177846 * ___builder1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_SerializeNumber_m1195210771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	uint64_t V_1 = 0;
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	Decimal_t724701077  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	double V_6 = 0.0;
	{
		Il2CppObject * L_0 = ___number0;
		if (!((Il2CppObject *)IsInstSealed(L_0, Int64_t909078037_il2cpp_TypeInfo_var)))
		{
			goto IL_002a;
		}
	}
	{
		StringBuilder_t1221177846 * L_1 = ___builder1;
		Il2CppObject * L_2 = ___number0;
		V_0 = ((*(int64_t*)((int64_t*)UnBox (L_2, Int64_t909078037_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_3 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = Int64_ToString_m1275187741((&V_0), L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3636508479(L_1, L_4, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___number0;
		if (!((Il2CppObject *)IsInstSealed(L_5, UInt64_t2909196914_il2cpp_TypeInfo_var)))
		{
			goto IL_0054;
		}
	}
	{
		StringBuilder_t1221177846 * L_6 = ___builder1;
		Il2CppObject * L_7 = ___number0;
		V_1 = ((*(uint64_t*)((uint64_t*)UnBox (L_7, UInt64_t2909196914_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_8 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = UInt64_ToString_m2892887310((&V_1), L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m3636508479(L_6, L_9, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_0054:
	{
		Il2CppObject * L_10 = ___number0;
		if (!((Il2CppObject *)IsInstSealed(L_10, Int32_t2071877448_il2cpp_TypeInfo_var)))
		{
			goto IL_007e;
		}
	}
	{
		StringBuilder_t1221177846 * L_11 = ___builder1;
		Il2CppObject * L_12 = ___number0;
		V_2 = ((*(int32_t*)((int32_t*)UnBox (L_12, Int32_t2071877448_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_13 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_14 = Int32_ToString_m526790770((&V_2), L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Append_m3636508479(L_11, L_14, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_007e:
	{
		Il2CppObject * L_15 = ___number0;
		if (!((Il2CppObject *)IsInstSealed(L_15, UInt32_t2149682021_il2cpp_TypeInfo_var)))
		{
			goto IL_00a8;
		}
	}
	{
		StringBuilder_t1221177846 * L_16 = ___builder1;
		Il2CppObject * L_17 = ___number0;
		V_3 = ((*(uint32_t*)((uint32_t*)UnBox (L_17, UInt32_t2149682021_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_18 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_19 = UInt32_ToString_m3894907091((&V_3), L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m3636508479(L_16, L_19, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a8:
	{
		Il2CppObject * L_20 = ___number0;
		if (!((Il2CppObject *)IsInstSealed(L_20, Decimal_t724701077_il2cpp_TypeInfo_var)))
		{
			goto IL_00d3;
		}
	}
	{
		StringBuilder_t1221177846 * L_21 = ___builder1;
		Il2CppObject * L_22 = ___number0;
		V_4 = ((*(Decimal_t724701077 *)((Decimal_t724701077 *)UnBox (L_22, Decimal_t724701077_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_23 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_24 = Decimal_ToString_m752193835((&V_4), L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m3636508479(L_21, L_24, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00d3:
	{
		Il2CppObject * L_25 = ___number0;
		if (!((Il2CppObject *)IsInstSealed(L_25, Single_t2076509932_il2cpp_TypeInfo_var)))
		{
			goto IL_00fe;
		}
	}
	{
		StringBuilder_t1221177846 * L_26 = ___builder1;
		Il2CppObject * L_27 = ___number0;
		V_5 = ((*(float*)((float*)UnBox (L_27, Single_t2076509932_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_28 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_29 = Single_ToString_m1229799376((&V_5), L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m3636508479(L_26, L_29, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00fe:
	{
		StringBuilder_t1221177846 * L_30 = ___builder1;
		Il2CppObject * L_31 = ___number0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_32 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		double L_33 = Convert_ToDouble_m574888941(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		CultureInfo_t3500843524 * L_34 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_35 = Double_ToString_m1474956491((&V_6), _stringLiteral372029392, L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m3636508479(L_30, L_35, /*hidden argument*/NULL);
	}

IL_0123:
	{
		return (bool)1;
	}
}
// System.Boolean SimpleJson.SimpleJson::IsNumeric(System.Object)
extern Il2CppClass* SByte_t454417549_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16_t4041245914_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt16_t986882611_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern Il2CppClass* Decimal_t724701077_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_IsNumeric_m810085359_MetadataUsageId;
extern "C"  bool SimpleJson_IsNumeric_m810085359 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_IsNumeric_m810085359_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_0, SByte_t454417549_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)1;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_1, Byte_t3683104436_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		Il2CppObject * L_2 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_2, Int16_t4041245914_il2cpp_TypeInfo_var)))
		{
			goto IL_0027;
		}
	}
	{
		return (bool)1;
	}

IL_0027:
	{
		Il2CppObject * L_3 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_3, UInt16_t986882611_il2cpp_TypeInfo_var)))
		{
			goto IL_0034;
		}
	}
	{
		return (bool)1;
	}

IL_0034:
	{
		Il2CppObject * L_4 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_4, Int32_t2071877448_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		return (bool)1;
	}

IL_0041:
	{
		Il2CppObject * L_5 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_5, UInt32_t2149682021_il2cpp_TypeInfo_var)))
		{
			goto IL_004e;
		}
	}
	{
		return (bool)1;
	}

IL_004e:
	{
		Il2CppObject * L_6 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_6, Int64_t909078037_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		return (bool)1;
	}

IL_005b:
	{
		Il2CppObject * L_7 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_7, UInt64_t2909196914_il2cpp_TypeInfo_var)))
		{
			goto IL_0068;
		}
	}
	{
		return (bool)1;
	}

IL_0068:
	{
		Il2CppObject * L_8 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_8, Single_t2076509932_il2cpp_TypeInfo_var)))
		{
			goto IL_0075;
		}
	}
	{
		return (bool)1;
	}

IL_0075:
	{
		Il2CppObject * L_9 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_9, Double_t4078015681_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		return (bool)1;
	}

IL_0082:
	{
		Il2CppObject * L_10 = ___value0;
		if (!((Il2CppObject *)IsInstSealed(L_10, Decimal_t724701077_il2cpp_TypeInfo_var)))
		{
			goto IL_008f;
		}
	}
	{
		return (bool)1;
	}

IL_008f:
	{
		return (bool)0;
	}
}
// SimpleJson.IJsonSerializerStrategy SimpleJson.SimpleJson::get_CurrentJsonSerializerStrategy()
extern Il2CppClass* SimpleJson_t3569903358_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_get_CurrentJsonSerializerStrategy_m3073481146_MetadataUsageId;
extern "C"  Il2CppObject * SimpleJson_get_CurrentJsonSerializerStrategy_m3073481146 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_get_CurrentJsonSerializerStrategy_m3073481146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * G_B2_0 = NULL;
	Il2CppObject * G_B1_0 = NULL;
	{
		Il2CppObject * L_0 = ((SimpleJson_t3569903358_StaticFields*)SimpleJson_t3569903358_il2cpp_TypeInfo_var->static_fields)->get__currentJsonSerializerStrategy_0();
		Il2CppObject * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0017;
		}
	}
	{
		PocoJsonSerializerStrategy_t2810850750 * L_2 = SimpleJson_get_PocoJsonSerializerStrategy_m3655250970(NULL /*static, unused*/, /*hidden argument*/NULL);
		PocoJsonSerializerStrategy_t2810850750 * L_3 = L_2;
		((SimpleJson_t3569903358_StaticFields*)SimpleJson_t3569903358_il2cpp_TypeInfo_var->static_fields)->set__currentJsonSerializerStrategy_0(L_3);
		G_B2_0 = ((Il2CppObject *)(L_3));
	}

IL_0017:
	{
		return G_B2_0;
	}
}
// SimpleJson.PocoJsonSerializerStrategy SimpleJson.SimpleJson::get_PocoJsonSerializerStrategy()
extern Il2CppClass* SimpleJson_t3569903358_il2cpp_TypeInfo_var;
extern Il2CppClass* PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var;
extern const uint32_t SimpleJson_get_PocoJsonSerializerStrategy_m3655250970_MetadataUsageId;
extern "C"  PocoJsonSerializerStrategy_t2810850750 * SimpleJson_get_PocoJsonSerializerStrategy_m3655250970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SimpleJson_get_PocoJsonSerializerStrategy_m3655250970_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PocoJsonSerializerStrategy_t2810850750 * G_B2_0 = NULL;
	PocoJsonSerializerStrategy_t2810850750 * G_B1_0 = NULL;
	{
		PocoJsonSerializerStrategy_t2810850750 * L_0 = ((SimpleJson_t3569903358_StaticFields*)SimpleJson_t3569903358_il2cpp_TypeInfo_var->static_fields)->get__pocoJsonSerializerStrategy_1();
		PocoJsonSerializerStrategy_t2810850750 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0017;
		}
	}
	{
		PocoJsonSerializerStrategy_t2810850750 * L_2 = (PocoJsonSerializerStrategy_t2810850750 *)il2cpp_codegen_object_new(PocoJsonSerializerStrategy_t2810850750_il2cpp_TypeInfo_var);
		PocoJsonSerializerStrategy__ctor_m679193024(L_2, /*hidden argument*/NULL);
		PocoJsonSerializerStrategy_t2810850750 * L_3 = L_2;
		((SimpleJson_t3569903358_StaticFields*)SimpleJson_t3569903358_il2cpp_TypeInfo_var->static_fields)->set__pocoJsonSerializerStrategy_1(L_3);
		G_B2_0 = L_3;
	}

IL_0017:
	{
		return G_B2_0;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m2231330368 (AddComponentMenu_t1099699699 * __this, String_t* ___menuName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C"  void AddComponentMenu__ctor_m648737891 (AddComponentMenu_t1099699699 * __this, String_t* ___menuName0, int32_t ___order1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_m_Ordering_1(L_1);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAdsDelegate__ctor_m583892427 (UnityAdsDelegate_t3613839672 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::Invoke()
extern "C"  void UnityAdsDelegate_Invoke_m3968753935 (UnityAdsDelegate_t3613839672 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAdsDelegate_Invoke_m3968753935((UnityAdsDelegate_t3613839672 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAdsDelegate_t3613839672 (UnityAdsDelegate_t3613839672 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAdsDelegate_BeginInvoke_m1012566798 (UnityAdsDelegate_t3613839672 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Advertisements.UnityAdsDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAdsDelegate_EndInvoke_m246878933 (UnityAdsDelegate_t3613839672 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::.ctor()
extern "C"  void UnityAdsInternal__ctor_m212446971 (UnityAdsInternal_t2792527316 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_add_onCampaignsAvailable_m1879828001_MetadataUsageId;
extern "C"  void UnityAdsInternal_add_onCampaignsAvailable_m1879828001 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_add_onCampaignsAvailable_m1879828001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onCampaignsAvailable_0();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onCampaignsAvailable_0(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsAvailable(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_remove_onCampaignsAvailable_m4140418646_MetadataUsageId;
extern "C"  void UnityAdsInternal_remove_onCampaignsAvailable_m4140418646 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_remove_onCampaignsAvailable_m4140418646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onCampaignsAvailable_0();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onCampaignsAvailable_0(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_add_onCampaignsFetchFailed_m1707903351_MetadataUsageId;
extern "C"  void UnityAdsInternal_add_onCampaignsFetchFailed_m1707903351 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_add_onCampaignsFetchFailed_m1707903351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onCampaignsFetchFailed_1();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onCampaignsFetchFailed_1(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onCampaignsFetchFailed(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_remove_onCampaignsFetchFailed_m3632462420_MetadataUsageId;
extern "C"  void UnityAdsInternal_remove_onCampaignsFetchFailed_m3632462420 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_remove_onCampaignsFetchFailed_m3632462420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onCampaignsFetchFailed_1();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onCampaignsFetchFailed_1(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_add_onShow_m4085422052_MetadataUsageId;
extern "C"  void UnityAdsInternal_add_onShow_m4085422052 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_add_onShow_m4085422052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onShow_2();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onShow_2(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onShow(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_remove_onShow_m2025989273_MetadataUsageId;
extern "C"  void UnityAdsInternal_remove_onShow_m2025989273 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_remove_onShow_m2025989273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onShow_2();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onShow_2(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_add_onHide_m1355080865_MetadataUsageId;
extern "C"  void UnityAdsInternal_add_onHide_m1355080865 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_add_onHide_m1355080865_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onHide_3();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onHide_3(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onHide(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_remove_onHide_m3602445128_MetadataUsageId;
extern "C"  void UnityAdsInternal_remove_onHide_m3602445128 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_remove_onHide_m3602445128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onHide_3();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onHide_3(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_2_t1684806294_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_add_onVideoCompleted_m114092648_MetadataUsageId;
extern "C"  void UnityAdsInternal_add_onVideoCompleted_m114092648 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_2_t1684806294 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_add_onVideoCompleted_m114092648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t1684806294 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onVideoCompleted_4();
		UnityAdsDelegate_2_t1684806294 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onVideoCompleted_4(((UnityAdsDelegate_2_t1684806294 *)CastclassSealed(L_2, UnityAdsDelegate_2_t1684806294_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoCompleted(UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_2_t1684806294_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_remove_onVideoCompleted_m1708120711_MetadataUsageId;
extern "C"  void UnityAdsInternal_remove_onVideoCompleted_m1708120711 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_2_t1684806294 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_remove_onVideoCompleted_m1708120711_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_2_t1684806294 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onVideoCompleted_4();
		UnityAdsDelegate_2_t1684806294 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onVideoCompleted_4(((UnityAdsDelegate_2_t1684806294 *)CastclassSealed(L_2, UnityAdsDelegate_2_t1684806294_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::add_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_add_onVideoStarted_m1352216253_MetadataUsageId;
extern "C"  void UnityAdsInternal_add_onVideoStarted_m1352216253 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_add_onVideoStarted_m1352216253_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onVideoStarted_5();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onVideoStarted_5(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::remove_onVideoStarted(UnityEngine.Advertisements.UnityAdsDelegate)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_remove_onVideoStarted_m251164176_MetadataUsageId;
extern "C"  void UnityAdsInternal_remove_onVideoStarted_m251164176 (Il2CppObject * __this /* static, unused */, UnityAdsDelegate_t3613839672 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_remove_onVideoStarted_m251164176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onVideoStarted_5();
		UnityAdsDelegate_t3613839672 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onVideoStarted_5(((UnityAdsDelegate_t3613839672 *)CastclassSealed(L_2, UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RemoveAllEventHandlers()
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_RemoveAllEventHandlers_m1166353867_MetadataUsageId;
extern "C"  void UnityAdsInternal_RemoveAllEventHandlers_m1166353867 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_RemoveAllEventHandlers_m1166353867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onCampaignsAvailable_0((UnityAdsDelegate_t3613839672 *)NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onCampaignsFetchFailed_1((UnityAdsDelegate_t3613839672 *)NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onShow_2((UnityAdsDelegate_t3613839672 *)NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onHide_3((UnityAdsDelegate_t3613839672 *)NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onVideoCompleted_4((UnityAdsDelegate_2_t1684806294 *)NULL);
		((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->set_onVideoStarted_5((UnityAdsDelegate_t3613839672 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsAvailable()
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_CallUnityAdsCampaignsAvailable_m743841450_MetadataUsageId;
extern "C"  void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m743841450 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_CallUnityAdsCampaignsAvailable_m743841450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t3613839672 * V_0 = NULL;
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onCampaignsAvailable_0();
		V_0 = L_0;
		UnityAdsDelegate_t3613839672 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t3613839672 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m3968753935(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsCampaignsFetchFailed()
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m2641284592_MetadataUsageId;
extern "C"  void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m2641284592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m2641284592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t3613839672 * V_0 = NULL;
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onCampaignsFetchFailed_1();
		V_0 = L_0;
		UnityAdsDelegate_t3613839672 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t3613839672 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m3968753935(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsShow()
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_CallUnityAdsShow_m3749200707_MetadataUsageId;
extern "C"  void UnityAdsInternal_CallUnityAdsShow_m3749200707 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_CallUnityAdsShow_m3749200707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t3613839672 * V_0 = NULL;
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onShow_2();
		V_0 = L_0;
		UnityAdsDelegate_t3613839672 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t3613839672 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m3968753935(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsHide()
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_CallUnityAdsHide_m1451410048_MetadataUsageId;
extern "C"  void UnityAdsInternal_CallUnityAdsHide_m1451410048 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_CallUnityAdsHide_m1451410048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t3613839672 * V_0 = NULL;
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onHide_3();
		V_0 = L_0;
		UnityAdsDelegate_t3613839672 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t3613839672 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m3968753935(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoCompleted(System.String,System.Boolean)
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsDelegate_2_Invoke_m1826557381_MethodInfo_var;
extern const uint32_t UnityAdsInternal_CallUnityAdsVideoCompleted_m2685251105_MetadataUsageId;
extern "C"  void UnityAdsInternal_CallUnityAdsVideoCompleted_m2685251105 (Il2CppObject * __this /* static, unused */, String_t* ___rewardItemKey0, bool ___skipped1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_CallUnityAdsVideoCompleted_m2685251105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_2_t1684806294 * V_0 = NULL;
	{
		UnityAdsDelegate_2_t1684806294 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onVideoCompleted_4();
		V_0 = L_0;
		UnityAdsDelegate_2_t1684806294 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		UnityAdsDelegate_2_t1684806294 * L_2 = V_0;
		String_t* L_3 = ___rewardItemKey0;
		bool L_4 = ___skipped1;
		NullCheck(L_2);
		UnityAdsDelegate_2_Invoke_m1826557381(L_2, L_3, L_4, /*hidden argument*/UnityAdsDelegate_2_Invoke_m1826557381_MethodInfo_var);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::CallUnityAdsVideoStarted()
extern Il2CppClass* UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsInternal_CallUnityAdsVideoStarted_m882181564_MetadataUsageId;
extern "C"  void UnityAdsInternal_CallUnityAdsVideoStarted_m882181564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsInternal_CallUnityAdsVideoStarted_m882181564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UnityAdsDelegate_t3613839672 * V_0 = NULL;
	{
		UnityAdsDelegate_t3613839672 * L_0 = ((UnityAdsInternal_t2792527316_StaticFields*)UnityAdsInternal_t2792527316_il2cpp_TypeInfo_var->static_fields)->get_onVideoStarted_5();
		V_0 = L_0;
		UnityAdsDelegate_t3613839672 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		UnityAdsDelegate_t3613839672 * L_2 = V_0;
		NullCheck(L_2);
		UnityAdsDelegate_Invoke_m3968753935(L_2, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
extern "C"  void UnityAdsInternal_RegisterNative_m2502472975 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_RegisterNative_m2502472975_ftn) ();
	static UnityAdsInternal_RegisterNative_m2502472975_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_RegisterNative_m2502472975_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
extern "C"  void UnityAdsInternal_Init_m600965261 (Il2CppObject * __this /* static, unused */, String_t* ___gameId0, bool ___testModeEnabled1, bool ___debugModeEnabled2, String_t* ___unityVersion3, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_Init_m600965261_ftn) (String_t*, bool, bool, String_t*);
	static UnityAdsInternal_Init_m600965261_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Init_m600965261_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)");
	_il2cpp_icall_func(___gameId0, ___testModeEnabled1, ___debugModeEnabled2, ___unityVersion3);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
extern "C"  bool UnityAdsInternal_Show_m3790165766 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, String_t* ___rewardItemKey1, String_t* ___options2, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_Show_m3790165766_ftn) (String_t*, String_t*, String_t*);
	static UnityAdsInternal_Show_m3790165766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_Show_m3790165766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)");
	return _il2cpp_icall_func(___zoneId0, ___rewardItemKey1, ___options2);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
extern "C"  bool UnityAdsInternal_CanShowAds_m3383840268 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, const MethodInfo* method)
{
	typedef bool (*UnityAdsInternal_CanShowAds_m3383840268_ftn) (String_t*);
	static UnityAdsInternal_CanShowAds_m3383840268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_CanShowAds_m3383840268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)");
	return _il2cpp_icall_func(___zoneId0);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
extern "C"  void UnityAdsInternal_SetLogLevel_m1327512352 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetLogLevel_m1327512352_ftn) (int32_t);
	static UnityAdsInternal_SetLogLevel_m1327512352_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetLogLevel_m1327512352_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)");
	_il2cpp_icall_func(___logLevel0);
}
// System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
extern "C"  void UnityAdsInternal_SetCampaignDataURL_m1966380984 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	typedef void (*UnityAdsInternal_SetCampaignDataURL_m1966380984_ftn) (String_t*);
	static UnityAdsInternal_SetCampaignDataURL_m1966380984_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsInternal_SetCampaignDataURL_m1966380984_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)");
	_il2cpp_icall_func(___url0);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsManager::get_enabled()
extern "C"  bool UnityAdsManager_get_enabled_m3410277395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*UnityAdsManager_get_enabled_m3410277395_ftn) ();
	static UnityAdsManager_get_enabled_m3410277395_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsManager_get_enabled_m3410277395_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsManager::get_enabled()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Advertisements.UnityAdsManager::IsPlatformEnabled(UnityEngine.RuntimePlatform)
extern "C"  bool UnityAdsManager_IsPlatformEnabled_m3745516095 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method)
{
	typedef bool (*UnityAdsManager_IsPlatformEnabled_m3745516095_ftn) (int32_t);
	static UnityAdsManager_IsPlatformEnabled_m3745516095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsManager_IsPlatformEnabled_m3745516095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsManager::IsPlatformEnabled(UnityEngine.RuntimePlatform)");
	return _il2cpp_icall_func(___platform0);
}
// System.Boolean UnityEngine.Advertisements.UnityAdsManager::get_initializeOnStartup()
extern "C"  bool UnityAdsManager_get_initializeOnStartup_m1942065522 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*UnityAdsManager_get_initializeOnStartup_m1942065522_ftn) ();
	static UnityAdsManager_get_initializeOnStartup_m1942065522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsManager_get_initializeOnStartup_m1942065522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsManager::get_initializeOnStartup()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Advertisements.UnityAdsManager::get_testMode()
extern "C"  bool UnityAdsManager_get_testMode_m1639009775 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*UnityAdsManager_get_testMode_m1639009775_ftn) ();
	static UnityAdsManager_get_testMode_m1639009775_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsManager_get_testMode_m1639009775_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsManager::get_testMode()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Advertisements.UnityAdsManager::GetGameId(UnityEngine.RuntimePlatform)
extern "C"  String_t* UnityAdsManager_GetGameId_m2796497895 (Il2CppObject * __this /* static, unused */, int32_t ___platform0, const MethodInfo* method)
{
	typedef String_t* (*UnityAdsManager_GetGameId_m2796497895_ftn) (int32_t);
	static UnityAdsManager_GetGameId_m2796497895_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityAdsManager_GetGameId_m2796497895_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Advertisements.UnityAdsManager::GetGameId(UnityEngine.RuntimePlatform)");
	return _il2cpp_icall_func(___platform0);
}
// System.Void UnityEngine.Animation::Stop()
extern "C"  void Animation_Stop_m1726655695 (Animation_t2068071072 * __this, const MethodInfo* method)
{
	{
		Animation_INTERNAL_CALL_Stop_m2000147263(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
extern "C"  void Animation_INTERNAL_CALL_Stop_m2000147263 (Il2CppObject * __this /* static, unused */, Animation_t2068071072 * ___self0, const MethodInfo* method)
{
	typedef void (*Animation_INTERNAL_CALL_Stop_m2000147263_ftn) (Animation_t2068071072 *);
	static Animation_INTERNAL_CALL_Stop_m2000147263_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_INTERNAL_CALL_Stop_m2000147263_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.Animation::Stop(System.String)
extern "C"  void Animation_Stop_m4105547977 (Animation_t2068071072 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_StopByName_m3955812477(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_StopByName(System.String)
extern "C"  void Animation_Internal_StopByName_m3955812477 (Animation_t2068071072 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animation_Internal_StopByName_m3955812477_ftn) (Animation_t2068071072 *, String_t*);
	static Animation_Internal_StopByName_m3955812477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_StopByName_m3955812477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_StopByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animation::Rewind(System.String)
extern "C"  void Animation_Rewind_m1869730758 (Animation_t2068071072 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animation_Internal_RewindByName_m91751306(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
extern "C"  void Animation_Internal_RewindByName_m91751306 (Animation_t2068071072 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animation_Internal_RewindByName_m91751306_ftn) (Animation_t2068071072 *, String_t*);
	static Animation_Internal_RewindByName_m91751306_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Internal_RewindByName_m91751306_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Internal_RewindByName(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.AnimationState UnityEngine.Animation::get_Item(System.String)
extern "C"  AnimationState_t1303741697 * Animation_get_Item_m4198128320 (Animation_t2068071072 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		AnimationState_t1303741697 * L_1 = Animation_GetState_m1608765041(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
extern "C"  bool Animation_Play_m315011537 (Animation_t2068071072 * __this, String_t* ___animation0, int32_t ___mode1, const MethodInfo* method)
{
	typedef bool (*Animation_Play_m315011537_ftn) (Animation_t2068071072 *, String_t*, int32_t);
	static Animation_Play_m315011537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Play_m315011537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)");
	return _il2cpp_icall_func(__this, ___animation0, ___mode1);
}
// System.Boolean UnityEngine.Animation::Play(System.String)
extern "C"  bool Animation_Play_m976361057 (Animation_t2068071072 * __this, String_t* ___animation0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___animation0;
		int32_t L_1 = V_0;
		bool L_2 = Animation_Play_m315011537(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
extern "C"  void Animation_CrossFade_m3167311838 (Animation_t2068071072 * __this, String_t* ___animation0, float ___fadeLength1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Animation_CrossFade_m3167311838_ftn) (Animation_t2068071072 *, String_t*, float, int32_t);
	static Animation_CrossFade_m3167311838_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_CrossFade_m3167311838_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)");
	_il2cpp_icall_func(__this, ___animation0, ___fadeLength1, ___mode2);
}
// System.Void UnityEngine.Animation::Blend(System.String,System.Single,System.Single)
extern "C"  void Animation_Blend_m2530155026 (Animation_t2068071072 * __this, String_t* ___animation0, float ___targetWeight1, float ___fadeLength2, const MethodInfo* method)
{
	typedef void (*Animation_Blend_m2530155026_ftn) (Animation_t2068071072 *, String_t*, float, float);
	static Animation_Blend_m2530155026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_Blend_m2530155026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::Blend(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___animation0, ___targetWeight1, ___fadeLength2);
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String)
extern "C"  void Animation_AddClip_m2495773389 (Animation_t2068071072 * __this, AnimationClip_t3510324950 * ___clip0, String_t* ___newName1, const MethodInfo* method)
{
	{
		AnimationClip_t3510324950 * L_0 = ___clip0;
		String_t* L_1 = ___newName1;
		Animation_AddClip_m686158425(__this, L_0, L_1, ((int32_t)-2147483648LL), ((int32_t)2147483647LL), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
extern "C"  void Animation_AddClip_m900889592 (Animation_t2068071072 * __this, AnimationClip_t3510324950 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, bool ___addLoopFrame4, const MethodInfo* method)
{
	typedef void (*Animation_AddClip_m900889592_ftn) (Animation_t2068071072 *, AnimationClip_t3510324950 *, String_t*, int32_t, int32_t, bool);
	static Animation_AddClip_m900889592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_AddClip_m900889592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___clip0, ___newName1, ___firstFrame2, ___lastFrame3, ___addLoopFrame4);
}
// System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32)
extern "C"  void Animation_AddClip_m686158425 (Animation_t2068071072 * __this, AnimationClip_t3510324950 * ___clip0, String_t* ___newName1, int32_t ___firstFrame2, int32_t ___lastFrame3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		AnimationClip_t3510324950 * L_0 = ___clip0;
		String_t* L_1 = ___newName1;
		int32_t L_2 = ___firstFrame2;
		int32_t L_3 = ___lastFrame3;
		bool L_4 = V_0;
		Animation_AddClip_m900889592(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.Animation::GetEnumerator()
extern Il2CppClass* Enumerator_t59479314_il2cpp_TypeInfo_var;
extern const uint32_t Animation_GetEnumerator_m3067441975_MetadataUsageId;
extern "C"  Il2CppObject * Animation_GetEnumerator_m3067441975 (Animation_t2068071072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Animation_GetEnumerator_m3067441975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t59479314 * L_0 = (Enumerator_t59479314 *)il2cpp_codegen_object_new(Enumerator_t59479314_il2cpp_TypeInfo_var);
		Enumerator__ctor_m1257319481(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
extern "C"  AnimationState_t1303741697 * Animation_GetState_m1608765041 (Animation_t2068071072 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef AnimationState_t1303741697 * (*Animation_GetState_m1608765041_ftn) (Animation_t2068071072 *, String_t*);
	static Animation_GetState_m1608765041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetState_m1608765041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetState(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
extern "C"  AnimationState_t1303741697 * Animation_GetStateAtIndex_m2774853377 (Animation_t2068071072 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef AnimationState_t1303741697 * (*Animation_GetStateAtIndex_m2774853377_ftn) (Animation_t2068071072 *, int32_t);
	static Animation_GetStateAtIndex_m2774853377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateAtIndex_m2774853377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateAtIndex(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Int32 UnityEngine.Animation::GetStateCount()
extern "C"  int32_t Animation_GetStateCount_m1145090795 (Animation_t2068071072 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animation_GetStateCount_m1145090795_ftn) (Animation_t2068071072 *);
	static Animation_GetStateCount_m1145090795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animation_GetStateCount_m1145090795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animation::GetStateCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animation/Enumerator::.ctor(UnityEngine.Animation)
extern "C"  void Enumerator__ctor_m1257319481 (Enumerator_t59479314 * __this, Animation_t2068071072 * ___outer0, const MethodInfo* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Animation_t2068071072 * L_0 = ___outer0;
		__this->set_m_Outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Animation/Enumerator::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1209866037 (Enumerator_t59479314 * __this, const MethodInfo* method)
{
	{
		Animation_t2068071072 * L_0 = __this->get_m_Outer_0();
		int32_t L_1 = __this->get_m_CurrentIndex_1();
		NullCheck(L_0);
		AnimationState_t1303741697 * L_2 = Animation_GetStateAtIndex_m2774853377(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Animation/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1994650764 (Enumerator_t59479314 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Animation_t2068071072 * L_0 = __this->get_m_Outer_0();
		NullCheck(L_0);
		int32_t L_1 = Animation_GetStateCount_m1145090795(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_m_CurrentIndex_1();
		__this->set_m_CurrentIndex_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = __this->get_m_CurrentIndex_1();
		int32_t L_4 = V_0;
		return (bool)((((int32_t)L_3) < ((int32_t)L_4))? 1 : 0);
	}
}
// System.Void UnityEngine.Animation/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m1246523333 (Enumerator_t59479314 * __this, const MethodInfo* method)
{
	{
		__this->set_m_CurrentIndex_1((-1));
		return;
	}
}
// System.Void UnityEngine.AnimationClip::.ctor()
extern "C"  void AnimationClip__ctor_m3495408353 (AnimationClip_t3510324950 * __this, const MethodInfo* method)
{
	{
		Motion__ctor_m784531835(__this, /*hidden argument*/NULL);
		AnimationClip_Internal_CreateAnimationClip_m2274684340(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
extern "C"  void AnimationClip_Internal_CreateAnimationClip_m2274684340 (Il2CppObject * __this /* static, unused */, AnimationClip_t3510324950 * ___self0, const MethodInfo* method)
{
	typedef void (*AnimationClip_Internal_CreateAnimationClip_m2274684340_ftn) (AnimationClip_t3510324950 *);
	static AnimationClip_Internal_CreateAnimationClip_m2274684340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_Internal_CreateAnimationClip_m2274684340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
extern "C"  void AnimationClip_SetCurve_m2504807125 (AnimationClip_t3510324950 * __this, String_t* ___relativePath0, Type_t * ___type1, String_t* ___propertyName2, AnimationCurve_t3306541151 * ___curve3, const MethodInfo* method)
{
	typedef void (*AnimationClip_SetCurve_m2504807125_ftn) (AnimationClip_t3510324950 *, String_t*, Type_t *, String_t*, AnimationCurve_t3306541151 *);
	static AnimationClip_SetCurve_m2504807125_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClip_SetCurve_m2504807125_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)");
	_il2cpp_icall_func(__this, ___relativePath0, ___type1, ___propertyName2, ___curve3);
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2814448007 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* ___keys0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t449065829* L_0 = ___keys0;
		AnimationCurve_Init_m1486386337(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m3707994114 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m1486386337(__this, (KeyframeU5BU5D_t449065829*)(KeyframeU5BU5D_t449065829*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m2190142678 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m2190142678_ftn) (AnimationCurve_t3306541151 *);
	static AnimationCurve_Cleanup_m2190142678_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m2190142678_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m3393741894 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m2190142678(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m3698879322 (AnimationCurve_t3306541151 * __this, float ___time0, const MethodInfo* method)
{
	typedef float (*AnimationCurve_Evaluate_m3698879322_ftn) (AnimationCurve_t3306541151 *, float);
	static AnimationCurve_Evaluate_m3698879322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Evaluate_m3698879322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Evaluate(System.Single)");
	return _il2cpp_icall_func(__this, ___time0);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern "C"  KeyframeU5BU5D_t449065829* AnimationCurve_get_keys_m162753017 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	{
		KeyframeU5BU5D_t449065829* L_0 = AnimationCurve_GetKeys_m2987996186(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationCurve::set_keys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_set_keys_m2941016540 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* ___value0, const MethodInfo* method)
{
	{
		KeyframeU5BU5D_t449065829* L_0 = ___value0;
		AnimationCurve_SetKeys_m1387699805(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Keyframe UnityEngine.AnimationCurve::get_Item(System.Int32)
extern "C"  Keyframe_t1449471340  AnimationCurve_get_Item_m3780274863 (AnimationCurve_t3306541151 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Keyframe_t1449471340  L_1 = AnimationCurve_GetKey_Internal_m4198730374(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationCurve::get_length()
extern "C"  int32_t AnimationCurve_get_length_m1757155287 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_length_m1757155287_ftn) (AnimationCurve_t3306541151 *);
	static AnimationCurve_get_length_m1757155287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_length_m1757155287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_SetKeys_m1387699805 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_SetKeys_m1387699805_ftn) (AnimationCurve_t3306541151 *, KeyframeU5BU5D_t449065829*);
	static AnimationCurve_SetKeys_m1387699805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_SetKeys_m1387699805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey_Internal(System.Int32)
extern "C"  Keyframe_t1449471340  AnimationCurve_GetKey_Internal_m4198730374 (AnimationCurve_t3306541151 * __this, int32_t ___index0, const MethodInfo* method)
{
	Keyframe_t1449471340  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		AnimationCurve_INTERNAL_CALL_GetKey_Internal_m4032409550(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Keyframe_t1449471340  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)
extern "C"  void AnimationCurve_INTERNAL_CALL_GetKey_Internal_m4032409550 (Il2CppObject * __this /* static, unused */, AnimationCurve_t3306541151 * ___self0, int32_t ___index1, Keyframe_t1449471340 * ___value2, const MethodInfo* method)
{
	typedef void (*AnimationCurve_INTERNAL_CALL_GetKey_Internal_m4032409550_ftn) (AnimationCurve_t3306541151 *, int32_t, Keyframe_t1449471340 *);
	static AnimationCurve_INTERNAL_CALL_GetKey_Internal_m4032409550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_INTERNAL_CALL_GetKey_Internal_m4032409550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::INTERNAL_CALL_GetKey_Internal(UnityEngine.AnimationCurve,System.Int32,UnityEngine.Keyframe&)");
	_il2cpp_icall_func(___self0, ___index1, ___value2);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C"  KeyframeU5BU5D_t449065829* AnimationCurve_GetKeys_m2987996186 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	typedef KeyframeU5BU5D_t449065829* (*AnimationCurve_GetKeys_m2987996186_ftn) (AnimationCurve_t3306541151 *);
	static AnimationCurve_GetKeys_m2987996186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_GetKeys_m2987996186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::GetKeys()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AnimationCurve UnityEngine.AnimationCurve::Linear(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern const uint32_t AnimationCurve_Linear_m125562148_MetadataUsageId;
extern "C"  AnimationCurve_t3306541151 * AnimationCurve_Linear_m125562148 (Il2CppObject * __this /* static, unused */, float ___timeStart0, float ___valueStart1, float ___timeEnd2, float ___valueEnd3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationCurve_Linear_m125562148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	KeyframeU5BU5D_t449065829* V_1 = NULL;
	{
		float L_0 = ___valueEnd3;
		float L_1 = ___valueStart1;
		float L_2 = ___timeEnd2;
		float L_3 = ___timeStart0;
		V_0 = ((float)((float)((float)((float)L_0-(float)L_1))/(float)((float)((float)L_2-(float)L_3))));
		KeyframeU5BU5D_t449065829* L_4 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		float L_5 = ___timeStart0;
		float L_6 = ___valueStart1;
		float L_7 = V_0;
		Keyframe_t1449471340  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Keyframe__ctor_m140082843(&L_8, L_5, L_6, (0.0f), L_7, /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_8;
		KeyframeU5BU5D_t449065829* L_9 = L_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		float L_10 = ___timeEnd2;
		float L_11 = ___valueEnd3;
		float L_12 = V_0;
		Keyframe_t1449471340  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Keyframe__ctor_m140082843(&L_13, L_10, L_11, L_12, (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_13;
		V_1 = L_9;
		KeyframeU5BU5D_t449065829* L_14 = V_1;
		AnimationCurve_t3306541151 * L_15 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_15, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
extern "C"  int32_t AnimationCurve_get_preWrapMode_m1489197552 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_preWrapMode_m1489197552_ftn) (AnimationCurve_t3306541151 *);
	static AnimationCurve_get_preWrapMode_m1489197552_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_preWrapMode_m1489197552_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_preWrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationCurve_set_preWrapMode_m2999148321 (AnimationCurve_t3306541151 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_set_preWrapMode_m2999148321_ftn) (AnimationCurve_t3306541151 *, int32_t);
	static AnimationCurve_set_preWrapMode_m2999148321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_set_preWrapMode_m2999148321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
extern "C"  int32_t AnimationCurve_get_postWrapMode_m1346218857 (AnimationCurve_t3306541151 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_postWrapMode_m1346218857_ftn) (AnimationCurve_t3306541151 *);
	static AnimationCurve_get_postWrapMode_m1346218857_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_postWrapMode_m1346218857_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_postWrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationCurve_set_postWrapMode_m262982620 (AnimationCurve_t3306541151 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_set_postWrapMode_m262982620_ftn) (AnimationCurve_t3306541151 *, int32_t);
	static AnimationCurve_set_postWrapMode_m262982620_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_set_postWrapMode_m262982620_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m1486386337 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m1486386337_ftn) (AnimationCurve_t3306541151 *, KeyframeU5BU5D_t449065829*);
	static AnimationCurve_Init_m1486386337_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m1486386337_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_back(const AnimationCurve_t3306541151_marshaled_pinvoke& marshaled, AnimationCurve_t3306541151& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_pinvoke_cleanup(AnimationCurve_t3306541151_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_com(const AnimationCurve_t3306541151& unmarshaled, AnimationCurve_t3306541151_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3306541151_marshal_com_back(const AnimationCurve_t3306541151_marshaled_com& marshaled, AnimationCurve_t3306541151& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3306541151_marshal_com_cleanup(AnimationCurve_t3306541151_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AnimationEvent__ctor_m3458990599_MetadataUsageId;
extern "C"  void AnimationEvent__ctor_m3458990599 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent__ctor_m3458990599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		__this->set_m_Time_0((0.0f));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_FunctionName_1(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_StringParameter_2(L_1);
		__this->set_m_ObjectReferenceParameter_3((Object_t1021602117 *)NULL);
		__this->set_m_FloatParameter_4((0.0f));
		__this->set_m_IntParameter_5(0);
		__this->set_m_MessageOptions_6(0);
		__this->set_m_Source_7(0);
		__this->set_m_StateSender_8((AnimationState_t1303741697 *)NULL);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C"  String_t* AnimationEvent_get_data_m1944226119 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C"  void AnimationEvent_set_data_m2305843164 (AnimationEvent_t2428323300 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C"  String_t* AnimationEvent_get_stringParameter_m3994883119 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C"  void AnimationEvent_set_stringParameter_m3956270534 (AnimationEvent_t2428323300 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C"  float AnimationEvent_get_floatParameter_m2695208933 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_FloatParameter_4();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C"  void AnimationEvent_set_floatParameter_m4139544688 (AnimationEvent_t2428323300 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_FloatParameter_4(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C"  int32_t AnimationEvent_get_intParameter_m903783586 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_IntParameter_5();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C"  void AnimationEvent_set_intParameter_m1174062029 (AnimationEvent_t2428323300 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_IntParameter_5(L_0);
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C"  Object_t1021602117 * AnimationEvent_get_objectReferenceParameter_m3565420672 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = __this->get_m_ObjectReferenceParameter_3();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C"  void AnimationEvent_set_objectReferenceParameter_m3171652515 (AnimationEvent_t2428323300 * __this, Object_t1021602117 * ___value0, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = ___value0;
		__this->set_m_ObjectReferenceParameter_3(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C"  String_t* AnimationEvent_get_functionName_m4178006856 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_FunctionName_1();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C"  void AnimationEvent_set_functionName_m1910707421 (AnimationEvent_t2428323300 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_FunctionName_1(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C"  float AnimationEvent_get_time_m2837507241 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Time_0();
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C"  void AnimationEvent_set_time_m2162176572 (AnimationEvent_t2428323300 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C"  int32_t AnimationEvent_get_messageOptions_m3547411650 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_MessageOptions_6();
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C"  void AnimationEvent_set_messageOptions_m3825202925 (AnimationEvent_t2428323300 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_MessageOptions_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C"  bool AnimationEvent_get_isFiredByLegacy_m2585487710 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Source_7();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C"  bool AnimationEvent_get_isFiredByAnimator_m3399078288 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Source_7();
		return (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2898685537;
extern const uint32_t AnimationEvent_get_animationState_m1069191380_MetadataUsageId;
extern "C"  AnimationState_t1303741697 * AnimationEvent_get_animationState_m1069191380 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animationState_m1069191380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m2585487710(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2898685537, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t1303741697 * L_1 = __this->get_m_StateSender_8();
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral619768799;
extern const uint32_t AnimationEvent_get_animatorStateInfo_m4255437518_MetadataUsageId;
extern "C"  AnimatorStateInfo_t2577870592  AnimationEvent_get_animatorStateInfo_m4255437518 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorStateInfo_m4255437518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m3399078288(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral619768799, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t2577870592  L_1 = __this->get_m_AnimatorStateInfo_9();
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1382485084;
extern const uint32_t AnimationEvent_get_animatorClipInfo_m615328956_MetadataUsageId;
extern "C"  AnimatorClipInfo_t3905751349  AnimationEvent_get_animatorClipInfo_m615328956 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorClipInfo_m615328956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m3399078288(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1382485084, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t3905751349  L_1 = __this->get_m_AnimatorClipInfo_10();
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C"  int32_t AnimationEvent_GetHash_m2850593057 (AnimationEvent_t2428323300 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m4178006856(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m931956593(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m2837507241(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m3102305584((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke(const AnimationEvent_t2428323300& unmarshaled, AnimationEvent_t2428323300_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_back(const AnimationEvent_t2428323300_marshaled_pinvoke& marshaled, AnimationEvent_t2428323300& unmarshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_pinvoke_cleanup(AnimationEvent_t2428323300_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_com(const AnimationEvent_t2428323300& unmarshaled, AnimationEvent_t2428323300_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
extern "C" void AnimationEvent_t2428323300_marshal_com_back(const AnimationEvent_t2428323300_marshaled_com& marshaled, AnimationEvent_t2428323300& unmarshaled)
{
	Il2CppCodeGenException* ___m_ObjectReferenceParameter_3Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ObjectReferenceParameter' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ObjectReferenceParameter_3Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t2428323300_marshal_com_cleanup(AnimationEvent_t2428323300_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimationState::get_enabled()
extern "C"  bool AnimationState_get_enabled_m1939858006 (AnimationState_t1303741697 * __this, const MethodInfo* method)
{
	typedef bool (*AnimationState_get_enabled_m1939858006_ftn) (AnimationState_t1303741697 *);
	static AnimationState_get_enabled_m1939858006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_enabled_m1939858006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
extern "C"  void AnimationState_set_enabled_m2079619927 (AnimationState_t1303741697 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_enabled_m2079619927_ftn) (AnimationState_t1303741697 *, bool);
	static AnimationState_set_enabled_m2079619927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_enabled_m2079619927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_weight(System.Single)
extern "C"  void AnimationState_set_weight_m2370306600 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_weight_m2370306600_ftn) (AnimationState_t1303741697 *, float);
	static AnimationState_set_weight_m2370306600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_weight_m2370306600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_weight(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WrapMode UnityEngine.AnimationState::get_wrapMode()
extern "C"  int32_t AnimationState_get_wrapMode_m1444624849 (AnimationState_t1303741697 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationState_get_wrapMode_m1444624849_ftn) (AnimationState_t1303741697 *);
	static AnimationState_get_wrapMode_m1444624849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_wrapMode_m1444624849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_wrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationState_set_wrapMode_m2342385428 (AnimationState_t1303741697 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_wrapMode_m2342385428_ftn) (AnimationState_t1303741697 *, int32_t);
	static AnimationState_set_wrapMode_m2342385428_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_wrapMode_m2342385428_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_time()
extern "C"  float AnimationState_get_time_m2280025052 (AnimationState_t1303741697 * __this, const MethodInfo* method)
{
	typedef float (*AnimationState_get_time_m2280025052_ftn) (AnimationState_t1303741697 *);
	static AnimationState_get_time_m2280025052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_time_m2280025052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_time()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_time(System.Single)
extern "C"  void AnimationState_set_time_m1882411177 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_time_m1882411177_ftn) (AnimationState_t1303741697 *, float);
	static AnimationState_set_time_m1882411177_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_time_m1882411177_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
extern "C"  void AnimationState_set_normalizedTime_m3942659976 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_normalizedTime_m3942659976_ftn) (AnimationState_t1303741697 *, float);
	static AnimationState_set_normalizedTime_m3942659976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_normalizedTime_m3942659976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_normalizedTime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::set_speed(System.Single)
extern "C"  void AnimationState_set_speed_m465014523 (AnimationState_t1303741697 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_speed_m465014523_ftn) (AnimationState_t1303741697 *, float);
	static AnimationState_set_speed_m465014523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_speed_m465014523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AnimationState::get_length()
extern "C"  float AnimationState_get_length_m2895238571 (AnimationState_t1303741697 * __this, const MethodInfo* method)
{
	typedef float (*AnimationState_get_length_m2895238571_ftn) (AnimationState_t1303741697 *);
	static AnimationState_get_length_m2895238571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_length_m2895238571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_layer(System.Int32)
extern "C"  void AnimationState_set_layer_m139053567 (AnimationState_t1303741697 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_layer_m139053567_ftn) (AnimationState_t1303741697 *, int32_t);
	static AnimationState_set_layer_m139053567_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_layer_m139053567_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
extern "C"  void AnimationState_AddMixingTransform_m2640254805 (AnimationState_t1303741697 * __this, Transform_t3275118058 * ___mix0, bool ___recursive1, const MethodInfo* method)
{
	typedef void (*AnimationState_AddMixingTransform_m2640254805_ftn) (AnimationState_t1303741697 *, Transform_t3275118058 *, bool);
	static AnimationState_AddMixingTransform_m2640254805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_AddMixingTransform_m2640254805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___mix0, ___recursive1);
}
// System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform)
extern "C"  void AnimationState_AddMixingTransform_m668666486 (AnimationState_t1303741697 * __this, Transform_t3275118058 * ___mix0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		Transform_t3275118058 * L_0 = ___mix0;
		bool L_1 = V_0;
		AnimationState_AddMixingTransform_m2640254805(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.AnimationState::get_name()
extern "C"  String_t* AnimationState_get_name_m1043580151 (AnimationState_t1303741697 * __this, const MethodInfo* method)
{
	typedef String_t* (*AnimationState_get_name_m1043580151_ftn) (AnimationState_t1303741697 *);
	static AnimationState_get_name_m1043580151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_get_name_m1043580151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
extern "C"  void AnimationState_set_blendMode_m3997078040 (AnimationState_t1303741697 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationState_set_blendMode_m3997078040_ftn) (AnimationState_t1303741697 *, int32_t);
	static AnimationState_set_blendMode_m3997078040_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationState_set_blendMode_m3997078040_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Animator::get_isHuman()
extern "C"  bool Animator_get_isHuman_m1301163560 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_isHuman_m1301163560_ftn) (Animator_t69676727 *);
	static Animator_get_isHuman_m1301163560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_isHuman_m1301163560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_isHuman()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_humanScale()
extern "C"  float Animator_get_humanScale_m658813206 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_humanScale_m658813206_ftn) (Animator_t69676727 *);
	static Animator_get_humanScale_m658813206_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_humanScale_m658813206_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_humanScale()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::GetFloat(System.Int32)
extern "C"  float Animator_GetFloat_m3714897923 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = Animator_GetFloatID_m2459156264(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single)
extern "C"  void Animator_SetFloat_m2956422680 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___value1;
		Animator_SetFloatID_m315788701(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloat_m871614584 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		float L_1 = ___value1;
		float L_2 = ___dampTime2;
		float L_3 = ___deltaTime3;
		Animator_SetFloatIDDamp_m3704956665(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::GetBool(System.Int32)
extern "C"  bool Animator_GetBool_m919083767 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		bool L_1 = Animator_GetBoolID_m618935642(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.Int32,System.Boolean)
extern "C"  void Animator_SetBool_m2272870950 (Animator_t69676727 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		bool L_1 = ___value1;
		Animator_SetBoolID_m1875705373(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Animator::GetInteger(System.Int32)
extern "C"  int32_t Animator_GetInteger_m1012099561 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = Animator_GetIntegerID_m224950522(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Animator::SetInteger(System.Int32,System.Int32)
extern "C"  void Animator_SetInteger_m1464706502 (Animator_t69676727 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		int32_t L_1 = ___value1;
		Animator_SetIntegerID_m1279848667(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m3418492570 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_SetTriggerString_m2002790359(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m865269317 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_ResetTriggerString_m1445965342(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurve(System.String)
extern "C"  bool Animator_IsParameterControlledByCurve_m4289614957 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		bool L_1 = Animator_IsParameterControlledByCurveString_m1958306560(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C"  Vector3_t2243707580  Animator_get_deltaPosition_m1710146426 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_deltaPosition_m3555533205(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_deltaPosition_m3555533205 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_deltaPosition_m3555533205_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_get_deltaPosition_m3555533205_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_deltaPosition_m3555533205_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_deltaRotation()
extern "C"  Quaternion_t4030073918  Animator_get_deltaRotation_m1211723535 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_deltaRotation_m1178344088(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_deltaRotation_m1178344088 (Animator_t69676727 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_deltaRotation_m1178344088_ftn) (Animator_t69676727 *, Quaternion_t4030073918 *);
	static Animator_INTERNAL_get_deltaRotation_m1178344088_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_deltaRotation_m1178344088_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_rootPosition()
extern "C"  Vector3_t2243707580  Animator_get_rootPosition_m240413096 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_rootPosition_m1694908883(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_rootPosition_m1694908883 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_rootPosition_m1694908883_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_get_rootPosition_m1694908883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_rootPosition_m1694908883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C"  Quaternion_t4030073918  Animator_get_rootRotation_m1814890025 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_rootRotation_m4269311362(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_rootRotation_m4269311362 (Animator_t69676727 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_rootRotation_m4269311362_ftn) (Animator_t69676727 *, Quaternion_t4030073918 *);
	static Animator_INTERNAL_get_rootRotation_m4269311362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_rootRotation_m4269311362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Animator::get_applyRootMotion()
extern "C"  bool Animator_get_applyRootMotion_m915232763 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_applyRootMotion_m915232763_ftn) (Animator_t69676727 *);
	static Animator_get_applyRootMotion_m915232763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_applyRootMotion_m915232763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_applyRootMotion()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C"  void Animator_set_applyRootMotion_m635228566 (Animator_t69676727 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_applyRootMotion_m635228566_ftn) (Animator_t69676727 *, bool);
	static Animator_set_applyRootMotion_m635228566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_applyRootMotion_m635228566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_applyRootMotion(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_gravityWeight()
extern "C"  float Animator_get_gravityWeight_m449373735 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_gravityWeight_m449373735_ftn) (Animator_t69676727 *);
	static Animator_get_gravityWeight_m449373735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_gravityWeight_m449373735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_gravityWeight()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_bodyPosition()
extern "C"  Vector3_t2243707580  Animator_get_bodyPosition_m3886685512 (Animator_t69676727 * __this, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = Animator_GetBodyPositionInternal_m1960999654(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Animator::set_bodyPosition(UnityEngine.Vector3)
extern "C"  void Animator_set_bodyPosition_m3816012379 (Animator_t69676727 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = ___value0;
		Animator_SetBodyPositionInternal_m2126057571(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::GetBodyPositionInternal()
extern "C"  Vector3_t2243707580  Animator_GetBodyPositionInternal_m1960999654 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_CALL_GetBodyPositionInternal_m3696917400(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetBodyPositionInternal_m3696917400 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___value1, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_GetBodyPositionInternal_m3696917400_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_CALL_GetBodyPositionInternal_m3696917400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_GetBodyPositionInternal_m3696917400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_GetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___value1);
}
// System.Void UnityEngine.Animator::SetBodyPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetBodyPositionInternal_m2126057571 (Animator_t69676727 * __this, Vector3_t2243707580  ___bodyPosition0, const MethodInfo* method)
{
	{
		Animator_INTERNAL_CALL_SetBodyPositionInternal_m4293717132(NULL /*static, unused*/, __this, (&___bodyPosition0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetBodyPositionInternal_m4293717132 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___bodyPosition1, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetBodyPositionInternal_m4293717132_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_CALL_SetBodyPositionInternal_m4293717132_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetBodyPositionInternal_m4293717132_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___bodyPosition1);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_bodyRotation()
extern "C"  Quaternion_t4030073918  Animator_get_bodyRotation_m2990103407 (Animator_t69676727 * __this, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = Animator_GetBodyRotationInternal_m208861807(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Animator::set_bodyRotation(UnityEngine.Quaternion)
extern "C"  void Animator_set_bodyRotation_m3092584822 (Animator_t69676727 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = ___value0;
		Animator_SetBodyRotationInternal_m1205087780(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::GetBodyRotationInternal()
extern "C"  Quaternion_t4030073918  Animator_GetBodyRotationInternal_m208861807 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_CALL_GetBodyRotationInternal_m2953975611(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetBodyRotationInternal_m2953975611 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Quaternion_t4030073918 * ___value1, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_GetBodyRotationInternal_m2953975611_ftn) (Animator_t69676727 *, Quaternion_t4030073918 *);
	static Animator_INTERNAL_CALL_GetBodyRotationInternal_m2953975611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_GetBodyRotationInternal_m2953975611_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_GetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___value1);
}
// System.Void UnityEngine.Animator::SetBodyRotationInternal(UnityEngine.Quaternion)
extern "C"  void Animator_SetBodyRotationInternal_m1205087780 (Animator_t69676727 * __this, Quaternion_t4030073918  ___bodyRotation0, const MethodInfo* method)
{
	{
		Animator_INTERNAL_CALL_SetBodyRotationInternal_m1212579919(NULL /*static, unused*/, __this, (&___bodyRotation0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetBodyRotationInternal_m1212579919 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Quaternion_t4030073918 * ___bodyRotation1, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetBodyRotationInternal_m1212579919_ftn) (Animator_t69676727 *, Quaternion_t4030073918 *);
	static Animator_INTERNAL_CALL_SetBodyRotationInternal_m1212579919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetBodyRotationInternal_m1212579919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___bodyRotation1);
}
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPosition(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t2243707580  Animator_GetIKPosition_m184972236 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Vector3_t2243707580  L_1 = Animator_GetIKPositionInternal_m2286444847(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::GetIKPositionInternal(UnityEngine.AvatarIKGoal)
extern "C"  Vector3_t2243707580  Animator_GetIKPositionInternal_m2286444847 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_GetIKPositionInternal_m2121966543(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_GetIKPositionInternal_m2121966543 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_GetIKPositionInternal_m2121966543_ftn) (Animator_t69676727 *, int32_t, Vector3_t2243707580 *);
	static Animator_INTERNAL_CALL_GetIKPositionInternal_m2121966543_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_GetIKPositionInternal_m2121966543_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___goal1, ___value2);
}
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPosition_m3754744543 (Animator_t69676727 * __this, int32_t ___goal0, Vector3_t2243707580  ___goalPosition1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Vector3_t2243707580  L_1 = ___goalPosition1;
		Animator_SetIKPositionInternal_m627652436(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C"  void Animator_SetIKPositionInternal_m627652436 (Animator_t69676727 * __this, int32_t ___goal0, Vector3_t2243707580  ___goalPosition1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_SetIKPositionInternal_m2542413499(NULL /*static, unused*/, __this, L_0, (&___goalPosition1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetIKPositionInternal_m2542413499 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Vector3_t2243707580 * ___goalPosition2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetIKPositionInternal_m2542413499_ftn) (Animator_t69676727 *, int32_t, Vector3_t2243707580 *);
	static Animator_INTERNAL_CALL_SetIKPositionInternal_m2542413499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetIKPositionInternal_m2542413499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___goal1, ___goalPosition2);
}
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotation(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t4030073918  Animator_GetIKRotation_m1400774255 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Quaternion_t4030073918  L_1 = Animator_GetIKRotationInternal_m879540238(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Animator::GetIKRotationInternal(UnityEngine.AvatarIKGoal)
extern "C"  Quaternion_t4030073918  Animator_GetIKRotationInternal_m879540238 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_GetIKRotationInternal_m3409638438(NULL /*static, unused*/, __this, L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_GetIKRotationInternal_m3409638438 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Quaternion_t4030073918 * ___value2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_GetIKRotationInternal_m3409638438_ftn) (Animator_t69676727 *, int32_t, Quaternion_t4030073918 *);
	static Animator_INTERNAL_CALL_GetIKRotationInternal_m3409638438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_GetIKRotationInternal_m3409638438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___goal1, ___value2);
}
// System.Void UnityEngine.Animator::SetIKRotation(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotation_m4119847878 (Animator_t69676727 * __this, int32_t ___goal0, Quaternion_t4030073918  ___goalRotation1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		Quaternion_t4030073918  L_1 = ___goalRotation1;
		Animator_SetIKRotationInternal_m2182387227(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKRotationInternal(UnityEngine.AvatarIKGoal,UnityEngine.Quaternion)
extern "C"  void Animator_SetIKRotationInternal_m2182387227 (Animator_t69676727 * __this, int32_t ___goal0, Quaternion_t4030073918  ___goalRotation1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___goal0;
		Animator_INTERNAL_CALL_SetIKRotationInternal_m4087004338(NULL /*static, unused*/, __this, L_0, (&___goalRotation1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_CALL_SetIKRotationInternal_m4087004338 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, int32_t ___goal1, Quaternion_t4030073918 * ___goalRotation2, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetIKRotationInternal_m4087004338_ftn) (Animator_t69676727 *, int32_t, Quaternion_t4030073918 *);
	static Animator_INTERNAL_CALL_SetIKRotationInternal_m4087004338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetIKRotationInternal_m4087004338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___goal1, ___goalRotation2);
}
// System.Single UnityEngine.Animator::GetIKPositionWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeight_m2727777112 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = Animator_GetIKPositionWeightInternal_m4211544419(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKPositionWeightInternal_m4211544419 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	typedef float (*Animator_GetIKPositionWeightInternal_m4211544419_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetIKPositionWeightInternal_m4211544419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIKPositionWeightInternal_m4211544419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)");
	return _il2cpp_icall_func(__this, ___goal0);
}
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeight_m2782442723 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = ___value1;
		Animator_SetIKPositionWeightInternal_m3301773304(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKPositionWeightInternal_m3301773304 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetIKPositionWeightInternal_m3301773304_ftn) (Animator_t69676727 *, int32_t, float);
	static Animator_SetIKPositionWeightInternal_m3301773304_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIKPositionWeightInternal_m3301773304_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal0, ___value1);
}
// System.Single UnityEngine.Animator::GetIKRotationWeight(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeight_m3754472649 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = Animator_GetIKRotationWeightInternal_m953924988(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
extern "C"  float Animator_GetIKRotationWeightInternal_m953924988 (Animator_t69676727 * __this, int32_t ___goal0, const MethodInfo* method)
{
	typedef float (*Animator_GetIKRotationWeightInternal_m953924988_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetIKRotationWeightInternal_m953924988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIKRotationWeightInternal_m953924988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)");
	return _il2cpp_icall_func(__this, ___goal0);
}
// System.Void UnityEngine.Animator::SetIKRotationWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeight_m3417634268 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal0;
		float L_1 = ___value1;
		Animator_SetIKRotationWeightInternal_m3149308873(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C"  void Animator_SetIKRotationWeightInternal_m3149308873 (Animator_t69676727 * __this, int32_t ___goal0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetIKRotationWeightInternal_m3149308873_ftn) (Animator_t69676727 *, int32_t, float);
	static Animator_SetIKRotationWeightInternal_m3149308873_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIKRotationWeightInternal_m3149308873_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal0, ___value1);
}
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPosition_m2085986408 (Animator_t69676727 * __this, Vector3_t2243707580  ___lookAtPosition0, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = ___lookAtPosition0;
		Animator_SetLookAtPositionInternal_m1849644817(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C"  void Animator_SetLookAtPositionInternal_m1849644817 (Animator_t69676727 * __this, Vector3_t2243707580  ___lookAtPosition0, const MethodInfo* method)
{
	{
		Animator_INTERNAL_CALL_SetLookAtPositionInternal_m2150993438(NULL /*static, unused*/, __this, (&___lookAtPosition0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m2150993438 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___lookAtPosition1, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetLookAtPositionInternal_m2150993438_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_CALL_SetLookAtPositionInternal_m2150993438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetLookAtPositionInternal_m2150993438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___lookAtPosition1);
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m2799625122 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.5f);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = ___eyesWeight3;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m3872681091(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3239175203 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m3872681091(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m1126855746 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		V_2 = (1.0f);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = V_2;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m3872681091(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single)
extern "C"  void Animator_SetLookAtWeight_m3938735683 (Animator_t69676727 * __this, float ___weight0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		V_2 = (1.0f);
		V_3 = (0.0f);
		float L_0 = ___weight0;
		float L_1 = V_3;
		float L_2 = V_2;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m3872681091(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeight_m3872681091 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m4048236447(__this, /*hidden argument*/NULL);
		float L_0 = ___weight0;
		float L_1 = ___bodyWeight1;
		float L_2 = ___headWeight2;
		float L_3 = ___eyesWeight3;
		float L_4 = ___clampWeight4;
		Animator_SetLookAtWeightInternal_m1849982518(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetLookAtWeightInternal_m1849982518 (Animator_t69676727 * __this, float ___weight0, float ___bodyWeight1, float ___headWeight2, float ___eyesWeight3, float ___clampWeight4, const MethodInfo* method)
{
	typedef void (*Animator_SetLookAtWeightInternal_m1849982518_ftn) (Animator_t69676727 *, float, float, float, float, float);
	static Animator_SetLookAtWeightInternal_m1849982518_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLookAtWeightInternal_m1849982518_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___weight0, ___bodyWeight1, ___headWeight2, ___eyesWeight3, ___clampWeight4);
}
// System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
extern "C"  void Animator_set_stabilizeFeet_m224067765 (Animator_t69676727 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_stabilizeFeet_m224067765_ftn) (Animator_t69676727 *, bool);
	static Animator_set_stabilizeFeet_m224067765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_stabilizeFeet_m224067765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_stabilizeFeet(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Animator::get_layerCount()
extern "C"  int32_t Animator_get_layerCount_m2341760273 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animator_get_layerCount_m2341760273_ftn) (Animator_t69676727 *);
	static Animator_get_layerCount_m2341760273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layerCount_m2341760273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layerCount()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Animator::GetLayerName(System.Int32)
extern "C"  String_t* Animator_GetLayerName_m1585786478 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef String_t* (*Animator_GetLayerName_m1585786478_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetLayerName_m1585786478_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerName_m1585786478_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerName(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
extern "C"  float Animator_GetLayerWeight_m2228986294 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef float (*Animator_GetLayerWeight_m2228986294_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetLayerWeight_m2228986294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetLayerWeight_m2228986294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetLayerWeight(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
extern "C"  void Animator_SetLayerWeight_m2914961795 (Animator_t69676727 * __this, int32_t ___layerIndex0, float ___weight1, const MethodInfo* method)
{
	typedef void (*Animator_SetLayerWeight_m2914961795_ftn) (Animator_t69676727 *, int32_t, float);
	static Animator_SetLayerWeight_m2914961795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLayerWeight_m2914961795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___layerIndex0, ___weight1);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t2577870592  Animator_GetCurrentAnimatorStateInfo_m1931338898 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef AnimatorStateInfo_t2577870592  (*Animator_GetCurrentAnimatorStateInfo_m1931338898_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetCurrentAnimatorStateInfo_m1931338898_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetCurrentAnimatorStateInfo_m1931338898_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
extern "C"  AnimatorStateInfo_t2577870592  Animator_GetNextAnimatorStateInfo_m3085812524 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef AnimatorStateInfo_t2577870592  (*Animator_GetNextAnimatorStateInfo_m3085812524_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetNextAnimatorStateInfo_m3085812524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetNextAnimatorStateInfo_m3085812524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
extern "C"  AnimatorTransitionInfo_t2410896200  Animator_GetAnimatorTransitionInfo_m2734940813 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef AnimatorTransitionInfo_t2410896200  (*Animator_GetAnimatorTransitionInfo_m2734940813_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetAnimatorTransitionInfo_m2734940813_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetAnimatorTransitionInfo_m2734940813_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
extern "C"  bool Animator_IsInTransition_m3442920577 (Animator_t69676727 * __this, int32_t ___layerIndex0, const MethodInfo* method)
{
	typedef bool (*Animator_IsInTransition_m3442920577_ftn) (Animator_t69676727 *, int32_t);
	static Animator_IsInTransition_m3442920577_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsInTransition_m3442920577_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsInTransition(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex0);
}
// System.Single UnityEngine.Animator::get_feetPivotActive()
extern "C"  float Animator_get_feetPivotActive_m2453426283 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_feetPivotActive_m2453426283_ftn) (Animator_t69676727 *);
	static Animator_get_feetPivotActive_m2453426283_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_feetPivotActive_m2453426283_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_feetPivotActive()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
extern "C"  void Animator_set_feetPivotActive_m3070161808 (Animator_t69676727 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_feetPivotActive_m3070161808_ftn) (Animator_t69676727 *, float);
	static Animator_set_feetPivotActive_m3070161808_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_feetPivotActive_m3070161808_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_feetPivotActive(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_pivotWeight()
extern "C"  float Animator_get_pivotWeight_m1286872529 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_pivotWeight_m1286872529_ftn) (Animator_t69676727 *);
	static Animator_get_pivotWeight_m1286872529_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_pivotWeight_m1286872529_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_pivotWeight()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_pivotPosition()
extern "C"  Vector3_t2243707580  Animator_get_pivotPosition_m217711564 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_pivotPosition_m612074601(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_pivotPosition_m612074601 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_pivotPosition_m612074601_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_get_pivotPosition_m612074601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_pivotPosition_m612074601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::MatchTarget(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask,System.Single,System.Single)
extern "C"  void Animator_MatchTarget_m1237878570 (Animator_t69676727 * __this, Vector3_t2243707580  ___matchPosition0, Quaternion_t4030073918  ___matchRotation1, int32_t ___targetBodyPart2, MatchTargetWeightMask_t296470556  ___weightMask3, float ___startNormalizedTime4, float ___targetNormalizedTime5, const MethodInfo* method)
{
	{
		int32_t L_0 = ___targetBodyPart2;
		float L_1 = ___startNormalizedTime4;
		float L_2 = ___targetNormalizedTime5;
		Animator_INTERNAL_CALL_MatchTarget_m156824911(NULL /*static, unused*/, __this, (&___matchPosition0), (&___matchRotation1), L_0, (&___weightMask3), L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
extern "C"  void Animator_INTERNAL_CALL_MatchTarget_m156824911 (Il2CppObject * __this /* static, unused */, Animator_t69676727 * ___self0, Vector3_t2243707580 * ___matchPosition1, Quaternion_t4030073918 * ___matchRotation2, int32_t ___targetBodyPart3, MatchTargetWeightMask_t296470556 * ___weightMask4, float ___startNormalizedTime5, float ___targetNormalizedTime6, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_MatchTarget_m156824911_ftn) (Animator_t69676727 *, Vector3_t2243707580 *, Quaternion_t4030073918 *, int32_t, MatchTargetWeightMask_t296470556 *, float, float);
	static Animator_INTERNAL_CALL_MatchTarget_m156824911_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_MatchTarget_m156824911_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)");
	_il2cpp_icall_func(___self0, ___matchPosition1, ___matchRotation2, ___targetBodyPart3, ___weightMask4, ___startNormalizedTime5, ___targetNormalizedTime6);
}
// System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
extern "C"  void Animator_InterruptMatchTarget_m3522396796 (Animator_t69676727 * __this, bool ___completeMatch0, const MethodInfo* method)
{
	typedef void (*Animator_InterruptMatchTarget_m3522396796_ftn) (Animator_t69676727 *, bool);
	static Animator_InterruptMatchTarget_m3522396796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_InterruptMatchTarget_m3522396796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::InterruptMatchTarget(System.Boolean)");
	_il2cpp_icall_func(__this, ___completeMatch0);
}
// System.Boolean UnityEngine.Animator::get_isMatchingTarget()
extern "C"  bool Animator_get_isMatchingTarget_m2982519915 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_isMatchingTarget_m2982519915_ftn) (Animator_t69676727 *);
	static Animator_get_isMatchingTarget_m2982519915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_isMatchingTarget_m2982519915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_isMatchingTarget()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_speed()
extern "C"  float Animator_get_speed_m3591305940 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_speed_m3591305940_ftn) (Animator_t69676727 *);
	static Animator_get_speed_m3591305940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_speed_m3591305940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_speed()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C"  void Animator_set_speed_m3511108817 (Animator_t69676727 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_speed_m3511108817_ftn) (Animator_t69676727 *, float);
	static Animator_set_speed_m3511108817_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_speed_m3511108817_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m1296137205 (Animator_t69676727 * __this, String_t* ___stateName0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___transitionDuration1;
		int32_t L_3 = ___layer2;
		float L_4 = ___normalizedTime3;
		Animator_CrossFade_m3234175154(__this, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
extern "C"  void Animator_CrossFade_m3234175154 (Animator_t69676727 * __this, int32_t ___stateNameHash0, float ___transitionDuration1, int32_t ___layer2, float ___normalizedTime3, const MethodInfo* method)
{
	typedef void (*Animator_CrossFade_m3234175154_ftn) (Animator_t69676727 *, int32_t, float, int32_t, float);
	static Animator_CrossFade_m3234175154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_CrossFade_m3234175154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___transitionDuration1, ___layer2, ___normalizedTime3);
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m2181614708 (Animator_t69676727 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer1;
		float L_3 = ___normalizedTime2;
		Animator_Play_m3632052371(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m3632052371 (Animator_t69676727 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method)
{
	typedef void (*Animator_Play_m3632052371_ftn) (Animator_t69676727 *, int32_t, int32_t, float);
	static Animator_Play_m3632052371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_Play_m3632052371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___layer1, ___normalizedTime2);
}
// System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
extern "C"  void Animator_SetTarget_m3396402303 (Animator_t69676727 * __this, int32_t ___targetIndex0, float ___targetNormalizedTime1, const MethodInfo* method)
{
	typedef void (*Animator_SetTarget_m3396402303_ftn) (Animator_t69676727 *, int32_t, float);
	static Animator_SetTarget_m3396402303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTarget_m3396402303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)");
	_il2cpp_icall_func(__this, ___targetIndex0, ___targetNormalizedTime1);
}
// UnityEngine.Vector3 UnityEngine.Animator::get_targetPosition()
extern "C"  Vector3_t2243707580  Animator_get_targetPosition_m1133944793 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_targetPosition_m1320711800(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
extern "C"  void Animator_INTERNAL_get_targetPosition_m1320711800 (Animator_t69676727 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_targetPosition_m1320711800_ftn) (Animator_t69676727 *, Vector3_t2243707580 *);
	static Animator_INTERNAL_get_targetPosition_m1320711800_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_targetPosition_m1320711800_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_targetRotation()
extern "C"  Quaternion_t4030073918  Animator_get_targetRotation_m2166002050 (Animator_t69676727 * __this, const MethodInfo* method)
{
	Quaternion_t4030073918  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_INTERNAL_get_targetRotation_m1157869071(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t4030073918  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
extern "C"  void Animator_INTERNAL_get_targetRotation_m1157869071 (Animator_t69676727 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_targetRotation_m1157869071_ftn) (Animator_t69676727 *, Quaternion_t4030073918 *);
	static Animator_INTERNAL_get_targetRotation_m1157869071_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_targetRotation_m1157869071_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C"  Transform_t3275118058 * Animator_GetBoneTransform_m1849768799 (Animator_t69676727 * __this, int32_t ___humanBoneId0, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Animator_GetBoneTransform_m1849768799_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetBoneTransform_m1849768799_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoneTransform_m1849768799_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)");
	return _il2cpp_icall_func(__this, ___humanBoneId0);
}
// UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
extern "C"  int32_t Animator_get_cullingMode_m2883110248 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef int32_t (*Animator_get_cullingMode_m2883110248_ftn) (Animator_t69676727 *);
	static Animator_get_cullingMode_m2883110248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_cullingMode_m2883110248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_cullingMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
extern "C"  void Animator_set_cullingMode_m2321274215 (Animator_t69676727 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_cullingMode_m2321274215_ftn) (Animator_t69676727 *, int32_t);
	static Animator_set_cullingMode_m2321274215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_cullingMode_m2321274215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::StartPlayback()
extern "C"  void Animator_StartPlayback_m1725683593 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef void (*Animator_StartPlayback_m1725683593_ftn) (Animator_t69676727 *);
	static Animator_StartPlayback_m1725683593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StartPlayback_m1725683593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StartPlayback()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::StopPlayback()
extern "C"  void Animator_StopPlayback_m419679945 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef void (*Animator_StopPlayback_m419679945_ftn) (Animator_t69676727 *);
	static Animator_StopPlayback_m419679945_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StopPlayback_m419679945_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StopPlayback()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_playbackTime()
extern "C"  float Animator_get_playbackTime_m3236275835 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_playbackTime_m3236275835_ftn) (Animator_t69676727 *);
	static Animator_get_playbackTime_m3236275835_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_playbackTime_m3236275835_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_playbackTime()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_playbackTime(System.Single)
extern "C"  void Animator_set_playbackTime_m2149309450 (Animator_t69676727 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_playbackTime_m2149309450_ftn) (Animator_t69676727 *, float);
	static Animator_set_playbackTime_m2149309450_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_playbackTime_m2149309450_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_playbackTime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::StartRecording(System.Int32)
extern "C"  void Animator_StartRecording_m4246916954 (Animator_t69676727 * __this, int32_t ___frameCount0, const MethodInfo* method)
{
	typedef void (*Animator_StartRecording_m4246916954_ftn) (Animator_t69676727 *, int32_t);
	static Animator_StartRecording_m4246916954_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StartRecording_m4246916954_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StartRecording(System.Int32)");
	_il2cpp_icall_func(__this, ___frameCount0);
}
// System.Void UnityEngine.Animator::StopRecording()
extern "C"  void Animator_StopRecording_m3875895403 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef void (*Animator_StopRecording_m3875895403_ftn) (Animator_t69676727 *);
	static Animator_StopRecording_m3875895403_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StopRecording_m3875895403_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StopRecording()");
	_il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_recorderStartTime()
extern "C"  float Animator_get_recorderStartTime_m4211465364 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_recorderStartTime_m4211465364_ftn) (Animator_t69676727 *);
	static Animator_get_recorderStartTime_m4211465364_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_recorderStartTime_m4211465364_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_recorderStartTime()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_recorderStopTime()
extern "C"  float Animator_get_recorderStopTime_m1804781412 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_recorderStopTime_m1804781412_ftn) (Animator_t69676727 *);
	static Animator_get_recorderStopTime_m1804781412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_recorderStopTime_m1804781412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_recorderStopTime()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C"  RuntimeAnimatorController_t670468573 * Animator_get_runtimeAnimatorController_m652575931 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t670468573 * (*Animator_get_runtimeAnimatorController_m652575931_ftn) (Animator_t69676727 *);
	static Animator_get_runtimeAnimatorController_m652575931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m652575931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m3313850714 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m3313850714_ftn) (String_t*);
	static Animator_StringToHash_m3313850714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m3313850714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral778916875;
extern const uint32_t Animator_CheckIfInIKPass_m4048236447_MetadataUsageId;
extern "C"  void Animator_CheckIfInIKPass_m4048236447 (Animator_t69676727 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Animator_CheckIfInIKPass_m4048236447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Animator_get_logWarnings_m485644130(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		bool L_1 = Animator_CheckIfInIKPassInternal_m1624362606(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral778916875, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C"  bool Animator_CheckIfInIKPassInternal_m1624362606 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_CheckIfInIKPassInternal_m1624362606_ftn) (Animator_t69676727 *);
	static Animator_CheckIfInIKPassInternal_m1624362606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_CheckIfInIKPassInternal_m1624362606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::CheckIfInIKPassInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
extern "C"  void Animator_SetFloatID_m315788701 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatID_m315788701_ftn) (Animator_t69676727 *, int32_t, float);
	static Animator_SetFloatID_m315788701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatID_m315788701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatID(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Single UnityEngine.Animator::GetFloatID(System.Int32)
extern "C"  float Animator_GetFloatID_m2459156264 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method)
{
	typedef float (*Animator_GetFloatID_m2459156264_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetFloatID_m2459156264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetFloatID_m2459156264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetFloatID(System.Int32)");
	return _il2cpp_icall_func(__this, ___id0);
}
// System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
extern "C"  void Animator_SetBoolID_m1875705373 (Animator_t69676727 * __this, int32_t ___id0, bool ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolID_m1875705373_ftn) (Animator_t69676727 *, int32_t, bool);
	static Animator_SetBoolID_m1875705373_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolID_m1875705373_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
extern "C"  bool Animator_GetBoolID_m618935642 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method)
{
	typedef bool (*Animator_GetBoolID_m618935642_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetBoolID_m618935642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoolID_m618935642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoolID(System.Int32)");
	return _il2cpp_icall_func(__this, ___id0);
}
// System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
extern "C"  void Animator_SetIntegerID_m1279848667 (Animator_t69676727 * __this, int32_t ___id0, int32_t ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetIntegerID_m1279848667_ftn) (Animator_t69676727 *, int32_t, int32_t);
	static Animator_SetIntegerID_m1279848667_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIntegerID_m1279848667_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___id0, ___value1);
}
// System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
extern "C"  int32_t Animator_GetIntegerID_m224950522 (Animator_t69676727 * __this, int32_t ___id0, const MethodInfo* method)
{
	typedef int32_t (*Animator_GetIntegerID_m224950522_ftn) (Animator_t69676727 *, int32_t);
	static Animator_GetIntegerID_m224950522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetIntegerID_m224950522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetIntegerID(System.Int32)");
	return _il2cpp_icall_func(__this, ___id0);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m2002790359 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m2002790359_ftn) (Animator_t69676727 *, String_t*);
	static Animator_SetTriggerString_m2002790359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m2002790359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1445965342 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m1445965342_ftn) (Animator_t69676727 *, String_t*);
	static Animator_ResetTriggerString_m1445965342_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m1445965342_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
extern "C"  bool Animator_IsParameterControlledByCurveString_m1958306560 (Animator_t69676727 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef bool (*Animator_IsParameterControlledByCurveString_m1958306560_ftn) (Animator_t69676727 *, String_t*);
	static Animator_IsParameterControlledByCurveString_m1958306560_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_IsParameterControlledByCurveString_m1958306560_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::IsParameterControlledByCurveString(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
extern "C"  void Animator_SetFloatIDDamp_m3704956665 (Animator_t69676727 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatIDDamp_m3704956665_ftn) (Animator_t69676727 *, int32_t, float, float, float);
	static Animator_SetFloatIDDamp_m3704956665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatIDDamp_m3704956665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___id0, ___value1, ___dampTime2, ___deltaTime3);
}
// System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
extern "C"  bool Animator_get_layersAffectMassCenter_m635095639 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_layersAffectMassCenter_m635095639_ftn) (Animator_t69676727 *);
	static Animator_get_layersAffectMassCenter_m635095639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_layersAffectMassCenter_m635095639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_layersAffectMassCenter()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
extern "C"  void Animator_set_layersAffectMassCenter_m2611371262 (Animator_t69676727 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_layersAffectMassCenter_m2611371262_ftn) (Animator_t69676727 *, bool);
	static Animator_set_layersAffectMassCenter_m2611371262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_layersAffectMassCenter_m2611371262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
extern "C"  float Animator_get_leftFeetBottomHeight_m3607592682 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_leftFeetBottomHeight_m3607592682_ftn) (Animator_t69676727 *);
	static Animator_get_leftFeetBottomHeight_m3607592682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_leftFeetBottomHeight_m3607592682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_leftFeetBottomHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
extern "C"  float Animator_get_rightFeetBottomHeight_m276028979 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef float (*Animator_get_rightFeetBottomHeight_m276028979_ftn) (Animator_t69676727 *);
	static Animator_get_rightFeetBottomHeight_m276028979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_rightFeetBottomHeight_m276028979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_rightFeetBottomHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C"  bool Animator_get_logWarnings_m485644130 (Animator_t69676727 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_logWarnings_m485644130_ftn) (Animator_t69676727 *);
	static Animator_get_logWarnings_m485644130_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_logWarnings_m485644130_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_logWarnings()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t3905751349_marshal_pinvoke(const AnimatorClipInfo_t3905751349& unmarshaled, AnimatorClipInfo_t3905751349_marshaled_pinvoke& marshaled)
{
	marshaled.___m_ClipInstanceID_0 = unmarshaled.get_m_ClipInstanceID_0();
	marshaled.___m_Weight_1 = unmarshaled.get_m_Weight_1();
}
extern "C" void AnimatorClipInfo_t3905751349_marshal_pinvoke_back(const AnimatorClipInfo_t3905751349_marshaled_pinvoke& marshaled, AnimatorClipInfo_t3905751349& unmarshaled)
{
	int32_t unmarshaled_m_ClipInstanceID_temp_0 = 0;
	unmarshaled_m_ClipInstanceID_temp_0 = marshaled.___m_ClipInstanceID_0;
	unmarshaled.set_m_ClipInstanceID_0(unmarshaled_m_ClipInstanceID_temp_0);
	float unmarshaled_m_Weight_temp_1 = 0.0f;
	unmarshaled_m_Weight_temp_1 = marshaled.___m_Weight_1;
	unmarshaled.set_m_Weight_1(unmarshaled_m_Weight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t3905751349_marshal_pinvoke_cleanup(AnimatorClipInfo_t3905751349_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t3905751349_marshal_com(const AnimatorClipInfo_t3905751349& unmarshaled, AnimatorClipInfo_t3905751349_marshaled_com& marshaled)
{
	marshaled.___m_ClipInstanceID_0 = unmarshaled.get_m_ClipInstanceID_0();
	marshaled.___m_Weight_1 = unmarshaled.get_m_Weight_1();
}
extern "C" void AnimatorClipInfo_t3905751349_marshal_com_back(const AnimatorClipInfo_t3905751349_marshaled_com& marshaled, AnimatorClipInfo_t3905751349& unmarshaled)
{
	int32_t unmarshaled_m_ClipInstanceID_temp_0 = 0;
	unmarshaled_m_ClipInstanceID_temp_0 = marshaled.___m_ClipInstanceID_0;
	unmarshaled.set_m_ClipInstanceID_0(unmarshaled_m_ClipInstanceID_temp_0);
	float unmarshaled_m_Weight_temp_1 = 0.0f;
	unmarshaled_m_Weight_temp_1 = marshaled.___m_Weight_1;
	unmarshaled.set_m_Weight_1(unmarshaled_m_Weight_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorClipInfo
extern "C" void AnimatorClipInfo_t3905751349_marshal_com_cleanup(AnimatorClipInfo_t3905751349_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m4069203550 (AnimatorStateInfo_t2577870592 * __this, String_t* ___name0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_m_FullPath_2();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_m_Name_0();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_m_Path_1();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return (bool)G_B4_0;
	}
}
extern "C"  bool AnimatorStateInfo_IsName_m4069203550_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_IsName_m4069203550(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m3941998936 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FullPath_2();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m3941998936_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_fullPathHash_m3941998936(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m1703033713 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Path_1();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m1703033713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_nameHash_m1703033713(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m3915898263 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Name_0();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m3915898263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_shortNameHash_m3915898263(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C"  float AnimatorStateInfo_get_normalizedTime_m1330221276 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_normalizedTime_m1330221276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_normalizedTime_m1330221276(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C"  float AnimatorStateInfo_get_length_m3151009408 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Length_4();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_length_m3151009408_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_length_m3151009408(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C"  float AnimatorStateInfo_get_speed_m3265333371 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_Speed_5();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_speed_m3265333371_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_speed_m3265333371(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m3451382250 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_SpeedMultiplier_6();
		return L_0;
	}
}
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m3451382250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_speedMultiplier_m3451382250(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m1559842952 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Tag_7();
		return L_0;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m1559842952_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_tagHash_m1559842952(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C"  bool AnimatorStateInfo_IsTag_m1529621725 (AnimatorStateInfo_t2577870592 * __this, String_t* ___tag0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Tag_7();
		return (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
extern "C"  bool AnimatorStateInfo_IsTag_m1529621725_AdjustorThunk (Il2CppObject * __this, String_t* ___tag0, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_IsTag_m1529621725(_thisAdjusted, ___tag0, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C"  bool AnimatorStateInfo_get_loop_m765573376 (AnimatorStateInfo_t2577870592 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Loop_8();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool AnimatorStateInfo_get_loop_m765573376_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t2577870592 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t2577870592 *>(__this + 1);
	return AnimatorStateInfo_get_loop_m765573376(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t2577870592_marshal_pinvoke(const AnimatorStateInfo_t2577870592& unmarshaled, AnimatorStateInfo_t2577870592_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Name_0 = unmarshaled.get_m_Name_0();
	marshaled.___m_Path_1 = unmarshaled.get_m_Path_1();
	marshaled.___m_FullPath_2 = unmarshaled.get_m_FullPath_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_Length_4 = unmarshaled.get_m_Length_4();
	marshaled.___m_Speed_5 = unmarshaled.get_m_Speed_5();
	marshaled.___m_SpeedMultiplier_6 = unmarshaled.get_m_SpeedMultiplier_6();
	marshaled.___m_Tag_7 = unmarshaled.get_m_Tag_7();
	marshaled.___m_Loop_8 = unmarshaled.get_m_Loop_8();
}
extern "C" void AnimatorStateInfo_t2577870592_marshal_pinvoke_back(const AnimatorStateInfo_t2577870592_marshaled_pinvoke& marshaled, AnimatorStateInfo_t2577870592& unmarshaled)
{
	int32_t unmarshaled_m_Name_temp_0 = 0;
	unmarshaled_m_Name_temp_0 = marshaled.___m_Name_0;
	unmarshaled.set_m_Name_0(unmarshaled_m_Name_temp_0);
	int32_t unmarshaled_m_Path_temp_1 = 0;
	unmarshaled_m_Path_temp_1 = marshaled.___m_Path_1;
	unmarshaled.set_m_Path_1(unmarshaled_m_Path_temp_1);
	int32_t unmarshaled_m_FullPath_temp_2 = 0;
	unmarshaled_m_FullPath_temp_2 = marshaled.___m_FullPath_2;
	unmarshaled.set_m_FullPath_2(unmarshaled_m_FullPath_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	float unmarshaled_m_Length_temp_4 = 0.0f;
	unmarshaled_m_Length_temp_4 = marshaled.___m_Length_4;
	unmarshaled.set_m_Length_4(unmarshaled_m_Length_temp_4);
	float unmarshaled_m_Speed_temp_5 = 0.0f;
	unmarshaled_m_Speed_temp_5 = marshaled.___m_Speed_5;
	unmarshaled.set_m_Speed_5(unmarshaled_m_Speed_temp_5);
	float unmarshaled_m_SpeedMultiplier_temp_6 = 0.0f;
	unmarshaled_m_SpeedMultiplier_temp_6 = marshaled.___m_SpeedMultiplier_6;
	unmarshaled.set_m_SpeedMultiplier_6(unmarshaled_m_SpeedMultiplier_temp_6);
	int32_t unmarshaled_m_Tag_temp_7 = 0;
	unmarshaled_m_Tag_temp_7 = marshaled.___m_Tag_7;
	unmarshaled.set_m_Tag_7(unmarshaled_m_Tag_temp_7);
	int32_t unmarshaled_m_Loop_temp_8 = 0;
	unmarshaled_m_Loop_temp_8 = marshaled.___m_Loop_8;
	unmarshaled.set_m_Loop_8(unmarshaled_m_Loop_temp_8);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t2577870592_marshal_pinvoke_cleanup(AnimatorStateInfo_t2577870592_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t2577870592_marshal_com(const AnimatorStateInfo_t2577870592& unmarshaled, AnimatorStateInfo_t2577870592_marshaled_com& marshaled)
{
	marshaled.___m_Name_0 = unmarshaled.get_m_Name_0();
	marshaled.___m_Path_1 = unmarshaled.get_m_Path_1();
	marshaled.___m_FullPath_2 = unmarshaled.get_m_FullPath_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_Length_4 = unmarshaled.get_m_Length_4();
	marshaled.___m_Speed_5 = unmarshaled.get_m_Speed_5();
	marshaled.___m_SpeedMultiplier_6 = unmarshaled.get_m_SpeedMultiplier_6();
	marshaled.___m_Tag_7 = unmarshaled.get_m_Tag_7();
	marshaled.___m_Loop_8 = unmarshaled.get_m_Loop_8();
}
extern "C" void AnimatorStateInfo_t2577870592_marshal_com_back(const AnimatorStateInfo_t2577870592_marshaled_com& marshaled, AnimatorStateInfo_t2577870592& unmarshaled)
{
	int32_t unmarshaled_m_Name_temp_0 = 0;
	unmarshaled_m_Name_temp_0 = marshaled.___m_Name_0;
	unmarshaled.set_m_Name_0(unmarshaled_m_Name_temp_0);
	int32_t unmarshaled_m_Path_temp_1 = 0;
	unmarshaled_m_Path_temp_1 = marshaled.___m_Path_1;
	unmarshaled.set_m_Path_1(unmarshaled_m_Path_temp_1);
	int32_t unmarshaled_m_FullPath_temp_2 = 0;
	unmarshaled_m_FullPath_temp_2 = marshaled.___m_FullPath_2;
	unmarshaled.set_m_FullPath_2(unmarshaled_m_FullPath_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	float unmarshaled_m_Length_temp_4 = 0.0f;
	unmarshaled_m_Length_temp_4 = marshaled.___m_Length_4;
	unmarshaled.set_m_Length_4(unmarshaled_m_Length_temp_4);
	float unmarshaled_m_Speed_temp_5 = 0.0f;
	unmarshaled_m_Speed_temp_5 = marshaled.___m_Speed_5;
	unmarshaled.set_m_Speed_5(unmarshaled_m_Speed_temp_5);
	float unmarshaled_m_SpeedMultiplier_temp_6 = 0.0f;
	unmarshaled_m_SpeedMultiplier_temp_6 = marshaled.___m_SpeedMultiplier_6;
	unmarshaled.set_m_SpeedMultiplier_6(unmarshaled_m_SpeedMultiplier_temp_6);
	int32_t unmarshaled_m_Tag_temp_7 = 0;
	unmarshaled_m_Tag_temp_7 = marshaled.___m_Tag_7;
	unmarshaled.set_m_Tag_7(unmarshaled_m_Tag_temp_7);
	int32_t unmarshaled_m_Loop_temp_8 = 0;
	unmarshaled_m_Loop_temp_8 = marshaled.___m_Loop_8;
	unmarshaled.set_m_Loop_8(unmarshaled_m_Loop_temp_8);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorStateInfo
extern "C" void AnimatorStateInfo_t2577870592_marshal_com_cleanup(AnimatorStateInfo_t2577870592_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsName_m2561957738 (AnimatorTransitionInfo_t2410896200 * __this, String_t* ___name0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Name_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name0;
		int32_t L_4 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_FullPath_0();
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsName_m2561957738_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_IsName_m2561957738(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsUserName_m3771933881 (AnimatorTransitionInfo_t2410896200 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m3313850714(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_UserName_1();
		return (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
extern "C"  bool AnimatorTransitionInfo_IsUserName_m3771933881_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_IsUserName_m3771933881(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2689544352 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_FullPath_0();
		return L_0;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2689544352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_fullPathHash_m2689544352(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m460043277 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Name_2();
		return L_0;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m460043277_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_nameHash_m460043277(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m971244190 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_UserName_1();
		return L_0;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m971244190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_userNameHash_m971244190(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m2741999844 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		return L_0;
	}
}
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m2741999844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_normalizedTime_m2741999844(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C"  bool AnimatorTransitionInfo_get_anyState_m3474272231 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_AnyState_4();
		return L_0;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_anyState_m3474272231_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_anyState_m3474272231(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C"  bool AnimatorTransitionInfo_get_entry_m1140118520 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool AnimatorTransitionInfo_get_entry_m1140118520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_entry_m1140118520(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C"  bool AnimatorTransitionInfo_get_exit_m3018502934 (AnimatorTransitionInfo_t2410896200 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool AnimatorTransitionInfo_get_exit_m3018502934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2410896200 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2410896200 *>(__this + 1);
	return AnimatorTransitionInfo_get_exit_m3018502934(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke(const AnimatorTransitionInfo_t2410896200& unmarshaled, AnimatorTransitionInfo_t2410896200_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = unmarshaled.get_m_AnyState_4();
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_back(const AnimatorTransitionInfo_t2410896200_marshaled_pinvoke& marshaled, AnimatorTransitionInfo_t2410896200& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = marshaled.___m_AnyState_4;
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_pinvoke_cleanup(AnimatorTransitionInfo_t2410896200_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_com(const AnimatorTransitionInfo_t2410896200& unmarshaled, AnimatorTransitionInfo_t2410896200_marshaled_com& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = unmarshaled.get_m_AnyState_4();
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_com_back(const AnimatorTransitionInfo_t2410896200_marshaled_com& marshaled, AnimatorTransitionInfo_t2410896200& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = marshaled.___m_AnyState_4;
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2410896200_marshal_com_cleanup(AnimatorTransitionInfo_t2410896200_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m3885595876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Application_Quit_m3885595876_ftn) ();
	static Application_Quit_m3885595876_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m3885595876_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::CanStreamedLevelBeLoadedByName(System.String)
extern "C"  bool Application_CanStreamedLevelBeLoadedByName_m186115376 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method)
{
	typedef bool (*Application_CanStreamedLevelBeLoadedByName_m186115376_ftn) (String_t*);
	static Application_CanStreamedLevelBeLoadedByName_m186115376_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_CanStreamedLevelBeLoadedByName_m186115376_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::CanStreamedLevelBeLoadedByName(System.String)");
	return _il2cpp_icall_func(___levelName0);
}
// System.Boolean UnityEngine.Application::CanStreamedLevelBeLoaded(System.Int32)
extern "C"  bool Application_CanStreamedLevelBeLoaded_m2932847449 (Il2CppObject * __this /* static, unused */, int32_t ___levelIndex0, const MethodInfo* method)
{
	typedef bool (*Application_CanStreamedLevelBeLoaded_m2932847449_ftn) (int32_t);
	static Application_CanStreamedLevelBeLoaded_m2932847449_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_CanStreamedLevelBeLoaded_m2932847449_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::CanStreamedLevelBeLoaded(System.Int32)");
	return _il2cpp_icall_func(___levelIndex0);
}
// System.Boolean UnityEngine.Application::CanStreamedLevelBeLoaded(System.String)
extern "C"  bool Application_CanStreamedLevelBeLoaded_m2196620446 (Il2CppObject * __this /* static, unused */, String_t* ___levelName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___levelName0;
		bool L_1 = Application_CanStreamedLevelBeLoadedByName_m186115376(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m4091950718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m4091950718_ftn) ();
	static Application_get_isPlaying_m4091950718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m4091950718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m2474583393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m2474583393_ftn) ();
	static Application_get_isEditor_m2474583393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m2474583393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m3989224144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m3989224144_ftn) ();
	static Application_get_platform_m3989224144_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m3989224144_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)
extern "C"  void Application_CaptureScreenshot_m3594286264 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, int32_t ___superSize1, const MethodInfo* method)
{
	typedef void (*Application_CaptureScreenshot_m3594286264_ftn) (String_t*, int32_t);
	static Application_CaptureScreenshot_m3594286264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_CaptureScreenshot_m3594286264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)");
	_il2cpp_icall_func(___filename0, ___superSize1);
}
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C"  void Application_set_runInBackground_m3543179741 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Application_set_runInBackground_m3543179741_ftn) (bool);
	static Application_set_runInBackground_m3543179741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_runInBackground_m3543179741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_runInBackground(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C"  String_t* Application_get_persistentDataPath_m3129298355 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_persistentDataPath_m3129298355_ftn) ();
	static Application_get_persistentDataPath_m3129298355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_persistentDataPath_m3129298355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_persistentDataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_unityVersion()
extern "C"  String_t* Application_get_unityVersion_m3302058834 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_unityVersion_m3302058834_ftn) ();
	static Application_get_unityVersion_m3302058834_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_unityVersion_m3302058834_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_unityVersion()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern Il2CppClass* Application_t354826772_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLogCallback_m3408386792_MetadataUsageId;
extern "C"  void Application_CallLogCallback_m3408386792 (Il2CppObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m3408386792_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t1867914413 * V_0 = NULL;
	LogCallback_t1867914413 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t1867914413 * L_1 = ((Application_t354826772_StaticFields*)Application_t354826772_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandler_0();
		V_0 = L_1;
		LogCallback_t1867914413 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t1867914413 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m3921378796(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001b:
	{
		LogCallback_t1867914413 * L_7 = ((Application_t354826772_StaticFields*)Application_t354826772_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandlerThreaded_1();
		V_1 = L_7;
		LogCallback_t1867914413 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		LogCallback_t1867914413 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m3921378796(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m898185969 (LogCallback_t1867914413 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m3921378796 (LogCallback_t1867914413 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m3921378796((LogCallback_t1867914413 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t1867914413 (LogCallback_t1867914413 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern Il2CppClass* LogType_t1559732862_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m2485641857_MetadataUsageId;
extern "C"  Il2CppObject * LogCallback_BeginInvoke_m2485641857 (LogCallback_t1867914413 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m2485641857_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t1559732862_il2cpp_TypeInfo_var, &___type2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m1910375855 (LogCallback_t1867914413 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C"  void AssemblyIsEditorAssembly__ctor_m2639706522 (AssemblyIsEditorAssembly_t1557026495 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C"  void AssetBundleCreateRequest__ctor_m2967025878 (AssetBundleCreateRequest_t1038783543 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C"  AssetBundle_t2054978754 * AssetBundleCreateRequest_get_assetBundle_m2931245245 (AssetBundleCreateRequest_t1038783543 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t2054978754 * (*AssetBundleCreateRequest_get_assetBundle_m2931245245_ftn) (AssetBundleCreateRequest_t1038783543 *);
	static AssetBundleCreateRequest_get_assetBundle_m2931245245_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m2931245245_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.AssetBundleCreateRequest/CompatibilityCheck UnityEngine.AssetBundleCreateRequest::get_compatibilityChecks()
extern "C"  int32_t AssetBundleCreateRequest_get_compatibilityChecks_m1268624964 (AssetBundleCreateRequest_t1038783543 * __this, const MethodInfo* method)
{
	typedef int32_t (*AssetBundleCreateRequest_get_compatibilityChecks_m1268624964_ftn) (AssetBundleCreateRequest_t1038783543 *);
	static AssetBundleCreateRequest_get_compatibilityChecks_m1268624964_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_compatibilityChecks_m1268624964_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_compatibilityChecks()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::set_compatibilityChecks(UnityEngine.AssetBundleCreateRequest/CompatibilityCheck)
extern "C"  void AssetBundleCreateRequest_set_compatibilityChecks_m1667023067 (AssetBundleCreateRequest_t1038783543 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_set_compatibilityChecks_m1667023067_ftn) (AssetBundleCreateRequest_t1038783543 *, int32_t);
	static AssetBundleCreateRequest_set_compatibilityChecks_m1667023067_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_set_compatibilityChecks_m1667023067_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::set_compatibilityChecks(UnityEngine.AssetBundleCreateRequest/CompatibilityCheck)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C"  void AssetBundleRequest__ctor_m2870261062 (AssetBundleRequest_t2674559435 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2914860946(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C"  Object_t1021602117 * AssetBundleRequest_get_asset_m624603186 (AssetBundleRequest_t2674559435 * __this, const MethodInfo* method)
{
	typedef Object_t1021602117 * (*AssetBundleRequest_get_asset_m624603186_ftn) (AssetBundleRequest_t2674559435 *);
	static AssetBundleRequest_get_asset_m624603186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_asset_m624603186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_asset()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C"  ObjectU5BU5D_t4217747464* AssetBundleRequest_get_allAssets_m3314852268 (AssetBundleRequest_t2674559435 * __this, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t4217747464* (*AssetBundleRequest_get_allAssets_m3314852268_ftn) (AssetBundleRequest_t2674559435 *);
	static AssetBundleRequest_get_allAssets_m3314852268_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_allAssets_m3314852268_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_allAssets()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke(const AssetBundleRequest_t2674559435& unmarshaled, AssetBundleRequest_t2674559435_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_back(const AssetBundleRequest_t2674559435_marshaled_pinvoke& marshaled, AssetBundleRequest_t2674559435& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_pinvoke_cleanup(AssetBundleRequest_t2674559435_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_com(const AssetBundleRequest_t2674559435& unmarshaled, AssetBundleRequest_t2674559435_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t2674559435_marshal_com_back(const AssetBundleRequest_t2674559435_marshaled_com& marshaled, AssetBundleRequest_t2674559435& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t2674559435_marshal_com_cleanup(AssetBundleRequest_t2674559435_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2914860946 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m3312061823 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m3312061823_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_InternalDestroy_m3312061823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m3312061823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m67522446 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m3312061823(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern "C"  bool AsyncOperation_get_isDone_m1085614149 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_isDone_m1085614149_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_isDone_m1085614149_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_isDone_m1085614149_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.AsyncOperation::get_progress()
extern "C"  float AsyncOperation_get_progress_m478775228 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef float (*AsyncOperation_get_progress_m478775228_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_progress_m478775228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_progress_m478775228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_progress()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AsyncOperation::get_priority()
extern "C"  int32_t AsyncOperation_get_priority_m966065939 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef int32_t (*AsyncOperation_get_priority_m966065939_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_priority_m966065939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_priority_m966065939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_priority()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
extern "C"  void AsyncOperation_set_priority_m3581020444 (AsyncOperation_t3814632279 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_priority_m3581020444_ftn) (AsyncOperation_t3814632279 *, int32_t);
	static AsyncOperation_set_priority_m3581020444_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_priority_m3581020444_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_priority(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
extern "C"  bool AsyncOperation_get_allowSceneActivation_m2825451182 (AsyncOperation_t3814632279 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_allowSceneActivation_m2825451182_ftn) (AsyncOperation_t3814632279 *);
	static AsyncOperation_get_allowSceneActivation_m2825451182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_allowSceneActivation_m2825451182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_allowSceneActivation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
extern "C"  void AsyncOperation_set_allowSceneActivation_m3988498951 (AsyncOperation_t3814632279 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_allowSceneActivation_m3988498951_ftn) (AsyncOperation_t3814632279 *, bool);
	static AsyncOperation_set_allowSceneActivation_m3988498951_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_allowSceneActivation_m3988498951_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke(const AsyncOperation_t3814632279& unmarshaled, AsyncOperation_t3814632279_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_back(const AsyncOperation_t3814632279_marshaled_pinvoke& marshaled, AsyncOperation_t3814632279& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_pinvoke_cleanup(AsyncOperation_t3814632279_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_com(const AsyncOperation_t3814632279& unmarshaled, AsyncOperation_t3814632279_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t3814632279_marshal_com_back(const AsyncOperation_t3814632279_marshaled_com& marshaled, AsyncOperation_t3814632279& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t3814632279_marshal_com_cleanup(AsyncOperation_t3814632279_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern Il2CppClass* DisallowMultipleComponentU5BU5D_t674354611_il2cpp_TypeInfo_var;
extern Il2CppClass* AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteInEditModeU5BU5D_t1783660110_il2cpp_TypeInfo_var;
extern Il2CppClass* RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m1775592582_MetadataUsageId;
extern "C"  void AttributeHelperEngine__cctor_m1775592582 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m1775592582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t958797062_StaticFields*)AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var->static_fields)->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t674354611*)SZArrayNew(DisallowMultipleComponentU5BU5D_t674354611_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t958797062_StaticFields*)AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var->static_fields)->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t1783660110*)SZArrayNew(ExecuteInEditModeU5BU5D_t1783660110_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t958797062_StaticFields*)AttributeHelperEngine_t958797062_il2cpp_TypeInfo_var->static_fields)->set__requireComponentArray_2(((RequireComponentU5BU5D_t2214070761*)SZArrayNew(RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t1158329972_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t2656950_0_0_0_var;
extern Il2CppClass* Stack_1_t2391531380_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m1240888107_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m1219431354_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m685736912_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m1292854535_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m685343645_MetadataUsageId;
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m685343645 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m685343645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t2391531380 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t3614634134* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Stack_1_t2391531380 * L_0 = (Stack_1_t2391531380 *)il2cpp_codegen_object_new(Stack_1_t2391531380_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1240888107(L_0, /*hidden argument*/Stack_1__ctor_m1240888107_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t2391531380 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m1219431354(L_1, L_2, /*hidden argument*/Stack_1_Push_m1219431354_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005c;
	}

IL_0037:
	{
		Stack_1_t2391531380 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m685736912(L_8, /*hidden argument*/Stack_1_Pop_m685736912_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t2656950_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t3614634134* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t3614634134* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_005c;
		}
	}
	{
		Type_t * L_15 = V_1;
		return L_15;
	}

IL_005c:
	{
		Stack_1_t2391531380 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m1292854535(L_16, /*hidden argument*/Stack_1_get_Count_m1292854535_MethodInfo_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t864575032_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t1158329972_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t672924358_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4047179467_MethodInfo_var;
extern const MethodInfo* List_1_Add_m176071399_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1070939693_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m120894667_MetadataUsageId;
extern "C"  TypeU5BU5D_t1664964607* AttributeHelperEngine_GetRequiredComponents_m120894667 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m120894667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t672924358 * V_0 = NULL;
	RequireComponentU5BU5D_t2214070761* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t864575032 * V_3 = NULL;
	RequireComponentU5BU5D_t2214070761* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t1664964607* V_6 = NULL;
	{
		V_0 = (List_1_t672924358 *)NULL;
		goto IL_00e0;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t864575032_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t3614634134* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t2214070761*)Castclass(L_2, RequireComponentU5BU5D_t2214070761_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t2214070761* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00d2;
	}

IL_0030:
	{
		RequireComponentU5BU5D_t2214070761* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		RequireComponent_t864575032 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t672924358 * L_10 = V_0;
		if (L_10)
		{
			goto IL_007b;
		}
	}
	{
		RequireComponentU5BU5D_t2214070761* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13))))
		{
			goto IL_007b;
		}
	}
	{
		TypeU5BU5D_t1664964607* L_14 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t864575032 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1664964607* L_17 = L_14;
		RequireComponent_t864575032 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t1664964607* L_20 = L_17;
		RequireComponent_t864575032 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t1664964607* L_23 = V_6;
		return L_23;
	}

IL_007b:
	{
		List_1_t672924358 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0087;
		}
	}
	{
		List_1_t672924358 * L_25 = (List_1_t672924358 *)il2cpp_codegen_object_new(List_1_t672924358_il2cpp_TypeInfo_var);
		List_1__ctor_m4047179467(L_25, /*hidden argument*/List_1__ctor_m4047179467_MethodInfo_var);
		V_0 = L_25;
	}

IL_0087:
	{
		RequireComponent_t864575032 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_009e;
		}
	}
	{
		List_1_t672924358 * L_28 = V_0;
		RequireComponent_t864575032 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m176071399(L_28, L_30, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
	}

IL_009e:
	{
		RequireComponent_t864575032 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00b5;
		}
	}
	{
		List_1_t672924358 * L_33 = V_0;
		RequireComponent_t864575032 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m176071399(L_33, L_35, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
	}

IL_00b5:
	{
		RequireComponent_t864575032 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00cc;
		}
	}
	{
		List_1_t672924358 * L_38 = V_0;
		RequireComponent_t864575032 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m176071399(L_38, L_40, /*hidden argument*/List_1_Add_m176071399_MethodInfo_var);
	}

IL_00cc:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00d2:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t2214070761* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00e0:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_00f6;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_46) == ((Il2CppObject*)(Type_t *)L_47))))
		{
			goto IL_0007;
		}
	}

IL_00f6:
	{
		List_1_t672924358 * L_48 = V_0;
		if (L_48)
		{
			goto IL_00fe;
		}
	}
	{
		return (TypeU5BU5D_t1664964607*)NULL;
	}

IL_00fe:
	{
		List_1_t672924358 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t1664964607* L_50 = List_1_ToArray_m1070939693(L_49, /*hidden argument*/List_1_ToArray_m1070939693_MethodInfo_var);
		return L_50;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t3043633143_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t1158329972_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m2980171478_MetadataUsageId;
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m2980171478 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m2980171478_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t3614634134* V_0 = NULL;
	int32_t V_1 = 0;
	{
		goto IL_002b;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t3043633143_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t3614634134* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t3614634134*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t3614634134* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (bool)1;
	}

IL_0023:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_002b:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0041;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t1158329972_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0005;
		}
	}

IL_0041:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.Audio.AudioMixer::SetFloat(System.String,System.Single)
extern "C"  bool AudioMixer_SetFloat_m1339880289 (AudioMixer_t3244290001 * __this, String_t* ___name0, float ___value1, const MethodInfo* method)
{
	typedef bool (*AudioMixer_SetFloat_m1339880289_ftn) (AudioMixer_t3244290001 *, String_t*, float);
	static AudioMixer_SetFloat_m1339880289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioMixer_SetFloat_m1339880289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioMixer::SetFloat(System.String,System.Single)");
	return _il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Boolean UnityEngine.Audio.AudioMixer::GetFloat(System.String,System.Single&)
extern "C"  bool AudioMixer_GetFloat_m2814101327 (AudioMixer_t3244290001 * __this, String_t* ___name0, float* ___value1, const MethodInfo* method)
{
	typedef bool (*AudioMixer_GetFloat_m2814101327_ftn) (AudioMixer_t3244290001 *, String_t*, float*);
	static AudioMixer_GetFloat_m2814101327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioMixer_GetFloat_m2814101327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Audio.AudioMixer::GetFloat(System.String,System.Single&)");
	return _il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Single UnityEngine.AudioClip::get_length()
extern "C"  float AudioClip_get_length_m3881628918 (AudioClip_t1932558630 * __this, const MethodInfo* method)
{
	typedef float (*AudioClip_get_length_m3881628918_ftn) (AudioClip_t1932558630 *);
	static AudioClip_get_length_m3881628918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m3881628918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C"  void AudioClip_InvokePCMReaderCallback_Internal_m1966286598 (AudioClip_t1932558630 * __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method)
{
	{
		PCMReaderCallback_t3007145346 * L_0 = __this->get_m_PCMReaderCallback_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t3007145346 * L_1 = __this->get_m_PCMReaderCallback_2();
		SingleU5BU5D_t577127397* L_2 = ___data0;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m3610389815(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C"  void AudioClip_InvokePCMSetPositionCallback_Internal_m2304858844 (AudioClip_t1932558630 * __this, int32_t ___position0, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t421863554 * L_0 = __this->get_m_PCMSetPositionCallback_3();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t421863554 * L_1 = __this->get_m_PCMSetPositionCallback_3();
		int32_t L_2 = ___position0;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m651987035(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMReaderCallback__ctor_m4217492708 (PCMReaderCallback_t3007145346 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m3610389815 (PCMReaderCallback_t3007145346 * __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMReaderCallback_Invoke_m3610389815((PCMReaderCallback_t3007145346 *)__this->get_prev_9(),___data0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMReaderCallback_t3007145346 (PCMReaderCallback_t3007145346 * __this, SingleU5BU5D_t577127397* ___data0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(float*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___data0' to native representation
	float* ____data0_marshaled = NULL;
	____data0_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data0);

	// Native function invocation
	il2cppPInvokeFunc(____data0_marshaled);

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PCMReaderCallback_BeginInvoke_m3119095492 (PCMReaderCallback_t3007145346 * __this, SingleU5BU5D_t577127397* ___data0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMReaderCallback_EndInvoke_m909664362 (PCMReaderCallback_t3007145346 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMSetPositionCallback__ctor_m232778710 (PCMSetPositionCallback_t421863554 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m651987035 (PCMSetPositionCallback_t421863554 * __this, int32_t ___position0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMSetPositionCallback_Invoke_m651987035((PCMSetPositionCallback_t421863554 *)__this->get_prev_9(),___position0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMSetPositionCallback_t421863554 (PCMSetPositionCallback_t421863554 * __this, int32_t ___position0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___position0);

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t PCMSetPositionCallback_BeginInvoke_m2507142524_MetadataUsageId;
extern "C"  Il2CppObject * PCMSetPositionCallback_BeginInvoke_m2507142524 (PCMSetPositionCallback_t421863554 * __this, int32_t ___position0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PCMSetPositionCallback_BeginInvoke_m2507142524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___position0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMSetPositionCallback_EndInvoke_m4290184144 (PCMSetPositionCallback_t421863554 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioListener::set_volume(System.Single)
extern "C"  void AudioListener_set_volume_m1233107753 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioListener_set_volume_m1233107753_ftn) (float);
	static AudioListener_set_volume_m1233107753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioListener_set_volume_m1233107753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioListener::set_volume(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern Il2CppClass* AudioSettings_t3144015719_il2cpp_TypeInfo_var;
extern const uint32_t AudioSettings_InvokeOnAudioConfigurationChanged_m3225073778_MetadataUsageId;
extern "C"  void AudioSettings_InvokeOnAudioConfigurationChanged_m3225073778 (Il2CppObject * __this /* static, unused */, bool ___deviceWasChanged0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioConfigurationChanged_m3225073778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t3743753033 * L_0 = ((AudioSettings_t3144015719_StaticFields*)AudioSettings_t3144015719_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t3743753033 * L_1 = ((AudioSettings_t3144015719_StaticFields*)AudioSettings_t3144015719_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		bool L_2 = ___deviceWasChanged0;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m635374412(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AudioConfigurationChangeHandler__ctor_m114228029 (AudioConfigurationChangeHandler_t3743753033 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m635374412 (AudioConfigurationChangeHandler_t3743753033 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m635374412((AudioConfigurationChangeHandler_t3743753033 *)__this->get_prev_9(),___deviceWasChanged0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t3743753033 (AudioConfigurationChangeHandler_t3743753033 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___deviceWasChanged0);

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t AudioConfigurationChangeHandler_BeginInvoke_m2418670597_MetadataUsageId;
extern "C"  Il2CppObject * AudioConfigurationChangeHandler_BeginInvoke_m2418670597 (AudioConfigurationChangeHandler_t3743753033 * __this, bool ___deviceWasChanged0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioConfigurationChangeHandler_BeginInvoke_m2418670597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___deviceWasChanged0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AudioConfigurationChangeHandler_EndInvoke_m2237100787 (AudioConfigurationChangeHandler_t3743753033 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.AudioSource::get_volume()
extern "C"  float AudioSource_get_volume_m66289169 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m66289169_ftn) (AudioSource_t1135106623 *);
	static AudioSource_get_volume_m66289169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m66289169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C"  void AudioSource_set_volume_m2777308722 (AudioSource_t1135106623 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m2777308722_ftn) (AudioSource_t1135106623 *, float);
	static AudioSource_set_volume_m2777308722_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m2777308722_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C"  float AudioSource_get_pitch_m4220572439 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m4220572439_ftn) (AudioSource_t1135106623 *);
	static AudioSource_get_pitch_m4220572439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m4220572439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C"  void AudioSource_set_pitch_m3064416458 (AudioSource_t1135106623 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m3064416458_ftn) (AudioSource_t1135106623 *, float);
	static AudioSource_set_pitch_m3064416458_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m3064416458_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t1932558630 * AudioSource_get_clip_m2127996365 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	typedef AudioClip_t1932558630 * (*AudioSource_get_clip_m2127996365_ftn) (AudioSource_t1135106623 *);
	static AudioSource_get_clip_m2127996365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m2127996365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m738814682 (AudioSource_t1135106623 * __this, AudioClip_t1932558630 * ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m738814682_ftn) (AudioSource_t1135106623 *, AudioClip_t1932558630 *);
	static AudioSource_set_clip_m738814682_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m738814682_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C"  void AudioSource_Play_m889724421 (AudioSource_t1135106623 * __this, uint64_t ___delay0, const MethodInfo* method)
{
	typedef void (*AudioSource_Play_m889724421_ftn) (AudioSource_t1135106623 *, uint64_t);
	static AudioSource_Play_m889724421_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m889724421_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay0);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m353744792 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = V_0;
		AudioSource_Play_m889724421(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m3452679614 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	typedef void (*AudioSource_Stop_m3452679614_ftn) (AudioSource_t1135106623 *);
	static AudioSource_Stop_m3452679614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Stop_m3452679614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Stop()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::Pause()
extern "C"  void AudioSource_Pause_m71375470 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	{
		AudioSource_INTERNAL_CALL_Pause_m835013885(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)
extern "C"  void AudioSource_INTERNAL_CALL_Pause_m835013885 (Il2CppObject * __this /* static, unused */, AudioSource_t1135106623 * ___self0, const MethodInfo* method)
{
	typedef void (*AudioSource_INTERNAL_CALL_Pause_m835013885_ftn) (AudioSource_t1135106623 *);
	static AudioSource_INTERNAL_CALL_Pause_m835013885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_INTERNAL_CALL_Pause_m835013885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m3677592677 (AudioSource_t1135106623 * __this, const MethodInfo* method)
{
	typedef bool (*AudioSource_get_isPlaying_m3677592677_ftn) (AudioSource_t1135106623 *);
	static AudioSource_get_isPlaying_m3677592677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_isPlaying_m3677592677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShot_m4118899740 (AudioSource_t1135106623 * __this, AudioClip_t1932558630 * ___clip0, float ___volumeScale1, const MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m4118899740_ftn) (AudioSource_t1135106623 *, AudioClip_t1932558630 *, float);
	static AudioSource_PlayOneShot_m4118899740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m4118899740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip0, ___volumeScale1);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C"  void AudioSource_PlayOneShot_m286472761 (AudioSource_t1135106623 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t1932558630 * L_0 = ___clip0;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m4118899740(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::PlayClipAtPoint(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern const Il2CppType* AudioSource_t1135106623_0_0_0_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* AudioSource_t1135106623_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3185157184;
extern const uint32_t AudioSource_PlayClipAtPoint_m1469997862_MetadataUsageId;
extern "C"  void AudioSource_PlayClipAtPoint_m1469997862 (Il2CppObject * __this /* static, unused */, AudioClip_t1932558630 * ___clip0, Vector3_t2243707580  ___position1, float ___volume2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AudioSource_PlayClipAtPoint_m1469997862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	AudioSource_t1135106623 * V_1 = NULL;
	float G_B2_0 = 0.0f;
	GameObject_t1756533147 * G_B2_1 = NULL;
	float G_B1_0 = 0.0f;
	GameObject_t1756533147 * G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	GameObject_t1756533147 * G_B3_2 = NULL;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral3185157184, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = ___position1;
		NullCheck(L_2);
		Transform_set_position_m2469242620(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(AudioSource_t1135106623_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		Component_t3819376471 * L_6 = GameObject_AddComponent_m3757565614(L_4, L_5, /*hidden argument*/NULL);
		V_1 = ((AudioSource_t1135106623 *)CastclassSealed(L_6, AudioSource_t1135106623_il2cpp_TypeInfo_var));
		AudioSource_t1135106623 * L_7 = V_1;
		AudioClip_t1932558630 * L_8 = ___clip0;
		NullCheck(L_7);
		AudioSource_set_clip_m738814682(L_7, L_8, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_9 = V_1;
		NullCheck(L_9);
		AudioSource_set_spatialBlend_m387590113(L_9, (1.0f), /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_10 = V_1;
		float L_11 = ___volume2;
		NullCheck(L_10);
		AudioSource_set_volume_m2777308722(L_10, L_11, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_12 = V_1;
		NullCheck(L_12);
		AudioSource_Play_m353744792(L_12, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = V_0;
		AudioClip_t1932558630 * L_14 = ___clip0;
		NullCheck(L_14);
		float L_15 = AudioClip_get_length_m3881628918(L_14, /*hidden argument*/NULL);
		float L_16 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = L_15;
		G_B1_1 = L_13;
		if ((!(((float)L_16) < ((float)(0.01f)))))
		{
			G_B2_0 = L_15;
			G_B2_1 = L_13;
			goto IL_006c;
		}
	}
	{
		G_B3_0 = (0.01f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0071;
	}

IL_006c:
	{
		float L_17 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_17;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, G_B3_2, ((float)((float)G_B3_1*(float)G_B3_0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
extern "C"  void AudioSource_set_loop_m313035616 (AudioSource_t1135106623 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_loop_m313035616_ftn) (AudioSource_t1135106623 *, bool);
	static AudioSource_set_loop_m313035616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_loop_m313035616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_loop(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C"  void AudioSource_set_playOnAwake_m858475204 (AudioSource_t1135106623 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_playOnAwake_m858475204_ftn) (AudioSource_t1135106623 *, bool);
	static AudioSource_set_playOnAwake_m858475204_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_playOnAwake_m858475204_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
extern "C"  void AudioSource_set_spatialBlend_m387590113 (AudioSource_t1135106623 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_spatialBlend_m387590113_ftn) (AudioSource_t1135106623 *, float);
	static AudioSource_set_spatialBlend_m387590113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_spatialBlend_m387590113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_spatialBlend(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
extern "C"  void AudioSource_set_mute_m3645750033 (AudioSource_t1135106623 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_mute_m3645750033_ftn) (AudioSource_t1135106623 *, bool);
	static AudioSource_set_mute_m3645750033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_mute_m3645750033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_mute(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m2699265412 (Behaviour_t955675639 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m205306948(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m4079055610 (Behaviour_t955675639 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m4079055610_ftn) (Behaviour_t955675639 *);
	static Behaviour_get_enabled_m4079055610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m4079055610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1796096907 (Behaviour_t955675639 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m1796096907_ftn) (Behaviour_t955675639 *, bool);
	static Behaviour_set_enabled_m1796096907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m1796096907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m3838334305 (Behaviour_t955675639 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m3838334305_ftn) (Behaviour_t955675639 *);
	static Behaviour_get_isActiveAndEnabled_m3838334305_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m3838334305_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.BitStream::.ctor()
extern "C"  void BitStream__ctor_m3947899936 (BitStream_t1979465639 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serializeb(System.Int32&)
extern "C"  void BitStream_Serializeb_m2728727597 (BitStream_t1979465639 * __this, int32_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializeb_m2728727597_ftn) (BitStream_t1979465639 *, int32_t*);
	static BitStream_Serializeb_m2728727597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializeb_m2728727597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializeb(System.Int32&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializec(System.Char&)
extern "C"  void BitStream_Serializec_m2418807390 (BitStream_t1979465639 * __this, Il2CppChar* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializec_m2418807390_ftn) (BitStream_t1979465639 *, Il2CppChar*);
	static BitStream_Serializec_m2418807390_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializec_m2418807390_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializec(System.Char&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializes(System.Int16&)
extern "C"  void BitStream_Serializes_m1379945554 (BitStream_t1979465639 * __this, int16_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializes_m1379945554_ftn) (BitStream_t1979465639 *, int16_t*);
	static BitStream_Serializes_m1379945554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializes_m1379945554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializes(System.Int16&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializei(System.Int32&)
extern "C"  void BitStream_Serializei_m1463666006 (BitStream_t1979465639 * __this, int32_t* ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serializei_m1463666006_ftn) (BitStream_t1979465639 *, int32_t*);
	static BitStream_Serializei_m1463666006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializei_m1463666006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializei(System.Int32&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BitStream::Serializef(System.Single&,System.Single)
extern "C"  void BitStream_Serializef_m745934178 (BitStream_t1979465639 * __this, float* ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	typedef void (*BitStream_Serializef_m745934178_ftn) (BitStream_t1979465639 *, float*, float);
	static BitStream_Serializef_m745934178_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serializef_m745934178_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serializef(System.Single&,System.Single)");
	_il2cpp_icall_func(__this, ___value0, ___maximumDelta1);
}
// System.Void UnityEngine.BitStream::Serializeq(UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_Serializeq_m4825889 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	{
		Quaternion_t4030073918 * L_0 = ___value0;
		float L_1 = ___maximumDelta1;
		BitStream_INTERNAL_CALL_Serializeq_m2945252026(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_INTERNAL_CALL_Serializeq_m2945252026 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___self0, Quaternion_t4030073918 * ___value1, float ___maximumDelta2, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializeq_m2945252026_ftn) (BitStream_t1979465639 *, Quaternion_t4030073918 *, float);
	static BitStream_INTERNAL_CALL_Serializeq_m2945252026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializeq_m2945252026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)");
	_il2cpp_icall_func(___self0, ___value1, ___maximumDelta2);
}
// System.Void UnityEngine.BitStream::Serializev(UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_Serializev_m2025246298 (BitStream_t1979465639 * __this, Vector3_t2243707580 * ___value0, float ___maximumDelta1, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = ___value0;
		float L_1 = ___maximumDelta1;
		BitStream_INTERNAL_CALL_Serializev_m928884041(NULL /*static, unused*/, __this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_INTERNAL_CALL_Serializev_m928884041 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___self0, Vector3_t2243707580 * ___value1, float ___maximumDelta2, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializev_m928884041_ftn) (BitStream_t1979465639 *, Vector3_t2243707580 *, float);
	static BitStream_INTERNAL_CALL_Serializev_m928884041_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializev_m928884041_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)");
	_il2cpp_icall_func(___self0, ___value1, ___maximumDelta2);
}
// System.Void UnityEngine.BitStream::Serializen(UnityEngine.NetworkViewID&)
extern "C"  void BitStream_Serializen_m1340978949 (BitStream_t1979465639 * __this, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	{
		NetworkViewID_t3942988548 * L_0 = ___viewID0;
		BitStream_INTERNAL_CALL_Serializen_m1742444286(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)
extern "C"  void BitStream_INTERNAL_CALL_Serializen_m1742444286 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___self0, NetworkViewID_t3942988548 * ___viewID1, const MethodInfo* method)
{
	typedef void (*BitStream_INTERNAL_CALL_Serializen_m1742444286_ftn) (BitStream_t1979465639 *, NetworkViewID_t3942988548 *);
	static BitStream_INTERNAL_CALL_Serializen_m1742444286_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_INTERNAL_CALL_Serializen_m1742444286_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)");
	_il2cpp_icall_func(___self0, ___viewID1);
}
// System.Void UnityEngine.BitStream::Serialize(System.Boolean&)
extern "C"  void BitStream_Serialize_m390089021 (BitStream_t1979465639 * __this, bool* ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	bool* G_B5_0 = NULL;
	bool* G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	bool* G_B6_1 = NULL;
	{
		bool* L_0 = ___value0;
		if (!(*((int8_t*)L_0)))
		{
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
	}

IL_000e:
	{
		V_0 = G_B3_0;
		BitStream_Serializeb_m2728727597(__this, (&V_0), /*hidden argument*/NULL);
		bool* L_1 = ___value0;
		int32_t L_2 = V_0;
		G_B4_0 = L_1;
		if (L_2)
		{
			G_B5_0 = L_1;
			goto IL_0024;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		goto IL_0025;
	}

IL_0024:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0025:
	{
		*((int8_t*)(G_B6_1)) = (int8_t)G_B6_0;
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Char&)
extern "C"  void BitStream_Serialize_m2742111985 (BitStream_t1979465639 * __this, Il2CppChar* ___value0, const MethodInfo* method)
{
	{
		Il2CppChar* L_0 = ___value0;
		BitStream_Serializec_m2418807390(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Int16&)
extern "C"  void BitStream_Serialize_m1625036513 (BitStream_t1979465639 * __this, int16_t* ___value0, const MethodInfo* method)
{
	{
		int16_t* L_0 = ___value0;
		BitStream_Serializes_m1379945554(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Int32&)
extern "C"  void BitStream_Serialize_m3312079847 (BitStream_t1979465639 * __this, int32_t* ___value0, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___value0;
		BitStream_Serializei_m1463666006(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Single&)
extern "C"  void BitStream_Serialize_m2064522995 (BitStream_t1979465639 * __this, float* ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		float* L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m622035106(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(System.Single&,System.Single)
extern "C"  void BitStream_Serialize_m622035106 (BitStream_t1979465639 * __this, float* ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		float* L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializef_m745934178(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&)
extern "C"  void BitStream_Serialize_m2762694425 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		Quaternion_t4030073918 * L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m3564943814(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&,System.Single)
extern "C"  void BitStream_Serialize_m3564943814 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		Quaternion_t4030073918 * L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializeq_m4825889(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&)
extern "C"  void BitStream_Serialize_m753399111 (BitStream_t1979465639 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0E-05f);
		Vector3_t2243707580 * L_0 = ___value0;
		float L_1 = V_0;
		BitStream_Serialize_m1992343662(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&,System.Single)
extern "C"  void BitStream_Serialize_m1992343662 (BitStream_t1979465639 * __this, Vector3_t2243707580 * ___value0, float ___maxDelta1, const MethodInfo* method)
{
	{
		Vector3_t2243707580 * L_0 = ___value0;
		float L_1 = ___maxDelta1;
		BitStream_Serializev_m2025246298(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.NetworkPlayer&)
extern "C"  void BitStream_Serialize_m2844992970 (BitStream_t1979465639 * __this, NetworkPlayer_t1243528291 * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		NetworkPlayer_t1243528291 * L_0 = ___value0;
		int32_t L_1 = L_0->get_index_0();
		V_0 = L_1;
		BitStream_Serializei_m1463666006(__this, (&V_0), /*hidden argument*/NULL);
		NetworkPlayer_t1243528291 * L_2 = ___value0;
		int32_t L_3 = V_0;
		L_2->set_index_0(L_3);
		return;
	}
}
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.NetworkViewID&)
extern "C"  void BitStream_Serialize_m2826028759 (BitStream_t1979465639 * __this, NetworkViewID_t3942988548 * ___viewID0, const MethodInfo* method)
{
	{
		NetworkViewID_t3942988548 * L_0 = ___viewID0;
		BitStream_Serializen_m1340978949(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.BitStream::get_isReading()
extern "C"  bool BitStream_get_isReading_m1625418235 (BitStream_t1979465639 * __this, const MethodInfo* method)
{
	typedef bool (*BitStream_get_isReading_m1625418235_ftn) (BitStream_t1979465639 *);
	static BitStream_get_isReading_m1625418235_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_get_isReading_m1625418235_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::get_isReading()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.BitStream::get_isWriting()
extern "C"  bool BitStream_get_isWriting_m3519823449 (BitStream_t1979465639 * __this, const MethodInfo* method)
{
	typedef bool (*BitStream_get_isWriting_m3519823449_ftn) (BitStream_t1979465639 *);
	static BitStream_get_isWriting_m3519823449_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_get_isWriting_m3519823449_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::get_isWriting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.BitStream::Serialize(System.String&)
extern "C"  void BitStream_Serialize_m1778986188 (BitStream_t1979465639 * __this, String_t** ___value0, const MethodInfo* method)
{
	typedef void (*BitStream_Serialize_m1778986188_ftn) (BitStream_t1979465639 *, String_t**);
	static BitStream_Serialize_m1778986188_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BitStream_Serialize_m1778986188_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BitStream::Serialize(System.String&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m1202659404 (Bounds_t3033363703 * __this, Vector3_t2243707580  ___center0, Vector3_t2243707580  ___size1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___center0;
		__this->set_m_Center_0(L_0);
		Vector3_t2243707580  L_1 = ___size1;
		Vector3_t2243707580  L_2 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_2);
		return;
	}
}
extern "C"  void Bounds__ctor_m1202659404_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___center0, Vector3_t2243707580  ___size1, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	Bounds__ctor_m1202659404(_thisAdjusted, ___center0, ___size1, method);
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m4284443179 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t2243707580  L_0 = Bounds_get_center_m129401026(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m1754570744((&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Bounds_get_extents_m4077324178(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m1754570744((&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
extern "C"  int32_t Bounds_GetHashCode_m4284443179_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_GetHashCode_m4284443179(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern Il2CppClass* Bounds_t3033363703_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Bounds_Equals_m839506137_MetadataUsageId;
extern "C"  bool Bounds_Equals_m839506137 (Bounds_t3033363703 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Equals_m839506137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Bounds_t3033363703_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Bounds_t3033363703 *)((Bounds_t3033363703 *)UnBox (L_1, Bounds_t3033363703_il2cpp_TypeInfo_var))));
		Vector3_t2243707580  L_2 = Bounds_get_center_m129401026(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t2243707580  L_3 = Bounds_get_center_m129401026((&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = L_3;
		Il2CppObject * L_5 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m2692262876((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		Vector3_t2243707580  L_7 = Bounds_get_extents_m4077324178(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t2243707580  L_8 = Bounds_get_extents_m4077324178((&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m2692262876((&V_2), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool Bounds_Equals_m839506137_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_Equals_m839506137(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t2243707580  Bounds_get_center_m129401026 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Center_0();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Bounds_get_center_m129401026_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_get_center_m129401026(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m2069004927 (Bounds_t3033363703 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Center_0(L_0);
		return;
	}
}
extern "C"  void Bounds_set_center_m2069004927_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	Bounds_set_center_m2069004927(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t2243707580  Bounds_get_size_m1728027642 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Extents_1();
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  Vector3_t2243707580  Bounds_get_size_m1728027642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_get_size_m1728027642(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m3943815629 (Bounds_t3033363703 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		Vector3_t2243707580  L_1 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_1);
		return;
	}
}
extern "C"  void Bounds_set_size_m3943815629_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	Bounds_set_size_m3943815629(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t2243707580  Bounds_get_extents_m4077324178 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Extents_1();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  Bounds_get_extents_m4077324178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_get_extents_m4077324178(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m2829577033 (Bounds_t3033363703 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_m_Extents_1(L_0);
		return;
	}
}
extern "C"  void Bounds_set_extents_m2829577033_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	Bounds_set_extents_m2829577033(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t2243707580  Bounds_get_min_m2405290441 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Bounds_get_center_m129401026(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Bounds_get_extents_m4077324178(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector3_t2243707580  Bounds_get_min_m2405290441_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_get_min_m2405290441(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t2243707580  Bounds_get_max_m4247050707 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Bounds_get_center_m129401026(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Bounds_get_extents_m4077324178(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  Vector3_t2243707580  Bounds_get_max_m4247050707_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_get_max_m4247050707(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1523110422 (Bounds_t3033363703 * __this, Vector3_t2243707580  ___min0, Vector3_t2243707580  ___max1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___max1;
		Vector3_t2243707580  L_1 = ___min0;
		Vector3_t2243707580  L_2 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m2829577033(__this, L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = ___min0;
		Vector3_t2243707580  L_5 = Bounds_get_extents_m4077324178(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m2069004927(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_SetMinMax_m1523110422_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___min0, Vector3_t2243707580  ___max1, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	Bounds_SetMinMax_m1523110422(_thisAdjusted, ___min0, ___max1, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3688171368 (Bounds_t3033363703 * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Bounds_get_min_m2405290441(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = ___point0;
		Vector3_t2243707580  L_2 = Vector3_Min_m4249067335(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Bounds_get_max_m4247050707(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = ___point0;
		Vector3_t2243707580  L_5 = Vector3_Max_m2105825185(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m1523110422(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m3688171368_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___point0, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	Bounds_Encapsulate_m3688171368(_thisAdjusted, ___point0, method);
}
// System.String UnityEngine.Bounds::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3664271751;
extern const uint32_t Bounds_ToString_m1966597703_MetadataUsageId;
extern "C"  String_t* Bounds_ToString_m1966597703 (Bounds_t3033363703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bounds_ToString_m1966597703_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t2243707580  L_1 = __this->get_m_Center_0();
		Vector3_t2243707580  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		Vector3_t2243707580  L_5 = __this->get_m_Extents_1();
		Vector3_t2243707580  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3664271751, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
extern "C"  String_t* Bounds_ToString_m1966597703_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t3033363703 * _thisAdjusted = reinterpret_cast<Bounds_t3033363703 *>(__this + 1);
	return Bounds_ToString_m1966597703(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m2161830280 (Il2CppObject * __this /* static, unused */, Bounds_t3033363703  ___lhs0, Bounds_t3033363703  ___rhs1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector3_t2243707580  L_0 = Bounds_get_center_m129401026((&___lhs0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Bounds_get_center_m129401026((&___rhs1), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t2243707580  L_3 = Bounds_get_extents_m4077324178((&___lhs0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Bounds_get_extents_m4077324178((&___rhs1), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m2315096783 (Il2CppObject * __this /* static, unused */, Bounds_t3033363703  ___lhs0, Bounds_t3033363703  ___rhs1, const MethodInfo* method)
{
	{
		Bounds_t3033363703  L_0 = ___lhs0;
		Bounds_t3033363703  L_1 = ___rhs1;
		bool L_2 = Bounds_op_Equality_m2161830280(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t3033363703_marshal_pinvoke(const Bounds_t3033363703& unmarshaled, Bounds_t3033363703_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Center_0(), marshaled.___m_Center_0);
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Extents_1(), marshaled.___m_Extents_1);
}
extern "C" void Bounds_t3033363703_marshal_pinvoke_back(const Bounds_t3033363703_marshaled_pinvoke& marshaled, Bounds_t3033363703& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Center_temp_0;
	memset(&unmarshaled_m_Center_temp_0, 0, sizeof(unmarshaled_m_Center_temp_0));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Center_0, unmarshaled_m_Center_temp_0);
	unmarshaled.set_m_Center_0(unmarshaled_m_Center_temp_0);
	Vector3_t2243707580  unmarshaled_m_Extents_temp_1;
	memset(&unmarshaled_m_Extents_temp_1, 0, sizeof(unmarshaled_m_Extents_temp_1));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Extents_1, unmarshaled_m_Extents_temp_1);
	unmarshaled.set_m_Extents_1(unmarshaled_m_Extents_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t3033363703_marshal_pinvoke_cleanup(Bounds_t3033363703_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Center_0);
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Extents_1);
}
// Conversion methods for marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t3033363703_marshal_com(const Bounds_t3033363703& unmarshaled, Bounds_t3033363703_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Center_0(), marshaled.___m_Center_0);
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Extents_1(), marshaled.___m_Extents_1);
}
extern "C" void Bounds_t3033363703_marshal_com_back(const Bounds_t3033363703_marshaled_com& marshaled, Bounds_t3033363703& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Center_temp_0;
	memset(&unmarshaled_m_Center_temp_0, 0, sizeof(unmarshaled_m_Center_temp_0));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Center_0, unmarshaled_m_Center_temp_0);
	unmarshaled.set_m_Center_0(unmarshaled_m_Center_temp_0);
	Vector3_t2243707580  unmarshaled_m_Extents_temp_1;
	memset(&unmarshaled_m_Extents_temp_1, 0, sizeof(unmarshaled_m_Extents_temp_1));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Extents_1, unmarshaled_m_Extents_temp_1);
	unmarshaled.set_m_Extents_1(unmarshaled_m_Extents_temp_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Bounds
extern "C" void Bounds_t3033363703_marshal_com_cleanup(Bounds_t3033363703_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Center_0);
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Extents_1);
}
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m3384007405 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_fieldOfView_m3384007405_ftn) (Camera_t189460977 *);
	static Camera_get_fieldOfView_m3384007405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_fieldOfView_m3384007405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_fieldOfView()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m3974156396 (Camera_t189460977 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_fieldOfView_m3974156396_ftn) (Camera_t189460977 *, float);
	static Camera_set_fieldOfView_m3974156396_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_fieldOfView_m3974156396_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_fieldOfView(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m3536967407 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m3536967407_ftn) (Camera_t189460977 *);
	static Camera_get_nearClipPlane_m3536967407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m3536967407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C"  void Camera_set_nearClipPlane_m3510849382 (Camera_t189460977 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_nearClipPlane_m3510849382_ftn) (Camera_t189460977 *, float);
	static Camera_set_nearClipPlane_m3510849382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_nearClipPlane_m3510849382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_nearClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m3137713566 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m3137713566_ftn) (Camera_t189460977 *);
	static Camera_get_farClipPlane_m3137713566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m3137713566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C"  void Camera_set_farClipPlane_m1845065941 (Camera_t189460977 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_farClipPlane_m1845065941_ftn) (Camera_t189460977 *, float);
	static Camera_set_farClipPlane_m1845065941_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_farClipPlane_m1845065941_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_farClipPlane(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m1801974358 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_orthographicSize_m1801974358_ftn) (Camera_t189460977 *);
	static Camera_get_orthographicSize_m1801974358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_orthographicSize_m1801974358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_orthographicSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m2708824189 (Camera_t189460977 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographicSize_m2708824189_ftn) (Camera_t189460977 *, float);
	static Camera_set_orthographicSize_m2708824189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m2708824189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C"  float Camera_get_depth_m1329692468 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_depth_m1329692468_ftn) (Camera_t189460977 *);
	static Camera_get_depth_m1329692468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m1329692468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m935361871 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_aspect_m935361871_ftn) (Camera_t189460977 *);
	static Camera_get_aspect_m935361871_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_aspect_m935361871_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_aspect()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_aspect(System.Single)
extern "C"  void Camera_set_aspect_m792328692 (Camera_t189460977 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_aspect_m792328692_ftn) (Camera_t189460977 *, float);
	static Camera_set_aspect_m792328692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_aspect_m792328692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_aspect(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m73686965 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m73686965_ftn) (Camera_t189460977 *);
	static Camera_get_cullingMask_m73686965_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m73686965_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m2396665826 (Camera_t189460977 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_cullingMask_m2396665826_ftn) (Camera_t189460977 *, int32_t);
	static Camera_set_cullingMask_m2396665826_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_cullingMask_m2396665826_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_cullingMask(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m4241372419 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m4241372419_ftn) (Camera_t189460977 *);
	static Camera_get_eventMask_m4241372419_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m4241372419_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern "C"  Color_t2020392075  Camera_get_backgroundColor_m2767968515 (Camera_t189460977 * __this, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_get_backgroundColor_m2704073336(__this, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C"  void Camera_set_backgroundColor_m2927893592 (Camera_t189460977 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_backgroundColor_m3660646068(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)
extern "C"  void Camera_INTERNAL_get_backgroundColor_m2704073336 (Camera_t189460977 * __this, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_backgroundColor_m2704073336_ftn) (Camera_t189460977 *, Color_t2020392075 *);
	static Camera_INTERNAL_get_backgroundColor_m2704073336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_backgroundColor_m2704073336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
extern "C"  void Camera_INTERNAL_set_backgroundColor_m3660646068 (Camera_t189460977 * __this, Color_t2020392075 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_backgroundColor_m3660646068_ftn) (Camera_t189460977 *, Color_t2020392075 *);
	static Camera_INTERNAL_set_backgroundColor_m3660646068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_backgroundColor_m3660646068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C"  Rect_t3681755626  Camera_get_rect_m2873096661 (Camera_t189460977 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_get_rect_m3971999434(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C"  void Camera_set_rect_m1838810502 (Camera_t189460977 * __this, Rect_t3681755626  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_rect_m610424166(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_rect_m3971999434 (Camera_t189460977 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_rect_m3971999434_ftn) (Camera_t189460977 *, Rect_t3681755626 *);
	static Camera_INTERNAL_get_rect_m3971999434_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_rect_m3971999434_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_set_rect_m610424166 (Camera_t189460977 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_rect_m610424166_ftn) (Camera_t189460977 *, Rect_t3681755626 *);
	static Camera_INTERNAL_set_rect_m610424166_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_rect_m610424166_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t3681755626  Camera_get_pixelRect_m2084185953 (Camera_t189460977 * __this, const MethodInfo* method)
{
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_get_pixelRect_m1785951490(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t3681755626  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_pixelRect(UnityEngine.Rect)
extern "C"  void Camera_set_pixelRect_m1366013782 (Camera_t189460977 * __this, Rect_t3681755626  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_pixelRect_m3301406310(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m1785951490 (Camera_t189460977 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m1785951490_ftn) (Camera_t189460977 *, Rect_t3681755626 *);
	static Camera_INTERNAL_get_pixelRect_m1785951490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m1785951490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_set_pixelRect_m3301406310 (Camera_t189460977 * __this, Rect_t3681755626 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_pixelRect_m3301406310_ftn) (Camera_t189460977 *, Rect_t3681755626 *);
	static Camera_INTERNAL_set_pixelRect_m3301406310_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_pixelRect_m3301406310_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2666733923 * Camera_get_targetTexture_m705925974 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t2666733923 * (*Camera_get_targetTexture_m705925974_ftn) (Camera_t189460977 *);
	static Camera_get_targetTexture_m705925974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m705925974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m1743144302 (Camera_t189460977 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m1743144302_ftn) (Camera_t189460977 *);
	static Camera_get_clearFlags_m1743144302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m1743144302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_WorldToScreenPoint_m638747266 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_WorldToScreenPoint_m720233894(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToScreenPoint_m720233894 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Vector3_t2243707580 * ___position1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_WorldToScreenPoint_m720233894_ftn) (Camera_t189460977 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Camera_INTERNAL_CALL_WorldToScreenPoint_m720233894_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToScreenPoint_m720233894_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_ScreenToWorldPoint_m929392728 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_ScreenToWorldPoint_m196899300(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m196899300 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Vector3_t2243707580 * ___position1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m196899300_ftn) (Camera_t189460977 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m196899300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m196899300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_ScreenToViewportPoint_m2666228286 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_ScreenToViewportPoint_m529506484(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2243707580  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m529506484 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Vector3_t2243707580 * ___position1, Vector3_t2243707580 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m529506484_ftn) (Camera_t189460977 *, Vector3_t2243707580 *, Vector3_t2243707580 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m529506484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m529506484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t2469606224  Camera_ScreenPointToRay_m614889538 (Camera_t189460977 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Ray_t2469606224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t2469606224  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Vector3_t2243707580 * ___position1, Ray_t2469606224 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646_ftn) (Camera_t189460977 *, Vector3_t2243707580 *, Ray_t2469606224 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m2752248646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t189460977 * (*Camera_get_main_m475173995_ftn) ();
	static Camera_get_main_m475173995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m475173995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m989474043 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m989474043_ftn) ();
	static Camera_get_allCamerasCount_m989474043_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m989474043_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m2922515227 (Il2CppObject * __this /* static, unused */, CameraU5BU5D_t3079764780* ___cameras0, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m2922515227_ftn) (CameraU5BU5D_t3079764780*);
	static Camera_GetAllCameras_m2922515227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m2922515227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras0);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m1679634170_MetadataUsageId;
extern "C"  void Camera_FireOnPreCull_m1679634170 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m1679634170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t834278767 * L_0 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		Camera_t189460977 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3079065225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreRender_m24116662_MetadataUsageId;
extern "C"  void Camera_FireOnPreRender_m24116662 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m24116662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t834278767 * L_0 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		Camera_t189460977 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3079065225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPostRender_m94860165_MetadataUsageId;
extern "C"  void Camera_FireOnPostRender_m94860165 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m94860165_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t834278767 * L_0 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t834278767 * L_1 = ((Camera_t189460977_StaticFields*)Camera_t189460977_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		Camera_t189460977 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m3079065225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * Camera_RaycastTry_m3412198936 (Camera_t189460977 * __this, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		int32_t L_2 = V_0;
		GameObject_t1756533147 * L_3 = Camera_INTERNAL_CALL_RaycastTry_m2295752796(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  GameObject_t1756533147 * Camera_INTERNAL_CALL_RaycastTry_m2295752796 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Ray_t2469606224 * ___ray1, float ___distance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*Camera_INTERNAL_CALL_RaycastTry_m2295752796_ftn) (Camera_t189460977 *, Ray_t2469606224 *, float, int32_t, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m2295752796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m2295752796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3, ___queryTriggerInteraction4);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * Camera_RaycastTry2D_m755036866 (Camera_t189460977 * __this, Ray_t2469606224  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t1756533147 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m1020711785(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1756533147 * Camera_INTERNAL_CALL_RaycastTry2D_m1020711785 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___self0, Ray_t2469606224 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*Camera_INTERNAL_CALL_RaycastTry2D_m1020711785_ftn) (Camera_t189460977 *, Ray_t2469606224 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m1020711785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m1020711785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m2929748071 (CameraCallback_t834278767 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m3079065225 (CameraCallback_t834278767 * __this, Camera_t189460977 * ___cam0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m3079065225((CameraCallback_t834278767 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Camera_t189460977 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t189460977 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CameraCallback_BeginInvoke_m144217562 (CameraCallback_t834278767 * __this, Camera_t189460977 * ___cam0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m2103705933 (CameraCallback_t834278767 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern Il2CppClass* Canvas_t209405766_il2cpp_TypeInfo_var;
extern Il2CppClass* WillRenderCanvases_t3522132132_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_add_willRenderCanvases_m3467125643_MetadataUsageId;
extern "C"  void Canvas_add_willRenderCanvases_m3467125643 (Il2CppObject * __this /* static, unused */, WillRenderCanvases_t3522132132 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Canvas_add_willRenderCanvases_m3467125643_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t3522132132 * L_0 = ((Canvas_t209405766_StaticFields*)Canvas_t209405766_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		WillRenderCanvases_t3522132132 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t209405766_StaticFields*)Canvas_t209405766_il2cpp_TypeInfo_var->static_fields)->set_willRenderCanvases_2(((WillRenderCanvases_t3522132132 *)CastclassSealed(L_2, WillRenderCanvases_t3522132132_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern Il2CppClass* Canvas_t209405766_il2cpp_TypeInfo_var;
extern Il2CppClass* WillRenderCanvases_t3522132132_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_remove_willRenderCanvases_m1070419298_MetadataUsageId;
extern "C"  void Canvas_remove_willRenderCanvases_m1070419298 (Il2CppObject * __this /* static, unused */, WillRenderCanvases_t3522132132 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Canvas_remove_willRenderCanvases_m1070419298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t3522132132 * L_0 = ((Canvas_t209405766_StaticFields*)Canvas_t209405766_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		WillRenderCanvases_t3522132132 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t209405766_StaticFields*)Canvas_t209405766_il2cpp_TypeInfo_var->static_fields)->set_willRenderCanvases_2(((WillRenderCanvases_t3522132132 *)CastclassSealed(L_2, WillRenderCanvases_t3522132132_il2cpp_TypeInfo_var)));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C"  int32_t Canvas_get_renderMode_m1816014618 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m1816014618_ftn) (Canvas_t209405766 *);
	static Canvas_get_renderMode_m1816014618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m1816014618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C"  bool Canvas_get_isRootCanvas_m2407426044 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m2407426044_ftn) (Canvas_t209405766 *);
	static Canvas_get_isRootCanvas_m2407426044_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m2407426044_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C"  Camera_t189460977 * Canvas_get_worldCamera_m1187332710 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef Camera_t189460977 * (*Canvas_get_worldCamera_m1187332710_ftn) (Canvas_t209405766 *);
	static Canvas_get_worldCamera_m1187332710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m1187332710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C"  float Canvas_get_scaleFactor_m1115379735 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m1115379735_ftn) (Canvas_t209405766 *);
	static Canvas_get_scaleFactor_m1115379735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m1115379735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C"  void Canvas_set_scaleFactor_m298557412 (Canvas_t209405766 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m298557412_ftn) (Canvas_t209405766 *, float);
	static Canvas_set_scaleFactor_m298557412_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m298557412_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C"  float Canvas_get_referencePixelsPerUnit_m1520474171 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m1520474171_ftn) (Canvas_t209405766 *);
	static Canvas_get_referencePixelsPerUnit_m1520474171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m1520474171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C"  void Canvas_set_referencePixelsPerUnit_m1969549562 (Canvas_t209405766 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m1969549562_ftn) (Canvas_t209405766 *, float);
	static Canvas_set_referencePixelsPerUnit_m1969549562_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m1969549562_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C"  bool Canvas_get_pixelPerfect_m730801767 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m730801767_ftn) (Canvas_t209405766 *);
	static Canvas_get_pixelPerfect_m730801767_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m730801767_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C"  int32_t Canvas_get_renderOrder_m2874842494 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m2874842494_ftn) (Canvas_t209405766 *);
	static Canvas_get_renderOrder_m2874842494_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m2874842494_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_overrideSorting()
extern "C"  bool Canvas_get_overrideSorting_m3223770298 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_overrideSorting_m3223770298_ftn) (Canvas_t209405766 *);
	static Canvas_get_overrideSorting_m3223770298_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_overrideSorting_m3223770298_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_overrideSorting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
extern "C"  void Canvas_set_overrideSorting_m3982973573 (Canvas_t209405766 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_overrideSorting_m3982973573_ftn) (Canvas_t209405766 *, bool);
	static Canvas_set_overrideSorting_m3982973573_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_overrideSorting_m3982973573_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_overrideSorting(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C"  int32_t Canvas_get_sortingOrder_m3120854436 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m3120854436_ftn) (Canvas_t209405766 *);
	static Canvas_get_sortingOrder_m3120854436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m3120854436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
extern "C"  void Canvas_set_sortingOrder_m2922819993 (Canvas_t209405766 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingOrder_m2922819993_ftn) (Canvas_t209405766 *, int32_t);
	static Canvas_set_sortingOrder_m2922819993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingOrder_m2922819993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C"  int32_t Canvas_get_sortingLayerID_m1396307660 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m1396307660_ftn) (Canvas_t209405766 *);
	static Canvas_get_sortingLayerID_m1396307660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m1396307660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
extern "C"  void Canvas_set_sortingLayerID_m537411565 (Canvas_t209405766 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingLayerID_m537411565_ftn) (Canvas_t209405766 *, int32_t);
	static Canvas_set_sortingLayerID_m537411565_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingLayerID_m537411565_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
extern "C"  Canvas_t209405766 * Canvas_get_rootCanvas_m1790974328 (Canvas_t209405766 * __this, const MethodInfo* method)
{
	typedef Canvas_t209405766 * (*Canvas_get_rootCanvas_m1790974328_ftn) (Canvas_t209405766 *);
	static Canvas_get_rootCanvas_m1790974328_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_rootCanvas_m1790974328_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_rootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C"  Material_t193706927 * Canvas_GetDefaultCanvasMaterial_m1290649642 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Canvas_GetDefaultCanvasMaterial_m1290649642_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m1290649642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m1290649642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
extern "C"  Material_t193706927 * Canvas_GetETC1SupportedCanvasMaterial_m1927784046 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t193706927 * (*Canvas_GetETC1SupportedCanvasMaterial_m1927784046_ftn) ();
	static Canvas_GetETC1SupportedCanvasMaterial_m1927784046_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetETC1SupportedCanvasMaterial_m1927784046_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern Il2CppClass* Canvas_t209405766_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_SendWillRenderCanvases_m3796535067_MetadataUsageId;
extern "C"  void Canvas_SendWillRenderCanvases_m3796535067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Canvas_SendWillRenderCanvases_m3796535067_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t3522132132 * L_0 = ((Canvas_t209405766_StaticFields*)Canvas_t209405766_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t3522132132 * L_1 = ((Canvas_t209405766_StaticFields*)Canvas_t209405766_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m472872824(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C"  void Canvas_ForceUpdateCanvases_m2446828475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m3796535067(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C"  void WillRenderCanvases__ctor_m1996025778 (WillRenderCanvases_t3522132132 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C"  void WillRenderCanvases_Invoke_m472872824 (WillRenderCanvases_t3522132132 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WillRenderCanvases_Invoke_m472872824((WillRenderCanvases_t3522132132 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_WillRenderCanvases_t3522132132 (WillRenderCanvases_t3522132132 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WillRenderCanvases_BeginInvoke_m1914028027 (WillRenderCanvases_t3522132132 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C"  void WillRenderCanvases_EndInvoke_m3215676440 (WillRenderCanvases_t3522132132 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C"  float CanvasGroup_get_alpha_m1304564441 (CanvasGroup_t3296560743 * __this, const MethodInfo* method)
{
	typedef float (*CanvasGroup_get_alpha_m1304564441_ftn) (CanvasGroup_t3296560743 *);
	static CanvasGroup_get_alpha_m1304564441_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_alpha_m1304564441_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_alpha()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C"  void CanvasGroup_set_alpha_m3292984624 (CanvasGroup_t3296560743 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_alpha_m3292984624_ftn) (CanvasGroup_t3296560743 *, float);
	static CanvasGroup_set_alpha_m3292984624_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m3292984624_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C"  bool CanvasGroup_get_interactable_m3354621007 (CanvasGroup_t3296560743 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m3354621007_ftn) (CanvasGroup_t3296560743 *);
	static CanvasGroup_get_interactable_m3354621007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m3354621007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_interactable(System.Boolean)
extern "C"  void CanvasGroup_set_interactable_m2046144056 (CanvasGroup_t3296560743 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_interactable_m2046144056_ftn) (CanvasGroup_t3296560743 *, bool);
	static CanvasGroup_set_interactable_m2046144056_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_interactable_m2046144056_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_interactable(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C"  bool CanvasGroup_get_blocksRaycasts_m3945388797 (CanvasGroup_t3296560743 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m3945388797_ftn) (CanvasGroup_t3296560743 *);
	static CanvasGroup_get_blocksRaycasts_m3945388797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m3945388797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
extern "C"  void CanvasGroup_set_blocksRaycasts_m3812230476 (CanvasGroup_t3296560743 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_blocksRaycasts_m3812230476_ftn) (CanvasGroup_t3296560743 *, bool);
	static CanvasGroup_set_blocksRaycasts_m3812230476_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_blocksRaycasts_m3812230476_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C"  bool CanvasGroup_get_ignoreParentGroups_m534041855 (CanvasGroup_t3296560743 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m534041855_ftn) (CanvasGroup_t3296560743 *);
	static CanvasGroup_get_ignoreParentGroups_m534041855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m534041855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_ignoreParentGroups(System.Boolean)
extern "C"  void CanvasGroup_set_ignoreParentGroups_m1962090536 (CanvasGroup_t3296560743 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_ignoreParentGroups_m1962090536_ftn) (CanvasGroup_t3296560743 *, bool);
	static CanvasGroup_set_ignoreParentGroups_m1962090536_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_ignoreParentGroups_m1962090536_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_ignoreParentGroups(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool CanvasGroup_IsRaycastLocationValid_m1999696282 (CanvasGroup_t3296560743 * __this, Vector2_t2243707579  ___sp0, Camera_t189460977 * ___eventCamera1, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m3945388797(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C"  void CanvasRenderer_SetColor_m1241254365 (CanvasRenderer_t261436805 * __this, Color_t2020392075  ___color0, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m3342810068(NULL /*static, unused*/, __this, (&___color0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_SetColor_m3342810068 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t261436805 * ___self0, Color_t2020392075 * ___color1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m3342810068_ftn) (CanvasRenderer_t261436805 *, Color_t2020392075 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m3342810068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m3342810068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___color1);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C"  Color_t2020392075  CanvasRenderer_GetColor_m3395389094 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CanvasRenderer_INTERNAL_CALL_GetColor_m132373064(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Color_t2020392075  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_GetColor_m132373064 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t261436805 * ___self0, Color_t2020392075 * ___value1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_GetColor_m132373064_ftn) (CanvasRenderer_t261436805 *, Color_t2020392075 *);
	static CanvasRenderer_INTERNAL_CALL_GetColor_m132373064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_GetColor_m132373064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___value1);
}
// System.Void UnityEngine.CanvasRenderer::EnableRectClipping(UnityEngine.Rect)
extern "C"  void CanvasRenderer_EnableRectClipping_m478557626 (CanvasRenderer_t261436805 * __this, Rect_t3681755626  ___rect0, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m2214772297(NULL /*static, unused*/, __this, (&___rect0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m2214772297 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t261436805 * ___self0, Rect_t3681755626 * ___rect1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m2214772297_ftn) (CanvasRenderer_t261436805 *, Rect_t3681755626 *);
	static CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m2214772297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m2214772297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1);
}
// System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
extern "C"  void CanvasRenderer_DisableRectClipping_m2720508088 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_DisableRectClipping_m2720508088_ftn) (CanvasRenderer_t261436805 *);
	static CanvasRenderer_DisableRectClipping_m2720508088_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_DisableRectClipping_m2720508088_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::DisableRectClipping()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
extern "C"  void CanvasRenderer_set_hasPopInstruction_m1388844875 (CanvasRenderer_t261436805 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_hasPopInstruction_m1388844875_ftn) (CanvasRenderer_t261436805 *, bool);
	static CanvasRenderer_set_hasPopInstruction_m1388844875_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_hasPopInstruction_m1388844875_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C"  int32_t CanvasRenderer_get_materialCount_m2862217439 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_materialCount_m2862217439_ftn) (CanvasRenderer_t261436805 *);
	static CanvasRenderer_get_materialCount_m2862217439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_materialCount_m2862217439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_materialCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C"  void CanvasRenderer_set_materialCount_m803316782 (CanvasRenderer_t261436805 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_materialCount_m803316782_ftn) (CanvasRenderer_t261436805 *, int32_t);
	static CanvasRenderer_set_materialCount_m803316782_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_materialCount_m803316782_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_materialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetMaterial_m1462355522 (CanvasRenderer_t261436805 * __this, Material_t193706927 * ___material0, int32_t ___index1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m1462355522_ftn) (CanvasRenderer_t261436805 *, Material_t193706927 *, int32_t);
	static CanvasRenderer_SetMaterial_m1462355522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m1462355522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetMaterial_m1741420785 (CanvasRenderer_t261436805 * __this, Material_t193706927 * ___material0, Texture_t2243626319 * ___texture1, const MethodInfo* method)
{
	{
		int32_t L_0 = CanvasRenderer_get_materialCount_m2862217439(__this, /*hidden argument*/NULL);
		int32_t L_1 = Math_Max_m2671311541(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		CanvasRenderer_set_materialCount_m803316782(__this, L_1, /*hidden argument*/NULL);
		Material_t193706927 * L_2 = ___material0;
		CanvasRenderer_SetMaterial_m1462355522(__this, L_2, 0, /*hidden argument*/NULL);
		Texture_t2243626319 * L_3 = ___texture1;
		CanvasRenderer_SetTexture_m2394798173(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
extern "C"  void CanvasRenderer_set_popMaterialCount_m3394823403 (CanvasRenderer_t261436805 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_popMaterialCount_m3394823403_ftn) (CanvasRenderer_t261436805 *, int32_t);
	static CanvasRenderer_set_popMaterialCount_m3394823403_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_popMaterialCount_m3394823403_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetPopMaterial_m3522214039 (CanvasRenderer_t261436805 * __this, Material_t193706927 * ___material0, int32_t ___index1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetPopMaterial_m3522214039_ftn) (CanvasRenderer_t261436805 *, Material_t193706927 *, int32_t);
	static CanvasRenderer_SetPopMaterial_m3522214039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetPopMaterial_m3522214039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetTexture_m2394798173 (CanvasRenderer_t261436805 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetTexture_m2394798173_ftn) (CanvasRenderer_t261436805 *, Texture_t2243626319 *);
	static CanvasRenderer_SetTexture_m2394798173_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetTexture_m2394798173_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetAlphaTexture_m3093886085 (CanvasRenderer_t261436805 * __this, Texture_t2243626319 * ___texture0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetAlphaTexture_m3093886085_ftn) (CanvasRenderer_t261436805 *, Texture_t2243626319 *);
	static CanvasRenderer_SetAlphaTexture_m3093886085_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetAlphaTexture_m3093886085_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C"  void CanvasRenderer_SetMesh_m2850571117 (CanvasRenderer_t261436805 * __this, Mesh_t1356156583 * ___mesh0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMesh_m2850571117_ftn) (CanvasRenderer_t261436805 *, Mesh_t1356156583 *);
	static CanvasRenderer_SetMesh_m2850571117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMesh_m2850571117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh0);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C"  void CanvasRenderer_Clear_m2419751129 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m2419751129_ftn) (CanvasRenderer_t261436805 *);
	static CanvasRenderer_Clear_m2419751129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m2419751129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreams(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_SplitUIVertexStreams_m2145837300 (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___verts0, List_1_t1612828712 * ___positions1, List_1_t243638650 * ___colors2, List_1_t1612828711 * ___uv0S3, List_1_t1612828711 * ___uv1S4, List_1_t1612828712 * ___normals5, List_1_t1612828713 * ___tangents6, List_1_t1440998580 * ___indicies7, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___verts0;
		List_1_t1612828712 * L_1 = ___positions1;
		List_1_t243638650 * L_2 = ___colors2;
		List_1_t1612828711 * L_3 = ___uv0S3;
		List_1_t1612828711 * L_4 = ___uv1S4;
		List_1_t1612828712 * L_5 = ___normals5;
		List_1_t1612828713 * L_6 = ___tangents6;
		CanvasRenderer_SplitUIVertexStreamsInternal_m729405782(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		List_1_t573379950 * L_7 = ___verts0;
		List_1_t1440998580 * L_8 = ___indicies7;
		CanvasRenderer_SplitIndiciesStreamsInternal_m3005676420(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitUIVertexStreamsInternal_m729405782 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitUIVertexStreamsInternal_m729405782_ftn) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_SplitUIVertexStreamsInternal_m729405782_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitUIVertexStreamsInternal_m729405782_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___normals5, ___tangents6);
}
// System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitIndiciesStreamsInternal_m3005676420 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___indicies1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitIndiciesStreamsInternal_m3005676420_ftn) (Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_SplitIndiciesStreamsInternal_m3005676420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitIndiciesStreamsInternal_m3005676420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___indicies1);
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_CreateUIVertexStream_m197449703 (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___verts0, List_1_t1612828712 * ___positions1, List_1_t243638650 * ___colors2, List_1_t1612828711 * ___uv0S3, List_1_t1612828711 * ___uv1S4, List_1_t1612828712 * ___normals5, List_1_t1612828713 * ___tangents6, List_1_t1440998580 * ___indicies7, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___verts0;
		List_1_t1612828712 * L_1 = ___positions1;
		List_1_t243638650 * L_2 = ___colors2;
		List_1_t1612828711 * L_3 = ___uv0S3;
		List_1_t1612828711 * L_4 = ___uv1S4;
		List_1_t1612828712 * L_5 = ___normals5;
		List_1_t1612828713 * L_6 = ___tangents6;
		List_1_t1440998580 * L_7 = ___indicies7;
		CanvasRenderer_CreateUIVertexStreamInternal_m2886336395(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_CreateUIVertexStreamInternal_m2886336395 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, Il2CppObject * ___indicies7, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_CreateUIVertexStreamInternal_m2886336395_ftn) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_CreateUIVertexStreamInternal_m2886336395_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_CreateUIVertexStreamInternal_m2886336395_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___normals5, ___tangents6, ___indicies7);
}
// System.Void UnityEngine.CanvasRenderer::AddUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void CanvasRenderer_AddUIVertexStream_m1334037553 (Il2CppObject * __this /* static, unused */, List_1_t573379950 * ___verts0, List_1_t1612828712 * ___positions1, List_1_t243638650 * ___colors2, List_1_t1612828711 * ___uv0S3, List_1_t1612828711 * ___uv1S4, List_1_t1612828712 * ___normals5, List_1_t1612828713 * ___tangents6, const MethodInfo* method)
{
	{
		List_1_t573379950 * L_0 = ___verts0;
		List_1_t1612828712 * L_1 = ___positions1;
		List_1_t243638650 * L_2 = ___colors2;
		List_1_t1612828711 * L_3 = ___uv0S3;
		List_1_t1612828711 * L_4 = ___uv1S4;
		List_1_t1612828712 * L_5 = ___normals5;
		List_1_t1612828713 * L_6 = ___tangents6;
		CanvasRenderer_SplitUIVertexStreamsInternal_m729405782(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.CanvasRenderer::get_cull()
extern "C"  bool CanvasRenderer_get_cull_m3577089379 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_cull_m3577089379_ftn) (CanvasRenderer_t261436805 *);
	static CanvasRenderer_get_cull_m3577089379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_cull_m3577089379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_cull()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
extern "C"  void CanvasRenderer_set_cull_m1437892490 (CanvasRenderer_t261436805 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_cull_m1437892490_ftn) (CanvasRenderer_t261436805 *, bool);
	static CanvasRenderer_set_cull_m1437892490_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_cull_m1437892490_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_cull(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C"  int32_t CanvasRenderer_get_absoluteDepth_m4243141 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m4243141_ftn) (CanvasRenderer_t261436805 *);
	static CanvasRenderer_get_absoluteDepth_m4243141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m4243141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
extern "C"  bool CanvasRenderer_get_hasMoved_m2428030996 (CanvasRenderer_t261436805 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_hasMoved_m2428030996_ftn) (CanvasRenderer_t261436805 *);
	static CanvasRenderer_get_hasMoved_m2428030996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_hasMoved_m2428030996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_hasMoved()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CharacterController::SimpleMove(UnityEngine.Vector3)
extern "C"  bool CharacterController_SimpleMove_m3872077378 (CharacterController_t4094781467 * __this, Vector3_t2243707580  ___speed0, const MethodInfo* method)
{
	{
		bool L_0 = CharacterController_INTERNAL_CALL_SimpleMove_m63258103(NULL /*static, unused*/, __this, (&___speed0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  bool CharacterController_INTERNAL_CALL_SimpleMove_m63258103 (Il2CppObject * __this /* static, unused */, CharacterController_t4094781467 * ___self0, Vector3_t2243707580 * ___speed1, const MethodInfo* method)
{
	typedef bool (*CharacterController_INTERNAL_CALL_SimpleMove_m63258103_ftn) (CharacterController_t4094781467 *, Vector3_t2243707580 *);
	static CharacterController_INTERNAL_CALL_SimpleMove_m63258103_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_CALL_SimpleMove_m63258103_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self0, ___speed1);
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m3456882757 (CharacterController_t4094781467 * __this, Vector3_t2243707580  ___motion0, const MethodInfo* method)
{
	{
		int32_t L_0 = CharacterController_INTERNAL_CALL_Move_m2826125634(NULL /*static, unused*/, __this, (&___motion0), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
extern "C"  int32_t CharacterController_INTERNAL_CALL_Move_m2826125634 (Il2CppObject * __this /* static, unused */, CharacterController_t4094781467 * ___self0, Vector3_t2243707580 * ___motion1, const MethodInfo* method)
{
	typedef int32_t (*CharacterController_INTERNAL_CALL_Move_m2826125634_ftn) (CharacterController_t4094781467 *, Vector3_t2243707580 *);
	static CharacterController_INTERNAL_CALL_Move_m2826125634_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_CALL_Move_m2826125634_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self0, ___motion1);
}
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m2594228107 (CharacterController_t4094781467 * __this, const MethodInfo* method)
{
	typedef bool (*CharacterController_get_isGrounded_m2594228107_ftn) (CharacterController_t4094781467 *);
	static CharacterController_get_isGrounded_m2594228107_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_get_isGrounded_m2594228107_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::get_isGrounded()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.CollisionFlags UnityEngine.CharacterController::get_collisionFlags()
extern "C"  int32_t CharacterController_get_collisionFlags_m3772321073 (CharacterController_t4094781467 * __this, const MethodInfo* method)
{
	typedef int32_t (*CharacterController_get_collisionFlags_m3772321073_ftn) (CharacterController_t4094781467 *);
	static CharacterController_get_collisionFlags_m3772321073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_get_collisionFlags_m3772321073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::get_collisionFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CharacterController::set_radius(System.Single)
extern "C"  void CharacterController_set_radius_m1695843322 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_radius_m1695843322_ftn) (CharacterController_t4094781467 *, float);
	static CharacterController_set_radius_m1695843322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_radius_m1695843322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_radius(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_height(System.Single)
extern "C"  void CharacterController_set_height_m2274211287 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_height_m2274211287_ftn) (CharacterController_t4094781467 *, float);
	static CharacterController_set_height_m2274211287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_height_m2274211287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_height(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_center(UnityEngine.Vector3)
extern "C"  void CharacterController_set_center_m634679819 (CharacterController_t4094781467 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		CharacterController_INTERNAL_set_center_m1598929593(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void CharacterController_INTERNAL_set_center_m1598929593 (CharacterController_t4094781467 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_INTERNAL_set_center_m1598929593_ftn) (CharacterController_t4094781467 *, Vector3_t2243707580 *);
	static CharacterController_INTERNAL_set_center_m1598929593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_INTERNAL_set_center_m1598929593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_slopeLimit(System.Single)
extern "C"  void CharacterController_set_slopeLimit_m1597209790 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_slopeLimit_m1597209790_ftn) (CharacterController_t4094781467 *, float);
	static CharacterController_set_slopeLimit_m1597209790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_slopeLimit_m1597209790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_slopeLimit(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_stepOffset(System.Single)
extern "C"  void CharacterController_set_stepOffset_m3474051307 (CharacterController_t4094781467 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_stepOffset_m3474051307_ftn) (CharacterController_t4094781467 *, float);
	static CharacterController_set_stepOffset_m3474051307_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_stepOffset_m3474051307_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_stepOffset(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CharacterController::set_detectCollisions(System.Boolean)
extern "C"  void CharacterController_set_detectCollisions_m1206451172 (CharacterController_t4094781467 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CharacterController_set_detectCollisions_m1206451172_ftn) (CharacterController_t4094781467 *, bool);
	static CharacterController_set_detectCollisions_m1206451172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CharacterController_set_detectCollisions_m1206451172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CharacterController::set_detectCollisions(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t4233889191 * Collider_get_attachedRigidbody_m3279305420 (Collider_t3497673348 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t4233889191 * (*Collider_get_attachedRigidbody_m3279305420_ftn) (Collider_t3497673348 *);
	static Collider_get_attachedRigidbody_m3279305420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m3279305420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.PhysicMaterial UnityEngine.Collider::get_material()
extern "C"  PhysicMaterial_t578636151 * Collider_get_material_m1079622242 (Collider_t3497673348 * __this, const MethodInfo* method)
{
	typedef PhysicMaterial_t578636151 * (*Collider_get_material_m1079622242_ftn) (Collider_t3497673348 *);
	static Collider_get_material_m1079622242_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_material_m1079622242_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C"  void Collider2D_set_isTrigger_m1243156937 (Collider2D_t646061738 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Collider2D_set_isTrigger_m1243156937_ftn) (Collider2D_t646061738 *, bool);
	static Collider2D_set_isTrigger_m1243156937_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_set_isTrigger_m1243156937_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::set_isTrigger(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t502193897 * Collider2D_get_attachedRigidbody_m1321121400 (Collider2D_t646061738 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t502193897 * (*Collider2D_get_attachedRigidbody_m1321121400_ftn) (Collider2D_t646061738 *);
	static Collider2D_get_attachedRigidbody_m1321121400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m1321121400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Collider2D::get_shapeCount()
extern "C"  int32_t Collider2D_get_shapeCount_m95259526 (Collider2D_t646061738 * __this, const MethodInfo* method)
{
	typedef int32_t (*Collider2D_get_shapeCount_m95259526_ftn) (Collider2D_t646061738 *);
	static Collider2D_get_shapeCount_m95259526_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_shapeCount_m95259526_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_shapeCount()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
extern "C"  PhysicsMaterial2D_t851691520 * Collider2D_get_sharedMaterial_m1412882174 (Collider2D_t646061738 * __this, const MethodInfo* method)
{
	typedef PhysicsMaterial2D_t851691520 * (*Collider2D_get_sharedMaterial_m1412882174_ftn) (Collider2D_t646061738 *);
	static Collider2D_get_sharedMaterial_m1412882174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_sharedMaterial_m1412882174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Collision::.ctor()
extern "C"  void Collision__ctor_m635081107 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C"  Vector3_t2243707580  Collision_get_relativeVelocity_m2302609283 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_RelativeVelocity_1();
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C"  Rigidbody_t4233889191 * Collision_get_rigidbody_m3671561778 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Rigidbody_t4233889191 * L_0 = __this->get_m_Rigidbody_2();
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C"  Collider_t3497673348 * Collision_get_collider_m3340328360 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_3();
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Collision::get_transform()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Collision_get_transform_m4132935520_MetadataUsageId;
extern "C"  Transform_t3275118058 * Collision_get_transform_m4132935520 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_transform_m4132935520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t3275118058 * G_B3_0 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = Collision_get_rigidbody_m3671561778(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody_t4233889191 * L_2 = Collision_get_rigidbody_m3671561778(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider_t3497673348 * L_4 = Collision_get_collider_m3340328360(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Collision_get_gameObject_m1370363400_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Collision_get_gameObject_m1370363400 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_gameObject_m1370363400_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * G_B3_0 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody_t4233889191 * L_2 = __this->get_m_Rigidbody_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider_t3497673348 * L_4 = __this->get_m_Collider_3();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern "C"  ContactPointU5BU5D_t1084937515* Collision_get_contacts_m266635379 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		ContactPointU5BU5D_t1084937515* L_0 = __this->get_m_Contacts_4();
		return L_0;
	}
}
// System.Collections.IEnumerator UnityEngine.Collision::GetEnumerator()
extern "C"  Il2CppObject * Collision_GetEnumerator_m4009889947 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		ContactPointU5BU5D_t1084937515* L_0 = Collision_get_contacts_m266635379(__this, /*hidden argument*/NULL);
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Il2CppObject * L_1 = Array_GetEnumerator_m2284404958((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impulse()
extern "C"  Vector3_t2243707580  Collision_get_impulse_m1836134987 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Impulse_0();
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impactForceSum()
extern "C"  Vector3_t2243707580  Collision_get_impactForceSum_m521972354 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Collision_get_relativeVelocity_m2302609283(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_frictionForceSum()
extern "C"  Vector3_t2243707580  Collision_get_frictionForceSum_m3438602686 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Component UnityEngine.Collision::get_other()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Collision_get_other_m4238363497_MetadataUsageId;
extern "C"  Component_t3819376471 * Collision_get_other_m4238363497 (Collision_t2876846408 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_other_m4238363497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t4233889191 * G_B3_0 = NULL;
	{
		Rigidbody_t4233889191 * L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Rigidbody_t4233889191 * L_2 = __this->get_m_Rigidbody_2();
		G_B3_0 = L_2;
		goto IL_0022;
	}

IL_001c:
	{
		Collider_t3497673348 * L_3 = __this->get_m_Collider_3();
		G_B3_0 = ((Rigidbody_t4233889191 *)(L_3));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Collision
extern "C" void Collision_t2876846408_marshal_pinvoke(const Collision_t2876846408& unmarshaled, Collision_t2876846408_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
extern "C" void Collision_t2876846408_marshal_pinvoke_back(const Collision_t2876846408_marshaled_pinvoke& marshaled, Collision_t2876846408& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
extern "C" void Collision_t2876846408_marshal_pinvoke_cleanup(Collision_t2876846408_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision
extern "C" void Collision_t2876846408_marshal_com(const Collision_t2876846408& unmarshaled, Collision_t2876846408_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
extern "C" void Collision_t2876846408_marshal_com_back(const Collision_t2876846408_marshaled_com& marshaled, Collision_t2876846408& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
extern "C" void Collision_t2876846408_marshal_com_cleanup(Collision_t2876846408_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Collision2D::.ctor()
extern "C"  void Collision2D__ctor_m2559810705 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Collision2D::get_enabled()
extern "C"  bool Collision2D_get_enabled_m1627968529 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_Enabled_4();
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern "C"  Rigidbody2D_t502193897 * Collision2D_get_rigidbody_m4058558226 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_m_Rigidbody_0();
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C"  Collider2D_t646061738 * Collision2D_get_collider_m3330356936 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	{
		Collider2D_t646061738 * L_0 = __this->get_m_Collider_1();
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Collision2D::get_transform()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Collision2D_get_transform_m314016758_MetadataUsageId;
extern "C"  Transform_t3275118058 * Collision2D_get_transform_m314016758 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_transform_m314016758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t3275118058 * G_B3_0 = NULL;
	{
		Rigidbody2D_t502193897 * L_0 = Collision2D_get_rigidbody_m4058558226(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody2D_t502193897 * L_2 = Collision2D_get_rigidbody_m4058558226(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider2D_t646061738 * L_4 = Collision2D_get_collider_m3330356936(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Collision2D_get_gameObject_m4234358314_MetadataUsageId;
extern "C"  GameObject_t1756533147 * Collision2D_get_gameObject_m4234358314 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_gameObject_m4234358314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * G_B3_0 = NULL;
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_m_Rigidbody_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody2D_t502193897 * L_2 = __this->get_m_Rigidbody_0();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider2D_t646061738 * L_4 = __this->get_m_Collider_1();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::get_contacts()
extern "C"  ContactPoint2DU5BU5D_t1215651809* Collision2D_get_contacts_m2230578195 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	{
		ContactPoint2DU5BU5D_t1215651809* L_0 = __this->get_m_Contacts_2();
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern "C"  Vector2_t2243707579  Collision2D_get_relativeVelocity_m2410092780 (Collision2D_t1539500754 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_RelativeVelocity_3();
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t1539500754_marshal_pinvoke(const Collision2D_t1539500754& unmarshaled, Collision2D_t1539500754_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
extern "C" void Collision2D_t1539500754_marshal_pinvoke_back(const Collision2D_t1539500754_marshaled_pinvoke& marshaled, Collision2D_t1539500754& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t1539500754_marshal_pinvoke_cleanup(Collision2D_t1539500754_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t1539500754_marshal_com(const Collision2D_t1539500754& unmarshaled, Collision2D_t1539500754_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
extern "C" void Collision2D_t1539500754_marshal_com_back(const Collision2D_t1539500754_marshaled_com& marshaled, Collision2D_t1539500754& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t1539500754_marshal_com_cleanup(Collision2D_t1539500754_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m1909920690_AdjustorThunk (Il2CppObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	Color__ctor_m1909920690(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3811852957 (Color_t2020392075 * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		__this->set_a_3((1.0f));
		return;
	}
}
extern "C"  void Color__ctor_m3811852957_AdjustorThunk (Il2CppObject * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	Color__ctor_m3811852957(_thisAdjusted, ___r0, ___g1, ___b2, method);
}
// System.String UnityEngine.Color::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2715435867;
extern const uint32_t Color_ToString_m4028093047_MetadataUsageId;
extern "C"  String_t* Color_ToString_m4028093047 (Color_t2020392075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m4028093047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral2715435867, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Color_ToString_m4028093047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_ToString_m4028093047(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m3182525367 (Color_t2020392075 * __this, const MethodInfo* method)
{
	Vector4_t2243707581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector4_t2243707581  L_0 = Color_op_Implicit_m1067945802(NULL /*static, unused*/, (*(Color_t2020392075 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m1576457715((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
extern "C"  int32_t Color_GetHashCode_m3182525367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_GetHashCode_m3182525367(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m661618137_MetadataUsageId;
extern "C"  bool Color_Equals_m661618137 (Color_t2020392075 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m661618137_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Color_t2020392075_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		Il2CppObject * L_1 = ___other0;
		V_0 = ((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox (L_1, Color_t2020392075_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_0)->get_r_0();
		bool L_4 = Single_Equals_m3359827399(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_0)->get_g_1();
		bool L_7 = Single_Equals_m3359827399(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_0)->get_b_2();
		bool L_10 = Single_Equals_m3359827399(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_0)->get_a_3();
		bool L_13 = Single_Equals_m3359827399(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return (bool)G_B7_0;
	}
}
extern "C"  bool Color_Equals_m661618137_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_Equals_m661618137(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Color_Lerp_m3323752807_MetadataUsageId;
extern "C"  Color_t2020392075  Color_Lerp_m3323752807 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___a0, Color_t2020392075  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_Lerp_m3323752807_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_r_0();
		float L_3 = (&___b1)->get_r_0();
		float L_4 = (&___a0)->get_r_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_g_1();
		float L_7 = (&___b1)->get_g_1();
		float L_8 = (&___a0)->get_g_1();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_b_2();
		float L_11 = (&___b1)->get_b_2();
		float L_12 = (&___a0)->get_b_2();
		float L_13 = ___t2;
		float L_14 = (&___a0)->get_a_3();
		float L_15 = (&___b1)->get_a_3();
		float L_16 = (&___a0)->get_a_3();
		float L_17 = ___t2;
		Color_t2020392075  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m1909920690(&L_18, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		return L_18;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2020392075  Color_get_red_m2410286591 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2020392075  Color_get_green_m2671273823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m3987539815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m2650940523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2020392075  Color_get_yellow_m3741935494 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C"  Color_t2020392075  Color_get_gray_m1396712533 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t2020392075  Color_get_grey_m1961362537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t2020392075  Color_get_clear_m1469108305 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m1909920690(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Color::get_grayscale()
extern "C"  float Color_get_grayscale_m2989668074 (Color_t2020392075 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_r_0();
		float L_1 = __this->get_g_1();
		float L_2 = __this->get_b_2();
		return ((float)((float)((float)((float)((float)((float)(0.299f)*(float)L_0))+(float)((float)((float)(0.587f)*(float)L_1))))+(float)((float)((float)(0.114f)*(float)L_2))));
	}
}
extern "C"  float Color_get_grayscale_m2989668074_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_get_grayscale_m2989668074(_thisAdjusted, method);
}
// UnityEngine.Color UnityEngine.Color::get_linear()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Color_get_linear_m3851532985_MetadataUsageId;
extern "C"  Color_t2020392075  Color_get_linear_m3851532985 (Color_t2020392075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_get_linear_m3851532985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_GammaToLinearSpace_m3697142437(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_g_1();
		float L_3 = Mathf_GammaToLinearSpace_m3697142437(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_b_2();
		float L_5 = Mathf_GammaToLinearSpace_m3697142437(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_a_3();
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m1909920690(&L_7, L_1, L_3, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  Color_t2020392075  Color_get_linear_m3851532985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_get_linear_m3851532985(_thisAdjusted, method);
}
// UnityEngine.Color UnityEngine.Color::get_gamma()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Color_get_gamma_m1982353575_MetadataUsageId;
extern "C"  Color_t2020392075  Color_get_gamma_m1982353575 (Color_t2020392075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_get_gamma_m1982353575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_LinearToGammaSpace_m1586839609(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = __this->get_g_1();
		float L_3 = Mathf_LinearToGammaSpace_m1586839609(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_b_2();
		float L_5 = Mathf_LinearToGammaSpace_m1586839609(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_a_3();
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m1909920690(&L_7, L_1, L_3, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  Color_t2020392075  Color_get_gamma_m1982353575_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_get_gamma_m1982353575(_thisAdjusted, method);
}
// System.Single UnityEngine.Color::get_maxColorComponent()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Color_get_maxColorComponent_m1239472485_MetadataUsageId;
extern "C"  float Color_get_maxColorComponent_m1239472485 (Color_t2020392075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color_get_maxColorComponent_m1239472485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_r_0();
		float L_1 = __this->get_g_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_b_2();
		float L_4 = Mathf_Max_m2564622569(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
extern "C"  float Color_get_maxColorComponent_m1239472485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2020392075 * _thisAdjusted = reinterpret_cast<Color_t2020392075 *>(__this + 1);
	return Color_get_maxColorComponent_m1239472485(_thisAdjusted, method);
}
// UnityEngine.Color UnityEngine.Color::op_Addition(UnityEngine.Color,UnityEngine.Color)
extern "C"  Color_t2020392075  Color_op_Addition_m1759335993 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___a0, Color_t2020392075  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = (&___b1)->get_r_0();
		float L_2 = (&___a0)->get_g_1();
		float L_3 = (&___b1)->get_g_1();
		float L_4 = (&___a0)->get_b_2();
		float L_5 = (&___b1)->get_b_2();
		float L_6 = (&___a0)->get_a_3();
		float L_7 = (&___b1)->get_a_3();
		Color_t2020392075  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m1909920690(&L_8, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Subtraction(UnityEngine.Color,UnityEngine.Color)
extern "C"  Color_t2020392075  Color_op_Subtraction_m3931992385 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___a0, Color_t2020392075  ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = (&___b1)->get_r_0();
		float L_2 = (&___a0)->get_g_1();
		float L_3 = (&___b1)->get_g_1();
		float L_4 = (&___a0)->get_b_2();
		float L_5 = (&___b1)->get_b_2();
		float L_6 = (&___a0)->get_a_3();
		float L_7 = (&___b1)->get_a_3();
		Color_t2020392075  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m1909920690(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  Color_op_Multiply_m325555950 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___a0, float ___b1, const MethodInfo* method)
{
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = ___b1;
		float L_2 = (&___a0)->get_g_1();
		float L_3 = ___b1;
		float L_4 = (&___a0)->get_b_2();
		float L_5 = ___b1;
		float L_6 = (&___a0)->get_a_3();
		float L_7 = ___b1;
		Color_t2020392075  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m1909920690(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m3156451394 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___lhs0, Color_t2020392075  ___rhs1, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___lhs0;
		Vector4_t2243707581  L_1 = Color_op_Implicit_m1067945802(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t2020392075  L_2 = ___rhs1;
		Vector4_t2243707581  L_3 = Color_op_Implicit_m1067945802(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = Vector4_op_Equality_m1825453464(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Color::op_Inequality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Inequality_m3949383683 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___lhs0, Color_t2020392075  ___rhs1, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = ___lhs0;
		Color_t2020392075  L_1 = ___rhs1;
		bool L_2 = Color_op_Equality_m3156451394(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t2243707581  Color_op_Implicit_m1067945802 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method)
{
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t2243707581  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m1222289168(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Conversion methods for marshalling of: UnityEngine.Color
extern "C" void Color_t2020392075_marshal_pinvoke(const Color_t2020392075& unmarshaled, Color_t2020392075_marshaled_pinvoke& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color_t2020392075_marshal_pinvoke_back(const Color_t2020392075_marshaled_pinvoke& marshaled, Color_t2020392075& unmarshaled)
{
	float unmarshaled_r_temp_0 = 0.0f;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	float unmarshaled_g_temp_1 = 0.0f;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	float unmarshaled_b_temp_2 = 0.0f;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	float unmarshaled_a_temp_3 = 0.0f;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color
extern "C" void Color_t2020392075_marshal_pinvoke_cleanup(Color_t2020392075_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Color
extern "C" void Color_t2020392075_marshal_com(const Color_t2020392075& unmarshaled, Color_t2020392075_marshaled_com& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color_t2020392075_marshal_com_back(const Color_t2020392075_marshaled_com& marshaled, Color_t2020392075& unmarshaled)
{
	float unmarshaled_r_temp_0 = 0.0f;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	float unmarshaled_g_temp_1 = 0.0f;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	float unmarshaled_b_temp_2 = 0.0f;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	float unmarshaled_a_temp_3 = 0.0f;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color
extern "C" void Color_t2020392075_marshal_com_cleanup(Color_t2020392075_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m1932627809 (Color32_t874517518 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r0;
		__this->set_r_0(L_0);
		uint8_t L_1 = ___g1;
		__this->set_g_1(L_1);
		uint8_t L_2 = ___b2;
		__this->set_b_2(L_2);
		uint8_t L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color32__ctor_m1932627809_AdjustorThunk (Il2CppObject * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method)
{
	Color32_t874517518 * _thisAdjusted = reinterpret_cast<Color32_t874517518 *>(__this + 1);
	Color32__ctor_m1932627809(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.String UnityEngine.Color32::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Byte_t3683104436_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1199940147;
extern const uint32_t Color32_ToString_m1408142756_MetadataUsageId;
extern "C"  String_t* Color32_ToString_m1408142756 (Color32_t874517518 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color32_ToString_m1408142756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_1 = __this->get_r_0();
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t3614634134* L_4 = L_0;
		uint8_t L_5 = __this->get_g_1();
		uint8_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		uint8_t L_9 = __this->get_b_2();
		uint8_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t3614634134* L_12 = L_8;
		uint8_t L_13 = __this->get_a_3();
		uint8_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Byte_t3683104436_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1199940147, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
extern "C"  String_t* Color32_ToString_m1408142756_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color32_t874517518 * _thisAdjusted = reinterpret_cast<Color32_t874517518 *>(__this + 1);
	return Color32_ToString_m1408142756(_thisAdjusted, method);
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Color32_op_Implicit_m624191464_MetadataUsageId;
extern "C"  Color32_t874517518  Color32_op_Implicit_m624191464 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Color32_op_Implicit_m624191464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (&___c0)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___c0)->get_g_1();
		float L_3 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___c0)->get_b_2();
		float L_5 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (&___c0)->get_a_3();
		float L_7 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t874517518  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color32__ctor_m1932627809(&L_8, (((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t2020392075  Color32_op_Implicit_m889975790 (Il2CppObject * __this /* static, unused */, Color32_t874517518  ___c0, const MethodInfo* method)
{
	{
		uint8_t L_0 = (&___c0)->get_r_0();
		uint8_t L_1 = (&___c0)->get_g_1();
		uint8_t L_2 = (&___c0)->get_b_2();
		uint8_t L_3 = (&___c0)->get_a_3();
		Color_t2020392075  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m1909920690(&L_4, ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
// Conversion methods for marshalling of: UnityEngine.Color32
extern "C" void Color32_t874517518_marshal_pinvoke(const Color32_t874517518& unmarshaled, Color32_t874517518_marshaled_pinvoke& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color32_t874517518_marshal_pinvoke_back(const Color32_t874517518_marshaled_pinvoke& marshaled, Color32_t874517518& unmarshaled)
{
	uint8_t unmarshaled_r_temp_0 = 0x0;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	uint8_t unmarshaled_g_temp_1 = 0x0;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	uint8_t unmarshaled_b_temp_2 = 0x0;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	uint8_t unmarshaled_a_temp_3 = 0x0;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color32
extern "C" void Color32_t874517518_marshal_pinvoke_cleanup(Color32_t874517518_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Color32
extern "C" void Color32_t874517518_marshal_com(const Color32_t874517518& unmarshaled, Color32_t874517518_marshaled_com& marshaled)
{
	marshaled.___r_0 = unmarshaled.get_r_0();
	marshaled.___g_1 = unmarshaled.get_g_1();
	marshaled.___b_2 = unmarshaled.get_b_2();
	marshaled.___a_3 = unmarshaled.get_a_3();
}
extern "C" void Color32_t874517518_marshal_com_back(const Color32_t874517518_marshaled_com& marshaled, Color32_t874517518& unmarshaled)
{
	uint8_t unmarshaled_r_temp_0 = 0x0;
	unmarshaled_r_temp_0 = marshaled.___r_0;
	unmarshaled.set_r_0(unmarshaled_r_temp_0);
	uint8_t unmarshaled_g_temp_1 = 0x0;
	unmarshaled_g_temp_1 = marshaled.___g_1;
	unmarshaled.set_g_1(unmarshaled_g_temp_1);
	uint8_t unmarshaled_b_temp_2 = 0x0;
	unmarshaled_b_temp_2 = marshaled.___b_2;
	unmarshaled.set_b_2(unmarshaled_b_temp_2);
	uint8_t unmarshaled_a_temp_3 = 0x0;
	unmarshaled_a_temp_3 = marshaled.___a_3;
	unmarshaled.set_a_3(unmarshaled_a_temp_3);
}
// Conversion method for clean up from marshalling of: UnityEngine.Color32
extern "C" void Color32_t874517518_marshal_com_cleanup(Color32_t874517518_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Component::.ctor()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t Component__ctor_m205306948_MetadataUsageId;
extern "C"  void Component__ctor_m205306948 (Component_t3819376471 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m205306948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object__ctor_m197157284(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method)
{
	typedef Transform_t3275118058 * (*Component_get_transform_m2697483695_ftn) (Component_t3819376471 *);
	static Component_get_transform_m2697483695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m2697483695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method)
{
	typedef GameObject_t1756533147 * (*Component_get_gameObject_m3105766835_ftn) (Component_t3819376471 *);
	static Component_get_gameObject_m3105766835_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m3105766835_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponent_m4225719715 (Component_t3819376471 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t3819376471 * L_2 = GameObject_GetComponent_m306258075(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m2700814707 (Component_t3819376471 * __this, Type_t * ___type0, IntPtr_t ___oneFurtherThanResultValue1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m2700814707_ftn) (Component_t3819376471 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m2700814707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m2700814707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
extern "C"  Component_t3819376471 * Component_GetComponent_m2473832642 (Component_t3819376471 * __this, String_t* ___type0, const MethodInfo* method)
{
	typedef Component_t3819376471 * (*Component_GetComponent_m2473832642_ftn) (Component_t3819376471 *, String_t*);
	static Component_GetComponent_m2473832642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponent_m2473832642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponent(System.String)");
	return _il2cpp_icall_func(__this, ___type0);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t3819376471 * Component_GetComponentInChildren_m3925629424 (Component_t3819376471 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t3819376471 * L_3 = GameObject_GetComponentInChildren_m4263325740(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponentInChildren_m3985003615 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___t0;
		Component_t3819376471 * L_1 = Component_GetComponentInChildren_m3925629424(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInChildren_m843288020 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t4136971630* L_2 = Component_GetComponentsInChildren_m908027537(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInChildren_m908027537 (Component_t3819376471 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t4136971630* L_3 = GameObject_GetComponentsInChildren_m993725821(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponentInParent_m2799402500 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t3819376471 * L_2 = GameObject_GetComponentInParent_m1235194528(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInParent_m4192184629 (Component_t3819376471 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t4136971630* L_2 = Component_GetComponentsInParent_m1920178904(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponentsInParent_m1920178904 (Component_t3819376471 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t4136971630* L_3 = GameObject_GetComponentsInParent_m1568786844(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponents(System.Type)
extern "C"  ComponentU5BU5D_t4136971630* Component_GetComponents_m637589504 (Component_t3819376471 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		ComponentU5BU5D_t4136971630* L_2 = GameObject_GetComponents_m297658252(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m4241828391 (Component_t3819376471 * __this, Type_t * ___searchType0, Il2CppObject * ___resultList1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m4241828391_ftn) (Component_t3819376471 *, Type_t *, Il2CppObject *);
	static Component_GetComponentsForListInternal_m4241828391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m4241828391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m3712441745 (Component_t3819376471 * __this, Type_t * ___type0, List_1_t3188497603 * ___results1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t3188497603 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m4241828391(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m357168014 (Component_t3819376471 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m1425941094(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Component::set_tag(System.String)
extern "C"  void Component_set_tag_m4292293503 (Component_t3819376471 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		GameObject_set_tag_m717375123(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C"  bool Component_CompareTag_m3443292365 (Component_t3819376471 * __this, String_t* ___tag0, const MethodInfo* method)
{
	typedef bool (*Component_CompareTag_m3443292365_ftn) (Component_t3819376471 *, String_t*);
	static Component_CompareTag_m3443292365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_CompareTag_m3443292365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::CompareTag(System.String)");
	return _il2cpp_icall_func(__this, ___tag0);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m2584088787 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessageUpwards_m2584088787_ftn) (Component_t3819376471 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessageUpwards_m2584088787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessageUpwards_m2584088787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object)
extern "C"  void Component_SendMessageUpwards_m325086847 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m2584088787(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String)
extern "C"  void Component_SendMessageUpwards_m2041012277 (Component_t3819376471 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m2584088787(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m19741277 (Component_t3819376471 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessageUpwards_m2584088787(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m2241432133 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessage_m2241432133_ftn) (Component_t3819376471 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessage_m2241432133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m2241432133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C"  void Component_SendMessage_m913946877 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessage_m2241432133(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C"  void Component_SendMessage_m3615678587 (Component_t3819376471 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessage_m2241432133(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m4199581575 (Component_t3819376471 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessage_m2241432133(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m2230184532 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_BroadcastMessage_m2230184532_ftn) (Component_t3819376471 *, String_t*, Il2CppObject *, int32_t);
	static Component_BroadcastMessage_m2230184532_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_BroadcastMessage_m2230184532_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___parameter1, ___options2);
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object)
extern "C"  void Component_BroadcastMessage_m1308086896 (Component_t3819376471 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___parameter1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m2230184532(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String)
extern "C"  void Component_BroadcastMessage_m1706240890 (Component_t3819376471 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m2230184532(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m444672650 (Component_t3819376471 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_BroadcastMessage_m2230184532(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_point()
extern "C"  Vector3_t2243707580  ContactPoint_get_point_m371381516 (ContactPoint_t1376425630 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  ContactPoint_get_point_m371381516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactPoint_t1376425630 * _thisAdjusted = reinterpret_cast<ContactPoint_t1376425630 *>(__this + 1);
	return ContactPoint_get_point_m371381516(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.ContactPoint::get_normal()
extern "C"  Vector3_t2243707580  ContactPoint_get_normal_m91778337 (ContactPoint_t1376425630 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector3_t2243707580  ContactPoint_get_normal_m91778337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactPoint_t1376425630 * _thisAdjusted = reinterpret_cast<ContactPoint_t1376425630 *>(__this + 1);
	return ContactPoint_get_normal_m91778337(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t1376425630_marshal_pinvoke(const ContactPoint_t1376425630& unmarshaled, ContactPoint_t1376425630_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Point_0(), marshaled.___m_Point_0);
	Vector3_t2243707580_marshal_pinvoke(unmarshaled.get_m_Normal_1(), marshaled.___m_Normal_1);
	marshaled.___m_ThisColliderInstanceID_2 = unmarshaled.get_m_ThisColliderInstanceID_2();
	marshaled.___m_OtherColliderInstanceID_3 = unmarshaled.get_m_OtherColliderInstanceID_3();
	marshaled.___m_Separation_4 = unmarshaled.get_m_Separation_4();
}
extern "C" void ContactPoint_t1376425630_marshal_pinvoke_back(const ContactPoint_t1376425630_marshaled_pinvoke& marshaled, ContactPoint_t1376425630& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Point_temp_0;
	memset(&unmarshaled_m_Point_temp_0, 0, sizeof(unmarshaled_m_Point_temp_0));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Point_0, unmarshaled_m_Point_temp_0);
	unmarshaled.set_m_Point_0(unmarshaled_m_Point_temp_0);
	Vector3_t2243707580  unmarshaled_m_Normal_temp_1;
	memset(&unmarshaled_m_Normal_temp_1, 0, sizeof(unmarshaled_m_Normal_temp_1));
	Vector3_t2243707580_marshal_pinvoke_back(marshaled.___m_Normal_1, unmarshaled_m_Normal_temp_1);
	unmarshaled.set_m_Normal_1(unmarshaled_m_Normal_temp_1);
	int32_t unmarshaled_m_ThisColliderInstanceID_temp_2 = 0;
	unmarshaled_m_ThisColliderInstanceID_temp_2 = marshaled.___m_ThisColliderInstanceID_2;
	unmarshaled.set_m_ThisColliderInstanceID_2(unmarshaled_m_ThisColliderInstanceID_temp_2);
	int32_t unmarshaled_m_OtherColliderInstanceID_temp_3 = 0;
	unmarshaled_m_OtherColliderInstanceID_temp_3 = marshaled.___m_OtherColliderInstanceID_3;
	unmarshaled.set_m_OtherColliderInstanceID_3(unmarshaled_m_OtherColliderInstanceID_temp_3);
	float unmarshaled_m_Separation_temp_4 = 0.0f;
	unmarshaled_m_Separation_temp_4 = marshaled.___m_Separation_4;
	unmarshaled.set_m_Separation_4(unmarshaled_m_Separation_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t1376425630_marshal_pinvoke_cleanup(ContactPoint_t1376425630_marshaled_pinvoke& marshaled)
{
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Point_0);
	Vector3_t2243707580_marshal_pinvoke_cleanup(marshaled.___m_Normal_1);
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t1376425630_marshal_com(const ContactPoint_t1376425630& unmarshaled, ContactPoint_t1376425630_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Point_0(), marshaled.___m_Point_0);
	Vector3_t2243707580_marshal_com(unmarshaled.get_m_Normal_1(), marshaled.___m_Normal_1);
	marshaled.___m_ThisColliderInstanceID_2 = unmarshaled.get_m_ThisColliderInstanceID_2();
	marshaled.___m_OtherColliderInstanceID_3 = unmarshaled.get_m_OtherColliderInstanceID_3();
	marshaled.___m_Separation_4 = unmarshaled.get_m_Separation_4();
}
extern "C" void ContactPoint_t1376425630_marshal_com_back(const ContactPoint_t1376425630_marshaled_com& marshaled, ContactPoint_t1376425630& unmarshaled)
{
	Vector3_t2243707580  unmarshaled_m_Point_temp_0;
	memset(&unmarshaled_m_Point_temp_0, 0, sizeof(unmarshaled_m_Point_temp_0));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Point_0, unmarshaled_m_Point_temp_0);
	unmarshaled.set_m_Point_0(unmarshaled_m_Point_temp_0);
	Vector3_t2243707580  unmarshaled_m_Normal_temp_1;
	memset(&unmarshaled_m_Normal_temp_1, 0, sizeof(unmarshaled_m_Normal_temp_1));
	Vector3_t2243707580_marshal_com_back(marshaled.___m_Normal_1, unmarshaled_m_Normal_temp_1);
	unmarshaled.set_m_Normal_1(unmarshaled_m_Normal_temp_1);
	int32_t unmarshaled_m_ThisColliderInstanceID_temp_2 = 0;
	unmarshaled_m_ThisColliderInstanceID_temp_2 = marshaled.___m_ThisColliderInstanceID_2;
	unmarshaled.set_m_ThisColliderInstanceID_2(unmarshaled_m_ThisColliderInstanceID_temp_2);
	int32_t unmarshaled_m_OtherColliderInstanceID_temp_3 = 0;
	unmarshaled_m_OtherColliderInstanceID_temp_3 = marshaled.___m_OtherColliderInstanceID_3;
	unmarshaled.set_m_OtherColliderInstanceID_3(unmarshaled_m_OtherColliderInstanceID_temp_3);
	float unmarshaled_m_Separation_temp_4 = 0.0f;
	unmarshaled_m_Separation_temp_4 = marshaled.___m_Separation_4;
	unmarshaled.set_m_Separation_4(unmarshaled_m_Separation_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint
extern "C" void ContactPoint_t1376425630_marshal_com_cleanup(ContactPoint_t1376425630_marshaled_com& marshaled)
{
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Point_0);
	Vector3_t2243707580_marshal_com_cleanup(marshaled.___m_Normal_1);
}
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
extern "C"  Vector2_t2243707579  ContactPoint2D_get_point_m2343925229 (ContactPoint2D_t3659330976 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_Point_0();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  ContactPoint2D_get_point_m2343925229_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactPoint2D_t3659330976 * _thisAdjusted = reinterpret_cast<ContactPoint2D_t3659330976 *>(__this + 1);
	return ContactPoint2D_get_point_m2343925229(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_normal()
extern "C"  Vector2_t2243707579  ContactPoint2D_get_normal_m3786302012 (ContactPoint2D_t3659330976 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_m_Normal_1();
		return L_0;
	}
}
extern "C"  Vector2_t2243707579  ContactPoint2D_get_normal_m3786302012_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactPoint2D_t3659330976 * _thisAdjusted = reinterpret_cast<ContactPoint2D_t3659330976 *>(__this + 1);
	return ContactPoint2D_get_normal_m3786302012(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t3659330976_marshal_pinvoke(const ContactPoint2D_t3659330976& unmarshaled, ContactPoint2D_t3659330976_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
extern "C" void ContactPoint2D_t3659330976_marshal_pinvoke_back(const ContactPoint2D_t3659330976_marshaled_pinvoke& marshaled, ContactPoint2D_t3659330976& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t3659330976_marshal_pinvoke_cleanup(ContactPoint2D_t3659330976_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t3659330976_marshal_com(const ContactPoint2D_t3659330976& unmarshaled, ContactPoint2D_t3659330976_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
extern "C" void ContactPoint2D_t3659330976_marshal_com_back(const ContactPoint2D_t3659330976_marshaled_com& marshaled, ContactPoint2D_t3659330976& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'ContactPoint2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactPoint2D
extern "C" void ContactPoint2D_t3659330976_marshal_com_cleanup(ContactPoint2D_t3659330976_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ControllerColliderHit::.ctor()
extern "C"  void ControllerColliderHit__ctor_m2990717326 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::get_controller()
extern "C"  CharacterController_t4094781467 * ControllerColliderHit_get_controller_m531319176 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		CharacterController_t4094781467 * L_0 = __this->get_m_Controller_0();
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.ControllerColliderHit::get_collider()
extern "C"  Collider_t3497673348 * ControllerColliderHit_get_collider_m3897495767 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_1();
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.ControllerColliderHit::get_rigidbody()
extern "C"  Rigidbody_t4233889191 * ControllerColliderHit_get_rigidbody_m4025380167 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		Rigidbody_t4233889191 * L_1 = Collider_get_attachedRigidbody_m3279305420(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.GameObject UnityEngine.ControllerColliderHit::get_gameObject()
extern "C"  GameObject_t1756533147 * ControllerColliderHit_get_gameObject_m3793104279 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.ControllerColliderHit::get_transform()
extern "C"  Transform_t3275118058 * ControllerColliderHit_get_transform_m3183428555 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Collider_t3497673348 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_point()
extern "C"  Vector3_t2243707580  ControllerColliderHit_get_point_m3573703281 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Point_2();
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_normal()
extern "C"  Vector3_t2243707580  ControllerColliderHit_get_normal_m1098215280 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_Normal_3();
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C"  Vector3_t2243707580  ControllerColliderHit_get_moveDirection_m3053186297 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_m_MoveDirection_4();
		return L_0;
	}
}
// System.Single UnityEngine.ControllerColliderHit::get_moveLength()
extern "C"  float ControllerColliderHit_get_moveLength_m94453726 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_MoveLength_5();
		return L_0;
	}
}
// System.Boolean UnityEngine.ControllerColliderHit::get_push()
extern "C"  bool ControllerColliderHit_get_push_m2528314697 (ControllerColliderHit_t4070855101 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Push_6();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.ControllerColliderHit::set_push(System.Boolean)
extern "C"  void ControllerColliderHit_set_push_m3582188484 (ControllerColliderHit_t4070855101 * __this, bool ___value0, const MethodInfo* method)
{
	ControllerColliderHit_t4070855101 * G_B2_0 = NULL;
	ControllerColliderHit_t4070855101 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ControllerColliderHit_t4070855101 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_Push_6(G_B3_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke(const ControllerColliderHit_t4070855101& unmarshaled, ControllerColliderHit_t4070855101_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_back(const ControllerColliderHit_t4070855101_marshaled_pinvoke& marshaled, ControllerColliderHit_t4070855101& unmarshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t4070855101_marshal_pinvoke_cleanup(ControllerColliderHit_t4070855101_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t4070855101_marshal_com(const ControllerColliderHit_t4070855101& unmarshaled, ControllerColliderHit_t4070855101_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
extern "C" void ControllerColliderHit_t4070855101_marshal_com_back(const ControllerColliderHit_t4070855101_marshaled_com& marshaled, ControllerColliderHit_t4070855101& unmarshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t4070855101_marshal_com_cleanup(ControllerColliderHit_t4070855101_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m1253937571 (Coroutine_t2299508840 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m2014522928(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m833118514 (Coroutine_t2299508840 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m833118514_ftn) (Coroutine_t2299508840 *);
	static Coroutine_ReleaseCoroutine_m833118514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m833118514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m3953619693 (Coroutine_t2299508840 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m833118514(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_pinvoke(const Coroutine_t2299508840& unmarshaled, Coroutine_t2299508840_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t2299508840_marshal_pinvoke_back(const Coroutine_t2299508840_marshaled_pinvoke& marshaled, Coroutine_t2299508840& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_pinvoke_cleanup(Coroutine_t2299508840_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_com(const Coroutine_t2299508840& unmarshaled, Coroutine_t2299508840_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t2299508840_marshal_com_back(const Coroutine_t2299508840_marshaled_com& marshaled, Coroutine_t2299508840& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t2299508840_marshal_com_cleanup(Coroutine_t2299508840_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_Finalize_m3172731580_MetadataUsageId;
extern "C"  void CullingGroup_Finalize_m3172731580 (CullingGroup_t1091689465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m3172731580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Ptr_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3044532593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_001b;
			}
		}

IL_0015:
		{
			CullingGroup_FinalizerFailure_m3675513936(__this, /*hidden argument*/NULL);
		}

IL_001b:
		{
			IL2CPP_LEAVE(0x27, FINALLY_0020);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0020;
	}

FINALLY_0020:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(32)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(32)
	{
		IL2CPP_JUMP_TBL(0x27, IL_0027)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m2629191995 (CullingGroup_t1091689465 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_Dispose_m2629191995_ftn) (CullingGroup_t1091689465 *);
	static CullingGroup_Dispose_m2629191995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m2629191995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m1292564468 (Il2CppObject * __this /* static, unused */, CullingGroup_t1091689465 * ___cullingGroup0, IntPtr_t ___eventsPtr1, int32_t ___count2, const MethodInfo* method)
{
	CullingGroupEvent_t1057617917 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m1888290092((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t1057617917 *)L_0;
		CullingGroup_t1091689465 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t2480912210 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		V_1 = 0;
		goto IL_0039;
	}

IL_001b:
	{
		CullingGroup_t1091689465 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t2480912210 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t1057617917 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = sizeof(CullingGroupEvent_t1057617917 );
		NullCheck(L_4);
		StateChanged_Invoke_m2308261448(L_4, (*(CullingGroupEvent_t1057617917 *)((CullingGroupEvent_t1057617917 *)((intptr_t)L_5+(int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_001b;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m3675513936 (CullingGroup_t1091689465 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m3675513936_ftn) (CullingGroup_t1091689465 *);
	static CullingGroup_FinalizerFailure_m3675513936_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m3675513936_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_pinvoke(const CullingGroup_t1091689465& unmarshaled, CullingGroup_t1091689465_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern Il2CppClass* StateChanged_t2480912210_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1091689465_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_back(const CullingGroup_t1091689465_marshaled_pinvoke& marshaled, CullingGroup_t1091689465& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1091689465_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2480912210>(marshaled.___m_OnStateChanged_1, StateChanged_t2480912210_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_pinvoke_cleanup(CullingGroup_t1091689465_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_com(const CullingGroup_t1091689465& unmarshaled, CullingGroup_t1091689465_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern Il2CppClass* StateChanged_t2480912210_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t1091689465_com_FromNativeMethodDefinition_MetadataUsageId;
extern "C" void CullingGroup_t1091689465_marshal_com_back(const CullingGroup_t1091689465_marshaled_com& marshaled, CullingGroup_t1091689465& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t1091689465_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2480912210>(marshaled.___m_OnStateChanged_1, StateChanged_t2480912210_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t1091689465_marshal_com_cleanup(CullingGroup_t1091689465_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m2322131884 (StateChanged_t2480912210 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m2308261448 (StateChanged_t2480912210 * __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m2308261448((StateChanged_t2480912210 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t2480912210 (StateChanged_t2480912210 * __this, CullingGroupEvent_t1057617917  ___sphere0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t1057617917_marshaled_pinvoke);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___sphere0' to native representation
	CullingGroupEvent_t1057617917_marshaled_pinvoke ____sphere0_marshaled = { };
	CullingGroupEvent_t1057617917_marshal_pinvoke(___sphere0, ____sphere0_marshaled);

	// Native function invocation
	il2cppPInvokeFunc(____sphere0_marshaled);

	// Marshaling cleanup of parameter '___sphere0' native representation
	CullingGroupEvent_t1057617917_marshal_pinvoke_cleanup(____sphere0_marshaled);

}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern Il2CppClass* CullingGroupEvent_t1057617917_il2cpp_TypeInfo_var;
extern const uint32_t StateChanged_BeginInvoke_m1716538087_MetadataUsageId;
extern "C"  Il2CppObject * StateChanged_BeginInvoke_m1716538087 (StateChanged_t2480912210 * __this, CullingGroupEvent_t1057617917  ___sphere0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m1716538087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t1057617917_il2cpp_TypeInfo_var, &___sphere0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m2186648314 (StateChanged_t2480912210 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t1057617917_marshal_pinvoke(const CullingGroupEvent_t1057617917& unmarshaled, CullingGroupEvent_t1057617917_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Index_0 = unmarshaled.get_m_Index_0();
	marshaled.___m_PrevState_1 = unmarshaled.get_m_PrevState_1();
	marshaled.___m_ThisState_2 = unmarshaled.get_m_ThisState_2();
}
extern "C" void CullingGroupEvent_t1057617917_marshal_pinvoke_back(const CullingGroupEvent_t1057617917_marshaled_pinvoke& marshaled, CullingGroupEvent_t1057617917& unmarshaled)
{
	int32_t unmarshaled_m_Index_temp_0 = 0;
	unmarshaled_m_Index_temp_0 = marshaled.___m_Index_0;
	unmarshaled.set_m_Index_0(unmarshaled_m_Index_temp_0);
	uint8_t unmarshaled_m_PrevState_temp_1 = 0x0;
	unmarshaled_m_PrevState_temp_1 = marshaled.___m_PrevState_1;
	unmarshaled.set_m_PrevState_1(unmarshaled_m_PrevState_temp_1);
	uint8_t unmarshaled_m_ThisState_temp_2 = 0x0;
	unmarshaled_m_ThisState_temp_2 = marshaled.___m_ThisState_2;
	unmarshaled.set_m_ThisState_2(unmarshaled_m_ThisState_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t1057617917_marshal_pinvoke_cleanup(CullingGroupEvent_t1057617917_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t1057617917_marshal_com(const CullingGroupEvent_t1057617917& unmarshaled, CullingGroupEvent_t1057617917_marshaled_com& marshaled)
{
	marshaled.___m_Index_0 = unmarshaled.get_m_Index_0();
	marshaled.___m_PrevState_1 = unmarshaled.get_m_PrevState_1();
	marshaled.___m_ThisState_2 = unmarshaled.get_m_ThisState_2();
}
extern "C" void CullingGroupEvent_t1057617917_marshal_com_back(const CullingGroupEvent_t1057617917_marshaled_com& marshaled, CullingGroupEvent_t1057617917& unmarshaled)
{
	int32_t unmarshaled_m_Index_temp_0 = 0;
	unmarshaled_m_Index_temp_0 = marshaled.___m_Index_0;
	unmarshaled.set_m_Index_0(unmarshaled_m_Index_temp_0);
	uint8_t unmarshaled_m_PrevState_temp_1 = 0x0;
	unmarshaled_m_PrevState_temp_1 = marshaled.___m_PrevState_1;
	unmarshaled.set_m_PrevState_1(unmarshaled_m_PrevState_temp_1);
	uint8_t unmarshaled_m_ThisState_temp_2 = 0x0;
	unmarshaled_m_ThisState_temp_2 = marshaled.___m_ThisState_2;
	unmarshaled.set_m_ThisState_2(unmarshaled_m_ThisState_temp_2);
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroupEvent
extern "C" void CullingGroupEvent_t1057617917_marshal_com_cleanup(CullingGroupEvent_t1057617917_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m860533511 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Cursor_set_visible_m860533511_ftn) (bool);
	static Cursor_set_visible_m860533511_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_set_visible_m860533511_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::set_visible(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m90193718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Cursor_get_lockState_m90193718_ftn) ();
	static Cursor_get_lockState_m90193718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_lockState_m90193718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_lockState()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C"  void Cursor_set_lockState_m387168319 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Cursor_set_lockState_m387168319_ftn) (int32_t);
	static Cursor_set_lockState_m387168319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_set_lockState_m387168319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m1721050687 (CustomYieldInstruction_t1786092740 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern "C"  Il2CppObject * CustomYieldInstruction_get_Current_m1743233962 (CustomYieldInstruction_t1786092740 * __this, const MethodInfo* method)
{
	{
		return NULL;
	}
}
// System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern "C"  bool CustomYieldInstruction_MoveNext_m486982469 (CustomYieldInstruction_t1786092740 * __this, const MethodInfo* method)
{
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, __this);
		return L_0;
	}
}
// System.Void UnityEngine.CustomYieldInstruction::Reset()
extern "C"  void CustomYieldInstruction_Reset_m3409422382 (CustomYieldInstruction_t1786092740 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern Il2CppClass* DebugLogHandler_t865810509_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t3328995178_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m2981642087_MetadataUsageId;
extern "C"  void Debug__cctor_m2981642087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m2981642087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DebugLogHandler_t865810509 * L_0 = (DebugLogHandler_t865810509 *)il2cpp_codegen_object_new(DebugLogHandler_t865810509_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m3134219506(L_0, /*hidden argument*/NULL);
		Logger_t3328995178 * L_1 = (Logger_t3328995178 *)il2cpp_codegen_object_new(Logger_t3328995178_il2cpp_TypeInfo_var);
		Logger__ctor_m3834134587(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t1368543263_StaticFields*)Debug_t1368543263_il2cpp_TypeInfo_var->static_fields)->set_s_Logger_0(L_1);
		return;
	}
}
// UnityEngine.ILogger UnityEngine.Debug::get_logger()
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug_get_logger_m4173808038_MetadataUsageId;
extern "C"  Il2CppObject * Debug_get_logger_m4173808038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_logger_m4173808038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Logger_t3328995178 * L_0 = ((Debug_t1368543263_StaticFields*)Debug_t1368543263_il2cpp_TypeInfo_var->static_fields)->get_s_Logger_0();
		return L_0;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawLine_m1854161812_MetadataUsageId;
extern "C"  void Debug_DrawLine_m1854161812 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, Color_t2020392075  ___color2, float ___duration3, bool ___depthTest4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m1854161812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___duration3;
		bool L_1 = ___depthTest4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m1075270313(NULL /*static, unused*/, (&___start0), (&___end1), (&___color2), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawLine_m3455422326_MetadataUsageId;
extern "C"  void Debug_DrawLine_m3455422326 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___end1, Color_t2020392075  ___color2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawLine_m3455422326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		float L_0 = V_1;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_INTERNAL_CALL_DrawLine_m1075270313(NULL /*static, unused*/, (&___start0), (&___end1), (&___color2), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern "C"  void Debug_INTERNAL_CALL_DrawLine_m1075270313 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580 * ___start0, Vector3_t2243707580 * ___end1, Color_t2020392075 * ___color2, float ___duration3, bool ___depthTest4, const MethodInfo* method)
{
	typedef void (*Debug_INTERNAL_CALL_DrawLine_m1075270313_ftn) (Vector3_t2243707580 *, Vector3_t2243707580 *, Color_t2020392075 *, float, bool);
	static Debug_INTERNAL_CALL_DrawLine_m1075270313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_INTERNAL_CALL_DrawLine_m1075270313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)");
	_il2cpp_icall_func(___start0, ___end1, ___color2, ___duration3, ___depthTest4);
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawRay_m628516414_MetadataUsageId;
extern "C"  void Debug_DrawRay_m628516414 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___dir1, Color_t2020392075  ___color2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m628516414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		V_0 = (bool)1;
		V_1 = (0.0f);
		Vector3_t2243707580  L_0 = ___start0;
		Vector3_t2243707580  L_1 = ___dir1;
		Color_t2020392075  L_2 = ___color2;
		float L_3 = V_1;
		bool L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawRay_m4006913064(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Debug_DrawRay_m4006913064_MetadataUsageId;
extern "C"  void Debug_DrawRay_m4006913064 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___start0, Vector3_t2243707580  ___dir1, Color_t2020392075  ___color2, float ___duration3, bool ___depthTest4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_DrawRay_m4006913064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t2243707580  L_0 = ___start0;
		Vector3_t2243707580  L_1 = ___start0;
		Vector3_t2243707580  L_2 = ___dir1;
		Vector3_t2243707580  L_3 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Color_t2020392075  L_4 = ___color2;
		float L_5 = ___duration3;
		bool L_6 = ___depthTest4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m1854161812(NULL /*static, unused*/, L_0, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m920475918_MetadataUsageId;
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m920475918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object,UnityEngine.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m4271593384_MetadataUsageId;
extern "C"  void Debug_Log_m4271593384 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m4271593384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t1021602117 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 3, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogError_m3715728798_MetadataUsageId;
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m3715728798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogError_m865553560_MetadataUsageId;
extern "C"  void Debug_LogError_m865553560 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m865553560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t1021602117 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogException_m1861430175_MetadataUsageId;
extern "C"  void Debug_LogException_m1861430175 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m1861430175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1927440687 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t1927440687 *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_0, L_1, (Object_t1021602117 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogException_m3078170301_MetadataUsageId;
extern "C"  void Debug_LogException_m3078170301 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m3078170301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1927440687 * L_1 = ___exception0;
		Object_t1021602117 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t1927440687 *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogWarning_m2503577968_MetadataUsageId;
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m2503577968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 2, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogger_t1425954571_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogWarning_m1280021602_MetadataUsageId;
extern "C"  void Debug_LogWarning_m1280021602 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m1280021602_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t1021602117 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t1021602117 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t1425954571_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ILogHandler_t264057413_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogWarningFormat_m79553173_MetadataUsageId;
extern "C"  void Debug_LogWarningFormat_m79553173 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___context0, String_t* ___format1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarningFormat_m79553173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m4173808038(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t1021602117 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t3614634134* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t1021602117 *, String_t*, ObjectU5BU5D_t3614634134* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t264057413_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2, L_3);
		return;
	}
}
// System.Boolean UnityEngine.Debug::get_isDebugBuild()
extern "C"  bool Debug_get_isDebugBuild_m3461630994 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Debug_get_isDebugBuild_m3461630994_ftn) ();
	static Debug_get_isDebugBuild_m3461630994_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_get_isDebugBuild_m3461630994_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::get_isDebugBuild()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m3134219506 (DebugLogHandler_t865810509 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m3491540823 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t1021602117 * ___obj2, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m3491540823_ftn) (int32_t, String_t*, Object_t1021602117 *);
	static DebugLogHandler_Internal_Log_m3491540823_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m3491540823_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m317712981 (Il2CppObject * __this /* static, unused */, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___obj1, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m317712981_ftn) (Exception_t1927440687 *, Object_t1021602117 *);
	static DebugLogHandler_Internal_LogException_m317712981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m317712981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DebugLogHandler_LogFormat_m177245518_MetadataUsageId;
extern "C"  void DebugLogHandler_LogFormat_m177245518 (DebugLogHandler_t865810509 * __this, int32_t ___logType0, Object_t1021602117 * ___context1, String_t* ___format2, ObjectU5BU5D_t3614634134* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m177245518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t3614634134* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m1263743648(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t1021602117 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m3491540823(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m769094553 (DebugLogHandler_t865810509 * __this, Exception_t1927440687 * ___exception0, Object_t1021602117 * ___context1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ___exception0;
		Object_t1021602117 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m317712981(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C"  void DisallowMultipleComponent__ctor_m533952133 (DisallowMultipleComponent_t2656950 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m2167115811 (Display_t3666191348 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m2996690883(&L_0, 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m563295973 (Display_t3666191348 * __this, IntPtr_t ___nativeDisplay0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern Il2CppClass* DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var;
extern Il2CppClass* Display_t3666191348_il2cpp_TypeInfo_var;
extern const uint32_t Display__cctor_m7440126_MetadataUsageId;
extern "C"  void Display__cctor_m7440126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m7440126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t1314630077* L_0 = ((DisplayU5BU5D_t1314630077*)SZArrayNew(DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t3666191348 * L_1 = (Display_t3666191348 *)il2cpp_codegen_object_new(Display_t3666191348_il2cpp_TypeInfo_var);
		Display__ctor_m2167115811(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t3666191348 *)L_1);
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set_displays_1(L_0);
		DisplayU5BU5D_t1314630077* L_2 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		Display_t3666191348 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_4);
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t3423469815 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern Il2CppClass* DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var;
extern Il2CppClass* Display_t3666191348_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m3412638488_MetadataUsageId;
extern "C"  void Display_RecreateDisplayList_m3412638488 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t169632028* ___nativeDisplay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m3412638488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t169632028* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set_displays_1(((DisplayU5BU5D_t1314630077*)SZArrayNew(DisplayU5BU5D_t1314630077_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1314630077* L_1 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t169632028* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		IntPtr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t3666191348 * L_7 = (Display_t3666191348 *)il2cpp_codegen_object_new(Display_t3666191348_il2cpp_TypeInfo_var);
		Display__ctor_m563295973(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t3666191348 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t169632028* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t1314630077* L_11 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		Display_t3666191348 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern Il2CppClass* Display_t3666191348_il2cpp_TypeInfo_var;
extern const uint32_t Display_FireDisplaysUpdated_m3557250167_MetadataUsageId;
extern "C"  void Display_FireDisplaysUpdated_m3557250167 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m3557250167_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t3423469815 * L_0 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t3666191348_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t3423469815 * L_1 = ((Display_t3666191348_StaticFields*)Display_t3666191348_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m3646339243(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m2851669167 (DisplaysUpdatedDelegate_t3423469815 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m3646339243 (DisplaysUpdatedDelegate_t3423469815 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m3646339243((DisplaysUpdatedDelegate_t3423469815 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t3423469815 (DisplaysUpdatedDelegate_t3423469815 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DisplaysUpdatedDelegate_BeginInvoke_m2030617484 (DisplaysUpdatedDelegate_t3423469815 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m3234564837 (DisplaysUpdatedDelegate_t3423469815 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m310530075 (DrivenRectTransformTracker_t154385424 * __this, Object_t1021602117 * ___driver0, RectTransform_t3349966182 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Add_m310530075_AdjustorThunk (Il2CppObject * __this, Object_t1021602117 * ___driver0, RectTransform_t3349966182 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method)
{
	DrivenRectTransformTracker_t154385424 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t154385424 *>(__this + 1);
	DrivenRectTransformTracker_Add_m310530075(_thisAdjusted, ___driver0, ___rectTransform1, ___drivenProperties2, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m864483440 (DrivenRectTransformTracker_t154385424 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m864483440_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DrivenRectTransformTracker_t154385424 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t154385424 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m864483440(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t154385424_marshal_pinvoke(const DrivenRectTransformTracker_t154385424& unmarshaled, DrivenRectTransformTracker_t154385424_marshaled_pinvoke& marshaled)
{
}
extern "C" void DrivenRectTransformTracker_t154385424_marshal_pinvoke_back(const DrivenRectTransformTracker_t154385424_marshaled_pinvoke& marshaled, DrivenRectTransformTracker_t154385424& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t154385424_marshal_pinvoke_cleanup(DrivenRectTransformTracker_t154385424_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t154385424_marshal_com(const DrivenRectTransformTracker_t154385424& unmarshaled, DrivenRectTransformTracker_t154385424_marshaled_com& marshaled)
{
}
extern "C" void DrivenRectTransformTracker_t154385424_marshal_com_back(const DrivenRectTransformTracker_t154385424_marshaled_com& marshaled, DrivenRectTransformTracker_t154385424& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.DrivenRectTransformTracker
extern "C" void DrivenRectTransformTracker_t154385424_marshal_com_cleanup(DrivenRectTransformTracker_t154385424_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Event::.ctor()
extern "C"  void Event__ctor_m4174297401 (Event_t3028476042 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Event_Init_m3901382626(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.Int32)
extern "C"  void Event__ctor_m3375547476 (Event_t3028476042 * __this, int32_t ___displayIndex0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___displayIndex0;
		Event_Init_m3901382626(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Finalize()
extern "C"  void Event_Finalize_m3215242047 (Event_t3028476042 * __this, const MethodInfo* method)
{
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m1195902101(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m4087144328(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C"  Vector2_t2243707579  Event_get_mousePosition_m3789571399 (Event_t3028476042 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Event_Internal_GetMousePosition_m38523489(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_delta()
extern "C"  Vector2_t2243707579  Event_get_delta_m2710333371 (Event_t3028476042 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Event_Internal_GetMouseDelta_m2840326024(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2243707579  L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.Event::get_shift()
extern "C"  bool Event_get_shift_m1229469022 (Event_t3028476042 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Event::get_alt()
extern "C"  bool Event_get_alt_m2170452849 (Event_t3028476042 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern Il2CppClass* Event_t3028476042_il2cpp_TypeInfo_var;
extern const uint32_t Event_get_current_m2901774193_MetadataUsageId;
extern "C"  Event_t3028476042 * Event_get_current_m2901774193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_get_current_m2901774193_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t3028476042 * L_0 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_s_Current_1();
		return L_0;
	}
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent(System.Int32)
extern Il2CppClass* Event_t3028476042_il2cpp_TypeInfo_var;
extern const uint32_t Event_Internal_MakeMasterEventCurrent_m1829330051_MetadataUsageId;
extern "C"  void Event_Internal_MakeMasterEventCurrent_m1829330051 (Il2CppObject * __this /* static, unused */, int32_t ___displayIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_Internal_MakeMasterEventCurrent_m1829330051_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t3028476042 * L_0 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___displayIndex0;
		Event_t3028476042 * L_2 = (Event_t3028476042 *)il2cpp_codegen_object_new(Event_t3028476042_il2cpp_TypeInfo_var);
		Event__ctor_m3375547476(L_2, L_1, /*hidden argument*/NULL);
		((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->set_s_MasterEvent_2(L_2);
	}

IL_0015:
	{
		Event_t3028476042 * L_3 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		int32_t L_4 = ___displayIndex0;
		NullCheck(L_3);
		Event_set_displayIndex_m3631666901(L_3, L_4, /*hidden argument*/NULL);
		Event_t3028476042 * L_5 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->set_s_Current_1(L_5);
		Event_t3028476042 * L_6 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		NullCheck(L_6);
		IntPtr_t L_7 = L_6->get_m_Ptr_0();
		Event_Internal_SetNativeEvent_m1899228752(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C"  bool Event_get_isKey_m1145918225 (Event_t3028476042 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C"  bool Event_get_isMouse_m569219555 (Event_t3028476042 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return (bool)G_B5_0;
	}
}
// UnityEngine.Event UnityEngine.Event::KeyboardEvent(System.String)
extern const Il2CppType* KeyCode_t2283395152_0_0_0_var;
extern Il2CppClass* Event_t3028476042_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3986656710_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t3259014390_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2118310873_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1209957957_MethodInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m2977303364_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral104530092;
extern Il2CppCodeGenString* _stringLiteral2833413447;
extern Il2CppCodeGenString* _stringLiteral3236697974;
extern Il2CppCodeGenString* _stringLiteral1670614033;
extern Il2CppCodeGenString* _stringLiteral2430128920;
extern Il2CppCodeGenString* _stringLiteral864044979;
extern Il2CppCodeGenString* _stringLiteral1267329506;
extern Il2CppCodeGenString* _stringLiteral3996212861;
extern Il2CppCodeGenString* _stringLiteral4043267028;
extern Il2CppCodeGenString* _stringLiteral2477183087;
extern Il2CppCodeGenString* _stringLiteral1623559866;
extern Il2CppCodeGenString* _stringLiteral57475925;
extern Il2CppCodeGenString* _stringLiteral1220275339;
extern Il2CppCodeGenString* _stringLiteral2026844393;
extern Il2CppCodeGenString* _stringLiteral507814619;
extern Il2CppCodeGenString* _stringLiteral1178848913;
extern Il2CppCodeGenString* _stringLiteral1335981990;
extern Il2CppCodeGenString* _stringLiteral1543969241;
extern Il2CppCodeGenString* _stringLiteral1367190538;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral695394959;
extern Il2CppCodeGenString* _stringLiteral1414245067;
extern Il2CppCodeGenString* _stringLiteral3068682171;
extern Il2CppCodeGenString* _stringLiteral331999482;
extern Il2CppCodeGenString* _stringLiteral2532232006;
extern Il2CppCodeGenString* _stringLiteral3199469141;
extern Il2CppCodeGenString* _stringLiteral151017037;
extern Il2CppCodeGenString* _stringLiteral4226874623;
extern Il2CppCodeGenString* _stringLiteral1381955065;
extern Il2CppCodeGenString* _stringLiteral696029671;
extern Il2CppCodeGenString* _stringLiteral2827728167;
extern Il2CppCodeGenString* _stringLiteral3231012694;
extern Il2CppCodeGenString* _stringLiteral1664928753;
extern Il2CppCodeGenString* _stringLiteral2424443640;
extern Il2CppCodeGenString* _stringLiteral858359699;
extern Il2CppCodeGenString* _stringLiteral1261644226;
extern Il2CppCodeGenString* _stringLiteral3990527581;
extern Il2CppCodeGenString* _stringLiteral4037581748;
extern Il2CppCodeGenString* _stringLiteral2471497807;
extern Il2CppCodeGenString* _stringLiteral2833411447;
extern Il2CppCodeGenString* _stringLiteral2833411446;
extern Il2CppCodeGenString* _stringLiteral2833411445;
extern Il2CppCodeGenString* _stringLiteral2833411444;
extern Il2CppCodeGenString* _stringLiteral2833411451;
extern Il2CppCodeGenString* _stringLiteral2833411450;
extern Il2CppCodeGenString* _stringLiteral2757751167;
extern Il2CppCodeGenString* _stringLiteral3687636376;
extern Il2CppCodeGenString* _stringLiteral1144830560;
extern Il2CppCodeGenString* _stringLiteral1755577857;
extern const uint32_t Event_KeyboardEvent_m1089796218_MetadataUsageId;
extern "C"  Event_t3028476042 * Event_KeyboardEvent_m1089796218 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_KeyboardEvent_m1089796218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Event_t3028476042 * V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = NULL;
	Il2CppChar V_4 = 0x0;
	String_t* V_5 = NULL;
	Dictionary_2_t3986656710 * V_6 = NULL;
	int32_t V_7 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Event_t3028476042 * L_0 = (Event_t3028476042 *)il2cpp_codegen_object_new(Event_t3028476042_il2cpp_TypeInfo_var);
		Event__ctor_m3375547476(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		Event_t3028476042 * L_1 = V_0;
		NullCheck(L_1);
		Event_set_type_m4059414571(L_1, 4, /*hidden argument*/NULL);
		String_t* L_2 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		Event_t3028476042 * L_4 = V_0;
		return L_4;
	}

IL_001b:
	{
		V_1 = 0;
		V_2 = (bool)0;
	}

IL_001f:
	{
		V_2 = (bool)1;
		int32_t L_5 = V_1;
		String_t* L_6 = ___key0;
		NullCheck(L_6);
		int32_t L_7 = String_get_Length_m1606060069(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0034;
		}
	}
	{
		V_2 = (bool)0;
		goto IL_00ce;
	}

IL_0034:
	{
		String_t* L_8 = ___key0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		Il2CppChar L_10 = String_get_Chars_m4230566705(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Il2CppChar L_11 = V_4;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 0)
		{
			goto IL_00aa;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 1)
		{
			goto IL_0057;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 2)
		{
			goto IL_0093;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)35))) == 3)
		{
			goto IL_0065;
		}
	}

IL_0057:
	{
		Il2CppChar L_12 = V_4;
		if ((((int32_t)L_12) == ((int32_t)((int32_t)94))))
		{
			goto IL_007c;
		}
	}
	{
		goto IL_00c1;
	}

IL_0065:
	{
		Event_t3028476042 * L_13 = V_0;
		Event_t3028476042 * L_14 = L_13;
		NullCheck(L_14);
		int32_t L_15 = Event_get_modifiers_m430092210(L_14, /*hidden argument*/NULL);
		NullCheck(L_14);
		Event_set_modifiers_m2565042639(L_14, ((int32_t)((int32_t)L_15|(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_00c8;
	}

IL_007c:
	{
		Event_t3028476042 * L_17 = V_0;
		Event_t3028476042 * L_18 = L_17;
		NullCheck(L_18);
		int32_t L_19 = Event_get_modifiers_m430092210(L_18, /*hidden argument*/NULL);
		NullCheck(L_18);
		Event_set_modifiers_m2565042639(L_18, ((int32_t)((int32_t)L_19|(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		goto IL_00c8;
	}

IL_0093:
	{
		Event_t3028476042 * L_21 = V_0;
		Event_t3028476042 * L_22 = L_21;
		NullCheck(L_22);
		int32_t L_23 = Event_get_modifiers_m430092210(L_22, /*hidden argument*/NULL);
		NullCheck(L_22);
		Event_set_modifiers_m2565042639(L_22, ((int32_t)((int32_t)L_23|(int32_t)8)), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
		goto IL_00c8;
	}

IL_00aa:
	{
		Event_t3028476042 * L_25 = V_0;
		Event_t3028476042 * L_26 = L_25;
		NullCheck(L_26);
		int32_t L_27 = Event_get_modifiers_m430092210(L_26, /*hidden argument*/NULL);
		NullCheck(L_26);
		Event_set_modifiers_m2565042639(L_26, ((int32_t)((int32_t)L_27|(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_28 = V_1;
		V_1 = ((int32_t)((int32_t)L_28+(int32_t)1));
		goto IL_00c8;
	}

IL_00c1:
	{
		V_2 = (bool)0;
		goto IL_00c8;
	}

IL_00c8:
	{
		bool L_29 = V_2;
		if (L_29)
		{
			goto IL_001f;
		}
	}

IL_00ce:
	{
		String_t* L_30 = ___key0;
		int32_t L_31 = V_1;
		String_t* L_32 = ___key0;
		NullCheck(L_32);
		int32_t L_33 = String_get_Length_m1606060069(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_1;
		NullCheck(L_30);
		String_t* L_35 = String_Substring_m12482732(L_30, L_31, ((int32_t)((int32_t)L_33-(int32_t)L_34)), /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_36 = String_ToLower_m2994460523(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		String_t* L_37 = V_3;
		V_5 = L_37;
		String_t* L_38 = V_5;
		if (!L_38)
		{
			goto IL_09e6;
		}
	}
	{
		Dictionary_2_t3986656710 * L_39 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_3();
		if (L_39)
		{
			goto IL_03ac;
		}
	}
	{
		Dictionary_2_t3986656710 * L_40 = (Dictionary_2_t3986656710 *)il2cpp_codegen_object_new(Dictionary_2_t3986656710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2118310873(L_40, ((int32_t)49), /*hidden argument*/Dictionary_2__ctor_m2118310873_MethodInfo_var);
		V_6 = L_40;
		Dictionary_2_t3986656710 * L_41 = V_6;
		NullCheck(L_41);
		Dictionary_2_Add_m1209957957(L_41, _stringLiteral104530092, 0, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_42 = V_6;
		NullCheck(L_42);
		Dictionary_2_Add_m1209957957(L_42, _stringLiteral2833413447, 1, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_43 = V_6;
		NullCheck(L_43);
		Dictionary_2_Add_m1209957957(L_43, _stringLiteral3236697974, 2, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_44 = V_6;
		NullCheck(L_44);
		Dictionary_2_Add_m1209957957(L_44, _stringLiteral1670614033, 3, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_45 = V_6;
		NullCheck(L_45);
		Dictionary_2_Add_m1209957957(L_45, _stringLiteral2430128920, 4, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_46 = V_6;
		NullCheck(L_46);
		Dictionary_2_Add_m1209957957(L_46, _stringLiteral864044979, 5, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_47 = V_6;
		NullCheck(L_47);
		Dictionary_2_Add_m1209957957(L_47, _stringLiteral1267329506, 6, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_48 = V_6;
		NullCheck(L_48);
		Dictionary_2_Add_m1209957957(L_48, _stringLiteral3996212861, 7, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_49 = V_6;
		NullCheck(L_49);
		Dictionary_2_Add_m1209957957(L_49, _stringLiteral4043267028, 8, /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_50 = V_6;
		NullCheck(L_50);
		Dictionary_2_Add_m1209957957(L_50, _stringLiteral2477183087, ((int32_t)9), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_51 = V_6;
		NullCheck(L_51);
		Dictionary_2_Add_m1209957957(L_51, _stringLiteral1623559866, ((int32_t)10), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_52 = V_6;
		NullCheck(L_52);
		Dictionary_2_Add_m1209957957(L_52, _stringLiteral57475925, ((int32_t)11), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_53 = V_6;
		NullCheck(L_53);
		Dictionary_2_Add_m1209957957(L_53, _stringLiteral1220275339, ((int32_t)12), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_54 = V_6;
		NullCheck(L_54);
		Dictionary_2_Add_m1209957957(L_54, _stringLiteral2026844393, ((int32_t)13), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_55 = V_6;
		NullCheck(L_55);
		Dictionary_2_Add_m1209957957(L_55, _stringLiteral507814619, ((int32_t)14), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_56 = V_6;
		NullCheck(L_56);
		Dictionary_2_Add_m1209957957(L_56, _stringLiteral1178848913, ((int32_t)15), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_57 = V_6;
		NullCheck(L_57);
		Dictionary_2_Add_m1209957957(L_57, _stringLiteral1335981990, ((int32_t)16), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_58 = V_6;
		NullCheck(L_58);
		Dictionary_2_Add_m1209957957(L_58, _stringLiteral1543969241, ((int32_t)17), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_59 = V_6;
		NullCheck(L_59);
		Dictionary_2_Add_m1209957957(L_59, _stringLiteral1367190538, ((int32_t)18), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_60 = V_6;
		NullCheck(L_60);
		Dictionary_2_Add_m1209957957(L_60, _stringLiteral3423761043, ((int32_t)19), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_61 = V_6;
		NullCheck(L_61);
		Dictionary_2_Add_m1209957957(L_61, _stringLiteral109637592, ((int32_t)20), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_62 = V_6;
		NullCheck(L_62);
		Dictionary_2_Add_m1209957957(L_62, _stringLiteral695394959, ((int32_t)21), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_63 = V_6;
		NullCheck(L_63);
		Dictionary_2_Add_m1209957957(L_63, _stringLiteral1414245067, ((int32_t)22), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_64 = V_6;
		NullCheck(L_64);
		Dictionary_2_Add_m1209957957(L_64, _stringLiteral3068682171, ((int32_t)23), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_65 = V_6;
		NullCheck(L_65);
		Dictionary_2_Add_m1209957957(L_65, _stringLiteral331999482, ((int32_t)24), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_66 = V_6;
		NullCheck(L_66);
		Dictionary_2_Add_m1209957957(L_66, _stringLiteral2532232006, ((int32_t)25), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_67 = V_6;
		NullCheck(L_67);
		Dictionary_2_Add_m1209957957(L_67, _stringLiteral3199469141, ((int32_t)26), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_68 = V_6;
		NullCheck(L_68);
		Dictionary_2_Add_m1209957957(L_68, _stringLiteral151017037, ((int32_t)27), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_69 = V_6;
		NullCheck(L_69);
		Dictionary_2_Add_m1209957957(L_69, _stringLiteral4226874623, ((int32_t)28), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_70 = V_6;
		NullCheck(L_70);
		Dictionary_2_Add_m1209957957(L_70, _stringLiteral1381955065, ((int32_t)29), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_71 = V_6;
		NullCheck(L_71);
		Dictionary_2_Add_m1209957957(L_71, _stringLiteral696029671, ((int32_t)30), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_72 = V_6;
		NullCheck(L_72);
		Dictionary_2_Add_m1209957957(L_72, _stringLiteral2827728167, ((int32_t)31), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_73 = V_6;
		NullCheck(L_73);
		Dictionary_2_Add_m1209957957(L_73, _stringLiteral3231012694, ((int32_t)32), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_74 = V_6;
		NullCheck(L_74);
		Dictionary_2_Add_m1209957957(L_74, _stringLiteral1664928753, ((int32_t)33), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_75 = V_6;
		NullCheck(L_75);
		Dictionary_2_Add_m1209957957(L_75, _stringLiteral2424443640, ((int32_t)34), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_76 = V_6;
		NullCheck(L_76);
		Dictionary_2_Add_m1209957957(L_76, _stringLiteral858359699, ((int32_t)35), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_77 = V_6;
		NullCheck(L_77);
		Dictionary_2_Add_m1209957957(L_77, _stringLiteral1261644226, ((int32_t)36), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_78 = V_6;
		NullCheck(L_78);
		Dictionary_2_Add_m1209957957(L_78, _stringLiteral3990527581, ((int32_t)37), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_79 = V_6;
		NullCheck(L_79);
		Dictionary_2_Add_m1209957957(L_79, _stringLiteral4037581748, ((int32_t)38), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_80 = V_6;
		NullCheck(L_80);
		Dictionary_2_Add_m1209957957(L_80, _stringLiteral2471497807, ((int32_t)39), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_81 = V_6;
		NullCheck(L_81);
		Dictionary_2_Add_m1209957957(L_81, _stringLiteral2833411447, ((int32_t)40), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_82 = V_6;
		NullCheck(L_82);
		Dictionary_2_Add_m1209957957(L_82, _stringLiteral2833411446, ((int32_t)41), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_83 = V_6;
		NullCheck(L_83);
		Dictionary_2_Add_m1209957957(L_83, _stringLiteral2833411445, ((int32_t)42), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_84 = V_6;
		NullCheck(L_84);
		Dictionary_2_Add_m1209957957(L_84, _stringLiteral2833411444, ((int32_t)43), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_85 = V_6;
		NullCheck(L_85);
		Dictionary_2_Add_m1209957957(L_85, _stringLiteral2833411451, ((int32_t)44), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_86 = V_6;
		NullCheck(L_86);
		Dictionary_2_Add_m1209957957(L_86, _stringLiteral2833411450, ((int32_t)45), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_87 = V_6;
		NullCheck(L_87);
		Dictionary_2_Add_m1209957957(L_87, _stringLiteral2757751167, ((int32_t)46), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_88 = V_6;
		NullCheck(L_88);
		Dictionary_2_Add_m1209957957(L_88, _stringLiteral3687636376, ((int32_t)47), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_89 = V_6;
		NullCheck(L_89);
		Dictionary_2_Add_m1209957957(L_89, _stringLiteral1144830560, ((int32_t)48), /*hidden argument*/Dictionary_2_Add_m1209957957_MethodInfo_var);
		Dictionary_2_t3986656710 * L_90 = V_6;
		((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map0_3(L_90);
	}

IL_03ac:
	{
		Dictionary_2_t3986656710 * L_91 = ((Event_t3028476042_StaticFields*)Event_t3028476042_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map0_3();
		String_t* L_92 = V_5;
		NullCheck(L_91);
		bool L_93 = Dictionary_2_TryGetValue_m2977303364(L_91, L_92, (&V_7), /*hidden argument*/Dictionary_2_TryGetValue_m2977303364_MethodInfo_var);
		if (!L_93)
		{
			goto IL_09e6;
		}
	}
	{
		int32_t L_94 = V_7;
		if (L_94 == 0)
		{
			goto IL_048f;
		}
		if (L_94 == 1)
		{
			goto IL_04a7;
		}
		if (L_94 == 2)
		{
			goto IL_04bf;
		}
		if (L_94 == 3)
		{
			goto IL_04d7;
		}
		if (L_94 == 4)
		{
			goto IL_04ef;
		}
		if (L_94 == 5)
		{
			goto IL_0507;
		}
		if (L_94 == 6)
		{
			goto IL_051f;
		}
		if (L_94 == 7)
		{
			goto IL_0537;
		}
		if (L_94 == 8)
		{
			goto IL_054f;
		}
		if (L_94 == 9)
		{
			goto IL_0567;
		}
		if (L_94 == 10)
		{
			goto IL_057f;
		}
		if (L_94 == 11)
		{
			goto IL_0597;
		}
		if (L_94 == 12)
		{
			goto IL_05af;
		}
		if (L_94 == 13)
		{
			goto IL_05c7;
		}
		if (L_94 == 14)
		{
			goto IL_05df;
		}
		if (L_94 == 15)
		{
			goto IL_05f7;
		}
		if (L_94 == 16)
		{
			goto IL_060f;
		}
		if (L_94 == 17)
		{
			goto IL_0627;
		}
		if (L_94 == 18)
		{
			goto IL_0646;
		}
		if (L_94 == 19)
		{
			goto IL_0665;
		}
		if (L_94 == 20)
		{
			goto IL_0684;
		}
		if (L_94 == 21)
		{
			goto IL_06a3;
		}
		if (L_94 == 22)
		{
			goto IL_06c2;
		}
		if (L_94 == 23)
		{
			goto IL_06e1;
		}
		if (L_94 == 24)
		{
			goto IL_0700;
		}
		if (L_94 == 25)
		{
			goto IL_071f;
		}
		if (L_94 == 26)
		{
			goto IL_073e;
		}
		if (L_94 == 27)
		{
			goto IL_075d;
		}
		if (L_94 == 28)
		{
			goto IL_077c;
		}
		if (L_94 == 29)
		{
			goto IL_0797;
		}
		if (L_94 == 30)
		{
			goto IL_07b3;
		}
		if (L_94 == 31)
		{
			goto IL_07c0;
		}
		if (L_94 == 32)
		{
			goto IL_07df;
		}
		if (L_94 == 33)
		{
			goto IL_07fe;
		}
		if (L_94 == 34)
		{
			goto IL_081d;
		}
		if (L_94 == 35)
		{
			goto IL_083c;
		}
		if (L_94 == 36)
		{
			goto IL_085b;
		}
		if (L_94 == 37)
		{
			goto IL_087a;
		}
		if (L_94 == 38)
		{
			goto IL_0899;
		}
		if (L_94 == 39)
		{
			goto IL_08b8;
		}
		if (L_94 == 40)
		{
			goto IL_08d7;
		}
		if (L_94 == 41)
		{
			goto IL_08f6;
		}
		if (L_94 == 42)
		{
			goto IL_0915;
		}
		if (L_94 == 43)
		{
			goto IL_0934;
		}
		if (L_94 == 44)
		{
			goto IL_0953;
		}
		if (L_94 == 45)
		{
			goto IL_0972;
		}
		if (L_94 == 46)
		{
			goto IL_0991;
		}
		if (L_94 == 47)
		{
			goto IL_099e;
		}
		if (L_94 == 48)
		{
			goto IL_09c2;
		}
	}
	{
		goto IL_09e6;
	}

IL_048f:
	{
		Event_t3028476042 * L_95 = V_0;
		NullCheck(L_95);
		Event_set_character_m2479063678(L_95, ((int32_t)48), /*hidden argument*/NULL);
		Event_t3028476042 * L_96 = V_0;
		NullCheck(L_96);
		Event_set_keyCode_m1893308983(L_96, ((int32_t)256), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04a7:
	{
		Event_t3028476042 * L_97 = V_0;
		NullCheck(L_97);
		Event_set_character_m2479063678(L_97, ((int32_t)49), /*hidden argument*/NULL);
		Event_t3028476042 * L_98 = V_0;
		NullCheck(L_98);
		Event_set_keyCode_m1893308983(L_98, ((int32_t)257), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04bf:
	{
		Event_t3028476042 * L_99 = V_0;
		NullCheck(L_99);
		Event_set_character_m2479063678(L_99, ((int32_t)50), /*hidden argument*/NULL);
		Event_t3028476042 * L_100 = V_0;
		NullCheck(L_100);
		Event_set_keyCode_m1893308983(L_100, ((int32_t)258), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04d7:
	{
		Event_t3028476042 * L_101 = V_0;
		NullCheck(L_101);
		Event_set_character_m2479063678(L_101, ((int32_t)51), /*hidden argument*/NULL);
		Event_t3028476042 * L_102 = V_0;
		NullCheck(L_102);
		Event_set_keyCode_m1893308983(L_102, ((int32_t)259), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_04ef:
	{
		Event_t3028476042 * L_103 = V_0;
		NullCheck(L_103);
		Event_set_character_m2479063678(L_103, ((int32_t)52), /*hidden argument*/NULL);
		Event_t3028476042 * L_104 = V_0;
		NullCheck(L_104);
		Event_set_keyCode_m1893308983(L_104, ((int32_t)260), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0507:
	{
		Event_t3028476042 * L_105 = V_0;
		NullCheck(L_105);
		Event_set_character_m2479063678(L_105, ((int32_t)53), /*hidden argument*/NULL);
		Event_t3028476042 * L_106 = V_0;
		NullCheck(L_106);
		Event_set_keyCode_m1893308983(L_106, ((int32_t)261), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_051f:
	{
		Event_t3028476042 * L_107 = V_0;
		NullCheck(L_107);
		Event_set_character_m2479063678(L_107, ((int32_t)54), /*hidden argument*/NULL);
		Event_t3028476042 * L_108 = V_0;
		NullCheck(L_108);
		Event_set_keyCode_m1893308983(L_108, ((int32_t)262), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0537:
	{
		Event_t3028476042 * L_109 = V_0;
		NullCheck(L_109);
		Event_set_character_m2479063678(L_109, ((int32_t)55), /*hidden argument*/NULL);
		Event_t3028476042 * L_110 = V_0;
		NullCheck(L_110);
		Event_set_keyCode_m1893308983(L_110, ((int32_t)263), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_054f:
	{
		Event_t3028476042 * L_111 = V_0;
		NullCheck(L_111);
		Event_set_character_m2479063678(L_111, ((int32_t)56), /*hidden argument*/NULL);
		Event_t3028476042 * L_112 = V_0;
		NullCheck(L_112);
		Event_set_keyCode_m1893308983(L_112, ((int32_t)264), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0567:
	{
		Event_t3028476042 * L_113 = V_0;
		NullCheck(L_113);
		Event_set_character_m2479063678(L_113, ((int32_t)57), /*hidden argument*/NULL);
		Event_t3028476042 * L_114 = V_0;
		NullCheck(L_114);
		Event_set_keyCode_m1893308983(L_114, ((int32_t)265), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_057f:
	{
		Event_t3028476042 * L_115 = V_0;
		NullCheck(L_115);
		Event_set_character_m2479063678(L_115, ((int32_t)46), /*hidden argument*/NULL);
		Event_t3028476042 * L_116 = V_0;
		NullCheck(L_116);
		Event_set_keyCode_m1893308983(L_116, ((int32_t)266), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0597:
	{
		Event_t3028476042 * L_117 = V_0;
		NullCheck(L_117);
		Event_set_character_m2479063678(L_117, ((int32_t)47), /*hidden argument*/NULL);
		Event_t3028476042 * L_118 = V_0;
		NullCheck(L_118);
		Event_set_keyCode_m1893308983(L_118, ((int32_t)267), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05af:
	{
		Event_t3028476042 * L_119 = V_0;
		NullCheck(L_119);
		Event_set_character_m2479063678(L_119, ((int32_t)45), /*hidden argument*/NULL);
		Event_t3028476042 * L_120 = V_0;
		NullCheck(L_120);
		Event_set_keyCode_m1893308983(L_120, ((int32_t)269), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05c7:
	{
		Event_t3028476042 * L_121 = V_0;
		NullCheck(L_121);
		Event_set_character_m2479063678(L_121, ((int32_t)43), /*hidden argument*/NULL);
		Event_t3028476042 * L_122 = V_0;
		NullCheck(L_122);
		Event_set_keyCode_m1893308983(L_122, ((int32_t)270), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05df:
	{
		Event_t3028476042 * L_123 = V_0;
		NullCheck(L_123);
		Event_set_character_m2479063678(L_123, ((int32_t)61), /*hidden argument*/NULL);
		Event_t3028476042 * L_124 = V_0;
		NullCheck(L_124);
		Event_set_keyCode_m1893308983(L_124, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_05f7:
	{
		Event_t3028476042 * L_125 = V_0;
		NullCheck(L_125);
		Event_set_character_m2479063678(L_125, ((int32_t)61), /*hidden argument*/NULL);
		Event_t3028476042 * L_126 = V_0;
		NullCheck(L_126);
		Event_set_keyCode_m1893308983(L_126, ((int32_t)272), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_060f:
	{
		Event_t3028476042 * L_127 = V_0;
		NullCheck(L_127);
		Event_set_character_m2479063678(L_127, ((int32_t)10), /*hidden argument*/NULL);
		Event_t3028476042 * L_128 = V_0;
		NullCheck(L_128);
		Event_set_keyCode_m1893308983(L_128, ((int32_t)271), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0627:
	{
		Event_t3028476042 * L_129 = V_0;
		NullCheck(L_129);
		Event_set_keyCode_m1893308983(L_129, ((int32_t)273), /*hidden argument*/NULL);
		Event_t3028476042 * L_130 = V_0;
		Event_t3028476042 * L_131 = L_130;
		NullCheck(L_131);
		int32_t L_132 = Event_get_modifiers_m430092210(L_131, /*hidden argument*/NULL);
		NullCheck(L_131);
		Event_set_modifiers_m2565042639(L_131, ((int32_t)((int32_t)L_132|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0646:
	{
		Event_t3028476042 * L_133 = V_0;
		NullCheck(L_133);
		Event_set_keyCode_m1893308983(L_133, ((int32_t)274), /*hidden argument*/NULL);
		Event_t3028476042 * L_134 = V_0;
		Event_t3028476042 * L_135 = L_134;
		NullCheck(L_135);
		int32_t L_136 = Event_get_modifiers_m430092210(L_135, /*hidden argument*/NULL);
		NullCheck(L_135);
		Event_set_modifiers_m2565042639(L_135, ((int32_t)((int32_t)L_136|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0665:
	{
		Event_t3028476042 * L_137 = V_0;
		NullCheck(L_137);
		Event_set_keyCode_m1893308983(L_137, ((int32_t)276), /*hidden argument*/NULL);
		Event_t3028476042 * L_138 = V_0;
		Event_t3028476042 * L_139 = L_138;
		NullCheck(L_139);
		int32_t L_140 = Event_get_modifiers_m430092210(L_139, /*hidden argument*/NULL);
		NullCheck(L_139);
		Event_set_modifiers_m2565042639(L_139, ((int32_t)((int32_t)L_140|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0684:
	{
		Event_t3028476042 * L_141 = V_0;
		NullCheck(L_141);
		Event_set_keyCode_m1893308983(L_141, ((int32_t)275), /*hidden argument*/NULL);
		Event_t3028476042 * L_142 = V_0;
		Event_t3028476042 * L_143 = L_142;
		NullCheck(L_143);
		int32_t L_144 = Event_get_modifiers_m430092210(L_143, /*hidden argument*/NULL);
		NullCheck(L_143);
		Event_set_modifiers_m2565042639(L_143, ((int32_t)((int32_t)L_144|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_06a3:
	{
		Event_t3028476042 * L_145 = V_0;
		NullCheck(L_145);
		Event_set_keyCode_m1893308983(L_145, ((int32_t)277), /*hidden argument*/NULL);
		Event_t3028476042 * L_146 = V_0;
		Event_t3028476042 * L_147 = L_146;
		NullCheck(L_147);
		int32_t L_148 = Event_get_modifiers_m430092210(L_147, /*hidden argument*/NULL);
		NullCheck(L_147);
		Event_set_modifiers_m2565042639(L_147, ((int32_t)((int32_t)L_148|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_06c2:
	{
		Event_t3028476042 * L_149 = V_0;
		NullCheck(L_149);
		Event_set_keyCode_m1893308983(L_149, ((int32_t)278), /*hidden argument*/NULL);
		Event_t3028476042 * L_150 = V_0;
		Event_t3028476042 * L_151 = L_150;
		NullCheck(L_151);
		int32_t L_152 = Event_get_modifiers_m430092210(L_151, /*hidden argument*/NULL);
		NullCheck(L_151);
		Event_set_modifiers_m2565042639(L_151, ((int32_t)((int32_t)L_152|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_06e1:
	{
		Event_t3028476042 * L_153 = V_0;
		NullCheck(L_153);
		Event_set_keyCode_m1893308983(L_153, ((int32_t)279), /*hidden argument*/NULL);
		Event_t3028476042 * L_154 = V_0;
		Event_t3028476042 * L_155 = L_154;
		NullCheck(L_155);
		int32_t L_156 = Event_get_modifiers_m430092210(L_155, /*hidden argument*/NULL);
		NullCheck(L_155);
		Event_set_modifiers_m2565042639(L_155, ((int32_t)((int32_t)L_156|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0700:
	{
		Event_t3028476042 * L_157 = V_0;
		NullCheck(L_157);
		Event_set_keyCode_m1893308983(L_157, ((int32_t)281), /*hidden argument*/NULL);
		Event_t3028476042 * L_158 = V_0;
		Event_t3028476042 * L_159 = L_158;
		NullCheck(L_159);
		int32_t L_160 = Event_get_modifiers_m430092210(L_159, /*hidden argument*/NULL);
		NullCheck(L_159);
		Event_set_modifiers_m2565042639(L_159, ((int32_t)((int32_t)L_160|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_071f:
	{
		Event_t3028476042 * L_161 = V_0;
		NullCheck(L_161);
		Event_set_keyCode_m1893308983(L_161, ((int32_t)280), /*hidden argument*/NULL);
		Event_t3028476042 * L_162 = V_0;
		Event_t3028476042 * L_163 = L_162;
		NullCheck(L_163);
		int32_t L_164 = Event_get_modifiers_m430092210(L_163, /*hidden argument*/NULL);
		NullCheck(L_163);
		Event_set_modifiers_m2565042639(L_163, ((int32_t)((int32_t)L_164|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_073e:
	{
		Event_t3028476042 * L_165 = V_0;
		NullCheck(L_165);
		Event_set_keyCode_m1893308983(L_165, ((int32_t)280), /*hidden argument*/NULL);
		Event_t3028476042 * L_166 = V_0;
		Event_t3028476042 * L_167 = L_166;
		NullCheck(L_167);
		int32_t L_168 = Event_get_modifiers_m430092210(L_167, /*hidden argument*/NULL);
		NullCheck(L_167);
		Event_set_modifiers_m2565042639(L_167, ((int32_t)((int32_t)L_168|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_075d:
	{
		Event_t3028476042 * L_169 = V_0;
		NullCheck(L_169);
		Event_set_keyCode_m1893308983(L_169, ((int32_t)281), /*hidden argument*/NULL);
		Event_t3028476042 * L_170 = V_0;
		Event_t3028476042 * L_171 = L_170;
		NullCheck(L_171);
		int32_t L_172 = Event_get_modifiers_m430092210(L_171, /*hidden argument*/NULL);
		NullCheck(L_171);
		Event_set_modifiers_m2565042639(L_171, ((int32_t)((int32_t)L_172|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_077c:
	{
		Event_t3028476042 * L_173 = V_0;
		NullCheck(L_173);
		Event_set_keyCode_m1893308983(L_173, 8, /*hidden argument*/NULL);
		Event_t3028476042 * L_174 = V_0;
		Event_t3028476042 * L_175 = L_174;
		NullCheck(L_175);
		int32_t L_176 = Event_get_modifiers_m430092210(L_175, /*hidden argument*/NULL);
		NullCheck(L_175);
		Event_set_modifiers_m2565042639(L_175, ((int32_t)((int32_t)L_176|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0797:
	{
		Event_t3028476042 * L_177 = V_0;
		NullCheck(L_177);
		Event_set_keyCode_m1893308983(L_177, ((int32_t)127), /*hidden argument*/NULL);
		Event_t3028476042 * L_178 = V_0;
		Event_t3028476042 * L_179 = L_178;
		NullCheck(L_179);
		int32_t L_180 = Event_get_modifiers_m430092210(L_179, /*hidden argument*/NULL);
		NullCheck(L_179);
		Event_set_modifiers_m2565042639(L_179, ((int32_t)((int32_t)L_180|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07b3:
	{
		Event_t3028476042 * L_181 = V_0;
		NullCheck(L_181);
		Event_set_keyCode_m1893308983(L_181, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07c0:
	{
		Event_t3028476042 * L_182 = V_0;
		NullCheck(L_182);
		Event_set_keyCode_m1893308983(L_182, ((int32_t)282), /*hidden argument*/NULL);
		Event_t3028476042 * L_183 = V_0;
		Event_t3028476042 * L_184 = L_183;
		NullCheck(L_184);
		int32_t L_185 = Event_get_modifiers_m430092210(L_184, /*hidden argument*/NULL);
		NullCheck(L_184);
		Event_set_modifiers_m2565042639(L_184, ((int32_t)((int32_t)L_185|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07df:
	{
		Event_t3028476042 * L_186 = V_0;
		NullCheck(L_186);
		Event_set_keyCode_m1893308983(L_186, ((int32_t)283), /*hidden argument*/NULL);
		Event_t3028476042 * L_187 = V_0;
		Event_t3028476042 * L_188 = L_187;
		NullCheck(L_188);
		int32_t L_189 = Event_get_modifiers_m430092210(L_188, /*hidden argument*/NULL);
		NullCheck(L_188);
		Event_set_modifiers_m2565042639(L_188, ((int32_t)((int32_t)L_189|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_07fe:
	{
		Event_t3028476042 * L_190 = V_0;
		NullCheck(L_190);
		Event_set_keyCode_m1893308983(L_190, ((int32_t)284), /*hidden argument*/NULL);
		Event_t3028476042 * L_191 = V_0;
		Event_t3028476042 * L_192 = L_191;
		NullCheck(L_192);
		int32_t L_193 = Event_get_modifiers_m430092210(L_192, /*hidden argument*/NULL);
		NullCheck(L_192);
		Event_set_modifiers_m2565042639(L_192, ((int32_t)((int32_t)L_193|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_081d:
	{
		Event_t3028476042 * L_194 = V_0;
		NullCheck(L_194);
		Event_set_keyCode_m1893308983(L_194, ((int32_t)285), /*hidden argument*/NULL);
		Event_t3028476042 * L_195 = V_0;
		Event_t3028476042 * L_196 = L_195;
		NullCheck(L_196);
		int32_t L_197 = Event_get_modifiers_m430092210(L_196, /*hidden argument*/NULL);
		NullCheck(L_196);
		Event_set_modifiers_m2565042639(L_196, ((int32_t)((int32_t)L_197|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_083c:
	{
		Event_t3028476042 * L_198 = V_0;
		NullCheck(L_198);
		Event_set_keyCode_m1893308983(L_198, ((int32_t)286), /*hidden argument*/NULL);
		Event_t3028476042 * L_199 = V_0;
		Event_t3028476042 * L_200 = L_199;
		NullCheck(L_200);
		int32_t L_201 = Event_get_modifiers_m430092210(L_200, /*hidden argument*/NULL);
		NullCheck(L_200);
		Event_set_modifiers_m2565042639(L_200, ((int32_t)((int32_t)L_201|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_085b:
	{
		Event_t3028476042 * L_202 = V_0;
		NullCheck(L_202);
		Event_set_keyCode_m1893308983(L_202, ((int32_t)287), /*hidden argument*/NULL);
		Event_t3028476042 * L_203 = V_0;
		Event_t3028476042 * L_204 = L_203;
		NullCheck(L_204);
		int32_t L_205 = Event_get_modifiers_m430092210(L_204, /*hidden argument*/NULL);
		NullCheck(L_204);
		Event_set_modifiers_m2565042639(L_204, ((int32_t)((int32_t)L_205|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_087a:
	{
		Event_t3028476042 * L_206 = V_0;
		NullCheck(L_206);
		Event_set_keyCode_m1893308983(L_206, ((int32_t)288), /*hidden argument*/NULL);
		Event_t3028476042 * L_207 = V_0;
		Event_t3028476042 * L_208 = L_207;
		NullCheck(L_208);
		int32_t L_209 = Event_get_modifiers_m430092210(L_208, /*hidden argument*/NULL);
		NullCheck(L_208);
		Event_set_modifiers_m2565042639(L_208, ((int32_t)((int32_t)L_209|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0899:
	{
		Event_t3028476042 * L_210 = V_0;
		NullCheck(L_210);
		Event_set_keyCode_m1893308983(L_210, ((int32_t)289), /*hidden argument*/NULL);
		Event_t3028476042 * L_211 = V_0;
		Event_t3028476042 * L_212 = L_211;
		NullCheck(L_212);
		int32_t L_213 = Event_get_modifiers_m430092210(L_212, /*hidden argument*/NULL);
		NullCheck(L_212);
		Event_set_modifiers_m2565042639(L_212, ((int32_t)((int32_t)L_213|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_08b8:
	{
		Event_t3028476042 * L_214 = V_0;
		NullCheck(L_214);
		Event_set_keyCode_m1893308983(L_214, ((int32_t)290), /*hidden argument*/NULL);
		Event_t3028476042 * L_215 = V_0;
		Event_t3028476042 * L_216 = L_215;
		NullCheck(L_216);
		int32_t L_217 = Event_get_modifiers_m430092210(L_216, /*hidden argument*/NULL);
		NullCheck(L_216);
		Event_set_modifiers_m2565042639(L_216, ((int32_t)((int32_t)L_217|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_08d7:
	{
		Event_t3028476042 * L_218 = V_0;
		NullCheck(L_218);
		Event_set_keyCode_m1893308983(L_218, ((int32_t)291), /*hidden argument*/NULL);
		Event_t3028476042 * L_219 = V_0;
		Event_t3028476042 * L_220 = L_219;
		NullCheck(L_220);
		int32_t L_221 = Event_get_modifiers_m430092210(L_220, /*hidden argument*/NULL);
		NullCheck(L_220);
		Event_set_modifiers_m2565042639(L_220, ((int32_t)((int32_t)L_221|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_08f6:
	{
		Event_t3028476042 * L_222 = V_0;
		NullCheck(L_222);
		Event_set_keyCode_m1893308983(L_222, ((int32_t)292), /*hidden argument*/NULL);
		Event_t3028476042 * L_223 = V_0;
		Event_t3028476042 * L_224 = L_223;
		NullCheck(L_224);
		int32_t L_225 = Event_get_modifiers_m430092210(L_224, /*hidden argument*/NULL);
		NullCheck(L_224);
		Event_set_modifiers_m2565042639(L_224, ((int32_t)((int32_t)L_225|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0915:
	{
		Event_t3028476042 * L_226 = V_0;
		NullCheck(L_226);
		Event_set_keyCode_m1893308983(L_226, ((int32_t)293), /*hidden argument*/NULL);
		Event_t3028476042 * L_227 = V_0;
		Event_t3028476042 * L_228 = L_227;
		NullCheck(L_228);
		int32_t L_229 = Event_get_modifiers_m430092210(L_228, /*hidden argument*/NULL);
		NullCheck(L_228);
		Event_set_modifiers_m2565042639(L_228, ((int32_t)((int32_t)L_229|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0934:
	{
		Event_t3028476042 * L_230 = V_0;
		NullCheck(L_230);
		Event_set_keyCode_m1893308983(L_230, ((int32_t)294), /*hidden argument*/NULL);
		Event_t3028476042 * L_231 = V_0;
		Event_t3028476042 * L_232 = L_231;
		NullCheck(L_232);
		int32_t L_233 = Event_get_modifiers_m430092210(L_232, /*hidden argument*/NULL);
		NullCheck(L_232);
		Event_set_modifiers_m2565042639(L_232, ((int32_t)((int32_t)L_233|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0953:
	{
		Event_t3028476042 * L_234 = V_0;
		NullCheck(L_234);
		Event_set_keyCode_m1893308983(L_234, ((int32_t)295), /*hidden argument*/NULL);
		Event_t3028476042 * L_235 = V_0;
		Event_t3028476042 * L_236 = L_235;
		NullCheck(L_236);
		int32_t L_237 = Event_get_modifiers_m430092210(L_236, /*hidden argument*/NULL);
		NullCheck(L_236);
		Event_set_modifiers_m2565042639(L_236, ((int32_t)((int32_t)L_237|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0972:
	{
		Event_t3028476042 * L_238 = V_0;
		NullCheck(L_238);
		Event_set_keyCode_m1893308983(L_238, ((int32_t)296), /*hidden argument*/NULL);
		Event_t3028476042 * L_239 = V_0;
		Event_t3028476042 * L_240 = L_239;
		NullCheck(L_240);
		int32_t L_241 = Event_get_modifiers_m430092210(L_240, /*hidden argument*/NULL);
		NullCheck(L_240);
		Event_set_modifiers_m2565042639(L_240, ((int32_t)((int32_t)L_241|(int32_t)((int32_t)64))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_0991:
	{
		Event_t3028476042 * L_242 = V_0;
		NullCheck(L_242);
		Event_set_keyCode_m1893308983(L_242, ((int32_t)27), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_099e:
	{
		Event_t3028476042 * L_243 = V_0;
		NullCheck(L_243);
		Event_set_character_m2479063678(L_243, ((int32_t)10), /*hidden argument*/NULL);
		Event_t3028476042 * L_244 = V_0;
		NullCheck(L_244);
		Event_set_keyCode_m1893308983(L_244, ((int32_t)13), /*hidden argument*/NULL);
		Event_t3028476042 * L_245 = V_0;
		Event_t3028476042 * L_246 = L_245;
		NullCheck(L_246);
		int32_t L_247 = Event_get_modifiers_m430092210(L_246, /*hidden argument*/NULL);
		NullCheck(L_246);
		Event_set_modifiers_m2565042639(L_246, ((int32_t)((int32_t)L_247&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_09c2:
	{
		Event_t3028476042 * L_248 = V_0;
		NullCheck(L_248);
		Event_set_keyCode_m1893308983(L_248, ((int32_t)32), /*hidden argument*/NULL);
		Event_t3028476042 * L_249 = V_0;
		NullCheck(L_249);
		Event_set_character_m2479063678(L_249, ((int32_t)32), /*hidden argument*/NULL);
		Event_t3028476042 * L_250 = V_0;
		Event_t3028476042 * L_251 = L_250;
		NullCheck(L_251);
		int32_t L_252 = Event_get_modifiers_m430092210(L_251, /*hidden argument*/NULL);
		NullCheck(L_251);
		Event_set_modifiers_m2565042639(L_251, ((int32_t)((int32_t)L_252&(int32_t)((int32_t)-65))), /*hidden argument*/NULL);
		goto IL_0a6c;
	}

IL_09e6:
	{
		String_t* L_253 = V_3;
		NullCheck(L_253);
		int32_t L_254 = String_get_Length_m1606060069(L_253, /*hidden argument*/NULL);
		if ((((int32_t)L_254) == ((int32_t)1)))
		{
			goto IL_0a37;
		}
	}

IL_09f2:
	try
	{ // begin try (depth: 1)
		Event_t3028476042 * L_255 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_256 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(KeyCode_t2283395152_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_257 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_258 = Enum_Parse_m982704874(NULL /*static, unused*/, L_256, L_257, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_255);
		Event_set_keyCode_m1893308983(L_255, ((*(int32_t*)((int32_t*)UnBox (L_258, Int32_t2071877448_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		goto IL_0a32;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t3259014390_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0a13;
		throw e;
	}

CATCH_0a13:
	{ // begin catch(System.ArgumentException)
		ObjectU5BU5D_t3614634134* L_259 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_260 = V_3;
		NullCheck(L_259);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_259, 0);
		ArrayElementTypeCheck (L_259, L_260);
		(L_259)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_260);
		String_t* L_261 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral1755577857, L_259, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_261, /*hidden argument*/NULL);
		goto IL_0a32;
	} // end catch (depth: 1)

IL_0a32:
	{
		goto IL_0a67;
	}

IL_0a37:
	{
		Event_t3028476042 * L_262 = V_0;
		String_t* L_263 = V_3;
		NullCheck(L_263);
		String_t* L_264 = String_ToLower_m2994460523(L_263, /*hidden argument*/NULL);
		NullCheck(L_264);
		Il2CppChar L_265 = String_get_Chars_m4230566705(L_264, 0, /*hidden argument*/NULL);
		NullCheck(L_262);
		Event_set_character_m2479063678(L_262, L_265, /*hidden argument*/NULL);
		Event_t3028476042 * L_266 = V_0;
		Event_t3028476042 * L_267 = V_0;
		NullCheck(L_267);
		Il2CppChar L_268 = Event_get_character_m3740896233(L_267, /*hidden argument*/NULL);
		NullCheck(L_266);
		Event_set_keyCode_m1893308983(L_266, L_268, /*hidden argument*/NULL);
		Event_t3028476042 * L_269 = V_0;
		NullCheck(L_269);
		int32_t L_270 = Event_get_modifiers_m430092210(L_269, /*hidden argument*/NULL);
		if (!L_270)
		{
			goto IL_0a67;
		}
	}
	{
		Event_t3028476042 * L_271 = V_0;
		NullCheck(L_271);
		Event_set_character_m2479063678(L_271, 0, /*hidden argument*/NULL);
	}

IL_0a67:
	{
		goto IL_0a6c;
	}

IL_0a6c:
	{
		Event_t3028476042 * L_272 = V_0;
		return L_272;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C"  int32_t Event_GetHashCode_m2214628668 (Event_t3028476042 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m1145918225(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m3364681288(__this, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint16_t)L_1)));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m569219555(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t2243707579  L_3 = Event_get_mousePosition_m3789571399(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m2353429373((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern Il2CppClass* Event_t3028476042_il2cpp_TypeInfo_var;
extern const uint32_t Event_Equals_m57747812_MetadataUsageId;
extern "C"  bool Event_Equals_m57747812 (Event_t3028476042 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_Equals_m57747812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Event_t3028476042 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return (bool)1;
	}

IL_0016:
	{
		Il2CppObject * L_3 = ___obj0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m191970594(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m191970594(__this, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)0;
	}

IL_0029:
	{
		Il2CppObject * L_6 = ___obj0;
		V_0 = ((Event_t3028476042 *)CastclassSealed(L_6, Event_t3028476042_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		Event_t3028476042 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m2426033198(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		Event_t3028476042 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m430092210(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_005a;
		}
	}

IL_0058:
	{
		return (bool)0;
	}

IL_005a:
	{
		bool L_13 = Event_get_isKey_m1145918225(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m3364681288(__this, /*hidden argument*/NULL);
		Event_t3028476042 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m3364681288(L_15, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
	}

IL_0074:
	{
		bool L_17 = Event_get_isMouse_m569219555(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t2243707579  L_18 = Event_get_mousePosition_m3789571399(__this, /*hidden argument*/NULL);
		Event_t3028476042 * L_19 = V_0;
		NullCheck(L_19);
		Vector2_t2243707579  L_20 = Event_get_mousePosition_m3789571399(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m4168854394(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0091:
	{
		return (bool)0;
	}
}
// System.String UnityEngine.Event::ToString()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* EventType_t3919834026_il2cpp_TypeInfo_var;
extern Il2CppClass* EventModifiers_t2690251474_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyCode_t2283395152_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3843465330;
extern Il2CppCodeGenString* _stringLiteral4288185556;
extern Il2CppCodeGenString* _stringLiteral1689989475;
extern Il2CppCodeGenString* _stringLiteral4122007790;
extern Il2CppCodeGenString* _stringLiteral3835881582;
extern Il2CppCodeGenString* _stringLiteral991452214;
extern Il2CppCodeGenString* _stringLiteral858294771;
extern const uint32_t Event_ToString_m2977598998_MetadataUsageId;
extern "C"  String_t* Event_ToString_m2977598998 (Event_t3028476042 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Event_ToString_m2977598998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m1145918225(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00b5;
		}
	}
	{
		Il2CppChar L_1 = Event_get_character_m3740896233(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_2 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_3 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		ObjectU5BU5D_t3614634134* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(EventModifiers_t2690251474_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m3364681288(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(KeyCode_t2283395152_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		String_t* L_14 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral3843465330, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t3614634134* L_15 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral4288185556);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral4288185556);
		ObjectU5BU5D_t3614634134* L_16 = L_15;
		int32_t L_17 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_19);
		ObjectU5BU5D_t3614634134* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral1689989475);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1689989475);
		ObjectU5BU5D_t3614634134* L_21 = L_20;
		Il2CppChar L_22 = Event_get_character_m3740896233(__this, /*hidden argument*/NULL);
		int32_t L_23 = ((int32_t)L_22);
		Il2CppObject * L_24 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_24);
		ObjectU5BU5D_t3614634134* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral4122007790);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral4122007790);
		ObjectU5BU5D_t3614634134* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(EventModifiers_t2690251474_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_29);
		ObjectU5BU5D_t3614634134* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral3835881582);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral3835881582);
		ObjectU5BU5D_t3614634134* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m3364681288(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(KeyCode_t2283395152_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m3881798623(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		return L_35;
	}

IL_00b5:
	{
		bool L_36 = Event_get_isMouse_m569219555(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00fb;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_37 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_38 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 0);
		ArrayElementTypeCheck (L_37, L_40);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_40);
		ObjectU5BU5D_t3614634134* L_41 = L_37;
		Vector2_t2243707579  L_42 = Event_get_mousePosition_m3789571399(__this, /*hidden argument*/NULL);
		Vector2_t2243707579  L_43 = L_42;
		Il2CppObject * L_44 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		ArrayElementTypeCheck (L_41, L_44);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t3614634134* L_45 = L_41;
		int32_t L_46 = Event_get_modifiers_m430092210(__this, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(EventModifiers_t2690251474_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 2);
		ArrayElementTypeCheck (L_45, L_48);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_48);
		String_t* L_49 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral991452214, L_45, /*hidden argument*/NULL);
		return L_49;
	}

IL_00fb:
	{
		int32_t L_50 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)((int32_t)14))))
		{
			goto IL_0115;
		}
	}
	{
		int32_t L_51 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_013d;
		}
	}

IL_0115:
	{
		ObjectU5BU5D_t3614634134* L_52 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_53 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		int32_t L_54 = L_53;
		Il2CppObject * L_55 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_55);
		ObjectU5BU5D_t3614634134* L_56 = L_52;
		String_t* L_57 = Event_get_commandName_m4234691381(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_57);
		String_t* L_58 = UnityString_Format_m2949645127(NULL /*static, unused*/, _stringLiteral858294771, L_56, /*hidden argument*/NULL);
		return L_58;
	}

IL_013d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_59 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_60 = Event_get_type_m2426033198(__this, /*hidden argument*/NULL);
		int32_t L_61 = L_60;
		Il2CppObject * L_62 = Box(EventType_t3919834026_il2cpp_TypeInfo_var, &L_61);
		String_t* L_63 = String_Concat_m56707527(NULL /*static, unused*/, L_59, L_62, /*hidden argument*/NULL);
		return L_63;
	}
}
// System.Void UnityEngine.Event::Init(System.Int32)
extern "C"  void Event_Init_m3901382626 (Event_t3028476042 * __this, int32_t ___displayIndex0, const MethodInfo* method)
{
	typedef void (*Event_Init_m3901382626_ftn) (Event_t3028476042 *, int32_t);
	static Event_Init_m3901382626_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m3901382626_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init(System.Int32)");
	_il2cpp_icall_func(__this, ___displayIndex0);
}
// System.Void UnityEngine.Event::Cleanup()
extern "C"  void Event_Cleanup_m1195902101 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m1195902101_ftn) (Event_t3028476042 *);
	static Event_Cleanup_m1195902101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m1195902101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C"  int32_t Event_get_rawType_m1373640154 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m1373640154_ftn) (Event_t3028476042 *);
	static Event_get_rawType_m1373640154_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m1373640154_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C"  int32_t Event_get_type_m2426033198 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m2426033198_ftn) (Event_t3028476042 *);
	static Event_get_type_m2426033198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m2426033198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
extern "C"  void Event_set_type_m4059414571 (Event_t3028476042 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_type_m4059414571_ftn) (Event_t3028476042 *, int32_t);
	static Event_set_type_m4059414571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_type_m4059414571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_type(UnityEngine.EventType)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
extern "C"  int32_t Event_GetTypeForControl_m3906355766 (Event_t3028476042 * __this, int32_t ___controlID0, const MethodInfo* method)
{
	typedef int32_t (*Event_GetTypeForControl_m3906355766_ftn) (Event_t3028476042 *, int32_t);
	static Event_GetTypeForControl_m3906355766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_GetTypeForControl_m3906355766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::GetTypeForControl(System.Int32)");
	return _il2cpp_icall_func(__this, ___controlID0);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C"  void Event_Internal_GetMousePosition_m38523489 (Event_t3028476042 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m38523489_ftn) (Event_t3028476042 *, Vector2_t2243707579 *);
	static Event_Internal_GetMousePosition_m38523489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m38523489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
extern "C"  void Event_Internal_GetMouseDelta_m2840326024 (Event_t3028476042 * __this, Vector2_t2243707579 * ___value0, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMouseDelta_m2840326024_ftn) (Event_t3028476042 *, Vector2_t2243707579 *);
	static Event_Internal_GetMouseDelta_m2840326024_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMouseDelta_m2840326024_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C"  int32_t Event_get_modifiers_m430092210 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m430092210_ftn) (Event_t3028476042 *);
	static Event_get_modifiers_m430092210_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m430092210_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
extern "C"  void Event_set_modifiers_m2565042639 (Event_t3028476042 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_modifiers_m2565042639_ftn) (Event_t3028476042 *, int32_t);
	static Event_set_modifiers_m2565042639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_modifiers_m2565042639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Event::get_clickCount()
extern "C"  int32_t Event_get_clickCount_m2139778827 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_clickCount_m2139778827_ftn) (Event_t3028476042 *);
	static Event_get_clickCount_m2139778827_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_clickCount_m2139778827_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_clickCount()");
	return _il2cpp_icall_func(__this);
}
// System.Char UnityEngine.Event::get_character()
extern "C"  Il2CppChar Event_get_character_m3740896233 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef Il2CppChar (*Event_get_character_m3740896233_ftn) (Event_t3028476042 *);
	static Event_get_character_m3740896233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m3740896233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_character(System.Char)
extern "C"  void Event_set_character_m2479063678 (Event_t3028476042 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_character_m2479063678_ftn) (Event_t3028476042 *, Il2CppChar);
	static Event_set_character_m2479063678_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_character_m2479063678_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_character(System.Char)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.String UnityEngine.Event::get_commandName()
extern "C"  String_t* Event_get_commandName_m4234691381 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m4234691381_ftn) (Event_t3028476042 *);
	static Event_get_commandName_m4234691381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m4234691381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C"  int32_t Event_get_keyCode_m3364681288 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m3364681288_ftn) (Event_t3028476042 *);
	static Event_get_keyCode_m3364681288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m3364681288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
extern "C"  void Event_set_keyCode_m1893308983 (Event_t3028476042 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_keyCode_m1893308983_ftn) (Event_t3028476042 *, int32_t);
	static Event_set_keyCode_m1893308983_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_keyCode_m1893308983_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C"  void Event_Internal_SetNativeEvent_m1899228752 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m1899228752_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m1899228752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m1899228752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr0);
}
// System.Void UnityEngine.Event::set_displayIndex(System.Int32)
extern "C"  void Event_set_displayIndex_m3631666901 (Event_t3028476042 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_displayIndex_m3631666901_ftn) (Event_t3028476042 *, int32_t);
	static Event_set_displayIndex_m3631666901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_displayIndex_m3631666901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_displayIndex(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Event::Use()
extern "C"  void Event_Use_m3575594482 (Event_t3028476042 * __this, const MethodInfo* method)
{
	typedef void (*Event_Use_m3575594482_ftn) (Event_t3028476042 *);
	static Event_Use_m3575594482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Use_m3575594482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Use()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C"  bool Event_PopEvent_m1952528237 (Il2CppObject * __this /* static, unused */, Event_t3028476042 * ___outEvent0, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m1952528237_ftn) (Event_t3028476042 *);
	static Event_PopEvent_m1952528237_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m1952528237_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent0);
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t3028476042_marshal_pinvoke(const Event_t3028476042& unmarshaled, Event_t3028476042_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Event_t3028476042_marshal_pinvoke_back(const Event_t3028476042_marshaled_pinvoke& marshaled, Event_t3028476042& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t3028476042_marshal_pinvoke_cleanup(Event_t3028476042_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t3028476042_marshal_com(const Event_t3028476042& unmarshaled, Event_t3028476042_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Event_t3028476042_marshal_com_back(const Event_t3028476042_marshaled_com& marshaled, Event_t3028476042& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)marshaled.___m_Ptr_0));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t3028476042_marshal_com_cleanup(Event_t3028476042_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m484215967 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t1021602117 * ArgumentCache_get_unityObjectArgument_m4277835372 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = __this->get_m_ObjectArgument_0();
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m1563669919 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		return L_0;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m74719732 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		return L_0;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m1351089439 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_FloatArgument_3();
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m3051619173 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m602177467 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		return L_0;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3774245231;
extern Il2CppCodeGenString* _stringLiteral1256080173;
extern Il2CppCodeGenString* _stringLiteral1653664622;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m1302102306_MetadataUsageId;
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m1302102306 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m1302102306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m4251815737(L_2, _stringLiteral3774245231, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m4290821911(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0037:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m4251815737(L_8, _stringLiteral1256080173, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m4290821911(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_0057:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m4251815737(L_14, _stringLiteral1653664622, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m4290821911(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_0077:
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_0083;
		}
	}
	{
		return;
	}

IL_0083:
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m12482732(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m2819142469 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1302102306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m3936525937 (ArgumentCache_t4810721 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1302102306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m1107507914 (BaseInvokableCall_t2229564840 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* ArgumentNullException_t628810857_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1081251833;
extern Il2CppCodeGenString* _stringLiteral878805882;
extern const uint32_t BaseInvokableCall__ctor_m2877580597_MetadataUsageId;
extern "C"  void BaseInvokableCall__ctor_m2877580597 (BaseInvokableCall_t2229564840 * __this, Il2CppObject * ___target0, MethodInfo_t * ___function1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m2877580597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t628810857 * L_1 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_1, _stringLiteral1081251833, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t628810857 * L_3 = (ArgumentNullException_t628810857 *)il2cpp_codegen_object_new(ArgumentNullException_t628810857_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3380712306(L_3, _stringLiteral878805882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t BaseInvokableCall_AllowInvoke_m88556325_MetadataUsageId;
extern "C"  bool BaseInvokableCall_AllowInvoke_m88556325 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * ___delegate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m88556325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Object_t1021602117 * V_1 = NULL;
	{
		Delegate_t3022476291 * L_0 = ___delegate0;
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)1;
	}

IL_000f:
	{
		Il2CppObject * L_3 = V_0;
		V_1 = ((Object_t1021602117 *)IsInstClass(L_3, Object_t1021602117_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_4 = V_1;
		bool L_5 = Object_ReferenceEquals_m3900584722(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_002a;
		}
	}
	{
		Object_t1021602117 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		return L_7;
	}

IL_002a:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const Il2CppType* UnityAction_t4025899511_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m1854031676_MetadataUsageId;
extern "C"  void InvokableCall__ctor_m1854031676 (InvokableCall_t2183506063 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m1854031676_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m2877580597(__this, L_0, L_1, /*hidden argument*/NULL);
		UnityAction_t4025899511 * L_2 = __this->get_Delegate_0();
		MethodInfo_t * L_3 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(UnityAction_t4025899511_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_5 = ___target0;
		Delegate_t3022476291 * L_6 = NetFxCoreExtensions_CreateDelegate_m2492743074(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		Delegate_t3022476291 * L_7 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_2, ((UnityAction_t4025899511 *)CastclassSealed(L_6, UnityAction_t4025899511_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_t4025899511 *)CastclassSealed(L_7, UnityAction_t4025899511_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern Il2CppClass* UnityAction_t4025899511_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m738302370_MetadataUsageId;
extern "C"  void InvokableCall__ctor_m738302370 (InvokableCall_t2183506063 * __this, UnityAction_t4025899511 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m738302370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseInvokableCall__ctor_m1107507914(__this, /*hidden argument*/NULL);
		UnityAction_t4025899511 * L_0 = __this->get_Delegate_0();
		UnityAction_t4025899511 * L_1 = ___action0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_Delegate_0(((UnityAction_t4025899511 *)CastclassSealed(L_2, UnityAction_t4025899511_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m250936937 (InvokableCall_t2183506063 * __this, ObjectU5BU5D_t3614634134* ___args0, const MethodInfo* method)
{
	{
		UnityAction_t4025899511 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m88556325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UnityAction_t4025899511 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m3703026869(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_Find_m932898921 (InvokableCall_t2183506063 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_t4025899511 * L_0 = __this->get_Delegate_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953(L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_t4025899511 * L_3 = __this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		G_B3_0 = ((((Il2CppObject*)(MethodInfo_t *)L_4) == ((Il2CppObject*)(MethodInfo_t *)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern Il2CppClass* List_1_t1598685972_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m735894212_MethodInfo_var;
extern const uint32_t InvokableCallList__ctor_m1209277579_MetadataUsageId;
extern "C"  void InvokableCallList__ctor_m1209277579 (InvokableCallList_t2295673753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m1209277579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1598685972 * L_0 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_0, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t1598685972 * L_1 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_1, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t1598685972 * L_2 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_2, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo* List_1_Add_m3015244976_MethodInfo_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m1312060710_MetadataUsageId;
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m1312060710 (InvokableCallList_t2295673753 * __this, BaseInvokableCall_t2229564840 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m1312060710_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1598685972 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t2229564840 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m3015244976(L_0, L_1, /*hidden argument*/List_1_Add_m3015244976_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo* List_1_Add_m3015244976_MethodInfo_var;
extern const uint32_t InvokableCallList_AddListener_m786891050_MetadataUsageId;
extern "C"  void InvokableCallList_AddListener_m786891050 (InvokableCallList_t2295673753 * __this, BaseInvokableCall_t2229564840 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddListener_m786891050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1598685972 * L_0 = __this->get_m_RuntimeCalls_1();
		BaseInvokableCall_t2229564840 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m3015244976(L_0, L_1, /*hidden argument*/List_1_Add_m3015244976_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* List_1_t1598685972_il2cpp_TypeInfo_var;
extern Il2CppClass* Predicate_1_t672534955_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m735894212_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1328165365_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3015244976_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2851893812_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m2431180149_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m3244229183_MethodInfo_var;
extern const uint32_t InvokableCallList_RemoveListener_m3776101648_MetadataUsageId;
extern "C"  void InvokableCallList_RemoveListener_m3776101648 (InvokableCallList_t2295673753 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_RemoveListener_m3776101648_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1598685972 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t1598685972 * L_0 = (List_1_t1598685972 *)il2cpp_codegen_object_new(List_1_t1598685972_il2cpp_TypeInfo_var);
		List_1__ctor_m735894212(L_0, /*hidden argument*/List_1__ctor_m735894212_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		List_1_t1598685972 * L_1 = __this->get_m_RuntimeCalls_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t2229564840 * L_3 = List_1_get_Item_m1328165365(L_1, L_2, /*hidden argument*/List_1_get_Item_m1328165365_MethodInfo_var);
		Il2CppObject * L_4 = ___targetObj0;
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_3);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t1598685972 * L_7 = V_0;
		List_1_t1598685972 * L_8 = __this->get_m_RuntimeCalls_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t2229564840 * L_10 = List_1_get_Item_m1328165365(L_8, L_9, /*hidden argument*/List_1_get_Item_m1328165365_MethodInfo_var);
		NullCheck(L_7);
		List_1_Add_m3015244976(L_7, L_10, /*hidden argument*/List_1_Add_m3015244976_MethodInfo_var);
	}

IL_0037:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_12 = V_1;
		List_1_t1598685972 * L_13 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m2851893812(L_13, /*hidden argument*/List_1_get_Count_m2851893812_MethodInfo_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1598685972 * L_15 = __this->get_m_RuntimeCalls_1();
		List_1_t1598685972 * L_16 = V_0;
		List_1_t1598685972 * L_17 = L_16;
		IntPtr_t L_18;
		L_18.set_m_value_0((void*)(void*)GetVirtualMethodInfo(L_17, 24));
		Predicate_1_t672534955 * L_19 = (Predicate_1_t672534955 *)il2cpp_codegen_object_new(Predicate_1_t672534955_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2431180149(L_19, L_17, L_18, /*hidden argument*/Predicate_1__ctor_m2431180149_MethodInfo_var);
		NullCheck(L_15);
		List_1_RemoveAll_m3244229183(L_15, L_19, /*hidden argument*/List_1_RemoveAll_m3244229183_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo* List_1_Clear_m649507351_MethodInfo_var;
extern const uint32_t InvokableCallList_ClearPersistent_m3606847843_MetadataUsageId;
extern "C"  void InvokableCallList_ClearPersistent_m3606847843 (InvokableCallList_t2295673753 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m3606847843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1598685972 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m649507351(L_0, /*hidden argument*/List_1_Clear_m649507351_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo* List_1_Clear_m649507351_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1709189726_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1328165365_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2851893812_MethodInfo_var;
extern const uint32_t InvokableCallList_Invoke_m1839244635_MetadataUsageId;
extern "C"  void InvokableCallList_Invoke_m1839244635 (InvokableCallList_t2295673753 * __this, ObjectU5BU5D_t3614634134* ___parameters0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_Invoke_m1839244635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		List_1_t1598685972 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m649507351(L_1, /*hidden argument*/List_1_Clear_m649507351_MethodInfo_var);
		List_1_t1598685972 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t1598685972 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m1709189726(L_2, L_3, /*hidden argument*/List_1_AddRange_m1709189726_MethodInfo_var);
		List_1_t1598685972 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t1598685972 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m1709189726(L_4, L_5, /*hidden argument*/List_1_AddRange_m1709189726_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_003f:
	{
		V_0 = 0;
		goto IL_005c;
	}

IL_0046:
	{
		List_1_t1598685972 * L_6 = __this->get_m_ExecutingCalls_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		BaseInvokableCall_t2229564840 * L_8 = List_1_get_Item_m1328165365(L_6, L_7, /*hidden argument*/List_1_get_Item_m1328165365_MethodInfo_var);
		ObjectU5BU5D_t3614634134* L_9 = ___parameters0;
		NullCheck(L_8);
		VirtActionInvoker1< ObjectU5BU5D_t3614634134* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_11 = V_0;
		List_1_t1598685972 * L_12 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m2851893812(L_12, /*hidden argument*/List_1_get_Count_m2851893812_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern Il2CppClass* ArgumentCache_t4810721_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m2079279187_MetadataUsageId;
extern "C"  void PersistentCall__ctor_m2079279187 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m2079279187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ArgumentCache_t4810721 * L_0 = (ArgumentCache_t4810721 *)il2cpp_codegen_object_new(ArgumentCache_t4810721_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m484215967(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t1021602117 * PersistentCall_get_target_m380082118 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	{
		Object_t1021602117 * L_0 = __this->get_m_Target_0();
		return L_0;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m2279595219 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		return L_0;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m775523786 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_Mode_2();
		return L_0;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t4810721 * PersistentCall_get_arguments_m3775191318 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_t4810721 * L_0 = __this->get_m_Arguments_3();
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_IsValid_m3478768035_MetadataUsageId;
extern "C"  bool PersistentCall_IsValid_m3478768035 (PersistentCall_t3793436469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m3478768035_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Object_t1021602117 * L_0 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m2279595219(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return (bool)G_B3_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern Il2CppClass* CachedInvokableCall_1_t870059823_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t865427339_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t822770124_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t2619124609_il2cpp_TypeInfo_var;
extern Il2CppClass* InvokableCall_t2183506063_il2cpp_TypeInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m3238306320_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m127496184_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m702509745_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m2563320212_MethodInfo_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m1957889272_MetadataUsageId;
extern "C"  BaseInvokableCall_t2229564840 * PersistentCall_GetRuntimeCall_m1957889272 (PersistentCall_t3793436469 * __this, UnityEventBase_t828812576 * ___theEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m1957889272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		UnityEventBase_t828812576 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (BaseInvokableCall_t2229564840 *)NULL;
	}

IL_0013:
	{
		UnityEventBase_t828812576 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m3336182189(L_2, __this, /*hidden argument*/NULL);
		V_0 = L_3;
		MethodInfo_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0023;
		}
	}
	{
		return (BaseInvokableCall_t2229564840 *)NULL;
	}

IL_0023:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6 == 0)
		{
			goto IL_0051;
		}
		if (L_6 == 1)
		{
			goto IL_00d2;
		}
		if (L_6 == 2)
		{
			goto IL_005f;
		}
		if (L_6 == 3)
		{
			goto IL_008a;
		}
		if (L_6 == 4)
		{
			goto IL_0072;
		}
		if (L_6 == 5)
		{
			goto IL_00a2;
		}
		if (L_6 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		UnityEventBase_t828812576 * L_7 = ___theEvent0;
		Object_t1021602117 * L_8 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_0;
		NullCheck(L_7);
		BaseInvokableCall_t2229564840 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t2229564840 *, Il2CppObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		return L_10;
	}

IL_005f:
	{
		Object_t1021602117 * L_11 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_0;
		ArgumentCache_t4810721 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t2229564840 * L_14 = PersistentCall_GetObjectCall_m4123094223(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_0072:
	{
		Object_t1021602117 * L_15 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_0;
		ArgumentCache_t4810721 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m1351089439(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t870059823 * L_19 = (CachedInvokableCall_1_t870059823 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t870059823_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3238306320(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m3238306320_MethodInfo_var);
		return L_19;
	}

IL_008a:
	{
		Object_t1021602117 * L_20 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_0;
		ArgumentCache_t4810721 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m74719732(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t865427339 * L_24 = (CachedInvokableCall_1_t865427339 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t865427339_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m127496184(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m127496184_MethodInfo_var);
		return L_24;
	}

IL_00a2:
	{
		Object_t1021602117 * L_25 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_0;
		ArgumentCache_t4810721 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m3051619173(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t822770124 * L_29 = (CachedInvokableCall_1_t822770124 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t822770124_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m702509745(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m702509745_MethodInfo_var);
		return L_29;
	}

IL_00ba:
	{
		Object_t1021602117 * L_30 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_0;
		ArgumentCache_t4810721 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m602177467(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t2619124609 * L_34 = (CachedInvokableCall_1_t2619124609 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t2619124609_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2563320212(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m2563320212_MethodInfo_var);
		return L_34;
	}

IL_00d2:
	{
		Object_t1021602117 * L_35 = PersistentCall_get_target_m380082118(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_0;
		InvokableCall_t2183506063 * L_37 = (InvokableCall_t2183506063 *)il2cpp_codegen_object_new(InvokableCall_t2183506063_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m1854031676(L_37, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}

IL_00df:
	{
		return (BaseInvokableCall_t2229564840 *)NULL;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const Il2CppType* Object_t1021602117_0_0_0_var;
extern const Il2CppType* CachedInvokableCall_1_t224769006_0_0_0_var;
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* BaseInvokableCall_t2229564840_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m4123094223_MetadataUsageId;
extern "C"  BaseInvokableCall_t2229564840 * PersistentCall_GetObjectCall_m4123094223 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t4810721 * ___arguments2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m4123094223_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t2851816542 * V_3 = NULL;
	Object_t1021602117 * V_4 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Object_t1021602117_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t4810721 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m1563669919(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentCache_t4810721 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m1563669919(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m402049910, L_5, (bool)0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Object_t1021602117_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t224769006_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t1664964607* L_11 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t1664964607* >::Invoke(82 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t1664964607* L_15 = ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(Object_t1021602117_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t1664964607* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t1664964607* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t2851816542 * L_21 = Type_GetConstructor_m132234455(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t4810721 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t1021602117 * L_23 = ArgumentCache_get_unityObjectArgument_m4277835372(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t1021602117 * L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_24, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00aa;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t1021602117 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m191970594(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (Object_t1021602117 *)NULL;
	}

IL_00aa:
	{
		ConstructorInfo_t2851816542 * L_30 = V_3;
		ObjectU5BU5D_t3614634134* L_31 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t1021602117 * L_32 = ___target0;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 0);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_32);
		ObjectU5BU5D_t3614634134* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 1);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_34);
		ObjectU5BU5D_t3614634134* L_35 = L_33;
		Object_t1021602117 * L_36 = V_4;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 2);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_36);
		NullCheck(L_30);
		Il2CppObject * L_37 = ConstructorInfo_Invoke_m2144827141(L_30, L_35, /*hidden argument*/NULL);
		return ((BaseInvokableCall_t2229564840 *)IsInstClass(L_37, BaseInvokableCall_t2229564840_il2cpp_TypeInfo_var));
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern Il2CppClass* List_1_t3162557601_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m477348949_MethodInfo_var;
extern const uint32_t PersistentCallGroup__ctor_m3217457172_MetadataUsageId;
extern "C"  void PersistentCallGroup__ctor_m3217457172 (PersistentCallGroup_t339478082 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m3217457172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t3162557601 * L_0 = (List_1_t3162557601 *)il2cpp_codegen_object_new(List_1_t3162557601_il2cpp_TypeInfo_var);
		List_1__ctor_m477348949(L_0, /*hidden argument*/List_1__ctor_m477348949_MethodInfo_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern Il2CppClass* Enumerator_t2697287275_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3968995744_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2948343100_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2913526710_MethodInfo_var;
extern const uint32_t PersistentCallGroup_Initialize_m3908067069_MetadataUsageId;
extern "C"  void PersistentCallGroup_Initialize_m3908067069 (PersistentCallGroup_t339478082 * __this, InvokableCallList_t2295673753 * ___invokableList0, UnityEventBase_t828812576 * ___unityEventBase1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m3908067069_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PersistentCall_t3793436469 * V_0 = NULL;
	Enumerator_t2697287275  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t2229564840 * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3162557601 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t2697287275  L_1 = List_1_GetEnumerator_m3968995744(L_0, /*hidden argument*/List_1_GetEnumerator_m3968995744_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			PersistentCall_t3793436469 * L_2 = Enumerator_get_Current_m2948343100((&V_1), /*hidden argument*/Enumerator_get_Current_m2948343100_MethodInfo_var);
			V_0 = L_2;
			PersistentCall_t3793436469 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m3478768035(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			PersistentCall_t3793436469 * L_5 = V_0;
			UnityEventBase_t828812576 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t2229564840 * L_7 = PersistentCall_GetRuntimeCall_m1957889272(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t2229564840 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			InvokableCallList_t2295673753 * L_9 = ___invokableList0;
			BaseInvokableCall_t2229564840 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m1312060710(L_9, L_10, /*hidden argument*/NULL);
		}

IL_003e:
		{
			bool L_11 = Enumerator_MoveNext_m2913526710((&V_1), /*hidden argument*/Enumerator_MoveNext_m2913526710_MethodInfo_var);
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		Enumerator_t2697287275  L_12 = V_1;
		Enumerator_t2697287275  L_13 = L_12;
		Il2CppObject * L_14 = Box(Enumerator_t2697287275_il2cpp_TypeInfo_var, &L_13);
		NullCheck((Il2CppObject *)L_14);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_14);
		IL2CPP_END_FINALLY(79)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2649891629 (UnityAction_t4025899511 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m3703026869 (UnityAction_t4025899511 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m3703026869((UnityAction_t4025899511 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t4025899511 (UnityAction_t4025899511 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_BeginInvoke_m407736228 (UnityAction_t4025899511 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m2252669283 (UnityAction_t4025899511 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent__ctor_m588741179_MetadataUsageId;
extern "C"  void UnityEvent__ctor_m588741179 (UnityEvent_t408735097 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent__ctor_m588741179_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)0)));
		UnityEventBase__ctor_m4062111756(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m1596810379 (UnityEvent_t408735097 * __this, UnityAction_t4025899511 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_t4025899511 * L_0 = ___call0;
		BaseInvokableCall_t2229564840 * L_1 = UnityEvent_GetDelegate_m2675843778(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m1842680419(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_RemoveListener_m2073859718 (UnityEvent_t408735097 * __this, UnityAction_t4025899511 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_t4025899511 * L_0 = ___call0;
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m896795953(L_0, /*hidden argument*/NULL);
		UnityAction_t4025899511 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m2715372889(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		UnityEventBase_RemoveListener_m2645959491(__this, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern Il2CppClass* TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_FindMethod_Impl_m3303953986_MetadataUsageId;
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m3303953986 (UnityEvent_t408735097 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m3303953986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m1834951552(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t1664964607*)SZArrayNew(TypeU5BU5D_t1664964607_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern Il2CppClass* InvokableCall_t2183506063_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_GetDelegate_m2580105876_MetadataUsageId;
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_GetDelegate_m2580105876 (UnityEvent_t408735097 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m2580105876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t2183506063 * L_2 = (InvokableCall_t2183506063 *)il2cpp_codegen_object_new(InvokableCall_t2183506063_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m1854031676(L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern Il2CppClass* InvokableCall_t2183506063_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_GetDelegate_m2675843778_MetadataUsageId;
extern "C"  BaseInvokableCall_t2229564840 * UnityEvent_GetDelegate_m2675843778 (Il2CppObject * __this /* static, unused */, UnityAction_t4025899511 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m2675843778_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UnityAction_t4025899511 * L_0 = ___action0;
		InvokableCall_t2183506063 * L_1 = (InvokableCall_t2183506063 *)il2cpp_codegen_object_new(InvokableCall_t2183506063_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m738302370(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m4163344491 (UnityEvent_t408735097 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = __this->get_m_InvokeArray_4();
		UnityEventBase_Invoke_m2706435282(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
