﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableCount
struct HashTableCount_t2501191333;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableCount::.ctor()
extern "C"  void HashTableCount__ctor_m3144675921 (HashTableCount_t2501191333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCount::Reset()
extern "C"  void HashTableCount_Reset_m3332673010 (HashTableCount_t2501191333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCount::OnEnter()
extern "C"  void HashTableCount_OnEnter_m4271288864 (HashTableCount_t2501191333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableCount::doHashTableCount()
extern "C"  void HashTableCount_doHashTableCount_m4133679265 (HashTableCount_t2501191333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
