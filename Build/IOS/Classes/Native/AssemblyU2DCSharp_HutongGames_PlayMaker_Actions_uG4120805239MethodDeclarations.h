﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue
struct uGuiSliderGetNormalizedValue_t4120805239;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::.ctor()
extern "C"  void uGuiSliderGetNormalizedValue__ctor_m1557990773 (uGuiSliderGetNormalizedValue_t4120805239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::Reset()
extern "C"  void uGuiSliderGetNormalizedValue_Reset_m4135293412 (uGuiSliderGetNormalizedValue_t4120805239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::OnEnter()
extern "C"  void uGuiSliderGetNormalizedValue_OnEnter_m4062169310 (uGuiSliderGetNormalizedValue_t4120805239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::OnUpdate()
extern "C"  void uGuiSliderGetNormalizedValue_OnUpdate_m3352698345 (uGuiSliderGetNormalizedValue_t4120805239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderGetNormalizedValue::DoGetValue()
extern "C"  void uGuiSliderGetNormalizedValue_DoGetValue_m850825431 (uGuiSliderGetNormalizedValue_t4120805239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
