﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiGetIsInteractable
struct uGuiGetIsInteractable_t2532531748;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::.ctor()
extern "C"  void uGuiGetIsInteractable__ctor_m461893246 (uGuiGetIsInteractable_t2532531748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::Reset()
extern "C"  void uGuiGetIsInteractable_Reset_m1084672641 (uGuiGetIsInteractable_t2532531748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::OnEnter()
extern "C"  void uGuiGetIsInteractable_OnEnter_m405291737 (uGuiGetIsInteractable_t2532531748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiGetIsInteractable::DoGetValue()
extern "C"  void uGuiGetIsInteractable_DoGetValue_m778443704 (uGuiGetIsInteractable_t2532531748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
