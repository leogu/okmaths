﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DebugFloat
struct DebugFloat_t2231934453;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DebugFloat::.ctor()
extern "C"  void DebugFloat__ctor_m2091638635 (DebugFloat_t2231934453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugFloat::Reset()
extern "C"  void DebugFloat_Reset_m3777575586 (DebugFloat_t2231934453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DebugFloat::OnEnter()
extern "C"  void DebugFloat_OnEnter_m2978573212 (DebugFloat_t2231934453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
