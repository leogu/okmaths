﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.UTF8Encoding
struct UTF8Encoding_t111055448;
// HutongGames.PlayMaker.INamedVariable
struct INamedVariable_t4287019078;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t340212445;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.IO.Stream
struct Stream_t3255436806;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// UnityEngine.Object
struct Object_t1021602117;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_Type1303803226.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1273009179.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool664485696.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector22430450063.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector33996534004.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmRect19023354.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion878438756.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor118301965.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_PlayMakerFSM437737208.h"

// System.Text.UTF8Encoding HutongGames.PlayMaker.FsmUtility::get_Encoding()
extern "C"  UTF8Encoding_t111055448 * FsmUtility_get_Encoding_m1718890584 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmUtility::GetVariableType(HutongGames.PlayMaker.INamedVariable)
extern "C"  int32_t FsmUtility_GetVariableType_m2328415600 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.FsmUtility::GetVariableRealType(HutongGames.PlayMaker.VariableType)
extern "C"  Type_t * FsmUtility_GetVariableRealType_m5299223 (Il2CppObject * __this /* static, unused */, int32_t ___variableType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmUtility::GetEnum(System.Type,System.Int32)
extern "C"  Il2CppObject * FsmUtility_GetEnum_m78625907 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, int32_t ___enumValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmEventToByteArray(HutongGames.PlayMaker.FsmEvent)
extern "C"  Il2CppObject* FsmUtility_FsmEventToByteArray_m1098876034 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmFloatToByteArray(HutongGames.PlayMaker.FsmFloat)
extern "C"  Il2CppObject* FsmUtility_FsmFloatToByteArray_m306424642 (Il2CppObject * __this /* static, unused */, FsmFloat_t937133978 * ___fsmFloat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmIntToByteArray(HutongGames.PlayMaker.FsmInt)
extern "C"  Il2CppObject* FsmUtility_FsmIntToByteArray_m1695239618 (Il2CppObject * __this /* static, unused */, FsmInt_t1273009179 * ___fsmInt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmBoolToByteArray(HutongGames.PlayMaker.FsmBool)
extern "C"  Il2CppObject* FsmUtility_FsmBoolToByteArray_m1927982082 (Il2CppObject * __this /* static, unused */, FsmBool_t664485696 * ___fsmBool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmVector2ToByteArray(HutongGames.PlayMaker.FsmVector2)
extern "C"  Il2CppObject* FsmUtility_FsmVector2ToByteArray_m2384160962 (Il2CppObject * __this /* static, unused */, FsmVector2_t2430450063 * ___fsmVector20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmVector3ToByteArray(HutongGames.PlayMaker.FsmVector3)
extern "C"  Il2CppObject* FsmUtility_FsmVector3ToByteArray_m1942106242 (Il2CppObject * __this /* static, unused */, FsmVector3_t3996534004 * ___fsmVector30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmRectToByteArray(HutongGames.PlayMaker.FsmRect)
extern "C"  Il2CppObject* FsmUtility_FsmRectToByteArray_m4179802514 (Il2CppObject * __this /* static, unused */, FsmRect_t19023354 * ___fsmRect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmQuaternionToByteArray(HutongGames.PlayMaker.FsmQuaternion)
extern "C"  Il2CppObject* FsmUtility_FsmQuaternionToByteArray_m819857202 (Il2CppObject * __this /* static, unused */, FsmQuaternion_t878438756 * ___fsmQuaternion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::FsmColorToByteArray(HutongGames.PlayMaker.FsmColor)
extern "C"  Il2CppObject* FsmUtility_FsmColorToByteArray_m1269959970 (Il2CppObject * __this /* static, unused */, FsmColor_t118301965 * ___fsmColor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::ColorToByteArray(UnityEngine.Color)
extern "C"  Il2CppObject* FsmUtility_ColorToByteArray_m1490547827 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::Vector2ToByteArray(UnityEngine.Vector2)
extern "C"  Il2CppObject* FsmUtility_Vector2ToByteArray_m3062420227 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___vector20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::Vector3ToByteArray(UnityEngine.Vector3)
extern "C"  Il2CppObject* FsmUtility_Vector3ToByteArray_m453412327 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___vector30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::Vector4ToByteArray(UnityEngine.Vector4)
extern "C"  Il2CppObject* FsmUtility_Vector4ToByteArray_m2608548819 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___vector40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::RectToByteArray(UnityEngine.Rect)
extern "C"  Il2CppObject* FsmUtility_RectToByteArray_m512795515 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.Byte> HutongGames.PlayMaker.FsmUtility::QuaternionToByteArray(UnityEngine.Quaternion)
extern "C"  Il2CppObject* FsmUtility_QuaternionToByteArray_m843544923 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___quaternion0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility::StringToByteArray(System.String)
extern "C"  ByteU5BU5D_t3397334013* FsmUtility_StringToByteArray_m710107384 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::ByteArrayToString(System.Byte[])
extern "C"  String_t* FsmUtility_ByteArrayToString_m2732123336 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::ByteArrayToString(System.Byte[],System.Int32,System.Int32)
extern "C"  String_t* FsmUtility_ByteArrayToString_m845633530 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmEvent(System.Byte[],System.Int32,System.Int32)
extern "C"  FsmEvent_t1258573736 * FsmUtility_ByteArrayToFsmEvent_m3711123935 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, int32_t ___size2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmFloat(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmFloat_t937133978 * FsmUtility_ByteArrayToFsmFloat_m1756473061 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmInt(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmInt_t1273009179 * FsmUtility_ByteArrayToFsmInt_m2449046373 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmBool(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmBool_t664485696 * FsmUtility_ByteArrayToFsmBool_m3723558661 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.FsmUtility::ByteArrayToColor(System.Byte[],System.Int32)
extern "C"  Color_t2020392075  FsmUtility_ByteArrayToColor_m1434383295 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 HutongGames.PlayMaker.FsmUtility::ByteArrayToVector2(System.Byte[],System.Int32)
extern "C"  Vector2_t2243707579  FsmUtility_ByteArrayToVector2_m2667905567 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmVector2(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmVector2_t2430450063 * FsmUtility_ByteArrayToFsmVector2_m2817109413 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.FsmUtility::ByteArrayToVector3(System.Byte[],System.Int32)
extern "C"  Vector3_t2243707580  FsmUtility_ByteArrayToVector3_m3860558011 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmVector3(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmVector3_t3996534004 * FsmUtility_ByteArrayToFsmVector3_m1622912485 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmRect(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmRect_t19023354 * FsmUtility_ByteArrayToFsmRect_m3277287897 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmQuaternion(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmQuaternion_t878438756 * FsmUtility_ByteArrayToFsmQuaternion_m1431908985 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.FsmUtility::ByteArrayToFsmColor(HutongGames.PlayMaker.Fsm,System.Byte[],System.Int32,System.Int32)
extern "C"  FsmColor_t118301965 * FsmUtility_ByteArrayToFsmColor_m3633125381 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___startIndex2, int32_t ___totalLength3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 HutongGames.PlayMaker.FsmUtility::ByteArrayToVector4(System.Byte[],System.Int32)
extern "C"  Vector4_t2243707581  FsmUtility_ByteArrayToVector4_m3568805375 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.PlayMaker.FsmUtility::ByteArrayToRect(System.Byte[],System.Int32)
extern "C"  Rect_t3681755626  FsmUtility_ByteArrayToRect_m2857791315 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion HutongGames.PlayMaker.FsmUtility::ByteArrayToQuaternion(System.Byte[],System.Int32)
extern "C"  Quaternion_t4030073918  FsmUtility_ByteArrayToQuaternion_m475340115 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, int32_t ___startIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] HutongGames.PlayMaker.FsmUtility::ReadToEnd(System.IO.Stream)
extern "C"  ByteU5BU5D_t3397334013* FsmUtility_ReadToEnd_m1100838908 (Il2CppObject * __this /* static, unused */, Stream_t3255436806 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::StripNamespace(System.String)
extern "C"  String_t* FsmUtility_StripNamespace_m2120651009 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetPath(HutongGames.PlayMaker.FsmState)
extern "C"  String_t* FsmUtility_GetPath_m686246434 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetPath(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction)
extern "C"  String_t* FsmUtility_GetPath_m3065591209 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___state0, FsmStateAction_t2862378169 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetPath(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.String)
extern "C"  String_t* FsmUtility_GetPath_m2182142631 (Il2CppObject * __this /* static, unused */, FsmState_t1643911659 * ___state0, FsmStateAction_t2862378169 * ___action1, String_t* ___parameter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFullFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* FsmUtility_GetFullFsmLabel_m4134766633 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFullFsmLabel(PlayMakerFSM)
extern "C"  String_t* FsmUtility_GetFullFsmLabel_m3442621419 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* FsmUtility_GetFsmLabel_m3290734132 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object HutongGames.PlayMaker.FsmUtility::GetOwner(HutongGames.PlayMaker.Fsm)
extern "C"  Object_t1021602117 * FsmUtility_GetOwner_m3740151977 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
