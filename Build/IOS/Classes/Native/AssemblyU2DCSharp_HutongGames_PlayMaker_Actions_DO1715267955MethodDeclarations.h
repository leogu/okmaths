﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformJumpAnchorPos
struct DOTweenRectTransformJumpAnchorPos_t1715267955;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformJumpAnchorPos::.ctor()
extern "C"  void DOTweenRectTransformJumpAnchorPos__ctor_m3825523727 (DOTweenRectTransformJumpAnchorPos_t1715267955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformJumpAnchorPos::Reset()
extern "C"  void DOTweenRectTransformJumpAnchorPos_Reset_m2648458388 (DOTweenRectTransformJumpAnchorPos_t1715267955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformJumpAnchorPos::OnEnter()
extern "C"  void DOTweenRectTransformJumpAnchorPos_OnEnter_m318360866 (DOTweenRectTransformJumpAnchorPos_t1715267955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformJumpAnchorPos::<OnEnter>m__70()
extern "C"  void DOTweenRectTransformJumpAnchorPos_U3COnEnterU3Em__70_m2596966658 (DOTweenRectTransformJumpAnchorPos_t1715267955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformJumpAnchorPos::<OnEnter>m__71()
extern "C"  void DOTweenRectTransformJumpAnchorPos_U3COnEnterU3Em__71_m2596966691 (DOTweenRectTransformJumpAnchorPos_t1715267955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
