﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalRotateBy
struct DOTweenTransformBlendableLocalRotateBy_t2339023232;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalRotateBy::.ctor()
extern "C"  void DOTweenTransformBlendableLocalRotateBy__ctor_m1665785900 (DOTweenTransformBlendableLocalRotateBy_t2339023232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalRotateBy::Reset()
extern "C"  void DOTweenTransformBlendableLocalRotateBy_Reset_m1846739341 (DOTweenTransformBlendableLocalRotateBy_t2339023232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalRotateBy::OnEnter()
extern "C"  void DOTweenTransformBlendableLocalRotateBy_OnEnter_m3653527141 (DOTweenTransformBlendableLocalRotateBy_t2339023232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalRotateBy::<OnEnter>m__A8()
extern "C"  void DOTweenTransformBlendableLocalRotateBy_U3COnEnterU3Em__A8_m3197370307 (DOTweenTransformBlendableLocalRotateBy_t2339023232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableLocalRotateBy::<OnEnter>m__A9()
extern "C"  void DOTweenTransformBlendableLocalRotateBy_U3COnEnterU3Em__A9_m1651489342 (DOTweenTransformBlendableLocalRotateBy_t2339023232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
