﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar
struct uGuiInputFieldSetAsterixChar_t2374437934;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::.ctor()
extern "C"  void uGuiInputFieldSetAsterixChar__ctor_m479329552 (uGuiInputFieldSetAsterixChar_t2374437934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::.cctor()
extern "C"  void uGuiInputFieldSetAsterixChar__cctor_m3158914621 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::Reset()
extern "C"  void uGuiInputFieldSetAsterixChar_Reset_m1315104343 (uGuiInputFieldSetAsterixChar_t2374437934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::OnEnter()
extern "C"  void uGuiInputFieldSetAsterixChar_OnEnter_m3823876031 (uGuiInputFieldSetAsterixChar_t2374437934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::DoSetValue()
extern "C"  void uGuiInputFieldSetAsterixChar_DoSetValue_m2045532570 (uGuiInputFieldSetAsterixChar_t2374437934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetAsterixChar::OnExit()
extern "C"  void uGuiInputFieldSetAsterixChar_OnExit_m652447887 (uGuiInputFieldSetAsterixChar_t2374437934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
