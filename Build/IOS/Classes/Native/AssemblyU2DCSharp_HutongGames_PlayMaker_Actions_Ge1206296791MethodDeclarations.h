﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo
struct GetAnimatorCurrentTransitionInfo_t1206296791;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfo__ctor_m477201979 (GetAnimatorCurrentTransitionInfo_t1206296791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfo_Reset_m2202413372 (GetAnimatorCurrentTransitionInfo_t1206296791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnEnter()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnEnter_m4089939922 (GetAnimatorCurrentTransitionInfo_t1206296791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::OnActionUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfo_OnActionUpdate_m1369014613 (GetAnimatorCurrentTransitionInfo_t1206296791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfo::GetTransitionInfo()
extern "C"  void GetAnimatorCurrentTransitionInfo_GetTransitionInfo_m2780396476 (GetAnimatorCurrentTransitionInfo_t1206296791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
