﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet
struct SetAnimatorStabilizeFeet_t3575473090;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::.ctor()
extern "C"  void SetAnimatorStabilizeFeet__ctor_m2590380926 (SetAnimatorStabilizeFeet_t3575473090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::Reset()
extern "C"  void SetAnimatorStabilizeFeet_Reset_m1146170127 (SetAnimatorStabilizeFeet_t3575473090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::OnEnter()
extern "C"  void SetAnimatorStabilizeFeet_OnEnter_m3102292471 (SetAnimatorStabilizeFeet_t3575473090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorStabilizeFeet::DoStabilizeFeet()
extern "C"  void SetAnimatorStabilizeFeet_DoStabilizeFeet_m2036505336 (SetAnimatorStabilizeFeet_t3575473090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
