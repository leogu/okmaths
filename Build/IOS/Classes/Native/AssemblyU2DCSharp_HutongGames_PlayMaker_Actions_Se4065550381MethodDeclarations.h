﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmString
struct SetFsmString_t4065550381;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmString::.ctor()
extern "C"  void SetFsmString__ctor_m132739869 (SetFsmString_t4065550381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::Reset()
extern "C"  void SetFsmString_Reset_m2984007902 (SetFsmString_t4065550381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::OnEnter()
extern "C"  void SetFsmString_OnEnter_m2119816876 (SetFsmString_t4065550381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::DoSetFsmString()
extern "C"  void SetFsmString_DoSetFsmString_m474226881 (SetFsmString_t4065550381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmString::OnUpdate()
extern "C"  void SetFsmString_OnUpdate_m1681826129 (SetFsmString_t4065550381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
