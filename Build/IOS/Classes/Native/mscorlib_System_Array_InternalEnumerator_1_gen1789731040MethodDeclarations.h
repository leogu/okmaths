﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1789731040.h"
#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.VariableType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3369332175_gshared (InternalEnumerator_1_t1789731040 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3369332175(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1789731040 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3369332175_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.VariableType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m293308079_gshared (InternalEnumerator_1_t1789731040 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m293308079(__this, method) ((  void (*) (InternalEnumerator_1_t1789731040 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m293308079_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.VariableType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548744379_gshared (InternalEnumerator_1_t1789731040 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548744379(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1789731040 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m548744379_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.VariableType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2012279630_gshared (InternalEnumerator_1_t1789731040 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2012279630(__this, method) ((  void (*) (InternalEnumerator_1_t1789731040 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2012279630_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.VariableType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2578664187_gshared (InternalEnumerator_1_t1789731040 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2578664187(__this, method) ((  bool (*) (InternalEnumerator_1_t1789731040 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2578664187_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.VariableType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m871111872_gshared (InternalEnumerator_1_t1789731040 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m871111872(__this, method) ((  int32_t (*) (InternalEnumerator_1_t1789731040 *, const MethodInfo*))InternalEnumerator_1_get_Current_m871111872_gshared)(__this, method)
