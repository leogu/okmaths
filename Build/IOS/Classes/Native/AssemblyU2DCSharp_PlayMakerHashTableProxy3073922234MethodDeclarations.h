﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerHashTableProxy
struct PlayMakerHashTableProxy_t3073922234;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerHashTableProxy::.ctor()
extern "C"  void PlayMakerHashTableProxy__ctor_m3752800367 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable PlayMakerHashTableProxy::get_hashTable()
extern "C"  Hashtable_t909839986 * PlayMakerHashTableProxy_get_hashTable_m3153922573 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerHashTableProxy::Awake()
extern "C"  void PlayMakerHashTableProxy_Awake_m3979826214 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerHashTableProxy::isCollectionDefined()
extern "C"  bool PlayMakerHashTableProxy_isCollectionDefined_m1440963666 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerHashTableProxy::TakeSnapShot()
extern "C"  void PlayMakerHashTableProxy_TakeSnapShot_m2586978514 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerHashTableProxy::RevertToSnapShot()
extern "C"  void PlayMakerHashTableProxy_RevertToSnapShot_m1134899826 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerHashTableProxy::InspectorEdit(System.Int32)
extern "C"  void PlayMakerHashTableProxy_InspectorEdit_m3118930417 (PlayMakerHashTableProxy_t3073922234 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerHashTableProxy::PreFillHashTable()
extern "C"  void PlayMakerHashTableProxy_PreFillHashTable_m990394611 (PlayMakerHashTableProxy_t3073922234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
