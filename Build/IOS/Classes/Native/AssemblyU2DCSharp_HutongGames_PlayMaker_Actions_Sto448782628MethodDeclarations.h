﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StopLocationServiceUpdates
struct StopLocationServiceUpdates_t448782628;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::.ctor()
extern "C"  void StopLocationServiceUpdates__ctor_m916593232 (StopLocationServiceUpdates_t448782628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::Reset()
extern "C"  void StopLocationServiceUpdates_Reset_m4141952489 (StopLocationServiceUpdates_t448782628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StopLocationServiceUpdates::OnEnter()
extern "C"  void StopLocationServiceUpdates_OnEnter_m3988457369 (StopLocationServiceUpdates_t448782628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
