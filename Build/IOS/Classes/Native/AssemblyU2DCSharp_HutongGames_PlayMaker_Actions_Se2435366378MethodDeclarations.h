﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetCameraFOV
struct SetCameraFOV_t2435366378;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::.ctor()
extern "C"  void SetCameraFOV__ctor_m2394408766 (SetCameraFOV_t2435366378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::Reset()
extern "C"  void SetCameraFOV_Reset_m2213960591 (SetCameraFOV_t2435366378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::OnEnter()
extern "C"  void SetCameraFOV_OnEnter_m326759375 (SetCameraFOV_t2435366378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::OnUpdate()
extern "C"  void SetCameraFOV_OnUpdate_m1339578168 (SetCameraFOV_t2435366378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraFOV::DoSetCameraFOV()
extern "C"  void SetCameraFOV_DoSetCameraFOV_m2304958017 (SetCameraFOV_t2435366378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
