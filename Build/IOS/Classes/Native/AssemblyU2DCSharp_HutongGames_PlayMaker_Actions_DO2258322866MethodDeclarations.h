﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAnimateString
struct DOTweenAnimateString_t2258322866;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateString::.ctor()
extern "C"  void DOTweenAnimateString__ctor_m297957410 (DOTweenAnimateString_t2258322866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateString::Reset()
extern "C"  void DOTweenAnimateString_Reset_m3517359643 (DOTweenAnimateString_t2258322866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateString::OnEnter()
extern "C"  void DOTweenAnimateString_OnEnter_m517379051 (DOTweenAnimateString_t2258322866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.DOTweenAnimateString::<OnEnter>m__10()
extern "C"  String_t* DOTweenAnimateString_U3COnEnterU3Em__10_m3271445394 (DOTweenAnimateString_t2258322866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateString::<OnEnter>m__11(System.String)
extern "C"  void DOTweenAnimateString_U3COnEnterU3Em__11_m2347879518 (DOTweenAnimateString_t2258322866 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateString::<OnEnter>m__12()
extern "C"  void DOTweenAnimateString_U3COnEnterU3Em__12_m2202111527 (DOTweenAnimateString_t2258322866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAnimateString::<OnEnter>m__13()
extern "C"  void DOTweenAnimateString_U3COnEnterU3Em__13_m656230562 (DOTweenAnimateString_t2258322866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
