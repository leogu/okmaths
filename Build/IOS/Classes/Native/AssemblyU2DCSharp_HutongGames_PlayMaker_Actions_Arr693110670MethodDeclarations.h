﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetPrevious
struct ArrayListGetPrevious_t693110670;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetPrevious::.ctor()
extern "C"  void ArrayListGetPrevious__ctor_m3381066622 (ArrayListGetPrevious_t693110670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetPrevious::Reset()
extern "C"  void ArrayListGetPrevious_Reset_m1050880127 (ArrayListGetPrevious_t693110670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetPrevious::OnEnter()
extern "C"  void ArrayListGetPrevious_OnEnter_m2689061239 (ArrayListGetPrevious_t693110670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetPrevious::DoGetPreviousItem()
extern "C"  void ArrayListGetPrevious_DoGetPreviousItem_m170112767 (ArrayListGetPrevious_t693110670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetPrevious::GetItemAtIndex()
extern "C"  void ArrayListGetPrevious_GetItemAtIndex_m4236215714 (ArrayListGetPrevious_t693110670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
