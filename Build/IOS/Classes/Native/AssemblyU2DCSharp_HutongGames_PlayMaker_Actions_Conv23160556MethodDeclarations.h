﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertStringToInt
struct ConvertStringToInt_t23160556;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::.ctor()
extern "C"  void ConvertStringToInt__ctor_m3317392276 (ConvertStringToInt_t23160556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::Reset()
extern "C"  void ConvertStringToInt_Reset_m708655805 (ConvertStringToInt_t23160556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::OnEnter()
extern "C"  void ConvertStringToInt_OnEnter_m469557413 (ConvertStringToInt_t23160556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::OnUpdate()
extern "C"  void ConvertStringToInt_OnUpdate_m3820560818 (ConvertStringToInt_t23160556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertStringToInt::DoConvertStringToInt()
extern "C"  void ConvertStringToInt_DoConvertStringToInt_m923201633 (ConvertStringToInt_t23160556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
