﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects
struct  ArrayListActivateGameObjects_t3706966832  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::activate
	FsmBool_t664485696 * ___activate_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_15;
	// System.Boolean[] HutongGames.PlayMaker.Actions.ArrayListActivateGameObjects::_activeStates
	BooleanU5BU5D_t3568034315* ____activeStates_16;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListActivateGameObjects_t3706966832, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListActivateGameObjects_t3706966832, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_activate_14() { return static_cast<int32_t>(offsetof(ArrayListActivateGameObjects_t3706966832, ___activate_14)); }
	inline FsmBool_t664485696 * get_activate_14() const { return ___activate_14; }
	inline FsmBool_t664485696 ** get_address_of_activate_14() { return &___activate_14; }
	inline void set_activate_14(FsmBool_t664485696 * value)
	{
		___activate_14 = value;
		Il2CppCodeGenWriteBarrier(&___activate_14, value);
	}

	inline static int32_t get_offset_of_resetOnExit_15() { return static_cast<int32_t>(offsetof(ArrayListActivateGameObjects_t3706966832, ___resetOnExit_15)); }
	inline FsmBool_t664485696 * get_resetOnExit_15() const { return ___resetOnExit_15; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_15() { return &___resetOnExit_15; }
	inline void set_resetOnExit_15(FsmBool_t664485696 * value)
	{
		___resetOnExit_15 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_15, value);
	}

	inline static int32_t get_offset_of__activeStates_16() { return static_cast<int32_t>(offsetof(ArrayListActivateGameObjects_t3706966832, ____activeStates_16)); }
	inline BooleanU5BU5D_t3568034315* get__activeStates_16() const { return ____activeStates_16; }
	inline BooleanU5BU5D_t3568034315** get_address_of__activeStates_16() { return &____activeStates_16; }
	inline void set__activeStates_16(BooleanU5BU5D_t3568034315* value)
	{
		____activeStates_16 = value;
		Il2CppCodeGenWriteBarrier(&____activeStates_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
