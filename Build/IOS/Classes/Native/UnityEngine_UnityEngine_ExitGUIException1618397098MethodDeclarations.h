﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ExitGUIException
struct ExitGUIException_t1618397098;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.ExitGUIException::.ctor()
extern "C"  void ExitGUIException__ctor_m1016168781 (ExitGUIException_t1618397098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
