﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlayerPrefsHasKey
struct PlayerPrefsHasKey_t543342762;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::.ctor()
extern "C"  void PlayerPrefsHasKey__ctor_m4275440108 (PlayerPrefsHasKey_t543342762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::Reset()
extern "C"  void PlayerPrefsHasKey_Reset_m4293237959 (PlayerPrefsHasKey_t543342762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlayerPrefsHasKey::OnEnter()
extern "C"  void PlayerPrefsHasKey_OnEnter_m2373790543 (PlayerPrefsHasKey_t543342762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
