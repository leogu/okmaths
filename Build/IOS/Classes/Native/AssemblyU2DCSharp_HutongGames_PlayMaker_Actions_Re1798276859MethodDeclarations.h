﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint
struct RectTransformWorldToScreenPoint_t1798276859;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::.ctor()
extern "C"  void RectTransformWorldToScreenPoint__ctor_m189752135 (RectTransformWorldToScreenPoint_t1798276859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::Reset()
extern "C"  void RectTransformWorldToScreenPoint_Reset_m1542851420 (RectTransformWorldToScreenPoint_t1798276859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::OnEnter()
extern "C"  void RectTransformWorldToScreenPoint_OnEnter_m1091943578 (RectTransformWorldToScreenPoint_t1798276859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::OnActionUpdate()
extern "C"  void RectTransformWorldToScreenPoint_OnActionUpdate_m2264598225 (RectTransformWorldToScreenPoint_t1798276859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformWorldToScreenPoint::DoWorldToScreenPoint()
extern "C"  void RectTransformWorldToScreenPoint_DoWorldToScreenPoint_m2457540213 (RectTransformWorldToScreenPoint_t1798276859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
