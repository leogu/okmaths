﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenOutlineColor
struct DOTweenOutlineColor_t4270823209;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineColor::.ctor()
extern "C"  void DOTweenOutlineColor__ctor_m2877422681 (DOTweenOutlineColor_t4270823209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineColor::Reset()
extern "C"  void DOTweenOutlineColor_Reset_m1002082086 (DOTweenOutlineColor_t4270823209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineColor::OnEnter()
extern "C"  void DOTweenOutlineColor_OnEnter_m3590898244 (DOTweenOutlineColor_t4270823209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineColor::<OnEnter>m__64()
extern "C"  void DOTweenOutlineColor_U3COnEnterU3Em__64_m2600019137 (DOTweenOutlineColor_t4270823209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenOutlineColor::<OnEnter>m__65()
extern "C"  void DOTweenOutlineColor_U3COnEnterU3Em__65_m2600019042 (DOTweenOutlineColor_t4270823209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
