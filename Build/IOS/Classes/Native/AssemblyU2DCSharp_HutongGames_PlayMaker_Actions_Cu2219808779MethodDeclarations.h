﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveVector3
struct CurveVector3_t2219808779;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveVector3::.ctor()
extern "C"  void CurveVector3__ctor_m285638387 (CurveVector3_t2219808779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::Reset()
extern "C"  void CurveVector3_Reset_m1732861460 (CurveVector3_t2219808779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::OnEnter()
extern "C"  void CurveVector3_OnEnter_m3123994138 (CurveVector3_t2219808779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::OnExit()
extern "C"  void CurveVector3_OnExit_m2560556290 (CurveVector3_t2219808779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveVector3::OnUpdate()
extern "C"  void CurveVector3_OnUpdate_m3792536563 (CurveVector3_t2219808779 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
