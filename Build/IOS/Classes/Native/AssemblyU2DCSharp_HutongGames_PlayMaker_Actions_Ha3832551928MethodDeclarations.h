﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableRevertSnapShot
struct HashTableRevertSnapShot_t3832551928;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableRevertSnapShot::.ctor()
extern "C"  void HashTableRevertSnapShot__ctor_m275238916 (HashTableRevertSnapShot_t3832551928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableRevertSnapShot::Reset()
extern "C"  void HashTableRevertSnapShot_Reset_m4258001745 (HashTableRevertSnapShot_t3832551928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableRevertSnapShot::OnEnter()
extern "C"  void HashTableRevertSnapShot_OnEnter_m1254989561 (HashTableRevertSnapShot_t3832551928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableRevertSnapShot::DoHashTableRevertToSnapShot()
extern "C"  void HashTableRevertSnapShot_DoHashTableRevertToSnapShot_m396788820 (HashTableRevertSnapShot_t3832551928 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
