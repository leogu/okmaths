﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformMoveZ
struct DOTweenTransformMoveZ_t4123356105;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveZ::.ctor()
extern "C"  void DOTweenTransformMoveZ__ctor_m1341954295 (DOTweenTransformMoveZ_t4123356105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveZ::Reset()
extern "C"  void DOTweenTransformMoveZ_Reset_m1809442250 (DOTweenTransformMoveZ_t4123356105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveZ::OnEnter()
extern "C"  void DOTweenTransformMoveZ_OnEnter_m3609803620 (DOTweenTransformMoveZ_t4123356105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveZ::<OnEnter>m__CA()
extern "C"  void DOTweenTransformMoveZ_U3COnEnterU3Em__CA_m1806316063 (DOTweenTransformMoveZ_t4123356105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMoveZ::<OnEnter>m__CB()
extern "C"  void DOTweenTransformMoveZ_U3COnEnterU3Em__CB_m1806316162 (DOTweenTransformMoveZ_t4123356105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
