﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddMixingTransform
struct AddMixingTransform_t3108552105;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::.ctor()
extern "C"  void AddMixingTransform__ctor_m3102971179 (AddMixingTransform_t3108552105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::Reset()
extern "C"  void AddMixingTransform_Reset_m2029272690 (AddMixingTransform_t3108552105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::OnEnter()
extern "C"  void AddMixingTransform_OnEnter_m2966994196 (AddMixingTransform_t3108552105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddMixingTransform::DoAddMixingTransform()
extern "C"  void AddMixingTransform_DoAddMixingTransform_m3283896833 (AddMixingTransform_t3108552105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
