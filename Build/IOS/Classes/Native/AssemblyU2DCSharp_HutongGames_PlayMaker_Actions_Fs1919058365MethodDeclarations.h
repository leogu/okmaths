﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmStateActionAdvanced
struct FsmStateActionAdvanced_t1919058365;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::.ctor()
extern "C"  void FsmStateActionAdvanced__ctor_m2214743713 (FsmStateActionAdvanced_t1919058365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::Reset()
extern "C"  void FsmStateActionAdvanced_Reset_m526875106 (FsmStateActionAdvanced_t1919058365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::Awake()
extern "C"  void FsmStateActionAdvanced_Awake_m324202910 (FsmStateActionAdvanced_t1919058365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::OnUpdate()
extern "C"  void FsmStateActionAdvanced_OnUpdate_m3148091429 (FsmStateActionAdvanced_t1919058365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::OnLateUpdate()
extern "C"  void FsmStateActionAdvanced_OnLateUpdate_m2083464145 (FsmStateActionAdvanced_t1919058365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmStateActionAdvanced::OnFixedUpdate()
extern "C"  void FsmStateActionAdvanced_OnFixedUpdate_m3280396391 (FsmStateActionAdvanced_t1919058365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
