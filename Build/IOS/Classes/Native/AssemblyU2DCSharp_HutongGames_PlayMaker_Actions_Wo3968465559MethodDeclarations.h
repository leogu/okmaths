﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WorldToScreenPoint
struct WorldToScreenPoint_t3968465559;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::.ctor()
extern "C"  void WorldToScreenPoint__ctor_m1843100479 (WorldToScreenPoint_t3968465559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::Reset()
extern "C"  void WorldToScreenPoint_Reset_m121130280 (WorldToScreenPoint_t3968465559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::OnEnter()
extern "C"  void WorldToScreenPoint_OnEnter_m652592550 (WorldToScreenPoint_t3968465559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::OnUpdate()
extern "C"  void WorldToScreenPoint_OnUpdate_m4166876359 (WorldToScreenPoint_t3968465559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WorldToScreenPoint::DoWorldToScreenPoint()
extern "C"  void WorldToScreenPoint_DoWorldToScreenPoint_m2558902209 (WorldToScreenPoint_t3968465559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
