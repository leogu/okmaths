﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomFloat
struct RandomFloat_t3076278315;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomFloat::.ctor()
extern "C"  void RandomFloat__ctor_m765953073 (RandomFloat_t3076278315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomFloat::Reset()
extern "C"  void RandomFloat_Reset_m807849540 (RandomFloat_t3076278315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomFloat::OnEnter()
extern "C"  void RandomFloat_OnEnter_m3849631174 (RandomFloat_t3076278315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
