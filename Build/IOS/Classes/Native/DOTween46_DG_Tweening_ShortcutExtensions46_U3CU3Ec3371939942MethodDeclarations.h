﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t3371939942;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m2320169091 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
