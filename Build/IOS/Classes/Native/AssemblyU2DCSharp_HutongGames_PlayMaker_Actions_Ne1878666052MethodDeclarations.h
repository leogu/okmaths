﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix
struct NetworkSetLevelPrefix_t1878666052;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix::.ctor()
extern "C"  void NetworkSetLevelPrefix__ctor_m513966558 (NetworkSetLevelPrefix_t1878666052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix::Reset()
extern "C"  void NetworkSetLevelPrefix_Reset_m1175437057 (NetworkSetLevelPrefix_t1878666052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkSetLevelPrefix::OnEnter()
extern "C"  void NetworkSetLevelPrefix_OnEnter_m3682941993 (NetworkSetLevelPrefix_t1878666052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
