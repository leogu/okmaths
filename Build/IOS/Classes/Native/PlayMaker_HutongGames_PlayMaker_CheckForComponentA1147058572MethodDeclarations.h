﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.CheckForComponentAttribute
struct CheckForComponentAttribute_t1147058572;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"

// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::get_Type0()
extern "C"  Type_t * CheckForComponentAttribute_get_Type0_m629991000 (CheckForComponentAttribute_t1147058572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::get_Type1()
extern "C"  Type_t * CheckForComponentAttribute_get_Type1_m629991095 (CheckForComponentAttribute_t1147058572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.CheckForComponentAttribute::get_Type2()
extern "C"  Type_t * CheckForComponentAttribute_get_Type2_m629990934 (CheckForComponentAttribute_t1147058572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CheckForComponentAttribute::.ctor(System.Type)
extern "C"  void CheckForComponentAttribute__ctor_m4080617798 (CheckForComponentAttribute_t1147058572 * __this, Type_t * ___type00, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CheckForComponentAttribute::.ctor(System.Type,System.Type)
extern "C"  void CheckForComponentAttribute__ctor_m1543271715 (CheckForComponentAttribute_t1147058572 * __this, Type_t * ___type00, Type_t * ___type11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.CheckForComponentAttribute::.ctor(System.Type,System.Type,System.Type)
extern "C"  void CheckForComponentAttribute__ctor_m1193526598 (CheckForComponentAttribute_t1147058572 * __this, Type_t * ___type00, Type_t * ___type11, Type_t * ___type22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
