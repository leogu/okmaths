﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGlobals
struct PlayMakerGlobals_t2120229426;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"
#include "mscorlib_System_String2029220233.h"

// System.Boolean PlayMakerGlobals::get_Initialized()
extern "C"  bool PlayMakerGlobals_get_Initialized_m2540978452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern "C"  void PlayMakerGlobals_set_Initialized_m2688991965 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern "C"  bool PlayMakerGlobals_get_IsPlayingInEditor_m2116337626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlayingInEditor_m2559612605 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsPlaying()
extern "C"  bool PlayMakerGlobals_get_IsPlaying_m1998291744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlaying_m3897118763 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern "C"  bool PlayMakerGlobals_get_IsEditor_m2851556513 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsEditor_m1340019058 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern "C"  bool PlayMakerGlobals_get_IsBuilding_m3989868652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsBuilding(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsBuilding_m4135337617 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C"  void PlayMakerGlobals_InitApplicationFlags_m1473070140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::Initialize()
extern "C"  void PlayMakerGlobals_Initialize_m3494352383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerGlobals PlayMakerGlobals::get_Instance()
extern "C"  PlayMakerGlobals_t2120229426 * PlayMakerGlobals_get_Instance_m4066551560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::ResetInstance()
extern "C"  void PlayMakerGlobals_ResetInstance_m1747765483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables PlayMakerGlobals::get_Variables()
extern "C"  FsmVariables_t630687169 * PlayMakerGlobals_get_Variables_m2249473751 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerGlobals_set_Variables_m4203227634 (PlayMakerGlobals_t2120229426 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C"  List_1_t1398341365 * PlayMakerGlobals_get_Events_m2654294232 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C"  void PlayMakerGlobals_set_Events_m1743941641 (PlayMakerGlobals_t2120229426 * __this, List_1_t1398341365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent PlayMakerGlobals::AddEvent(System.String)
extern "C"  FsmEvent_t1258573736 * PlayMakerGlobals_AddEvent_m677616813 (PlayMakerGlobals_t2120229426 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::OnEnable()
extern "C"  void PlayMakerGlobals_OnEnable_m2794833831 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::OnDestroy()
extern "C"  void PlayMakerGlobals_OnDestroy_m124376554 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::.ctor()
extern "C"  void PlayMakerGlobals__ctor_m3164927587 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
