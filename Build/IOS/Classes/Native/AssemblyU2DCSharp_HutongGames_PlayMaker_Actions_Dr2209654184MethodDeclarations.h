﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawFullscreenColor
struct DrawFullscreenColor_t2209654184;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawFullscreenColor::.ctor()
extern "C"  void DrawFullscreenColor__ctor_m4244897300 (DrawFullscreenColor_t2209654184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawFullscreenColor::Reset()
extern "C"  void DrawFullscreenColor_Reset_m3956780033 (DrawFullscreenColor_t2209654184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawFullscreenColor::OnGUI()
extern "C"  void DrawFullscreenColor_OnGUI_m3664159292 (DrawFullscreenColor_t2209654184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
