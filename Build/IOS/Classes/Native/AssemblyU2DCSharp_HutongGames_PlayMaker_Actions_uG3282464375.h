﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor
struct  uGuiInputFieldSetSelectionColor_t3282464375  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::selectionColor
	FsmColor_t118301965 * ___selectionColor_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::_inputField
	InputField_t1631627530 * ____inputField_15;
	// UnityEngine.Color HutongGames.PlayMaker.Actions.uGuiInputFieldSetSelectionColor::_originalValue
	Color_t2020392075  ____originalValue_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetSelectionColor_t3282464375, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_selectionColor_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetSelectionColor_t3282464375, ___selectionColor_12)); }
	inline FsmColor_t118301965 * get_selectionColor_12() const { return ___selectionColor_12; }
	inline FsmColor_t118301965 ** get_address_of_selectionColor_12() { return &___selectionColor_12; }
	inline void set_selectionColor_12(FsmColor_t118301965 * value)
	{
		___selectionColor_12 = value;
		Il2CppCodeGenWriteBarrier(&___selectionColor_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetSelectionColor_t3282464375, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetSelectionColor_t3282464375, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__inputField_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetSelectionColor_t3282464375, ____inputField_15)); }
	inline InputField_t1631627530 * get__inputField_15() const { return ____inputField_15; }
	inline InputField_t1631627530 ** get_address_of__inputField_15() { return &____inputField_15; }
	inline void set__inputField_15(InputField_t1631627530 * value)
	{
		____inputField_15 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_15, value);
	}

	inline static int32_t get_offset_of__originalValue_16() { return static_cast<int32_t>(offsetof(uGuiInputFieldSetSelectionColor_t3282464375, ____originalValue_16)); }
	inline Color_t2020392075  get__originalValue_16() const { return ____originalValue_16; }
	inline Color_t2020392075 * get_address_of__originalValue_16() { return &____originalValue_16; }
	inline void set__originalValue_16(Color_t2020392075  value)
	{
		____originalValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
