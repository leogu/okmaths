﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorTarget
struct GetAnimatorTarget_t827266558;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::.ctor()
extern "C"  void GetAnimatorTarget__ctor_m2782250622 (GetAnimatorTarget_t827266558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::Reset()
extern "C"  void GetAnimatorTarget_Reset_m1782798387 (GetAnimatorTarget_t827266558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnEnter()
extern "C"  void GetAnimatorTarget_OnEnter_m2790471115 (GetAnimatorTarget_t827266558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::OnActionUpdate()
extern "C"  void GetAnimatorTarget_OnActionUpdate_m1912763454 (GetAnimatorTarget_t827266558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorTarget::DoGetTarget()
extern "C"  void GetAnimatorTarget_DoGetTarget_m2750272940 (GetAnimatorTarget_t827266558 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
