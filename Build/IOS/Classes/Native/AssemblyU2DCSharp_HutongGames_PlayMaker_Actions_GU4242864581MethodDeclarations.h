﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutFloatLabel
struct GUILayoutFloatLabel_t4242864581;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::.ctor()
extern "C"  void GUILayoutFloatLabel__ctor_m2630645073 (GUILayoutFloatLabel_t4242864581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::Reset()
extern "C"  void GUILayoutFloatLabel_Reset_m3349806598 (GUILayoutFloatLabel_t4242864581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutFloatLabel::OnGUI()
extern "C"  void GUILayoutFloatLabel_OnGUI_m1328909071 (GUILayoutFloatLabel_t4242864581 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
