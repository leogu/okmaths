﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmString
struct GetFsmString_t3623271585;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmString::.ctor()
extern "C"  void GetFsmString__ctor_m2029511433 (GetFsmString_t3623271585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::Reset()
extern "C"  void GetFsmString_Reset_m586616050 (GetFsmString_t3623271585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::OnEnter()
extern "C"  void GetFsmString_OnEnter_m287696416 (GetFsmString_t3623271585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::OnUpdate()
extern "C"  void GetFsmString_OnUpdate_m4145787837 (GetFsmString_t3623271585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmString::DoGetFsmString()
extern "C"  void GetFsmString_DoGetFsmString_m4236960193 (GetFsmString_t3623271585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
