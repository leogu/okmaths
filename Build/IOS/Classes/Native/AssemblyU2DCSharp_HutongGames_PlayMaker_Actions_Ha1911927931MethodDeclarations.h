﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableTakeSnapShot
struct HashTableTakeSnapShot_t1911927931;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableTakeSnapShot::.ctor()
extern "C"  void HashTableTakeSnapShot__ctor_m2969592833 (HashTableTakeSnapShot_t1911927931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableTakeSnapShot::Reset()
extern "C"  void HashTableTakeSnapShot_Reset_m3414784436 (HashTableTakeSnapShot_t1911927931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableTakeSnapShot::OnEnter()
extern "C"  void HashTableTakeSnapShot_OnEnter_m1531568662 (HashTableTakeSnapShot_t1911927931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableTakeSnapShot::DoHashTableTakeSnapShot()
extern "C"  void HashTableTakeSnapShot_DoHashTableTakeSnapShot_m444514593 (HashTableTakeSnapShot_t1911927931 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
