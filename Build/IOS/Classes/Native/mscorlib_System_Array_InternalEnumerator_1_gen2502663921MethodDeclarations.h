﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmState>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m1863470982(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2502663921 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m853313801_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmState>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9824886(__this, method) ((  void (*) (InternalEnumerator_1_t2502663921 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmState>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546218026(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2502663921 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmState>::Dispose()
#define InternalEnumerator_1_Dispose_m1836767815(__this, method) ((  void (*) (InternalEnumerator_1_t2502663921 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1636767846_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmState>::MoveNext()
#define InternalEnumerator_1_MoveNext_m428984702(__this, method) ((  bool (*) (InternalEnumerator_1_t2502663921 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1047150157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<HutongGames.PlayMaker.FsmState>::get_Current()
#define InternalEnumerator_1_get_Current_m894953391(__this, method) ((  FsmState_t1643911659 * (*) (InternalEnumerator_1_t2502663921 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3206960238_gshared)(__this, method)
