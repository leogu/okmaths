﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformMove
struct DOTweenTransformMove_t527145667;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMove::.ctor()
extern "C"  void DOTweenTransformMove__ctor_m2526281617 (DOTweenTransformMove_t527145667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMove::Reset()
extern "C"  void DOTweenTransformMove_Reset_m840854280 (DOTweenTransformMove_t527145667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMove::OnEnter()
extern "C"  void DOTweenTransformMove_OnEnter_m4009533162 (DOTweenTransformMove_t527145667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMove::<OnEnter>m__C4()
extern "C"  void DOTweenTransformMove_U3COnEnterU3Em__C4_m1471777790 (DOTweenTransformMove_t527145667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformMove::<OnEnter>m__C5()
extern "C"  void DOTweenTransformMove_U3COnEnterU3Em__C5_m4220864121 (DOTweenTransformMove_t527145667 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
