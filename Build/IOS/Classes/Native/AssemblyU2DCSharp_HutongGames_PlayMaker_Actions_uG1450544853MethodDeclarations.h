﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent
struct uGuiToggleOnClickEvent_t1450544853;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::.ctor()
extern "C"  void uGuiToggleOnClickEvent__ctor_m4111968705 (uGuiToggleOnClickEvent_t1450544853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::Reset()
extern "C"  void uGuiToggleOnClickEvent_Reset_m1780416674 (uGuiToggleOnClickEvent_t1450544853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::OnEnter()
extern "C"  void uGuiToggleOnClickEvent_OnEnter_m1178240336 (uGuiToggleOnClickEvent_t1450544853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::OnExit()
extern "C"  void uGuiToggleOnClickEvent_OnExit_m641774076 (uGuiToggleOnClickEvent_t1450544853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleOnClickEvent::DoOnValueChanged(System.Boolean)
extern "C"  void uGuiToggleOnClickEvent_DoOnValueChanged_m3735341737 (uGuiToggleOnClickEvent_t1450544853 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
