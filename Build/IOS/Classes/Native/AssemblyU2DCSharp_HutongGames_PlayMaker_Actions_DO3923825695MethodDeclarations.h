﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosX
struct DOTweenRectTransformAnchorPosX_t3923825695;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosX::.ctor()
extern "C"  void DOTweenRectTransformAnchorPosX__ctor_m2506859029 (DOTweenRectTransformAnchorPosX_t3923825695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosX::Reset()
extern "C"  void DOTweenRectTransformAnchorPosX_Reset_m3947539748 (DOTweenRectTransformAnchorPosX_t3923825695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosX::OnEnter()
extern "C"  void DOTweenRectTransformAnchorPosX_OnEnter_m4190857030 (DOTweenRectTransformAnchorPosX_t3923825695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosX::<OnEnter>m__6C()
extern "C"  void DOTweenRectTransformAnchorPosX_U3COnEnterU3Em__6C_m2326285034 (DOTweenRectTransformAnchorPosX_t3923825695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPosX::<OnEnter>m__6D()
extern "C"  void DOTweenRectTransformAnchorPosX_U3COnEnterU3Em__6D_m3589840997 (DOTweenRectTransformAnchorPosX_t3923825695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
