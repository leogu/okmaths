﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListRemoveAt
struct ArrayListRemoveAt_t3991006866;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveAt::.ctor()
extern "C"  void ArrayListRemoveAt__ctor_m839856298 (ArrayListRemoveAt_t3991006866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveAt::Reset()
extern "C"  void ArrayListRemoveAt_Reset_m1545971015 (ArrayListRemoveAt_t3991006866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveAt::OnEnter()
extern "C"  void ArrayListRemoveAt_OnEnter_m209346591 (ArrayListRemoveAt_t3991006866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListRemoveAt::doArrayListRemoveAt()
extern "C"  void ArrayListRemoveAt_doArrayListRemoveAt_m1024191657 (ArrayListRemoveAt_t3991006866 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
