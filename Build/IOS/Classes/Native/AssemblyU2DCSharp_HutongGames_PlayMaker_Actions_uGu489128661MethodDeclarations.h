﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent
struct uGuiScrollbarOnClickEvent_t489128661;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::.ctor()
extern "C"  void uGuiScrollbarOnClickEvent__ctor_m1087639647 (uGuiScrollbarOnClickEvent_t489128661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::Reset()
extern "C"  void uGuiScrollbarOnClickEvent_Reset_m959875730 (uGuiScrollbarOnClickEvent_t489128661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::OnEnter()
extern "C"  void uGuiScrollbarOnClickEvent_OnEnter_m705852572 (uGuiScrollbarOnClickEvent_t489128661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::OnExit()
extern "C"  void uGuiScrollbarOnClickEvent_OnExit_m2340364108 (uGuiScrollbarOnClickEvent_t489128661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarOnClickEvent::DoOnValueChanged(System.Single)
extern "C"  void uGuiScrollbarOnClickEvent_DoOnValueChanged_m3202675097 (uGuiScrollbarOnClickEvent_t489128661 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
