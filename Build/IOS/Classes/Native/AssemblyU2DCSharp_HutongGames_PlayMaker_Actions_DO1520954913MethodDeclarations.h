﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillById
struct DOTweenControlMethodsKillById_t1520954913;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillById::.ctor()
extern "C"  void DOTweenControlMethodsKillById__ctor_m2105337535 (DOTweenControlMethodsKillById_t1520954913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillById::Reset()
extern "C"  void DOTweenControlMethodsKillById_Reset_m988741058 (DOTweenControlMethodsKillById_t1520954913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsKillById::OnEnter()
extern "C"  void DOTweenControlMethodsKillById_OnEnter_m3325493932 (DOTweenControlMethodsKillById_t1520954913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
