﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale
struct DOTweenTransformShakeScale_t131829328;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::.ctor()
extern "C"  void DOTweenTransformShakeScale__ctor_m2568153840 (DOTweenTransformShakeScale_t131829328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::Reset()
extern "C"  void DOTweenTransformShakeScale_Reset_m4254088225 (DOTweenTransformShakeScale_t131829328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::OnEnter()
extern "C"  void DOTweenTransformShakeScale_OnEnter_m124741561 (DOTweenTransformShakeScale_t131829328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::<OnEnter>m__E2()
extern "C"  void DOTweenTransformShakeScale_U3COnEnterU3Em__E2_m4248324193 (DOTweenTransformShakeScale_t131829328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformShakeScale::<OnEnter>m__E3()
extern "C"  void DOTweenTransformShakeScale_U3COnEnterU3Em__E3_m4107161692 (DOTweenTransformShakeScale_t131829328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
