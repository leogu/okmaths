﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.NoActionTargetsAttribute
struct NoActionTargetsAttribute_t1534437589;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.NoActionTargetsAttribute::.ctor()
extern "C"  void NoActionTargetsAttribute__ctor_m2144721362 (NoActionTargetsAttribute_t1534437589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
