﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties
struct uGuiNavigationExplicitGetProperties_t3748042257;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::.ctor()
extern "C"  void uGuiNavigationExplicitGetProperties__ctor_m4282801219 (uGuiNavigationExplicitGetProperties_t3748042257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::Reset()
extern "C"  void uGuiNavigationExplicitGetProperties_Reset_m5639726 (uGuiNavigationExplicitGetProperties_t3748042257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::OnEnter()
extern "C"  void uGuiNavigationExplicitGetProperties_OnEnter_m2617321880 (uGuiNavigationExplicitGetProperties_t3748042257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationExplicitGetProperties::DoGetValue()
extern "C"  void uGuiNavigationExplicitGetProperties_DoGetValue_m2847112505 (uGuiNavigationExplicitGetProperties_t3748042257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
