﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie
struct DevicePlayFullScreenMovie_t2948372227;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::.ctor()
extern "C"  void DevicePlayFullScreenMovie__ctor_m4246118621 (DevicePlayFullScreenMovie_t2948372227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::Reset()
extern "C"  void DevicePlayFullScreenMovie_Reset_m549852960 (DevicePlayFullScreenMovie_t2948372227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DevicePlayFullScreenMovie::OnEnter()
extern "C"  void DevicePlayFullScreenMovie_OnEnter_m3700984394 (DevicePlayFullScreenMovie_t2948372227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
