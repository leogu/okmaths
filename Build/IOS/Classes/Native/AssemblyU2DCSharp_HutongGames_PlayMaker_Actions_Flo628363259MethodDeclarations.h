﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatAdd
struct FloatAdd_t628363259;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatAdd::.ctor()
extern "C"  void FloatAdd__ctor_m3157495703 (FloatAdd_t628363259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::Reset()
extern "C"  void FloatAdd_Reset_m2976243648 (FloatAdd_t628363259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::OnEnter()
extern "C"  void FloatAdd_OnEnter_m1398911862 (FloatAdd_t628363259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::OnUpdate()
extern "C"  void FloatAdd_OnUpdate_m3409889095 (FloatAdd_t628363259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAdd::DoFloatAdd()
extern "C"  void FloatAdd_DoFloatAdd_m369218305 (FloatAdd_t628363259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
