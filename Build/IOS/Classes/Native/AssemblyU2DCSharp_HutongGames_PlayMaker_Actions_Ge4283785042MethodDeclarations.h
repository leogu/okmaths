﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetStringLeft
struct GetStringLeft_t4283785042;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::.ctor()
extern "C"  void GetStringLeft__ctor_m3717716290 (GetStringLeft_t4283785042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::Reset()
extern "C"  void GetStringLeft_Reset_m1879306383 (GetStringLeft_t4283785042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::OnEnter()
extern "C"  void GetStringLeft_OnEnter_m3190647839 (GetStringLeft_t4283785042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::OnUpdate()
extern "C"  void GetStringLeft_OnUpdate_m3184078132 (GetStringLeft_t4283785042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetStringLeft::DoGetStringLeft()
extern "C"  void GetStringLeft_DoGetStringLeft_m3733297577 (GetStringLeft_t4283785042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
