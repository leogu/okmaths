﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.WakeUp
struct WakeUp_t2954057791;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.WakeUp::.ctor()
extern "C"  void WakeUp__ctor_m3039364907 (WakeUp_t2954057791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp::Reset()
extern "C"  void WakeUp_Reset_m4109249292 (WakeUp_t2954057791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp::OnEnter()
extern "C"  void WakeUp_OnEnter_m2006265786 (WakeUp_t2954057791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.WakeUp::DoWakeUp()
extern "C"  void WakeUp_DoWakeUp_m4161547713 (WakeUp_t2954057791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
