﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade
struct  DOTweenCanvasGroupFade_t4164124255  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::to
	FsmFloat_t937133978 * ___to_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::setRelative
	FsmBool_t664485696 * ___setRelative_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::duration
	FsmFloat_t937133978 * ___duration_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::startDelay
	FsmFloat_t937133978 * ___startDelay_16;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::playInReverse
	FsmBool_t664485696 * ___playInReverse_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::setReverseRelative
	FsmBool_t664485696 * ___setReverseRelative_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::startEvent
	FsmEvent_t1258573736 * ___startEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_21;
	// System.String HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::tweenIdDescription
	String_t* ___tweenIdDescription_22;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::tweenIdType
	int32_t ___tweenIdType_23;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::stringAsId
	FsmString_t2414474701 * ___stringAsId_24;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::tagAsId
	FsmString_t2414474701 * ___tagAsId_25;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::selectedEase
	int32_t ___selectedEase_26;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::easeType
	int32_t ___easeType_27;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_28;
	// System.String HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_29;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::loops
	FsmInt_t1273009179 * ___loops_30;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::loopType
	int32_t ___loopType_31;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_32;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::recyclable
	FsmBool_t664485696 * ___recyclable_33;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::updateType
	int32_t ___updateType_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_35;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::debugThis
	FsmBool_t664485696 * ___debugThis_36;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenCanvasGroupFade::tweener
	Tweener_t760404022 * ___tweener_37;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_to_12() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___to_12)); }
	inline FsmFloat_t937133978 * get_to_12() const { return ___to_12; }
	inline FsmFloat_t937133978 ** get_address_of_to_12() { return &___to_12; }
	inline void set_to_12(FsmFloat_t937133978 * value)
	{
		___to_12 = value;
		Il2CppCodeGenWriteBarrier(&___to_12, value);
	}

	inline static int32_t get_offset_of_setRelative_13() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___setRelative_13)); }
	inline FsmBool_t664485696 * get_setRelative_13() const { return ___setRelative_13; }
	inline FsmBool_t664485696 ** get_address_of_setRelative_13() { return &___setRelative_13; }
	inline void set_setRelative_13(FsmBool_t664485696 * value)
	{
		___setRelative_13 = value;
		Il2CppCodeGenWriteBarrier(&___setRelative_13, value);
	}

	inline static int32_t get_offset_of_duration_14() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___duration_14)); }
	inline FsmFloat_t937133978 * get_duration_14() const { return ___duration_14; }
	inline FsmFloat_t937133978 ** get_address_of_duration_14() { return &___duration_14; }
	inline void set_duration_14(FsmFloat_t937133978 * value)
	{
		___duration_14 = value;
		Il2CppCodeGenWriteBarrier(&___duration_14, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_15() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___setSpeedBased_15)); }
	inline FsmBool_t664485696 * get_setSpeedBased_15() const { return ___setSpeedBased_15; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_15() { return &___setSpeedBased_15; }
	inline void set_setSpeedBased_15(FsmBool_t664485696 * value)
	{
		___setSpeedBased_15 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_15, value);
	}

	inline static int32_t get_offset_of_startDelay_16() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___startDelay_16)); }
	inline FsmFloat_t937133978 * get_startDelay_16() const { return ___startDelay_16; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_16() { return &___startDelay_16; }
	inline void set_startDelay_16(FsmFloat_t937133978 * value)
	{
		___startDelay_16 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_16, value);
	}

	inline static int32_t get_offset_of_playInReverse_17() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___playInReverse_17)); }
	inline FsmBool_t664485696 * get_playInReverse_17() const { return ___playInReverse_17; }
	inline FsmBool_t664485696 ** get_address_of_playInReverse_17() { return &___playInReverse_17; }
	inline void set_playInReverse_17(FsmBool_t664485696 * value)
	{
		___playInReverse_17 = value;
		Il2CppCodeGenWriteBarrier(&___playInReverse_17, value);
	}

	inline static int32_t get_offset_of_setReverseRelative_18() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___setReverseRelative_18)); }
	inline FsmBool_t664485696 * get_setReverseRelative_18() const { return ___setReverseRelative_18; }
	inline FsmBool_t664485696 ** get_address_of_setReverseRelative_18() { return &___setReverseRelative_18; }
	inline void set_setReverseRelative_18(FsmBool_t664485696 * value)
	{
		___setReverseRelative_18 = value;
		Il2CppCodeGenWriteBarrier(&___setReverseRelative_18, value);
	}

	inline static int32_t get_offset_of_startEvent_19() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___startEvent_19)); }
	inline FsmEvent_t1258573736 * get_startEvent_19() const { return ___startEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_19() { return &___startEvent_19; }
	inline void set_startEvent_19(FsmEvent_t1258573736 * value)
	{
		___startEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_19, value);
	}

	inline static int32_t get_offset_of_finishEvent_20() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___finishEvent_20)); }
	inline FsmEvent_t1258573736 * get_finishEvent_20() const { return ___finishEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_20() { return &___finishEvent_20; }
	inline void set_finishEvent_20(FsmEvent_t1258573736 * value)
	{
		___finishEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_20, value);
	}

	inline static int32_t get_offset_of_finishImmediately_21() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___finishImmediately_21)); }
	inline FsmBool_t664485696 * get_finishImmediately_21() const { return ___finishImmediately_21; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_21() { return &___finishImmediately_21; }
	inline void set_finishImmediately_21(FsmBool_t664485696 * value)
	{
		___finishImmediately_21 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_21, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_22() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___tweenIdDescription_22)); }
	inline String_t* get_tweenIdDescription_22() const { return ___tweenIdDescription_22; }
	inline String_t** get_address_of_tweenIdDescription_22() { return &___tweenIdDescription_22; }
	inline void set_tweenIdDescription_22(String_t* value)
	{
		___tweenIdDescription_22 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_22, value);
	}

	inline static int32_t get_offset_of_tweenIdType_23() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___tweenIdType_23)); }
	inline int32_t get_tweenIdType_23() const { return ___tweenIdType_23; }
	inline int32_t* get_address_of_tweenIdType_23() { return &___tweenIdType_23; }
	inline void set_tweenIdType_23(int32_t value)
	{
		___tweenIdType_23 = value;
	}

	inline static int32_t get_offset_of_stringAsId_24() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___stringAsId_24)); }
	inline FsmString_t2414474701 * get_stringAsId_24() const { return ___stringAsId_24; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_24() { return &___stringAsId_24; }
	inline void set_stringAsId_24(FsmString_t2414474701 * value)
	{
		___stringAsId_24 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_24, value);
	}

	inline static int32_t get_offset_of_tagAsId_25() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___tagAsId_25)); }
	inline FsmString_t2414474701 * get_tagAsId_25() const { return ___tagAsId_25; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_25() { return &___tagAsId_25; }
	inline void set_tagAsId_25(FsmString_t2414474701 * value)
	{
		___tagAsId_25 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_25, value);
	}

	inline static int32_t get_offset_of_selectedEase_26() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___selectedEase_26)); }
	inline int32_t get_selectedEase_26() const { return ___selectedEase_26; }
	inline int32_t* get_address_of_selectedEase_26() { return &___selectedEase_26; }
	inline void set_selectedEase_26(int32_t value)
	{
		___selectedEase_26 = value;
	}

	inline static int32_t get_offset_of_easeType_27() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___easeType_27)); }
	inline int32_t get_easeType_27() const { return ___easeType_27; }
	inline int32_t* get_address_of_easeType_27() { return &___easeType_27; }
	inline void set_easeType_27(int32_t value)
	{
		___easeType_27 = value;
	}

	inline static int32_t get_offset_of_animationCurve_28() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___animationCurve_28)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_28() const { return ___animationCurve_28; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_28() { return &___animationCurve_28; }
	inline void set_animationCurve_28(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_28 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_28, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_29() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___loopsDescriptionArea_29)); }
	inline String_t* get_loopsDescriptionArea_29() const { return ___loopsDescriptionArea_29; }
	inline String_t** get_address_of_loopsDescriptionArea_29() { return &___loopsDescriptionArea_29; }
	inline void set_loopsDescriptionArea_29(String_t* value)
	{
		___loopsDescriptionArea_29 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_29, value);
	}

	inline static int32_t get_offset_of_loops_30() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___loops_30)); }
	inline FsmInt_t1273009179 * get_loops_30() const { return ___loops_30; }
	inline FsmInt_t1273009179 ** get_address_of_loops_30() { return &___loops_30; }
	inline void set_loops_30(FsmInt_t1273009179 * value)
	{
		___loops_30 = value;
		Il2CppCodeGenWriteBarrier(&___loops_30, value);
	}

	inline static int32_t get_offset_of_loopType_31() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___loopType_31)); }
	inline int32_t get_loopType_31() const { return ___loopType_31; }
	inline int32_t* get_address_of_loopType_31() { return &___loopType_31; }
	inline void set_loopType_31(int32_t value)
	{
		___loopType_31 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_32() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___autoKillOnCompletion_32)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_32() const { return ___autoKillOnCompletion_32; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_32() { return &___autoKillOnCompletion_32; }
	inline void set_autoKillOnCompletion_32(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_32 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_32, value);
	}

	inline static int32_t get_offset_of_recyclable_33() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___recyclable_33)); }
	inline FsmBool_t664485696 * get_recyclable_33() const { return ___recyclable_33; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_33() { return &___recyclable_33; }
	inline void set_recyclable_33(FsmBool_t664485696 * value)
	{
		___recyclable_33 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_33, value);
	}

	inline static int32_t get_offset_of_updateType_34() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___updateType_34)); }
	inline int32_t get_updateType_34() const { return ___updateType_34; }
	inline int32_t* get_address_of_updateType_34() { return &___updateType_34; }
	inline void set_updateType_34(int32_t value)
	{
		___updateType_34 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_35() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___isIndependentUpdate_35)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_35() const { return ___isIndependentUpdate_35; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_35() { return &___isIndependentUpdate_35; }
	inline void set_isIndependentUpdate_35(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_35 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_35, value);
	}

	inline static int32_t get_offset_of_debugThis_36() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___debugThis_36)); }
	inline FsmBool_t664485696 * get_debugThis_36() const { return ___debugThis_36; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_36() { return &___debugThis_36; }
	inline void set_debugThis_36(FsmBool_t664485696 * value)
	{
		___debugThis_36 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_36, value);
	}

	inline static int32_t get_offset_of_tweener_37() { return static_cast<int32_t>(offsetof(DOTweenCanvasGroupFade_t4164124255, ___tweener_37)); }
	inline Tweener_t760404022 * get_tweener_37() const { return ___tweener_37; }
	inline Tweener_t760404022 ** get_address_of_tweener_37() { return &___tweener_37; }
	inline void set_tweener_37(Tweener_t760404022 * value)
	{
		___tweener_37 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
