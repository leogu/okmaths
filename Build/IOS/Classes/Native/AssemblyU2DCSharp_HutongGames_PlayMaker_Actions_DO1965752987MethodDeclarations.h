﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialVectorProperty
struct DOTweenMaterialVectorProperty_t1965752987;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialVectorProperty::.ctor()
extern "C"  void DOTweenMaterialVectorProperty__ctor_m3430557509 (DOTweenMaterialVectorProperty_t1965752987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialVectorProperty::Reset()
extern "C"  void DOTweenMaterialVectorProperty_Reset_m4053328952 (DOTweenMaterialVectorProperty_t1965752987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialVectorProperty::OnEnter()
extern "C"  void DOTweenMaterialVectorProperty_OnEnter_m3880903474 (DOTweenMaterialVectorProperty_t1965752987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialVectorProperty::<OnEnter>m__62()
extern "C"  void DOTweenMaterialVectorProperty_U3COnEnterU3Em__62_m4201856127 (DOTweenMaterialVectorProperty_t1965752987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialVectorProperty::<OnEnter>m__63()
extern "C"  void DOTweenMaterialVectorProperty_U3COnEnterU3Em__63_m4201856094 (DOTweenMaterialVectorProperty_t1965752987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
