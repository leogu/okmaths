﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax
struct RectTransformGetAnchorMinAndMax_t3269994400;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::.ctor()
extern "C"  void RectTransformGetAnchorMinAndMax__ctor_m1524749642 (RectTransformGetAnchorMinAndMax_t3269994400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::Reset()
extern "C"  void RectTransformGetAnchorMinAndMax_Reset_m1992250005 (RectTransformGetAnchorMinAndMax_t3269994400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::OnEnter()
extern "C"  void RectTransformGetAnchorMinAndMax_OnEnter_m1052599061 (RectTransformGetAnchorMinAndMax_t3269994400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::OnActionUpdate()
extern "C"  void RectTransformGetAnchorMinAndMax_OnActionUpdate_m4010283774 (RectTransformGetAnchorMinAndMax_t3269994400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMinAndMax::DoGetValues()
extern "C"  void RectTransformGetAnchorMinAndMax_DoGetValues_m1165454809 (RectTransformGetAnchorMinAndMax_t3269994400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
