﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListLastIndexOf
struct ArrayListLastIndexOf_t1878537788;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListLastIndexOf::.ctor()
extern "C"  void ArrayListLastIndexOf__ctor_m654108822 (ArrayListLastIndexOf_t1878537788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListLastIndexOf::Reset()
extern "C"  void ArrayListLastIndexOf_Reset_m4157480325 (ArrayListLastIndexOf_t1878537788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListLastIndexOf::OnEnter()
extern "C"  void ArrayListLastIndexOf_OnEnter_m3279258701 (ArrayListLastIndexOf_t1878537788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListLastIndexOf::DoArrayListLastIndex()
extern "C"  void ArrayListLastIndexOf_DoArrayListLastIndex_m2233906854 (ArrayListLastIndexOf_t1878537788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
