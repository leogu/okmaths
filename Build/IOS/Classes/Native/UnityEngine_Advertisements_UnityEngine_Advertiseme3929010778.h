﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_Advertisements_UnityEngine_Advertisemen714383836.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Advertisements.UnityAdsNative
struct  UnityAdsNative_t3929010778  : public UnityAdsPlatform_t714383836
{
public:
	// System.Boolean UnityEngine.Advertisements.UnityAdsNative::s_FetchCompleted
	bool ___s_FetchCompleted_0;

public:
	inline static int32_t get_offset_of_s_FetchCompleted_0() { return static_cast<int32_t>(offsetof(UnityAdsNative_t3929010778, ___s_FetchCompleted_0)); }
	inline bool get_s_FetchCompleted_0() const { return ___s_FetchCompleted_0; }
	inline bool* get_address_of_s_FetchCompleted_0() { return &___s_FetchCompleted_0; }
	inline void set_s_FetchCompleted_0(bool value)
	{
		___s_FetchCompleted_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
