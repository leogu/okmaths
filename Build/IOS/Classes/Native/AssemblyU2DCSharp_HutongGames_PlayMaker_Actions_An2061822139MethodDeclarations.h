﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnyKey
struct AnyKey_t2061822139;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnyKey::.ctor()
extern "C"  void AnyKey__ctor_m75977103 (AnyKey_t2061822139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnyKey::Reset()
extern "C"  void AnyKey_Reset_m1762990056 (AnyKey_t2061822139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnyKey::OnUpdate()
extern "C"  void AnyKey_OnUpdate_m4182988359 (AnyKey_t2061822139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
