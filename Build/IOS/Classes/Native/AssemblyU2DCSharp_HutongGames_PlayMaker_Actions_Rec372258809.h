﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Canvas
struct Canvas_t209405766;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint
struct  RectTransformPixelAdjustPoint_t372258809  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::canvas
	FsmGameObject_t3097142863 * ___canvas_14;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::screenPoint
	FsmVector2_t2430450063 * ___screenPoint_15;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::pixelPoint
	FsmVector2_t2430450063 * ___pixelPoint_16;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::_rt
	RectTransform_t3349966182 * ____rt_17;
	// UnityEngine.Canvas HutongGames.PlayMaker.Actions.RectTransformPixelAdjustPoint::_canvas
	Canvas_t209405766 * ____canvas_18;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t372258809, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_canvas_14() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t372258809, ___canvas_14)); }
	inline FsmGameObject_t3097142863 * get_canvas_14() const { return ___canvas_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_canvas_14() { return &___canvas_14; }
	inline void set_canvas_14(FsmGameObject_t3097142863 * value)
	{
		___canvas_14 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_14, value);
	}

	inline static int32_t get_offset_of_screenPoint_15() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t372258809, ___screenPoint_15)); }
	inline FsmVector2_t2430450063 * get_screenPoint_15() const { return ___screenPoint_15; }
	inline FsmVector2_t2430450063 ** get_address_of_screenPoint_15() { return &___screenPoint_15; }
	inline void set_screenPoint_15(FsmVector2_t2430450063 * value)
	{
		___screenPoint_15 = value;
		Il2CppCodeGenWriteBarrier(&___screenPoint_15, value);
	}

	inline static int32_t get_offset_of_pixelPoint_16() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t372258809, ___pixelPoint_16)); }
	inline FsmVector2_t2430450063 * get_pixelPoint_16() const { return ___pixelPoint_16; }
	inline FsmVector2_t2430450063 ** get_address_of_pixelPoint_16() { return &___pixelPoint_16; }
	inline void set_pixelPoint_16(FsmVector2_t2430450063 * value)
	{
		___pixelPoint_16 = value;
		Il2CppCodeGenWriteBarrier(&___pixelPoint_16, value);
	}

	inline static int32_t get_offset_of__rt_17() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t372258809, ____rt_17)); }
	inline RectTransform_t3349966182 * get__rt_17() const { return ____rt_17; }
	inline RectTransform_t3349966182 ** get_address_of__rt_17() { return &____rt_17; }
	inline void set__rt_17(RectTransform_t3349966182 * value)
	{
		____rt_17 = value;
		Il2CppCodeGenWriteBarrier(&____rt_17, value);
	}

	inline static int32_t get_offset_of__canvas_18() { return static_cast<int32_t>(offsetof(RectTransformPixelAdjustPoint_t372258809, ____canvas_18)); }
	inline Canvas_t209405766 * get__canvas_18() const { return ____canvas_18; }
	inline Canvas_t209405766 ** get_address_of__canvas_18() { return &____canvas_18; }
	inline void set__canvas_18(Canvas_t209405766 * value)
	{
		____canvas_18 = value;
		Il2CppCodeGenWriteBarrier(&____canvas_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
