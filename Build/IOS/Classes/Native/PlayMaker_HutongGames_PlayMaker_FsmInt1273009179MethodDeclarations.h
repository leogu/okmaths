﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1273009179.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Int32 HutongGames.PlayMaker.FsmInt::get_Value()
extern "C"  int32_t FsmInt_get_Value_m3705703582 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::set_Value(System.Int32)
extern "C"  void FsmInt_set_Value_m4097648685 (FsmInt_t1273009179 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmInt::get_RawValue()
extern "C"  Il2CppObject * FsmInt_get_RawValue_m3318461249 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::set_RawValue(System.Object)
extern "C"  void FsmInt_set_RawValue_m3882601590 (FsmInt_t1273009179 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::SafeAssign(System.Object)
extern "C"  void FsmInt_SafeAssign_m1180058440 (FsmInt_t1273009179 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::.ctor()
extern "C"  void FsmInt__ctor_m3152485982 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::.ctor(System.String)
extern "C"  void FsmInt__ctor_m2308285416 (FsmInt_t1273009179 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::.ctor(HutongGames.PlayMaker.FsmInt)
extern "C"  void FsmInt__ctor_m3124037915 (FsmInt_t1273009179 * __this, FsmInt_t1273009179 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmInt::Clone()
extern "C"  NamedVariable_t3026441313 * FsmInt_Clone_m1783990849 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmInt::get_VariableType()
extern "C"  int32_t FsmInt_get_VariableType_m3641657696 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmInt::ToString()
extern "C"  String_t* FsmInt_ToString_m856388121 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.FsmInt::op_Implicit(System.Int32)
extern "C"  FsmInt_t1273009179 * FsmInt_op_Implicit_m2429643636 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
