﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListContains
struct ArrayListContains_t3890704214;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListContains::.ctor()
extern "C"  void ArrayListContains__ctor_m954636722 (ArrayListContains_t3890704214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListContains::Reset()
extern "C"  void ArrayListContains_Reset_m2463032399 (ArrayListContains_t3890704214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListContains::OnEnter()
extern "C"  void ArrayListContains_OnEnter_m630071383 (ArrayListContains_t3890704214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListContains::doesArrayListContains()
extern "C"  void ArrayListContains_doesArrayListContains_m3748136615 (ArrayListContains_t3890704214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
