﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetCurrentResolution
struct  GetCurrentResolution_t386323333  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCurrentResolution::width
	FsmFloat_t937133978 * ___width_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCurrentResolution::height
	FsmFloat_t937133978 * ___height_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GetCurrentResolution::refreshRate
	FsmFloat_t937133978 * ___refreshRate_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.GetCurrentResolution::currentResolution
	FsmVector3_t3996534004 * ___currentResolution_14;

public:
	inline static int32_t get_offset_of_width_11() { return static_cast<int32_t>(offsetof(GetCurrentResolution_t386323333, ___width_11)); }
	inline FsmFloat_t937133978 * get_width_11() const { return ___width_11; }
	inline FsmFloat_t937133978 ** get_address_of_width_11() { return &___width_11; }
	inline void set_width_11(FsmFloat_t937133978 * value)
	{
		___width_11 = value;
		Il2CppCodeGenWriteBarrier(&___width_11, value);
	}

	inline static int32_t get_offset_of_height_12() { return static_cast<int32_t>(offsetof(GetCurrentResolution_t386323333, ___height_12)); }
	inline FsmFloat_t937133978 * get_height_12() const { return ___height_12; }
	inline FsmFloat_t937133978 ** get_address_of_height_12() { return &___height_12; }
	inline void set_height_12(FsmFloat_t937133978 * value)
	{
		___height_12 = value;
		Il2CppCodeGenWriteBarrier(&___height_12, value);
	}

	inline static int32_t get_offset_of_refreshRate_13() { return static_cast<int32_t>(offsetof(GetCurrentResolution_t386323333, ___refreshRate_13)); }
	inline FsmFloat_t937133978 * get_refreshRate_13() const { return ___refreshRate_13; }
	inline FsmFloat_t937133978 ** get_address_of_refreshRate_13() { return &___refreshRate_13; }
	inline void set_refreshRate_13(FsmFloat_t937133978 * value)
	{
		___refreshRate_13 = value;
		Il2CppCodeGenWriteBarrier(&___refreshRate_13, value);
	}

	inline static int32_t get_offset_of_currentResolution_14() { return static_cast<int32_t>(offsetof(GetCurrentResolution_t386323333, ___currentResolution_14)); }
	inline FsmVector3_t3996534004 * get_currentResolution_14() const { return ___currentResolution_14; }
	inline FsmVector3_t3996534004 ** get_address_of_currentResolution_14() { return &___currentResolution_14; }
	inline void set_currentResolution_14(FsmVector3_t3996534004 * value)
	{
		___currentResolution_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentResolution_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
