﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t4101825636;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// UnityEngine.GUIContent
struct GUIContent_t4210063000;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayMakerGUI
struct PlayMakerGUI_t2662579489;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Comparison`1<PlayMakerFSM>
struct Comparison_1_t1699476059;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGUI
struct  PlayMakerGUI_t2662579489  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerGUI::previewOnGUI
	bool ___previewOnGUI_6;
	// System.Boolean PlayMakerGUI::enableGUILayout
	bool ___enableGUILayout_7;
	// System.Boolean PlayMakerGUI::drawStateLabels
	bool ___drawStateLabels_8;
	// System.Boolean PlayMakerGUI::GUITextureStateLabels
	bool ___GUITextureStateLabels_9;
	// System.Boolean PlayMakerGUI::GUITextStateLabels
	bool ___GUITextStateLabels_10;
	// System.Boolean PlayMakerGUI::filterLabelsWithDistance
	bool ___filterLabelsWithDistance_11;
	// System.Single PlayMakerGUI::maxLabelDistance
	float ___maxLabelDistance_12;
	// System.Boolean PlayMakerGUI::controlMouseCursor
	bool ___controlMouseCursor_13;
	// System.Single PlayMakerGUI::labelScale
	float ___labelScale_14;
	// System.Single PlayMakerGUI::initLabelScale
	float ___initLabelScale_26;

public:
	inline static int32_t get_offset_of_previewOnGUI_6() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___previewOnGUI_6)); }
	inline bool get_previewOnGUI_6() const { return ___previewOnGUI_6; }
	inline bool* get_address_of_previewOnGUI_6() { return &___previewOnGUI_6; }
	inline void set_previewOnGUI_6(bool value)
	{
		___previewOnGUI_6 = value;
	}

	inline static int32_t get_offset_of_enableGUILayout_7() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___enableGUILayout_7)); }
	inline bool get_enableGUILayout_7() const { return ___enableGUILayout_7; }
	inline bool* get_address_of_enableGUILayout_7() { return &___enableGUILayout_7; }
	inline void set_enableGUILayout_7(bool value)
	{
		___enableGUILayout_7 = value;
	}

	inline static int32_t get_offset_of_drawStateLabels_8() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___drawStateLabels_8)); }
	inline bool get_drawStateLabels_8() const { return ___drawStateLabels_8; }
	inline bool* get_address_of_drawStateLabels_8() { return &___drawStateLabels_8; }
	inline void set_drawStateLabels_8(bool value)
	{
		___drawStateLabels_8 = value;
	}

	inline static int32_t get_offset_of_GUITextureStateLabels_9() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___GUITextureStateLabels_9)); }
	inline bool get_GUITextureStateLabels_9() const { return ___GUITextureStateLabels_9; }
	inline bool* get_address_of_GUITextureStateLabels_9() { return &___GUITextureStateLabels_9; }
	inline void set_GUITextureStateLabels_9(bool value)
	{
		___GUITextureStateLabels_9 = value;
	}

	inline static int32_t get_offset_of_GUITextStateLabels_10() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___GUITextStateLabels_10)); }
	inline bool get_GUITextStateLabels_10() const { return ___GUITextStateLabels_10; }
	inline bool* get_address_of_GUITextStateLabels_10() { return &___GUITextStateLabels_10; }
	inline void set_GUITextStateLabels_10(bool value)
	{
		___GUITextStateLabels_10 = value;
	}

	inline static int32_t get_offset_of_filterLabelsWithDistance_11() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___filterLabelsWithDistance_11)); }
	inline bool get_filterLabelsWithDistance_11() const { return ___filterLabelsWithDistance_11; }
	inline bool* get_address_of_filterLabelsWithDistance_11() { return &___filterLabelsWithDistance_11; }
	inline void set_filterLabelsWithDistance_11(bool value)
	{
		___filterLabelsWithDistance_11 = value;
	}

	inline static int32_t get_offset_of_maxLabelDistance_12() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___maxLabelDistance_12)); }
	inline float get_maxLabelDistance_12() const { return ___maxLabelDistance_12; }
	inline float* get_address_of_maxLabelDistance_12() { return &___maxLabelDistance_12; }
	inline void set_maxLabelDistance_12(float value)
	{
		___maxLabelDistance_12 = value;
	}

	inline static int32_t get_offset_of_controlMouseCursor_13() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___controlMouseCursor_13)); }
	inline bool get_controlMouseCursor_13() const { return ___controlMouseCursor_13; }
	inline bool* get_address_of_controlMouseCursor_13() { return &___controlMouseCursor_13; }
	inline void set_controlMouseCursor_13(bool value)
	{
		___controlMouseCursor_13 = value;
	}

	inline static int32_t get_offset_of_labelScale_14() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___labelScale_14)); }
	inline float get_labelScale_14() const { return ___labelScale_14; }
	inline float* get_address_of_labelScale_14() { return &___labelScale_14; }
	inline void set_labelScale_14(float value)
	{
		___labelScale_14 = value;
	}

	inline static int32_t get_offset_of_initLabelScale_26() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___initLabelScale_26)); }
	inline float get_initLabelScale_26() const { return ___initLabelScale_26; }
	inline float* get_address_of_initLabelScale_26() { return &___initLabelScale_26; }
	inline void set_initLabelScale_26(float value)
	{
		___initLabelScale_26 = value;
	}
};

struct PlayMakerGUI_t2662579489_StaticFields
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::fsmList
	List_1_t4101825636 * ___fsmList_3;
	// HutongGames.PlayMaker.Fsm PlayMakerGUI::SelectedFSM
	Fsm_t917886356 * ___SelectedFSM_4;
	// UnityEngine.GUIContent PlayMakerGUI::labelContent
	GUIContent_t4210063000 * ___labelContent_5;
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::SortedFsmList
	List_1_t4101825636 * ___SortedFsmList_15;
	// UnityEngine.GameObject PlayMakerGUI::labelGameObject
	GameObject_t1756533147 * ___labelGameObject_16;
	// System.Single PlayMakerGUI::fsmLabelIndex
	float ___fsmLabelIndex_17;
	// PlayMakerGUI PlayMakerGUI::instance
	PlayMakerGUI_t2662579489 * ___instance_18;
	// UnityEngine.GUISkin PlayMakerGUI::guiSkin
	GUISkin_t1436893342 * ___guiSkin_19;
	// UnityEngine.Color PlayMakerGUI::guiColor
	Color_t2020392075  ___guiColor_20;
	// UnityEngine.Color PlayMakerGUI::guiBackgroundColor
	Color_t2020392075  ___guiBackgroundColor_21;
	// UnityEngine.Color PlayMakerGUI::guiContentColor
	Color_t2020392075  ___guiContentColor_22;
	// UnityEngine.Matrix4x4 PlayMakerGUI::guiMatrix
	Matrix4x4_t2933234003  ___guiMatrix_23;
	// UnityEngine.GUIStyle PlayMakerGUI::stateLabelStyle
	GUIStyle_t1799908754 * ___stateLabelStyle_24;
	// UnityEngine.Texture2D PlayMakerGUI::stateLabelBackground
	Texture2D_t3542995729 * ___stateLabelBackground_25;
	// UnityEngine.Texture PlayMakerGUI::<MouseCursor>k__BackingField
	Texture_t2243626319 * ___U3CMouseCursorU3Ek__BackingField_27;
	// System.Boolean PlayMakerGUI::<LockCursor>k__BackingField
	bool ___U3CLockCursorU3Ek__BackingField_28;
	// System.Boolean PlayMakerGUI::<HideCursor>k__BackingField
	bool ___U3CHideCursorU3Ek__BackingField_29;
	// System.Comparison`1<PlayMakerFSM> PlayMakerGUI::CS$<>9__CachedAnonymousMethodDelegate2
	Comparison_1_t1699476059 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30;

public:
	inline static int32_t get_offset_of_fsmList_3() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___fsmList_3)); }
	inline List_1_t4101825636 * get_fsmList_3() const { return ___fsmList_3; }
	inline List_1_t4101825636 ** get_address_of_fsmList_3() { return &___fsmList_3; }
	inline void set_fsmList_3(List_1_t4101825636 * value)
	{
		___fsmList_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsmList_3, value);
	}

	inline static int32_t get_offset_of_SelectedFSM_4() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___SelectedFSM_4)); }
	inline Fsm_t917886356 * get_SelectedFSM_4() const { return ___SelectedFSM_4; }
	inline Fsm_t917886356 ** get_address_of_SelectedFSM_4() { return &___SelectedFSM_4; }
	inline void set_SelectedFSM_4(Fsm_t917886356 * value)
	{
		___SelectedFSM_4 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedFSM_4, value);
	}

	inline static int32_t get_offset_of_labelContent_5() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___labelContent_5)); }
	inline GUIContent_t4210063000 * get_labelContent_5() const { return ___labelContent_5; }
	inline GUIContent_t4210063000 ** get_address_of_labelContent_5() { return &___labelContent_5; }
	inline void set_labelContent_5(GUIContent_t4210063000 * value)
	{
		___labelContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___labelContent_5, value);
	}

	inline static int32_t get_offset_of_SortedFsmList_15() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___SortedFsmList_15)); }
	inline List_1_t4101825636 * get_SortedFsmList_15() const { return ___SortedFsmList_15; }
	inline List_1_t4101825636 ** get_address_of_SortedFsmList_15() { return &___SortedFsmList_15; }
	inline void set_SortedFsmList_15(List_1_t4101825636 * value)
	{
		___SortedFsmList_15 = value;
		Il2CppCodeGenWriteBarrier(&___SortedFsmList_15, value);
	}

	inline static int32_t get_offset_of_labelGameObject_16() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___labelGameObject_16)); }
	inline GameObject_t1756533147 * get_labelGameObject_16() const { return ___labelGameObject_16; }
	inline GameObject_t1756533147 ** get_address_of_labelGameObject_16() { return &___labelGameObject_16; }
	inline void set_labelGameObject_16(GameObject_t1756533147 * value)
	{
		___labelGameObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___labelGameObject_16, value);
	}

	inline static int32_t get_offset_of_fsmLabelIndex_17() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___fsmLabelIndex_17)); }
	inline float get_fsmLabelIndex_17() const { return ___fsmLabelIndex_17; }
	inline float* get_address_of_fsmLabelIndex_17() { return &___fsmLabelIndex_17; }
	inline void set_fsmLabelIndex_17(float value)
	{
		___fsmLabelIndex_17 = value;
	}

	inline static int32_t get_offset_of_instance_18() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___instance_18)); }
	inline PlayMakerGUI_t2662579489 * get_instance_18() const { return ___instance_18; }
	inline PlayMakerGUI_t2662579489 ** get_address_of_instance_18() { return &___instance_18; }
	inline void set_instance_18(PlayMakerGUI_t2662579489 * value)
	{
		___instance_18 = value;
		Il2CppCodeGenWriteBarrier(&___instance_18, value);
	}

	inline static int32_t get_offset_of_guiSkin_19() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiSkin_19)); }
	inline GUISkin_t1436893342 * get_guiSkin_19() const { return ___guiSkin_19; }
	inline GUISkin_t1436893342 ** get_address_of_guiSkin_19() { return &___guiSkin_19; }
	inline void set_guiSkin_19(GUISkin_t1436893342 * value)
	{
		___guiSkin_19 = value;
		Il2CppCodeGenWriteBarrier(&___guiSkin_19, value);
	}

	inline static int32_t get_offset_of_guiColor_20() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiColor_20)); }
	inline Color_t2020392075  get_guiColor_20() const { return ___guiColor_20; }
	inline Color_t2020392075 * get_address_of_guiColor_20() { return &___guiColor_20; }
	inline void set_guiColor_20(Color_t2020392075  value)
	{
		___guiColor_20 = value;
	}

	inline static int32_t get_offset_of_guiBackgroundColor_21() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiBackgroundColor_21)); }
	inline Color_t2020392075  get_guiBackgroundColor_21() const { return ___guiBackgroundColor_21; }
	inline Color_t2020392075 * get_address_of_guiBackgroundColor_21() { return &___guiBackgroundColor_21; }
	inline void set_guiBackgroundColor_21(Color_t2020392075  value)
	{
		___guiBackgroundColor_21 = value;
	}

	inline static int32_t get_offset_of_guiContentColor_22() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiContentColor_22)); }
	inline Color_t2020392075  get_guiContentColor_22() const { return ___guiContentColor_22; }
	inline Color_t2020392075 * get_address_of_guiContentColor_22() { return &___guiContentColor_22; }
	inline void set_guiContentColor_22(Color_t2020392075  value)
	{
		___guiContentColor_22 = value;
	}

	inline static int32_t get_offset_of_guiMatrix_23() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiMatrix_23)); }
	inline Matrix4x4_t2933234003  get_guiMatrix_23() const { return ___guiMatrix_23; }
	inline Matrix4x4_t2933234003 * get_address_of_guiMatrix_23() { return &___guiMatrix_23; }
	inline void set_guiMatrix_23(Matrix4x4_t2933234003  value)
	{
		___guiMatrix_23 = value;
	}

	inline static int32_t get_offset_of_stateLabelStyle_24() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___stateLabelStyle_24)); }
	inline GUIStyle_t1799908754 * get_stateLabelStyle_24() const { return ___stateLabelStyle_24; }
	inline GUIStyle_t1799908754 ** get_address_of_stateLabelStyle_24() { return &___stateLabelStyle_24; }
	inline void set_stateLabelStyle_24(GUIStyle_t1799908754 * value)
	{
		___stateLabelStyle_24 = value;
		Il2CppCodeGenWriteBarrier(&___stateLabelStyle_24, value);
	}

	inline static int32_t get_offset_of_stateLabelBackground_25() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___stateLabelBackground_25)); }
	inline Texture2D_t3542995729 * get_stateLabelBackground_25() const { return ___stateLabelBackground_25; }
	inline Texture2D_t3542995729 ** get_address_of_stateLabelBackground_25() { return &___stateLabelBackground_25; }
	inline void set_stateLabelBackground_25(Texture2D_t3542995729 * value)
	{
		___stateLabelBackground_25 = value;
		Il2CppCodeGenWriteBarrier(&___stateLabelBackground_25, value);
	}

	inline static int32_t get_offset_of_U3CMouseCursorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___U3CMouseCursorU3Ek__BackingField_27)); }
	inline Texture_t2243626319 * get_U3CMouseCursorU3Ek__BackingField_27() const { return ___U3CMouseCursorU3Ek__BackingField_27; }
	inline Texture_t2243626319 ** get_address_of_U3CMouseCursorU3Ek__BackingField_27() { return &___U3CMouseCursorU3Ek__BackingField_27; }
	inline void set_U3CMouseCursorU3Ek__BackingField_27(Texture_t2243626319 * value)
	{
		___U3CMouseCursorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMouseCursorU3Ek__BackingField_27, value);
	}

	inline static int32_t get_offset_of_U3CLockCursorU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___U3CLockCursorU3Ek__BackingField_28)); }
	inline bool get_U3CLockCursorU3Ek__BackingField_28() const { return ___U3CLockCursorU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CLockCursorU3Ek__BackingField_28() { return &___U3CLockCursorU3Ek__BackingField_28; }
	inline void set_U3CLockCursorU3Ek__BackingField_28(bool value)
	{
		___U3CLockCursorU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CHideCursorU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___U3CHideCursorU3Ek__BackingField_29)); }
	inline bool get_U3CHideCursorU3Ek__BackingField_29() const { return ___U3CHideCursorU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CHideCursorU3Ek__BackingField_29() { return &___U3CHideCursorU3Ek__BackingField_29; }
	inline void set_U3CHideCursorU3Ek__BackingField_29(bool value)
	{
		___U3CHideCursorU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30)); }
	inline Comparison_1_t1699476059 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30; }
	inline Comparison_1_t1699476059 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30(Comparison_1_t1699476059 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
