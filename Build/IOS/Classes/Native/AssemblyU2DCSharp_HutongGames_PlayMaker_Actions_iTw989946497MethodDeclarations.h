﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveFrom
struct iTweenMoveFrom_t989946497;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::.ctor()
extern "C"  void iTweenMoveFrom__ctor_m1773367261 (iTweenMoveFrom_t989946497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::Reset()
extern "C"  void iTweenMoveFrom_Reset_m704266662 (iTweenMoveFrom_t989946497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::OnEnter()
extern "C"  void iTweenMoveFrom_OnEnter_m1576489148 (iTweenMoveFrom_t989946497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::OnExit()
extern "C"  void iTweenMoveFrom_OnExit_m975310592 (iTweenMoveFrom_t989946497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveFrom::DoiTween()
extern "C"  void iTweenMoveFrom_DoiTween_m582067466 (iTweenMoveFrom_t989946497 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
