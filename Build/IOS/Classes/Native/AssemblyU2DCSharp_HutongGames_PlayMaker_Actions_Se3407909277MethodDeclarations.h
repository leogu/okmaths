﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed
struct SetAnimatorPlayBackSpeed_t3407909277;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::.ctor()
extern "C"  void SetAnimatorPlayBackSpeed__ctor_m3123018219 (SetAnimatorPlayBackSpeed_t3407909277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::Reset()
extern "C"  void SetAnimatorPlayBackSpeed_Reset_m548659938 (SetAnimatorPlayBackSpeed_t3407909277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::OnEnter()
extern "C"  void SetAnimatorPlayBackSpeed_OnEnter_m837261508 (SetAnimatorPlayBackSpeed_t3407909277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::OnUpdate()
extern "C"  void SetAnimatorPlayBackSpeed_OnUpdate_m181170115 (SetAnimatorPlayBackSpeed_t3407909277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorPlayBackSpeed::DoPlayBackSpeed()
extern "C"  void SetAnimatorPlayBackSpeed_DoPlayBackSpeed_m707694510 (SetAnimatorPlayBackSpeed_t3407909277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
