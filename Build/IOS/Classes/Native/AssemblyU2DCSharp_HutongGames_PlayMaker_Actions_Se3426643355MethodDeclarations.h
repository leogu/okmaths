﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRectFields
struct SetRectFields_t3426643355;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRectFields::.ctor()
extern "C"  void SetRectFields__ctor_m559320517 (SetRectFields_t3426643355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::Reset()
extern "C"  void SetRectFields_Reset_m1244860824 (SetRectFields_t3426643355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::OnEnter()
extern "C"  void SetRectFields_OnEnter_m2208074162 (SetRectFields_t3426643355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::OnUpdate()
extern "C"  void SetRectFields_OnUpdate_m3860581569 (SetRectFields_t3426643355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRectFields::DoSetRectFields()
extern "C"  void SetRectFields_DoSetRectFields_m4166714817 (SetRectFields_t3426643355 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
