﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderOnClickEvent
struct uGuiSliderOnClickEvent_t1306116382;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderOnClickEvent::.ctor()
extern "C"  void uGuiSliderOnClickEvent__ctor_m288246986 (uGuiSliderOnClickEvent_t1306116382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderOnClickEvent::Reset()
extern "C"  void uGuiSliderOnClickEvent_Reset_m3789155075 (uGuiSliderOnClickEvent_t1306116382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderOnClickEvent::OnEnter()
extern "C"  void uGuiSliderOnClickEvent_OnEnter_m2353502403 (uGuiSliderOnClickEvent_t1306116382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderOnClickEvent::OnExit()
extern "C"  void uGuiSliderOnClickEvent_OnExit_m3977426803 (uGuiSliderOnClickEvent_t1306116382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderOnClickEvent::DoOnValueChanged(System.Single)
extern "C"  void uGuiSliderOnClickEvent_DoOnValueChanged_m3779543480 (uGuiSliderOnClickEvent_t1306116382 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
