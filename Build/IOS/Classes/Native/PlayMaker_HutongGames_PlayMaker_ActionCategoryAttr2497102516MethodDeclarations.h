﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionCategoryAttribute
struct ActionCategoryAttribute_t2497102516;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionCategory928321528.h"

// System.String HutongGames.PlayMaker.ActionCategoryAttribute::get_Category()
extern "C"  String_t* ActionCategoryAttribute_get_Category_m3905789775 (ActionCategoryAttribute_t2497102516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionCategoryAttribute::.ctor(System.String)
extern "C"  void ActionCategoryAttribute__ctor_m2748872401 (ActionCategoryAttribute_t2497102516 * __this, String_t* ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionCategoryAttribute::.ctor(HutongGames.PlayMaker.ActionCategory)
extern "C"  void ActionCategoryAttribute__ctor_m1009142555 (ActionCategoryAttribute_t2497102516 * __this, int32_t ___category0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
