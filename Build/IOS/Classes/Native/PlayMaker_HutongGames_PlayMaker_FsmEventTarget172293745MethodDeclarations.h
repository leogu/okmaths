﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"

// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.FsmEventTarget::get_Self()
extern "C"  FsmEventTarget_t172293745 * FsmEventTarget_get_Self_m202831877 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventTarget::.ctor()
extern "C"  void FsmEventTarget__ctor_m3189862134 (FsmEventTarget_t172293745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventTarget::.ctor(HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void FsmEventTarget__ctor_m3802359515 (FsmEventTarget_t172293745 * __this, FsmEventTarget_t172293745 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEventTarget::ResetParameters()
extern "C"  void FsmEventTarget_ResetParameters_m4003224563 (FsmEventTarget_t172293745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
