﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ScaleTime
struct ScaleTime_t3218346659;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ScaleTime::.ctor()
extern "C"  void ScaleTime__ctor_m2234279935 (ScaleTime_t3218346659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::Reset()
extern "C"  void ScaleTime_Reset_m1933309508 (ScaleTime_t3218346659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::OnEnter()
extern "C"  void ScaleTime_OnEnter_m2192647378 (ScaleTime_t3218346659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::OnUpdate()
extern "C"  void ScaleTime_OnUpdate_m149372159 (ScaleTime_t3218346659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ScaleTime::DoTimeScale()
extern "C"  void ScaleTime_DoTimeScale_m60958537 (ScaleTime_t3218346659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
