﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveTo
struct iTweenMoveTo_t4276196252;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::.ctor()
extern "C"  void iTweenMoveTo__ctor_m1672905378 (iTweenMoveTo_t4276196252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnDrawActionGizmos()
extern "C"  void iTweenMoveTo_OnDrawActionGizmos_m4163737522 (iTweenMoveTo_t4276196252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::Reset()
extern "C"  void iTweenMoveTo_Reset_m595973249 (iTweenMoveTo_t4276196252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnEnter()
extern "C"  void iTweenMoveTo_OnEnter_m306130665 (iTweenMoveTo_t4276196252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::OnExit()
extern "C"  void iTweenMoveTo_OnExit_m1500203061 (iTweenMoveTo_t4276196252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveTo::DoiTween()
extern "C"  void iTweenMoveTo_DoiTween_m3433461559 (iTweenMoveTo_t4276196252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
