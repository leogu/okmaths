﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformRotate
struct DOTweenTransformRotate_t2860755553;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformRotate::.ctor()
extern "C"  void DOTweenTransformRotate__ctor_m181877725 (DOTweenTransformRotate_t2860755553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformRotate::Reset()
extern "C"  void DOTweenTransformRotate_Reset_m2794429830 (DOTweenTransformRotate_t2860755553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformRotate::OnEnter()
extern "C"  void DOTweenTransformRotate_OnEnter_m1391261932 (DOTweenTransformRotate_t2860755553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformRotate::<OnEnter>m__D4()
extern "C"  void DOTweenTransformRotate_U3COnEnterU3Em__D4_m1796078267 (DOTweenTransformRotate_t2860755553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformRotate::<OnEnter>m__D5()
extern "C"  void DOTweenTransformRotate_U3COnEnterU3Em__D5_m1937240768 (DOTweenTransformRotate_t2860755553 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
