﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmLog
struct FsmLog_t3672513366;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry>
struct List_1_t234398430;
// HutongGames.PlayMaker.FsmLogEntry
struct FsmLogEntry_t865277298;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry865277298.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType1161667734.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.FsmLog::.cctor()
extern "C"  void FsmLog__cctor_m1592706722 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_LoggingEnabled()
extern "C"  bool FsmLog_get_LoggingEnabled_m4195358394 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_LoggingEnabled(System.Boolean)
extern "C"  void FsmLog_set_LoggingEnabled_m2851754425 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_MirrorDebugLog()
extern "C"  bool FsmLog_get_MirrorDebugLog_m1442452588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_MirrorDebugLog(System.Boolean)
extern "C"  void FsmLog_set_MirrorDebugLog_m3810011059 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_EnableDebugFlow()
extern "C"  bool FsmLog_get_EnableDebugFlow_m1646989596 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_EnableDebugFlow(System.Boolean)
extern "C"  void FsmLog_set_EnableDebugFlow_m736591301 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmLog::get_Fsm()
extern "C"  Fsm_t917886356 * FsmLog_get_Fsm_m1271074697 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_Fsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmLog_set_Fsm_m3065325948 (FsmLog_t3672513366 * __this, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmLogEntry> HutongGames.PlayMaker.FsmLog::get_Entries()
extern "C"  List_1_t234398430 * FsmLog_get_Entries_m1578850161 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::get_Resized()
extern "C"  bool FsmLog_get_Resized_m3216083954 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_Resized(System.Boolean)
extern "C"  void FsmLog_set_Resized_m2702402949 (FsmLog_t3672513366 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::.ctor(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmLog__ctor_m2374068519 (FsmLog_t3672513366 * __this, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmLog HutongGames.PlayMaker.FsmLog::GetLog(HutongGames.PlayMaker.Fsm)
extern "C"  FsmLog_t3672513366 * FsmLog_GetLog_m1142960846 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::ClearLogs()
extern "C"  void FsmLog_ClearLogs_m3971916691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::AddEntry(HutongGames.PlayMaker.FsmLogEntry,System.Boolean)
extern "C"  void FsmLog_AddEntry_m3806006217 (FsmLog_t3672513366 * __this, FsmLogEntry_t865277298 * ___entry0, bool ___sendToUnityLog1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::IsCollisionEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmLog_IsCollisionEvent_m2314849711 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::IsTriggerEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmLog_IsTriggerEvent_m804975213 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::IsCollision2DEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmLog_IsCollision2DEvent_m862107185 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmLog::IsTrigger2DEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmLog_IsTrigger2DEvent_m3197293107 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogEvent(HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogEvent_m2057081314 (FsmLog_t3672513366 * __this, FsmEvent_t1258573736 * ___fsmEvent0, FsmState_t1643911659 * ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogSendEvent(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventTarget)
extern "C"  void FsmLog_LogSendEvent_m1649384377 (FsmLog_t3672513366 * __this, FsmState_t1643911659 * ___state0, FsmEvent_t1258573736 * ___fsmEvent1, FsmEventTarget_t172293745 * ___eventTarget2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogExitState(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogExitState_m3229085391 (FsmLog_t3672513366 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogEnterState(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogEnterState_m934049095 (FsmLog_t3672513366 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogTransition(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmTransition)
extern "C"  void FsmLog_LogTransition_m1499436216 (FsmLog_t3672513366 * __this, FsmState_t1643911659 * ___fromState0, FsmTransition_t1534990431 * ___transition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogBreak()
extern "C"  void FsmLog_LogBreak_m1047551766 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogAction(HutongGames.PlayMaker.FsmLogType,System.String,System.Boolean)
extern "C"  void FsmLog_LogAction_m75856540 (FsmLog_t3672513366 * __this, int32_t ___logType0, String_t* ___text1, bool ___sendToUnityLog2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::Log(HutongGames.PlayMaker.FsmLogType,System.String)
extern "C"  void FsmLog_Log_m3167783669 (FsmLog_t3672513366 * __this, int32_t ___logType0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogStart(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmLog_LogStart_m1325758222 (FsmLog_t3672513366 * __this, FsmState_t1643911659 * ___startState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogStop()
extern "C"  void FsmLog_LogStop_m2341376289 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::Log(System.String)
extern "C"  void FsmLog_Log_m2726931475 (FsmLog_t3672513366 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogWarning(System.String)
extern "C"  void FsmLog_LogWarning_m2735679177 (FsmLog_t3672513366 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::LogError(System.String)
extern "C"  void FsmLog_LogError_m3084508003 (FsmLog_t3672513366 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmLog::FormatUnityLogString(System.String)
extern "C"  String_t* FsmLog_FormatUnityLogString_m576280855 (FsmLog_t3672513366 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::Clear()
extern "C"  void FsmLog_Clear_m3293220642 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::OnDestroy()
extern "C"  void FsmLog_OnDestroy_m3769965590 (FsmLog_t3672513366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
