﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ActivateGameObject
struct ActivateGameObject_t2336463844;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::.ctor()
extern "C"  void ActivateGameObject__ctor_m2047820100 (ActivateGameObject_t2336463844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::Reset()
extern "C"  void ActivateGameObject_Reset_m84101997 (ActivateGameObject_t2336463844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnEnter()
extern "C"  void ActivateGameObject_OnEnter_m1122693709 (ActivateGameObject_t2336463844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnUpdate()
extern "C"  void ActivateGameObject_OnUpdate_m1753852122 (ActivateGameObject_t2336463844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::OnExit()
extern "C"  void ActivateGameObject_OnExit_m2404673401 (ActivateGameObject_t2336463844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::DoActivateGameObject()
extern "C"  void ActivateGameObject_DoActivateGameObject_m484775137 (ActivateGameObject_t2336463844 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ActivateGameObject::SetActiveRecursively(UnityEngine.GameObject,System.Boolean)
extern "C"  void ActivateGameObject_SetActiveRecursively_m2812259728 (ActivateGameObject_t2336463844 * __this, GameObject_t1756533147 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
