﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Sleep2d
struct Sleep2d_t915208513;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Sleep2d::.ctor()
extern "C"  void Sleep2d__ctor_m3669502431 (Sleep2d_t915208513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep2d::Reset()
extern "C"  void Sleep2d_Reset_m4112920578 (Sleep2d_t915208513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep2d::OnEnter()
extern "C"  void Sleep2d_OnEnter_m1785578508 (Sleep2d_t915208513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Sleep2d::DoSleep()
extern "C"  void Sleep2d_DoSleep_m4278520199 (Sleep2d_t915208513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
