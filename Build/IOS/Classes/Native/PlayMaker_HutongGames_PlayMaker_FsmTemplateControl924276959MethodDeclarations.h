﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t924276959;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// FsmTemplate
struct FsmTemplate_t1285897084;
// HutongGames.PlayMaker.FsmVarOverride[]
struct FsmVarOverrideU5BU5D_t4083454968;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTemplateControl924276959.h"
#include "PlayMaker_FsmTemplate1285897084.h"

// System.Int32 HutongGames.PlayMaker.FsmTemplateControl::get_ID()
extern "C"  int32_t FsmTemplateControl_get_ID_m858283904 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::set_ID(System.Int32)
extern "C"  void FsmTemplateControl_set_ID_m460581871 (FsmTemplateControl_t924276959 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmTemplateControl::get_RunFsm()
extern "C"  Fsm_t917886356 * FsmTemplateControl_get_RunFsm_m4245613439 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::set_RunFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmTemplateControl_set_RunFsm_m166719220 (FsmTemplateControl_t924276959 * __this, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::.ctor()
extern "C"  void FsmTemplateControl__ctor_m2769513368 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::.ctor(HutongGames.PlayMaker.FsmTemplateControl)
extern "C"  void FsmTemplateControl__ctor_m3840995355 (FsmTemplateControl_t924276959 * __this, FsmTemplateControl_t924276959 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::SetFsmTemplate(FsmTemplate)
extern "C"  void FsmTemplateControl_SetFsmTemplate_m3967116436 (FsmTemplateControl_t924276959 * __this, FsmTemplate_t1285897084 * ___template0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmTemplateControl::InstantiateFsm()
extern "C"  Fsm_t917886356 * FsmTemplateControl_InstantiateFsm_m4229157401 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVarOverride[] HutongGames.PlayMaker.FsmTemplateControl::CopyOverrides(HutongGames.PlayMaker.FsmTemplateControl)
extern "C"  FsmVarOverrideU5BU5D_t4083454968* FsmTemplateControl_CopyOverrides_m1446135747 (Il2CppObject * __this /* static, unused */, FsmTemplateControl_t924276959 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::ClearOverrides()
extern "C"  void FsmTemplateControl_ClearOverrides_m1640822924 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::UpdateOverrides()
extern "C"  void FsmTemplateControl_UpdateOverrides_m2569056422 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::UpdateValues()
extern "C"  void FsmTemplateControl_UpdateValues_m3784608603 (FsmTemplateControl_t924276959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmTemplateControl::ApplyOverrides(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmTemplateControl_ApplyOverrides_m2902926407 (FsmTemplateControl_t924276959 * __this, Fsm_t917886356 * ___overrideFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
