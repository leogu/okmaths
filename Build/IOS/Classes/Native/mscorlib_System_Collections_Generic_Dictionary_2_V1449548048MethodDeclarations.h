﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2359028745MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m570686599(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1449548048 *, Dictionary_2_t2746488205 *, const MethodInfo*))ValueCollection__ctor_m1875345419_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3852722357(__this, ___item0, method) ((  void (*) (ValueCollection_t1449548048 *, RaycastHit2D_t4063908774 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m835338641_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m764378290(__this, method) ((  void (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m560067856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1869475179(__this, ___item0, method) ((  bool (*) (ValueCollection_t1449548048 *, RaycastHit2D_t4063908774 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3875449127_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m671268944(__this, ___item0, method) ((  bool (*) (ValueCollection_t1449548048 *, RaycastHit2D_t4063908774 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3698227602_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4206419828(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1327821650_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3869163386(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1449548048 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1087717004_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2831135653(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2795042721_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m67907980(__this, method) ((  bool (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1278818006_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1705358426(__this, method) ((  bool (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2686570292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m293713684(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1507176018_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m132884090(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1449548048 *, RaycastHit2DU5BU5D_t4176517891*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m1572297112_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3425700351(__this, method) ((  Enumerator_t138053673  (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_GetEnumerator_m3932942483_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Count()
#define ValueCollection_get_Count_m212821878(__this, method) ((  int32_t (*) (ValueCollection_t1449548048 *, const MethodInfo*))ValueCollection_get_Count_m2014429424_gshared)(__this, method)
