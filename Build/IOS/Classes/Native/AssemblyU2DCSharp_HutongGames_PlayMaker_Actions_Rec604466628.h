﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fs1919058365.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformGetRect
struct  RectTransformGetRect_t604466628  : public FsmStateActionAdvanced_t1919058365
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformGetRect::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Actions.RectTransformGetRect::rect
	FsmRect_t19023354 * ___rect_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::x
	FsmFloat_t937133978 * ___x_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::y
	FsmFloat_t937133978 * ___y_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::width
	FsmFloat_t937133978 * ___width_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RectTransformGetRect::height
	FsmFloat_t937133978 * ___height_18;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformGetRect::_rt
	RectTransform_t3349966182 * ____rt_19;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_rect_14() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ___rect_14)); }
	inline FsmRect_t19023354 * get_rect_14() const { return ___rect_14; }
	inline FsmRect_t19023354 ** get_address_of_rect_14() { return &___rect_14; }
	inline void set_rect_14(FsmRect_t19023354 * value)
	{
		___rect_14 = value;
		Il2CppCodeGenWriteBarrier(&___rect_14, value);
	}

	inline static int32_t get_offset_of_x_15() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ___x_15)); }
	inline FsmFloat_t937133978 * get_x_15() const { return ___x_15; }
	inline FsmFloat_t937133978 ** get_address_of_x_15() { return &___x_15; }
	inline void set_x_15(FsmFloat_t937133978 * value)
	{
		___x_15 = value;
		Il2CppCodeGenWriteBarrier(&___x_15, value);
	}

	inline static int32_t get_offset_of_y_16() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ___y_16)); }
	inline FsmFloat_t937133978 * get_y_16() const { return ___y_16; }
	inline FsmFloat_t937133978 ** get_address_of_y_16() { return &___y_16; }
	inline void set_y_16(FsmFloat_t937133978 * value)
	{
		___y_16 = value;
		Il2CppCodeGenWriteBarrier(&___y_16, value);
	}

	inline static int32_t get_offset_of_width_17() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ___width_17)); }
	inline FsmFloat_t937133978 * get_width_17() const { return ___width_17; }
	inline FsmFloat_t937133978 ** get_address_of_width_17() { return &___width_17; }
	inline void set_width_17(FsmFloat_t937133978 * value)
	{
		___width_17 = value;
		Il2CppCodeGenWriteBarrier(&___width_17, value);
	}

	inline static int32_t get_offset_of_height_18() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ___height_18)); }
	inline FsmFloat_t937133978 * get_height_18() const { return ___height_18; }
	inline FsmFloat_t937133978 ** get_address_of_height_18() { return &___height_18; }
	inline void set_height_18(FsmFloat_t937133978 * value)
	{
		___height_18 = value;
		Il2CppCodeGenWriteBarrier(&___height_18, value);
	}

	inline static int32_t get_offset_of__rt_19() { return static_cast<int32_t>(offsetof(RectTransformGetRect_t604466628, ____rt_19)); }
	inline RectTransform_t3349966182 * get__rt_19() const { return ____rt_19; }
	inline RectTransform_t3349966182 ** get_address_of__rt_19() { return &____rt_19; }
	inline void set__rt_19(RectTransform_t3349966182 * value)
	{
		____rt_19 = value;
		Il2CppCodeGenWriteBarrier(&____rt_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
