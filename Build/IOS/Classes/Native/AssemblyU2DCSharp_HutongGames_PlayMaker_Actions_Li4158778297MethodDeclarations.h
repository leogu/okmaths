﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.LineCast2d
struct LineCast2d_t4158778297;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.LineCast2d::.ctor()
extern "C"  void LineCast2d__ctor_m1089876807 (LineCast2d_t4158778297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::Reset()
extern "C"  void LineCast2d_Reset_m2776667238 (LineCast2d_t4158778297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::OnEnter()
extern "C"  void LineCast2d_OnEnter_m4280162032 (LineCast2d_t4158778297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::OnUpdate()
extern "C"  void LineCast2d_OnUpdate_m3598437095 (LineCast2d_t4158778297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.LineCast2d::DoRaycast()
extern "C"  void LineCast2d_DoRaycast_m4137100927 (LineCast2d_t4158778297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
