﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiTextSetText
struct uGuiTextSetText_t2405364386;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiTextSetText::.ctor()
extern "C"  void uGuiTextSetText__ctor_m3030596286 (uGuiTextSetText_t2405364386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextSetText::Reset()
extern "C"  void uGuiTextSetText_Reset_m2692488387 (uGuiTextSetText_t2405364386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextSetText::OnEnter()
extern "C"  void uGuiTextSetText_OnEnter_m360757043 (uGuiTextSetText_t2405364386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextSetText::OnUpdate()
extern "C"  void uGuiTextSetText_OnUpdate_m551321472 (uGuiTextSetText_t2405364386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextSetText::DoSetTextValue()
extern "C"  void uGuiTextSetText_DoSetTextValue_m1849874181 (uGuiTextSetText_t2405364386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTextSetText::OnExit()
extern "C"  void uGuiTextSetText_OnExit_m1721537579 (uGuiTextSetText_t2405364386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
