﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSwapItems
struct ArrayListSwapItems_t1245773342;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSwapItems::.ctor()
extern "C"  void ArrayListSwapItems__ctor_m4000254538 (ArrayListSwapItems_t1245773342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSwapItems::Reset()
extern "C"  void ArrayListSwapItems_Reset_m1423733507 (ArrayListSwapItems_t1245773342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSwapItems::OnEnter()
extern "C"  void ArrayListSwapItems_OnEnter_m1032550083 (ArrayListSwapItems_t1245773342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSwapItems::doArrayListSwap()
extern "C"  void ArrayListSwapItems_doArrayListSwap_m1911855351 (ArrayListSwapItems_t1245773342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
