﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorPivot
struct GetAnimatorPivot_t3860494805;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::.ctor()
extern "C"  void GetAnimatorPivot__ctor_m609116083 (GetAnimatorPivot_t3860494805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::Reset()
extern "C"  void GetAnimatorPivot_Reset_m2334096378 (GetAnimatorPivot_t3860494805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnEnter()
extern "C"  void GetAnimatorPivot_OnEnter_m1025487180 (GetAnimatorPivot_t3860494805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::OnActionUpdate()
extern "C"  void GetAnimatorPivot_OnActionUpdate_m2102049733 (GetAnimatorPivot_t3860494805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorPivot::DoCheckPivot()
extern "C"  void GetAnimatorPivot_DoCheckPivot_m2909979092 (GetAnimatorPivot_t3860494805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
