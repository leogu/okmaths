﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs
struct NetworkGetMinimumAllocatableViewIDs_t3996160495;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs::.ctor()
extern "C"  void NetworkGetMinimumAllocatableViewIDs__ctor_m1037971981 (NetworkGetMinimumAllocatableViewIDs_t3996160495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs::Reset()
extern "C"  void NetworkGetMinimumAllocatableViewIDs_Reset_m4227851144 (NetworkGetMinimumAllocatableViewIDs_t3996160495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetMinimumAllocatableViewIDs::OnEnter()
extern "C"  void NetworkGetMinimumAllocatableViewIDs_OnEnter_m891738122 (NetworkGetMinimumAllocatableViewIDs_t3996160495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
