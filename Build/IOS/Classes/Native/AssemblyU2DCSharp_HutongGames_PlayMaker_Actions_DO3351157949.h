﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// System.String
struct String_t;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat
struct  DOTweenAudioMixerSetFloat_t3351157949  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// System.String HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::descriptionArea
	String_t* ___descriptionArea_12;
	// UnityEngine.Audio.AudioMixer HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::audioMixer
	AudioMixer_t3244290001 * ___audioMixer_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::floatName
	FsmString_t2414474701 * ___floatName_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::to
	FsmFloat_t937133978 * ___to_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::setRelative
	FsmBool_t664485696 * ___setRelative_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::duration
	FsmFloat_t937133978 * ___duration_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::startDelay
	FsmFloat_t937133978 * ___startDelay_19;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::playInReverse
	FsmBool_t664485696 * ___playInReverse_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::setReverseRelative
	FsmBool_t664485696 * ___setReverseRelative_21;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::startEvent
	FsmEvent_t1258573736 * ___startEvent_22;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_24;
	// System.String HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::tweenIdDescription
	String_t* ___tweenIdDescription_25;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::tweenIdType
	int32_t ___tweenIdType_26;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::stringAsId
	FsmString_t2414474701 * ___stringAsId_27;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::tagAsId
	FsmString_t2414474701 * ___tagAsId_28;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::selectedEase
	int32_t ___selectedEase_29;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::easeType
	int32_t ___easeType_30;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_31;
	// System.String HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_32;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::loops
	FsmInt_t1273009179 * ___loops_33;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::loopType
	int32_t ___loopType_34;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_35;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::recyclable
	FsmBool_t664485696 * ___recyclable_36;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::updateType
	int32_t ___updateType_37;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_38;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::debugThis
	FsmBool_t664485696 * ___debugThis_39;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenAudioMixerSetFloat::tweener
	Tweener_t760404022 * ___tweener_40;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_descriptionArea_12() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___descriptionArea_12)); }
	inline String_t* get_descriptionArea_12() const { return ___descriptionArea_12; }
	inline String_t** get_address_of_descriptionArea_12() { return &___descriptionArea_12; }
	inline void set_descriptionArea_12(String_t* value)
	{
		___descriptionArea_12 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionArea_12, value);
	}

	inline static int32_t get_offset_of_audioMixer_13() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___audioMixer_13)); }
	inline AudioMixer_t3244290001 * get_audioMixer_13() const { return ___audioMixer_13; }
	inline AudioMixer_t3244290001 ** get_address_of_audioMixer_13() { return &___audioMixer_13; }
	inline void set_audioMixer_13(AudioMixer_t3244290001 * value)
	{
		___audioMixer_13 = value;
		Il2CppCodeGenWriteBarrier(&___audioMixer_13, value);
	}

	inline static int32_t get_offset_of_floatName_14() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___floatName_14)); }
	inline FsmString_t2414474701 * get_floatName_14() const { return ___floatName_14; }
	inline FsmString_t2414474701 ** get_address_of_floatName_14() { return &___floatName_14; }
	inline void set_floatName_14(FsmString_t2414474701 * value)
	{
		___floatName_14 = value;
		Il2CppCodeGenWriteBarrier(&___floatName_14, value);
	}

	inline static int32_t get_offset_of_to_15() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___to_15)); }
	inline FsmFloat_t937133978 * get_to_15() const { return ___to_15; }
	inline FsmFloat_t937133978 ** get_address_of_to_15() { return &___to_15; }
	inline void set_to_15(FsmFloat_t937133978 * value)
	{
		___to_15 = value;
		Il2CppCodeGenWriteBarrier(&___to_15, value);
	}

	inline static int32_t get_offset_of_setRelative_16() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___setRelative_16)); }
	inline FsmBool_t664485696 * get_setRelative_16() const { return ___setRelative_16; }
	inline FsmBool_t664485696 ** get_address_of_setRelative_16() { return &___setRelative_16; }
	inline void set_setRelative_16(FsmBool_t664485696 * value)
	{
		___setRelative_16 = value;
		Il2CppCodeGenWriteBarrier(&___setRelative_16, value);
	}

	inline static int32_t get_offset_of_duration_17() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___duration_17)); }
	inline FsmFloat_t937133978 * get_duration_17() const { return ___duration_17; }
	inline FsmFloat_t937133978 ** get_address_of_duration_17() { return &___duration_17; }
	inline void set_duration_17(FsmFloat_t937133978 * value)
	{
		___duration_17 = value;
		Il2CppCodeGenWriteBarrier(&___duration_17, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_18() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___setSpeedBased_18)); }
	inline FsmBool_t664485696 * get_setSpeedBased_18() const { return ___setSpeedBased_18; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_18() { return &___setSpeedBased_18; }
	inline void set_setSpeedBased_18(FsmBool_t664485696 * value)
	{
		___setSpeedBased_18 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_18, value);
	}

	inline static int32_t get_offset_of_startDelay_19() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___startDelay_19)); }
	inline FsmFloat_t937133978 * get_startDelay_19() const { return ___startDelay_19; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_19() { return &___startDelay_19; }
	inline void set_startDelay_19(FsmFloat_t937133978 * value)
	{
		___startDelay_19 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_19, value);
	}

	inline static int32_t get_offset_of_playInReverse_20() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___playInReverse_20)); }
	inline FsmBool_t664485696 * get_playInReverse_20() const { return ___playInReverse_20; }
	inline FsmBool_t664485696 ** get_address_of_playInReverse_20() { return &___playInReverse_20; }
	inline void set_playInReverse_20(FsmBool_t664485696 * value)
	{
		___playInReverse_20 = value;
		Il2CppCodeGenWriteBarrier(&___playInReverse_20, value);
	}

	inline static int32_t get_offset_of_setReverseRelative_21() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___setReverseRelative_21)); }
	inline FsmBool_t664485696 * get_setReverseRelative_21() const { return ___setReverseRelative_21; }
	inline FsmBool_t664485696 ** get_address_of_setReverseRelative_21() { return &___setReverseRelative_21; }
	inline void set_setReverseRelative_21(FsmBool_t664485696 * value)
	{
		___setReverseRelative_21 = value;
		Il2CppCodeGenWriteBarrier(&___setReverseRelative_21, value);
	}

	inline static int32_t get_offset_of_startEvent_22() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___startEvent_22)); }
	inline FsmEvent_t1258573736 * get_startEvent_22() const { return ___startEvent_22; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_22() { return &___startEvent_22; }
	inline void set_startEvent_22(FsmEvent_t1258573736 * value)
	{
		___startEvent_22 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_22, value);
	}

	inline static int32_t get_offset_of_finishEvent_23() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___finishEvent_23)); }
	inline FsmEvent_t1258573736 * get_finishEvent_23() const { return ___finishEvent_23; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_23() { return &___finishEvent_23; }
	inline void set_finishEvent_23(FsmEvent_t1258573736 * value)
	{
		___finishEvent_23 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_23, value);
	}

	inline static int32_t get_offset_of_finishImmediately_24() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___finishImmediately_24)); }
	inline FsmBool_t664485696 * get_finishImmediately_24() const { return ___finishImmediately_24; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_24() { return &___finishImmediately_24; }
	inline void set_finishImmediately_24(FsmBool_t664485696 * value)
	{
		___finishImmediately_24 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_24, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_25() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___tweenIdDescription_25)); }
	inline String_t* get_tweenIdDescription_25() const { return ___tweenIdDescription_25; }
	inline String_t** get_address_of_tweenIdDescription_25() { return &___tweenIdDescription_25; }
	inline void set_tweenIdDescription_25(String_t* value)
	{
		___tweenIdDescription_25 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_25, value);
	}

	inline static int32_t get_offset_of_tweenIdType_26() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___tweenIdType_26)); }
	inline int32_t get_tweenIdType_26() const { return ___tweenIdType_26; }
	inline int32_t* get_address_of_tweenIdType_26() { return &___tweenIdType_26; }
	inline void set_tweenIdType_26(int32_t value)
	{
		___tweenIdType_26 = value;
	}

	inline static int32_t get_offset_of_stringAsId_27() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___stringAsId_27)); }
	inline FsmString_t2414474701 * get_stringAsId_27() const { return ___stringAsId_27; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_27() { return &___stringAsId_27; }
	inline void set_stringAsId_27(FsmString_t2414474701 * value)
	{
		___stringAsId_27 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_27, value);
	}

	inline static int32_t get_offset_of_tagAsId_28() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___tagAsId_28)); }
	inline FsmString_t2414474701 * get_tagAsId_28() const { return ___tagAsId_28; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_28() { return &___tagAsId_28; }
	inline void set_tagAsId_28(FsmString_t2414474701 * value)
	{
		___tagAsId_28 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_28, value);
	}

	inline static int32_t get_offset_of_selectedEase_29() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___selectedEase_29)); }
	inline int32_t get_selectedEase_29() const { return ___selectedEase_29; }
	inline int32_t* get_address_of_selectedEase_29() { return &___selectedEase_29; }
	inline void set_selectedEase_29(int32_t value)
	{
		___selectedEase_29 = value;
	}

	inline static int32_t get_offset_of_easeType_30() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___easeType_30)); }
	inline int32_t get_easeType_30() const { return ___easeType_30; }
	inline int32_t* get_address_of_easeType_30() { return &___easeType_30; }
	inline void set_easeType_30(int32_t value)
	{
		___easeType_30 = value;
	}

	inline static int32_t get_offset_of_animationCurve_31() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___animationCurve_31)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_31() const { return ___animationCurve_31; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_31() { return &___animationCurve_31; }
	inline void set_animationCurve_31(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_31 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_31, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_32() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___loopsDescriptionArea_32)); }
	inline String_t* get_loopsDescriptionArea_32() const { return ___loopsDescriptionArea_32; }
	inline String_t** get_address_of_loopsDescriptionArea_32() { return &___loopsDescriptionArea_32; }
	inline void set_loopsDescriptionArea_32(String_t* value)
	{
		___loopsDescriptionArea_32 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_32, value);
	}

	inline static int32_t get_offset_of_loops_33() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___loops_33)); }
	inline FsmInt_t1273009179 * get_loops_33() const { return ___loops_33; }
	inline FsmInt_t1273009179 ** get_address_of_loops_33() { return &___loops_33; }
	inline void set_loops_33(FsmInt_t1273009179 * value)
	{
		___loops_33 = value;
		Il2CppCodeGenWriteBarrier(&___loops_33, value);
	}

	inline static int32_t get_offset_of_loopType_34() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___loopType_34)); }
	inline int32_t get_loopType_34() const { return ___loopType_34; }
	inline int32_t* get_address_of_loopType_34() { return &___loopType_34; }
	inline void set_loopType_34(int32_t value)
	{
		___loopType_34 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_35() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___autoKillOnCompletion_35)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_35() const { return ___autoKillOnCompletion_35; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_35() { return &___autoKillOnCompletion_35; }
	inline void set_autoKillOnCompletion_35(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_35 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_35, value);
	}

	inline static int32_t get_offset_of_recyclable_36() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___recyclable_36)); }
	inline FsmBool_t664485696 * get_recyclable_36() const { return ___recyclable_36; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_36() { return &___recyclable_36; }
	inline void set_recyclable_36(FsmBool_t664485696 * value)
	{
		___recyclable_36 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_36, value);
	}

	inline static int32_t get_offset_of_updateType_37() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___updateType_37)); }
	inline int32_t get_updateType_37() const { return ___updateType_37; }
	inline int32_t* get_address_of_updateType_37() { return &___updateType_37; }
	inline void set_updateType_37(int32_t value)
	{
		___updateType_37 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_38() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___isIndependentUpdate_38)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_38() const { return ___isIndependentUpdate_38; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_38() { return &___isIndependentUpdate_38; }
	inline void set_isIndependentUpdate_38(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_38 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_38, value);
	}

	inline static int32_t get_offset_of_debugThis_39() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___debugThis_39)); }
	inline FsmBool_t664485696 * get_debugThis_39() const { return ___debugThis_39; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_39() { return &___debugThis_39; }
	inline void set_debugThis_39(FsmBool_t664485696 * value)
	{
		___debugThis_39 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_39, value);
	}

	inline static int32_t get_offset_of_tweener_40() { return static_cast<int32_t>(offsetof(DOTweenAudioMixerSetFloat_t3351157949, ___tweener_40)); }
	inline Tweener_t760404022 * get_tweener_40() const { return ___tweener_40; }
	inline Tweener_t760404022 ** get_address_of_tweener_40() { return &___tweener_40; }
	inline void set_tweener_40(Tweener_t760404022 * value)
	{
		___tweener_40 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_40, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
