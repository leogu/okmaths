﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RemoveMixingTransform
struct RemoveMixingTransform_t2841934220;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RemoveMixingTransform::.ctor()
extern "C"  void RemoveMixingTransform__ctor_m1674071184 (RemoveMixingTransform_t2841934220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RemoveMixingTransform::Reset()
extern "C"  void RemoveMixingTransform_Reset_m674614725 (RemoveMixingTransform_t2841934220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RemoveMixingTransform::OnEnter()
extern "C"  void RemoveMixingTransform_OnEnter_m580009245 (RemoveMixingTransform_t2841934220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RemoveMixingTransform::DoRemoveMixingTransform()
extern "C"  void RemoveMixingTransform_DoRemoveMixingTransform_m3431249005 (RemoveMixingTransform_t2841934220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
