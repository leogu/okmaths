﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformBlendableRotateBy
struct DOTweenTransformBlendableRotateBy_t1506494913;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableRotateBy::.ctor()
extern "C"  void DOTweenTransformBlendableRotateBy__ctor_m2262433001 (DOTweenTransformBlendableRotateBy_t1506494913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableRotateBy::Reset()
extern "C"  void DOTweenTransformBlendableRotateBy_Reset_m1240659830 (DOTweenTransformBlendableRotateBy_t1506494913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableRotateBy::OnEnter()
extern "C"  void DOTweenTransformBlendableRotateBy_OnEnter_m342640748 (DOTweenTransformBlendableRotateBy_t1506494913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableRotateBy::<OnEnter>m__AC()
extern "C"  void DOTweenTransformBlendableRotateBy_U3COnEnterU3Em__AC_m145797417 (DOTweenTransformBlendableRotateBy_t1506494913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableRotateBy::<OnEnter>m__AD()
extern "C"  void DOTweenTransformBlendableRotateBy_U3COnEnterU3Em__AD_m145797318 (DOTweenTransformBlendableRotateBy_t1506494913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
