﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.CommonDivisorMultiple
struct  CommonDivisorMultiple_t2931552185  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.CommonDivisorMultiple::integer1
	FsmInt_t1273009179 * ___integer1_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.CommonDivisorMultiple::integer2
	FsmInt_t1273009179 * ___integer2_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.CommonDivisorMultiple::storeLC_Divisor
	FsmInt_t1273009179 * ___storeLC_Divisor_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.CommonDivisorMultiple::storeGC_Multiple
	FsmInt_t1273009179 * ___storeGC_Multiple_14;
	// System.Boolean HutongGames.PlayMaker.Actions.CommonDivisorMultiple::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_integer1_11() { return static_cast<int32_t>(offsetof(CommonDivisorMultiple_t2931552185, ___integer1_11)); }
	inline FsmInt_t1273009179 * get_integer1_11() const { return ___integer1_11; }
	inline FsmInt_t1273009179 ** get_address_of_integer1_11() { return &___integer1_11; }
	inline void set_integer1_11(FsmInt_t1273009179 * value)
	{
		___integer1_11 = value;
		Il2CppCodeGenWriteBarrier(&___integer1_11, value);
	}

	inline static int32_t get_offset_of_integer2_12() { return static_cast<int32_t>(offsetof(CommonDivisorMultiple_t2931552185, ___integer2_12)); }
	inline FsmInt_t1273009179 * get_integer2_12() const { return ___integer2_12; }
	inline FsmInt_t1273009179 ** get_address_of_integer2_12() { return &___integer2_12; }
	inline void set_integer2_12(FsmInt_t1273009179 * value)
	{
		___integer2_12 = value;
		Il2CppCodeGenWriteBarrier(&___integer2_12, value);
	}

	inline static int32_t get_offset_of_storeLC_Divisor_13() { return static_cast<int32_t>(offsetof(CommonDivisorMultiple_t2931552185, ___storeLC_Divisor_13)); }
	inline FsmInt_t1273009179 * get_storeLC_Divisor_13() const { return ___storeLC_Divisor_13; }
	inline FsmInt_t1273009179 ** get_address_of_storeLC_Divisor_13() { return &___storeLC_Divisor_13; }
	inline void set_storeLC_Divisor_13(FsmInt_t1273009179 * value)
	{
		___storeLC_Divisor_13 = value;
		Il2CppCodeGenWriteBarrier(&___storeLC_Divisor_13, value);
	}

	inline static int32_t get_offset_of_storeGC_Multiple_14() { return static_cast<int32_t>(offsetof(CommonDivisorMultiple_t2931552185, ___storeGC_Multiple_14)); }
	inline FsmInt_t1273009179 * get_storeGC_Multiple_14() const { return ___storeGC_Multiple_14; }
	inline FsmInt_t1273009179 ** get_address_of_storeGC_Multiple_14() { return &___storeGC_Multiple_14; }
	inline void set_storeGC_Multiple_14(FsmInt_t1273009179 * value)
	{
		___storeGC_Multiple_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeGC_Multiple_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(CommonDivisorMultiple_t2931552185, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
