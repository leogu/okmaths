﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ApplicationQuit
struct ApplicationQuit_t772909113;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::.ctor()
extern "C"  void ApplicationQuit__ctor_m684239173 (ApplicationQuit_t772909113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::Reset()
extern "C"  void ApplicationQuit_Reset_m2926277586 (ApplicationQuit_t772909113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ApplicationQuit::OnEnter()
extern "C"  void ApplicationQuit_OnEnter_m2747518232 (ApplicationQuit_t772909113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
