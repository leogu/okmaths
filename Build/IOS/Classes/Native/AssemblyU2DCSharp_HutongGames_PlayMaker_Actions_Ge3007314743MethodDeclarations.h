﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight
struct GetAnimatorRightFootBottomHeight_t3007314743;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::.ctor()
extern "C"  void GetAnimatorRightFootBottomHeight__ctor_m426491441 (GetAnimatorRightFootBottomHeight_t3007314743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::Reset()
extern "C"  void GetAnimatorRightFootBottomHeight_Reset_m2756386112 (GetAnimatorRightFootBottomHeight_t3007314743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::OnEnter()
extern "C"  void GetAnimatorRightFootBottomHeight_OnEnter_m3378472290 (GetAnimatorRightFootBottomHeight_t3007314743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::OnLateUpdate()
extern "C"  void GetAnimatorRightFootBottomHeight_OnLateUpdate_m4051204625 (GetAnimatorRightFootBottomHeight_t3007314743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorRightFootBottomHeight::_getRightFootBottonHeight()
extern "C"  void GetAnimatorRightFootBottomHeight__getRightFootBottonHeight_m2239363037 (GetAnimatorRightFootBottomHeight_t3007314743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
