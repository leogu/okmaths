﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightFlare
struct SetLightFlare_t118845802;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::.ctor()
extern "C"  void SetLightFlare__ctor_m2823877650 (SetLightFlare_t118845802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::Reset()
extern "C"  void SetLightFlare_Reset_m3529992863 (SetLightFlare_t118845802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::OnEnter()
extern "C"  void SetLightFlare_OnEnter_m1257001143 (SetLightFlare_t118845802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightFlare::DoSetLightRange()
extern "C"  void SetLightFlare_DoSetLightRange_m2955202660 (SetLightFlare_t118845802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
