﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Interpolate
struct Vector2Interpolate_t1996302654;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Interpolate::.ctor()
extern "C"  void Vector2Interpolate__ctor_m3567756078 (Vector2Interpolate_t1996302654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Interpolate::Reset()
extern "C"  void Vector2Interpolate_Reset_m1231608335 (Vector2Interpolate_t1996302654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Interpolate::OnEnter()
extern "C"  void Vector2Interpolate_OnEnter_m2882964519 (Vector2Interpolate_t1996302654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Interpolate::OnUpdate()
extern "C"  void Vector2Interpolate_OnUpdate_m3201035488 (Vector2Interpolate_t1996302654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
