﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableValues
struct  HashTableValues_t2480936398  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableValues::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableValues::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableValues::arrayListGameObject
	FsmOwnerDefault_t2023674184 * ___arrayListGameObject_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableValues::arrayListReference
	FsmString_t2414474701 * ___arrayListReference_15;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableValues_t2480936398, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableValues_t2480936398, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_arrayListGameObject_14() { return static_cast<int32_t>(offsetof(HashTableValues_t2480936398, ___arrayListGameObject_14)); }
	inline FsmOwnerDefault_t2023674184 * get_arrayListGameObject_14() const { return ___arrayListGameObject_14; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_arrayListGameObject_14() { return &___arrayListGameObject_14; }
	inline void set_arrayListGameObject_14(FsmOwnerDefault_t2023674184 * value)
	{
		___arrayListGameObject_14 = value;
		Il2CppCodeGenWriteBarrier(&___arrayListGameObject_14, value);
	}

	inline static int32_t get_offset_of_arrayListReference_15() { return static_cast<int32_t>(offsetof(HashTableValues_t2480936398, ___arrayListReference_15)); }
	inline FsmString_t2414474701 * get_arrayListReference_15() const { return ___arrayListReference_15; }
	inline FsmString_t2414474701 ** get_address_of_arrayListReference_15() { return &___arrayListReference_15; }
	inline void set_arrayListReference_15(FsmString_t2414474701 * value)
	{
		___arrayListReference_15 = value;
		Il2CppCodeGenWriteBarrier(&___arrayListReference_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
