﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView
struct GUILayoutBeginScrollView_t3359579804;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::.ctor()
extern "C"  void GUILayoutBeginScrollView__ctor_m2053448182 (GUILayoutBeginScrollView_t3359579804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::Reset()
extern "C"  void GUILayoutBeginScrollView_Reset_m1261852389 (GUILayoutBeginScrollView_t3359579804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginScrollView::OnGUI()
extern "C"  void GUILayoutBeginScrollView_OnGUI_m993295994 (GUILayoutBeginScrollView_t3359579804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
