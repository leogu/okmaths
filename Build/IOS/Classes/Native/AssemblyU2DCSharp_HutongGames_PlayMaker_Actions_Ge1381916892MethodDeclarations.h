﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetButtonDown
struct GetButtonDown_t1381916892;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::.ctor()
extern "C"  void GetButtonDown__ctor_m1141424014 (GetButtonDown_t1381916892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::Reset()
extern "C"  void GetButtonDown_Reset_m3445692337 (GetButtonDown_t1381916892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetButtonDown::OnUpdate()
extern "C"  void GetButtonDown_OnUpdate_m284093664 (GetButtonDown_t1381916892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
