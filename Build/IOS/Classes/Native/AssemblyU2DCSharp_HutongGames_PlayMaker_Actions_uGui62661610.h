﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.InputField
struct InputField_t1631627530;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt
struct  uGuiInputFieldGetTextAsInt_t62661610  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::value
	FsmInt_t1273009179 * ___value_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::isInt
	FsmBool_t664485696 * ___isInt_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::isIntEvent
	FsmEvent_t1258573736 * ___isIntEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::isNotIntEvent
	FsmEvent_t1258573736 * ___isNotIntEvent_15;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::everyFrame
	bool ___everyFrame_16;
	// UnityEngine.UI.InputField HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::_inputField
	InputField_t1631627530 * ____inputField_17;
	// System.Int32 HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::_value
	int32_t ____value_18;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiInputFieldGetTextAsInt::_success
	bool ____success_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_value_12() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ___value_12)); }
	inline FsmInt_t1273009179 * get_value_12() const { return ___value_12; }
	inline FsmInt_t1273009179 ** get_address_of_value_12() { return &___value_12; }
	inline void set_value_12(FsmInt_t1273009179 * value)
	{
		___value_12 = value;
		Il2CppCodeGenWriteBarrier(&___value_12, value);
	}

	inline static int32_t get_offset_of_isInt_13() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ___isInt_13)); }
	inline FsmBool_t664485696 * get_isInt_13() const { return ___isInt_13; }
	inline FsmBool_t664485696 ** get_address_of_isInt_13() { return &___isInt_13; }
	inline void set_isInt_13(FsmBool_t664485696 * value)
	{
		___isInt_13 = value;
		Il2CppCodeGenWriteBarrier(&___isInt_13, value);
	}

	inline static int32_t get_offset_of_isIntEvent_14() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ___isIntEvent_14)); }
	inline FsmEvent_t1258573736 * get_isIntEvent_14() const { return ___isIntEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isIntEvent_14() { return &___isIntEvent_14; }
	inline void set_isIntEvent_14(FsmEvent_t1258573736 * value)
	{
		___isIntEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isIntEvent_14, value);
	}

	inline static int32_t get_offset_of_isNotIntEvent_15() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ___isNotIntEvent_15)); }
	inline FsmEvent_t1258573736 * get_isNotIntEvent_15() const { return ___isNotIntEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotIntEvent_15() { return &___isNotIntEvent_15; }
	inline void set_isNotIntEvent_15(FsmEvent_t1258573736 * value)
	{
		___isNotIntEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___isNotIntEvent_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__inputField_17() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ____inputField_17)); }
	inline InputField_t1631627530 * get__inputField_17() const { return ____inputField_17; }
	inline InputField_t1631627530 ** get_address_of__inputField_17() { return &____inputField_17; }
	inline void set__inputField_17(InputField_t1631627530 * value)
	{
		____inputField_17 = value;
		Il2CppCodeGenWriteBarrier(&____inputField_17, value);
	}

	inline static int32_t get_offset_of__value_18() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ____value_18)); }
	inline int32_t get__value_18() const { return ____value_18; }
	inline int32_t* get_address_of__value_18() { return &____value_18; }
	inline void set__value_18(int32_t value)
	{
		____value_18 = value;
	}

	inline static int32_t get_offset_of__success_19() { return static_cast<int32_t>(offsetof(uGuiInputFieldGetTextAsInt_t62661610, ____success_19)); }
	inline bool get__success_19() const { return ____success_19; }
	inline bool* get_address_of__success_19() { return &____success_19; }
	inline void set__success_19(bool value)
	{
		____success_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
