﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiTransitionSetType
struct uGuiTransitionSetType_t1899906747;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionSetType::.ctor()
extern "C"  void uGuiTransitionSetType__ctor_m2211747137 (uGuiTransitionSetType_t1899906747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionSetType::Reset()
extern "C"  void uGuiTransitionSetType_Reset_m2695428980 (uGuiTransitionSetType_t1899906747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionSetType::OnEnter()
extern "C"  void uGuiTransitionSetType_OnEnter_m1059004806 (uGuiTransitionSetType_t1899906747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionSetType::DoSetValue()
extern "C"  void uGuiTransitionSetType_DoSetValue_m274555139 (uGuiTransitionSetType_t1899906747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiTransitionSetType::OnExit()
extern "C"  void uGuiTransitionSetType_OnExit_m4171584018 (uGuiTransitionSetType_t1899906747 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
