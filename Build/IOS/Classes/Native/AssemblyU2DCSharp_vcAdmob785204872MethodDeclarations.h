﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// vcAdmob
struct vcAdmob_t785204872;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void vcAdmob::.ctor()
extern "C"  void vcAdmob__ctor_m1738613815 (vcAdmob_t785204872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::Start()
extern "C"  void vcAdmob_Start_m4130886627 (vcAdmob_t785204872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::Update()
extern "C"  void vcAdmob_Update_m2647775210 (vcAdmob_t785204872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::initAdmob()
extern "C"  void vcAdmob_initAdmob_m831542312 (vcAdmob_t785204872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::ShowAdBanner()
extern "C"  void vcAdmob_ShowAdBanner_m2202699501 (vcAdmob_t785204872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::onInterstitialEvent(System.String,System.String)
extern "C"  void vcAdmob_onInterstitialEvent_m4119337184 (vcAdmob_t785204872 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::onBannerEvent(System.String,System.String)
extern "C"  void vcAdmob_onBannerEvent_m1200778356 (vcAdmob_t785204872 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::onRewardedVideoEvent(System.String,System.String)
extern "C"  void vcAdmob_onRewardedVideoEvent_m2639597317 (vcAdmob_t785204872 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcAdmob::onNativeBannerEvent(System.String,System.String)
extern "C"  void vcAdmob_onNativeBannerEvent_m3390000147 (vcAdmob_t785204872 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
