﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformPunchRotation
struct DOTweenTransformPunchRotation_t3772112108;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchRotation::.ctor()
extern "C"  void DOTweenTransformPunchRotation__ctor_m3201007452 (DOTweenTransformPunchRotation_t3772112108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchRotation::Reset()
extern "C"  void DOTweenTransformPunchRotation_Reset_m1194465089 (DOTweenTransformPunchRotation_t3772112108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchRotation::OnEnter()
extern "C"  void DOTweenTransformPunchRotation_OnEnter_m2641865785 (DOTweenTransformPunchRotation_t3772112108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchRotation::<OnEnter>m__D0()
extern "C"  void DOTweenTransformPunchRotation_U3COnEnterU3Em__D0_m4224995432 (DOTweenTransformPunchRotation_t3772112108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformPunchRotation::<OnEnter>m__D1()
extern "C"  void DOTweenTransformPunchRotation_U3COnEnterU3Em__D1_m4224995399 (DOTweenTransformPunchRotation_t3772112108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
