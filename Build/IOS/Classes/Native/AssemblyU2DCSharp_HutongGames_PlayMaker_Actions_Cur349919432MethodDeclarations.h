﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CurveColor
struct CurveColor_t349919432;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CurveColor::.ctor()
extern "C"  void CurveColor__ctor_m739083854 (CurveColor_t349919432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveColor::Reset()
extern "C"  void CurveColor_Reset_m1532859925 (CurveColor_t349919432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveColor::OnEnter()
extern "C"  void CurveColor_OnEnter_m3998593301 (CurveColor_t349919432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveColor::OnExit()
extern "C"  void CurveColor_OnExit_m539431881 (CurveColor_t349919432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CurveColor::OnUpdate()
extern "C"  void CurveColor_OnUpdate_m998330744 (CurveColor_t349919432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
