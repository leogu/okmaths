﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUITextureAlpha
struct SetGUITextureAlpha_t859580376;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::.ctor()
extern "C"  void SetGUITextureAlpha__ctor_m2080590106 (SetGUITextureAlpha_t859580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::Reset()
extern "C"  void SetGUITextureAlpha_Reset_m117960673 (SetGUITextureAlpha_t859580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::OnEnter()
extern "C"  void SetGUITextureAlpha_OnEnter_m2968497881 (SetGUITextureAlpha_t859580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::OnUpdate()
extern "C"  void SetGUITextureAlpha_OnUpdate_m3027671620 (SetGUITextureAlpha_t859580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUITextureAlpha::DoGUITextureAlpha()
extern "C"  void SetGUITextureAlpha_DoGUITextureAlpha_m793071347 (SetGUITextureAlpha_t859580376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
