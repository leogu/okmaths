﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Multiply
struct Vector2Multiply_t466722903;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::.ctor()
extern "C"  void Vector2Multiply__ctor_m1191129193 (Vector2Multiply_t466722903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::Reset()
extern "C"  void Vector2Multiply_Reset_m3519468084 (Vector2Multiply_t466722903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::OnEnter()
extern "C"  void Vector2Multiply_OnEnter_m87321454 (Vector2Multiply_t466722903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Multiply::OnUpdate()
extern "C"  void Vector2Multiply_OnUpdate_m834351077 (Vector2Multiply_t466722903 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
