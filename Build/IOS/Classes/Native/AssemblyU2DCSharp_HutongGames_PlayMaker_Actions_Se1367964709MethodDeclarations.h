﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmGameObject
struct SetFsmGameObject_t1367964709;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::.ctor()
extern "C"  void SetFsmGameObject__ctor_m384338019 (SetFsmGameObject_t1367964709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::Reset()
extern "C"  void SetFsmGameObject_Reset_m196040394 (SetFsmGameObject_t1367964709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::OnEnter()
extern "C"  void SetFsmGameObject_OnEnter_m2480094220 (SetFsmGameObject_t1367964709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::DoSetFsmGameObject()
extern "C"  void SetFsmGameObject_DoSetFsmGameObject_m1016196609 (SetFsmGameObject_t1367964709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmGameObject::OnUpdate()
extern "C"  void SetFsmGameObject_OnUpdate_m3747720667 (SetFsmGameObject_t1367964709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
