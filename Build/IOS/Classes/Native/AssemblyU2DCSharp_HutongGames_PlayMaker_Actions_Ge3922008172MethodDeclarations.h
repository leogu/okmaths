﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetASine
struct GetASine_t3922008172;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetASine::.ctor()
extern "C"  void GetASine__ctor_m3033242022 (GetASine_t3922008172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::Reset()
extern "C"  void GetASine_Reset_m1068169397 (GetASine_t3922008172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::OnEnter()
extern "C"  void GetASine_OnEnter_m4271979309 (GetASine_t3922008172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::OnUpdate()
extern "C"  void GetASine_OnUpdate_m4220621872 (GetASine_t3922008172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetASine::DoASine()
extern "C"  void GetASine_DoASine_m1999520703 (GetASine_t3922008172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
