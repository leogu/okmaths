﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableEditKey
struct HashTableEditKey_t296592325;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableEditKey::.ctor()
extern "C"  void HashTableEditKey__ctor_m2916012761 (HashTableEditKey_t296592325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableEditKey::Reset()
extern "C"  void HashTableEditKey_Reset_m63080970 (HashTableEditKey_t296592325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableEditKey::OnEnter()
extern "C"  void HashTableEditKey_OnEnter_m3319015936 (HashTableEditKey_t296592325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableEditKey::EditHashTableKey()
extern "C"  void HashTableEditKey_EditHashTableKey_m715903374 (HashTableEditKey_t296592325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
