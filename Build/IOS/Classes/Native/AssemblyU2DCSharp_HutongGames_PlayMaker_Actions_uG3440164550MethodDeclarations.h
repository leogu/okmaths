﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers
struct uGuiSliderSetWholeNumbers_t3440164550;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::.ctor()
extern "C"  void uGuiSliderSetWholeNumbers__ctor_m3751255056 (uGuiSliderSetWholeNumbers_t3440164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::Reset()
extern "C"  void uGuiSliderSetWholeNumbers_Reset_m3778778851 (uGuiSliderSetWholeNumbers_t3440164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::OnEnter()
extern "C"  void uGuiSliderSetWholeNumbers_OnEnter_m2772964907 (uGuiSliderSetWholeNumbers_t3440164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::DoSetValue()
extern "C"  void uGuiSliderSetWholeNumbers_DoSetValue_m1430933642 (uGuiSliderSetWholeNumbers_t3440164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSliderSetWholeNumbers::OnExit()
extern "C"  void uGuiSliderSetWholeNumbers_OnExit_m1351579803 (uGuiSliderSetWholeNumbers_t3440164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
