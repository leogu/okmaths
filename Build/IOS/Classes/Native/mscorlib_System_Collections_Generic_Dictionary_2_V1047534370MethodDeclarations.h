﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3655968902;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1047534370.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m149441268_gshared (Enumerator_t1047534370 * __this, Dictionary_2_t3655968902 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m149441268(__this, ___host0, method) ((  void (*) (Enumerator_t1047534370 *, Dictionary_2_t3655968902 *, const MethodInfo*))Enumerator__ctor_m149441268_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2472267505_gshared (Enumerator_t1047534370 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2472267505(__this, method) ((  Il2CppObject * (*) (Enumerator_t1047534370 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2472267505_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3835539813_gshared (Enumerator_t1047534370 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3835539813(__this, method) ((  void (*) (Enumerator_t1047534370 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3835539813_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m2487551420_gshared (Enumerator_t1047534370 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2487551420(__this, method) ((  void (*) (Enumerator_t1047534370 *, const MethodInfo*))Enumerator_Dispose_m2487551420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3469359693_gshared (Enumerator_t1047534370 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3469359693(__this, method) ((  bool (*) (Enumerator_t1047534370 *, const MethodInfo*))Enumerator_MoveNext_m3469359693_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  RaycastHit2D_t4063908774  Enumerator_get_Current_m4247637291_gshared (Enumerator_t1047534370 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4247637291(__this, method) ((  RaycastHit2D_t4063908774  (*) (Enumerator_t1047534370 *, const MethodInfo*))Enumerator_get_Current_m4247637291_gshared)(__this, method)
