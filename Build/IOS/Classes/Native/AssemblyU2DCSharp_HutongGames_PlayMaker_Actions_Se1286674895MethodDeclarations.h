﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLayer
struct SetLayer_t1286674895;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLayer::.ctor()
extern "C"  void SetLayer__ctor_m1996532367 (SetLayer_t1286674895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLayer::Reset()
extern "C"  void SetLayer_Reset_m2833670520 (SetLayer_t1286674895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLayer::OnEnter()
extern "C"  void SetLayer_OnEnter_m2346280158 (SetLayer_t1286674895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLayer::DoSetLayer()
extern "C"  void SetLayer_DoSetLayer_m919597313 (SetLayer_t1286674895 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
