﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTextText
struct DOTweenTextText_t682163984;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTextText::.ctor()
extern "C"  void DOTweenTextText__ctor_m3894450182 (DOTweenTextText_t682163984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextText::Reset()
extern "C"  void DOTweenTextText_Reset_m3756964049 (DOTweenTextText_t682163984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextText::OnEnter()
extern "C"  void DOTweenTextText_OnEnter_m2619624761 (DOTweenTextText_t682163984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextText::<OnEnter>m__A0()
extern "C"  void DOTweenTextText_U3COnEnterU3Em__A0_m3425202835 (DOTweenTextText_t682163984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTextText::<OnEnter>m__A1()
extern "C"  void DOTweenTextText_U3COnEnterU3Em__A1_m3425202740 (DOTweenTextText_t682163984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
