﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRandomChild
struct GetRandomChild_t2938052525;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::.ctor()
extern "C"  void GetRandomChild__ctor_m321635589 (GetRandomChild_t2938052525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::Reset()
extern "C"  void GetRandomChild_Reset_m2650170998 (GetRandomChild_t2938052525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::OnEnter()
extern "C"  void GetRandomChild_OnEnter_m123996780 (GetRandomChild_t2938052525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRandomChild::DoGetRandomChild()
extern "C"  void GetRandomChild_DoGetRandomChild_m1382712129 (GetRandomChild_t2938052525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
