﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3PerSecond
struct Vector3PerSecond_t516668237;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3PerSecond::.ctor()
extern "C"  void Vector3PerSecond__ctor_m903355423 (Vector3PerSecond_t516668237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3PerSecond::Reset()
extern "C"  void Vector3PerSecond_Reset_m1697693022 (Vector3PerSecond_t516668237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3PerSecond::OnEnter()
extern "C"  void Vector3PerSecond_OnEnter_m1449262600 (Vector3PerSecond_t516668237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3PerSecond::OnUpdate()
extern "C"  void Vector3PerSecond_OnUpdate_m1782877423 (Vector3PerSecond_t516668237 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
