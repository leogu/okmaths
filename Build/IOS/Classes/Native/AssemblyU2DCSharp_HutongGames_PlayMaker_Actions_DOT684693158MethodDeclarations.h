﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveZ
struct DOTweenRigidbodyMoveZ_t684693158;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveZ::.ctor()
extern "C"  void DOTweenRigidbodyMoveZ__ctor_m2119834646 (DOTweenRigidbodyMoveZ_t684693158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveZ::Reset()
extern "C"  void DOTweenRigidbodyMoveZ_Reset_m126145211 (DOTweenRigidbodyMoveZ_t684693158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveZ::OnEnter()
extern "C"  void DOTweenRigidbodyMoveZ_OnEnter_m1933486451 (DOTweenRigidbodyMoveZ_t684693158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveZ::<OnEnter>m__8E()
extern "C"  void DOTweenRigidbodyMoveZ_U3COnEnterU3Em__8E_m2186508225 (DOTweenRigidbodyMoveZ_t684693158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMoveZ::<OnEnter>m__8F()
extern "C"  void DOTweenRigidbodyMoveZ_U3COnEnterU3Em__8F_m2186508064 (DOTweenRigidbodyMoveZ_t684693158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
