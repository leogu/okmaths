﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EventSystemExecuteEvent
struct EventSystemExecuteEvent_t3848617116;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EventSystemExecuteEvent::.ctor()
extern "C"  void EventSystemExecuteEvent__ctor_m3571171618 (EventSystemExecuteEvent_t3848617116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EventSystemExecuteEvent::Reset()
extern "C"  void EventSystemExecuteEvent_Reset_m4016354773 (EventSystemExecuteEvent_t3848617116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EventSystemExecuteEvent::OnEnter()
extern "C"  void EventSystemExecuteEvent_OnEnter_m4203738069 (EventSystemExecuteEvent_t3848617116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.EventSystemExecuteEvent::ExecuteEvent()
extern "C"  bool EventSystemExecuteEvent_ExecuteEvent_m3614966599 (EventSystemExecuteEvent_t3848617116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
