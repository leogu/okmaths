﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ProjectLocationToMap
struct ProjectLocationToMap_t2469662469;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::.ctor()
extern "C"  void ProjectLocationToMap__ctor_m3520792177 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::Reset()
extern "C"  void ProjectLocationToMap_Reset_m3704180882 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::OnEnter()
extern "C"  void ProjectLocationToMap_OnEnter_m4193414912 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::OnUpdate()
extern "C"  void ProjectLocationToMap_OnUpdate_m1586917469 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::DoProjectGPSLocation()
extern "C"  void ProjectLocationToMap_DoProjectGPSLocation_m957012580 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::DoEquidistantCylindrical()
extern "C"  void ProjectLocationToMap_DoEquidistantCylindrical_m705104243 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ProjectLocationToMap::DoMercatorProjection()
extern "C"  void ProjectLocationToMap_DoMercatorProjection_m3310355084 (ProjectLocationToMap_t2469662469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.ProjectLocationToMap::LatitudeToMercator(System.Single)
extern "C"  float ProjectLocationToMap_LatitudeToMercator_m3879849852 (Il2CppObject * __this /* static, unused */, float ___latitudeInDegrees0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
