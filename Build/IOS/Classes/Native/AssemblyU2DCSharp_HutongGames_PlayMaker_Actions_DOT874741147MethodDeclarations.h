﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseById
struct DOTweenControlMethodsTogglePauseById_t874741147;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseById::.ctor()
extern "C"  void DOTweenControlMethodsTogglePauseById__ctor_m3624388261 (DOTweenControlMethodsTogglePauseById_t874741147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseById::Reset()
extern "C"  void DOTweenControlMethodsTogglePauseById_Reset_m400390188 (DOTweenControlMethodsTogglePauseById_t874741147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsTogglePauseById::OnEnter()
extern "C"  void DOTweenControlMethodsTogglePauseById_OnEnter_m3441647046 (DOTweenControlMethodsTogglePauseById_t874741147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
