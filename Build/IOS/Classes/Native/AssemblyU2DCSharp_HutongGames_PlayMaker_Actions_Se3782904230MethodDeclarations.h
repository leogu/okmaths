﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetColorValue
struct SetColorValue_t3782904230;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetColorValue::.ctor()
extern "C"  void SetColorValue__ctor_m926535534 (SetColorValue_t3782904230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::Reset()
extern "C"  void SetColorValue_Reset_m1608563811 (SetColorValue_t3782904230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::OnEnter()
extern "C"  void SetColorValue_OnEnter_m1745584547 (SetColorValue_t3782904230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::OnUpdate()
extern "C"  void SetColorValue_OnUpdate_m3715363552 (SetColorValue_t3782904230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetColorValue::DoSetColorValue()
extern "C"  void SetColorValue_DoSetColorValue_m695182073 (SetColorValue_t3782904230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
