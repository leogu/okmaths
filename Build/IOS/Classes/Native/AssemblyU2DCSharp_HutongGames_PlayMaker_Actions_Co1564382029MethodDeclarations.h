﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ConvertIntToFloat
struct ConvertIntToFloat_t1564382029;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::.ctor()
extern "C"  void ConvertIntToFloat__ctor_m162389609 (ConvertIntToFloat_t1564382029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::Reset()
extern "C"  void ConvertIntToFloat_Reset_m2428498094 (ConvertIntToFloat_t1564382029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::OnEnter()
extern "C"  void ConvertIntToFloat_OnEnter_m4029988028 (ConvertIntToFloat_t1564382029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::OnUpdate()
extern "C"  void ConvertIntToFloat_OnUpdate_m4190855317 (ConvertIntToFloat_t1564382029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ConvertIntToFloat::DoConvertIntToFloat()
extern "C"  void ConvertIntToFloat_DoConvertIntToFloat_m3236760273 (ConvertIntToFloat_t1564382029 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
