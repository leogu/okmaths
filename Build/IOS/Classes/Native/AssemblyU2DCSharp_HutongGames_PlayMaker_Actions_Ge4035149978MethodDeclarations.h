﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetTriggerInfo
struct GetTriggerInfo_t4035149978;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::.ctor()
extern "C"  void GetTriggerInfo__ctor_m2661937424 (GetTriggerInfo_t4035149978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::Reset()
extern "C"  void GetTriggerInfo_Reset_m50184583 (GetTriggerInfo_t4035149978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::StoreTriggerInfo()
extern "C"  void GetTriggerInfo_StoreTriggerInfo_m725560883 (GetTriggerInfo_t4035149978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetTriggerInfo::OnEnter()
extern "C"  void GetTriggerInfo_OnEnter_m3359908311 (GetTriggerInfo_t4035149978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
