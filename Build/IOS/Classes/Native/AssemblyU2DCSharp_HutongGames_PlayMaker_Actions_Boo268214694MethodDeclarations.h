﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolTest
struct BoolTest_t268214694;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolTest::.ctor()
extern "C"  void BoolTest__ctor_m2535341752 (BoolTest_t268214694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolTest::Reset()
extern "C"  void BoolTest_Reset_m3365138159 (BoolTest_t268214694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolTest::OnEnter()
extern "C"  void BoolTest_OnEnter_m2174456039 (BoolTest_t268214694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolTest::OnUpdate()
extern "C"  void BoolTest_OnUpdate_m775552310 (BoolTest_t268214694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
