﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo
struct GetAnimatorNextStateInfo_t496900339;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::.ctor()
extern "C"  void GetAnimatorNextStateInfo__ctor_m2205186625 (GetAnimatorNextStateInfo_t496900339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::Reset()
extern "C"  void GetAnimatorNextStateInfo_Reset_m3041743416 (GetAnimatorNextStateInfo_t496900339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnEnter()
extern "C"  void GetAnimatorNextStateInfo_OnEnter_m1266195738 (GetAnimatorNextStateInfo_t496900339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::OnActionUpdate()
extern "C"  void GetAnimatorNextStateInfo_OnActionUpdate_m3959932919 (GetAnimatorNextStateInfo_t496900339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorNextStateInfo::GetLayerInfo()
extern "C"  void GetAnimatorNextStateInfo_GetLayerInfo_m738851198 (GetAnimatorNextStateInfo_t496900339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
