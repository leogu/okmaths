﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateBy
struct iTweenRotateBy_t1616803856;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::.ctor()
extern "C"  void iTweenRotateBy__ctor_m342191024 (iTweenRotateBy_t1616803856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::Reset()
extern "C"  void iTweenRotateBy_Reset_m3805987297 (iTweenRotateBy_t1616803856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::OnEnter()
extern "C"  void iTweenRotateBy_OnEnter_m3406211065 (iTweenRotateBy_t1616803856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::OnExit()
extern "C"  void iTweenRotateBy_OnExit_m3681645197 (iTweenRotateBy_t1616803856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateBy::DoiTween()
extern "C"  void iTweenRotateBy_DoiTween_m637318987 (iTweenRotateBy_t1616803856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
