﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Camera
struct Camera_t189460977;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle
struct  RectTransformScreenPointToWorldPointInRectangle_t3466560733  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::screenPointVector2
	FsmVector2_t2430450063 * ___screenPointVector2_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::orScreenPointVector3
	FsmVector3_t3996534004 * ___orScreenPointVector3_13;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::normalizedScreenPoint
	bool ___normalizedScreenPoint_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::camera
	FsmGameObject_t3097142863 * ___camera_15;
	// System.Boolean HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::everyFrame
	bool ___everyFrame_16;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::worldPosition
	FsmVector3_t3996534004 * ___worldPosition_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::isHit
	FsmBool_t664485696 * ___isHit_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::hitEvent
	FsmEvent_t1258573736 * ___hitEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::noHitEvent
	FsmEvent_t1258573736 * ___noHitEvent_20;
	// UnityEngine.RectTransform HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::_rt
	RectTransform_t3349966182 * ____rt_21;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RectTransformScreenPointToWorldPointInRectangle::_camera
	Camera_t189460977 * ____camera_22;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_screenPointVector2_12() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___screenPointVector2_12)); }
	inline FsmVector2_t2430450063 * get_screenPointVector2_12() const { return ___screenPointVector2_12; }
	inline FsmVector2_t2430450063 ** get_address_of_screenPointVector2_12() { return &___screenPointVector2_12; }
	inline void set_screenPointVector2_12(FsmVector2_t2430450063 * value)
	{
		___screenPointVector2_12 = value;
		Il2CppCodeGenWriteBarrier(&___screenPointVector2_12, value);
	}

	inline static int32_t get_offset_of_orScreenPointVector3_13() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___orScreenPointVector3_13)); }
	inline FsmVector3_t3996534004 * get_orScreenPointVector3_13() const { return ___orScreenPointVector3_13; }
	inline FsmVector3_t3996534004 ** get_address_of_orScreenPointVector3_13() { return &___orScreenPointVector3_13; }
	inline void set_orScreenPointVector3_13(FsmVector3_t3996534004 * value)
	{
		___orScreenPointVector3_13 = value;
		Il2CppCodeGenWriteBarrier(&___orScreenPointVector3_13, value);
	}

	inline static int32_t get_offset_of_normalizedScreenPoint_14() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___normalizedScreenPoint_14)); }
	inline bool get_normalizedScreenPoint_14() const { return ___normalizedScreenPoint_14; }
	inline bool* get_address_of_normalizedScreenPoint_14() { return &___normalizedScreenPoint_14; }
	inline void set_normalizedScreenPoint_14(bool value)
	{
		___normalizedScreenPoint_14 = value;
	}

	inline static int32_t get_offset_of_camera_15() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___camera_15)); }
	inline FsmGameObject_t3097142863 * get_camera_15() const { return ___camera_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_camera_15() { return &___camera_15; }
	inline void set_camera_15(FsmGameObject_t3097142863 * value)
	{
		___camera_15 = value;
		Il2CppCodeGenWriteBarrier(&___camera_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_worldPosition_17() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___worldPosition_17)); }
	inline FsmVector3_t3996534004 * get_worldPosition_17() const { return ___worldPosition_17; }
	inline FsmVector3_t3996534004 ** get_address_of_worldPosition_17() { return &___worldPosition_17; }
	inline void set_worldPosition_17(FsmVector3_t3996534004 * value)
	{
		___worldPosition_17 = value;
		Il2CppCodeGenWriteBarrier(&___worldPosition_17, value);
	}

	inline static int32_t get_offset_of_isHit_18() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___isHit_18)); }
	inline FsmBool_t664485696 * get_isHit_18() const { return ___isHit_18; }
	inline FsmBool_t664485696 ** get_address_of_isHit_18() { return &___isHit_18; }
	inline void set_isHit_18(FsmBool_t664485696 * value)
	{
		___isHit_18 = value;
		Il2CppCodeGenWriteBarrier(&___isHit_18, value);
	}

	inline static int32_t get_offset_of_hitEvent_19() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___hitEvent_19)); }
	inline FsmEvent_t1258573736 * get_hitEvent_19() const { return ___hitEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_hitEvent_19() { return &___hitEvent_19; }
	inline void set_hitEvent_19(FsmEvent_t1258573736 * value)
	{
		___hitEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___hitEvent_19, value);
	}

	inline static int32_t get_offset_of_noHitEvent_20() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ___noHitEvent_20)); }
	inline FsmEvent_t1258573736 * get_noHitEvent_20() const { return ___noHitEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_noHitEvent_20() { return &___noHitEvent_20; }
	inline void set_noHitEvent_20(FsmEvent_t1258573736 * value)
	{
		___noHitEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___noHitEvent_20, value);
	}

	inline static int32_t get_offset_of__rt_21() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ____rt_21)); }
	inline RectTransform_t3349966182 * get__rt_21() const { return ____rt_21; }
	inline RectTransform_t3349966182 ** get_address_of__rt_21() { return &____rt_21; }
	inline void set__rt_21(RectTransform_t3349966182 * value)
	{
		____rt_21 = value;
		Il2CppCodeGenWriteBarrier(&____rt_21, value);
	}

	inline static int32_t get_offset_of__camera_22() { return static_cast<int32_t>(offsetof(RectTransformScreenPointToWorldPointInRectangle_t3466560733, ____camera_22)); }
	inline Camera_t189460977 * get__camera_22() const { return ____camera_22; }
	inline Camera_t189460977 ** get_address_of__camera_22() { return &____camera_22; }
	inline void set__camera_22(Camera_t189460977 * value)
	{
		____camera_22 = value;
		Il2CppCodeGenWriteBarrier(&____camera_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
