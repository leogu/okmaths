﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax
struct RectTransformSetAnchorMax_t1618737691;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::.ctor()
extern "C"  void RectTransformSetAnchorMax__ctor_m4037650277 (RectTransformSetAnchorMax_t1618737691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::Reset()
extern "C"  void RectTransformSetAnchorMax_Reset_m341384120 (RectTransformSetAnchorMax_t1618737691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::OnEnter()
extern "C"  void RectTransformSetAnchorMax_OnEnter_m1117036962 (RectTransformSetAnchorMax_t1618737691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::OnActionUpdate()
extern "C"  void RectTransformSetAnchorMax_OnActionUpdate_m3171767211 (RectTransformSetAnchorMax_t1618737691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMax::DoSetAnchorMax()
extern "C"  void RectTransformSetAnchorMax_DoSetAnchorMax_m2994969589 (RectTransformSetAnchorMax_t1618737691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
