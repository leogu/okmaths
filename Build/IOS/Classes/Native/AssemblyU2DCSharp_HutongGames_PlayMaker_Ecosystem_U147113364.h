﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_4024510117.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable
struct  PlayMakerFsmVariable_t147113364  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.VariableSelectionChoice HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::variableSelectionChoice
	int32_t ___variableSelectionChoice_0;
	// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::selectedType
	int32_t ___selectedType_1;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::variableName
	String_t* ___variableName_2;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::defaultVariableName
	String_t* ___defaultVariableName_3;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::initialized
	bool ___initialized_4;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::targetUndefined
	bool ___targetUndefined_5;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::variableNameToUse
	String_t* ___variableNameToUse_6;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::fsmVariables
	FsmVariables_t630687169 * ___fsmVariables_7;
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_namedVariable
	NamedVariable_t3026441313 * ____namedVariable_8;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_float
	FsmFloat_t937133978 * ____float_9;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_int
	FsmInt_t1273009179 * ____int_10;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_bool
	FsmBool_t664485696 * ____bool_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_gameObject
	FsmGameObject_t3097142863 * ____gameObject_12;
	// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_color
	FsmColor_t118301965 * ____color_13;
	// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_material
	FsmMaterial_t1421632035 * ____material_14;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_object
	FsmObject_t2785794313 * ____object_15;
	// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_quaternion
	FsmQuaternion_t878438756 * ____quaternion_16;
	// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_rect
	FsmRect_t19023354 * ____rect_17;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_string
	FsmString_t2414474701 * ____string_18;
	// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_texture
	FsmTexture_t3372293163 * ____texture_19;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_vector2
	FsmVector2_t2430450063 * ____vector2_20;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariable::_vector3
	FsmVector3_t3996534004 * ____vector3_21;

public:
	inline static int32_t get_offset_of_variableSelectionChoice_0() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___variableSelectionChoice_0)); }
	inline int32_t get_variableSelectionChoice_0() const { return ___variableSelectionChoice_0; }
	inline int32_t* get_address_of_variableSelectionChoice_0() { return &___variableSelectionChoice_0; }
	inline void set_variableSelectionChoice_0(int32_t value)
	{
		___variableSelectionChoice_0 = value;
	}

	inline static int32_t get_offset_of_selectedType_1() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___selectedType_1)); }
	inline int32_t get_selectedType_1() const { return ___selectedType_1; }
	inline int32_t* get_address_of_selectedType_1() { return &___selectedType_1; }
	inline void set_selectedType_1(int32_t value)
	{
		___selectedType_1 = value;
	}

	inline static int32_t get_offset_of_variableName_2() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___variableName_2)); }
	inline String_t* get_variableName_2() const { return ___variableName_2; }
	inline String_t** get_address_of_variableName_2() { return &___variableName_2; }
	inline void set_variableName_2(String_t* value)
	{
		___variableName_2 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_2, value);
	}

	inline static int32_t get_offset_of_defaultVariableName_3() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___defaultVariableName_3)); }
	inline String_t* get_defaultVariableName_3() const { return ___defaultVariableName_3; }
	inline String_t** get_address_of_defaultVariableName_3() { return &___defaultVariableName_3; }
	inline void set_defaultVariableName_3(String_t* value)
	{
		___defaultVariableName_3 = value;
		Il2CppCodeGenWriteBarrier(&___defaultVariableName_3, value);
	}

	inline static int32_t get_offset_of_initialized_4() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___initialized_4)); }
	inline bool get_initialized_4() const { return ___initialized_4; }
	inline bool* get_address_of_initialized_4() { return &___initialized_4; }
	inline void set_initialized_4(bool value)
	{
		___initialized_4 = value;
	}

	inline static int32_t get_offset_of_targetUndefined_5() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___targetUndefined_5)); }
	inline bool get_targetUndefined_5() const { return ___targetUndefined_5; }
	inline bool* get_address_of_targetUndefined_5() { return &___targetUndefined_5; }
	inline void set_targetUndefined_5(bool value)
	{
		___targetUndefined_5 = value;
	}

	inline static int32_t get_offset_of_variableNameToUse_6() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___variableNameToUse_6)); }
	inline String_t* get_variableNameToUse_6() const { return ___variableNameToUse_6; }
	inline String_t** get_address_of_variableNameToUse_6() { return &___variableNameToUse_6; }
	inline void set_variableNameToUse_6(String_t* value)
	{
		___variableNameToUse_6 = value;
		Il2CppCodeGenWriteBarrier(&___variableNameToUse_6, value);
	}

	inline static int32_t get_offset_of_fsmVariables_7() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ___fsmVariables_7)); }
	inline FsmVariables_t630687169 * get_fsmVariables_7() const { return ___fsmVariables_7; }
	inline FsmVariables_t630687169 ** get_address_of_fsmVariables_7() { return &___fsmVariables_7; }
	inline void set_fsmVariables_7(FsmVariables_t630687169 * value)
	{
		___fsmVariables_7 = value;
		Il2CppCodeGenWriteBarrier(&___fsmVariables_7, value);
	}

	inline static int32_t get_offset_of__namedVariable_8() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____namedVariable_8)); }
	inline NamedVariable_t3026441313 * get__namedVariable_8() const { return ____namedVariable_8; }
	inline NamedVariable_t3026441313 ** get_address_of__namedVariable_8() { return &____namedVariable_8; }
	inline void set__namedVariable_8(NamedVariable_t3026441313 * value)
	{
		____namedVariable_8 = value;
		Il2CppCodeGenWriteBarrier(&____namedVariable_8, value);
	}

	inline static int32_t get_offset_of__float_9() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____float_9)); }
	inline FsmFloat_t937133978 * get__float_9() const { return ____float_9; }
	inline FsmFloat_t937133978 ** get_address_of__float_9() { return &____float_9; }
	inline void set__float_9(FsmFloat_t937133978 * value)
	{
		____float_9 = value;
		Il2CppCodeGenWriteBarrier(&____float_9, value);
	}

	inline static int32_t get_offset_of__int_10() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____int_10)); }
	inline FsmInt_t1273009179 * get__int_10() const { return ____int_10; }
	inline FsmInt_t1273009179 ** get_address_of__int_10() { return &____int_10; }
	inline void set__int_10(FsmInt_t1273009179 * value)
	{
		____int_10 = value;
		Il2CppCodeGenWriteBarrier(&____int_10, value);
	}

	inline static int32_t get_offset_of__bool_11() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____bool_11)); }
	inline FsmBool_t664485696 * get__bool_11() const { return ____bool_11; }
	inline FsmBool_t664485696 ** get_address_of__bool_11() { return &____bool_11; }
	inline void set__bool_11(FsmBool_t664485696 * value)
	{
		____bool_11 = value;
		Il2CppCodeGenWriteBarrier(&____bool_11, value);
	}

	inline static int32_t get_offset_of__gameObject_12() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____gameObject_12)); }
	inline FsmGameObject_t3097142863 * get__gameObject_12() const { return ____gameObject_12; }
	inline FsmGameObject_t3097142863 ** get_address_of__gameObject_12() { return &____gameObject_12; }
	inline void set__gameObject_12(FsmGameObject_t3097142863 * value)
	{
		____gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&____gameObject_12, value);
	}

	inline static int32_t get_offset_of__color_13() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____color_13)); }
	inline FsmColor_t118301965 * get__color_13() const { return ____color_13; }
	inline FsmColor_t118301965 ** get_address_of__color_13() { return &____color_13; }
	inline void set__color_13(FsmColor_t118301965 * value)
	{
		____color_13 = value;
		Il2CppCodeGenWriteBarrier(&____color_13, value);
	}

	inline static int32_t get_offset_of__material_14() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____material_14)); }
	inline FsmMaterial_t1421632035 * get__material_14() const { return ____material_14; }
	inline FsmMaterial_t1421632035 ** get_address_of__material_14() { return &____material_14; }
	inline void set__material_14(FsmMaterial_t1421632035 * value)
	{
		____material_14 = value;
		Il2CppCodeGenWriteBarrier(&____material_14, value);
	}

	inline static int32_t get_offset_of__object_15() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____object_15)); }
	inline FsmObject_t2785794313 * get__object_15() const { return ____object_15; }
	inline FsmObject_t2785794313 ** get_address_of__object_15() { return &____object_15; }
	inline void set__object_15(FsmObject_t2785794313 * value)
	{
		____object_15 = value;
		Il2CppCodeGenWriteBarrier(&____object_15, value);
	}

	inline static int32_t get_offset_of__quaternion_16() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____quaternion_16)); }
	inline FsmQuaternion_t878438756 * get__quaternion_16() const { return ____quaternion_16; }
	inline FsmQuaternion_t878438756 ** get_address_of__quaternion_16() { return &____quaternion_16; }
	inline void set__quaternion_16(FsmQuaternion_t878438756 * value)
	{
		____quaternion_16 = value;
		Il2CppCodeGenWriteBarrier(&____quaternion_16, value);
	}

	inline static int32_t get_offset_of__rect_17() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____rect_17)); }
	inline FsmRect_t19023354 * get__rect_17() const { return ____rect_17; }
	inline FsmRect_t19023354 ** get_address_of__rect_17() { return &____rect_17; }
	inline void set__rect_17(FsmRect_t19023354 * value)
	{
		____rect_17 = value;
		Il2CppCodeGenWriteBarrier(&____rect_17, value);
	}

	inline static int32_t get_offset_of__string_18() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____string_18)); }
	inline FsmString_t2414474701 * get__string_18() const { return ____string_18; }
	inline FsmString_t2414474701 ** get_address_of__string_18() { return &____string_18; }
	inline void set__string_18(FsmString_t2414474701 * value)
	{
		____string_18 = value;
		Il2CppCodeGenWriteBarrier(&____string_18, value);
	}

	inline static int32_t get_offset_of__texture_19() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____texture_19)); }
	inline FsmTexture_t3372293163 * get__texture_19() const { return ____texture_19; }
	inline FsmTexture_t3372293163 ** get_address_of__texture_19() { return &____texture_19; }
	inline void set__texture_19(FsmTexture_t3372293163 * value)
	{
		____texture_19 = value;
		Il2CppCodeGenWriteBarrier(&____texture_19, value);
	}

	inline static int32_t get_offset_of__vector2_20() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____vector2_20)); }
	inline FsmVector2_t2430450063 * get__vector2_20() const { return ____vector2_20; }
	inline FsmVector2_t2430450063 ** get_address_of__vector2_20() { return &____vector2_20; }
	inline void set__vector2_20(FsmVector2_t2430450063 * value)
	{
		____vector2_20 = value;
		Il2CppCodeGenWriteBarrier(&____vector2_20, value);
	}

	inline static int32_t get_offset_of__vector3_21() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariable_t147113364, ____vector3_21)); }
	inline FsmVector3_t3996534004 * get__vector3_21() const { return ____vector3_21; }
	inline FsmVector3_t3996534004 ** get_address_of__vector3_21() { return &____vector3_21; }
	inline void set__vector3_21(FsmVector3_t3996534004 * value)
	{
		____vector3_21 = value;
		Il2CppCodeGenWriteBarrier(&____vector3_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
