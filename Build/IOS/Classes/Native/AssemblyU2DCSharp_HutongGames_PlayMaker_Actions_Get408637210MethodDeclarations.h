﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName
struct GetAnimatorCurrentTransitionInfoIsName_t408637210;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::.ctor()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName__ctor_m2509275986 (GetAnimatorCurrentTransitionInfoIsName_t408637210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::Reset()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_Reset_m175859947 (GetAnimatorCurrentTransitionInfoIsName_t408637210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::OnEnter()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_OnEnter_m2395949971 (GetAnimatorCurrentTransitionInfoIsName_t408637210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::OnActionUpdate()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_OnActionUpdate_m1315226938 (GetAnimatorCurrentTransitionInfoIsName_t408637210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorCurrentTransitionInfoIsName::IsName()
extern "C"  void GetAnimatorCurrentTransitionInfoIsName_IsName_m2275620099 (GetAnimatorCurrentTransitionInfoIsName_t408637210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
