﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetDrag
struct SetDrag_t1013364552;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetDrag::.ctor()
extern "C"  void SetDrag__ctor_m1012650124 (SetDrag_t1013364552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::Reset()
extern "C"  void SetDrag_Reset_m13185481 (SetDrag_t1013364552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::OnEnter()
extern "C"  void SetDrag_OnEnter_m227680409 (SetDrag_t1013364552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::OnUpdate()
extern "C"  void SetDrag_OnUpdate_m3556185082 (SetDrag_t1013364552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetDrag::DoSetDrag()
extern "C"  void SetDrag_DoSetDrag_m1330522333 (SetDrag_t1013364552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
