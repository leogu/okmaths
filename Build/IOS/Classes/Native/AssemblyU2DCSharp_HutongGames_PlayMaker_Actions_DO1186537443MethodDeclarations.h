﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenMaterialBlendableColor
struct DOTweenMaterialBlendableColor_t1186537443;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialBlendableColor::.ctor()
extern "C"  void DOTweenMaterialBlendableColor__ctor_m50543421 (DOTweenMaterialBlendableColor_t1186537443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialBlendableColor::Reset()
extern "C"  void DOTweenMaterialBlendableColor_Reset_m798651232 (DOTweenMaterialBlendableColor_t1186537443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialBlendableColor::OnEnter()
extern "C"  void DOTweenMaterialBlendableColor_OnEnter_m3529305146 (DOTweenMaterialBlendableColor_t1186537443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialBlendableColor::<OnEnter>m__50()
extern "C"  void DOTweenMaterialBlendableColor_U3COnEnterU3Em__50_m1749857076 (DOTweenMaterialBlendableColor_t1186537443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenMaterialBlendableColor::<OnEnter>m__51()
extern "C"  void DOTweenMaterialBlendableColor_U3COnEnterU3Em__51_m1749857171 (DOTweenMaterialBlendableColor_t1186537443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
