﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetCollisionInfo
struct GetCollisionInfo_t3677821312;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::.ctor()
extern "C"  void GetCollisionInfo__ctor_m2655398848 (GetCollisionInfo_t3677821312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::Reset()
extern "C"  void GetCollisionInfo_Reset_m3730462353 (GetCollisionInfo_t3677821312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::StoreCollisionInfo()
extern "C"  void GetCollisionInfo_StoreCollisionInfo_m2900836691 (GetCollisionInfo_t3677821312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetCollisionInfo::OnEnter()
extern "C"  void GetCollisionInfo_OnEnter_m2992018937 (GetCollisionInfo_t3677821312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
