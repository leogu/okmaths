﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// System.Object
struct Il2CppObject;
// UnityEngine.UI.Image
struct Image_t2042527209;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// UnityEngine.UI.Outline
struct Outline_t1417504278;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t3793077019;
// DG.Tweening.Sequence
struct Sequence_t110643099;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t3371939845;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t3299805082;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_t308799701;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0
struct U3CU3Ec__DisplayClass13_0_t591124703;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t2735155078;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t4039116993;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t3017480080;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t3582130305;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t591124924;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t4039117214;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t3017480301;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t3371939942;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_t3299805272;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t308799891;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_t3582130274;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t591124893;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t4039117183;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_t3017480270;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t3371939977;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t3371940008;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t3371940074;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t3371940109;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t3371940140;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_U3CModuleU3E3783534214MethodDeclarations.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOTween2276353038MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940008MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940008.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940074MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940074.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940109MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940109.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940140MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371940140.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3299805082MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3299805082.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799701MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799701.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124703MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124703.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2735155078MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec2735155078.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039116993MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039116993.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3793077019.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214.h"
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode1501334721.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480301MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480301.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3299805272MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3299805272.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274.h"
#include "mscorlib_System_String2029220233.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3811326375MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3687449219MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3811326375.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3687449219.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen588429502.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117183MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117183.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480270MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480270.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOVirtual1722510550MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390MethodDeclarations.h"

// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2279406887 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2998039394 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3793077019 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3793077019 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.Core.Extensions::SetSpecialStartupMode<System.Object>(!!0,DG.Tweening.Core.Enums.SpecialStartupMode)
extern "C"  Il2CppObject * Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
#define Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.Core.Extensions::SetSpecialStartupMode<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>>(!!0,DG.Tweening.Core.Enums.SpecialStartupMode)
#define Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3793077019 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3793077019 *, int32_t, const MethodInfo*))Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Tweener>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250(__this /* static, unused */, p0, p1, p2, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tweener>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702(__this /* static, unused */, p0, p1, p2, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tweener>(!!0)
#define TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372(__this /* static, unused */, p0, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisIl2CppObject_m2082091712_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Sequence>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586(__this /* static, unused */, p0, p1, method) ((  Sequence_t110643099 * (*) (Il2CppObject * /* static, unused */, Sequence_t110643099 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Sequence>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241(__this /* static, unused */, p0, p1, method) ((  Sequence_t110643099 * (*) (Il2CppObject * /* static, unused */, Sequence_t110643099 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
#define TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Sequence>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnUpdate_TisSequence_t110643099_m3711708753(__this /* static, unused */, p0, p1, method) ((  Sequence_t110643099 * (*) (Il2CppObject * /* static, unused */, Sequence_t110643099 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared)(__this /* static, unused */, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<!!0,!!1,!!2> DG.Tweening.Core.Extensions::Blendable<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<!!0,!!1,!!2>)
extern "C"  TweenerCore_3_t2998039394 * Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2998039394 * p0, const MethodInfo* method);
#define Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691(__this /* static, unused */, p0, method) ((  TweenerCore_3_t2998039394 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_gshared)(__this /* static, unused */, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 DG.Tweening.DOTweenUtils46::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenUtils46_SwitchToRectTransform_m2431809528_MetadataUsageId;
extern "C"  Vector2_t2243707579  DOTweenUtils46_SwitchToRectTransform_m2431809528 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___from0, RectTransform_t3349966182 * ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DOTweenUtils46_SwitchToRectTransform_m2431809528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		RectTransform_t3349966182 * L_0 = ___from0;
		NullCheck(L_0);
		Rect_t3681755626  L_1 = RectTransform_get_rect_m73954734(L_0, /*hidden argument*/NULL);
		V_4 = L_1;
		float L_2 = Rect_get_width_m1138015702((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_3 = ___from0;
		NullCheck(L_3);
		Rect_t3681755626  L_4 = RectTransform_get_rect_m73954734(L_3, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_xMin_m1161102488((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_6 = ___from0;
		NullCheck(L_6);
		Rect_t3681755626  L_7 = RectTransform_get_rect_m73954734(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		float L_8 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_9 = ___from0;
		NullCheck(L_9);
		Rect_t3681755626  L_10 = RectTransform_get_rect_m73954734(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Rect_get_yMin_m1161103577((&V_4), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_1), ((float)((float)((float)((float)L_2*(float)(0.5f)))+(float)L_5)), ((float)((float)((float)((float)L_8*(float)(0.5f)))+(float)L_11)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_12 = ___from0;
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_14 = RectTransformUtility_WorldToScreenPoint_m1650782138(NULL /*static, unused*/, (Camera_t189460977 *)NULL, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Vector2_t2243707579  L_15 = V_2;
		Vector2_t2243707579  L_16 = V_1;
		Vector2_t2243707579  L_17 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		RectTransform_t3349966182 * L_18 = ___to1;
		Vector2_t2243707579  L_19 = V_2;
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080(NULL /*static, unused*/, L_18, L_19, (Camera_t189460977 *)NULL, (&V_0), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_20 = ___to1;
		NullCheck(L_20);
		Rect_t3681755626  L_21 = RectTransform_get_rect_m73954734(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = Rect_get_width_m1138015702((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_23 = ___to1;
		NullCheck(L_23);
		Rect_t3681755626  L_24 = RectTransform_get_rect_m73954734(L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		float L_25 = Rect_get_xMin_m1161102488((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_26 = ___to1;
		NullCheck(L_26);
		Rect_t3681755626  L_27 = RectTransform_get_rect_m73954734(L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		float L_28 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_29 = ___to1;
		NullCheck(L_29);
		Rect_t3681755626  L_30 = RectTransform_get_rect_m73954734(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		float L_31 = Rect_get_yMin_m1161103577((&V_4), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_3), ((float)((float)((float)((float)L_22*(float)(0.5f)))+(float)L_25)), ((float)((float)((float)((float)L_28*(float)(0.5f)))+(float)L_31)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_32 = ___to1;
		NullCheck(L_32);
		Vector2_t2243707579  L_33 = RectTransform_get_anchoredPosition_m3570822376(L_32, /*hidden argument*/NULL);
		Vector2_t2243707579  L_34 = V_0;
		Vector2_t2243707579  L_35 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Vector2_t2243707579  L_36 = V_3;
		Vector2_t2243707579  L_37 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass0_0_t3371939845_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3858616074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3734738918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3288120768_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2613085532_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m70814363_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m70814363 (Il2CppObject * __this /* static, unused */, CanvasGroup_t3296560743 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m70814363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass0_0_t3371939845 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_0 = (U3CU3Ec__DisplayClass0_0_t3371939845 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_0_t3371939845_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_0__ctor_m2279016740(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_1 = V_0;
		CanvasGroup_t3296560743 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491_MethodInfo_var);
		DOGetter_1_t3858616074 * L_5 = (DOGetter_1_t3858616074 *)il2cpp_codegen_object_new(DOGetter_1_t3858616074_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3288120768(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3288120768_MethodInfo_var);
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845_MethodInfo_var);
		DOSetter_1_t3734738918 * L_8 = (DOSetter_1_t3734738918 *)il2cpp_codegen_object_new(DOSetter_1_t3734738918_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2613085532(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m2613085532_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2279406887 * L_11 = DOTween_To_m914278462(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_12 = V_0;
		NullCheck(L_12);
		CanvasGroup_t3296560743 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2279406887 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass3_0_t3371939942_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOColor_m3476021741_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m3476021741 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m3476021741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t3371939942 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_0 = (U3CU3Ec__DisplayClass3_0_t3371939942 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t3371939942_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_m2320169091(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_1 = V_0;
		Image_t2042527209 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_11 = DOTween_To_m3916872722(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_12 = V_0;
		NullCheck(L_12);
		Image_t2042527209 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2998039394 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass4_0_t3371939977_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m2221289113_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m2221289113 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m2221289113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t3371939977 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_0 = (U3CU3Ec__DisplayClass4_0_t3371939977 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t3371939977_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m2122475432(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_1 = V_0;
		Image_t2042527209 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Tweener_t760404022 * L_11 = DOTween_ToAlpha_m4132971413(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_12 = V_0;
		NullCheck(L_12);
		Image_t2042527209 * L_13 = L_12->get_target_0();
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass5_0_t3371940008_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3858616074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3734738918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m1489628791_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3288120768_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m1563959817_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2613085532_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFillAmount_m2516534104_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFillAmount_m2516534104 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFillAmount_m2516534104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass5_0_t3371940008 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_t3371940008 * L_0 = (U3CU3Ec__DisplayClass5_0_t3371940008 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass5_0_t3371940008_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass5_0__ctor_m2085635913(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass5_0_t3371940008 * L_1 = V_0;
		Image_t2042527209 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		float L_3 = ___endValue1;
		if ((!(((float)L_3) > ((float)(1.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		___endValue1 = (1.0f);
		goto IL_002d;
	}

IL_001e:
	{
		float L_4 = ___endValue1;
		if ((!(((float)L_4) < ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		___endValue1 = (0.0f);
	}

IL_002d:
	{
		U3CU3Ec__DisplayClass5_0_t3371940008 * L_5 = V_0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m1489628791_MethodInfo_var);
		DOGetter_1_t3858616074 * L_7 = (DOGetter_1_t3858616074 *)il2cpp_codegen_object_new(DOGetter_1_t3858616074_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3288120768(L_7, L_5, L_6, /*hidden argument*/DOGetter_1__ctor_m3288120768_MethodInfo_var);
		U3CU3Ec__DisplayClass5_0_t3371940008 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m1563959817_MethodInfo_var);
		DOSetter_1_t3734738918 * L_10 = (DOSetter_1_t3734738918 *)il2cpp_codegen_object_new(DOSetter_1_t3734738918_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2613085532(L_10, L_8, L_9, /*hidden argument*/DOSetter_1__ctor_m2613085532_MethodInfo_var);
		float L_11 = ___endValue1;
		float L_12 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2279406887 * L_13 = DOTween_To_m914278462(NULL /*static, unused*/, L_7, L_10, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass5_0_t3371940008 * L_14 = V_0;
		NullCheck(L_14);
		Image_t2042527209 * L_15 = L_14->get_target_0();
		TweenerCore_3_t2279406887 * L_16 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass7_0_t3371940074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m4208448539_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m390067829_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFlexibleSize_m3060859622_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFlexibleSize_m3060859622 (Il2CppObject * __this /* static, unused */, LayoutElement_t2808691390 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFlexibleSize_m3060859622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass7_0_t3371940074 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass7_0_t3371940074 * L_0 = (U3CU3Ec__DisplayClass7_0_t3371940074 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass7_0_t3371940074_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass7_0__ctor_m2163627783(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass7_0_t3371940074 * L_1 = V_0;
		LayoutElement_t2808691390 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass7_0_t3371940074 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m4208448539_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass7_0_t3371940074 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m390067829_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_11 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass7_0_t3371940074 * L_14 = V_0;
		NullCheck(L_14);
		LayoutElement_t2808691390 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass8_0_t3371940109_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m3735793539_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m2021315045_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOMinSize_m3919863093_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOMinSize_m3919863093 (Il2CppObject * __this /* static, unused */, LayoutElement_t2808691390 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOMinSize_m3919863093_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass8_0_t3371940109 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass8_0_t3371940109 * L_0 = (U3CU3Ec__DisplayClass8_0_t3371940109 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass8_0_t3371940109_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass8_0__ctor_m2591525404(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass8_0_t3371940109 * L_1 = V_0;
		LayoutElement_t2808691390 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass8_0_t3371940109 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m3735793539_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass8_0_t3371940109 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m2021315045_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_11 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass8_0_t3371940109 * L_14 = V_0;
		NullCheck(L_14);
		LayoutElement_t2808691390 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass9_0_t3371940140_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_m1229978689_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m3961975735_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOPreferredSize_m1445534836_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOPreferredSize_m1445534836 (Il2CppObject * __this /* static, unused */, LayoutElement_t2808691390 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOPreferredSize_m1445534836_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass9_0_t3371940140 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass9_0_t3371940140 * L_0 = (U3CU3Ec__DisplayClass9_0_t3371940140 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass9_0_t3371940140_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass9_0__ctor_m2554685885(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass9_0_t3371940140 * L_1 = V_0;
		LayoutElement_t2808691390 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass9_0_t3371940140 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_m1229978689_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass9_0_t3371940140 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m3961975735_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_11 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass9_0_t3371940140 * L_14 = V_0;
		NullCheck(L_14);
		LayoutElement_t2808691390 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass10_0_t3299805082_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m4251884724_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2033857326_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOColor_m4031159738_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m4031159738 (Il2CppObject * __this /* static, unused */, Outline_t1417504278 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m4031159738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass10_0_t3299805082 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass10_0_t3299805082 * L_0 = (U3CU3Ec__DisplayClass10_0_t3299805082 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass10_0_t3299805082_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass10_0__ctor_m1255061513(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass10_0_t3299805082 * L_1 = V_0;
		Outline_t1417504278 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass10_0_t3299805082 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m4251884724_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass10_0_t3299805082 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2033857326_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_11 = DOTween_To_m3916872722(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass10_0_t3299805082 * L_12 = V_0;
		NullCheck(L_12);
		Outline_t1417504278 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2998039394 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass11_0_t308799701_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m1396123548_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m4290172248_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m204150586_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m204150586 (Il2CppObject * __this /* static, unused */, Outline_t1417504278 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m204150586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass11_0_t308799701 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass11_0_t308799701 * L_0 = (U3CU3Ec__DisplayClass11_0_t308799701 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass11_0_t308799701_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass11_0__ctor_m3713157006(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass11_0_t308799701 * L_1 = V_0;
		Outline_t1417504278 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass11_0_t308799701 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m1396123548_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass11_0_t308799701 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m4290172248_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Tweener_t760404022 * L_11 = DOTween_ToAlpha_m4132971413(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass11_0_t308799701 * L_12 = V_0;
		NullCheck(L_12);
		Outline_t1417504278 * L_13 = L_12->get_target_0();
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass13_0_t591124703_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m3380085955_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m226247393_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPos_m2991517945_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPos_m2991517945 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPos_m2991517945_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass13_0_t591124703 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass13_0_t591124703 * L_0 = (U3CU3Ec__DisplayClass13_0_t591124703 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass13_0_t591124703_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass13_0__ctor_m3304669848(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass13_0_t591124703 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass13_0_t591124703 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m3380085955_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass13_0_t591124703 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m226247393_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_11 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass13_0_t591124703 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3349966182 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass14_0_t2735155078_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m1474346752_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m3045962408_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPosX_m4074693926_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPosX_m4074693926 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPosX_m4074693926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass14_0_t2735155078 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass14_0_t2735155078 * L_0 = (U3CU3Ec__DisplayClass14_0_t2735155078 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_t2735155078_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass14_0__ctor_m438087197(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass14_0_t2735155078 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass14_0_t2735155078 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m1474346752_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass14_0_t2735155078 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m3045962408_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		float L_9 = ___endValue1;
		Vector2_t2243707579  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3067419446(&L_10, L_9, (0.0f), /*hidden argument*/NULL);
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_12 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping3;
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_12, 2, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass14_0_t2735155078 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3349966182 * L_16 = L_15->get_target_0();
		Tweener_t760404022 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass15_0_t4039116993_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3300516614_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m1384529474_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPosY_m1128768801_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPosY_m1128768801 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPosY_m1128768801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass15_0_t4039116993 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass15_0_t4039116993 * L_0 = (U3CU3Ec__DisplayClass15_0_t4039116993 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass15_0_t4039116993_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass15_0__ctor_m2896182690(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass15_0_t4039116993 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass15_0_t4039116993 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3300516614_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass15_0_t4039116993 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m1384529474_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		float L_9 = ___endValue1;
		Vector2_t2243707579  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3067419446(&L_10, (0.0f), L_9, /*hidden argument*/NULL);
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_12 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping3;
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_12, 4, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass15_0_t4039116993 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3349966182 * L_16 = L_15->get_target_0();
		Tweener_t760404022 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass16_0_t3017480080_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813722_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936566_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m1323234132_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m668528496_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPos3D_m3279580891_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPos3D_m3279580891 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPos3D_m3279580891_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass16_0_t3017480080 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_0 = (U3CU3Ec__DisplayClass16_0_t3017480080 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass16_0_t3017480080_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass16_0__ctor_m2864467495(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286_MethodInfo_var);
		DOGetter_1_t4025813722 * L_5 = (DOGetter_1_t4025813722 *)il2cpp_codegen_object_new(DOGetter_1_t4025813722_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1323234132(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1323234132_MethodInfo_var);
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412_MethodInfo_var);
		DOSetter_1_t3901936566 * L_8 = (DOSetter_1_t3901936566 *)il2cpp_codegen_object_new(DOSetter_1_t3901936566_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m668528496(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m668528496_MethodInfo_var);
		Vector3_t2243707580  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t1108663466 * L_11 = DOTween_To_m1457612583(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m4276703213(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3349966182 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass22_0_t3582130305_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m3309618817_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m37166355_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOSizeDelta_m2859745733_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOSizeDelta_m2859745733 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOSizeDelta_m2859745733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass22_0_t3582130305 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_0 = (U3CU3Ec__DisplayClass22_0_t3582130305 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_0_t3582130305_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass22_0__ctor_m1316328468(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m3309618817_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m37166355_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_11 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3349966182 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass23_0_t591124924_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813722_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936566_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m1323234132_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m668528496_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOPunchAnchorPos_m1987664933_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOPunchAnchorPos_m1987664933 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector2_t2243707579  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOPunchAnchorPos_m1987664933_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass23_0_t591124924 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass23_0_t591124924 * L_0 = (U3CU3Ec__DisplayClass23_0_t591124924 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass23_0_t591124924_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass23_0__ctor_m939556505(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass23_0_t591124924 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass23_0_t591124924 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077_MethodInfo_var);
		DOGetter_1_t4025813722 * L_5 = (DOGetter_1_t4025813722 *)il2cpp_codegen_object_new(DOGetter_1_t4025813722_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1323234132(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1323234132_MethodInfo_var);
		U3CU3Ec__DisplayClass23_0_t591124924 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759_MethodInfo_var);
		DOSetter_1_t3901936566 * L_8 = (DOSetter_1_t3901936566 *)il2cpp_codegen_object_new(DOSetter_1_t3901936566_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m668528496(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m668528496_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___punch1;
		Vector3_t2243707580  L_10 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		float L_11 = ___duration2;
		int32_t L_12 = ___vibrato3;
		float L_13 = ___elasticity4;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3793077019 * L_14 = DOTween_Punch_m2093241430(NULL /*static, unused*/, L_5, L_8, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass23_0_t591124924 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3349966182 * L_16 = L_15->get_target_0();
		TweenerCore_3_t3793077019 * L_17 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var);
		bool L_18 = ___snapping5;
		Tweener_t760404022 * L_19 = TweenSettingsExtensions_SetOptions_m3161000053(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass25_0_t4039117214_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813722_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936566_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m3287361143_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m1323234132_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m3144298357_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m668528496_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var;
extern const MethodInfo* Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOShakeAnchorPos_m1105810756_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOShakeAnchorPos_m1105810756 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, float ___duration1, Vector2_t2243707579  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOShakeAnchorPos_m1105810756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass25_0_t4039117214 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_0 = (U3CU3Ec__DisplayClass25_0_t4039117214 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass25_0_t4039117214_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass25_0__ctor_m531069347(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m3287361143_MethodInfo_var);
		DOGetter_1_t4025813722 * L_5 = (DOGetter_1_t4025813722 *)il2cpp_codegen_object_new(DOGetter_1_t4025813722_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1323234132(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1323234132_MethodInfo_var);
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m3144298357_MethodInfo_var);
		DOSetter_1_t3901936566 * L_8 = (DOSetter_1_t3901936566 *)il2cpp_codegen_object_new(DOSetter_1_t3901936566_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m668528496(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m668528496_MethodInfo_var);
		float L_9 = ___duration1;
		Vector2_t2243707579  L_10 = ___strength2;
		Vector3_t2243707580  L_11 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = ___vibrato3;
		float L_13 = ___randomness4;
		bool L_14 = ___fadeOut6;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3793077019 * L_15 = DOTween_Shake_m683174647(NULL /*static, unused*/, L_5, L_8, L_9, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_16 = V_0;
		NullCheck(L_16);
		RectTransform_t3349966182 * L_17 = L_16->get_target_0();
		TweenerCore_3_t3793077019 * L_18 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var);
		TweenerCore_3_t3793077019 * L_19 = Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834(NULL /*static, unused*/, L_18, 2, /*hidden argument*/Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834_MethodInfo_var);
		bool L_20 = ___snapping5;
		Tweener_t760404022 * L_21 = TweenSettingsExtensions_SetOptions_m3161000053(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions46::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass26_0_t3017480301_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* TweenCallback_t3697142134_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__0_m3773677991_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2108428307_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__1_m4262498353_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m1453722671_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__2_m3773678057_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__3_m904546355_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__4_m584448748_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnUpdate_TisSequence_t110643099_m3711708753_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOJumpAnchorPos_m3876617598_MetadataUsageId;
extern "C"  Sequence_t110643099 * ShortcutExtensions46_DOJumpAnchorPos_m3876617598 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector2_t2243707579  ___endValue1, float ___jumpPower2, int32_t ___numJumps3, float ___duration4, bool ___snapping5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOJumpAnchorPos_m3876617598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass26_0_t3017480301 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_0 = (U3CU3Ec__DisplayClass26_0_t3017480301 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass26_0_t3017480301_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass26_0__ctor_m499354152(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_3 = V_0;
		Vector2_t2243707579  L_4 = ___endValue1;
		NullCheck(L_3);
		L_3->set_endValue_4(L_4);
		int32_t L_5 = ___numJumps3;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		___numJumps3 = 1;
	}

IL_001b:
	{
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_6 = V_0;
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_7 = V_0;
		NullCheck(L_7);
		RectTransform_t3349966182 * L_8 = L_7->get_target_0();
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = RectTransform_get_anchoredPosition_m3570822376(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_2();
		NullCheck(L_6);
		L_6->set_startPosY_5(L_10);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_11 = V_0;
		NullCheck(L_11);
		L_11->set_offsetY_2((-1.0f));
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_12 = V_0;
		NullCheck(L_12);
		L_12->set_offsetYSet_1((bool)0);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Sequence_t110643099 * L_14 = DOTween_Sequence_m3562267366(NULL /*static, unused*/, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_15 = V_0;
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__0_m3773677991_MethodInfo_var);
		DOGetter_1_t4025813721 * L_17 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_17, L_15, L_16, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_18 = V_0;
		IntPtr_t L_19;
		L_19.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__1_m4262498353_MethodInfo_var);
		DOSetter_1_t3901936565 * L_20 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_20, L_18, L_19, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_21 = V_0;
		NullCheck(L_21);
		Vector2_t2243707579 * L_22 = L_21->get_address_of_endValue_4();
		float L_23 = L_22->get_x_1();
		Vector2_t2243707579  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector2__ctor_m3067419446(&L_24, L_23, (0.0f), /*hidden argument*/NULL);
		float L_25 = ___duration4;
		TweenerCore_3_t3250868854 * L_26 = DOTween_To_m432804578(NULL /*static, unused*/, L_17, L_20, L_24, L_25, /*hidden argument*/NULL);
		bool L_27 = ___snapping5;
		Tweener_t760404022 * L_28 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_26, 2, L_27, /*hidden argument*/NULL);
		Tweener_t760404022 * L_29 = TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288(NULL /*static, unused*/, L_28, 1, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288_MethodInfo_var);
		Sequence_t110643099 * L_30 = TweenSettingsExtensions_Append_m388120539(NULL /*static, unused*/, L_14, L_29, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_31 = V_0;
		IntPtr_t L_32;
		L_32.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__2_m3773678057_MethodInfo_var);
		DOGetter_1_t4025813721 * L_33 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2108428307(L_33, L_31, L_32, /*hidden argument*/DOGetter_1__ctor_m2108428307_MethodInfo_var);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_34 = V_0;
		IntPtr_t L_35;
		L_35.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__3_m904546355_MethodInfo_var);
		DOSetter_1_t3901936565 * L_36 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m1453722671(L_36, L_34, L_35, /*hidden argument*/DOSetter_1__ctor_m1453722671_MethodInfo_var);
		float L_37 = ___jumpPower2;
		Vector2_t2243707579  L_38;
		memset(&L_38, 0, sizeof(L_38));
		Vector2__ctor_m3067419446(&L_38, (0.0f), L_37, /*hidden argument*/NULL);
		float L_39 = ___duration4;
		int32_t L_40 = ___numJumps3;
		TweenerCore_3_t3250868854 * L_41 = DOTween_To_m432804578(NULL /*static, unused*/, L_33, L_36, L_38, ((float)((float)L_39/(float)(((float)((float)((int32_t)((int32_t)L_40*(int32_t)2))))))), /*hidden argument*/NULL);
		bool L_42 = ___snapping5;
		Tweener_t760404022 * L_43 = TweenSettingsExtensions_SetOptions_m1571209893(NULL /*static, unused*/, L_41, 4, L_42, /*hidden argument*/NULL);
		Tweener_t760404022 * L_44 = TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288(NULL /*static, unused*/, L_43, 6, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweener_t760404022_m2332716288_MethodInfo_var);
		int32_t L_45 = ___numJumps3;
		Tweener_t760404022 * L_46 = TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702(NULL /*static, unused*/, L_44, ((int32_t)((int32_t)L_45*(int32_t)2)), 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweener_t760404022_m3681501702_MethodInfo_var);
		Tweener_t760404022 * L_47 = TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372(NULL /*static, unused*/, L_46, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweener_t760404022_m2944918372_MethodInfo_var);
		Sequence_t110643099 * L_48 = TweenSettingsExtensions_Join_m3909252375(NULL /*static, unused*/, L_30, L_47, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_49 = V_0;
		NullCheck(L_49);
		RectTransform_t3349966182 * L_50 = L_49->get_target_0();
		Sequence_t110643099 * L_51 = TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586(NULL /*static, unused*/, L_48, L_50, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisSequence_t110643099_m43613586_MethodInfo_var);
		int32_t L_52 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_defaultEaseType_13();
		Sequence_t110643099 * L_53 = TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/TweenSettingsExtensions_SetEase_TisSequence_t110643099_m1985887241_MethodInfo_var);
		NullCheck(L_13);
		L_13->set_s_3(L_53);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_54 = V_0;
		NullCheck(L_54);
		Sequence_t110643099 * L_55 = L_54->get_s_3();
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_56 = V_0;
		IntPtr_t L_57;
		L_57.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__4_m584448748_MethodInfo_var);
		TweenCallback_t3697142134 * L_58 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_58, L_56, L_57, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnUpdate_TisSequence_t110643099_m3711708753(NULL /*static, unused*/, L_55, L_58, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisSequence_t110643099_m3711708753_MethodInfo_var);
		U3CU3Ec__DisplayClass26_0_t3017480301 * L_59 = V_0;
		NullCheck(L_59);
		Sequence_t110643099 * L_60 = L_59->get_s_3();
		return L_60;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass30_0_t3299805272_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3858616074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3734738918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__0_m955469785_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3288120768_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__1_m1756672311_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2613085532_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOValue_m3085057733_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOValue_m3085057733 (Il2CppObject * __this /* static, unused */, Slider_t297367283 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOValue_m3085057733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass30_0_t3299805272 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass30_0_t3299805272 * L_0 = (U3CU3Ec__DisplayClass30_0_t3299805272 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass30_0_t3299805272_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass30_0__ctor_m105447435(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass30_0_t3299805272 * L_1 = V_0;
		Slider_t297367283 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass30_0_t3299805272 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__0_m955469785_MethodInfo_var);
		DOGetter_1_t3858616074 * L_5 = (DOGetter_1_t3858616074 *)il2cpp_codegen_object_new(DOGetter_1_t3858616074_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3288120768(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3288120768_MethodInfo_var);
		U3CU3Ec__DisplayClass30_0_t3299805272 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__1_m1756672311_MethodInfo_var);
		DOSetter_1_t3734738918 * L_8 = (DOSetter_1_t3734738918 *)il2cpp_codegen_object_new(DOSetter_1_t3734738918_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2613085532(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m2613085532_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2279406887 * L_11 = DOTween_To_m914278462(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m937094726(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass30_0_t3299805272 * L_14 = V_0;
		NullCheck(L_14);
		Slider_t297367283 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass31_0_t308799891_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1587596721_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m1143202187_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOColor_m4265141523_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m4265141523 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m4265141523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass31_0_t308799891 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass31_0_t308799891 * L_0 = (U3CU3Ec__DisplayClass31_0_t308799891 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass31_0_t308799891_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass31_0__ctor_m2563542928(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass31_0_t308799891 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass31_0_t308799891 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1587596721_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass31_0_t308799891 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m1143202187_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_11 = DOTween_To_m3916872722(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass31_0_t308799891 * L_12 = V_0;
		NullCheck(L_12);
		Text_t356221433 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2998039394 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass32_0_t3582130274_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m2794982395_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m2794982395 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m2794982395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass32_0_t3582130274 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_0 = (U3CU3Ec__DisplayClass32_0_t3582130274 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass32_0_t3582130274_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass32_0__ctor_m2531827733(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Tweener_t760404022 * L_11 = DOTween_ToAlpha_m4132971413(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_12 = V_0;
		NullCheck(L_12);
		Text_t356221433 * L_13 = L_12->get_target_0();
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern Il2CppClass* U3CU3Ec__DisplayClass33_0_t591124893_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3811326375_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3687449219_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3472400603_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3227459959_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOText_m2936740990_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOText_m2936740990 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, String_t* ___endValue1, float ___duration2, bool ___richTextEnabled3, int32_t ___scrambleMode4, String_t* ___scrambleChars5, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOText_m2936740990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass33_0_t591124893 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass33_0_t591124893 * L_0 = (U3CU3Ec__DisplayClass33_0_t591124893 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass33_0_t591124893_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass33_0__ctor_m2155055770(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass33_0_t591124893 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass33_0_t591124893 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975_MethodInfo_var);
		DOGetter_1_t3811326375 * L_5 = (DOGetter_1_t3811326375 *)il2cpp_codegen_object_new(DOGetter_1_t3811326375_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3472400603(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3472400603_MethodInfo_var);
		U3CU3Ec__DisplayClass33_0_t591124893 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921_MethodInfo_var);
		DOSetter_1_t3687449219 * L_8 = (DOSetter_1_t3687449219 *)il2cpp_codegen_object_new(DOSetter_1_t3687449219_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3227459959(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3227459959_MethodInfo_var);
		String_t* L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t588429502 * L_11 = DOTween_To_m2764928386(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___richTextEnabled3;
		int32_t L_13 = ___scrambleMode4;
		String_t* L_14 = ___scrambleChars5;
		Tweener_t760404022 * L_15 = TweenSettingsExtensions_SetOptions_m1112657767(NULL /*static, unused*/, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass33_0_t591124893 * L_16 = V_0;
		NullCheck(L_16);
		Text_t356221433 * L_17 = L_16->get_target_0();
		Tweener_t760404022 * L_18 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass35_0_t4039117183_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass35_0_U3CDOBlendableColorU3Eb__0_m3779763642_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass35_0_U3CDOBlendableColorU3Eb__1_m3168441366_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOBlendableColor_m1472017758_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOBlendableColor_m1472017758 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOBlendableColor_m1472017758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass35_0_t4039117183 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_0 = (U3CU3Ec__DisplayClass35_0_t4039117183 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass35_0_t4039117183_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass35_0__ctor_m1746568612(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_1 = V_0;
		Image_t2042527209 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_1(L_2);
		Color_t2020392075  L_3 = ___endValue1;
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_4 = V_0;
		NullCheck(L_4);
		Image_t2042527209 * L_5 = L_4->get_target_1();
		NullCheck(L_5);
		Color_t2020392075  L_6 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		Color_t2020392075  L_7 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		___endValue1 = L_7;
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_8 = V_0;
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_to_0(L_9);
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass35_0_U3CDOBlendableColorU3Eb__0_m3779763642_MethodInfo_var);
		DOGetter_1_t3802498217 * L_12 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_12, L_10, L_11, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass35_0_U3CDOBlendableColorU3Eb__1_m3168441366_MethodInfo_var);
		DOSetter_1_t3678621061 * L_15 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_15, L_13, L_14, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_16 = ___endValue1;
		float L_17 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_18 = DOTween_To_m3916872722(NULL /*static, unused*/, L_12, L_15, L_16, L_17, /*hidden argument*/NULL);
		TweenerCore_3_t2998039394 * L_19 = Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691(NULL /*static, unused*/, L_18, /*hidden argument*/Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_MethodInfo_var);
		U3CU3Ec__DisplayClass35_0_t4039117183 * L_20 = V_0;
		NullCheck(L_20);
		Image_t2042527209 * L_21 = L_20->get_target_1();
		TweenerCore_3_t2998039394 * L_22 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_22;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass36_0_t3017480270_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__0_m2823944853_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__1_m1854242535_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOBlendableColor_m1939260110_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOBlendableColor_m1939260110 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOBlendableColor_m1939260110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass36_0_t3017480270 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_0 = (U3CU3Ec__DisplayClass36_0_t3017480270 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass36_0_t3017480270_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass36_0__ctor_m1714853417(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_1(L_2);
		Color_t2020392075  L_3 = ___endValue1;
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_4 = V_0;
		NullCheck(L_4);
		Text_t356221433 * L_5 = L_4->get_target_1();
		NullCheck(L_5);
		Color_t2020392075  L_6 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		Color_t2020392075  L_7 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		___endValue1 = L_7;
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_8 = V_0;
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_to_0(L_9);
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_10 = V_0;
		IntPtr_t L_11;
		L_11.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__0_m2823944853_MethodInfo_var);
		DOGetter_1_t3802498217 * L_12 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_12, L_10, L_11, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_13 = V_0;
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__1_m1854242535_MethodInfo_var);
		DOSetter_1_t3678621061 * L_15 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_15, L_13, L_14, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_16 = ___endValue1;
		float L_17 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_18 = DOTween_To_m3916872722(NULL /*static, unused*/, L_12, L_15, L_16, L_17, /*hidden argument*/NULL);
		TweenerCore_3_t2998039394 * L_19 = Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691(NULL /*static, unused*/, L_18, /*hidden argument*/Extensions_Blendable_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m2071899691_MethodInfo_var);
		U3CU3Ec__DisplayClass36_0_t3017480270 * L_20 = V_0;
		NullCheck(L_20);
		Text_t356221433 * L_21 = L_20->get_target_1();
		TweenerCore_3_t2998039394 * L_22 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_22;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m2279016740 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__0()
extern "C"  float U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, const MethodInfo* method)
{
	{
		CanvasGroup_t3296560743 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = CanvasGroup_get_alpha_m1304564441(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, float ___x0, const MethodInfo* method)
{
	{
		CanvasGroup_t3296560743 * L_0 = __this->get_target_0();
		float L_1 = ___x0;
		NullCheck(L_0);
		CanvasGroup_set_alpha_m3292984624(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass10_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass10_0__ctor_m1255061513 (U3CU3Ec__DisplayClass10_0_t3299805082 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass10_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m4251884724 (U3CU3Ec__DisplayClass10_0_t3299805082 * __this, const MethodInfo* method)
{
	{
		Outline_t1417504278 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = Shadow_get_effectColor_m792481977(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2033857326 (U3CU3Ec__DisplayClass10_0_t3299805082 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Outline_t1417504278 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		Shadow_set_effectColor_m3110056844(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass11_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass11_0__ctor_m3713157006 (U3CU3Ec__DisplayClass11_0_t308799701 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass11_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m1396123548 (U3CU3Ec__DisplayClass11_0_t308799701 * __this, const MethodInfo* method)
{
	{
		Outline_t1417504278 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = Shadow_get_effectColor_m792481977(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m4290172248 (U3CU3Ec__DisplayClass11_0_t308799701 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Outline_t1417504278 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		Shadow_set_effectColor_m3110056844(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass13_0__ctor_m3304669848 (U3CU3Ec__DisplayClass13_0_t591124703 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m3380085955 (U3CU3Ec__DisplayClass13_0_t591124703 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m226247393 (U3CU3Ec__DisplayClass13_0_t591124703 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass14_0__ctor_m438087197 (U3CU3Ec__DisplayClass14_0_t2735155078 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m1474346752 (U3CU3Ec__DisplayClass14_0_t2735155078 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m3045962408 (U3CU3Ec__DisplayClass14_0_t2735155078 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass15_0__ctor_m2896182690 (U3CU3Ec__DisplayClass15_0_t4039116993 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_m3300516614 (U3CU3Ec__DisplayClass15_0_t4039116993 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m1384529474 (U3CU3Ec__DisplayClass15_0_t4039116993 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m2864467495 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = RectTransform_get_anchoredPosition3D_m1202614648(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector3_t2243707580  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition3D_m3116733735(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass22_0__ctor_m1316328468 (U3CU3Ec__DisplayClass22_0_t3582130305 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m3309618817 (U3CU3Ec__DisplayClass22_0_t3582130305 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m37166355 (U3CU3Ec__DisplayClass22_0_t3582130305 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_sizeDelta_m2319668137(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass23_0__ctor_m939556505 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector3_t2243707580  L_1 = ___x0;
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass25_0__ctor_m531069347 (U3CU3Ec__DisplayClass25_0_t4039117214 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m3287361143 (U3CU3Ec__DisplayClass25_0_t4039117214 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m3144298357 (U3CU3Ec__DisplayClass25_0_t4039117214 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector3_t2243707580  L_1 = ___x0;
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass26_0__ctor_m499354152 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__0_m3773677991 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__1_m4262498353 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__2()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__2_m3773678057 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__3(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__3_m904546355 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__4()
extern "C"  void U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__4_m584448748 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	U3CU3Ec__DisplayClass26_0_t3017480301 * G_B3_0 = NULL;
	U3CU3Ec__DisplayClass26_0_t3017480301 * G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	U3CU3Ec__DisplayClass26_0_t3017480301 * G_B4_1 = NULL;
	{
		bool L_0 = __this->get_offsetYSet_1();
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		__this->set_offsetYSet_1((bool)1);
		Sequence_t110643099 * L_1 = __this->get_s_3();
		NullCheck(L_1);
		bool L_2 = ((Tween_t278478013 *)L_1)->get_isRelative_27();
		G_B2_0 = __this;
		if (L_2)
		{
			G_B3_0 = __this;
			goto IL_0031;
		}
	}
	{
		Vector2_t2243707579 * L_3 = __this->get_address_of_endValue_4();
		float L_4 = L_3->get_y_2();
		float L_5 = __this->get_startPosY_5();
		G_B4_0 = ((float)((float)L_4-(float)L_5));
		G_B4_1 = G_B2_0;
		goto IL_003c;
	}

IL_0031:
	{
		Vector2_t2243707579 * L_6 = __this->get_address_of_endValue_4();
		float L_7 = L_6->get_y_2();
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_003c:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_offsetY_2(G_B4_0);
	}

IL_0041:
	{
		RectTransform_t3349966182 * L_8 = __this->get_target_0();
		NullCheck(L_8);
		Vector2_t2243707579  L_9 = RectTransform_get_anchoredPosition_m3570822376(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		float* L_10 = (&V_0)->get_address_of_y_2();
		float* L_11 = L_10;
		float L_12 = __this->get_offsetY_2();
		Sequence_t110643099 * L_13 = __this->get_s_3();
		float L_14 = TweenExtensions_ElapsedDirectionalPercentage_m968488947(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = DOVirtual_EasedValue_m1602872611(NULL /*static, unused*/, (0.0f), L_12, L_14, 6, /*hidden argument*/NULL);
		*((float*)(L_11)) = (float)((float)((float)(*((float*)L_11))+(float)L_15));
		RectTransform_t3349966182 * L_16 = __this->get_target_0();
		Vector2_t2243707579  L_17 = V_0;
		NullCheck(L_16);
		RectTransform_set_anchoredPosition_m2077229449(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m2320169091 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass30_0__ctor_m105447435 (U3CU3Ec__DisplayClass30_0_t3299805272 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::<DOValue>b__0()
extern "C"  float U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__0_m955469785 (U3CU3Ec__DisplayClass30_0_t3299805272 * __this, const MethodInfo* method)
{
	{
		Slider_t297367283 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass30_0::<DOValue>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass30_0_U3CDOValueU3Eb__1_m1756672311 (U3CU3Ec__DisplayClass30_0_t3299805272 * __this, float ___x0, const MethodInfo* method)
{
	{
		Slider_t297367283 * L_0 = __this->get_target_0();
		float L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass31_0__ctor_m2563542928 (U3CU3Ec__DisplayClass31_0_t308799891 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1587596721 (U3CU3Ec__DisplayClass31_0_t308799891 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m1143202187 (U3CU3Ec__DisplayClass31_0_t308799891 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass32_0__ctor_m2531827733 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass33_0__ctor_m2155055770 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__0()
extern "C"  String_t* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(73 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__1(System.String)
extern "C"  void U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, String_t* ___x0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		String_t* L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass35_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass35_0__ctor_m1746568612 (U3CU3Ec__DisplayClass35_0_t4039117183 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass35_0::<DOBlendableColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass35_0_U3CDOBlendableColorU3Eb__0_m3779763642 (U3CU3Ec__DisplayClass35_0_t4039117183 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get_to_0();
		return L_0;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass35_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass35_0_U3CDOBlendableColorU3Eb__1_m3168441366 (U3CU3Ec__DisplayClass35_0_t4039117183 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = ___x0;
		Color_t2020392075  L_1 = __this->get_to_0();
		Color_t2020392075  L_2 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Color_t2020392075  L_3 = ___x0;
		__this->set_to_0(L_3);
		Image_t2042527209 * L_4 = __this->get_target_1();
		Image_t2042527209 * L_5 = L_4;
		NullCheck(L_5);
		Color_t2020392075  L_6 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		Color_t2020392075  L_7 = V_0;
		Color_t2020392075  L_8 = Color_op_Addition_m1759335993(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_8);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass36_0__ctor_m1714853417 (U3CU3Ec__DisplayClass36_0_t3017480270 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0::<DOBlendableColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__0_m2823944853 (U3CU3Ec__DisplayClass36_0_t3017480270 * __this, const MethodInfo* method)
{
	{
		Color_t2020392075  L_0 = __this->get_to_0();
		return L_0;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass36_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass36_0_U3CDOBlendableColorU3Eb__1_m1854242535 (U3CU3Ec__DisplayClass36_0_t3017480270 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = ___x0;
		Color_t2020392075  L_1 = __this->get_to_0();
		Color_t2020392075  L_2 = Color_op_Subtraction_m3931992385(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Color_t2020392075  L_3 = ___x0;
		__this->set_to_0(L_3);
		Text_t356221433 * L_4 = __this->get_target_1();
		Text_t356221433 * L_5 = L_4;
		NullCheck(L_5);
		Color_t2020392075  L_6 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		Color_t2020392075  L_7 = V_0;
		Color_t2020392075  L_8 = Color_op_Addition_m1759335993(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_8);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m2122475432 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(21 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(22 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m2085635913 (U3CU3Ec__DisplayClass5_0_t3371940008 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern "C"  float U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m1489628791 (U3CU3Ec__DisplayClass5_0_t3371940008 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = Image_get_fillAmount_m3354146540(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m1563959817 (U3CU3Ec__DisplayClass5_0_t3371940008 * __this, float ___x0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		float L_1 = ___x0;
		NullCheck(L_0);
		Image_set_fillAmount_m2220966753(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m2163627783 (U3CU3Ec__DisplayClass7_0_t3371940074 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m4208448539 (U3CU3Ec__DisplayClass7_0_t3371940074 * __this, const MethodInfo* method)
{
	{
		LayoutElement_t2808691390 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(38 /* System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth() */, L_0);
		LayoutElement_t2808691390 * L_2 = __this->get_target_0();
		NullCheck(L_2);
		float L_3 = VirtFuncInvoker0< float >::Invoke(40 /* System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight() */, L_2);
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m390067829 (U3CU3Ec__DisplayClass7_0_t3371940074 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		LayoutElement_t2808691390 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		float L_2 = L_1.get_x_1();
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(39 /* System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single) */, L_0, L_2);
		LayoutElement_t2808691390 * L_3 = __this->get_target_0();
		Vector2_t2243707579  L_4 = ___x0;
		float L_5 = L_4.get_y_2();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(41 /* System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single) */, L_3, L_5);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0__ctor_m2591525404 (U3CU3Ec__DisplayClass8_0_t3371940109 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0::<DOMinSize>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m3735793539 (U3CU3Ec__DisplayClass8_0_t3371940109 * __this, const MethodInfo* method)
{
	{
		LayoutElement_t2808691390 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(30 /* System.Single UnityEngine.UI.LayoutElement::get_minWidth() */, L_0);
		LayoutElement_t2808691390 * L_2 = __this->get_target_0();
		NullCheck(L_2);
		float L_3 = VirtFuncInvoker0< float >::Invoke(32 /* System.Single UnityEngine.UI.LayoutElement::get_minHeight() */, L_2);
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m2021315045 (U3CU3Ec__DisplayClass8_0_t3371940109 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		LayoutElement_t2808691390 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		float L_2 = L_1.get_x_1();
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(31 /* System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single) */, L_0, L_2);
		LayoutElement_t2808691390 * L_3 = __this->get_target_0();
		Vector2_t2243707579  L_4 = ___x0;
		float L_5 = L_4.get_y_2();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(33 /* System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single) */, L_3, L_5);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0__ctor_m2554685885 (U3CU3Ec__DisplayClass9_0_t3371940140 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_m1229978689 (U3CU3Ec__DisplayClass9_0_t3371940140 * __this, const MethodInfo* method)
{
	{
		LayoutElement_t2808691390 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(34 /* System.Single UnityEngine.UI.LayoutElement::get_preferredWidth() */, L_0);
		LayoutElement_t2808691390 * L_2 = __this->get_target_0();
		NullCheck(L_2);
		float L_3 = VirtFuncInvoker0< float >::Invoke(36 /* System.Single UnityEngine.UI.LayoutElement::get_preferredHeight() */, L_2);
		Vector2_t2243707579  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3067419446(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m3961975735 (U3CU3Ec__DisplayClass9_0_t3371940140 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		LayoutElement_t2808691390 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		float L_2 = L_1.get_x_1();
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(35 /* System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single) */, L_0, L_2);
		LayoutElement_t2808691390 * L_3 = __this->get_target_0();
		Vector2_t2243707579  L_4 = ___x0;
		float L_5 = L_4.get_y_2();
		NullCheck(L_3);
		VirtActionInvoker1< float >::Invoke(37 /* System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single) */, L_3, L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
