﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetScreenHeight
struct GetScreenHeight_t1538321199;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetScreenHeight::.ctor()
extern "C"  void GetScreenHeight__ctor_m2665676751 (GetScreenHeight_t1538321199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScreenHeight::Reset()
extern "C"  void GetScreenHeight_Reset_m803188324 (GetScreenHeight_t1538321199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetScreenHeight::OnEnter()
extern "C"  void GetScreenHeight_OnEnter_m1509375370 (GetScreenHeight_t1538321199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
