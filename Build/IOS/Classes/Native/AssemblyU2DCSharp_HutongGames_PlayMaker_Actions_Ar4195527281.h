﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListContainsGameObject
struct  ArrayListContainsGameObject_t4195527281  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::gameObjectName
	FsmString_t2414474701 * ___gameObjectName_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::withTag
	FsmString_t2414474701 * ___withTag_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::result
	FsmGameObject_t3097142863 * ___result_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::resultIndex
	FsmInt_t1273009179 * ___resultIndex_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::isContained
	FsmBool_t664485696 * ___isContained_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::isContainedEvent
	FsmEvent_t1258573736 * ___isContainedEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::isNotContainedEvent
	FsmEvent_t1258573736 * ___isNotContainedEvent_20;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_gameObjectName_14() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___gameObjectName_14)); }
	inline FsmString_t2414474701 * get_gameObjectName_14() const { return ___gameObjectName_14; }
	inline FsmString_t2414474701 ** get_address_of_gameObjectName_14() { return &___gameObjectName_14; }
	inline void set_gameObjectName_14(FsmString_t2414474701 * value)
	{
		___gameObjectName_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectName_14, value);
	}

	inline static int32_t get_offset_of_withTag_15() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___withTag_15)); }
	inline FsmString_t2414474701 * get_withTag_15() const { return ___withTag_15; }
	inline FsmString_t2414474701 ** get_address_of_withTag_15() { return &___withTag_15; }
	inline void set_withTag_15(FsmString_t2414474701 * value)
	{
		___withTag_15 = value;
		Il2CppCodeGenWriteBarrier(&___withTag_15, value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___result_16)); }
	inline FsmGameObject_t3097142863 * get_result_16() const { return ___result_16; }
	inline FsmGameObject_t3097142863 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmGameObject_t3097142863 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier(&___result_16, value);
	}

	inline static int32_t get_offset_of_resultIndex_17() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___resultIndex_17)); }
	inline FsmInt_t1273009179 * get_resultIndex_17() const { return ___resultIndex_17; }
	inline FsmInt_t1273009179 ** get_address_of_resultIndex_17() { return &___resultIndex_17; }
	inline void set_resultIndex_17(FsmInt_t1273009179 * value)
	{
		___resultIndex_17 = value;
		Il2CppCodeGenWriteBarrier(&___resultIndex_17, value);
	}

	inline static int32_t get_offset_of_isContained_18() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___isContained_18)); }
	inline FsmBool_t664485696 * get_isContained_18() const { return ___isContained_18; }
	inline FsmBool_t664485696 ** get_address_of_isContained_18() { return &___isContained_18; }
	inline void set_isContained_18(FsmBool_t664485696 * value)
	{
		___isContained_18 = value;
		Il2CppCodeGenWriteBarrier(&___isContained_18, value);
	}

	inline static int32_t get_offset_of_isContainedEvent_19() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___isContainedEvent_19)); }
	inline FsmEvent_t1258573736 * get_isContainedEvent_19() const { return ___isContainedEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_isContainedEvent_19() { return &___isContainedEvent_19; }
	inline void set_isContainedEvent_19(FsmEvent_t1258573736 * value)
	{
		___isContainedEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___isContainedEvent_19, value);
	}

	inline static int32_t get_offset_of_isNotContainedEvent_20() { return static_cast<int32_t>(offsetof(ArrayListContainsGameObject_t4195527281, ___isNotContainedEvent_20)); }
	inline FsmEvent_t1258573736 * get_isNotContainedEvent_20() const { return ___isNotContainedEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotContainedEvent_20() { return &___isNotContainedEvent_20; }
	inline void set_isNotContainedEvent_20(FsmEvent_t1258573736 * value)
	{
		___isNotContainedEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___isNotContainedEvent_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
