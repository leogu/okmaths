﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// admob.Admob
struct Admob_t546240967;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// admobdemo
struct  admobdemo_t4253778382  : public MonoBehaviour_t1158329972
{
public:
	// admob.Admob admobdemo::ad
	Admob_t546240967 * ___ad_2;

public:
	inline static int32_t get_offset_of_ad_2() { return static_cast<int32_t>(offsetof(admobdemo_t4253778382, ___ad_2)); }
	inline Admob_t546240967 * get_ad_2() const { return ___ad_2; }
	inline Admob_t546240967 ** get_address_of_ad_2() { return &___ad_2; }
	inline void set_ad_2(Admob_t546240967 * value)
	{
		___ad_2 = value;
		Il2CppCodeGenWriteBarrier(&___ad_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
