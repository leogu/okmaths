﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomInt
struct RandomInt_t922903046;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomInt::.ctor()
extern "C"  void RandomInt__ctor_m280458530 (RandomInt_t922903046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomInt::Reset()
extern "C"  void RandomInt_Reset_m134783999 (RandomInt_t922903046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomInt::OnEnter()
extern "C"  void RandomInt_OnEnter_m2208061079 (RandomInt_t922903046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
