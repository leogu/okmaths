﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.RaycastHit2D>
struct Dictionary_2_t3655968902;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2050505044.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2827285254_gshared (Enumerator_t2050505044 * __this, Dictionary_2_t3655968902 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2827285254(__this, ___host0, method) ((  void (*) (Enumerator_t2050505044 *, Dictionary_2_t3655968902 *, const MethodInfo*))Enumerator__ctor_m2827285254_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2567179441_gshared (Enumerator_t2050505044 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2567179441(__this, method) ((  Il2CppObject * (*) (Enumerator_t2050505044 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2567179441_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3257831405_gshared (Enumerator_t2050505044 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3257831405(__this, method) ((  void (*) (Enumerator_t2050505044 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3257831405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void Enumerator_Dispose_m3015934942_gshared (Enumerator_t2050505044 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3015934942(__this, method) ((  void (*) (Enumerator_t2050505044 *, const MethodInfo*))Enumerator_Dispose_m3015934942_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1689387893_gshared (Enumerator_t2050505044 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1689387893(__this, method) ((  bool (*) (Enumerator_t2050505044 *, const MethodInfo*))Enumerator_MoveNext_m1689387893_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.RaycastHit2D>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1386548683_gshared (Enumerator_t2050505044 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1386548683(__this, method) ((  Il2CppObject * (*) (Enumerator_t2050505044 *, const MethodInfo*))Enumerator_get_Current_m1386548683_gshared)(__this, method)
