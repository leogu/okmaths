﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.StartServer
struct StartServer_t817813875;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.StartServer::.ctor()
extern "C"  void StartServer__ctor_m3818958135 (StartServer_t817813875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartServer::Reset()
extern "C"  void StartServer_Reset_m1930579180 (StartServer_t817813875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.StartServer::OnEnter()
extern "C"  void StartServer_OnEnter_m3580944098 (StartServer_t817813875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
