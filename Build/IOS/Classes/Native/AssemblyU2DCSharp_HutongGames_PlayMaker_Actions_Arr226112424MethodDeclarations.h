﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListClear
struct ArrayListClear_t226112424;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListClear::.ctor()
extern "C"  void ArrayListClear__ctor_m3151388266 (ArrayListClear_t226112424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListClear::Reset()
extern "C"  void ArrayListClear_Reset_m1191997585 (ArrayListClear_t226112424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListClear::OnEnter()
extern "C"  void ArrayListClear_OnEnter_m4097982249 (ArrayListClear_t226112424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListClear::ClearArrayList()
extern "C"  void ArrayListClear_ClearArrayList_m4015362854 (ArrayListClear_t226112424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
