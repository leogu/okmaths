﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetSendRate
struct NetworkGetSendRate_t562759532;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetSendRate::.ctor()
extern "C"  void NetworkGetSendRate__ctor_m1659546890 (NetworkGetSendRate_t562759532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetSendRate::Reset()
extern "C"  void NetworkGetSendRate_Reset_m3618935257 (NetworkGetSendRate_t562759532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetSendRate::OnEnter()
extern "C"  void NetworkGetSendRate_OnEnter_m3716915129 (NetworkGetSendRate_t562759532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetSendRate::DoGetSendRate()
extern "C"  void NetworkGetSendRate_DoGetSendRate_m3575838379 (NetworkGetSendRate_t562759532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
