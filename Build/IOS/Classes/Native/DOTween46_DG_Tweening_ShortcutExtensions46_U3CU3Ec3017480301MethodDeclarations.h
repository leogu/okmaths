﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t3017480301;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass26_0__ctor_m499354152 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__0_m3773677991 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__1_m4262498353 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__2()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__2_m3773678057 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__3(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__3_m904546355 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass26_0::<DOJumpAnchorPos>b__4()
extern "C"  void U3CU3Ec__DisplayClass26_0_U3CDOJumpAnchorPosU3Eb__4_m584448748 (U3CU3Ec__DisplayClass26_0_t3017480301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
