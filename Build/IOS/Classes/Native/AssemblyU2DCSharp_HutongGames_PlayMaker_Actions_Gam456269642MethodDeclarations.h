﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GameObjectCompare
struct GameObjectCompare_t456269642;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::.ctor()
extern "C"  void GameObjectCompare__ctor_m2151672766 (GameObjectCompare_t456269642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::Reset()
extern "C"  void GameObjectCompare_Reset_m83919395 (GameObjectCompare_t456269642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnEnter()
extern "C"  void GameObjectCompare_OnEnter_m3004041163 (GameObjectCompare_t456269642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::OnUpdate()
extern "C"  void GameObjectCompare_OnUpdate_m3238780184 (GameObjectCompare_t456269642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GameObjectCompare::DoGameObjectCompare()
extern "C"  void GameObjectCompare_DoGameObjectCompare_m10469273 (GameObjectCompare_t456269642 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
