﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseFloat
struct EaseFloat_t1021472868;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseFloat::.ctor()
extern "C"  void EaseFloat__ctor_m753317170 (EaseFloat_t1021472868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::Reset()
extern "C"  void EaseFloat_Reset_m3919109893 (EaseFloat_t1021472868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnEnter()
extern "C"  void EaseFloat_OnEnter_m3058776141 (EaseFloat_t1021472868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnExit()
extern "C"  void EaseFloat_OnExit_m147599033 (EaseFloat_t1021472868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFloat::OnUpdate()
extern "C"  void EaseFloat_OnUpdate_m2446330836 (EaseFloat_t1021472868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
