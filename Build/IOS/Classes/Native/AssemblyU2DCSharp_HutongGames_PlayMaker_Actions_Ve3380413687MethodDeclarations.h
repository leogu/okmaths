﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Invert
struct Vector2Invert_t3380413687;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Invert::.ctor()
extern "C"  void Vector2Invert__ctor_m351264147 (Vector2Invert_t3380413687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Invert::Reset()
extern "C"  void Vector2Invert_Reset_m1859659824 (Vector2Invert_t3380413687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Invert::OnEnter()
extern "C"  void Vector2Invert_OnEnter_m2193547350 (Vector2Invert_t3380413687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Invert::OnUpdate()
extern "C"  void Vector2Invert_OnUpdate_m4220018571 (Vector2Invert_t3380413687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
