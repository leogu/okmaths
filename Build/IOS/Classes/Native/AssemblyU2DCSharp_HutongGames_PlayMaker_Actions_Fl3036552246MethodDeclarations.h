﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatOperator
struct FloatOperator_t3036552246;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatOperator::.ctor()
extern "C"  void FloatOperator__ctor_m3294421760 (FloatOperator_t3036552246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::Reset()
extern "C"  void FloatOperator_Reset_m3321945555 (FloatOperator_t3036552246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnEnter()
extern "C"  void FloatOperator_OnEnter_m582363867 (FloatOperator_t3036552246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::OnUpdate()
extern "C"  void FloatOperator_OnUpdate_m3138645510 (FloatOperator_t3036552246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatOperator::DoFloatOperator()
extern "C"  void FloatOperator_DoFloatOperator_m1937943657 (FloatOperator_t3036552246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
