﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t3371940074;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass7_0__ctor_m2163627783 (U3CU3Ec__DisplayClass7_0_t3371940074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m4208448539 (U3CU3Ec__DisplayClass7_0_t3371940074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m390067829 (U3CU3Ec__DisplayClass7_0_t3371940074 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
