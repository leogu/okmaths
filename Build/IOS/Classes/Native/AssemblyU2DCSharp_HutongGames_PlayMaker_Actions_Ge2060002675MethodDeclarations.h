﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetColorRGBA
struct GetColorRGBA_t2060002675;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::.ctor()
extern "C"  void GetColorRGBA__ctor_m3483968099 (GetColorRGBA_t2060002675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::Reset()
extern "C"  void GetColorRGBA_Reset_m4279381828 (GetColorRGBA_t2060002675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::OnEnter()
extern "C"  void GetColorRGBA_OnEnter_m94002818 (GetColorRGBA_t2060002675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::OnUpdate()
extern "C"  void GetColorRGBA_OnUpdate_m148898667 (GetColorRGBA_t2060002675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetColorRGBA::DoGetColorRGBA()
extern "C"  void GetColorRGBA_DoGetColorRGBA_m291326337 (GetColorRGBA_t2060002675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
