﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation1571958496.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties
struct  uGuiNavigationExplicitSetProperties_t922100773  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::selectOnDown
	FsmGameObject_t3097142863 * ___selectOnDown_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::selectOnUp
	FsmGameObject_t3097142863 * ___selectOnUp_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::selectOnLeft
	FsmGameObject_t3097142863 * ___selectOnLeft_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::selectOnRight
	FsmGameObject_t3097142863 * ___selectOnRight_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_16;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::_selectable
	Selectable_t1490392188 * ____selectable_17;
	// UnityEngine.UI.Navigation HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::_navigation
	Navigation_t1571958496  ____navigation_18;
	// UnityEngine.UI.Navigation HutongGames.PlayMaker.Actions.uGuiNavigationExplicitSetProperties::_originalState
	Navigation_t1571958496  ____originalState_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_selectOnDown_12() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ___selectOnDown_12)); }
	inline FsmGameObject_t3097142863 * get_selectOnDown_12() const { return ___selectOnDown_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnDown_12() { return &___selectOnDown_12; }
	inline void set_selectOnDown_12(FsmGameObject_t3097142863 * value)
	{
		___selectOnDown_12 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnDown_12, value);
	}

	inline static int32_t get_offset_of_selectOnUp_13() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ___selectOnUp_13)); }
	inline FsmGameObject_t3097142863 * get_selectOnUp_13() const { return ___selectOnUp_13; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnUp_13() { return &___selectOnUp_13; }
	inline void set_selectOnUp_13(FsmGameObject_t3097142863 * value)
	{
		___selectOnUp_13 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnUp_13, value);
	}

	inline static int32_t get_offset_of_selectOnLeft_14() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ___selectOnLeft_14)); }
	inline FsmGameObject_t3097142863 * get_selectOnLeft_14() const { return ___selectOnLeft_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnLeft_14() { return &___selectOnLeft_14; }
	inline void set_selectOnLeft_14(FsmGameObject_t3097142863 * value)
	{
		___selectOnLeft_14 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnLeft_14, value);
	}

	inline static int32_t get_offset_of_selectOnRight_15() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ___selectOnRight_15)); }
	inline FsmGameObject_t3097142863 * get_selectOnRight_15() const { return ___selectOnRight_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_selectOnRight_15() { return &___selectOnRight_15; }
	inline void set_selectOnRight_15(FsmGameObject_t3097142863 * value)
	{
		___selectOnRight_15 = value;
		Il2CppCodeGenWriteBarrier(&___selectOnRight_15, value);
	}

	inline static int32_t get_offset_of_resetOnExit_16() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ___resetOnExit_16)); }
	inline FsmBool_t664485696 * get_resetOnExit_16() const { return ___resetOnExit_16; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_16() { return &___resetOnExit_16; }
	inline void set_resetOnExit_16(FsmBool_t664485696 * value)
	{
		___resetOnExit_16 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_16, value);
	}

	inline static int32_t get_offset_of__selectable_17() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ____selectable_17)); }
	inline Selectable_t1490392188 * get__selectable_17() const { return ____selectable_17; }
	inline Selectable_t1490392188 ** get_address_of__selectable_17() { return &____selectable_17; }
	inline void set__selectable_17(Selectable_t1490392188 * value)
	{
		____selectable_17 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_17, value);
	}

	inline static int32_t get_offset_of__navigation_18() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ____navigation_18)); }
	inline Navigation_t1571958496  get__navigation_18() const { return ____navigation_18; }
	inline Navigation_t1571958496 * get_address_of__navigation_18() { return &____navigation_18; }
	inline void set__navigation_18(Navigation_t1571958496  value)
	{
		____navigation_18 = value;
	}

	inline static int32_t get_offset_of__originalState_19() { return static_cast<int32_t>(offsetof(uGuiNavigationExplicitSetProperties_t922100773, ____originalState_19)); }
	inline Navigation_t1571958496  get__originalState_19() const { return ____originalState_19; }
	inline Navigation_t1571958496 * get_address_of__originalState_19() { return &____originalState_19; }
	inline void set__originalState_19(Navigation_t1571958496  value)
	{
		____originalState_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
