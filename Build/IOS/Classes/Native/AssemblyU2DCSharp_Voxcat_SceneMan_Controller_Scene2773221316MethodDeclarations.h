﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Voxcat.SceneMan.Controller.SceneController
struct SceneController_t2773221316;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Voxcat.SceneMan.Controller.SceneController::.ctor()
extern "C"  void SceneController__ctor_m3725483592 (SceneController_t2773221316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Voxcat.SceneMan.Controller.SceneController::.cctor()
extern "C"  void SceneController__cctor_m3456874983 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Voxcat.SceneMan.Controller.SceneController::LoadLevel(System.String,System.Single)
extern "C"  void SceneController_LoadLevel_m766673163 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, float ___loadingSceneWaitTime1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Voxcat.SceneMan.Controller.SceneController::LoadPreviousScene(System.Single)
extern "C"  void SceneController_LoadPreviousScene_m585621984 (Il2CppObject * __this /* static, unused */, float ___loadSceneDelayTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
