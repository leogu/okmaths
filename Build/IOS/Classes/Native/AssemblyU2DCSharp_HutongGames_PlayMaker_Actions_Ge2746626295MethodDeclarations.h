﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive
struct GetAnimatorFeetPivotActive_t2746626295;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::.ctor()
extern "C"  void GetAnimatorFeetPivotActive__ctor_m1086971913 (GetAnimatorFeetPivotActive_t2746626295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::Reset()
extern "C"  void GetAnimatorFeetPivotActive_Reset_m254217448 (GetAnimatorFeetPivotActive_t2746626295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::OnEnter()
extern "C"  void GetAnimatorFeetPivotActive_OnEnter_m2140577138 (GetAnimatorFeetPivotActive_t2746626295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorFeetPivotActive::DoGetFeetPivotActive()
extern "C"  void GetAnimatorFeetPivotActive_DoGetFeetPivotActive_m1188293916 (GetAnimatorFeetPivotActive_t2746626295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
