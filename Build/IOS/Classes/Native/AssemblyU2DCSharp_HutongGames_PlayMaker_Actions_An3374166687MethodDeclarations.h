﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateVector3
struct AnimateVector3_t3374166687;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::.ctor()
extern "C"  void AnimateVector3__ctor_m3656359159 (AnimateVector3_t3374166687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::Reset()
extern "C"  void AnimateVector3_Reset_m158116784 (AnimateVector3_t3374166687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::OnEnter()
extern "C"  void AnimateVector3_OnEnter_m1556940254 (AnimateVector3_t3374166687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::UpdateVariableValue()
extern "C"  void AnimateVector3_UpdateVariableValue_m2164312819 (AnimateVector3_t3374166687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateVector3::OnUpdate()
extern "C"  void AnimateVector3_OnUpdate_m3557263807 (AnimateVector3_t3374166687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
