﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.BoolOperator
struct BoolOperator_t4161719424;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.BoolOperator::.ctor()
extern "C"  void BoolOperator__ctor_m2178882540 (BoolOperator_t4161719424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::Reset()
extern "C"  void BoolOperator_Reset_m4137697869 (BoolOperator_t4161719424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::OnEnter()
extern "C"  void BoolOperator_OnEnter_m4294778021 (BoolOperator_t4161719424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::OnUpdate()
extern "C"  void BoolOperator_OnUpdate_m1200360930 (BoolOperator_t4161719424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.BoolOperator::DoBoolOperator()
extern "C"  void BoolOperator_DoBoolOperator_m2165996673 (BoolOperator_t4161719424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
