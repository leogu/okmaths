﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Advertisements.UnityAdsPlatform
struct UnityAdsPlatform_t714383836;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Advertisements.UnityAdsPlatform::.ctor()
extern "C"  void UnityAdsPlatform__ctor_m1904688255 (UnityAdsPlatform_t714383836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
