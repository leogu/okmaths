﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableContainsKey
struct  HashTableContainsKey_t2348818626  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableContainsKey::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableContainsKey::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableContainsKey::key
	FsmString_t2414474701 * ___key_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.HashTableContainsKey::containsKey
	FsmBool_t664485696 * ___containsKey_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableContainsKey::keyFoundEvent
	FsmEvent_t1258573736 * ___keyFoundEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableContainsKey::keyNotFoundEvent
	FsmEvent_t1258573736 * ___keyNotFoundEvent_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableContainsKey_t2348818626, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableContainsKey_t2348818626, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(HashTableContainsKey_t2348818626, ___key_14)); }
	inline FsmString_t2414474701 * get_key_14() const { return ___key_14; }
	inline FsmString_t2414474701 ** get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(FsmString_t2414474701 * value)
	{
		___key_14 = value;
		Il2CppCodeGenWriteBarrier(&___key_14, value);
	}

	inline static int32_t get_offset_of_containsKey_15() { return static_cast<int32_t>(offsetof(HashTableContainsKey_t2348818626, ___containsKey_15)); }
	inline FsmBool_t664485696 * get_containsKey_15() const { return ___containsKey_15; }
	inline FsmBool_t664485696 ** get_address_of_containsKey_15() { return &___containsKey_15; }
	inline void set_containsKey_15(FsmBool_t664485696 * value)
	{
		___containsKey_15 = value;
		Il2CppCodeGenWriteBarrier(&___containsKey_15, value);
	}

	inline static int32_t get_offset_of_keyFoundEvent_16() { return static_cast<int32_t>(offsetof(HashTableContainsKey_t2348818626, ___keyFoundEvent_16)); }
	inline FsmEvent_t1258573736 * get_keyFoundEvent_16() const { return ___keyFoundEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_keyFoundEvent_16() { return &___keyFoundEvent_16; }
	inline void set_keyFoundEvent_16(FsmEvent_t1258573736 * value)
	{
		___keyFoundEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___keyFoundEvent_16, value);
	}

	inline static int32_t get_offset_of_keyNotFoundEvent_17() { return static_cast<int32_t>(offsetof(HashTableContainsKey_t2348818626, ___keyNotFoundEvent_17)); }
	inline FsmEvent_t1258573736 * get_keyNotFoundEvent_17() const { return ___keyNotFoundEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_keyNotFoundEvent_17() { return &___keyNotFoundEvent_17; }
	inline void set_keyNotFoundEvent_17(FsmEvent_t1258573736 * value)
	{
		___keyNotFoundEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___keyNotFoundEvent_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
