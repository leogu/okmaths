﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t426334949;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass9_0__ctor_m881455161 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__0_m3655295499 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__1_m133828321 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__2()
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__2_m2315173284 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__3()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__3_m3655295338 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass9_0::<DOJump>b__4(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass9_0_U3CDOJumpU3Eb__4_m2693710174 (U3CU3Ec__DisplayClass9_0_t426334949 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
