﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin
struct RectTransformSetAnchorMin_t1974968069;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::.ctor()
extern "C"  void RectTransformSetAnchorMin__ctor_m1825503011 (RectTransformSetAnchorMin_t1974968069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::Reset()
extern "C"  void RectTransformSetAnchorMin_Reset_m2448286366 (RectTransformSetAnchorMin_t1974968069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::OnEnter()
extern "C"  void RectTransformSetAnchorMin_OnEnter_m2454798112 (RectTransformSetAnchorMin_t1974968069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::OnActionUpdate()
extern "C"  void RectTransformSetAnchorMin_OnActionUpdate_m2828737973 (RectTransformSetAnchorMin_t1974968069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorMin::DoSetAnchorMin()
extern "C"  void RectTransformSetAnchorMin_DoSetAnchorMin_m3382573685 (RectTransformSetAnchorMin_t1974968069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
