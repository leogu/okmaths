﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralVector2
struct SetProceduralVector2_t1816528758;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::.ctor()
extern "C"  void SetProceduralVector2__ctor_m2142369310 (SetProceduralVector2_t1816528758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::Reset()
extern "C"  void SetProceduralVector2_Reset_m2976210271 (SetProceduralVector2_t1816528758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::OnEnter()
extern "C"  void SetProceduralVector2_OnEnter_m3798116063 (SetProceduralVector2_t1816528758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::OnUpdate()
extern "C"  void SetProceduralVector2_OnUpdate_m974517032 (SetProceduralVector2_t1816528758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector2::DoSetProceduralVector()
extern "C"  void SetProceduralVector2_DoSetProceduralVector_m2108403031 (SetProceduralVector2_t1816528758 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
