﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerProxyBase
struct PlayMakerProxyBase_t3141506643;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayMakerProxyBase::Awake()
extern "C"  void PlayMakerProxyBase_Awake_m2433124823 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerProxyBase::Reset()
extern "C"  void PlayMakerProxyBase_Reset_m3397845475 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerProxyBase::.ctor()
extern "C"  void PlayMakerProxyBase__ctor_m547361492 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
