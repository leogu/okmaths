﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs
struct NetworkViewRemoveRPCs_t1048117247;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::.ctor()
extern "C"  void NetworkViewRemoveRPCs__ctor_m2581456457 (NetworkViewRemoveRPCs_t1048117247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::Reset()
extern "C"  void NetworkViewRemoveRPCs_Reset_m3398213908 (NetworkViewRemoveRPCs_t1048117247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::OnEnter()
extern "C"  void NetworkViewRemoveRPCs_OnEnter_m3257900230 (NetworkViewRemoveRPCs_t1048117247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkViewRemoveRPCs::DoRemoveRPCsFromViewID()
extern "C"  void NetworkViewRemoveRPCs_DoRemoveRPCsFromViewID_m1755880946 (NetworkViewRemoveRPCs_t1048117247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
