﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionData
struct ActionData_t1467934444;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat>
struct List_1_t306255110;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt>
struct List_1_t642130311;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool>
struct List_1_t33606828;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2>
struct List_1_t1799571195;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3>
struct List_1_t3365655136;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor>
struct List_1_t3782390393;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect>
struct List_1_t3683111782;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion>
struct List_1_t247559888;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString>
struct List_1_t1783595833;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject>
struct List_1_t2154915445;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject>
struct List_1_t2466263995;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault>
struct List_1_t1392795316;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve>
struct List_1_t3990835989;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall>
struct List_1_t2123791062;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl>
struct List_1_t293398091;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar>
struct List_1_t2241713645;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray>
struct List_1_t4191548321;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum>
struct List_1_t2177637235;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty>
struct List_1_t155874627;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget>
struct List_1_t3836382173;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption>
struct List_1_t3068713609;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t125053523;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1497896004;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// HutongGames.PlayMaker.ActionData/Context
struct Context_t2765413152;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// System.Object
struct Il2CppObject;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Array
struct Il2CppArray;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmRect
struct FsmRect_t19023354;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmTemplateControl
struct FsmTemplateControl_t924276959;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;
// HutongGames.PlayMaker.FunctionCall
struct FunctionCall_t2754669930;
// HutongGames.PlayMaker.FsmProperty
struct FsmProperty_t786753495;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// HutongGames.PlayMaker.LayoutOption
struct LayoutOption_t3699592477;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmMaterial
struct FsmMaterial_t1421632035;
// HutongGames.PlayMaker.FsmTexture
struct FsmTexture_t3372293163;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t340212445;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionData_Context2765413152.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"

// System.Int32 HutongGames.PlayMaker.ActionData::get_ActionCount()
extern "C"  int32_t ActionData_get_ActionCount_m1338650817 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionData HutongGames.PlayMaker.ActionData::Copy()
extern "C"  ActionData_t1467934444 * ActionData_Copy_m2342128005 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.ActionData::CopyStringParams()
extern "C"  List_1_t1398341365 * ActionData_CopyStringParams_m341734436 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmFloat> HutongGames.PlayMaker.ActionData::CopyFsmFloatParams()
extern "C"  List_1_t306255110 * ActionData_CopyFsmFloatParams_m2953368453 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmInt> HutongGames.PlayMaker.ActionData::CopyFsmIntParams()
extern "C"  List_1_t642130311 * ActionData_CopyFsmIntParams_m4188845829 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmBool> HutongGames.PlayMaker.ActionData::CopyFsmBoolParams()
extern "C"  List_1_t33606828 * ActionData_CopyFsmBoolParams_m3743836321 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector2> HutongGames.PlayMaker.ActionData::CopyFsmVector2Params()
extern "C"  List_1_t1799571195 * ActionData_CopyFsmVector2Params_m2830742981 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVector3> HutongGames.PlayMaker.ActionData::CopyFsmVector3Params()
extern "C"  List_1_t3365655136 * ActionData_CopyFsmVector3Params_m3480801893 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmColor> HutongGames.PlayMaker.ActionData::CopyFsmColorParams()
extern "C"  List_1_t3782390393 * ActionData_CopyFsmColorParams_m2744247557 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmRect> HutongGames.PlayMaker.ActionData::CopyFsmRectParams()
extern "C"  List_1_t3683111782 * ActionData_CopyFsmRectParams_m1053003549 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmQuaternion> HutongGames.PlayMaker.ActionData::CopyFsmQuaternionParams()
extern "C"  List_1_t247559888 * ActionData_CopyFsmQuaternionParams_m1702650397 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmString> HutongGames.PlayMaker.ActionData::CopyFsmStringParams()
extern "C"  List_1_t1783595833 * ActionData_CopyFsmStringParams_m2676513073 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmObject> HutongGames.PlayMaker.ActionData::CopyFsmObjectParams()
extern "C"  List_1_t2154915445 * ActionData_CopyFsmObjectParams_m1024885265 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmGameObject> HutongGames.PlayMaker.ActionData::CopyFsmGameObjectParams()
extern "C"  List_1_t2466263995 * ActionData_CopyFsmGameObjectParams_m2846175845 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmOwnerDefault> HutongGames.PlayMaker.ActionData::CopyFsmOwnerDefaultParams()
extern "C"  List_1_t1392795316 * ActionData_CopyFsmOwnerDefaultParams_m2134605001 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmAnimationCurve> HutongGames.PlayMaker.ActionData::CopyAnimationCurveParams()
extern "C"  List_1_t3990835989 * ActionData_CopyAnimationCurveParams_m4184713465 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FunctionCall> HutongGames.PlayMaker.ActionData::CopyFunctionCallParams()
extern "C"  List_1_t2123791062 * ActionData_CopyFunctionCallParams_m1807913029 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmTemplateControl> HutongGames.PlayMaker.ActionData::CopyFsmTemplateControlParams()
extern "C"  List_1_t293398091 * ActionData_CopyFsmTemplateControlParams_m3828199045 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmVar> HutongGames.PlayMaker.ActionData::CopyFsmVarParams()
extern "C"  List_1_t2241713645 * ActionData_CopyFsmVarParams_m185437829 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmArray> HutongGames.PlayMaker.ActionData::CopyFsmArrayParams()
extern "C"  List_1_t4191548321 * ActionData_CopyFsmArrayParams_m2583863429 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEnum> HutongGames.PlayMaker.ActionData::CopyFsmEnumParams()
extern "C"  List_1_t2177637235 * ActionData_CopyFsmEnumParams_m601940485 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmProperty> HutongGames.PlayMaker.ActionData::CopyFsmPropertyParams()
extern "C"  List_1_t155874627 * ActionData_CopyFsmPropertyParams_m4031719965 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEventTarget> HutongGames.PlayMaker.ActionData::CopyFsmEventTargetParams()
extern "C"  List_1_t3836382173 * ActionData_CopyFsmEventTargetParams_m1255134597 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.LayoutOption> HutongGames.PlayMaker.ActionData::CopyLayoutOptionParams()
extern "C"  List_1_t3068713609 * ActionData_CopyLayoutOptionParams_m516167813 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::ClearActionData()
extern "C"  void ActionData_ClearActionData_m2363086970 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.ActionData::GetActionType(System.String)
extern "C"  Type_t * ActionData_GetActionType_m1629698017 (Il2CppObject * __this /* static, unused */, String_t* ___actionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo[] HutongGames.PlayMaker.ActionData::GetFields(System.Type)
extern "C"  FieldInfoU5BU5D_t125053523* ActionData_GetFields_m3041546842 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionData::GetActionTypeHashCode(System.Type)
extern "C"  int32_t ActionData_GetActionTypeHashCode_m394868013 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionData::GetStableHash(System.String)
extern "C"  int32_t ActionData_GetStableHash_m1544244038 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.ActionData::LoadActions(HutongGames.PlayMaker.FsmState)
extern "C"  FsmStateActionU5BU5D_t1497896004* ActionData_LoadActions_m63635267 (ActionData_t1467934444 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::CreateAction(HutongGames.PlayMaker.FsmState,System.Int32)
extern "C"  FsmStateAction_t2862378169 * ActionData_CreateAction_m1559844881 (ActionData_t1467934444 * __this, FsmState_t1643911659 * ___state0, int32_t ___actionIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::CreateAction(HutongGames.PlayMaker.ActionData/Context,System.Int32)
extern "C"  FsmStateAction_t2862378169 * ActionData_CreateAction_m3887299720 (ActionData_t1467934444 * __this, Context_t2765413152 * ___context0, int32_t ___actionIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LoadActionField(HutongGames.PlayMaker.Fsm,System.Object,System.Reflection.FieldInfo,System.Int32)
extern "C"  void ActionData_LoadActionField_m2117740638 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, Il2CppObject * ___obj1, FieldInfo_t * ___field2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LoadArrayElement(HutongGames.PlayMaker.Fsm,System.Array,System.Type,System.Int32,System.Int32)
extern "C"  void ActionData_LoadArrayElement_m3883674729 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, Il2CppArray * ___field1, Type_t * ___fieldType2, int32_t ___elementIndex3, int32_t ___paramIndex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LogError(HutongGames.PlayMaker.ActionData/Context,System.String)
extern "C"  void ActionData_LogError_m3612830801 (Il2CppObject * __this /* static, unused */, Context_t2765413152 * ___context0, String_t* ___error1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::LogInfo(HutongGames.PlayMaker.ActionData/Context,System.String)
extern "C"  void ActionData_LogInfo_m1115933611 (Il2CppObject * __this /* static, unused */, Context_t2765413152 * ___context0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.ActionData::GetFsmFloat(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmFloat_t937133978 * ActionData_GetFsmFloat_m1382487155 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.ActionData::GetFsmInt(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmInt_t1273009179 * ActionData_GetFsmInt_m901971059 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.ActionData::GetFsmBool(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmBool_t664485696 * ActionData_GetFsmBool_m1767229859 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.ActionData::GetFsmVector2(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmVector2_t2430450063 * ActionData_GetFsmVector2_m3447613171 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.ActionData::GetFsmVector3(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmVector3_t3996534004 * ActionData_GetFsmVector3_m97058419 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor HutongGames.PlayMaker.ActionData::GetFsmColor(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmColor_t118301965 * ActionData_GetFsmColor_m2024868755 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmRect HutongGames.PlayMaker.ActionData::GetFsmRect(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmRect_t19023354 * ActionData_GetFsmRect_m2693101971 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion HutongGames.PlayMaker.ActionData::GetFsmQuaternion(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmQuaternion_t878438756 * ActionData_GetFsmQuaternion_m93728691 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.ActionData::GetFsmGameObject(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmGameObject_t3097142863 * ActionData_GetFsmGameObject_m3582299887 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTemplateControl HutongGames.PlayMaker.ActionData::GetFsmTemplateControl(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmTemplateControl_t924276959 * ActionData_GetFsmTemplateControl_m3648355955 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.ActionData::GetFsmVar(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmVar_t2872592513 * ActionData_GetFsmVar_m3584855475 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.ActionData::GetFsmArray(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmArray_t527459893 * ActionData_GetFsmArray_m2276341651 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.ActionData::GetFsmEnum(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmEnum_t2808516103 * ActionData_GetFsmEnum_m1723943135 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FunctionCall HutongGames.PlayMaker.ActionData::GetFunctionCall(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FunctionCall_t2754669930 * ActionData_GetFunctionCall_m3014715123 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmProperty HutongGames.PlayMaker.ActionData::GetFsmProperty(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmProperty_t786753495 * ActionData_GetFsmProperty_m4177637239 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEventTarget HutongGames.PlayMaker.ActionData::GetFsmEventTarget(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmEventTarget_t172293745 * ActionData_GetFsmEventTarget_m1114348083 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.LayoutOption HutongGames.PlayMaker.ActionData::GetLayoutOption(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  LayoutOption_t3699592477 * ActionData_GetLayoutOption_m2321585811 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.ActionData::GetFsmOwnerDefault(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmOwnerDefault_t2023674184 * ActionData_GetFsmOwnerDefault_m3389969547 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.ActionData::GetFsmString(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmString_t2414474701 * ActionData_GetFsmString_m3568734895 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.ActionData::GetFsmObject(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmObject_t2785794313 * ActionData_GetFsmObject_m4214255631 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmMaterial HutongGames.PlayMaker.ActionData::GetFsmMaterial(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmMaterial_t1421632035 * ActionData_GetFsmMaterial_m1544482831 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTexture HutongGames.PlayMaker.ActionData::GetFsmTexture(HutongGames.PlayMaker.Fsm,System.Int32)
extern "C"  FsmTexture_t3372293163 * ActionData_GetFsmTexture_m4092447859 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::UsesDataVersion2()
extern "C"  bool ActionData_UsesDataVersion2_m1115962231 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.ActionData::TryFixActionName(System.String)
extern "C"  String_t* ActionData_TryFixActionName_m1991191957 (Il2CppObject * __this /* static, unused */, String_t* ___actionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction HutongGames.PlayMaker.ActionData::TryRecoverAction(HutongGames.PlayMaker.ActionData/Context,System.Type,HutongGames.PlayMaker.FsmStateAction,System.Int32)
extern "C"  FsmStateAction_t2862378169 * ActionData_TryRecoverAction_m2511207261 (ActionData_t1467934444 * __this, Context_t2765413152 * ___context0, Type_t * ___actionType1, FsmStateAction_t2862378169 * ___action2, int32_t ___actionIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo HutongGames.PlayMaker.ActionData::FindField(System.Type,System.Int32)
extern "C"  FieldInfo_t * ActionData_FindField_m5878047 (ActionData_t1467934444 * __this, Type_t * ___actionType0, int32_t ___paramIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.FieldInfo HutongGames.PlayMaker.ActionData::FindField(System.Type,System.String)
extern "C"  FieldInfo_t * ActionData_FindField_m3622128922 (Il2CppObject * __this /* static, unused */, Type_t * ___actionType0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::TryConvertParameter(HutongGames.PlayMaker.ActionData/Context,HutongGames.PlayMaker.FsmStateAction,System.Reflection.FieldInfo,System.Int32)
extern "C"  bool ActionData_TryConvertParameter_m375850848 (ActionData_t1467934444 * __this, Context_t2765413152 * ___context0, FsmStateAction_t2862378169 * ___action1, FieldInfo_t * ___field2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionData::TryConvertArrayElement(HutongGames.PlayMaker.Fsm,System.Array,HutongGames.PlayMaker.ParamDataType,HutongGames.PlayMaker.ParamDataType,System.Int32,System.Int32)
extern "C"  bool ActionData_TryConvertArrayElement_m2720618964 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, Il2CppArray * ___field1, int32_t ___originalParamType2, int32_t ___currentParamType3, int32_t ___elementIndex4, int32_t ___paramIndex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.ActionData::ConvertType(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.ParamDataType,HutongGames.PlayMaker.ParamDataType,System.Int32)
extern "C"  Il2CppObject * ActionData_ConvertType_m992211666 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, int32_t ___originalParamType1, int32_t ___currentParamType2, int32_t ___paramIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveActions(HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction[])
extern "C"  void ActionData_SaveActions_m1637254931 (ActionData_t1467934444 * __this, FsmState_t1643911659 * ___state0, FsmStateActionU5BU5D_t1497896004* ___actions1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveAction(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmStateAction)
extern "C"  void ActionData_SaveAction_m717666845 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, FsmStateAction_t2862378169 * ___action1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveActionField(HutongGames.PlayMaker.Fsm,System.Type,System.Object)
extern "C"  void ActionData_SaveActionField_m425980725 (ActionData_t1467934444 * __this, Fsm_t917886356 * ___fsm0, Type_t * ___fieldType1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::AddByteData(System.Collections.Generic.ICollection`1<System.Byte>)
extern "C"  void ActionData_AddByteData_m3552540132 (ActionData_t1467934444 * __this, Il2CppObject* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::SaveString(System.String)
extern "C"  void ActionData_SaveString_m532521245 (ActionData_t1467934444 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ParamDataType HutongGames.PlayMaker.ActionData::GetParamDataType(System.Type)
extern "C"  int32_t ActionData_GetParamDataType_m725564627 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::.ctor()
extern "C"  void ActionData__ctor_m3078690827 (ActionData_t1467934444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionData::.cctor()
extern "C"  void ActionData__cctor_m1481507448 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
