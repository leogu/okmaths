﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenRotateTo
struct iTweenRotateTo_t4036511040;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::.ctor()
extern "C"  void iTweenRotateTo__ctor_m1130740628 (iTweenRotateTo_t4036511040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::Reset()
extern "C"  void iTweenRotateTo_Reset_m2574940805 (iTweenRotateTo_t4036511040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::OnEnter()
extern "C"  void iTweenRotateTo_OnEnter_m1881780837 (iTweenRotateTo_t4036511040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::OnExit()
extern "C"  void iTweenRotateTo_OnExit_m1429481633 (iTweenRotateTo_t4036511040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenRotateTo::DoiTween()
extern "C"  void iTweenRotateTo_DoiTween_m3196508691 (iTweenRotateTo_t4036511040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
