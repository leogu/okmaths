﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListResetValues
struct ArrayListResetValues_t1751962620;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListResetValues::.ctor()
extern "C"  void ArrayListResetValues__ctor_m1069410618 (ArrayListResetValues_t1751962620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListResetValues::Reset()
extern "C"  void ArrayListResetValues_Reset_m3646713257 (ArrayListResetValues_t1751962620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListResetValues::OnEnter()
extern "C"  void ArrayListResetValues_OnEnter_m3054109689 (ArrayListResetValues_t1751962620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListResetValues::ResetArrayList()
extern "C"  void ArrayListResetValues_ResetArrayList_m3796489548 (ArrayListResetValues_t1751962620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
