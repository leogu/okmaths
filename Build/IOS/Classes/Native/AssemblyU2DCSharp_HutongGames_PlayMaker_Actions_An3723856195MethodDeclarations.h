﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimationSettings
struct AnimationSettings_t3723856195;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::.ctor()
extern "C"  void AnimationSettings__ctor_m48365361 (AnimationSettings_t3723856195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::Reset()
extern "C"  void AnimationSettings_Reset_m3082940676 (AnimationSettings_t3723856195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::OnEnter()
extern "C"  void AnimationSettings_OnEnter_m2526697694 (AnimationSettings_t3723856195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimationSettings::DoAnimationSettings()
extern "C"  void AnimationSettings_DoAnimationSettings_m3785843969 (AnimationSettings_t3723856195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
