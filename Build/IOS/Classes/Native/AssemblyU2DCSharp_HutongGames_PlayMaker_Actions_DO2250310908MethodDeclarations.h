﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenLayoutElementFlexibleSize
struct DOTweenLayoutElementFlexibleSize_t2250310908;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementFlexibleSize::.ctor()
extern "C"  void DOTweenLayoutElementFlexibleSize__ctor_m1403387704 (DOTweenLayoutElementFlexibleSize_t2250310908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementFlexibleSize::Reset()
extern "C"  void DOTweenLayoutElementFlexibleSize_Reset_m2235913249 (DOTweenLayoutElementFlexibleSize_t2250310908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementFlexibleSize::OnEnter()
extern "C"  void DOTweenLayoutElementFlexibleSize_OnEnter_m420241073 (DOTweenLayoutElementFlexibleSize_t2250310908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementFlexibleSize::<OnEnter>m__40()
extern "C"  void DOTweenLayoutElementFlexibleSize_U3COnEnterU3Em__40_m621492324 (DOTweenLayoutElementFlexibleSize_t2250310908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenLayoutElementFlexibleSize::<OnEnter>m__41()
extern "C"  void DOTweenLayoutElementFlexibleSize_U3COnEnterU3Em__41_m3370578655 (DOTweenLayoutElementFlexibleSize_t2250310908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
