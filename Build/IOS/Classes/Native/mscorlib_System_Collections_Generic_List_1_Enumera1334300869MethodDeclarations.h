﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m898983362(__this, ___l0, method) ((  void (*) (Enumerator_t1334300869 *, List_1_t1799571195 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3658850416(__this, method) ((  void (*) (Enumerator_t1334300869 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2807166318(__this, method) ((  Il2CppObject * (*) (Enumerator_t1334300869 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::Dispose()
#define Enumerator_Dispose_m2752090219(__this, method) ((  void (*) (Enumerator_t1334300869 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::VerifyState()
#define Enumerator_VerifyState_m2941246884(__this, method) ((  void (*) (Enumerator_t1334300869 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::MoveNext()
#define Enumerator_MoveNext_m2134099245(__this, method) ((  bool (*) (Enumerator_t1334300869 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmVector2>::get_Current()
#define Enumerator_get_Current_m964530145(__this, method) ((  FsmVector2_t2430450063 * (*) (Enumerator_t1334300869 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
