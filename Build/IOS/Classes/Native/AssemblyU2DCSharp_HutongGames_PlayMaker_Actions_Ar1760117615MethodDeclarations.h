﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSetVertexColors
struct ArrayListSetVertexColors_t1760117615;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexColors::.ctor()
extern "C"  void ArrayListSetVertexColors__ctor_m1313450809 (ArrayListSetVertexColors_t1760117615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexColors::Reset()
extern "C"  void ArrayListSetVertexColors_Reset_m3036229240 (ArrayListSetVertexColors_t1760117615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexColors::OnEnter()
extern "C"  void ArrayListSetVertexColors_OnEnter_m2714425562 (ArrayListSetVertexColors_t1760117615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexColors::OnUpdate()
extern "C"  void ArrayListSetVertexColors_OnUpdate_m1189863405 (ArrayListSetVertexColors_t1760117615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSetVertexColors::SetVertexColors()
extern "C"  void ArrayListSetVertexColors_SetVertexColors_m2634231693 (ArrayListSetVertexColors_t1760117615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
