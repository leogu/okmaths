﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FloatAbs
struct FloatAbs_t1838216842;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FloatAbs::.ctor()
extern "C"  void FloatAbs__ctor_m3344745634 (FloatAbs_t1838216842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::Reset()
extern "C"  void FloatAbs_Reset_m1625726459 (FloatAbs_t1838216842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnEnter()
extern "C"  void FloatAbs_OnEnter_m2892027763 (FloatAbs_t1838216842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::OnUpdate()
extern "C"  void FloatAbs_OnUpdate_m3956434676 (FloatAbs_t1838216842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FloatAbs::DoFloatAbs()
extern "C"  void FloatAbs_DoFloatAbs_m1243669889 (FloatAbs_t1838216842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
