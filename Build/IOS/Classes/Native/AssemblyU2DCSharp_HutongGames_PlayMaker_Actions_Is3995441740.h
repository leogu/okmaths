﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsPointerOverUiObject
struct  IsPointerOverUiObject_t3995441740  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IsPointerOverUiObject::pointerId
	FsmInt_t1273009179 * ___pointerId_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsPointerOverUiObject::pointerOverUI
	FsmEvent_t1258573736 * ___pointerOverUI_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsPointerOverUiObject::pointerNotOverUI
	FsmEvent_t1258573736 * ___pointerNotOverUI_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsPointerOverUiObject::isPointerOverUI
	FsmBool_t664485696 * ___isPointerOverUI_14;
	// System.Boolean HutongGames.PlayMaker.Actions.IsPointerOverUiObject::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_pointerId_11() { return static_cast<int32_t>(offsetof(IsPointerOverUiObject_t3995441740, ___pointerId_11)); }
	inline FsmInt_t1273009179 * get_pointerId_11() const { return ___pointerId_11; }
	inline FsmInt_t1273009179 ** get_address_of_pointerId_11() { return &___pointerId_11; }
	inline void set_pointerId_11(FsmInt_t1273009179 * value)
	{
		___pointerId_11 = value;
		Il2CppCodeGenWriteBarrier(&___pointerId_11, value);
	}

	inline static int32_t get_offset_of_pointerOverUI_12() { return static_cast<int32_t>(offsetof(IsPointerOverUiObject_t3995441740, ___pointerOverUI_12)); }
	inline FsmEvent_t1258573736 * get_pointerOverUI_12() const { return ___pointerOverUI_12; }
	inline FsmEvent_t1258573736 ** get_address_of_pointerOverUI_12() { return &___pointerOverUI_12; }
	inline void set_pointerOverUI_12(FsmEvent_t1258573736 * value)
	{
		___pointerOverUI_12 = value;
		Il2CppCodeGenWriteBarrier(&___pointerOverUI_12, value);
	}

	inline static int32_t get_offset_of_pointerNotOverUI_13() { return static_cast<int32_t>(offsetof(IsPointerOverUiObject_t3995441740, ___pointerNotOverUI_13)); }
	inline FsmEvent_t1258573736 * get_pointerNotOverUI_13() const { return ___pointerNotOverUI_13; }
	inline FsmEvent_t1258573736 ** get_address_of_pointerNotOverUI_13() { return &___pointerNotOverUI_13; }
	inline void set_pointerNotOverUI_13(FsmEvent_t1258573736 * value)
	{
		___pointerNotOverUI_13 = value;
		Il2CppCodeGenWriteBarrier(&___pointerNotOverUI_13, value);
	}

	inline static int32_t get_offset_of_isPointerOverUI_14() { return static_cast<int32_t>(offsetof(IsPointerOverUiObject_t3995441740, ___isPointerOverUI_14)); }
	inline FsmBool_t664485696 * get_isPointerOverUI_14() const { return ___isPointerOverUI_14; }
	inline FsmBool_t664485696 ** get_address_of_isPointerOverUI_14() { return &___isPointerOverUI_14; }
	inline void set_isPointerOverUI_14(FsmBool_t664485696 * value)
	{
		___isPointerOverUI_14 = value;
		Il2CppCodeGenWriteBarrier(&___isPointerOverUI_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(IsPointerOverUiObject_t3995441740, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
