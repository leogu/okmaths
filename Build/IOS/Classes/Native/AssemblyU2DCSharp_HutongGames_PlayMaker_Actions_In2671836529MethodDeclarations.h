﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IntOperator
struct IntOperator_t2671836529;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IntOperator::.ctor()
extern "C"  void IntOperator__ctor_m898627025 (IntOperator_t2671836529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntOperator::Reset()
extern "C"  void IntOperator_Reset_m2442166766 (IntOperator_t2671836529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntOperator::OnEnter()
extern "C"  void IntOperator_OnEnter_m4165213644 (IntOperator_t2671836529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntOperator::OnUpdate()
extern "C"  void IntOperator_OnUpdate_m2172142485 (IntOperator_t2671836529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IntOperator::DoIntOperator()
extern "C"  void IntOperator_DoIntOperator_m1347200673 (IntOperator_t2671836529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
