﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps
struct uGuiScrollbarSetNumberOfSteps_t1691940435;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps::.ctor()
extern "C"  void uGuiScrollbarSetNumberOfSteps__ctor_m719799573 (uGuiScrollbarSetNumberOfSteps_t1691940435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps::Reset()
extern "C"  void uGuiScrollbarSetNumberOfSteps_Reset_m1308774504 (uGuiScrollbarSetNumberOfSteps_t1691940435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps::OnEnter()
extern "C"  void uGuiScrollbarSetNumberOfSteps_OnEnter_m1940494106 (uGuiScrollbarSetNumberOfSteps_t1691940435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps::OnUpdate()
extern "C"  void uGuiScrollbarSetNumberOfSteps_OnUpdate_m2726507689 (uGuiScrollbarSetNumberOfSteps_t1691940435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps::DoSetValue()
extern "C"  void uGuiScrollbarSetNumberOfSteps_DoSetValue_m190974763 (uGuiScrollbarSetNumberOfSteps_t1691940435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollbarSetNumberOfSteps::OnExit()
extern "C"  void uGuiScrollbarSetNumberOfSteps_OnExit_m2726633470 (uGuiScrollbarSetNumberOfSteps_t1691940435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
