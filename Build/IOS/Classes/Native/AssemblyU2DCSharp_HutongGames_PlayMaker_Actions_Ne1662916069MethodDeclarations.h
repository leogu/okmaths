﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties
struct NetworkGetLocalPlayerProperties_t1662916069;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties::.ctor()
extern "C"  void NetworkGetLocalPlayerProperties__ctor_m2722214885 (NetworkGetLocalPlayerProperties_t1662916069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties::Reset()
extern "C"  void NetworkGetLocalPlayerProperties_Reset_m870969178 (NetworkGetLocalPlayerProperties_t1662916069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetLocalPlayerProperties::OnEnter()
extern "C"  void NetworkGetLocalPlayerProperties_OnEnter_m4088472336 (NetworkGetLocalPlayerProperties_t1662916069 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
