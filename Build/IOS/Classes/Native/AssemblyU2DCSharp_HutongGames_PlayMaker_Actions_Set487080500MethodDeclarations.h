﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmRect
struct SetFsmRect_t487080500;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::.ctor()
extern "C"  void SetFsmRect__ctor_m1494113240 (SetFsmRect_t487080500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::Reset()
extern "C"  void SetFsmRect_Reset_m4071714497 (SetFsmRect_t487080500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::OnEnter()
extern "C"  void SetFsmRect_OnEnter_m1958827369 (SetFsmRect_t487080500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::DoSetFsmBool()
extern "C"  void SetFsmRect_DoSetFsmBool_m3764728407 (SetFsmRect_t487080500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmRect::OnUpdate()
extern "C"  void SetFsmRect_OnUpdate_m2892650862 (SetFsmRect_t487080500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
