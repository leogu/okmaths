﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimatorMatchTarget
struct AnimatorMatchTarget_t2443589419;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::.ctor()
extern "C"  void AnimatorMatchTarget__ctor_m2902563795 (AnimatorMatchTarget_t2443589419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::Reset()
extern "C"  void AnimatorMatchTarget_Reset_m3608670560 (AnimatorMatchTarget_t2443589419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::OnEnter()
extern "C"  void AnimatorMatchTarget_OnEnter_m3853574102 (AnimatorMatchTarget_t2443589419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::OnUpdate()
extern "C"  void AnimatorMatchTarget_OnUpdate_m4213619547 (AnimatorMatchTarget_t2443589419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimatorMatchTarget::DoMatchTarget()
extern "C"  void AnimatorMatchTarget_DoMatchTarget_m859461204 (AnimatorMatchTarget_t2443589419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
