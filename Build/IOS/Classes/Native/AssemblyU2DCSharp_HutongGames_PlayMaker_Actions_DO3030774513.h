﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmAnimationCurve
struct FsmAnimationCurve_t326747561;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// DG.Tweening.Tweener
struct Tweener_t760404022;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_TweenId2061850634.h"
#include "AssemblyU2DCSharp_DOTweenActionsEnums_SelectedEase2113376909.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize
struct  DOTweenTrailRendererResize_t3030774513  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::toStartWidth
	FsmFloat_t937133978 * ___toStartWidth_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::toEndWidth
	FsmFloat_t937133978 * ___toEndWidth_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::duration
	FsmFloat_t937133978 * ___duration_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::setSpeedBased
	FsmBool_t664485696 * ___setSpeedBased_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::startDelay
	FsmFloat_t937133978 * ___startDelay_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::startEvent
	FsmEvent_t1258573736 * ___startEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::finishEvent
	FsmEvent_t1258573736 * ___finishEvent_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::finishImmediately
	FsmBool_t664485696 * ___finishImmediately_19;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::tweenIdDescription
	String_t* ___tweenIdDescription_20;
	// DOTweenActionsEnums/TweenId HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::tweenIdType
	int32_t ___tweenIdType_21;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::stringAsId
	FsmString_t2414474701 * ___stringAsId_22;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::tagAsId
	FsmString_t2414474701 * ___tagAsId_23;
	// DOTweenActionsEnums/SelectedEase HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::selectedEase
	int32_t ___selectedEase_24;
	// DG.Tweening.Ease HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::easeType
	int32_t ___easeType_25;
	// HutongGames.PlayMaker.FsmAnimationCurve HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::animationCurve
	FsmAnimationCurve_t326747561 * ___animationCurve_26;
	// System.String HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::loopsDescriptionArea
	String_t* ___loopsDescriptionArea_27;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::loops
	FsmInt_t1273009179 * ___loops_28;
	// DG.Tweening.LoopType HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::loopType
	int32_t ___loopType_29;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::autoKillOnCompletion
	FsmBool_t664485696 * ___autoKillOnCompletion_30;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::recyclable
	FsmBool_t664485696 * ___recyclable_31;
	// DG.Tweening.UpdateType HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::updateType
	int32_t ___updateType_32;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::isIndependentUpdate
	FsmBool_t664485696 * ___isIndependentUpdate_33;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::debugThis
	FsmBool_t664485696 * ___debugThis_34;
	// DG.Tweening.Tweener HutongGames.PlayMaker.Actions.DOTweenTrailRendererResize::tweener
	Tweener_t760404022 * ___tweener_35;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_toStartWidth_12() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___toStartWidth_12)); }
	inline FsmFloat_t937133978 * get_toStartWidth_12() const { return ___toStartWidth_12; }
	inline FsmFloat_t937133978 ** get_address_of_toStartWidth_12() { return &___toStartWidth_12; }
	inline void set_toStartWidth_12(FsmFloat_t937133978 * value)
	{
		___toStartWidth_12 = value;
		Il2CppCodeGenWriteBarrier(&___toStartWidth_12, value);
	}

	inline static int32_t get_offset_of_toEndWidth_13() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___toEndWidth_13)); }
	inline FsmFloat_t937133978 * get_toEndWidth_13() const { return ___toEndWidth_13; }
	inline FsmFloat_t937133978 ** get_address_of_toEndWidth_13() { return &___toEndWidth_13; }
	inline void set_toEndWidth_13(FsmFloat_t937133978 * value)
	{
		___toEndWidth_13 = value;
		Il2CppCodeGenWriteBarrier(&___toEndWidth_13, value);
	}

	inline static int32_t get_offset_of_duration_14() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___duration_14)); }
	inline FsmFloat_t937133978 * get_duration_14() const { return ___duration_14; }
	inline FsmFloat_t937133978 ** get_address_of_duration_14() { return &___duration_14; }
	inline void set_duration_14(FsmFloat_t937133978 * value)
	{
		___duration_14 = value;
		Il2CppCodeGenWriteBarrier(&___duration_14, value);
	}

	inline static int32_t get_offset_of_setSpeedBased_15() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___setSpeedBased_15)); }
	inline FsmBool_t664485696 * get_setSpeedBased_15() const { return ___setSpeedBased_15; }
	inline FsmBool_t664485696 ** get_address_of_setSpeedBased_15() { return &___setSpeedBased_15; }
	inline void set_setSpeedBased_15(FsmBool_t664485696 * value)
	{
		___setSpeedBased_15 = value;
		Il2CppCodeGenWriteBarrier(&___setSpeedBased_15, value);
	}

	inline static int32_t get_offset_of_startDelay_16() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___startDelay_16)); }
	inline FsmFloat_t937133978 * get_startDelay_16() const { return ___startDelay_16; }
	inline FsmFloat_t937133978 ** get_address_of_startDelay_16() { return &___startDelay_16; }
	inline void set_startDelay_16(FsmFloat_t937133978 * value)
	{
		___startDelay_16 = value;
		Il2CppCodeGenWriteBarrier(&___startDelay_16, value);
	}

	inline static int32_t get_offset_of_startEvent_17() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___startEvent_17)); }
	inline FsmEvent_t1258573736 * get_startEvent_17() const { return ___startEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_startEvent_17() { return &___startEvent_17; }
	inline void set_startEvent_17(FsmEvent_t1258573736 * value)
	{
		___startEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___startEvent_17, value);
	}

	inline static int32_t get_offset_of_finishEvent_18() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___finishEvent_18)); }
	inline FsmEvent_t1258573736 * get_finishEvent_18() const { return ___finishEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_finishEvent_18() { return &___finishEvent_18; }
	inline void set_finishEvent_18(FsmEvent_t1258573736 * value)
	{
		___finishEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___finishEvent_18, value);
	}

	inline static int32_t get_offset_of_finishImmediately_19() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___finishImmediately_19)); }
	inline FsmBool_t664485696 * get_finishImmediately_19() const { return ___finishImmediately_19; }
	inline FsmBool_t664485696 ** get_address_of_finishImmediately_19() { return &___finishImmediately_19; }
	inline void set_finishImmediately_19(FsmBool_t664485696 * value)
	{
		___finishImmediately_19 = value;
		Il2CppCodeGenWriteBarrier(&___finishImmediately_19, value);
	}

	inline static int32_t get_offset_of_tweenIdDescription_20() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___tweenIdDescription_20)); }
	inline String_t* get_tweenIdDescription_20() const { return ___tweenIdDescription_20; }
	inline String_t** get_address_of_tweenIdDescription_20() { return &___tweenIdDescription_20; }
	inline void set_tweenIdDescription_20(String_t* value)
	{
		___tweenIdDescription_20 = value;
		Il2CppCodeGenWriteBarrier(&___tweenIdDescription_20, value);
	}

	inline static int32_t get_offset_of_tweenIdType_21() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___tweenIdType_21)); }
	inline int32_t get_tweenIdType_21() const { return ___tweenIdType_21; }
	inline int32_t* get_address_of_tweenIdType_21() { return &___tweenIdType_21; }
	inline void set_tweenIdType_21(int32_t value)
	{
		___tweenIdType_21 = value;
	}

	inline static int32_t get_offset_of_stringAsId_22() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___stringAsId_22)); }
	inline FsmString_t2414474701 * get_stringAsId_22() const { return ___stringAsId_22; }
	inline FsmString_t2414474701 ** get_address_of_stringAsId_22() { return &___stringAsId_22; }
	inline void set_stringAsId_22(FsmString_t2414474701 * value)
	{
		___stringAsId_22 = value;
		Il2CppCodeGenWriteBarrier(&___stringAsId_22, value);
	}

	inline static int32_t get_offset_of_tagAsId_23() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___tagAsId_23)); }
	inline FsmString_t2414474701 * get_tagAsId_23() const { return ___tagAsId_23; }
	inline FsmString_t2414474701 ** get_address_of_tagAsId_23() { return &___tagAsId_23; }
	inline void set_tagAsId_23(FsmString_t2414474701 * value)
	{
		___tagAsId_23 = value;
		Il2CppCodeGenWriteBarrier(&___tagAsId_23, value);
	}

	inline static int32_t get_offset_of_selectedEase_24() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___selectedEase_24)); }
	inline int32_t get_selectedEase_24() const { return ___selectedEase_24; }
	inline int32_t* get_address_of_selectedEase_24() { return &___selectedEase_24; }
	inline void set_selectedEase_24(int32_t value)
	{
		___selectedEase_24 = value;
	}

	inline static int32_t get_offset_of_easeType_25() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___easeType_25)); }
	inline int32_t get_easeType_25() const { return ___easeType_25; }
	inline int32_t* get_address_of_easeType_25() { return &___easeType_25; }
	inline void set_easeType_25(int32_t value)
	{
		___easeType_25 = value;
	}

	inline static int32_t get_offset_of_animationCurve_26() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___animationCurve_26)); }
	inline FsmAnimationCurve_t326747561 * get_animationCurve_26() const { return ___animationCurve_26; }
	inline FsmAnimationCurve_t326747561 ** get_address_of_animationCurve_26() { return &___animationCurve_26; }
	inline void set_animationCurve_26(FsmAnimationCurve_t326747561 * value)
	{
		___animationCurve_26 = value;
		Il2CppCodeGenWriteBarrier(&___animationCurve_26, value);
	}

	inline static int32_t get_offset_of_loopsDescriptionArea_27() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___loopsDescriptionArea_27)); }
	inline String_t* get_loopsDescriptionArea_27() const { return ___loopsDescriptionArea_27; }
	inline String_t** get_address_of_loopsDescriptionArea_27() { return &___loopsDescriptionArea_27; }
	inline void set_loopsDescriptionArea_27(String_t* value)
	{
		___loopsDescriptionArea_27 = value;
		Il2CppCodeGenWriteBarrier(&___loopsDescriptionArea_27, value);
	}

	inline static int32_t get_offset_of_loops_28() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___loops_28)); }
	inline FsmInt_t1273009179 * get_loops_28() const { return ___loops_28; }
	inline FsmInt_t1273009179 ** get_address_of_loops_28() { return &___loops_28; }
	inline void set_loops_28(FsmInt_t1273009179 * value)
	{
		___loops_28 = value;
		Il2CppCodeGenWriteBarrier(&___loops_28, value);
	}

	inline static int32_t get_offset_of_loopType_29() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___loopType_29)); }
	inline int32_t get_loopType_29() const { return ___loopType_29; }
	inline int32_t* get_address_of_loopType_29() { return &___loopType_29; }
	inline void set_loopType_29(int32_t value)
	{
		___loopType_29 = value;
	}

	inline static int32_t get_offset_of_autoKillOnCompletion_30() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___autoKillOnCompletion_30)); }
	inline FsmBool_t664485696 * get_autoKillOnCompletion_30() const { return ___autoKillOnCompletion_30; }
	inline FsmBool_t664485696 ** get_address_of_autoKillOnCompletion_30() { return &___autoKillOnCompletion_30; }
	inline void set_autoKillOnCompletion_30(FsmBool_t664485696 * value)
	{
		___autoKillOnCompletion_30 = value;
		Il2CppCodeGenWriteBarrier(&___autoKillOnCompletion_30, value);
	}

	inline static int32_t get_offset_of_recyclable_31() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___recyclable_31)); }
	inline FsmBool_t664485696 * get_recyclable_31() const { return ___recyclable_31; }
	inline FsmBool_t664485696 ** get_address_of_recyclable_31() { return &___recyclable_31; }
	inline void set_recyclable_31(FsmBool_t664485696 * value)
	{
		___recyclable_31 = value;
		Il2CppCodeGenWriteBarrier(&___recyclable_31, value);
	}

	inline static int32_t get_offset_of_updateType_32() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___updateType_32)); }
	inline int32_t get_updateType_32() const { return ___updateType_32; }
	inline int32_t* get_address_of_updateType_32() { return &___updateType_32; }
	inline void set_updateType_32(int32_t value)
	{
		___updateType_32 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_33() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___isIndependentUpdate_33)); }
	inline FsmBool_t664485696 * get_isIndependentUpdate_33() const { return ___isIndependentUpdate_33; }
	inline FsmBool_t664485696 ** get_address_of_isIndependentUpdate_33() { return &___isIndependentUpdate_33; }
	inline void set_isIndependentUpdate_33(FsmBool_t664485696 * value)
	{
		___isIndependentUpdate_33 = value;
		Il2CppCodeGenWriteBarrier(&___isIndependentUpdate_33, value);
	}

	inline static int32_t get_offset_of_debugThis_34() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___debugThis_34)); }
	inline FsmBool_t664485696 * get_debugThis_34() const { return ___debugThis_34; }
	inline FsmBool_t664485696 ** get_address_of_debugThis_34() { return &___debugThis_34; }
	inline void set_debugThis_34(FsmBool_t664485696 * value)
	{
		___debugThis_34 = value;
		Il2CppCodeGenWriteBarrier(&___debugThis_34, value);
	}

	inline static int32_t get_offset_of_tweener_35() { return static_cast<int32_t>(offsetof(DOTweenTrailRendererResize_t3030774513, ___tweener_35)); }
	inline Tweener_t760404022 * get_tweener_35() const { return ___tweener_35; }
	inline Tweener_t760404022 ** get_address_of_tweener_35() { return &___tweener_35; }
	inline void set_tweener_35(Tweener_t760404022 * value)
	{
		___tweener_35 = value;
		Il2CppCodeGenWriteBarrier(&___tweener_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
