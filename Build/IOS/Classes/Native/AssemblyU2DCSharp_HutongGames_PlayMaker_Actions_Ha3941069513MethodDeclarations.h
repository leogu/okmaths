﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HasComponent
struct HasComponent_t3941069513;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.HasComponent::.ctor()
extern "C"  void HasComponent__ctor_m3882787423 (HasComponent_t3941069513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::Reset()
extern "C"  void HasComponent_Reset_m3084422798 (HasComponent_t3941069513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnEnter()
extern "C"  void HasComponent_OnEnter_m3785170304 (HasComponent_t3941069513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnUpdate()
extern "C"  void HasComponent_OnUpdate_m937526615 (HasComponent_t3941069513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::OnExit()
extern "C"  void HasComponent_OnExit_m365278840 (HasComponent_t3941069513 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HasComponent::DoHasComponent(UnityEngine.GameObject)
extern "C"  void HasComponent_DoHasComponent_m2610457005 (HasComponent_t3941069513 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
