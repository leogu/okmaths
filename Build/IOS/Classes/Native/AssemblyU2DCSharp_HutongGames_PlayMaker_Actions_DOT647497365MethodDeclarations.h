﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraPixelRect
struct DOTweenCameraPixelRect_t647497365;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraPixelRect::.ctor()
extern "C"  void DOTweenCameraPixelRect__ctor_m941401941 (DOTweenCameraPixelRect_t647497365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraPixelRect::Reset()
extern "C"  void DOTweenCameraPixelRect_Reset_m2016465446 (DOTweenCameraPixelRect_t647497365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraPixelRect::OnEnter()
extern "C"  void DOTweenCameraPixelRect_OnEnter_m1088843108 (DOTweenCameraPixelRect_t647497365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraPixelRect::<OnEnter>m__2E()
extern "C"  void DOTweenCameraPixelRect_U3COnEnterU3Em__2E_m815410030 (DOTweenCameraPixelRect_t647497365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraPixelRect::<OnEnter>m__2F()
extern "C"  void DOTweenCameraPixelRect_U3COnEnterU3Em__2F_m3846821363 (DOTweenCameraPixelRect_t647497365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
