﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetQuaternionFromRotation
struct GetQuaternionFromRotation_t1580566898;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::.ctor()
extern "C"  void GetQuaternionFromRotation__ctor_m509369412 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::Reset()
extern "C"  void GetQuaternionFromRotation_Reset_m2266539407 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnEnter()
extern "C"  void GetQuaternionFromRotation_OnEnter_m1581120871 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnUpdate()
extern "C"  void GetQuaternionFromRotation_OnUpdate_m200838090 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnLateUpdate()
extern "C"  void GetQuaternionFromRotation_OnLateUpdate_m2727692142 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::OnFixedUpdate()
extern "C"  void GetQuaternionFromRotation_OnFixedUpdate_m1963656304 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetQuaternionFromRotation::DoQuatFromRotation()
extern "C"  void GetQuaternionFromRotation_DoQuatFromRotation_m1540469834 (GetQuaternionFromRotation_t1580566898 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
