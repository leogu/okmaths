﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAudioVolume
struct SetAudioVolume_t3912171830;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::.ctor()
extern "C"  void SetAudioVolume__ctor_m1058494268 (SetAudioVolume_t3912171830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::Reset()
extern "C"  void SetAudioVolume_Reset_m259554683 (SetAudioVolume_t3912171830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::OnEnter()
extern "C"  void SetAudioVolume_OnEnter_m3508805619 (SetAudioVolume_t3912171830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::OnUpdate()
extern "C"  void SetAudioVolume_OnUpdate_m188704106 (SetAudioVolume_t3912171830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioVolume::DoSetAudioVolume()
extern "C"  void SetAudioVolume_DoSetAudioVolume_m2414347873 (SetAudioVolume_t3912171830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
