﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle
struct RectTransformScreenPointToLocalPointInRectangle_t3742213830;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::.ctor()
extern "C"  void RectTransformScreenPointToLocalPointInRectangle__ctor_m1212649264 (RectTransformScreenPointToLocalPointInRectangle_t3742213830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::Reset()
extern "C"  void RectTransformScreenPointToLocalPointInRectangle_Reset_m1264251811 (RectTransformScreenPointToLocalPointInRectangle_t3742213830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::OnEnter()
extern "C"  void RectTransformScreenPointToLocalPointInRectangle_OnEnter_m1926714107 (RectTransformScreenPointToLocalPointInRectangle_t3742213830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::OnUpdate()
extern "C"  void RectTransformScreenPointToLocalPointInRectangle_OnUpdate_m2913558838 (RectTransformScreenPointToLocalPointInRectangle_t3742213830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformScreenPointToLocalPointInRectangle::DoCheck()
extern "C"  void RectTransformScreenPointToLocalPointInRectangle_DoCheck_m1576315203 (RectTransformScreenPointToLocalPointInRectangle_t3742213830 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
