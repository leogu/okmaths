﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// System.Single HutongGames.PlayMaker.FsmFloat::get_Value()
extern "C"  float FsmFloat_get_Value_m1818441449 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::set_Value(System.Single)
extern "C"  void FsmFloat_set_Value_m3447553958 (FsmFloat_t937133978 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmFloat::get_RawValue()
extern "C"  Il2CppObject * FsmFloat_get_RawValue_m1578331870 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::set_RawValue(System.Object)
extern "C"  void FsmFloat_set_RawValue_m4275217561 (FsmFloat_t937133978 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::SafeAssign(System.Object)
extern "C"  void FsmFloat_SafeAssign_m2056285959 (FsmFloat_t937133978 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::.ctor()
extern "C"  void FsmFloat__ctor_m2608255745 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::.ctor(System.String)
extern "C"  void FsmFloat__ctor_m2365674699 (FsmFloat_t937133978 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::.ctor(HutongGames.PlayMaker.FsmFloat)
extern "C"  void FsmFloat__ctor_m2180534299 (FsmFloat_t937133978 * __this, FsmFloat_t937133978 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmFloat::Clone()
extern "C"  NamedVariable_t3026441313 * FsmFloat_Clone_m2659904798 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmFloat::get_VariableType()
extern "C"  int32_t FsmFloat_get_VariableType_m691727653 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmFloat::ToString()
extern "C"  String_t* FsmFloat_ToString_m552727030 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.FsmFloat::op_Implicit(System.Single)
extern "C"  FsmFloat_t937133978 * FsmFloat_op_Implicit_m475928236 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
