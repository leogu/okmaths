﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyComponent
struct DestroyComponent_t1081744515;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::.ctor()
extern "C"  void DestroyComponent__ctor_m3282823731 (DestroyComponent_t1081744515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::Reset()
extern "C"  void DestroyComponent_Reset_m1558696788 (DestroyComponent_t1081744515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::OnEnter()
extern "C"  void DestroyComponent_OnEnter_m2852022290 (DestroyComponent_t1081744515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyComponent::DoDestroyComponent(UnityEngine.GameObject)
extern "C"  void DestroyComponent_DoDestroyComponent_m4213702701 (DestroyComponent_t1081744515 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
