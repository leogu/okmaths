﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ResetInputAxes
struct ResetInputAxes_t1955623714;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ResetInputAxes::.ctor()
extern "C"  void ResetInputAxes__ctor_m3331000628 (ResetInputAxes_t1955623714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ResetInputAxes::Reset()
extern "C"  void ResetInputAxes_Reset_m997294419 (ResetInputAxes_t1955623714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ResetInputAxes::OnEnter()
extern "C"  void ResetInputAxes_OnEnter_m1102490147 (ResetInputAxes_t1955623714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
