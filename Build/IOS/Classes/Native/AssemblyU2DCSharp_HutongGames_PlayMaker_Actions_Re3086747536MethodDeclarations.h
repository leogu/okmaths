﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetPivot
struct RectTransformSetPivot_t3086747536;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetPivot::.ctor()
extern "C"  void RectTransformSetPivot__ctor_m674585476 (RectTransformSetPivot_t3086747536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetPivot::Reset()
extern "C"  void RectTransformSetPivot_Reset_m3827859633 (RectTransformSetPivot_t3086747536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetPivot::OnEnter()
extern "C"  void RectTransformSetPivot_OnEnter_m3844564689 (RectTransformSetPivot_t3086747536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetPivot::OnActionUpdate()
extern "C"  void RectTransformSetPivot_OnActionUpdate_m1140536816 (RectTransformSetPivot_t3086747536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetPivot::DoSetPivotPosition()
extern "C"  void RectTransformSetPivot_DoSetPivotPosition_m2044335420 (RectTransformSetPivot_t3086747536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
