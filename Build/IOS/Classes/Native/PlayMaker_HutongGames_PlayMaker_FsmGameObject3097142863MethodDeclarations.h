﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmGameObject3097142863.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.GameObject HutongGames.PlayMaker.FsmGameObject::get_Value()
extern "C"  GameObject_t1756533147 * FsmGameObject_get_Value_m15585107 (FsmGameObject_t3097142863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::set_Value(UnityEngine.GameObject)
extern "C"  void FsmGameObject_set_Value_m1923169006 (FsmGameObject_t3097142863 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.FsmGameObject::get_RawValue()
extern "C"  Il2CppObject * FsmGameObject_get_RawValue_m606997917 (FsmGameObject_t3097142863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::set_RawValue(System.Object)
extern "C"  void FsmGameObject_set_RawValue_m445551982 (FsmGameObject_t3097142863 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::SafeAssign(System.Object)
extern "C"  void FsmGameObject_SafeAssign_m1077802836 (FsmGameObject_t3097142863 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::.ctor()
extern "C"  void FsmGameObject__ctor_m3432344422 (FsmGameObject_t3097142863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::.ctor(System.String)
extern "C"  void FsmGameObject__ctor_m765442988 (FsmGameObject_t3097142863 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmGameObject::.ctor(HutongGames.PlayMaker.FsmGameObject)
extern "C"  void FsmGameObject__ctor_m1739481855 (FsmGameObject_t3097142863 * __this, FsmGameObject_t3097142863 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmGameObject::Clone()
extern "C"  NamedVariable_t3026441313 * FsmGameObject_Clone_m2117745265 (FsmGameObject_t3097142863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.FsmGameObject::get_VariableType()
extern "C"  int32_t FsmGameObject_get_VariableType_m391875282 (FsmGameObject_t3097142863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmGameObject::ToString()
extern "C"  String_t* FsmGameObject_ToString_m3431141253 (FsmGameObject_t3097142863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.FsmGameObject::op_Implicit(UnityEngine.GameObject)
extern "C"  FsmGameObject_t3097142863 * FsmGameObject_op_Implicit_m2611271695 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
