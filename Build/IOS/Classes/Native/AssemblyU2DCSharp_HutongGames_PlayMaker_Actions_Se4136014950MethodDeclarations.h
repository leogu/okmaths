﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMainCamera
struct SetMainCamera_t4136014950;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::.ctor()
extern "C"  void SetMainCamera__ctor_m3213215576 (SetMainCamera_t4136014950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::Reset()
extern "C"  void SetMainCamera_Reset_m3634324699 (SetMainCamera_t4136014950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMainCamera::OnEnter()
extern "C"  void SetMainCamera_OnEnter_m3952179851 (SetMainCamera_t4136014950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
