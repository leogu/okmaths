﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition605142169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiTransitionSetType
struct  uGuiTransitionSetType_t1899906747  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiTransitionSetType::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.uGuiTransitionSetType::transition
	int32_t ___transition_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiTransitionSetType::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiTransitionSetType::_selectable
	Selectable_t1490392188 * ____selectable_14;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.uGuiTransitionSetType::_originalTransition
	int32_t ____originalTransition_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiTransitionSetType_t1899906747, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_transition_12() { return static_cast<int32_t>(offsetof(uGuiTransitionSetType_t1899906747, ___transition_12)); }
	inline int32_t get_transition_12() const { return ___transition_12; }
	inline int32_t* get_address_of_transition_12() { return &___transition_12; }
	inline void set_transition_12(int32_t value)
	{
		___transition_12 = value;
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiTransitionSetType_t1899906747, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of__selectable_14() { return static_cast<int32_t>(offsetof(uGuiTransitionSetType_t1899906747, ____selectable_14)); }
	inline Selectable_t1490392188 * get__selectable_14() const { return ____selectable_14; }
	inline Selectable_t1490392188 ** get_address_of__selectable_14() { return &____selectable_14; }
	inline void set__selectable_14(Selectable_t1490392188 * value)
	{
		____selectable_14 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_14, value);
	}

	inline static int32_t get_offset_of__originalTransition_15() { return static_cast<int32_t>(offsetof(uGuiTransitionSetType_t1899906747, ____originalTransition_15)); }
	inline int32_t get__originalTransition_15() const { return ____originalTransition_15; }
	inline int32_t* get_address_of__originalTransition_15() { return &____originalTransition_15; }
	inline void set__originalTransition_15(int32_t value)
	{
		____originalTransition_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
