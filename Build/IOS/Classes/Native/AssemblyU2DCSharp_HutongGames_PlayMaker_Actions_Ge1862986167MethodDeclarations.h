﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmState
struct GetFsmState_t1862986167;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmState::.ctor()
extern "C"  void GetFsmState__ctor_m464028243 (GetFsmState_t1862986167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::Reset()
extern "C"  void GetFsmState_Reset_m3713156784 (GetFsmState_t1862986167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::OnEnter()
extern "C"  void GetFsmState_OnEnter_m4095886614 (GetFsmState_t1862986167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::OnUpdate()
extern "C"  void GetFsmState_OnUpdate_m559337739 (GetFsmState_t1862986167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmState::DoGetFsmState()
extern "C"  void GetFsmState_DoGetFsmState_m3087698701 (GetFsmState_t1862986167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
