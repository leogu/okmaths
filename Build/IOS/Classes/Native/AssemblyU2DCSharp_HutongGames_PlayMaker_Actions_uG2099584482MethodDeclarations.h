﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldOnEndEditEvent
struct uGuiInputFieldOnEndEditEvent_t2099584482;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnEndEditEvent::.ctor()
extern "C"  void uGuiInputFieldOnEndEditEvent__ctor_m172896412 (uGuiInputFieldOnEndEditEvent_t2099584482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnEndEditEvent::Reset()
extern "C"  void uGuiInputFieldOnEndEditEvent_Reset_m1613860139 (uGuiInputFieldOnEndEditEvent_t2099584482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnEndEditEvent::OnEnter()
extern "C"  void uGuiInputFieldOnEndEditEvent_OnEnter_m3480923747 (uGuiInputFieldOnEndEditEvent_t2099584482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnEndEditEvent::OnExit()
extern "C"  void uGuiInputFieldOnEndEditEvent_OnExit_m698331259 (uGuiInputFieldOnEndEditEvent_t2099584482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnEndEditEvent::DoOnEndEdit(System.String)
extern "C"  void uGuiInputFieldOnEndEditEvent_DoOnEndEdit_m2090172677 (uGuiInputFieldOnEndEditEvent_t2099584482 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
