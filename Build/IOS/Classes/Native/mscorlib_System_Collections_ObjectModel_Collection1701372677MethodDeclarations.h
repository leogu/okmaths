﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>
struct Collection_1_t1701372677;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// HutongGames.PlayMaker.ParamDataType[]
struct ParamDataTypeU5BU5D_t835534274;
// System.Collections.Generic.IEnumerator`1<HutongGames.PlayMaker.ParamDataType>
struct IEnumerator_1_t3930119046;
// System.Collections.Generic.IList`1<HutongGames.PlayMaker.ParamDataType>
struct IList_1_t2700568524;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "PlayMaker_HutongGames_PlayMaker_ParamDataType2159627923.h"

// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::.ctor()
extern "C"  void Collection_1__ctor_m989616036_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1__ctor_m989616036(__this, method) ((  void (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1__ctor_m989616036_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3225583773_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3225583773(__this, method) ((  bool (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3225583773_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m208054660_gshared (Collection_1_t1701372677 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m208054660(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1701372677 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m208054660_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m528873921_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m528873921(__this, method) ((  Il2CppObject * (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m528873921_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m689764356_gshared (Collection_1_t1701372677 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m689764356(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1701372677 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m689764356_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m960029946_gshared (Collection_1_t1701372677 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m960029946(__this, ___value0, method) ((  bool (*) (Collection_1_t1701372677 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m960029946_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m163788950_gshared (Collection_1_t1701372677 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m163788950(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1701372677 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m163788950_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2779270097_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2779270097(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2779270097_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4103044793_gshared (Collection_1_t1701372677 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4103044793(__this, ___value0, method) ((  void (*) (Collection_1_t1701372677 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4103044793_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1934922844_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1934922844(__this, method) ((  bool (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1934922844_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2633431844_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2633431844(__this, method) ((  Il2CppObject * (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2633431844_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m303259405_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m303259405(__this, method) ((  bool (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m303259405_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m517281048_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m517281048(__this, method) ((  bool (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m517281048_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2795564397_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2795564397(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2795564397_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m668020954_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m668020954(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m668020954_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Add(T)
extern "C"  void Collection_1_Add_m1714614453_gshared (Collection_1_t1701372677 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m1714614453(__this, ___item0, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_Add_m1714614453_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Clear()
extern "C"  void Collection_1_Clear_m478930305_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_Clear_m478930305(__this, method) ((  void (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_Clear_m478930305_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::ClearItems()
extern "C"  void Collection_1_ClearItems_m175481491_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m175481491(__this, method) ((  void (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_ClearItems_m175481491_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Contains(T)
extern "C"  bool Collection_1_Contains_m2003923359_gshared (Collection_1_t1701372677 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2003923359(__this, ___item0, method) ((  bool (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_Contains_m2003923359_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3161919909_gshared (Collection_1_t1701372677 * __this, ParamDataTypeU5BU5D_t835534274* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3161919909(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1701372677 *, ParamDataTypeU5BU5D_t835534274*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3161919909_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3426222048_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3426222048(__this, method) ((  Il2CppObject* (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_GetEnumerator_m3426222048_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m328798009_gshared (Collection_1_t1701372677 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m328798009(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m328798009_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4043983786_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4043983786(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m4043983786_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2629477679_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2629477679(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2629477679_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::Remove(T)
extern "C"  bool Collection_1_Remove_m2266832900_gshared (Collection_1_t1701372677 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2266832900(__this, ___item0, method) ((  bool (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_Remove_m2266832900_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3415025414_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3415025414(__this, ___index0, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3415025414_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2828908236_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2828908236(__this, ___index0, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2828908236_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2172422948_gshared (Collection_1_t1701372677 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2172422948(__this, method) ((  int32_t (*) (Collection_1_t1701372677 *, const MethodInfo*))Collection_1_get_Count_m2172422948_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m3648937816_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3648937816(__this, ___index0, method) ((  int32_t (*) (Collection_1_t1701372677 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3648937816_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2215330053_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2215330053(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m2215330053_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1631638474_gshared (Collection_1_t1701372677 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1631638474(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1701372677 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1631638474_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1592666323_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1592666323(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1592666323_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m10779027_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m10779027(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m10779027_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2160123103_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2160123103(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2160123103_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1963515951_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1963515951(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1963515951_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<HutongGames.PlayMaker.ParamDataType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2056382238_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2056382238(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2056382238_gshared)(__this /* static, unused */, ___list0, method)
