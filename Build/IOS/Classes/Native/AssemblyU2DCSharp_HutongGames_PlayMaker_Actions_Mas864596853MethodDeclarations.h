﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.MasterServerGetHostData
struct MasterServerGetHostData_t864596853;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::.ctor()
extern "C"  void MasterServerGetHostData__ctor_m245138089 (MasterServerGetHostData_t864596853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::Reset()
extern "C"  void MasterServerGetHostData_Reset_m3516579118 (MasterServerGetHostData_t864596853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::OnEnter()
extern "C"  void MasterServerGetHostData_OnEnter_m3641405844 (MasterServerGetHostData_t864596853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.MasterServerGetHostData::GetHostData()
extern "C"  void MasterServerGetHostData_GetHostData_m3725758599 (MasterServerGetHostData_t864596853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
