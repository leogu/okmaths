﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.TooltipAttribute
struct TooltipAttribute_t714119257;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.String HutongGames.PlayMaker.TooltipAttribute::get_Text()
extern "C"  String_t* TooltipAttribute_get_Text_m2820944101 (TooltipAttribute_t714119257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m2090038442 (TooltipAttribute_t714119257 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
