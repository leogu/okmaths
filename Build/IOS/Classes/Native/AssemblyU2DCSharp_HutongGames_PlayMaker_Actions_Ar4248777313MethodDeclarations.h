﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayLength
struct ArrayLength_t4248777313;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayLength::.ctor()
extern "C"  void ArrayLength__ctor_m506214515 (ArrayLength_t4248777313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::Reset()
extern "C"  void ArrayLength_Reset_m548099070 (ArrayLength_t4248777313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::OnEnter()
extern "C"  void ArrayLength_OnEnter_m3867378104 (ArrayLength_t4248777313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayLength::OnUpdate()
extern "C"  void ArrayLength_OnUpdate_m3807805787 (ArrayLength_t4248777313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
