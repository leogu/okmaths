﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListConcat
struct ArrayListConcat_t163014501;
// System.Collections.ArrayList
struct ArrayList_t4252133567;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ArrayList4252133567.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListConcat::.ctor()
extern "C"  void ArrayListConcat__ctor_m425884697 (ArrayListConcat_t163014501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListConcat::Reset()
extern "C"  void ArrayListConcat_Reset_m1141721630 (ArrayListConcat_t163014501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListConcat::OnEnter()
extern "C"  void ArrayListConcat_OnEnter_m2814805460 (ArrayListConcat_t163014501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListConcat::DoArrayListConcat(System.Collections.ArrayList)
extern "C"  void ArrayListConcat_DoArrayListConcat_m3064456404 (ArrayListConcat_t163014501 * __this, ArrayList_t4252133567 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
