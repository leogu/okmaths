﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMeshAgent
struct NavMeshAgent_t2171372499;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void UnityEngine.NavMeshAgent::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshAgent_set_velocity_m2966493589 (NavMeshAgent_t2171372499 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshAgent_INTERNAL_set_velocity_m987960335 (NavMeshAgent_t2171372499 * __this, Vector3_t2243707580 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
