﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.Collision
struct Collision_t2876846408;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// UnityEngine.Joint2D
struct Joint2D_t854621618;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Joint2D854621618.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"

// System.Void HutongGames.PlayMaker.FsmStateAction::Init(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmStateAction_Init_m2354445987 (FsmStateAction_t2862378169 * __this, FsmState_t1643911659 * ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Reset()
extern "C"  void FsmStateAction_Reset_m112890061 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnPreprocess()
extern "C"  void FsmStateAction_OnPreprocess_m2483162357 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Awake()
extern "C"  void FsmStateAction_Awake_m2464439313 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmStateAction_Event_m3458270206 (FsmStateAction_t2862378169 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Finish()
extern "C"  void FsmStateAction_Finish_m1955744727 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine HutongGames.PlayMaker.FsmStateAction::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * FsmStateAction_StartCoroutine_m1952285837 (FsmStateAction_t2862378169 * __this, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void FsmStateAction_StopCoroutine_m3744441267 (FsmStateAction_t2862378169 * __this, Coroutine_t2299508840 * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnEnter()
extern "C"  void FsmStateAction_OnEnter_m154996341 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnFixedUpdate()
extern "C"  void FsmStateAction_OnFixedUpdate_m95172284 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnUpdate()
extern "C"  void FsmStateAction_OnUpdate_m1589883426 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI()
extern "C"  void FsmStateAction_OnGUI_m3563772728 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnLateUpdate()
extern "C"  void FsmStateAction_OnLateUpdate_m4192026966 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnExit()
extern "C"  void FsmStateAction_OnExit_m1086895057 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnDrawActionGizmos()
extern "C"  void FsmStateAction_OnDrawActionGizmos_m1255816880 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnDrawActionGizmosSelected()
extern "C"  void FsmStateAction_OnDrawActionGizmosSelected_m3988466711 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmStateAction::AutoName()
extern "C"  String_t* FsmStateAction_AutoName_m3187188241 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::OnActionTargetInvoked(System.Object)
extern "C"  void FsmStateAction_OnActionTargetInvoked_m4263344590 (FsmStateAction_t2862378169 * __this, Il2CppObject * ___targetObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionEnter(UnityEngine.Collision)
extern "C"  void FsmStateAction_DoCollisionEnter_m3706254234 (FsmStateAction_t2862378169 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionStay(UnityEngine.Collision)
extern "C"  void FsmStateAction_DoCollisionStay_m2286948771 (FsmStateAction_t2862378169 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionExit(UnityEngine.Collision)
extern "C"  void FsmStateAction_DoCollisionExit_m601082136 (FsmStateAction_t2862378169 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerEnter(UnityEngine.Collider)
extern "C"  void FsmStateAction_DoTriggerEnter_m2059473400 (FsmStateAction_t2862378169 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerStay(UnityEngine.Collider)
extern "C"  void FsmStateAction_DoTriggerStay_m1871731229 (FsmStateAction_t2862378169 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerExit(UnityEngine.Collider)
extern "C"  void FsmStateAction_DoTriggerExit_m2724032282 (FsmStateAction_t2862378169 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoParticleCollision(UnityEngine.GameObject)
extern "C"  void FsmStateAction_DoParticleCollision_m3627206445 (FsmStateAction_t2862378169 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void FsmStateAction_DoCollisionEnter2D_m2049045370 (FsmStateAction_t2862378169 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void FsmStateAction_DoCollisionStay2D_m216822787 (FsmStateAction_t2862378169 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void FsmStateAction_DoCollisionExit2D_m3728525560 (FsmStateAction_t2862378169 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void FsmStateAction_DoTriggerEnter2D_m2738331720 (FsmStateAction_t2862378169 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void FsmStateAction_DoTriggerStay2D_m2573999513 (FsmStateAction_t2862378169 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void FsmStateAction_DoTriggerExit2D_m2710389578 (FsmStateAction_t2862378169 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void FsmStateAction_DoControllerColliderHit_m1094635016 (FsmStateAction_t2862378169 * __this, ControllerColliderHit_t4070855101 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoJointBreak(System.Single)
extern "C"  void FsmStateAction_DoJointBreak_m2970484123 (FsmStateAction_t2862378169 * __this, float ___force0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoJointBreak2D(UnityEngine.Joint2D)
extern "C"  void FsmStateAction_DoJointBreak2D_m2418629251 (FsmStateAction_t2862378169 * __this, Joint2D_t854621618 * ___joint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoAnimatorMove()
extern "C"  void FsmStateAction_DoAnimatorMove_m60845405 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::DoAnimatorIK(System.Int32)
extern "C"  void FsmStateAction_DoAnimatorIK_m4063812419 (FsmStateAction_t2862378169 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::Log(System.String)
extern "C"  void FsmStateAction_Log_m3131551472 (FsmStateAction_t2862378169 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::LogWarning(System.String)
extern "C"  void FsmStateAction_LogWarning_m1851739038 (FsmStateAction_t2862378169 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::LogError(System.String)
extern "C"  void FsmStateAction_LogError_m3656178960 (FsmStateAction_t2862378169 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmStateAction::ErrorCheck()
extern "C"  String_t* FsmStateAction_ErrorCheck_m1756846267 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmStateAction::get_Name()
extern "C"  String_t* FsmStateAction_get_Name_m1525479315 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Name(System.String)
extern "C"  void FsmStateAction_set_Name_m216974750 (FsmStateAction_t2862378169 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm HutongGames.PlayMaker.FsmStateAction::get_Fsm()
extern "C"  Fsm_t917886356 * FsmStateAction_get_Fsm_m4079598046 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Fsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmStateAction_set_Fsm_m1280837079 (FsmStateAction_t2862378169 * __this, Fsm_t917886356 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.FsmStateAction::get_Owner()
extern "C"  GameObject_t1756533147 * FsmStateAction_get_Owner_m2345900259 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Owner(UnityEngine.GameObject)
extern "C"  void FsmStateAction_set_Owner_m1677737522 (FsmStateAction_t2862378169 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.FsmStateAction::get_State()
extern "C"  FsmState_t1643911659 * FsmStateAction_get_State_m267961006 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_State(HutongGames.PlayMaker.FsmState)
extern "C"  void FsmStateAction_set_State_m139635987 (FsmStateAction_t2862378169 * __this, FsmState_t1643911659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Enabled()
extern "C"  bool FsmStateAction_get_Enabled_m3252509844 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Enabled(System.Boolean)
extern "C"  void FsmStateAction_set_Enabled_m1236839907 (FsmStateAction_t2862378169 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_IsOpen()
extern "C"  bool FsmStateAction_get_IsOpen_m3101215847 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_IsOpen(System.Boolean)
extern "C"  void FsmStateAction_set_IsOpen_m2863281734 (FsmStateAction_t2862378169 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_IsAutoNamed()
extern "C"  bool FsmStateAction_get_IsAutoNamed_m1593924131 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_IsAutoNamed(System.Boolean)
extern "C"  void FsmStateAction_set_IsAutoNamed_m3324030690 (FsmStateAction_t2862378169 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Entered()
extern "C"  bool FsmStateAction_get_Entered_m432977410 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Entered(System.Boolean)
extern "C"  void FsmStateAction_set_Entered_m560428099 (FsmStateAction_t2862378169 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Finished()
extern "C"  bool FsmStateAction_get_Finished_m252552503 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Finished(System.Boolean)
extern "C"  void FsmStateAction_set_Finished_m1080397364 (FsmStateAction_t2862378169 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Active()
extern "C"  bool FsmStateAction_get_Active_m2969348681 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::set_Active(System.Boolean)
extern "C"  void FsmStateAction_set_Active_m4019442972 (FsmStateAction_t2862378169 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmStateAction::.ctor()
extern "C"  void FsmStateAction__ctor_m1837021228 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
