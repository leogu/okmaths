﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkIsServer
struct NetworkIsServer_t2748619381;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::.ctor()
extern "C"  void NetworkIsServer__ctor_m1096628775 (NetworkIsServer_t2748619381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::Reset()
extern "C"  void NetworkIsServer_Reset_m1114434314 (NetworkIsServer_t2748619381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::OnEnter()
extern "C"  void NetworkIsServer_OnEnter_m553112380 (NetworkIsServer_t2748619381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkIsServer::DoCheckIsServer()
extern "C"  void NetworkIsServer_DoCheckIsServer_m451459333 (NetworkIsServer_t2748619381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
