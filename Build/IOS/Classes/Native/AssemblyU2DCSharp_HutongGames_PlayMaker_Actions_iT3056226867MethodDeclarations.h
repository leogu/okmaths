﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenShakePosition
struct iTweenShakePosition_t3056226867;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::.ctor()
extern "C"  void iTweenShakePosition__ctor_m3526859339 (iTweenShakePosition_t3056226867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::Reset()
extern "C"  void iTweenShakePosition_Reset_m3394025064 (iTweenShakePosition_t3056226867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::OnEnter()
extern "C"  void iTweenShakePosition_OnEnter_m2575624670 (iTweenShakePosition_t3056226867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::OnExit()
extern "C"  void iTweenShakePosition_OnExit_m1448778302 (iTweenShakePosition_t3056226867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakePosition::DoiTween()
extern "C"  void iTweenShakePosition_DoiTween_m1346401392 (iTweenShakePosition_t3056226867 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
