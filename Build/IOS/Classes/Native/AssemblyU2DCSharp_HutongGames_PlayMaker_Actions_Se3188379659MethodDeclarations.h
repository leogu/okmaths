﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive
struct SetAnimatorFeetPivotActive_t3188379659;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::.ctor()
extern "C"  void SetAnimatorFeetPivotActive__ctor_m2170126741 (SetAnimatorFeetPivotActive_t3188379659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::Reset()
extern "C"  void SetAnimatorFeetPivotActive_Reset_m1336011356 (SetAnimatorFeetPivotActive_t3188379659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::OnEnter()
extern "C"  void SetAnimatorFeetPivotActive_OnEnter_m3526038150 (SetAnimatorFeetPivotActive_t3188379659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFeetPivotActive::DoFeetPivotActive()
extern "C"  void SetAnimatorFeetPivotActive_DoFeetPivotActive_m3046968450 (SetAnimatorFeetPivotActive_t3188379659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
