﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.PlaySound
struct PlaySound_t469391397;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.PlaySound::.ctor()
extern "C"  void PlaySound__ctor_m2364096529 (PlaySound_t469391397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlaySound::Reset()
extern "C"  void PlaySound_Reset_m2075962630 (PlaySound_t469391397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlaySound::OnEnter()
extern "C"  void PlaySound_OnEnter_m2791118612 (PlaySound_t469391397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.PlaySound::DoPlaySound()
extern "C"  void PlaySound_DoPlaySound_m1441714225 (PlaySound_t469391397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
