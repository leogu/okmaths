﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition
struct RectTransformSetAnchoredPosition_t423179679;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::.ctor()
extern "C"  void RectTransformSetAnchoredPosition__ctor_m2982347117 (RectTransformSetAnchoredPosition_t423179679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::Reset()
extern "C"  void RectTransformSetAnchoredPosition_Reset_m1258214028 (RectTransformSetAnchoredPosition_t423179679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::OnEnter()
extern "C"  void RectTransformSetAnchoredPosition_OnEnter_m2145892054 (RectTransformSetAnchoredPosition_t423179679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::OnActionUpdate()
extern "C"  void RectTransformSetAnchoredPosition_OnActionUpdate_m73783123 (RectTransformSetAnchoredPosition_t423179679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchoredPosition::DoSetAnchoredPosition()
extern "C"  void RectTransformSetAnchoredPosition_DoSetAnchoredPosition_m264972997 (RectTransformSetAnchoredPosition_t423179679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
