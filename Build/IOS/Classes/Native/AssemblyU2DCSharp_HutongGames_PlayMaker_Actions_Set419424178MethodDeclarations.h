﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGUIDepth
struct SetGUIDepth_t419424178;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::.ctor()
extern "C"  void SetGUIDepth__ctor_m1371960598 (SetGUIDepth_t419424178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::Reset()
extern "C"  void SetGUIDepth_Reset_m1019509067 (SetGUIDepth_t419424178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::OnPreprocess()
extern "C"  void SetGUIDepth_OnPreprocess_m712994435 (SetGUIDepth_t419424178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGUIDepth::OnGUI()
extern "C"  void SetGUIDepth_OnGUI_m3675941614 (SetGUIDepth_t419424178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
