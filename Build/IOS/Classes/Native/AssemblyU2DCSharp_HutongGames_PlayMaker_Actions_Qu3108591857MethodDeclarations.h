﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionEuler
struct QuaternionEuler_t3108591857;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::.ctor()
extern "C"  void QuaternionEuler__ctor_m3197888333 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::Reset()
extern "C"  void QuaternionEuler_Reset_m3941136298 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnEnter()
extern "C"  void QuaternionEuler_OnEnter_m3993890080 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnUpdate()
extern "C"  void QuaternionEuler_OnUpdate_m2829733777 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnLateUpdate()
extern "C"  void QuaternionEuler_OnLateUpdate_m2276108989 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::OnFixedUpdate()
extern "C"  void QuaternionEuler_OnFixedUpdate_m3362574351 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionEuler::DoQuatEuler()
extern "C"  void QuaternionEuler_DoQuatEuler_m3284265890 (QuaternionEuler_t3108591857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
