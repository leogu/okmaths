﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PlayMakerArrayListProxy
struct PlayMakerArrayListProxy_t4012441185;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListCreate
struct  ArrayListCreate_t2643307797  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListCreate::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListCreate::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListCreate::removeOnExit
	FsmBool_t664485696 * ___removeOnExit_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListCreate::alreadyExistsEvent
	FsmEvent_t1258573736 * ___alreadyExistsEvent_15;
	// PlayMakerArrayListProxy HutongGames.PlayMaker.Actions.ArrayListCreate::addedComponent
	PlayMakerArrayListProxy_t4012441185 * ___addedComponent_16;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListCreate_t2643307797, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListCreate_t2643307797, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_removeOnExit_14() { return static_cast<int32_t>(offsetof(ArrayListCreate_t2643307797, ___removeOnExit_14)); }
	inline FsmBool_t664485696 * get_removeOnExit_14() const { return ___removeOnExit_14; }
	inline FsmBool_t664485696 ** get_address_of_removeOnExit_14() { return &___removeOnExit_14; }
	inline void set_removeOnExit_14(FsmBool_t664485696 * value)
	{
		___removeOnExit_14 = value;
		Il2CppCodeGenWriteBarrier(&___removeOnExit_14, value);
	}

	inline static int32_t get_offset_of_alreadyExistsEvent_15() { return static_cast<int32_t>(offsetof(ArrayListCreate_t2643307797, ___alreadyExistsEvent_15)); }
	inline FsmEvent_t1258573736 * get_alreadyExistsEvent_15() const { return ___alreadyExistsEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_alreadyExistsEvent_15() { return &___alreadyExistsEvent_15; }
	inline void set_alreadyExistsEvent_15(FsmEvent_t1258573736 * value)
	{
		___alreadyExistsEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___alreadyExistsEvent_15, value);
	}

	inline static int32_t get_offset_of_addedComponent_16() { return static_cast<int32_t>(offsetof(ArrayListCreate_t2643307797, ___addedComponent_16)); }
	inline PlayMakerArrayListProxy_t4012441185 * get_addedComponent_16() const { return ___addedComponent_16; }
	inline PlayMakerArrayListProxy_t4012441185 ** get_address_of_addedComponent_16() { return &___addedComponent_16; }
	inline void set_addedComponent_16(PlayMakerArrayListProxy_t4012441185 * value)
	{
		___addedComponent_16 = value;
		Il2CppCodeGenWriteBarrier(&___addedComponent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
