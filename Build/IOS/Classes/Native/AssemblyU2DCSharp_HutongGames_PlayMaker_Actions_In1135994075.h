﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntModulo
struct  IntModulo_t1135994075  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntModulo::dividend
	FsmInt_t1273009179 * ___dividend_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntModulo::diviser
	FsmInt_t1273009179 * ___diviser_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.IntModulo::result
	FsmFloat_t937133978 * ___result_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntModulo::resultAsInt
	FsmInt_t1273009179 * ___resultAsInt_14;
	// System.Boolean HutongGames.PlayMaker.Actions.IntModulo::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_dividend_11() { return static_cast<int32_t>(offsetof(IntModulo_t1135994075, ___dividend_11)); }
	inline FsmInt_t1273009179 * get_dividend_11() const { return ___dividend_11; }
	inline FsmInt_t1273009179 ** get_address_of_dividend_11() { return &___dividend_11; }
	inline void set_dividend_11(FsmInt_t1273009179 * value)
	{
		___dividend_11 = value;
		Il2CppCodeGenWriteBarrier(&___dividend_11, value);
	}

	inline static int32_t get_offset_of_diviser_12() { return static_cast<int32_t>(offsetof(IntModulo_t1135994075, ___diviser_12)); }
	inline FsmInt_t1273009179 * get_diviser_12() const { return ___diviser_12; }
	inline FsmInt_t1273009179 ** get_address_of_diviser_12() { return &___diviser_12; }
	inline void set_diviser_12(FsmInt_t1273009179 * value)
	{
		___diviser_12 = value;
		Il2CppCodeGenWriteBarrier(&___diviser_12, value);
	}

	inline static int32_t get_offset_of_result_13() { return static_cast<int32_t>(offsetof(IntModulo_t1135994075, ___result_13)); }
	inline FsmFloat_t937133978 * get_result_13() const { return ___result_13; }
	inline FsmFloat_t937133978 ** get_address_of_result_13() { return &___result_13; }
	inline void set_result_13(FsmFloat_t937133978 * value)
	{
		___result_13 = value;
		Il2CppCodeGenWriteBarrier(&___result_13, value);
	}

	inline static int32_t get_offset_of_resultAsInt_14() { return static_cast<int32_t>(offsetof(IntModulo_t1135994075, ___resultAsInt_14)); }
	inline FsmInt_t1273009179 * get_resultAsInt_14() const { return ___resultAsInt_14; }
	inline FsmInt_t1273009179 ** get_address_of_resultAsInt_14() { return &___resultAsInt_14; }
	inline void set_resultAsInt_14(FsmInt_t1273009179 * value)
	{
		___resultAsInt_14 = value;
		Il2CppCodeGenWriteBarrier(&___resultAsInt_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(IntModulo_t1135994075, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
