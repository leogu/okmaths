﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetRandomRotation
struct SetRandomRotation_t3063375797;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::.ctor()
extern "C"  void SetRandomRotation__ctor_m175987859 (SetRandomRotation_t3063375797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::Reset()
extern "C"  void SetRandomRotation_Reset_m774701902 (SetRandomRotation_t3063375797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::OnEnter()
extern "C"  void SetRandomRotation_OnEnter_m2530929984 (SetRandomRotation_t3063375797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetRandomRotation::DoRandomRotation()
extern "C"  void SetRandomRotation_DoRandomRotation_m1842635063 (SetRandomRotation_t3063375797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
