﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetRect
struct RectTransformGetRect_t604466628;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetRect::.ctor()
extern "C"  void RectTransformGetRect__ctor_m1605363632 (RectTransformGetRect_t604466628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetRect::Reset()
extern "C"  void RectTransformGetRect_Reset_m3053943401 (RectTransformGetRect_t604466628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetRect::OnEnter()
extern "C"  void RectTransformGetRect_OnEnter_m446183353 (RectTransformGetRect_t604466628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetRect::OnActionUpdate()
extern "C"  void RectTransformGetRect_OnActionUpdate_m4267823032 (RectTransformGetRect_t604466628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetRect::DoGetValues()
extern "C"  void RectTransformGetRect_DoGetValues_m4157243637 (RectTransformGetRect_t604466628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
