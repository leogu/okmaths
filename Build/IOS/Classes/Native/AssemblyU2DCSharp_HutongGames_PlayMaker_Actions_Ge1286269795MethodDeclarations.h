﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetLayer
struct GetLayer_t1286269795;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetLayer::.ctor()
extern "C"  void GetLayer__ctor_m3058141595 (GetLayer_t1286269795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::Reset()
extern "C"  void GetLayer_Reset_m3893918828 (GetLayer_t1286269795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::OnEnter()
extern "C"  void GetLayer_OnEnter_m3020736370 (GetLayer_t1286269795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::OnUpdate()
extern "C"  void GetLayer_OnUpdate_m3392670363 (GetLayer_t1286269795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetLayer::DoGetLayer()
extern "C"  void GetLayer_DoGetLayer_m1688412417 (GetLayer_t1286269795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
