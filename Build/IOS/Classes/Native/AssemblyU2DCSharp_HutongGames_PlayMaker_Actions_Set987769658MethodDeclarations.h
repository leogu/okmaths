﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties
struct SetWheelJoint2dProperties_t987769658;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::.ctor()
extern "C"  void SetWheelJoint2dProperties__ctor_m3095081448 (SetWheelJoint2dProperties_t987769658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::Reset()
extern "C"  void SetWheelJoint2dProperties_Reset_m3998490139 (SetWheelJoint2dProperties_t987769658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::OnEnter()
extern "C"  void SetWheelJoint2dProperties_OnEnter_m986462515 (SetWheelJoint2dProperties_t987769658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::OnUpdate()
extern "C"  void SetWheelJoint2dProperties_OnUpdate_m3711736190 (SetWheelJoint2dProperties_t987769658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetWheelJoint2dProperties::SetProperties()
extern "C"  void SetWheelJoint2dProperties_SetProperties_m2495545589 (SetWheelJoint2dProperties_t987769658 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
