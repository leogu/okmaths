﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayShuffle
struct ArrayShuffle_t2054726132;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::.ctor()
extern "C"  void ArrayShuffle__ctor_m3627956738 (ArrayShuffle_t2054726132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::Reset()
extern "C"  void ArrayShuffle_Reset_m1298846241 (ArrayShuffle_t2054726132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayShuffle::OnEnter()
extern "C"  void ArrayShuffle_OnEnter_m2463909297 (ArrayShuffle_t2054726132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
