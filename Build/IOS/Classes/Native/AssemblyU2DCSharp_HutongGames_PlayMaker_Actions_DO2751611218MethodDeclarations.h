﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformScaleZ
struct DOTweenTransformScaleZ_t2751611218;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleZ::.ctor()
extern "C"  void DOTweenTransformScaleZ__ctor_m2483679448 (DOTweenTransformScaleZ_t2751611218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleZ::Reset()
extern "C"  void DOTweenTransformScaleZ_Reset_m4172347583 (DOTweenTransformScaleZ_t2751611218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleZ::OnEnter()
extern "C"  void DOTweenTransformScaleZ_OnEnter_m1924155551 (DOTweenTransformScaleZ_t2751611218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleZ::<OnEnter>m__DC()
extern "C"  void DOTweenTransformScaleZ_U3COnEnterU3Em__DC_m1724009275 (DOTweenTransformScaleZ_t2751611218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformScaleZ::<OnEnter>m__DD()
extern "C"  void DOTweenTransformScaleZ_U3COnEnterU3Em__DD_m460453312 (DOTweenTransformScaleZ_t2751611218 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
