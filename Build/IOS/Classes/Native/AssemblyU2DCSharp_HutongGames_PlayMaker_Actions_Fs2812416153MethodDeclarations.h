﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FsmArraySet
struct FsmArraySet_t2812416153;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::.ctor()
extern "C"  void FsmArraySet__ctor_m3602317029 (FsmArraySet_t2812416153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::Reset()
extern "C"  void FsmArraySet_Reset_m4129070002 (FsmArraySet_t2812416153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::OnEnter()
extern "C"  void FsmArraySet_OnEnter_m4080279288 (FsmArraySet_t2812416153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::DoSetFsmString()
extern "C"  void FsmArraySet_DoSetFsmString_m1754378401 (FsmArraySet_t2812416153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FsmArraySet::OnUpdate()
extern "C"  void FsmArraySet_OnUpdate_m3471219657 (FsmArraySet_t2812416153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
