﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En681026308MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m82796352(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4066512907 *, Dictionary_2_t2746488205 *, const MethodInfo*))Enumerator__ctor_m3007524706_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1805338207(__this, method) ((  Il2CppObject * (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3594355539_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1290691615(__this, method) ((  void (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3390753507_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3609337028(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1608269866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2446222565(__this, method) ((  Il2CppObject * (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3989254625_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m964868885(__this, method) ((  Il2CppObject * (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m952755377_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::MoveNext()
#define Enumerator_MoveNext_m3194431675(__this, method) ((  bool (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_MoveNext_m1920508743_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_Current()
#define Enumerator_get_Current_m2045051035(__this, method) ((  KeyValuePair_2_t503833427  (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_get_Current_m2195469575_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m545288560(__this, method) ((  Fsm_t917886356 * (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_get_CurrentKey_m387894914_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m402600(__this, method) ((  RaycastHit2D_t4063908774  (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_get_CurrentValue_m3503556298_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::Reset()
#define Enumerator_Reset_m667406626(__this, method) ((  void (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_Reset_m2618179180_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::VerifyState()
#define Enumerator_VerifyState_m1780959797(__this, method) ((  void (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_VerifyState_m2175141401_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1564192975(__this, method) ((  void (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_VerifyCurrent_m4267695059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<HutongGames.PlayMaker.Fsm,UnityEngine.RaycastHit2D>::Dispose()
#define Enumerator_Dispose_m3620042340(__this, method) ((  void (*) (Enumerator_t4066512907 *, const MethodInfo*))Enumerator_Dispose_m3229374318_gshared)(__this, method)
