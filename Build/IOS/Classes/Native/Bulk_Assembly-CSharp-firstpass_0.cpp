﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// admob.Admob
struct Admob_t546240967;
// admob.Admob/AdmobEventHandler
struct AdmobEventHandler_t2983421020;
// System.String
struct String_t;
// admob.Admob/AdmobAdCallBack
struct AdmobAdCallBack_t390902866;
// admob.AdSize
struct AdSize_t3770813302;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// admob.AdmobEvent
struct AdmobEvent_t4123996035;
// admob.AdPosition
struct AdPosition_t1947228246;
// MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1970456718;
// System.Type
struct Type_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob546240967.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob546240967MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobEve2983421020.h"
#include "mscorlib_System_Delegate3022476291MethodDeclarations.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobAdCa390902866.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobAdCa390902866MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdSize3770813302.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdSize3770813302MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_Admob_AdmobEve2983421020MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdmobEvent4123996035.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdmobEvent4123996035MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdPosition1947228246.h"
#include "AssemblyU2DCSharpU2Dfirstpass_admob_AdPosition1947228246MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MonoPInvokeCallbackA1970456718.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MonoPInvokeCallbackA1970456718MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Attribute542643598MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void admob.Admob::.ctor()
extern "C"  void Admob__ctor_m3904493015 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.Admob::add_bannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_bannerEventHandler_m1557830709_MetadataUsageId;
extern "C"  void Admob_add_bannerEventHandler_m1557830709 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_bannerEventHandler_m1557830709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_bannerEventHandler_1();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_bannerEventHandler_1(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::remove_bannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_bannerEventHandler_m2591577026_MetadataUsageId;
extern "C"  void Admob_remove_bannerEventHandler_m2591577026 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_bannerEventHandler_m2591577026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_bannerEventHandler_1();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_bannerEventHandler_1(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::add_interstitialEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_interstitialEventHandler_m528202391_MetadataUsageId;
extern "C"  void Admob_add_interstitialEventHandler_m528202391 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_interstitialEventHandler_m528202391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_interstitialEventHandler_2();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_interstitialEventHandler_2(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::remove_interstitialEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_interstitialEventHandler_m2729753070_MetadataUsageId;
extern "C"  void Admob_remove_interstitialEventHandler_m2729753070 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_interstitialEventHandler_m2729753070_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_interstitialEventHandler_2();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_interstitialEventHandler_2(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::add_rewardedVideoEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_rewardedVideoEventHandler_m698753172_MetadataUsageId;
extern "C"  void Admob_add_rewardedVideoEventHandler_m698753172 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_rewardedVideoEventHandler_m698753172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_rewardedVideoEventHandler_3();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_rewardedVideoEventHandler_3(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::remove_rewardedVideoEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_rewardedVideoEventHandler_m2765341749_MetadataUsageId;
extern "C"  void Admob_remove_rewardedVideoEventHandler_m2765341749 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_rewardedVideoEventHandler_m2765341749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_rewardedVideoEventHandler_3();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_rewardedVideoEventHandler_3(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::add_nativeBannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_add_nativeBannerEventHandler_m2314856020_MetadataUsageId;
extern "C"  void Admob_add_nativeBannerEventHandler_m2314856020 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_add_nativeBannerEventHandler_m2314856020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_nativeBannerEventHandler_4();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_nativeBannerEventHandler_4(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void admob.Admob::remove_nativeBannerEventHandler(admob.Admob/AdmobEventHandler)
extern Il2CppClass* AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var;
extern const uint32_t Admob_remove_nativeBannerEventHandler_m4236789151_MetadataUsageId;
extern "C"  void Admob_remove_nativeBannerEventHandler_m4236789151 (Admob_t546240967 * __this, AdmobEventHandler_t2983421020 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_remove_nativeBannerEventHandler_m4236789151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdmobEventHandler_t2983421020 * L_0 = __this->get_nativeBannerEventHandler_4();
		AdmobEventHandler_t2983421020 * L_1 = ___value0;
		Delegate_t3022476291 * L_2 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->set_nativeBannerEventHandler_4(((AdmobEventHandler_t2983421020 *)CastclassSealed(L_2, AdmobEventHandler_t2983421020_il2cpp_TypeInfo_var)));
		return;
	}
}
// admob.Admob admob.Admob::Instance()
extern Il2CppClass* Admob_t546240967_il2cpp_TypeInfo_var;
extern const uint32_t Admob_Instance_m352023099_MetadataUsageId;
extern "C"  Admob_t546240967 * Admob_Instance_m352023099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_Instance_m352023099_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Admob_t546240967 * L_0 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Admob_t546240967 * L_1 = (Admob_t546240967 *)il2cpp_codegen_object_new(Admob_t546240967_il2cpp_TypeInfo_var);
		Admob__ctor_m3904493015(L_1, /*hidden argument*/NULL);
		((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->set__instance_0(L_1);
		Admob_t546240967 * L_2 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		NullCheck(L_2);
		Admob_preInitAdmob_m392180999(L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		Admob_t546240967 * L_3 = ((Admob_t546240967_StaticFields*)Admob_t546240967_il2cpp_TypeInfo_var->static_fields)->get__instance_0();
		return L_3;
	}
}
// System.Void admob.Admob::preInitAdmob()
extern "C"  void Admob_preInitAdmob_m392180999 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C" void DEFAULT_CALL _kminitAdmob(char*, char*, Il2CppMethodPointer);
// System.Void admob.Admob::_kminitAdmob(System.String,System.String,admob.Admob/AdmobAdCallBack)
extern "C"  void Admob__kminitAdmob_m3213489909 (Il2CppObject * __this /* static, unused */, String_t* ___bannerid0, String_t* ___fullid1, AdmobAdCallBack_t390902866 * ___callback2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, Il2CppMethodPointer);

	// Marshaling of parameter '___bannerid0' to native representation
	char* ____bannerid0_marshaled = NULL;
	____bannerid0_marshaled = il2cpp_codegen_marshal_string(___bannerid0);

	// Marshaling of parameter '___fullid1' to native representation
	char* ____fullid1_marshaled = NULL;
	____fullid1_marshaled = il2cpp_codegen_marshal_string(___fullid1);

	// Marshaling of parameter '___callback2' to native representation
	Il2CppMethodPointer ____callback2_marshaled = NULL;
	____callback2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kminitAdmob)(____bannerid0_marshaled, ____fullid1_marshaled, ____callback2_marshaled);

	// Marshaling cleanup of parameter '___bannerid0' native representation
	il2cpp_codegen_marshal_free(____bannerid0_marshaled);
	____bannerid0_marshaled = NULL;

	// Marshaling cleanup of parameter '___fullid1' native representation
	il2cpp_codegen_marshal_free(____fullid1_marshaled);
	____fullid1_marshaled = NULL;

}
// System.Void admob.Admob::initAdmob(System.String,System.String)
extern Il2CppClass* AdmobAdCallBack_t390902866_il2cpp_TypeInfo_var;
extern const MethodInfo* Admob_onAdmobEventCallBack_m1864886544_MethodInfo_var;
extern const uint32_t Admob_initAdmob_m1941857206_MetadataUsageId;
extern "C"  void Admob_initAdmob_m1941857206 (Admob_t546240967 * __this, String_t* ___bannerID0, String_t* ___fullID1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_initAdmob_m1941857206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___bannerID0;
		String_t* L_1 = ___fullID1;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Admob_onAdmobEventCallBack_m1864886544_MethodInfo_var);
		AdmobAdCallBack_t390902866 * L_3 = (AdmobAdCallBack_t390902866 *)il2cpp_codegen_object_new(AdmobAdCallBack_t390902866_il2cpp_TypeInfo_var);
		AdmobAdCallBack__ctor_m2242352453(L_3, NULL, L_2, /*hidden argument*/NULL);
		Admob__kminitAdmob_m3213489909(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowNativeBannerAbsolute(int32_t, int32_t, int32_t, int32_t, char*, char*);
// System.Void admob.Admob::_kmshowNativeBannerAbsolute(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob__kmshowNativeBannerAbsolute_m3098491723 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___x2, int32_t ___y3, String_t* ___nativeID4, String_t* ___instanceName5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*, char*);

	// Marshaling of parameter '___nativeID4' to native representation
	char* ____nativeID4_marshaled = NULL;
	____nativeID4_marshaled = il2cpp_codegen_marshal_string(___nativeID4);

	// Marshaling of parameter '___instanceName5' to native representation
	char* ____instanceName5_marshaled = NULL;
	____instanceName5_marshaled = il2cpp_codegen_marshal_string(___instanceName5);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowNativeBannerAbsolute)(___width0, ___height1, ___x2, ___y3, ____nativeID4_marshaled, ____instanceName5_marshaled);

	// Marshaling cleanup of parameter '___nativeID4' native representation
	il2cpp_codegen_marshal_free(____nativeID4_marshaled);
	____nativeID4_marshaled = NULL;

	// Marshaling cleanup of parameter '___instanceName5' native representation
	il2cpp_codegen_marshal_free(____instanceName5_marshaled);
	____instanceName5_marshaled = NULL;

}
// System.Void admob.Admob::showNativeBannerAbsolute(admob.AdSize,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob_showNativeBannerAbsolute_m2200024897 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___x1, int32_t ___y2, String_t* ___nativeID3, String_t* ___instanceName4, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___x1;
		int32_t L_5 = ___y2;
		String_t* L_6 = ___nativeID3;
		String_t* L_7 = ___instanceName4;
		Admob__kmshowNativeBannerAbsolute_m3098491723(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowNativeBannerRelative(int32_t, int32_t, int32_t, int32_t, char*, char*);
// System.Void admob.Admob::_kmshowNativeBannerRelative(System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob__kmshowNativeBannerRelative_m3881486200 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___position2, int32_t ___marginY3, String_t* ___nativeID4, String_t* ___instanceName5, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*, char*);

	// Marshaling of parameter '___nativeID4' to native representation
	char* ____nativeID4_marshaled = NULL;
	____nativeID4_marshaled = il2cpp_codegen_marshal_string(___nativeID4);

	// Marshaling of parameter '___instanceName5' to native representation
	char* ____instanceName5_marshaled = NULL;
	____instanceName5_marshaled = il2cpp_codegen_marshal_string(___instanceName5);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowNativeBannerRelative)(___width0, ___height1, ___position2, ___marginY3, ____nativeID4_marshaled, ____instanceName5_marshaled);

	// Marshaling cleanup of parameter '___nativeID4' native representation
	il2cpp_codegen_marshal_free(____nativeID4_marshaled);
	____nativeID4_marshaled = NULL;

	// Marshaling cleanup of parameter '___instanceName5' native representation
	il2cpp_codegen_marshal_free(____instanceName5_marshaled);
	____instanceName5_marshaled = NULL;

}
// System.Void admob.Admob::showNativeBannerRelative(admob.AdSize,System.Int32,System.Int32,System.String,System.String)
extern "C"  void Admob_showNativeBannerRelative_m4052082498 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___position1, int32_t ___marginY2, String_t* ___nativeID3, String_t* ___instanceName4, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___position1;
		int32_t L_5 = ___marginY2;
		String_t* L_6 = ___nativeID3;
		String_t* L_7 = ___instanceName4;
		Admob__kmshowNativeBannerRelative_m3881486200(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmremoveNativeBanner(char*);
// System.Void admob.Admob::_kmremoveNativeBanner(System.String)
extern "C"  void Admob__kmremoveNativeBanner_m3510797403 (Il2CppObject * __this /* static, unused */, String_t* ___instanceName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___instanceName0' to native representation
	char* ____instanceName0_marshaled = NULL;
	____instanceName0_marshaled = il2cpp_codegen_marshal_string(___instanceName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmremoveNativeBanner)(____instanceName0_marshaled);

	// Marshaling cleanup of parameter '___instanceName0' native representation
	il2cpp_codegen_marshal_free(____instanceName0_marshaled);
	____instanceName0_marshaled = NULL;

}
// System.Void admob.Admob::removeNativeBanner(System.String)
extern "C"  void Admob_removeNativeBanner_m348786124 (Admob_t546240967 * __this, String_t* ___instanceName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___instanceName0;
		Admob__kmremoveNativeBanner_m3510797403(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowBannerAbsolute(int32_t, int32_t, int32_t, int32_t, char*);
// System.Void admob.Admob::_kmshowBannerAbsolute(System.Int32,System.Int32,System.Int32,System.Int32,System.String)
extern "C"  void Admob__kmshowBannerAbsolute_m3495462606 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___x2, int32_t ___y3, String_t* ___instanceName4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*);

	// Marshaling of parameter '___instanceName4' to native representation
	char* ____instanceName4_marshaled = NULL;
	____instanceName4_marshaled = il2cpp_codegen_marshal_string(___instanceName4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowBannerAbsolute)(___width0, ___height1, ___x2, ___y3, ____instanceName4_marshaled);

	// Marshaling cleanup of parameter '___instanceName4' native representation
	il2cpp_codegen_marshal_free(____instanceName4_marshaled);
	____instanceName4_marshaled = NULL;

}
// System.Void admob.Admob::showBannerAbsolute(admob.AdSize,System.Int32,System.Int32,System.String)
extern "C"  void Admob_showBannerAbsolute_m450906656 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___x1, int32_t ___y2, String_t* ___instanceName3, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___x1;
		int32_t L_5 = ___y2;
		String_t* L_6 = ___instanceName3;
		Admob__kmshowBannerAbsolute_m3495462606(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmshowBannerRelative(int32_t, int32_t, int32_t, int32_t, char*);
// System.Void admob.Admob::_kmshowBannerRelative(System.Int32,System.Int32,System.Int32,System.Int32,System.String)
extern "C"  void Admob__kmshowBannerRelative_m1922121111 (Il2CppObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___position2, int32_t ___marginY3, String_t* ___instanceName4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t, char*);

	// Marshaling of parameter '___instanceName4' to native representation
	char* ____instanceName4_marshaled = NULL;
	____instanceName4_marshaled = il2cpp_codegen_marshal_string(___instanceName4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowBannerRelative)(___width0, ___height1, ___position2, ___marginY3, ____instanceName4_marshaled);

	// Marshaling cleanup of parameter '___instanceName4' native representation
	il2cpp_codegen_marshal_free(____instanceName4_marshaled);
	____instanceName4_marshaled = NULL;

}
// System.Void admob.Admob::showBannerRelative(admob.AdSize,System.Int32,System.Int32,System.String)
extern "C"  void Admob_showBannerRelative_m3640333681 (Admob_t546240967 * __this, AdSize_t3770813302 * ___size0, int32_t ___position1, int32_t ___marginY2, String_t* ___instanceName3, const MethodInfo* method)
{
	{
		AdSize_t3770813302 * L_0 = ___size0;
		NullCheck(L_0);
		int32_t L_1 = AdSize_get_Width_m317946431(L_0, /*hidden argument*/NULL);
		AdSize_t3770813302 * L_2 = ___size0;
		NullCheck(L_2);
		int32_t L_3 = AdSize_get_Height_m955561220(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___position1;
		int32_t L_5 = ___marginY2;
		String_t* L_6 = ___instanceName3;
		Admob__kmshowBannerRelative_m1922121111(NULL /*static, unused*/, L_1, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmremoveBanner(char*);
// System.Void admob.Admob::_kmremoveBanner(System.String)
extern "C"  void Admob__kmremoveBanner_m3583702054 (Il2CppObject * __this /* static, unused */, String_t* ___instanceName0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___instanceName0' to native representation
	char* ____instanceName0_marshaled = NULL;
	____instanceName0_marshaled = il2cpp_codegen_marshal_string(___instanceName0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmremoveBanner)(____instanceName0_marshaled);

	// Marshaling cleanup of parameter '___instanceName0' native representation
	il2cpp_codegen_marshal_free(____instanceName0_marshaled);
	____instanceName0_marshaled = NULL;

}
// System.Void admob.Admob::removeBanner(System.String)
extern "C"  void Admob_removeBanner_m2206939851 (Admob_t546240967 * __this, String_t* ___instanceName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___instanceName0;
		Admob__kmremoveBanner_m3583702054(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmloadInterstitial();
// System.Void admob.Admob::_kmloadInterstitial()
extern "C"  void Admob__kmloadInterstitial_m2535838764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmloadInterstitial)();

}
// System.Void admob.Admob::loadInterstitial()
extern "C"  void Admob_loadInterstitial_m2602394663 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Admob__kmloadInterstitial_m2535838764(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C" int32_t DEFAULT_CALL _kmisInterstitialReady();
// System.Boolean admob.Admob::_kmisInterstitialReady()
extern "C"  bool Admob__kmisInterstitialReady_m3914756345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_kmisInterstitialReady)();

	return returnValue;
}
// System.Boolean admob.Admob::isInterstitialReady()
extern "C"  bool Admob_isInterstitialReady_m3353041644 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Admob__kmisInterstitialReady_m3914756345(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C" void DEFAULT_CALL _kmshowInterstitial();
// System.Void admob.Admob::_kmshowInterstitial()
extern "C"  void Admob__kmshowInterstitial_m1396908119 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowInterstitial)();

}
// System.Void admob.Admob::showInterstitial()
extern "C"  void Admob_showInterstitial_m2032088296 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Admob__kmshowInterstitial_m1396908119(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmloadRewardedVideo(char*);
// System.Void admob.Admob::_kmloadRewardedVideo(System.String)
extern "C"  void Admob__kmloadRewardedVideo_m1533307933 (Il2CppObject * __this /* static, unused */, String_t* ___rewardedVideoID0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___rewardedVideoID0' to native representation
	char* ____rewardedVideoID0_marshaled = NULL;
	____rewardedVideoID0_marshaled = il2cpp_codegen_marshal_string(___rewardedVideoID0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmloadRewardedVideo)(____rewardedVideoID0_marshaled);

	// Marshaling cleanup of parameter '___rewardedVideoID0' native representation
	il2cpp_codegen_marshal_free(____rewardedVideoID0_marshaled);
	____rewardedVideoID0_marshaled = NULL;

}
// System.Void admob.Admob::loadRewardedVideo(System.String)
extern "C"  void Admob_loadRewardedVideo_m4191770420 (Admob_t546240967 * __this, String_t* ___rewardedVideoID0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___rewardedVideoID0;
		Admob__kmloadRewardedVideo_m1533307933(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" int32_t DEFAULT_CALL _kmisRewardedVideoReady();
// System.Boolean admob.Admob::_kmisRewardedVideoReady()
extern "C"  bool Admob__kmisRewardedVideoReady_m3707665492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(_kmisRewardedVideoReady)();

	return returnValue;
}
// System.Boolean admob.Admob::isRewardedVideoReady()
extern "C"  bool Admob_isRewardedVideoReady_m89423453 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Admob__kmisRewardedVideoReady_m3707665492(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C" void DEFAULT_CALL _kmshowRewardedVideo();
// System.Void admob.Admob::_kmshowRewardedVideo()
extern "C"  void Admob__kmshowRewardedVideo_m3329502270 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmshowRewardedVideo)();

}
// System.Void admob.Admob::showRewardedVideo()
extern "C"  void Admob_showRewardedVideo_m908495731 (Admob_t546240967 * __this, const MethodInfo* method)
{
	{
		Admob__kmshowRewardedVideo_m3329502270(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmsetTesting(int32_t);
// System.Void admob.Admob::_kmsetTesting(System.Boolean)
extern "C"  void Admob__kmsetTesting_m830384613 (Il2CppObject * __this /* static, unused */, bool ___v0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmsetTesting)(___v0);

}
// System.Void admob.Admob::setTesting(System.Boolean)
extern "C"  void Admob_setTesting_m4242041198 (Admob_t546240967 * __this, bool ___v0, const MethodInfo* method)
{
	{
		bool L_0 = ___v0;
		Admob__kmsetTesting_m830384613(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL _kmsetForChildren(int32_t);
// System.Void admob.Admob::_kmsetForChildren(System.Boolean)
extern "C"  void Admob__kmsetForChildren_m3748844775 (Il2CppObject * __this /* static, unused */, bool ___v0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(_kmsetForChildren)(___v0);

}
// System.Void admob.Admob::setForChildren(System.Boolean)
extern "C"  void Admob_setForChildren_m3468175206 (Admob_t546240967 * __this, bool ___v0, const MethodInfo* method)
{
	{
		bool L_0 = ___v0;
		Admob__kmsetForChildren_m3748844775(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.Admob::onAdmobEventCallBack(System.String,System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1222408196;
extern Il2CppCodeGenString* _stringLiteral3011292000;
extern Il2CppCodeGenString* _stringLiteral858169867;
extern Il2CppCodeGenString* _stringLiteral4099895337;
extern const uint32_t Admob_onAdmobEventCallBack_m1864886544_MetadataUsageId;
extern "C"  void Admob_onAdmobEventCallBack_m1864886544 (Il2CppObject * __this /* static, unused */, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Admob_onAdmobEventCallBack_m1864886544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_0, _stringLiteral1222408196, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		Admob_t546240967 * L_2 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		AdmobEventHandler_t2983421020 * L_3 = L_2->get_bannerEventHandler_1();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		Admob_t546240967 * L_4 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		AdmobEventHandler_t2983421020 * L_5 = L_4->get_bannerEventHandler_1();
		String_t* L_6 = ___eventName1;
		String_t* L_7 = ___msg2;
		NullCheck(L_5);
		AdmobEventHandler_Invoke_m1278357557(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0030:
	{
		goto IL_00cf;
	}

IL_0035:
	{
		String_t* L_8 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, _stringLiteral3011292000, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		Admob_t546240967 * L_10 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		AdmobEventHandler_t2983421020 * L_11 = L_10->get_interstitialEventHandler_2();
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		Admob_t546240967 * L_12 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		AdmobEventHandler_t2983421020 * L_13 = L_12->get_interstitialEventHandler_2();
		String_t* L_14 = ___eventName1;
		String_t* L_15 = ___msg2;
		NullCheck(L_13);
		AdmobEventHandler_Invoke_m1278357557(L_13, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0065:
	{
		goto IL_00cf;
	}

IL_006a:
	{
		String_t* L_16 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_16, _stringLiteral858169867, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009f;
		}
	}
	{
		Admob_t546240967 * L_18 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		AdmobEventHandler_t2983421020 * L_19 = L_18->get_rewardedVideoEventHandler_3();
		if (!L_19)
		{
			goto IL_009a;
		}
	}
	{
		Admob_t546240967 * L_20 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		AdmobEventHandler_t2983421020 * L_21 = L_20->get_rewardedVideoEventHandler_3();
		String_t* L_22 = ___eventName1;
		String_t* L_23 = ___msg2;
		NullCheck(L_21);
		AdmobEventHandler_Invoke_m1278357557(L_21, L_22, L_23, /*hidden argument*/NULL);
	}

IL_009a:
	{
		goto IL_00cf;
	}

IL_009f:
	{
		String_t* L_24 = ___adtype0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_24, _stringLiteral4099895337, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00cf;
		}
	}
	{
		Admob_t546240967 * L_26 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		AdmobEventHandler_t2983421020 * L_27 = L_26->get_nativeBannerEventHandler_4();
		if (!L_27)
		{
			goto IL_00cf;
		}
	}
	{
		Admob_t546240967 * L_28 = Admob_Instance_m352023099(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		AdmobEventHandler_t2983421020 * L_29 = L_28->get_nativeBannerEventHandler_4();
		String_t* L_30 = ___eventName1;
		String_t* L_31 = ___msg2;
		NullCheck(L_29);
		AdmobEventHandler_Invoke_m1278357557(L_29, L_30, L_31, /*hidden argument*/NULL);
	}

IL_00cf:
	{
		return;
	}
}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Admob_onAdmobEventCallBack_m1864886544(char* ___adtype0, char* ___eventName1, char* ___msg2)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;

	// Marshaling of parameter '___adtype0' to managed representation
	String_t* ____adtype0_unmarshaled = NULL;
	____adtype0_unmarshaled = il2cpp_codegen_marshal_string_result(___adtype0);

	// Marshaling of parameter '___eventName1' to managed representation
	String_t* ____eventName1_unmarshaled = NULL;
	____eventName1_unmarshaled = il2cpp_codegen_marshal_string_result(___eventName1);

	// Marshaling of parameter '___msg2' to managed representation
	String_t* ____msg2_unmarshaled = NULL;
	____msg2_unmarshaled = il2cpp_codegen_marshal_string_result(___msg2);

	// Managed method invocation
	Admob_onAdmobEventCallBack_m1864886544(NULL, ____adtype0_unmarshaled, ____eventName1_unmarshaled, ____msg2_unmarshaled, NULL);

}
// System.Void admob.Admob/AdmobAdCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void AdmobAdCallBack__ctor_m2242352453 (AdmobAdCallBack_t390902866 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void admob.Admob/AdmobAdCallBack::Invoke(System.String,System.String,System.String)
extern "C"  void AdmobAdCallBack_Invoke_m1861044811 (AdmobAdCallBack_t390902866 * __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AdmobAdCallBack_Invoke_m1861044811((AdmobAdCallBack_t390902866 *)__this->get_prev_9(),___adtype0, ___eventName1, ___msg2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___adtype0, ___eventName1, ___msg2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___adtype0, ___eventName1, ___msg2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___adtype0, ___eventName1, ___msg2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AdmobAdCallBack_t390902866 (AdmobAdCallBack_t390902866 * __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___adtype0' to native representation
	char* ____adtype0_marshaled = NULL;
	____adtype0_marshaled = il2cpp_codegen_marshal_string(___adtype0);

	// Marshaling of parameter '___eventName1' to native representation
	char* ____eventName1_marshaled = NULL;
	____eventName1_marshaled = il2cpp_codegen_marshal_string(___eventName1);

	// Marshaling of parameter '___msg2' to native representation
	char* ____msg2_marshaled = NULL;
	____msg2_marshaled = il2cpp_codegen_marshal_string(___msg2);

	// Native function invocation
	il2cppPInvokeFunc(____adtype0_marshaled, ____eventName1_marshaled, ____msg2_marshaled);

	// Marshaling cleanup of parameter '___adtype0' native representation
	il2cpp_codegen_marshal_free(____adtype0_marshaled);
	____adtype0_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventName1' native representation
	il2cpp_codegen_marshal_free(____eventName1_marshaled);
	____eventName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___msg2' native representation
	il2cpp_codegen_marshal_free(____msg2_marshaled);
	____msg2_marshaled = NULL;

}
// System.IAsyncResult admob.Admob/AdmobAdCallBack::BeginInvoke(System.String,System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AdmobAdCallBack_BeginInvoke_m1105238270 (AdmobAdCallBack_t390902866 * __this, String_t* ___adtype0, String_t* ___eventName1, String_t* ___msg2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___adtype0;
	__d_args[1] = ___eventName1;
	__d_args[2] = ___msg2;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void admob.Admob/AdmobAdCallBack::EndInvoke(System.IAsyncResult)
extern "C"  void AdmobAdCallBack_EndInvoke_m928280963 (AdmobAdCallBack_t390902866 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void admob.Admob/AdmobEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AdmobEventHandler__ctor_m943312861 (AdmobEventHandler_t2983421020 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void admob.Admob/AdmobEventHandler::Invoke(System.String,System.String)
extern "C"  void AdmobEventHandler_Invoke_m1278357557 (AdmobEventHandler_t2983421020 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AdmobEventHandler_Invoke_m1278357557((AdmobEventHandler_t2983421020 *)__this->get_prev_9(),___eventName0, ___msg1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___eventName0, ___msg1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___eventName0, ___msg1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___msg1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___eventName0, ___msg1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C"  void DelegatePInvokeWrapper_AdmobEventHandler_t2983421020 (AdmobEventHandler_t2983421020 * __this, String_t* ___eventName0, String_t* ___msg1, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___msg1' to native representation
	char* ____msg1_marshaled = NULL;
	____msg1_marshaled = il2cpp_codegen_marshal_string(___msg1);

	// Native function invocation
	il2cppPInvokeFunc(____eventName0_marshaled, ____msg1_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___msg1' native representation
	il2cpp_codegen_marshal_free(____msg1_marshaled);
	____msg1_marshaled = NULL;

}
// System.IAsyncResult admob.Admob/AdmobEventHandler::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AdmobEventHandler_BeginInvoke_m880931180 (AdmobEventHandler_t2983421020 * __this, String_t* ___eventName0, String_t* ___msg1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___eventName0;
	__d_args[1] = ___msg1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void admob.Admob/AdmobEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AdmobEventHandler_EndInvoke_m3142124079 (AdmobEventHandler_t2983421020 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void admob.AdmobEvent::.ctor()
extern "C"  void AdmobEvent__ctor_m2257084687 (AdmobEvent_t4123996035 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.AdmobEvent::.cctor()
extern Il2CppClass* AdmobEvent_t4123996035_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3012284099;
extern Il2CppCodeGenString* _stringLiteral3432291362;
extern Il2CppCodeGenString* _stringLiteral344657805;
extern Il2CppCodeGenString* _stringLiteral791142518;
extern Il2CppCodeGenString* _stringLiteral4016041990;
extern Il2CppCodeGenString* _stringLiteral2433883841;
extern Il2CppCodeGenString* _stringLiteral1454528267;
extern Il2CppCodeGenString* _stringLiteral2871947203;
extern const uint32_t AdmobEvent__cctor_m2614563490_MetadataUsageId;
extern "C"  void AdmobEvent__cctor_m2614563490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdmobEvent__cctor_m2614563490_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdLoaded_0(_stringLiteral3012284099);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdFailedToLoad_1(_stringLiteral3432291362);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdOpened_2(_stringLiteral344657805);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_adViewWillDismissScreen_3(_stringLiteral791142518);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdClosed_4(_stringLiteral4016041990);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onAdLeftApplication_5(_stringLiteral2433883841);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onRewardedVideoStarted_6(_stringLiteral1454528267);
		((AdmobEvent_t4123996035_StaticFields*)AdmobEvent_t4123996035_il2cpp_TypeInfo_var->static_fields)->set_onRewarded_7(_stringLiteral2871947203);
		return;
	}
}
// System.Void admob.AdPosition::.ctor()
extern "C"  void AdPosition__ctor_m417448268 (AdPosition_t1947228246 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void admob.AdPosition::.cctor()
extern Il2CppClass* AdPosition_t1947228246_il2cpp_TypeInfo_var;
extern const uint32_t AdPosition__cctor_m196470457_MetadataUsageId;
extern "C"  void AdPosition__cctor_m196470457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdPosition__cctor_m196470457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_TOP_LEFT_1(1);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_TOP_CENTER_2(2);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_TOP_RIGHT_3(3);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_MIDDLE_LEFT_4(4);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_MIDDLE_CENTER_5(5);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_MIDDLE_RIGHT_6(6);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_BOTTOM_LEFT_7(7);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_BOTTOM_CENTER_8(8);
		((AdPosition_t1947228246_StaticFields*)AdPosition_t1947228246_il2cpp_TypeInfo_var->static_fields)->set_BOTTOM_RIGHT_9(((int32_t)9));
		return;
	}
}
// System.Void admob.AdSize::.ctor(System.Int32,System.Int32)
extern "C"  void AdSize__ctor_m3068073238 (AdSize_t3770813302 * __this, int32_t ___width0, int32_t ___height1, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_width_0(L_0);
		int32_t L_1 = ___height1;
		__this->set_height_1(L_1);
		return;
	}
}
// System.Void admob.AdSize::.cctor()
extern Il2CppClass* AdSize_t3770813302_il2cpp_TypeInfo_var;
extern const uint32_t AdSize__cctor_m3201987233_MetadataUsageId;
extern "C"  void AdSize__cctor_m3201987233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdSize__cctor_m3201987233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AdSize_t3770813302 * L_0 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_0, ((int32_t)320), ((int32_t)50), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_Banner_2(L_0);
		AdSize_t3770813302 * L_1 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_1, ((int32_t)300), ((int32_t)250), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_MediumRectangle_3(L_1);
		AdSize_t3770813302 * L_2 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_2, ((int32_t)468), ((int32_t)60), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_IABBanner_4(L_2);
		AdSize_t3770813302 * L_3 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_3, ((int32_t)728), ((int32_t)90), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_Leaderboard_5(L_3);
		AdSize_t3770813302 * L_4 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_4, ((int32_t)120), ((int32_t)600), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_WideSkyscraper_6(L_4);
		AdSize_t3770813302 * L_5 = (AdSize_t3770813302 *)il2cpp_codegen_object_new(AdSize_t3770813302_il2cpp_TypeInfo_var);
		AdSize__ctor_m3068073238(L_5, (-1), ((int32_t)-2), /*hidden argument*/NULL);
		((AdSize_t3770813302_StaticFields*)AdSize_t3770813302_il2cpp_TypeInfo_var->static_fields)->set_SmartBanner_7(L_5);
		return;
	}
}
// System.Int32 admob.AdSize::get_Width()
extern "C"  int32_t AdSize_get_Width_m317946431 (AdSize_t3770813302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_width_0();
		return L_0;
	}
}
// System.Int32 admob.AdSize::get_Height()
extern "C"  int32_t AdSize_get_Height_m955561220 (AdSize_t3770813302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_height_1();
		return L_0;
	}
}
// System.Void MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C"  void MonoPInvokeCallbackAttribute__ctor_m1119843856 (MonoPInvokeCallbackAttribute_t1970456718 * __this, Type_t * ___type0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
