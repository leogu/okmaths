﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenTransformBlendableScaleBy
struct DOTweenTransformBlendableScaleBy_t2382086248;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableScaleBy::.ctor()
extern "C"  void DOTweenTransformBlendableScaleBy__ctor_m528044458 (DOTweenTransformBlendableScaleBy_t2382086248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableScaleBy::Reset()
extern "C"  void DOTweenTransformBlendableScaleBy_Reset_m2857135249 (DOTweenTransformBlendableScaleBy_t2382086248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableScaleBy::OnEnter()
extern "C"  void DOTweenTransformBlendableScaleBy_OnEnter_m2649717481 (DOTweenTransformBlendableScaleBy_t2382086248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableScaleBy::<OnEnter>m__AE()
extern "C"  void DOTweenTransformBlendableScaleBy_U3COnEnterU3Em__AE_m2754381708 (DOTweenTransformBlendableScaleBy_t2382086248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenTransformBlendableScaleBy::<OnEnter>m__AF()
extern "C"  void DOTweenTransformBlendableScaleBy_U3COnEnterU3Em__AF_m1490825745 (DOTweenTransformBlendableScaleBy_t2382086248 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
