﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition605142169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiTransitionGetType
struct  uGuiTransitionGetType_t1900312903  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiTransitionGetType::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiTransitionGetType::transition
	FsmString_t2414474701 * ___transition_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiTransitionGetType::colorTintEvent
	FsmEvent_t1258573736 * ___colorTintEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiTransitionGetType::spriteSwapEvent
	FsmEvent_t1258573736 * ___spriteSwapEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiTransitionGetType::animationEvent
	FsmEvent_t1258573736 * ___animationEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiTransitionGetType::noTransitionEvent
	FsmEvent_t1258573736 * ___noTransitionEvent_16;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiTransitionGetType::_selectable
	Selectable_t1490392188 * ____selectable_17;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.uGuiTransitionGetType::_originalTransition
	int32_t ____originalTransition_18;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_transition_12() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ___transition_12)); }
	inline FsmString_t2414474701 * get_transition_12() const { return ___transition_12; }
	inline FsmString_t2414474701 ** get_address_of_transition_12() { return &___transition_12; }
	inline void set_transition_12(FsmString_t2414474701 * value)
	{
		___transition_12 = value;
		Il2CppCodeGenWriteBarrier(&___transition_12, value);
	}

	inline static int32_t get_offset_of_colorTintEvent_13() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ___colorTintEvent_13)); }
	inline FsmEvent_t1258573736 * get_colorTintEvent_13() const { return ___colorTintEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_colorTintEvent_13() { return &___colorTintEvent_13; }
	inline void set_colorTintEvent_13(FsmEvent_t1258573736 * value)
	{
		___colorTintEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___colorTintEvent_13, value);
	}

	inline static int32_t get_offset_of_spriteSwapEvent_14() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ___spriteSwapEvent_14)); }
	inline FsmEvent_t1258573736 * get_spriteSwapEvent_14() const { return ___spriteSwapEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_spriteSwapEvent_14() { return &___spriteSwapEvent_14; }
	inline void set_spriteSwapEvent_14(FsmEvent_t1258573736 * value)
	{
		___spriteSwapEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___spriteSwapEvent_14, value);
	}

	inline static int32_t get_offset_of_animationEvent_15() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ___animationEvent_15)); }
	inline FsmEvent_t1258573736 * get_animationEvent_15() const { return ___animationEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_animationEvent_15() { return &___animationEvent_15; }
	inline void set_animationEvent_15(FsmEvent_t1258573736 * value)
	{
		___animationEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___animationEvent_15, value);
	}

	inline static int32_t get_offset_of_noTransitionEvent_16() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ___noTransitionEvent_16)); }
	inline FsmEvent_t1258573736 * get_noTransitionEvent_16() const { return ___noTransitionEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_noTransitionEvent_16() { return &___noTransitionEvent_16; }
	inline void set_noTransitionEvent_16(FsmEvent_t1258573736 * value)
	{
		___noTransitionEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___noTransitionEvent_16, value);
	}

	inline static int32_t get_offset_of__selectable_17() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ____selectable_17)); }
	inline Selectable_t1490392188 * get__selectable_17() const { return ____selectable_17; }
	inline Selectable_t1490392188 ** get_address_of__selectable_17() { return &____selectable_17; }
	inline void set__selectable_17(Selectable_t1490392188 * value)
	{
		____selectable_17 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_17, value);
	}

	inline static int32_t get_offset_of__originalTransition_18() { return static_cast<int32_t>(offsetof(uGuiTransitionGetType_t1900312903, ____originalTransition_18)); }
	inline int32_t get__originalTransition_18() const { return ____originalTransition_18; }
	inline int32_t* get_address_of__originalTransition_18() { return &____originalTransition_18; }
	inline void set__originalTransition_18(int32_t value)
	{
		____originalTransition_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
