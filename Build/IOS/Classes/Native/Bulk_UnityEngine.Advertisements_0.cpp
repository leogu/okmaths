﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// UnityEngine.Advertisements.ShowOptions
struct ShowOptions_t1358843767;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Advertisements.AsyncExec/CoroutineHostMonoBehaviour
struct CoroutineHostMonoBehaviour_t291073519;
// System.Object
struct Il2CppObject;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Action`1<UnityEngine.Advertisements.ShowResult>
struct Action_1_t258469394;
// UnityEngine.Advertisements.UnityAdsEditor
struct UnityAdsEditor_t1440819734;
// UnityEngine.Advertisements.UnityAdsEditorPlaceholder
struct UnityAdsEditorPlaceholder_t526501903;
// System.Action`1<UnityEngine.WWW>
struct Action_1_t2721744421;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t2603311978;
// UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0
struct U3CGetAdPlanU3Ec__Iterator0_t3446287961;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Advertisements.UnityAdsNative
struct UnityAdsNative_t3929010778;
// UnityEngine.Advertisements.UnityAdsPlatform
struct UnityAdsPlatform_t714383836;
// UnityEngine.Advertisements.UnityAdsUnsupported
struct UnityAdsUnsupported_t1914686426;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_Advertisements_U3CModuleU3E3783534214.h"
#include "UnityEngine_Advertisements_U3CModuleU3E3783534214MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3914199195.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3914199195MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1358843767.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen714383836.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1591511100.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3929010778MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1914686426MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3929010778.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1914686426.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1440819734MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1440819734.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsMan1959728304MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen714383836MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3613839672MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsInt2792527316MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel1684806294MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel3613839672.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDel1684806294.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1358843767MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen258469394MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen258469394.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen456670012.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme1591511100MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3964069003.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3964069003MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen291073519.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen291073519MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen456670012MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen526501903MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertisemen526501903.h"
#include "UnityEngine_UnityEngine_WWW2919945039MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2721744421MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_WWW2919945039.h"
#include "mscorlib_System_Action_1_gen2721744421.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3446287961MethodDeclarations.h"
#include "UnityEngine_Advertisements_UnityEngine_Advertiseme3446287961.h"
#include "mscorlib_System_Text_Encoding663144255MethodDeclarations.h"
#include "UnityEngine_SimpleJson_SimpleJson3569903358MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Int322071877448.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge362681087MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_ge362681087.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729MethodDeclarations.h"
#include "mscorlib_System_IO_File1930543328MethodDeclarations.h"
#include "mscorlib_System_IO_Path41728875MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen786852042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_ScaleMode324459649.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m2049635786(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Advertisements.AsyncExec/CoroutineHostMonoBehaviour>()
#define GameObject_AddComponent_TisCoroutineHostMonoBehaviour_t291073519_m488405314(__this, method) ((  CoroutineHostMonoBehaviour_t291073519 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Advertisements.UnityAdsEditorPlaceholder>()
#define GameObject_AddComponent_TisUnityAdsEditorPlaceholder_t526501903_m1667733857(__this, method) ((  UnityAdsEditorPlaceholder_t526501903 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m2049635786_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement__cctor_m1138733083_MetadataUsageId;
extern "C"  void Advertisement__cctor_m1138733083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement__cctor_m1138733083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Initialized_0((bool)0);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Showing_1((bool)0);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ShowOptions_2((ShowOptions_t1358843767 *)NULL);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ResultDelivered_3((bool)0);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Implementation_4((UnityAdsPlatform_t714383836 *)NULL);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ExtensionPath_5((String_t*)NULL);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_EditorSupportedPlatform_6((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		bool L_0 = Debug_get_isDebugBuild_m3461630994(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		G_B3_0 = ((int32_t)15);
		goto IL_003c;
	}

IL_003b:
	{
		G_B3_0 = 7;
	}

IL_003c:
	{
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set__debugLevel_7(G_B3_0);
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::LoadRuntime()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsNative_t3929010778_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsUnsupported_t1914686426_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_LoadRuntime_m3443607128_MetadataUsageId;
extern "C"  void Advertisement_LoadRuntime_m3443607128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_LoadRuntime_m3443607128_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		if (L_0)
		{
			goto IL_003d;
		}
	}
	{
		bool L_1 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		return;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_2 = Advertisement_get_isSupported_m1183881609(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		UnityAdsNative_t3929010778 * L_3 = (UnityAdsNative_t3929010778 *)il2cpp_codegen_object_new(UnityAdsNative_t3929010778_il2cpp_TypeInfo_var);
		UnityAdsNative__ctor_m1896064941(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Implementation_4(L_3);
		goto IL_0038;
	}

IL_002e:
	{
		UnityAdsUnsupported_t1914686426 * L_4 = (UnityAdsUnsupported_t1914686426 *)il2cpp_codegen_object_new(UnityAdsUnsupported_t1914686426_il2cpp_TypeInfo_var);
		UnityAdsUnsupported__ctor_m1034993361(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Implementation_4(L_4);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Load_m2226383838(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::LoadEditor(System.String,System.Boolean)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsEditor_t1440819734_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsUnsupported_t1914686426_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_LoadEditor_m194636370_MetadataUsageId;
extern "C"  void Advertisement_LoadEditor_m194636370 (Il2CppObject * __this /* static, unused */, String_t* ___extensionPath0, bool ___supportedPlatform1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_LoadEditor_m194636370_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_1 = ___extensionPath0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ExtensionPath_5(L_1);
		bool L_2 = ___supportedPlatform1;
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		UnityAdsEditor_t1440819734 * L_3 = (UnityAdsEditor_t1440819734 *)il2cpp_codegen_object_new(UnityAdsEditor_t1440819734_il2cpp_TypeInfo_var);
		UnityAdsEditor__ctor_m1232485421(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Implementation_4(L_3);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_EditorSupportedPlatform_6((bool)1);
		goto IL_0035;
	}

IL_002b:
	{
		UnityAdsUnsupported_t1914686426 * L_4 = (UnityAdsUnsupported_t1914686426 *)il2cpp_codegen_object_new(UnityAdsUnsupported_t1914686426_il2cpp_TypeInfo_var);
		UnityAdsUnsupported__ctor_m1034993361(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Implementation_4(L_4);
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Load_m2226383838(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::Load()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_Load_m2226383838_MetadataUsageId;
extern "C"  void Advertisement_Load_m2226383838 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_Load_m2226383838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = Advertisement_get_isSupported_m1183881609(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_RegisterNative_m2478110156(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Advertisement_get_initializeOnStartup_m2175852553(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		String_t* L_3 = Advertisement_get_gameId_m3262742795(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = Advertisement_get_testMode_m1240196736(NULL /*static, unused*/, /*hidden argument*/NULL);
		Advertisement_Initialize_m2995186509(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::get_initializeOnStartup()
extern "C"  bool Advertisement_get_initializeOnStartup_m2175852553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = UnityAdsManager_get_initializeOnStartup_m1942065522(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::RegisterNative()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_RegisterNative_m2478110156_MetadataUsageId;
extern "C"  void Advertisement_RegisterNative_m2478110156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_RegisterNative_m2478110156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		String_t* L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_ExtensionPath_5();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(4 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::RegisterNative(System.String) */, L_0, L_1);
		return;
	}
}
// UnityEngine.Advertisements.Advertisement/DebugLevel UnityEngine.Advertisements.Advertisement::get_debugLevel()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_get_debugLevel_m1094088667_MetadataUsageId;
extern "C"  int32_t Advertisement_get_debugLevel_m1094088667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_get_debugLevel_m1094088667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		int32_t L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get__debugLevel_7();
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::set_debugLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_set_debugLevel_m2904975526_MetadataUsageId;
extern "C"  void Advertisement_set_debugLevel_m2904975526 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_set_debugLevel_m2904975526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set__debugLevel_7(L_0);
		UnityAdsPlatform_t714383836 * L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_2 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		int32_t L_3 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get__debugLevel_7();
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(8 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::SetLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel) */, L_2, L_3);
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_get_isSupported_m1183881609_MetadataUsageId;
extern "C"  bool Advertisement_get_isSupported_m1183881609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_get_isSupported_m1183881609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_EditorSupportedPlatform_6();
		if (L_1)
		{
			goto IL_002b;
		}
	}

IL_0014:
	{
		int32_t L_2 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0031;
		}
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_4 = Advertisement_IsEnabled_m1631237063(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_4;
	}

IL_0031:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::IsEnabled()
extern "C"  bool Advertisement_IsEnabled_m1631237063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = UnityAdsManager_get_enabled_m3410277395(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = UnityAdsManager_IsPlatformEnabled_m3745516095(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_get_isInitialized_m167127575_MetadataUsageId;
extern "C"  bool Advertisement_get_isInitialized_m167127575 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_get_isInitialized_m167127575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Initialized_0();
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_Initialize_m1497874238_MetadataUsageId;
extern "C"  void Advertisement_Initialize_m1497874238 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_Initialize_m1497874238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___appId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Initialize_m2995186509(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern Il2CppClass* UnityAdsDelegate_2_t1684806294_il2cpp_TypeInfo_var;
extern const MethodInfo* Advertisement_OnHide_m2012390653_MethodInfo_var;
extern const MethodInfo* Advertisement_OnShow_m530994074_MethodInfo_var;
extern const MethodInfo* Advertisement_OnVideoCompleted_m3089778134_MethodInfo_var;
extern const MethodInfo* UnityAdsDelegate_2__ctor_m2702477870_MethodInfo_var;
extern const uint32_t Advertisement_Initialize_m2995186509_MetadataUsageId;
extern "C"  void Advertisement_Initialize_m2995186509 (Il2CppObject * __this /* static, unused */, String_t* ___appId0, bool ___testMode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_Initialize_m2995186509_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Initialized_0();
		if (L_0)
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Initialized_0((bool)1);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Advertisement_OnHide_m2012390653_MethodInfo_var);
		UnityAdsDelegate_t3613839672 * L_3 = (UnityAdsDelegate_t3613839672 *)il2cpp_codegen_object_new(UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var);
		UnityAdsDelegate__ctor_m583892427(L_3, NULL, L_2, /*hidden argument*/NULL);
		UnityAdsInternal_add_onHide_m1355080865(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)Advertisement_OnShow_m530994074_MethodInfo_var);
		UnityAdsDelegate_t3613839672 * L_5 = (UnityAdsDelegate_t3613839672 *)il2cpp_codegen_object_new(UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var);
		UnityAdsDelegate__ctor_m583892427(L_5, NULL, L_4, /*hidden argument*/NULL);
		UnityAdsInternal_add_onShow_m4085422052(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)Advertisement_OnVideoCompleted_m3089778134_MethodInfo_var);
		UnityAdsDelegate_2_t1684806294 * L_7 = (UnityAdsDelegate_2_t1684806294 *)il2cpp_codegen_object_new(UnityAdsDelegate_2_t1684806294_il2cpp_TypeInfo_var);
		UnityAdsDelegate_2__ctor_m2702477870(L_7, NULL, L_6, /*hidden argument*/UnityAdsDelegate_2__ctor_m2702477870_MethodInfo_var);
		UnityAdsInternal_add_onVideoCompleted_m114092648(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		UnityAdsPlatform_t714383836 * L_8 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		String_t* L_9 = ___appId0;
		bool L_10 = ___testMode1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, bool >::Invoke(5 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::Init(System.String,System.Boolean) */, L_8, L_9, L_10);
	}

IL_0059:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::Show()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_Show_m2036493855_MetadataUsageId;
extern "C"  void Advertisement_Show_m2036493855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_Show_m2036493855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m3789622005(NULL /*static, unused*/, (String_t*)NULL, (ShowOptions_t1358843767 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_Show_m994665589_MetadataUsageId;
extern "C"  void Advertisement_Show_m994665589 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_Show_m994665589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_Show_m3789622005(NULL /*static, unused*/, L_0, (ShowOptions_t1358843767 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2290710958_MethodInfo_var;
extern const uint32_t Advertisement_Show_m3789622005_MetadataUsageId;
extern "C"  void Advertisement_Show_m3789622005 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, ShowOptions_t1358843767 * ___options1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_Show_m3789622005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B4_0 = NULL;
	UnityAdsPlatform_t714383836 * G_B4_1 = NULL;
	String_t* G_B3_0 = NULL;
	UnityAdsPlatform_t714383836 * G_B3_1 = NULL;
	String_t* G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	UnityAdsPlatform_t714383836 * G_B5_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Initialized_0();
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Showing_1();
		if (L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_2 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		String_t* L_3 = ___zoneId0;
		ShowOptions_t1358843767 * L_4 = ___options1;
		G_B3_0 = L_3;
		G_B3_1 = L_2;
		if (!L_4)
		{
			G_B4_0 = L_3;
			G_B4_1 = L_2;
			goto IL_002b;
		}
	}
	{
		ShowOptions_t1358843767 * L_5 = ___options1;
		NullCheck(L_5);
		String_t* L_6 = ShowOptions_get_gamerSid_m3683233454(L_5, /*hidden argument*/NULL);
		G_B5_0 = L_6;
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_002c;
	}

IL_002b:
	{
		G_B5_0 = ((String_t*)(NULL));
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_002c:
	{
		NullCheck(G_B5_2);
		bool L_7 = VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(9 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::Show(System.String,System.String) */, G_B5_2, G_B5_1, G_B5_0);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Showing_1((bool)1);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ResultDelivered_3((bool)0);
		ShowOptions_t1358843767 * L_8 = ___options1;
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ShowOptions_2(L_8);
		goto IL_006a;
	}

IL_004d:
	{
		ShowOptions_t1358843767 * L_9 = ___options1;
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		ShowOptions_t1358843767 * L_10 = ___options1;
		NullCheck(L_10);
		Action_1_t258469394 * L_11 = ShowOptions_get_resultCallback_m3731275459(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006a;
		}
	}
	{
		ShowOptions_t1358843767 * L_12 = ___options1;
		NullCheck(L_12);
		Action_1_t258469394 * L_13 = ShowOptions_get_resultCallback_m3731275459(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Action_1_Invoke_m2290710958(L_13, 0, /*hidden argument*/Action_1_Invoke_m2290710958_MethodInfo_var);
	}

IL_006a:
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_IsReady_m876308337_MetadataUsageId;
extern "C"  bool Advertisement_IsReady_m876308337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_IsReady_m876308337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_1 = Advertisement_IsReady_m1389121855(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_IsReady_m1389121855_MetadataUsageId;
extern "C"  bool Advertisement_IsReady_m1389121855 (Il2CppObject * __this /* static, unused */, String_t* ___zoneId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_IsReady_m1389121855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Initialized_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		String_t* L_2 = ___zoneId0;
		NullCheck(L_1);
		bool L_3 = VirtFuncInvoker1< bool, String_t* >::Invoke(6 /* System.Boolean UnityEngine.Advertisements.UnityAdsPlatform::CanShowAds(System.String) */, L_1, L_2);
		return L_3;
	}

IL_0016:
	{
		return (bool)0;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_get_isShowing_m2678672924_MetadataUsageId;
extern "C"  bool Advertisement_get_isShowing_m2678672924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_get_isShowing_m2678672924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Showing_1();
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::SetCampaignDataURL(System.String)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_SetCampaignDataURL_m1645869985_MetadataUsageId;
extern "C"  void Advertisement_SetCampaignDataURL_m1645869985 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_SetCampaignDataURL_m1645869985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		UnityAdsPlatform_t714383836 * L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_Implementation_4();
		String_t* L_2 = ___url0;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void UnityEngine.Advertisements.UnityAdsPlatform::SetCampaignDataURL(System.String) */, L_1, L_2);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::OnHide()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_OnHide_m2012390653_MetadataUsageId;
extern "C"  void Advertisement_OnHide_m2012390653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_OnHide_m2012390653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Showing_1((bool)0);
		Advertisement_DeliverResult_m1285565465(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::OnShow()
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_OnShow_m530994074_MetadataUsageId;
extern "C"  void Advertisement_OnShow_m530994074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_OnShow_m530994074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_Showing_1((bool)1);
		return;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::OnVideoCompleted(System.String,System.Boolean)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const uint32_t Advertisement_OnVideoCompleted_m3089778134_MetadataUsageId;
extern "C"  void Advertisement_OnVideoCompleted_m3089778134 (Il2CppObject * __this /* static, unused */, String_t* ___rewardItemKey0, bool ___skipped1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_OnVideoCompleted_m3089778134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___skipped1;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 2;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		Advertisement_DeliverResult_m1285565465(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.Advertisement::get_testMode()
extern "C"  bool Advertisement_get_testMode_m1240196736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = UnityAdsManager_get_testMode_m1639009775(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Advertisements.Advertisement::get_gameId()
extern "C"  String_t* Advertisement_get_gameId_m3262742795 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = UnityAdsManager_GetGameId_m2796497895(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Advertisements.Advertisement::DeliverResult(UnityEngine.Advertisements.ShowResult)
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m2290710958_MethodInfo_var;
extern const uint32_t Advertisement_DeliverResult_m1285565465_MetadataUsageId;
extern "C"  void Advertisement_DeliverResult_m1285565465 (Il2CppObject * __this /* static, unused */, int32_t ___result0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Advertisement_DeliverResult_m1285565465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		bool L_0 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_ResultDelivered_3();
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		ShowOptions_t1358843767 * L_1 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_ShowOptions_2();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		ShowOptions_t1358843767 * L_2 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_ShowOptions_2();
		NullCheck(L_2);
		Action_1_t258469394 * L_3 = ShowOptions_get_resultCallback_m3731275459(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->set_s_ResultDelivered_3((bool)1);
		ShowOptions_t1358843767 * L_4 = ((Advertisement_t3914199195_StaticFields*)Advertisement_t3914199195_il2cpp_TypeInfo_var->static_fields)->get_s_ShowOptions_2();
		NullCheck(L_4);
		Action_1_t258469394 * L_5 = ShowOptions_get_resultCallback_m3731275459(L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___result0;
		NullCheck(L_5);
		Action_1_Invoke_m2290710958(L_5, L_6, /*hidden argument*/Action_1_Invoke_m2290710958_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.AsyncExec::.cctor()
extern "C"  void AsyncExec__cctor_m2415655067 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.MonoBehaviour UnityEngine.Advertisements.AsyncExec::get_coroutineHost()
extern Il2CppClass* AsyncExec_t3964069003_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCoroutineHostMonoBehaviour_t291073519_m488405314_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3909281475;
extern const uint32_t AsyncExec_get_coroutineHost_m1572365565_MetadataUsageId;
extern "C"  MonoBehaviour_t1158329972 * AsyncExec_get_coroutineHost_m1572365565 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncExec_get_coroutineHost_m1572365565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncExec_t3964069003_il2cpp_TypeInfo_var);
		MonoBehaviour_t1158329972 * L_0 = ((AsyncExec_t3964069003_StaticFields*)AsyncExec_t3964069003_il2cpp_TypeInfo_var->static_fields)->get_s_CoroutineHost_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_2, _stringLiteral3909281475, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		Object_set_hideFlags_m2204253440(L_3, ((int32_t)63), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AsyncExec_t3964069003_il2cpp_TypeInfo_var);
		((AsyncExec_t3964069003_StaticFields*)AsyncExec_t3964069003_il2cpp_TypeInfo_var->static_fields)->set_s_GameObject_0(L_4);
		GameObject_t1756533147 * L_5 = ((AsyncExec_t3964069003_StaticFields*)AsyncExec_t3964069003_il2cpp_TypeInfo_var->static_fields)->get_s_GameObject_0();
		NullCheck(L_5);
		CoroutineHostMonoBehaviour_t291073519 * L_6 = GameObject_AddComponent_TisCoroutineHostMonoBehaviour_t291073519_m488405314(L_5, /*hidden argument*/GameObject_AddComponent_TisCoroutineHostMonoBehaviour_t291073519_m488405314_MethodInfo_var);
		((AsyncExec_t3964069003_StaticFields*)AsyncExec_t3964069003_il2cpp_TypeInfo_var->static_fields)->set_s_CoroutineHost_1(L_6);
		GameObject_t1756533147 * L_7 = ((AsyncExec_t3964069003_StaticFields*)AsyncExec_t3964069003_il2cpp_TypeInfo_var->static_fields)->get_s_GameObject_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncExec_t3964069003_il2cpp_TypeInfo_var);
		MonoBehaviour_t1158329972 * L_8 = ((AsyncExec_t3964069003_StaticFields*)AsyncExec_t3964069003_il2cpp_TypeInfo_var->static_fields)->get_s_CoroutineHost_1();
		return L_8;
	}
}
// UnityEngine.Coroutine UnityEngine.Advertisements.AsyncExec::StartCoroutine(System.Collections.IEnumerator)
extern Il2CppClass* AsyncExec_t3964069003_il2cpp_TypeInfo_var;
extern const uint32_t AsyncExec_StartCoroutine_m979833225_MetadataUsageId;
extern "C"  Coroutine_t2299508840 * AsyncExec_StartCoroutine_m979833225 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___enumerator0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AsyncExec_StartCoroutine_m979833225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AsyncExec_t3964069003_il2cpp_TypeInfo_var);
		MonoBehaviour_t1158329972 * L_0 = AsyncExec_get_coroutineHost_m1572365565(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___enumerator0;
		NullCheck(L_0);
		Coroutine_t2299508840 * L_2 = MonoBehaviour_StartCoroutine_m2470621050(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Advertisements.AsyncExec/CoroutineHostMonoBehaviour::.ctor()
extern "C"  void CoroutineHostMonoBehaviour__ctor_m3170070935 (CoroutineHostMonoBehaviour_t291073519 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern "C"  void ShowOptions__ctor_m3915763896 (ShowOptions_t1358843767 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern "C"  Action_1_t258469394 * ShowOptions_get_resultCallback_m3731275459 (ShowOptions_t1358843767 * __this, const MethodInfo* method)
{
	{
		Action_1_t258469394 * L_0 = __this->get_U3CresultCallbackU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern "C"  void ShowOptions_set_resultCallback_m3136348778 (ShowOptions_t1358843767 * __this, Action_1_t258469394 * ___value0, const MethodInfo* method)
{
	{
		Action_1_t258469394 * L_0 = ___value0;
		__this->set_U3CresultCallbackU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern "C"  String_t* ShowOptions_get_gamerSid_m3683233454 (ShowOptions_t1358843767 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_U3CgamerSidU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern "C"  void ShowOptions_set_gamerSid_m3133060487 (ShowOptions_t1358843767 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CgamerSidU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor::.ctor()
extern Il2CppCodeGenString* _stringLiteral2065683807;
extern const uint32_t UnityAdsEditor__ctor_m1232485421_MetadataUsageId;
extern "C"  void UnityAdsEditor__ctor_m1232485421 (UnityAdsEditor_t1440819734 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor__ctor_m1232485421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_CampaignDataUrl_4(_stringLiteral2065683807);
		UnityAdsPlatform__ctor_m1904688255(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor::RegisterNative(System.String)
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisUnityAdsEditorPlaceholder_t526501903_m1667733857_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2995424262;
extern const uint32_t UnityAdsEditor_RegisterNative_m1539375443_MetadataUsageId;
extern "C"  void UnityAdsEditor_RegisterNative_m1539375443 (UnityAdsEditor_t1440819734 * __this, String_t* ___extensionPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_RegisterNative_m1539375443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral2995424262, /*hidden argument*/NULL);
		V_1 = L_0;
		GameObject_t1756533147 * L_1 = V_1;
		NullCheck(L_1);
		Object_set_hideFlags_m2204253440(L_1, ((int32_t)63), /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = V_1;
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		UnityAdsEditorPlaceholder_t526501903 * L_5 = GameObject_AddComponent_TisUnityAdsEditorPlaceholder_t526501903_m1667733857(L_4, /*hidden argument*/GameObject_AddComponent_TisUnityAdsEditorPlaceholder_t526501903_m1667733857_MethodInfo_var);
		__this->set_m_Placeholder_3(L_5);
		UnityAdsEditorPlaceholder_t526501903 * L_6 = __this->get_m_Placeholder_3();
		String_t* L_7 = ___extensionPath0;
		NullCheck(L_6);
		UnityAdsEditorPlaceholder_Load_m1622005924(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor::Init(System.String,System.Boolean)
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_1_t2721744421_il2cpp_TypeInfo_var;
extern Il2CppClass* AsyncExec_t3964069003_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsEditor_HandleResponse_m3310519774_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m86573166_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2860320642;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral1308698338;
extern Il2CppCodeGenString* _stringLiteral3959783850;
extern Il2CppCodeGenString* _stringLiteral851273870;
extern const uint32_t UnityAdsEditor_Init_m1905247856_MetadataUsageId;
extern "C"  void UnityAdsEditor_Init_m1905247856 (UnityAdsEditor_t1440819734 * __this, String_t* ___gameId0, bool ___testModeEnabled1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_Init_m1905247856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t3614634134* L_0 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral2860320642);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2860320642);
		ObjectU5BU5D_t3614634134* L_1 = L_0;
		String_t* L_2 = ___gameId0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_2);
		ObjectU5BU5D_t3614634134* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral811305474);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral811305474);
		ObjectU5BU5D_t3614634134* L_4 = L_3;
		bool L_5 = ___testModeEnabled1;
		bool L_6 = L_5;
		Il2CppObject * L_7 = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_7);
		ObjectU5BU5D_t3614634134* L_8 = L_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, _stringLiteral1308698338);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1308698338);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3881798623(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_10 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		String_t* L_11 = __this->get_m_CampaignDataUrl_4();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_11);
		StringU5BU5D_t1642385972* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, _stringLiteral3959783850);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3959783850);
		StringU5BU5D_t1642385972* L_13 = L_12;
		String_t* L_14 = ___gameId0;
		String_t* L_15 = WWW_EscapeURL_m3653156031(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_15);
		StringU5BU5D_t1642385972* L_16 = L_13;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, _stringLiteral851273870);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral851273870);
		StringU5BU5D_t1642385972* L_17 = L_16;
		String_t* L_18 = Application_get_unityVersion_m3302058834(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_19 = WWW_EscapeURL_m3653156031(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_19);
		String_t* L_20 = String_Concat_m626692867(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_20;
		String_t* L_21 = V_0;
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)UnityAdsEditor_HandleResponse_m3310519774_MethodInfo_var);
		Action_1_t2721744421 * L_23 = (Action_1_t2721744421 *)il2cpp_codegen_object_new(Action_1_t2721744421_il2cpp_TypeInfo_var);
		Action_1__ctor_m86573166(L_23, __this, L_22, /*hidden argument*/Action_1__ctor_m86573166_MethodInfo_var);
		Il2CppObject * L_24 = UnityAdsEditor_GetAdPlan_m1254245195(__this, L_21, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AsyncExec_t3964069003_il2cpp_TypeInfo_var);
		AsyncExec_StartCoroutine_m979833225(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.Advertisements.UnityAdsEditor::GetAdPlan(System.String,System.Action`1<UnityEngine.WWW>)
extern Il2CppClass* U3CGetAdPlanU3Ec__Iterator0_t3446287961_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsEditor_GetAdPlan_m1254245195_MetadataUsageId;
extern "C"  Il2CppObject * UnityAdsEditor_GetAdPlan_m1254245195 (UnityAdsEditor_t1440819734 * __this, String_t* ___URL0, Action_1_t2721744421 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_GetAdPlan_m1254245195_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetAdPlanU3Ec__Iterator0_t3446287961 * V_0 = NULL;
	{
		U3CGetAdPlanU3Ec__Iterator0_t3446287961 * L_0 = (U3CGetAdPlanU3Ec__Iterator0_t3446287961 *)il2cpp_codegen_object_new(U3CGetAdPlanU3Ec__Iterator0_t3446287961_il2cpp_TypeInfo_var);
		U3CGetAdPlanU3Ec__Iterator0__ctor_m1630137333(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetAdPlanU3Ec__Iterator0_t3446287961 * L_1 = V_0;
		String_t* L_2 = ___URL0;
		NullCheck(L_1);
		L_1->set_URL_0(L_2);
		U3CGetAdPlanU3Ec__Iterator0_t3446287961 * L_3 = V_0;
		Action_1_t2721744421 * L_4 = ___callback1;
		NullCheck(L_3);
		L_3->set_callback_2(L_4);
		U3CGetAdPlanU3Ec__Iterator0_t3446287961 * L_5 = V_0;
		String_t* L_6 = ___URL0;
		NullCheck(L_5);
		L_5->set_U3CU24U3EURL_5(L_6);
		U3CGetAdPlanU3Ec__Iterator0_t3446287961 * L_7 = V_0;
		Action_1_t2721744421 * L_8 = ___callback1;
		NullCheck(L_7);
		L_7->set_U3CU24U3Ecallback_6(L_8);
		U3CGetAdPlanU3Ec__Iterator0_t3446287961 * L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor::HandleResponse(UnityEngine.WWW)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4250303425;
extern Il2CppCodeGenString* _stringLiteral1959740304;
extern Il2CppCodeGenString* _stringLiteral3466283536;
extern Il2CppCodeGenString* _stringLiteral2619694;
extern Il2CppCodeGenString* _stringLiteral70956859;
extern Il2CppCodeGenString* _stringLiteral3687085417;
extern Il2CppCodeGenString* _stringLiteral463191907;
extern Il2CppCodeGenString* _stringLiteral2816272419;
extern Il2CppCodeGenString* _stringLiteral682277685;
extern const uint32_t UnityAdsEditor_HandleResponse_m3310519774_MetadataUsageId;
extern "C"  void UnityAdsEditor_HandleResponse_m3310519774 (UnityAdsEditor_t1440819734 * __this, WWW_t2919945039 * ___www0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_HandleResponse_m3310519774_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		WWW_t2919945039 * L_0 = ___www0;
		NullCheck(L_0);
		String_t* L_1 = WWW_get_error_m3092701216(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral4250303425, /*hidden argument*/NULL);
		goto IL_013e;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_3 = Encoding_get_UTF8_m1752852937(NULL /*static, unused*/, /*hidden argument*/NULL);
		WWW_t2919945039 * L_4 = ___www0;
		NullCheck(L_4);
		ByteU5BU5D_t3397334013* L_5 = WWW_get_bytes_m420718112(L_4, /*hidden argument*/NULL);
		WWW_t2919945039 * L_6 = ___www0;
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_7 = WWW_get_bytes_m420718112(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		NullCheck(L_3);
		String_t* L_8 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t3397334013*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_5, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))));
		V_0 = L_8;
		String_t* L_9 = V_0;
		Il2CppObject * L_10 = SimpleJson_DeserializeObject_m4008938349(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Il2CppObject * L_11 = V_1;
		if (!((Il2CppObject*)IsInst(L_11, IDictionary_2_t2603311978_il2cpp_TypeInfo_var)))
		{
			goto IL_0119;
		}
	}
	{
		Il2CppObject * L_12 = V_1;
		V_2 = ((Il2CppObject*)Castclass(L_12, IDictionary_2_t2603311978_il2cpp_TypeInfo_var));
		Il2CppObject* L_13 = V_2;
		NullCheck(L_13);
		bool L_14 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_13, _stringLiteral1959740304);
		if (!L_14)
		{
			goto IL_0104;
		}
	}
	{
		Il2CppObject* L_15 = V_2;
		NullCheck(L_15);
		Il2CppObject * L_16 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_15, _stringLiteral1959740304);
		V_3 = ((String_t*)CastclassSealed(L_16, String_t_il2cpp_TypeInfo_var));
		String_t* L_17 = V_3;
		NullCheck(L_17);
		bool L_18 = String_Equals_m2633592423(L_17, _stringLiteral3466283536, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00d0;
		}
	}
	{
		Il2CppObject* L_19 = V_2;
		NullCheck(L_19);
		bool L_20 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_19, _stringLiteral2619694);
		if (!L_20)
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_21 = V_2;
		NullCheck(L_21);
		Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_21, _stringLiteral2619694);
		if (!((Il2CppObject*)IsInst(L_22, IDictionary_2_t2603311978_il2cpp_TypeInfo_var)))
		{
			goto IL_00c4;
		}
	}
	{
		Il2CppObject* L_23 = V_2;
		NullCheck(L_23);
		Il2CppObject * L_24 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_23, _stringLiteral2619694);
		Il2CppObject* L_25 = UnityAdsEditor_CollectZoneIds_m2096882927(__this, ((Il2CppObject*)Castclass(L_24, IDictionary_2_t2603311978_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_m_ZoneIds_1(L_25);
	}

IL_00c4:
	{
		__this->set_m_Ready_2((bool)1);
		goto IL_00ff;
	}

IL_00d0:
	{
		Il2CppObject* L_26 = V_2;
		NullCheck(L_26);
		bool L_27 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_26, _stringLiteral70956859);
		if (!L_27)
		{
			goto IL_00ff;
		}
	}
	{
		Il2CppObject* L_28 = V_2;
		NullCheck(L_28);
		Il2CppObject * L_29 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_28, _stringLiteral70956859);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3687085417, ((String_t*)CastclassSealed(L_29, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
	}

IL_00ff:
	{
		goto IL_0114;
	}

IL_0104:
	{
		String_t* L_31 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral463191907, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
	}

IL_0114:
	{
		goto IL_0129;
	}

IL_0119:
	{
		String_t* L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2816272419, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
	}

IL_0129:
	{
		bool L_35 = __this->get_m_Ready_2();
		if (L_35)
		{
			goto IL_013e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral682277685, /*hidden argument*/NULL);
	}

IL_013e:
	{
		return;
	}
}
// System.Collections.Generic.ICollection`1<System.String> UnityEngine.Advertisements.UnityAdsEditor::CollectZoneIds(System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern Il2CppClass* HashSet_1_t362681087_il2cpp_TypeInfo_var;
extern Il2CppClass* IDictionary_2_t2603311978_il2cpp_TypeInfo_var;
extern Il2CppClass* IList_1_t3230389896_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t2981576340_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t164973122_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* HashSet_1__ctor_m1317948930_MethodInfo_var;
extern const MethodInfo* HashSet_1_Add_m3545243818_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1601824589;
extern Il2CppCodeGenString* _stringLiteral287061489;
extern Il2CppCodeGenString* _stringLiteral3564902503;
extern const uint32_t UnityAdsEditor_CollectZoneIds_m2096882927_MetadataUsageId;
extern "C"  Il2CppObject* UnityAdsEditor_CollectZoneIds_m2096882927 (UnityAdsEditor_t1440819734 * __this, Il2CppObject* ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_CollectZoneIds_m2096882927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HashSet_1_t362681087 * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject* V_3 = NULL;
	Il2CppObject* V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		HashSet_1_t362681087 * L_0 = (HashSet_1_t362681087 *)il2cpp_codegen_object_new(HashSet_1_t362681087_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m1317948930(L_0, /*hidden argument*/HashSet_1__ctor_m1317948930_MethodInfo_var);
		V_0 = L_0;
		Il2CppObject* L_1 = ___data0;
		NullCheck(L_1);
		bool L_2 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_1, _stringLiteral1601824589);
		if (!L_2)
		{
			goto IL_00f5;
		}
	}
	{
		Il2CppObject* L_3 = ___data0;
		NullCheck(L_3);
		Il2CppObject * L_4 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_3, _stringLiteral1601824589);
		if (!((Il2CppObject*)IsInst(L_4, IList_1_t3230389896_il2cpp_TypeInfo_var)))
		{
			goto IL_00f5;
		}
	}
	{
		Il2CppObject* L_5 = ___data0;
		NullCheck(L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_5, _stringLiteral1601824589);
		V_1 = ((Il2CppObject*)Castclass(L_6, IList_1_t3230389896_il2cpp_TypeInfo_var));
		Il2CppObject* L_7 = V_1;
		NullCheck(L_7);
		Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IEnumerable_1_t2981576340_il2cpp_TypeInfo_var, L_7);
		V_3 = L_8;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00da;
		}

IL_0048:
		{
			Il2CppObject* L_9 = V_3;
			NullCheck(L_9);
			Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IEnumerator_1_t164973122_il2cpp_TypeInfo_var, L_9);
			V_2 = L_10;
			Il2CppObject * L_11 = V_2;
			V_4 = ((Il2CppObject*)Castclass(L_11, IDictionary_2_t2603311978_il2cpp_TypeInfo_var));
			Il2CppObject* L_12 = V_4;
			NullCheck(L_12);
			bool L_13 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_12, _stringLiteral287061489);
			if (!L_13)
			{
				goto IL_0096;
			}
		}

IL_0068:
		{
			Il2CppObject* L_14 = V_4;
			NullCheck(L_14);
			Il2CppObject * L_15 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_14, _stringLiteral287061489);
			if (!((String_t*)IsInstSealed(L_15, String_t_il2cpp_TypeInfo_var)))
			{
				goto IL_0096;
			}
		}

IL_007e:
		{
			HashSet_1_t362681087 * L_16 = V_0;
			Il2CppObject* L_17 = V_4;
			NullCheck(L_17);
			Il2CppObject * L_18 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_17, _stringLiteral287061489);
			NullCheck(L_16);
			HashSet_1_Add_m3545243818(L_16, ((String_t*)CastclassSealed(L_18, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/HashSet_1_Add_m3545243818_MethodInfo_var);
		}

IL_0096:
		{
			Il2CppObject* L_19 = V_4;
			NullCheck(L_19);
			bool L_20 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::ContainsKey(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_19, _stringLiteral3564902503);
			if (!L_20)
			{
				goto IL_00da;
			}
		}

IL_00a7:
		{
			Il2CppObject* L_21 = V_4;
			NullCheck(L_21);
			Il2CppObject * L_22 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_21, _stringLiteral3564902503);
			if (!((Il2CppObject *)IsInstSealed(L_22, Boolean_t3825574718_il2cpp_TypeInfo_var)))
			{
				goto IL_00da;
			}
		}

IL_00bd:
		{
			Il2CppObject* L_23 = V_4;
			NullCheck(L_23);
			Il2CppObject * L_24 = InterfaceFuncInvoker1< Il2CppObject *, String_t* >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Item(!0) */, IDictionary_2_t2603311978_il2cpp_TypeInfo_var, L_23, _stringLiteral3564902503);
			if (!((*(bool*)((bool*)UnBox (L_24, Boolean_t3825574718_il2cpp_TypeInfo_var)))))
			{
				goto IL_00da;
			}
		}

IL_00d3:
		{
			__this->set_m_HasDefaultZone_0((bool)1);
		}

IL_00da:
		{
			Il2CppObject* L_25 = V_3;
			NullCheck(L_25);
			bool L_26 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_25);
			if (L_26)
			{
				goto IL_0048;
			}
		}

IL_00e5:
		{
			IL2CPP_LEAVE(0xF5, FINALLY_00ea);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ea;
	}

FINALLY_00ea:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_27 = V_3;
			if (L_27)
			{
				goto IL_00ee;
			}
		}

IL_00ed:
		{
			IL2CPP_END_FINALLY(234)
		}

IL_00ee:
		{
			Il2CppObject* L_28 = V_3;
			NullCheck(L_28);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_28);
			IL2CPP_END_FINALLY(234)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(234)
	{
		IL2CPP_JUMP_TBL(0xF5, IL_00f5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00f5:
	{
		HashSet_1_t362681087 * L_29 = V_0;
		return L_29;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsEditor::CanShowAds(System.String)
extern Il2CppClass* ICollection_1_t2981295538_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsEditor_CanShowAds_m1564114482_MetadataUsageId;
extern "C"  bool UnityAdsEditor_CanShowAds_m1564114482 (UnityAdsEditor_t1440819734 * __this, String_t* ___zoneId0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_CanShowAds_m1564114482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___zoneId0;
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		String_t* L_1 = ___zoneId0;
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m1606060069(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		Il2CppObject* L_3 = __this->get_m_ZoneIds_1();
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		Il2CppObject* L_4 = __this->get_m_ZoneIds_1();
		String_t* L_5 = ___zoneId0;
		NullCheck(L_4);
		bool L_6 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.String>::Contains(!0) */, ICollection_1_t2981295538_il2cpp_TypeInfo_var, L_4, L_5);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		bool L_7 = __this->get_m_Ready_2();
		return L_7;
	}

IL_0035:
	{
		goto IL_004c;
	}

IL_003a:
	{
		bool L_8 = __this->get_m_HasDefaultZone_0();
		if (!L_8)
		{
			goto IL_004c;
		}
	}
	{
		bool L_9 = __this->get_m_Ready_2();
		return L_9;
	}

IL_004c:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor::SetLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern Il2CppClass* DebugLevel_t1591511100_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3302183760;
extern Il2CppCodeGenString* _stringLiteral1308698338;
extern const uint32_t UnityAdsEditor_SetLogLevel_m2764409263_MetadataUsageId;
extern "C"  void UnityAdsEditor_SetLogLevel_m2764409263 (UnityAdsEditor_t1440819734 * __this, int32_t ___logLevel0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_SetLogLevel_m2764409263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___logLevel0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(DebugLevel_t1591511100_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral3302183760, L_2, _stringLiteral1308698338, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor::SetCampaignDataURL(System.String)
extern "C"  void UnityAdsEditor_SetCampaignDataURL_m4255293782 (UnityAdsEditor_t1440819734 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url0;
		__this->set_m_CampaignDataUrl_4(L_0);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsEditor::Show(System.String,System.String)
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* ICollection_1_t2981295538_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4071561497;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral1308698338;
extern const uint32_t UnityAdsEditor_Show_m32614720_MetadataUsageId;
extern "C"  bool UnityAdsEditor_Show_m32614720 (UnityAdsEditor_t1440819734 * __this, String_t* ___zoneId0, String_t* ___gamerSid1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditor_Show_m32614720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral4071561497);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4071561497);
		StringU5BU5D_t1642385972* L_1 = L_0;
		String_t* L_2 = ___zoneId0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_2);
		StringU5BU5D_t1642385972* L_3 = L_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		ArrayElementTypeCheck (L_3, _stringLiteral811305474);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral811305474);
		StringU5BU5D_t1642385972* L_4 = L_3;
		String_t* L_5 = ___gamerSid1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 3);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		ArrayElementTypeCheck (L_6, _stringLiteral1308698338);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral1308698338);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m626692867(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_8 = __this->get_m_Ready_2();
		if (L_8)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		String_t* L_9 = ___zoneId0;
		if (!L_9)
		{
			goto IL_0072;
		}
	}
	{
		String_t* L_10 = ___zoneId0;
		NullCheck(L_10);
		int32_t L_11 = String_get_Length_m1606060069(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0072;
		}
	}
	{
		Il2CppObject* L_12 = __this->get_m_ZoneIds_1();
		if (!L_12)
		{
			goto IL_006d;
		}
	}
	{
		Il2CppObject* L_13 = __this->get_m_ZoneIds_1();
		String_t* L_14 = ___zoneId0;
		NullCheck(L_13);
		bool L_15 = InterfaceFuncInvoker1< bool, String_t* >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.String>::Contains(!0) */, ICollection_1_t2981295538_il2cpp_TypeInfo_var, L_13, L_14);
		if (L_15)
		{
			goto IL_006d;
		}
	}
	{
		return (bool)0;
	}

IL_006d:
	{
		goto IL_007f;
	}

IL_0072:
	{
		bool L_16 = __this->get_m_HasDefaultZone_0();
		if (L_16)
		{
			goto IL_007f;
		}
	}
	{
		return (bool)0;
	}

IL_007f:
	{
		UnityAdsEditorPlaceholder_t526501903 * L_17 = __this->get_m_Placeholder_3();
		NullCheck(L_17);
		UnityAdsEditorPlaceholder_Show_m908840621(L_17, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::.ctor()
extern "C"  void U3CGetAdPlanU3Ec__Iterator0__ctor_m1630137333 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGetAdPlanU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m240623603 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetAdPlanU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3377943867 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::MoveNext()
extern Il2CppClass* WWW_t2919945039_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3834207925_MethodInfo_var;
extern const uint32_t U3CGetAdPlanU3Ec__Iterator0_MoveNext_m1262663651_MetadataUsageId;
extern "C"  bool U3CGetAdPlanU3Ec__Iterator0_MoveNext_m1262663651 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAdPlanU3Ec__Iterator0_MoveNext_m1262663651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_0062;
	}

IL_0021:
	{
		String_t* L_2 = __this->get_URL_0();
		WWW_t2919945039 * L_3 = (WWW_t2919945039 *)il2cpp_codegen_object_new(WWW_t2919945039_il2cpp_TypeInfo_var);
		WWW__ctor_m2024029190(L_3, L_2, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_1(L_3);
		WWW_t2919945039 * L_4 = __this->get_U3CwwwU3E__0_1();
		__this->set_U24current_4(L_4);
		__this->set_U24PC_3(1);
		goto IL_0064;
	}

IL_004a:
	{
		Action_1_t2721744421 * L_5 = __this->get_callback_2();
		WWW_t2919945039 * L_6 = __this->get_U3CwwwU3E__0_1();
		NullCheck(L_5);
		Action_1_Invoke_m3834207925(L_5, L_6, /*hidden argument*/Action_1_Invoke_m3834207925_MethodInfo_var);
		__this->set_U24PC_3((-1));
	}

IL_0062:
	{
		return (bool)0;
	}

IL_0064:
	{
		return (bool)1;
	}
	// Dead block : IL_0066: ldloc.1
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::Dispose()
extern "C"  void U3CGetAdPlanU3Ec__Iterator0_Dispose_m2511147710 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditor/<GetAdPlan>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetAdPlanU3Ec__Iterator0_Reset_m2466990564_MetadataUsageId;
extern "C"  void U3CGetAdPlanU3Ec__Iterator0_Reset_m2466990564 (U3CGetAdPlanU3Ec__Iterator0_t3446287961 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetAdPlanU3Ec__Iterator0_Reset_m2466990564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::.ctor()
extern "C"  void UnityAdsEditorPlaceholder__ctor_m1414031528 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture2D UnityEngine.Advertisements.UnityAdsEditorPlaceholder::TextureFromFile(System.String)
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern const uint32_t UnityAdsEditorPlaceholder_TextureFromFile_m1806185628_MetadataUsageId;
extern "C"  Texture2D_t3542995729 * UnityAdsEditorPlaceholder_TextureFromFile_m1806185628 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditorPlaceholder_TextureFromFile_m1806185628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Texture2D_t3542995729 * V_0 = NULL;
	{
		Texture2D_t3542995729 * L_0 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_0, 1, 1, /*hidden argument*/NULL);
		V_0 = L_0;
		Texture2D_t3542995729 * L_1 = V_0;
		String_t* L_2 = ___path0;
		ByteU5BU5D_t3397334013* L_3 = File_ReadAllBytes_m4085527721(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Texture2D_LoadImage_m867542842(L_1, L_3, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::Load(System.String)
extern Il2CppClass* Path_t41728875_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral397631557;
extern Il2CppCodeGenString* _stringLiteral397991051;
extern const uint32_t UnityAdsEditorPlaceholder_Load_m1622005924_MetadataUsageId;
extern "C"  void UnityAdsEditorPlaceholder_Load_m1622005924 (UnityAdsEditorPlaceholder_t526501903 * __this, String_t* ___extensionPath0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditorPlaceholder_Load_m1622005924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___extensionPath0;
		IL2CPP_RUNTIME_CLASS_INIT(Path_t41728875_il2cpp_TypeInfo_var);
		String_t* L_1 = Path_Combine_m3185811654(NULL /*static, unused*/, L_0, _stringLiteral397631557, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_2 = UnityAdsEditorPlaceholder_TextureFromFile_m1806185628(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_m_PlaceholderLandscape_2(L_2);
		String_t* L_3 = ___extensionPath0;
		String_t* L_4 = Path_Combine_m3185811654(NULL /*static, unused*/, L_3, _stringLiteral397991051, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_5 = UnityAdsEditorPlaceholder_TextureFromFile_m1806185628(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_m_PlaceholderPortrait_3(L_5);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::WindowFunc(System.Int32)
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3033402446;
extern const uint32_t UnityAdsEditorPlaceholder_WindowFunc_m980111513_MetadataUsageId;
extern "C"  void UnityAdsEditorPlaceholder_WindowFunc_m980111513 (UnityAdsEditorPlaceholder_t526501903 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditorPlaceholder_WindowFunc_m980111513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m1220545469((&V_0), (0.0f), (0.0f), (((float)((float)L_0))), (((float)((float)L_1))), /*hidden argument*/NULL);
		Rect_t3681755626  L_2 = V_0;
		Texture2D_t3542995729 * L_3 = Texture2D_get_blackTexture_m3457275908(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m1921388893(NULL /*static, unused*/, L_2, L_3, 0, (bool)0, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_004b;
		}
	}
	{
		Rect_t3681755626  L_6 = V_0;
		Texture2D_t3542995729 * L_7 = __this->get_m_PlaceholderLandscape_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3284533624(NULL /*static, unused*/, L_6, L_7, 1, /*hidden argument*/NULL);
		goto IL_0058;
	}

IL_004b:
	{
		Rect_t3681755626  L_8 = V_0;
		Texture2D_t3542995729 * L_9 = __this->get_m_PlaceholderPortrait_3();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m3284533624(NULL /*static, unused*/, L_8, L_9, 1, /*hidden argument*/NULL);
	}

IL_0058:
	{
		int32_t L_10 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m1220545469(&L_11, (((float)((float)((int32_t)((int32_t)L_10-(int32_t)((int32_t)160)))))), (10.0f), (150.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		bool L_12 = GUI_Button_m3054448581(NULL /*static, unused*/, L_11, _stringLiteral3033402446, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008d;
		}
	}
	{
		UnityAdsEditorPlaceholder_Hide_m2240550894(__this, /*hidden argument*/NULL);
	}

IL_008d:
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::OnGUI()
extern Il2CppClass* WindowFunction_t3486805455_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsEditorPlaceholder_WindowFunc_m980111513_MethodInfo_var;
extern const uint32_t UnityAdsEditorPlaceholder_OnGUI_m1249441572_MetadataUsageId;
extern "C"  void UnityAdsEditorPlaceholder_OnGUI_m1249441572 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsEditorPlaceholder_OnGUI_m1249441572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_m_Showing_4();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		int32_t L_1 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Rect__ctor_m1220545469(&L_3, (0.0f), (0.0f), (((float)((float)L_1))), (((float)((float)L_2))), /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)UnityAdsEditorPlaceholder_WindowFunc_m980111513_MethodInfo_var);
		WindowFunction_t3486805455 * L_5 = (WindowFunction_t3486805455 *)il2cpp_codegen_object_new(WindowFunction_t3486805455_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m977095815(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_ModalWindow_m2619556590(NULL /*static, unused*/, 0, L_3, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::Show()
extern "C"  void UnityAdsEditorPlaceholder_Show_m908840621 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method)
{
	{
		__this->set_m_Showing_4((bool)1);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsEditorPlaceholder::Hide()
extern "C"  void UnityAdsEditorPlaceholder_Hide_m2240550894 (UnityAdsEditorPlaceholder_t526501903 * __this, const MethodInfo* method)
{
	{
		UnityAdsInternal_CallUnityAdsVideoCompleted_m2685251105(NULL /*static, unused*/, (String_t*)NULL, (bool)0, /*hidden argument*/NULL);
		UnityAdsInternal_CallUnityAdsHide_m1451410048(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_Showing_4((bool)0);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::.ctor()
extern "C"  void UnityAdsNative__ctor_m1896064941 (UnityAdsNative_t3929010778 * __this, const MethodInfo* method)
{
	{
		UnityAdsPlatform__ctor_m1904688255(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::RegisterNative(System.String)
extern "C"  void UnityAdsNative_RegisterNative_m1442116391 (UnityAdsNative_t3929010778 * __this, String_t* ___extensionPath0, const MethodInfo* method)
{
	{
		UnityAdsInternal_RegisterNative_m2502472975(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::Init(System.String,System.Boolean)
extern Il2CppClass* UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var;
extern Il2CppClass* Advertisement_t3914199195_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAdsNative_OnFetchCompleted_m1509886233_MethodInfo_var;
extern const MethodInfo* UnityAdsNative_OnFetchFailed_m2294113143_MethodInfo_var;
extern const uint32_t UnityAdsNative_Init_m503966922_MetadataUsageId;
extern "C"  void UnityAdsNative_Init_m503966922 (UnityAdsNative_t3929010778 * __this, String_t* ___gameId0, bool ___testModeEnabled1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsNative_Init_m503966922_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool G_B2_0 = false;
	String_t* G_B2_1 = NULL;
	bool G_B1_0 = false;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	bool G_B3_1 = false;
	String_t* G_B3_2 = NULL;
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UnityAdsNative_OnFetchCompleted_m1509886233_MethodInfo_var);
		UnityAdsDelegate_t3613839672 * L_1 = (UnityAdsDelegate_t3613839672 *)il2cpp_codegen_object_new(UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var);
		UnityAdsDelegate__ctor_m583892427(L_1, __this, L_0, /*hidden argument*/NULL);
		UnityAdsInternal_add_onCampaignsAvailable_m1879828001(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UnityAdsNative_OnFetchFailed_m2294113143_MethodInfo_var);
		UnityAdsDelegate_t3613839672 * L_3 = (UnityAdsDelegate_t3613839672 *)il2cpp_codegen_object_new(UnityAdsDelegate_t3613839672_il2cpp_TypeInfo_var);
		UnityAdsDelegate__ctor_m583892427(L_3, __this, L_2, /*hidden argument*/NULL);
		UnityAdsInternal_add_onCampaignsFetchFailed_m1707903351(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___gameId0;
		bool L_5 = ___testModeEnabled1;
		IL2CPP_RUNTIME_CLASS_INIT(Advertisement_t3914199195_il2cpp_TypeInfo_var);
		int32_t L_6 = Advertisement_get_debugLevel_m1094088667(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B1_0 = L_5;
		G_B1_1 = L_4;
		if (!((int32_t)((int32_t)L_6&(int32_t)8)))
		{
			G_B2_0 = L_5;
			G_B2_1 = L_4;
			goto IL_0036;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0037;
	}

IL_0036:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0037:
	{
		String_t* L_7 = Application_get_unityVersion_m3302058834(NULL /*static, unused*/, /*hidden argument*/NULL);
		UnityAdsInternal_Init_m600965261(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsNative::CanShowAds(System.String)
extern "C"  bool UnityAdsNative_CanShowAds_m819958408 (UnityAdsNative_t3929010778 * __this, String_t* ___zoneId0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_s_FetchCompleted_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_1 = ___zoneId0;
		bool L_2 = UnityAdsInternal_CanShowAds_m3383840268(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::SetLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C"  void UnityAdsNative_SetLogLevel_m382806635 (UnityAdsNative_t3929010778 * __this, int32_t ___logLevel0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___logLevel0;
		UnityAdsInternal_SetLogLevel_m1327512352(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::SetCampaignDataURL(System.String)
extern "C"  void UnityAdsNative_SetCampaignDataURL_m2763296484 (UnityAdsNative_t3929010778 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url0;
		UnityAdsInternal_SetCampaignDataURL_m1966380984(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsNative::Show(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3786898560;
extern const uint32_t UnityAdsNative_Show_m2987710586_MetadataUsageId;
extern "C"  bool UnityAdsNative_Show_m2987710586 (UnityAdsNative_t3929010778 * __this, String_t* ___zoneId0, String_t* ___gamerSid1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityAdsNative_Show_m2987710586_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = (String_t*)NULL;
		String_t* L_0 = ___gamerSid1;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		String_t* L_1 = ___gamerSid1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3786898560, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0014:
	{
		String_t* L_3 = ___zoneId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		String_t* L_5 = V_0;
		bool L_6 = UnityAdsInternal_Show_m3790165766(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		return (bool)1;
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::OnFetchCompleted()
extern "C"  void UnityAdsNative_OnFetchCompleted_m1509886233 (UnityAdsNative_t3929010778 * __this, const MethodInfo* method)
{
	{
		__this->set_s_FetchCompleted_0((bool)1);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsNative::OnFetchFailed()
extern "C"  void UnityAdsNative_OnFetchFailed_m2294113143 (UnityAdsNative_t3929010778 * __this, const MethodInfo* method)
{
	{
		__this->set_s_FetchCompleted_0((bool)0);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsPlatform::.ctor()
extern "C"  void UnityAdsPlatform__ctor_m1904688255 (UnityAdsPlatform_t714383836 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::.ctor()
extern "C"  void UnityAdsUnsupported__ctor_m1034993361 (UnityAdsUnsupported_t1914686426 * __this, const MethodInfo* method)
{
	{
		UnityAdsPlatform__ctor_m1904688255(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::RegisterNative(System.String)
extern "C"  void UnityAdsUnsupported_RegisterNative_m3985146115 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___extensionPath0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::Init(System.String,System.Boolean)
extern "C"  void UnityAdsUnsupported_Init_m144900996 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___gameId0, bool ___testModeEnabled1, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsUnsupported::CanShowAds(System.String)
extern "C"  bool UnityAdsUnsupported_CanShowAds_m756742030 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___zoneId0, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::SetCampaignDataURL(System.String)
extern "C"  void UnityAdsUnsupported_SetCampaignDataURL_m2292937510 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___url0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Advertisements.UnityAdsUnsupported::SetLogLevel(UnityEngine.Advertisements.Advertisement/DebugLevel)
extern "C"  void UnityAdsUnsupported_SetLogLevel_m4284147799 (UnityAdsUnsupported_t1914686426 * __this, int32_t ___logLevel0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Advertisements.UnityAdsUnsupported::Show(System.String,System.String)
extern "C"  bool UnityAdsUnsupported_Show_m886011964 (UnityAdsUnsupported_t1914686426 * __this, String_t* ___zoneId0, String_t* ___gamerSid1, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
