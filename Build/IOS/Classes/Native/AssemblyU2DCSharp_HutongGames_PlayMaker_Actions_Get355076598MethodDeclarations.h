﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo
struct GetRayCastHit2dInfo_t355076598;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::.ctor()
extern "C"  void GetRayCastHit2dInfo__ctor_m4191061356 (GetRayCastHit2dInfo_t355076598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::Reset()
extern "C"  void GetRayCastHit2dInfo_Reset_m2200358423 (GetRayCastHit2dInfo_t355076598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::OnEnter()
extern "C"  void GetRayCastHit2dInfo_OnEnter_m3272375135 (GetRayCastHit2dInfo_t355076598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::OnUpdate()
extern "C"  void GetRayCastHit2dInfo_OnUpdate_m264804418 (GetRayCastHit2dInfo_t355076598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRayCastHit2dInfo::StoreRaycastInfo()
extern "C"  void GetRayCastHit2dInfo_StoreRaycastInfo_m2427713534 (GetRayCastHit2dInfo_t355076598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
