﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.IsFixedAngle2d
struct IsFixedAngle2d_t4026586481;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::.ctor()
extern "C"  void IsFixedAngle2d__ctor_m2518458017 (IsFixedAngle2d_t4026586481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::Reset()
extern "C"  void IsFixedAngle2d_Reset_m2338867546 (IsFixedAngle2d_t4026586481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::OnEnter()
extern "C"  void IsFixedAngle2d_OnEnter_m1775214528 (IsFixedAngle2d_t4026586481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::OnUpdate()
extern "C"  void IsFixedAngle2d_OnUpdate_m2860226669 (IsFixedAngle2d_t4026586481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.IsFixedAngle2d::DoIsFixedAngle()
extern "C"  void IsFixedAngle2d_DoIsFixedAngle_m3555900671 (IsFixedAngle2d_t4026586481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
