﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListGetAverageValue
struct ArrayListGetAverageValue_t1272662253;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::.ctor()
extern "C"  void ArrayListGetAverageValue__ctor_m1181686023 (ArrayListGetAverageValue_t1272662253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::Reset()
extern "C"  void ArrayListGetAverageValue_Reset_m2018264694 (ArrayListGetAverageValue_t1272662253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::OnEnter()
extern "C"  void ArrayListGetAverageValue_OnEnter_m1850422440 (ArrayListGetAverageValue_t1272662253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::OnUpdate()
extern "C"  void ArrayListGetAverageValue_OnUpdate_m2061437743 (ArrayListGetAverageValue_t1272662253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::DoGetAverageValue()
extern "C"  void ArrayListGetAverageValue_DoGetAverageValue_m270333726 (ArrayListGetAverageValue_t1272662253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::<DoGetAverageValue>m__E7(System.Single,System.Single)
extern "C"  float ArrayListGetAverageValue_U3CDoGetAverageValueU3Em__E7_m1985124073 (Il2CppObject * __this /* static, unused */, float ___acc0, float ___cur1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
