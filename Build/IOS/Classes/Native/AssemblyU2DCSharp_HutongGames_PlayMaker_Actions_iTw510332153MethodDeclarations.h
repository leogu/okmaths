﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenScaleAdd
struct iTweenScaleAdd_t510332153;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::.ctor()
extern "C"  void iTweenScaleAdd__ctor_m1661825671 (iTweenScaleAdd_t510332153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::Reset()
extern "C"  void iTweenScaleAdd_Reset_m216767398 (iTweenScaleAdd_t510332153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::OnEnter()
extern "C"  void iTweenScaleAdd_OnEnter_m3601321504 (iTweenScaleAdd_t510332153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::OnExit()
extern "C"  void iTweenScaleAdd_OnExit_m31421464 (iTweenScaleAdd_t510332153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenScaleAdd::DoiTween()
extern "C"  void iTweenScaleAdd_DoiTween_m590758038 (iTweenScaleAdd_t510332153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
