﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipById
struct DOTweenControlMethodsFlipById_t3209836018;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipById::.ctor()
extern "C"  void DOTweenControlMethodsFlipById__ctor_m4148380624 (DOTweenControlMethodsFlipById_t3209836018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipById::Reset()
extern "C"  void DOTweenControlMethodsFlipById_Reset_m694253523 (DOTweenControlMethodsFlipById_t3209836018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipById::OnEnter()
extern "C"  void DOTweenControlMethodsFlipById_OnEnter_m455659691 (DOTweenControlMethodsFlipById_t3209836018 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
