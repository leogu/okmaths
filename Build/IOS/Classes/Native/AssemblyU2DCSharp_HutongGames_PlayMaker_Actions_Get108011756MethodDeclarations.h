﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetMaterialTexture
struct GetMaterialTexture_t108011756;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::.ctor()
extern "C"  void GetMaterialTexture__ctor_m3962383720 (GetMaterialTexture_t108011756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::Reset()
extern "C"  void GetMaterialTexture_Reset_m2276721457 (GetMaterialTexture_t108011756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::OnEnter()
extern "C"  void GetMaterialTexture_OnEnter_m1302900385 (GetMaterialTexture_t108011756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetMaterialTexture::DoGetMaterialTexture()
extern "C"  void GetMaterialTexture_DoGetMaterialTexture_m47888609 (GetMaterialTexture_t108011756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
