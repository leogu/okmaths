﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVertexCount
struct GetVertexCount_t2606393523;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::.ctor()
extern "C"  void GetVertexCount__ctor_m221414179 (GetVertexCount_t2606393523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::Reset()
extern "C"  void GetVertexCount_Reset_m408333092 (GetVertexCount_t2606393523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::OnEnter()
extern "C"  void GetVertexCount_OnEnter_m2284804914 (GetVertexCount_t2606393523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::OnUpdate()
extern "C"  void GetVertexCount_OnUpdate_m1156963019 (GetVertexCount_t2606393523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexCount::DoGetVertexCount()
extern "C"  void GetVertexCount_DoGetVertexCount_m3142374849 (GetVertexCount_t2606393523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
