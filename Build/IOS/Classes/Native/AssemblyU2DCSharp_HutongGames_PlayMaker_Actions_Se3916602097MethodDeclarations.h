﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmTexture
struct SetFsmTexture_t3916602097;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::.ctor()
extern "C"  void SetFsmTexture__ctor_m3321557795 (SetFsmTexture_t3916602097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::Reset()
extern "C"  void SetFsmTexture_Reset_m3897946446 (SetFsmTexture_t3916602097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::OnEnter()
extern "C"  void SetFsmTexture_OnEnter_m3299250232 (SetFsmTexture_t3916602097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::DoSetFsmBool()
extern "C"  void SetFsmTexture_DoSetFsmBool_m1021498382 (SetFsmTexture_t3916602097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmTexture::OnUpdate()
extern "C"  void SetFsmTexture_OnUpdate_m176137419 (SetFsmTexture_t3916602097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
