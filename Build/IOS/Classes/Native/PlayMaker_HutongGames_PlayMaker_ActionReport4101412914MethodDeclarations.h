﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.ActionReport
struct ActionReport_t4101412914;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_ActionReport4101412914.h"

// System.Void HutongGames.PlayMaker.ActionReport::Start()
extern "C"  void ActionReport_Start_m1406056297 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.ActionReport HutongGames.PlayMaker.ActionReport::Log(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String,System.String,System.Boolean)
extern "C"  ActionReport_t4101412914 * ActionReport_Log_m2707909432 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, FsmState_t1643911659 * ___state1, FsmStateAction_t2862378169 * ___action2, int32_t ___actionIndex3, String_t* ___parameter4, String_t* ___logLine5, bool ___isError6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionReport::ActionReportContains(HutongGames.PlayMaker.ActionReport)
extern "C"  bool ActionReport_ActionReportContains_m2228143982 (Il2CppObject * __this /* static, unused */, ActionReport_t4101412914 * ___report0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.ActionReport::SameAs(HutongGames.PlayMaker.ActionReport)
extern "C"  bool ActionReport_SameAs_m3895425975 (ActionReport_t4101412914 * __this, ActionReport_t4101412914 * ___actionReport0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::LogWarning(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String,System.String)
extern "C"  void ActionReport_LogWarning_m718383904 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, FsmState_t1643911659 * ___state1, FsmStateAction_t2862378169 * ___action2, int32_t ___actionIndex3, String_t* ___parameter4, String_t* ___logLine5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::LogError(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String,System.String)
extern "C"  void ActionReport_LogError_m2780875234 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, FsmState_t1643911659 * ___state1, FsmStateAction_t2862378169 * ___action2, int32_t ___actionIndex3, String_t* ___parameter4, String_t* ___logLine5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::LogError(PlayMakerFSM,HutongGames.PlayMaker.FsmState,HutongGames.PlayMaker.FsmStateAction,System.Int32,System.String)
extern "C"  void ActionReport_LogError_m1630940474 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, FsmState_t1643911659 * ___state1, FsmStateAction_t2862378169 * ___action2, int32_t ___actionIndex3, String_t* ___logLine4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::Clear()
extern "C"  void ActionReport_Clear_m3211339154 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::Remove(PlayMakerFSM)
extern "C"  void ActionReport_Remove_m284130547 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.ActionReport::GetCount()
extern "C"  int32_t ActionReport_GetCount_m3716369346 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::.ctor()
extern "C"  void ActionReport__ctor_m4180426745 (ActionReport_t4101412914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.ActionReport::.cctor()
extern "C"  void ActionReport__cctor_m142977174 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
