﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Com791040716.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SetCameraFOV
struct  SetCameraFOV_t2435366378  : public ComponentAction_1_t791040716
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SetCameraFOV::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.SetCameraFOV::fieldOfView
	FsmFloat_t937133978 * ___fieldOfView_14;
	// System.Boolean HutongGames.PlayMaker.Actions.SetCameraFOV::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(SetCameraFOV_t2435366378, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_fieldOfView_14() { return static_cast<int32_t>(offsetof(SetCameraFOV_t2435366378, ___fieldOfView_14)); }
	inline FsmFloat_t937133978 * get_fieldOfView_14() const { return ___fieldOfView_14; }
	inline FsmFloat_t937133978 ** get_address_of_fieldOfView_14() { return &___fieldOfView_14; }
	inline void set_fieldOfView_14(FsmFloat_t937133978 * value)
	{
		___fieldOfView_14 = value;
		Il2CppCodeGenWriteBarrier(&___fieldOfView_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(SetCameraFOV_t2435366378, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
