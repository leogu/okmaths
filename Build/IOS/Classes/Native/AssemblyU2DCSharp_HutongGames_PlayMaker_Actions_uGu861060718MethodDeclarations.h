﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSetColorBlock
struct uGuiSetColorBlock_t861060718;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSetColorBlock::.ctor()
extern "C"  void uGuiSetColorBlock__ctor_m1522386696 (uGuiSetColorBlock_t861060718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetColorBlock::Reset()
extern "C"  void uGuiSetColorBlock_Reset_m300480235 (uGuiSetColorBlock_t861060718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetColorBlock::OnEnter()
extern "C"  void uGuiSetColorBlock_OnEnter_m2691772755 (uGuiSetColorBlock_t861060718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetColorBlock::OnUpdate()
extern "C"  void uGuiSetColorBlock_OnUpdate_m1141776750 (uGuiSetColorBlock_t861060718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetColorBlock::DoSetValue()
extern "C"  void uGuiSetColorBlock_DoSetValue_m2947271378 (uGuiSetColorBlock_t861060718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetColorBlock::OnExit()
extern "C"  void uGuiSetColorBlock_OnExit_m2012500099 (uGuiSetColorBlock_t861060718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
