﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t16885852;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetEventProperties
struct  GetEventProperties_t2419155715  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.GetEventProperties::keys
	FsmStringU5BU5D_t2699231328* ___keys_11;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.GetEventProperties::datas
	FsmVarU5BU5D_t16885852* ___datas_12;

public:
	inline static int32_t get_offset_of_keys_11() { return static_cast<int32_t>(offsetof(GetEventProperties_t2419155715, ___keys_11)); }
	inline FsmStringU5BU5D_t2699231328* get_keys_11() const { return ___keys_11; }
	inline FsmStringU5BU5D_t2699231328** get_address_of_keys_11() { return &___keys_11; }
	inline void set_keys_11(FsmStringU5BU5D_t2699231328* value)
	{
		___keys_11 = value;
		Il2CppCodeGenWriteBarrier(&___keys_11, value);
	}

	inline static int32_t get_offset_of_datas_12() { return static_cast<int32_t>(offsetof(GetEventProperties_t2419155715, ___datas_12)); }
	inline FsmVarU5BU5D_t16885852* get_datas_12() const { return ___datas_12; }
	inline FsmVarU5BU5D_t16885852** get_address_of_datas_12() { return &___datas_12; }
	inline void set_datas_12(FsmVarU5BU5D_t16885852* value)
	{
		___datas_12 = value;
		Il2CppCodeGenWriteBarrier(&___datas_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
