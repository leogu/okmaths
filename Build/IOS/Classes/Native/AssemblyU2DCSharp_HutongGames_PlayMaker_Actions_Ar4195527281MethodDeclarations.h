﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListContainsGameObject
struct ArrayListContainsGameObject_t4195527281;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::.ctor()
extern "C"  void ArrayListContainsGameObject__ctor_m772495537 (ArrayListContainsGameObject_t4195527281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::Reset()
extern "C"  void ArrayListContainsGameObject_Reset_m3179063022 (ArrayListContainsGameObject_t4195527281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::OnEnter()
extern "C"  void ArrayListContainsGameObject_OnEnter_m1831377052 (ArrayListContainsGameObject_t4195527281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.Actions.ArrayListContainsGameObject::DoContainsGo()
extern "C"  int32_t ArrayListContainsGameObject_DoContainsGo_m3225903783 (ArrayListContainsGameObject_t4195527281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
