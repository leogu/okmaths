﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenMoveBy
struct iTweenMoveBy_t1856489068;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::.ctor()
extern "C"  void iTweenMoveBy__ctor_m2662217662 (iTweenMoveBy_t1856489068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::Reset()
extern "C"  void iTweenMoveBy_Reset_m49436637 (iTweenMoveBy_t1856489068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::OnEnter()
extern "C"  void iTweenMoveBy_OnEnter_m3299869053 (iTweenMoveBy_t1856489068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::OnExit()
extern "C"  void iTweenMoveBy_OnExit_m917777953 (iTweenMoveBy_t1856489068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenMoveBy::DoiTween()
extern "C"  void iTweenMoveBy_DoiTween_m1704270959 (iTweenMoveBy_t1856489068 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
