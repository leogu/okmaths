﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetFsmColor
struct SetFsmColor_t759142125;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::.ctor()
extern "C"  void SetFsmColor__ctor_m3456286517 (SetFsmColor_t759142125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::Reset()
extern "C"  void SetFsmColor_Reset_m3286521322 (SetFsmColor_t759142125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::OnEnter()
extern "C"  void SetFsmColor_OnEnter_m2607201608 (SetFsmColor_t759142125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::DoSetFsmColor()
extern "C"  void SetFsmColor_DoSetFsmColor_m3716858801 (SetFsmColor_t759142125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetFsmColor::OnUpdate()
extern "C"  void SetFsmColor_OnUpdate_m337105881 (SetFsmColor_t759142125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
