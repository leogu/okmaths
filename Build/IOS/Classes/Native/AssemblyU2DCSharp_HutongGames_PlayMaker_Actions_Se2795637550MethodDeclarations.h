﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetGameVolume
struct SetGameVolume_t2795637550;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::.ctor()
extern "C"  void SetGameVolume__ctor_m2638152974 (SetGameVolume_t2795637550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::Reset()
extern "C"  void SetGameVolume_Reset_m1496471747 (SetGameVolume_t2795637550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::OnEnter()
extern "C"  void SetGameVolume_OnEnter_m4218872491 (SetGameVolume_t2795637550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetGameVolume::OnUpdate()
extern "C"  void SetGameVolume_OnUpdate_m2201804856 (SetGameVolume_t2795637550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
