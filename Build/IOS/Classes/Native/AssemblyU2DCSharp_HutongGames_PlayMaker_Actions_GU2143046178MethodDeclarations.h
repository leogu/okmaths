﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutBeginCentered
struct GUILayoutBeginCentered_t2143046178;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::.ctor()
extern "C"  void GUILayoutBeginCentered__ctor_m3681078470 (GUILayoutBeginCentered_t2143046178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::Reset()
extern "C"  void GUILayoutBeginCentered_Reset_m1722260967 (GUILayoutBeginCentered_t2143046178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutBeginCentered::OnGUI()
extern "C"  void GUILayoutBeginCentered_OnGUI_m3498796470 (GUILayoutBeginCentered_t2143046178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
