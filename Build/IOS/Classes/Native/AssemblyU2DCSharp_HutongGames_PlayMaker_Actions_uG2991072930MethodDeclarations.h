﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit
struct uGuiInputFieldSetCharacterLimit_t2991072930;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit::.ctor()
extern "C"  void uGuiInputFieldSetCharacterLimit__ctor_m3621906470 (uGuiInputFieldSetCharacterLimit_t2991072930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit::Reset()
extern "C"  void uGuiInputFieldSetCharacterLimit_Reset_m739282363 (uGuiInputFieldSetCharacterLimit_t2991072930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit::OnEnter()
extern "C"  void uGuiInputFieldSetCharacterLimit_OnEnter_m800915763 (uGuiInputFieldSetCharacterLimit_t2991072930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit::OnUpdate()
extern "C"  void uGuiInputFieldSetCharacterLimit_OnUpdate_m3564422784 (uGuiInputFieldSetCharacterLimit_t2991072930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit::DoSetValue()
extern "C"  void uGuiInputFieldSetCharacterLimit_DoSetValue_m2869467092 (uGuiInputFieldSetCharacterLimit_t2991072930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldSetCharacterLimit::OnExit()
extern "C"  void uGuiInputFieldSetCharacterLimit_OnExit_m1862198267 (uGuiInputFieldSetCharacterLimit_t2991072930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
