﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled
struct uGuiInputFieldGetWasCanceled_t3459342752;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::.ctor()
extern "C"  void uGuiInputFieldGetWasCanceled__ctor_m3395049032 (uGuiInputFieldGetWasCanceled_t3459342752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::Reset()
extern "C"  void uGuiInputFieldGetWasCanceled_Reset_m3214097641 (uGuiInputFieldGetWasCanceled_t3459342752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::OnEnter()
extern "C"  void uGuiInputFieldGetWasCanceled_OnEnter_m2203008601 (uGuiInputFieldGetWasCanceled_t3459342752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetWasCanceled::DoGetValue()
extern "C"  void uGuiInputFieldGetWasCanceled_DoGetValue_m3921878742 (uGuiInputFieldGetWasCanceled_t3459342752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
