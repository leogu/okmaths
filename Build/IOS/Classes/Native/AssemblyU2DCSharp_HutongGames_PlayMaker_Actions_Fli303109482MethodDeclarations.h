﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Flicker
struct Flicker_t303109482;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Flicker::.ctor()
extern "C"  void Flicker__ctor_m3263033246 (Flicker_t303109482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Flicker::Reset()
extern "C"  void Flicker_Reset_m1350576035 (Flicker_t303109482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnEnter()
extern "C"  void Flicker_OnEnter_m1091180651 (Flicker_t303109482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Flicker::OnUpdate()
extern "C"  void Flicker_OnUpdate_m1853248216 (Flicker_t303109482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
