﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetStringValue
struct SetStringValue_t487249498;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetStringValue::.ctor()
extern "C"  void SetStringValue__ctor_m2122327814 (SetStringValue_t487249498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::Reset()
extern "C"  void SetStringValue_Reset_m679706727 (SetStringValue_t487249498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::OnEnter()
extern "C"  void SetStringValue_OnEnter_m1691622015 (SetStringValue_t487249498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::OnUpdate()
extern "C"  void SetStringValue_OnUpdate_m3491882568 (SetStringValue_t487249498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetStringValue::DoSetStringValue()
extern "C"  void SetStringValue_DoSetStringValue_m4260105057 (SetStringValue_t487249498 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
