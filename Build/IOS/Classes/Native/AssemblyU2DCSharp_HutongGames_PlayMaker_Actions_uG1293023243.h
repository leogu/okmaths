﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition605142169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiNavigationGetMode
struct  uGuiNavigationGetMode_t1293023243  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::navigationMode
	FsmString_t2414474701 * ___navigationMode_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::automaticEvent
	FsmEvent_t1258573736 * ___automaticEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::horizontalEvent
	FsmEvent_t1258573736 * ___horizontalEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::verticalEvent
	FsmEvent_t1258573736 * ___verticalEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::explicitEvent
	FsmEvent_t1258573736 * ___explicitEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::noNavigationEvent
	FsmEvent_t1258573736 * ___noNavigationEvent_17;
	// UnityEngine.UI.Selectable HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::_selectable
	Selectable_t1490392188 * ____selectable_18;
	// UnityEngine.UI.Selectable/Transition HutongGames.PlayMaker.Actions.uGuiNavigationGetMode::_originalTransition
	int32_t ____originalTransition_19;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_navigationMode_12() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___navigationMode_12)); }
	inline FsmString_t2414474701 * get_navigationMode_12() const { return ___navigationMode_12; }
	inline FsmString_t2414474701 ** get_address_of_navigationMode_12() { return &___navigationMode_12; }
	inline void set_navigationMode_12(FsmString_t2414474701 * value)
	{
		___navigationMode_12 = value;
		Il2CppCodeGenWriteBarrier(&___navigationMode_12, value);
	}

	inline static int32_t get_offset_of_automaticEvent_13() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___automaticEvent_13)); }
	inline FsmEvent_t1258573736 * get_automaticEvent_13() const { return ___automaticEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_automaticEvent_13() { return &___automaticEvent_13; }
	inline void set_automaticEvent_13(FsmEvent_t1258573736 * value)
	{
		___automaticEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___automaticEvent_13, value);
	}

	inline static int32_t get_offset_of_horizontalEvent_14() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___horizontalEvent_14)); }
	inline FsmEvent_t1258573736 * get_horizontalEvent_14() const { return ___horizontalEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_horizontalEvent_14() { return &___horizontalEvent_14; }
	inline void set_horizontalEvent_14(FsmEvent_t1258573736 * value)
	{
		___horizontalEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalEvent_14, value);
	}

	inline static int32_t get_offset_of_verticalEvent_15() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___verticalEvent_15)); }
	inline FsmEvent_t1258573736 * get_verticalEvent_15() const { return ___verticalEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_verticalEvent_15() { return &___verticalEvent_15; }
	inline void set_verticalEvent_15(FsmEvent_t1258573736 * value)
	{
		___verticalEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___verticalEvent_15, value);
	}

	inline static int32_t get_offset_of_explicitEvent_16() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___explicitEvent_16)); }
	inline FsmEvent_t1258573736 * get_explicitEvent_16() const { return ___explicitEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_explicitEvent_16() { return &___explicitEvent_16; }
	inline void set_explicitEvent_16(FsmEvent_t1258573736 * value)
	{
		___explicitEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___explicitEvent_16, value);
	}

	inline static int32_t get_offset_of_noNavigationEvent_17() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ___noNavigationEvent_17)); }
	inline FsmEvent_t1258573736 * get_noNavigationEvent_17() const { return ___noNavigationEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_noNavigationEvent_17() { return &___noNavigationEvent_17; }
	inline void set_noNavigationEvent_17(FsmEvent_t1258573736 * value)
	{
		___noNavigationEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___noNavigationEvent_17, value);
	}

	inline static int32_t get_offset_of__selectable_18() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ____selectable_18)); }
	inline Selectable_t1490392188 * get__selectable_18() const { return ____selectable_18; }
	inline Selectable_t1490392188 ** get_address_of__selectable_18() { return &____selectable_18; }
	inline void set__selectable_18(Selectable_t1490392188 * value)
	{
		____selectable_18 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_18, value);
	}

	inline static int32_t get_offset_of__originalTransition_19() { return static_cast<int32_t>(offsetof(uGuiNavigationGetMode_t1293023243, ____originalTransition_19)); }
	inline int32_t get__originalTransition_19() const { return ____originalTransition_19; }
	inline int32_t* get_address_of__originalTransition_19() { return &____originalTransition_19; }
	inline void set__originalTransition_19(int32_t value)
	{
		____originalTransition_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
