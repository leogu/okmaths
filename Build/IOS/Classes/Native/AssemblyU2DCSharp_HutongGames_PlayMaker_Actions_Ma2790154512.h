﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.MasterServerSetProperties
struct  MasterServerSetProperties_t2790154512  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.MasterServerSetProperties::ipAddress
	FsmString_t2414474701 * ___ipAddress_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerSetProperties::port
	FsmInt_t1273009179 * ___port_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.MasterServerSetProperties::updateRate
	FsmInt_t1273009179 * ___updateRate_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.MasterServerSetProperties::dedicatedServer
	FsmBool_t664485696 * ___dedicatedServer_14;

public:
	inline static int32_t get_offset_of_ipAddress_11() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t2790154512, ___ipAddress_11)); }
	inline FsmString_t2414474701 * get_ipAddress_11() const { return ___ipAddress_11; }
	inline FsmString_t2414474701 ** get_address_of_ipAddress_11() { return &___ipAddress_11; }
	inline void set_ipAddress_11(FsmString_t2414474701 * value)
	{
		___ipAddress_11 = value;
		Il2CppCodeGenWriteBarrier(&___ipAddress_11, value);
	}

	inline static int32_t get_offset_of_port_12() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t2790154512, ___port_12)); }
	inline FsmInt_t1273009179 * get_port_12() const { return ___port_12; }
	inline FsmInt_t1273009179 ** get_address_of_port_12() { return &___port_12; }
	inline void set_port_12(FsmInt_t1273009179 * value)
	{
		___port_12 = value;
		Il2CppCodeGenWriteBarrier(&___port_12, value);
	}

	inline static int32_t get_offset_of_updateRate_13() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t2790154512, ___updateRate_13)); }
	inline FsmInt_t1273009179 * get_updateRate_13() const { return ___updateRate_13; }
	inline FsmInt_t1273009179 ** get_address_of_updateRate_13() { return &___updateRate_13; }
	inline void set_updateRate_13(FsmInt_t1273009179 * value)
	{
		___updateRate_13 = value;
		Il2CppCodeGenWriteBarrier(&___updateRate_13, value);
	}

	inline static int32_t get_offset_of_dedicatedServer_14() { return static_cast<int32_t>(offsetof(MasterServerSetProperties_t2790154512, ___dedicatedServer_14)); }
	inline FsmBool_t664485696 * get_dedicatedServer_14() const { return ___dedicatedServer_14; }
	inline FsmBool_t664485696 ** get_address_of_dedicatedServer_14() { return &___dedicatedServer_14; }
	inline void set_dedicatedServer_14(FsmBool_t664485696 * value)
	{
		___dedicatedServer_14 = value;
		Il2CppCodeGenWriteBarrier(&___dedicatedServer_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
