﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>
struct DefaultComparer_t1390871639;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::.ctor()
extern "C"  void DefaultComparer__ctor_m4285977443_gshared (DefaultComparer_t1390871639 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4285977443(__this, method) ((  void (*) (DefaultComparer_t1390871639 *, const MethodInfo*))DefaultComparer__ctor_m4285977443_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1270057078_gshared (DefaultComparer_t1390871639 * __this, RaycastHit2D_t4063908774  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1270057078(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1390871639 *, RaycastHit2D_t4063908774 , const MethodInfo*))DefaultComparer_GetHashCode_m1270057078_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit2D>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2244573842_gshared (DefaultComparer_t1390871639 * __this, RaycastHit2D_t4063908774  ___x0, RaycastHit2D_t4063908774  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2244573842(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1390871639 *, RaycastHit2D_t4063908774 , RaycastHit2D_t4063908774 , const MethodInfo*))DefaultComparer_Equals_m2244573842_gshared)(__this, ___x0, ___y1, method)
