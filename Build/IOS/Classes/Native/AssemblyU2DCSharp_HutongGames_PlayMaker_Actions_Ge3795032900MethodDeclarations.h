﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight
struct GetAnimatorLayerWeight_t3795032900;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::.ctor()
extern "C"  void GetAnimatorLayerWeight__ctor_m1282956890 (GetAnimatorLayerWeight_t3795032900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::Reset()
extern "C"  void GetAnimatorLayerWeight_Reset_m2112766729 (GetAnimatorLayerWeight_t3795032900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnEnter()
extern "C"  void GetAnimatorLayerWeight_OnEnter_m47463057 (GetAnimatorLayerWeight_t3795032900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::OnActionUpdate()
extern "C"  void GetAnimatorLayerWeight_OnActionUpdate_m3545661046 (GetAnimatorLayerWeight_t3795032900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorLayerWeight::GetLayerWeight()
extern "C"  void GetAnimatorLayerWeight_GetLayerWeight_m3879903285 (GetAnimatorLayerWeight_t3795032900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
