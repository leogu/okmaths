﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetMaterialFloat
struct SetMaterialFloat_t2637247815;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::.ctor()
extern "C"  void SetMaterialFloat__ctor_m3840640055 (SetMaterialFloat_t2637247815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::Reset()
extern "C"  void SetMaterialFloat_Reset_m379792240 (SetMaterialFloat_t2637247815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::OnEnter()
extern "C"  void SetMaterialFloat_OnEnter_m2998688966 (SetMaterialFloat_t2637247815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::OnUpdate()
extern "C"  void SetMaterialFloat_OnUpdate_m373720663 (SetMaterialFloat_t2637247815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetMaterialFloat::DoSetMaterialFloat()
extern "C"  void SetMaterialFloat_DoSetMaterialFloat_m421058817 (SetMaterialFloat_t2637247815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
