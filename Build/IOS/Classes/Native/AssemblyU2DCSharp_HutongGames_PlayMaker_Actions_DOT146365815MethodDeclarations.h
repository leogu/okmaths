﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipAll
struct DOTweenControlMethodsFlipAll_t146365815;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipAll::.ctor()
extern "C"  void DOTweenControlMethodsFlipAll__ctor_m3053228297 (DOTweenControlMethodsFlipAll_t146365815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipAll::Reset()
extern "C"  void DOTweenControlMethodsFlipAll_Reset_m1607370632 (DOTweenControlMethodsFlipAll_t146365815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsFlipAll::OnEnter()
extern "C"  void DOTweenControlMethodsFlipAll_OnEnter_m2274040802 (DOTweenControlMethodsFlipAll_t146365815 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
