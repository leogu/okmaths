﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ColorRamp
struct ColorRamp_t820777147;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ColorRamp::.ctor()
extern "C"  void ColorRamp__ctor_m2445871597 (ColorRamp_t820777147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::Reset()
extern "C"  void ColorRamp_Reset_m2879558320 (ColorRamp_t820777147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::OnEnter()
extern "C"  void ColorRamp_OnEnter_m3000161986 (ColorRamp_t820777147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::OnUpdate()
extern "C"  void ColorRamp_OnUpdate_m711933089 (ColorRamp_t820777147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ColorRamp::DoColorRamp()
extern "C"  void ColorRamp_DoColorRamp_m3591102273 (ColorRamp_t820777147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.ColorRamp::ErrorCheck()
extern "C"  String_t* ColorRamp_ErrorCheck_m1561365970 (ColorRamp_t820777147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
