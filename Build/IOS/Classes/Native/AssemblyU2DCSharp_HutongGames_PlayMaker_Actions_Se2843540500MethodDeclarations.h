﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAudioPitch
struct SetAudioPitch_t2843540500;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::.ctor()
extern "C"  void SetAudioPitch__ctor_m128356330 (SetAudioPitch_t2843540500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::Reset()
extern "C"  void SetAudioPitch_Reset_m728827693 (SetAudioPitch_t2843540500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::OnEnter()
extern "C"  void SetAudioPitch_OnEnter_m2326387597 (SetAudioPitch_t2843540500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::OnUpdate()
extern "C"  void SetAudioPitch_OnUpdate_m920794084 (SetAudioPitch_t2843540500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAudioPitch::DoSetAudioPitch()
extern "C"  void SetAudioPitch_DoSetAudioPitch_m1394952541 (SetAudioPitch_t2843540500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
