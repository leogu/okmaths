﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableGetNext
struct HashTableGetNext_t738169715;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableGetNext::.ctor()
extern "C"  void HashTableGetNext__ctor_m2015460451 (HashTableGetNext_t738169715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetNext::Reset()
extern "C"  void HashTableGetNext_Reset_m3973197604 (HashTableGetNext_t738169715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetNext::OnEnter()
extern "C"  void HashTableGetNext_OnEnter_m2578238450 (HashTableGetNext_t738169715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetNext::DoGetNextItem()
extern "C"  void HashTableGetNext_DoGetNextItem_m2910545610 (HashTableGetNext_t738169715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableGetNext::GetItemAtIndex()
extern "C"  void HashTableGetNext_GetItemAtIndex_m1927709479 (HashTableGetNext_t738169715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
