﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutIntField
struct GUILayoutIntField_t1246352742;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::.ctor()
extern "C"  void GUILayoutIntField__ctor_m2769933144 (GUILayoutIntField_t1246352742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::Reset()
extern "C"  void GUILayoutIntField_Reset_m208073723 (GUILayoutIntField_t1246352742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutIntField::OnGUI()
extern "C"  void GUILayoutIntField_OnGUI_m4105719048 (GUILayoutIntField_t1246352742 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
