﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmMaterial[]
struct FsmMaterialU5BU5D_t1567627762;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.Utility.Arrays`1<HutongGames.PlayMaker.FsmMaterial>
struct  Arrays_1_t719354869  : public Il2CppObject
{
public:

public:
};

struct Arrays_1_t719354869_StaticFields
{
public:
	// T[] HutongGames.Utility.Arrays`1::Empty
	FsmMaterialU5BU5D_t1567627762* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Arrays_1_t719354869_StaticFields, ___Empty_0)); }
	inline FsmMaterialU5BU5D_t1567627762* get_Empty_0() const { return ___Empty_0; }
	inline FsmMaterialU5BU5D_t1567627762** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(FsmMaterialU5BU5D_t1567627762* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier(&___Empty_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
