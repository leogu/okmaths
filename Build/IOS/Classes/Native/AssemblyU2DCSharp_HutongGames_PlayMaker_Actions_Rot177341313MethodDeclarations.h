﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Rotate
struct Rotate_t177341313;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Rotate::.ctor()
extern "C"  void Rotate__ctor_m4265028563 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::Reset()
extern "C"  void Rotate_Reset_m3188657962 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnPreprocess()
extern "C"  void Rotate_OnPreprocess_m3604473370 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnEnter()
extern "C"  void Rotate_OnEnter_m2591993596 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnUpdate()
extern "C"  void Rotate_OnUpdate_m3140593083 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnLateUpdate()
extern "C"  void Rotate_OnLateUpdate_m1407208759 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::OnFixedUpdate()
extern "C"  void Rotate_OnFixedUpdate_m3824521313 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Rotate::DoRotate()
extern "C"  void Rotate_DoRotate_m4026921729 (Rotate_t177341313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
