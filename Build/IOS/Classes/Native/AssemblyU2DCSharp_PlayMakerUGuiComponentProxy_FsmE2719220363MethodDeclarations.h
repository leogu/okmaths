﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiComponentProxy/FsmEventSetup
struct FsmEventSetup_t2719220363;
struct FsmEventSetup_t2719220363_marshaled_pinvoke;
struct FsmEventSetup_t2719220363_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct FsmEventSetup_t2719220363;
struct FsmEventSetup_t2719220363_marshaled_pinvoke;

extern "C" void FsmEventSetup_t2719220363_marshal_pinvoke(const FsmEventSetup_t2719220363& unmarshaled, FsmEventSetup_t2719220363_marshaled_pinvoke& marshaled);
extern "C" void FsmEventSetup_t2719220363_marshal_pinvoke_back(const FsmEventSetup_t2719220363_marshaled_pinvoke& marshaled, FsmEventSetup_t2719220363& unmarshaled);
extern "C" void FsmEventSetup_t2719220363_marshal_pinvoke_cleanup(FsmEventSetup_t2719220363_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct FsmEventSetup_t2719220363;
struct FsmEventSetup_t2719220363_marshaled_com;

extern "C" void FsmEventSetup_t2719220363_marshal_com(const FsmEventSetup_t2719220363& unmarshaled, FsmEventSetup_t2719220363_marshaled_com& marshaled);
extern "C" void FsmEventSetup_t2719220363_marshal_com_back(const FsmEventSetup_t2719220363_marshaled_com& marshaled, FsmEventSetup_t2719220363& unmarshaled);
extern "C" void FsmEventSetup_t2719220363_marshal_com_cleanup(FsmEventSetup_t2719220363_marshaled_com& marshaled);
