﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayContains
struct ArrayContains_t1787837484;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayContains::.ctor()
extern "C"  void ArrayContains__ctor_m1084707658 (ArrayContains_t1787837484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::Reset()
extern "C"  void ArrayContains_Reset_m981026221 (ArrayContains_t1787837484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::OnEnter()
extern "C"  void ArrayContains_OnEnter_m1933212453 (ArrayContains_t1787837484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayContains::DoCheckContainsValue()
extern "C"  void ArrayContains_DoCheckContainsValue_m614213405 (ArrayContains_t1787837484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
