﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmArray
struct GetFsmArray_t4161723327;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::.ctor()
extern "C"  void GetFsmArray__ctor_m2697755061 (GetFsmArray_t4161723327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::Reset()
extern "C"  void GetFsmArray_Reset_m4275562816 (GetFsmArray_t4161723327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::OnEnter()
extern "C"  void GetFsmArray_OnEnter_m3687854858 (GetFsmArray_t4161723327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmArray::DoSetFsmArrayCopy()
extern "C"  void GetFsmArray_DoSetFsmArrayCopy_m1055653038 (GetFsmArray_t4161723327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
