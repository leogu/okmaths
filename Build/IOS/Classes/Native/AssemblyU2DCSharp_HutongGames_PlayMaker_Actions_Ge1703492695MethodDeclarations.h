﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmInt
struct GetFsmInt_t1703492695;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::.ctor()
extern "C"  void GetFsmInt__ctor_m1093273991 (GetFsmInt_t1703492695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::Reset()
extern "C"  void GetFsmInt_Reset_m3549831116 (GetFsmInt_t1703492695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::OnEnter()
extern "C"  void GetFsmInt_OnEnter_m2483598402 (GetFsmInt_t1703492695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::OnUpdate()
extern "C"  void GetFsmInt_OnUpdate_m2830826095 (GetFsmInt_t1703492695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmInt::DoGetFsmInt()
extern "C"  void GetFsmInt_DoGetFsmInt_m2616268541 (GetFsmInt_t1703492695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
