﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Object2689449295.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"

// System.String HutongGames.PlayMaker.NamedVariable::get_Name()
extern "C"  String_t* NamedVariable_get_Name_m3272978519 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_Name(System.String)
extern "C"  void NamedVariable_set_Name_m986462410 (NamedVariable_t3026441313 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_VariableType()
extern "C"  int32_t NamedVariable_get_VariableType_m282711752 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type HutongGames.PlayMaker.NamedVariable::get_ObjectType()
extern "C"  Type_t * NamedVariable_get_ObjectType_m1341654870 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_ObjectType(System.Type)
extern "C"  void NamedVariable_set_ObjectType_m3828140067 (NamedVariable_t3026441313 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.VariableType HutongGames.PlayMaker.NamedVariable::get_TypeConstraint()
extern "C"  int32_t NamedVariable_get_TypeConstraint_m1929913303 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_RawValue(System.Object)
extern "C"  void NamedVariable_set_RawValue_m4169278048 (NamedVariable_t3026441313 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HutongGames.PlayMaker.NamedVariable::get_RawValue()
extern "C"  Il2CppObject * NamedVariable_get_RawValue_m3724604771 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.NamedVariable::get_Tooltip()
extern "C"  String_t* NamedVariable_get_Tooltip_m2483673961 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_Tooltip(System.String)
extern "C"  void NamedVariable_set_Tooltip_m1829125866 (NamedVariable_t3026441313 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_UseVariable()
extern "C"  bool NamedVariable_get_UseVariable_m3382536130 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_UseVariable(System.Boolean)
extern "C"  void NamedVariable_set_UseVariable_m1478981231 (NamedVariable_t3026441313 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_ShowInInspector()
extern "C"  bool NamedVariable_get_ShowInInspector_m2658678066 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_ShowInInspector(System.Boolean)
extern "C"  void NamedVariable_set_ShowInInspector_m4186068365 (NamedVariable_t3026441313 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_NetworkSync()
extern "C"  bool NamedVariable_get_NetworkSync_m1993448044 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::set_NetworkSync(System.Boolean)
extern "C"  void NamedVariable_set_NetworkSync_m1181377551 (NamedVariable_t3026441313 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::IsNullOrNone(HutongGames.PlayMaker.NamedVariable)
extern "C"  bool NamedVariable_IsNullOrNone_m3626357025 (Il2CppObject * __this /* static, unused */, NamedVariable_t3026441313 * ___variable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_IsNone()
extern "C"  bool NamedVariable_get_IsNone_m1506066313 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_UsesVariable()
extern "C"  bool NamedVariable_get_UsesVariable_m2256255047 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor()
extern "C"  void NamedVariable__ctor_m2975824920 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor(System.String)
extern "C"  void NamedVariable__ctor_m837119430 (NamedVariable_t3026441313 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::.ctor(HutongGames.PlayMaker.NamedVariable)
extern "C"  void NamedVariable__ctor_m3431351199 (NamedVariable_t3026441313 * __this, NamedVariable_t3026441313 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::TestTypeConstraint(HutongGames.PlayMaker.VariableType,System.Type)
extern "C"  bool NamedVariable_TestTypeConstraint_m1677408666 (NamedVariable_t3026441313 * __this, int32_t ___variableType0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.NamedVariable::SafeAssign(System.Object)
extern "C"  void NamedVariable_SafeAssign_m2966400610 (NamedVariable_t3026441313 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.NamedVariable::Clone()
extern "C"  NamedVariable_t3026441313 * NamedVariable_Clone_m3808334231 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.NamedVariable::GetDisplayName()
extern "C"  String_t* NamedVariable_GetDisplayName_m367276684 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.NamedVariable::CompareTo(System.Object)
extern "C"  int32_t NamedVariable_CompareTo_m573141808 (NamedVariable_t3026441313 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
