﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUtils
struct PlayMakerUtils_t3437478729;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t2110469976;
// HutongGames.PlayMaker.FsmEventTarget
struct FsmEventTarget_t172293745;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData2110469976.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventTarget172293745.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVar2872592513.h"
#include "mscorlib_System_Object2689449295.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void PlayMakerUtils::.ctor()
extern "C"  void PlayMakerUtils__ctor_m4260438998 (PlayMakerUtils_t3437478729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::CreateIfNeededGlobalEvent(System.String)
extern "C"  void PlayMakerUtils_CreateIfNeededGlobalEvent_m282075293 (Il2CppObject * __this /* static, unused */, String_t* ___globalEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::SendEventToGameObject(PlayMakerFSM,UnityEngine.GameObject,System.String,System.Boolean)
extern "C"  void PlayMakerUtils_SendEventToGameObject_m1362121633 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fromFsm0, GameObject_t1756533147 * ___target1, String_t* ___fsmEvent2, bool ___includeChildren3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::SendEventToGameObject(PlayMakerFSM,UnityEngine.GameObject,System.String)
extern "C"  void PlayMakerUtils_SendEventToGameObject_m1816110972 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fromFsm0, GameObject_t1756533147 * ___target1, String_t* ___fsmEvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::SendEventToGameObject(PlayMakerFSM,UnityEngine.GameObject,System.String,HutongGames.PlayMaker.FsmEventData)
extern "C"  void PlayMakerUtils_SendEventToGameObject_m3841254642 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fromFsm0, GameObject_t1756533147 * ___target1, String_t* ___fsmEvent2, FsmEventData_t2110469976 * ___eventData3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::SendEventToGameObject(PlayMakerFSM,UnityEngine.GameObject,System.String,System.Boolean,HutongGames.PlayMaker.FsmEventData)
extern "C"  void PlayMakerUtils_SendEventToGameObject_m4011426003 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fromFsm0, GameObject_t1756533147 * ___target1, String_t* ___fsmEvent2, bool ___includeChildren3, FsmEventData_t2110469976 * ___eventData4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUtils::DoesTargetImplementsEvent(HutongGames.PlayMaker.FsmEventTarget,System.String)
extern "C"  bool PlayMakerUtils_DoesTargetImplementsEvent_m518695945 (Il2CppObject * __this /* static, unused */, FsmEventTarget_t172293745 * ___target0, String_t* ___eventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUtils::DoesGameObjectImplementsEvent(UnityEngine.GameObject,System.String)
extern "C"  bool PlayMakerUtils_DoesGameObjectImplementsEvent_m200556160 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___fsmEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUtils::DoesGameObjectImplementsEvent(UnityEngine.GameObject,System.String,System.String)
extern "C"  bool PlayMakerUtils_DoesGameObjectImplementsEvent_m662996864 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, String_t* ___fsmEvent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUtils::DoesFsmImplementsEvent(PlayMakerFSM,System.String)
extern "C"  bool PlayMakerUtils_DoesFsmImplementsEvent_m764596855 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, String_t* ___fsmEvent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent PlayMakerUtils::CreateGlobalEvent(System.String)
extern "C"  FsmEvent_t1258573736 * PlayMakerUtils_CreateGlobalEvent_m3258247020 (PlayMakerUtils_t3437478729 * __this, String_t* ___EventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent PlayMakerUtils::CreateGlobalEvent(System.String,System.Boolean&)
extern "C"  FsmEvent_t1258573736 * PlayMakerUtils_CreateGlobalEvent_m1263320841 (PlayMakerUtils_t3437478729 * __this, String_t* ___EventName0, bool* ___ExistsAlready1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayMakerFSM PlayMakerUtils::FindFsmOnGameObject(UnityEngine.GameObject,System.String)
extern "C"  PlayMakerFSM_t437737208 * PlayMakerUtils_FindFsmOnGameObject_m326063546 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUtils::RefreshValueFromFsmVar(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVar)
extern "C"  void PlayMakerUtils_RefreshValueFromFsmVar_m1575558100 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fromFsm0, FsmVar_t2872592513 * ___fsmVar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerUtils::GetValueFromFsmVar(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVar)
extern "C"  Il2CppObject * PlayMakerUtils_GetValueFromFsmVar_m544385582 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fromFsm0, FsmVar_t2872592513 * ___fsmVar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerUtils::ApplyValueToFsmVar(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVar,System.Object)
extern "C"  bool PlayMakerUtils_ApplyValueToFsmVar_m2679850424 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fromFsm0, FsmVar_t2872592513 * ___fsmVar1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PlayMakerUtils::GetFloatFromObject(System.Object,HutongGames.PlayMaker.VariableType,System.Boolean)
extern "C"  float PlayMakerUtils_GetFloatFromObject_m870716586 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____obj0, int32_t ___targetType1, bool ___fastProcessingIfPossible2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerUtils::ParseFsmVarToString(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVar)
extern "C"  String_t* PlayMakerUtils_ParseFsmVarToString_m3863861822 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, FsmVar_t2872592513 * ___fsmVar1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerUtils::ParseValueToString(System.Object,System.Boolean)
extern "C"  String_t* PlayMakerUtils_ParseValueToString_m395829646 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, bool ___useBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerUtils::ParseValueToString(System.Object)
extern "C"  String_t* PlayMakerUtils_ParseValueToString_m1954774955 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerUtils::ParseValueFromString(System.String,System.Boolean)
extern "C"  Il2CppObject * PlayMakerUtils_ParseValueFromString_m30575753 (Il2CppObject * __this /* static, unused */, String_t* ___source0, bool ___useBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerUtils::ParseValueFromString(System.String,HutongGames.PlayMaker.VariableType)
extern "C"  Il2CppObject * PlayMakerUtils_ParseValueFromString_m1727338492 (Il2CppObject * __this /* static, unused */, String_t* ___source0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerUtils::ParseValueFromString(System.String,System.Type)
extern "C"  Il2CppObject * PlayMakerUtils_ParseValueFromString_m3549205789 (Il2CppObject * __this /* static, unused */, String_t* ___source0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayMakerUtils::ParseValueFromString(System.String)
extern "C"  Il2CppObject * PlayMakerUtils_ParseValueFromString_m1239335176 (Il2CppObject * __this /* static, unused */, String_t* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
