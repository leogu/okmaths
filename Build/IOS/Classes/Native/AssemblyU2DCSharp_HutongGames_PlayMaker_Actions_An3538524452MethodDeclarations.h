﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateColor
struct AnimateColor_t3538524452;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateColor::.ctor()
extern "C"  void AnimateColor__ctor_m1006635802 (AnimateColor_t3538524452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::Reset()
extern "C"  void AnimateColor_Reset_m2452203689 (AnimateColor_t3538524452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::OnEnter()
extern "C"  void AnimateColor_OnEnter_m1081633441 (AnimateColor_t3538524452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::UpdateVariableValue()
extern "C"  void AnimateColor_UpdateVariableValue_m3102351160 (AnimateColor_t3538524452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateColor::OnUpdate()
extern "C"  void AnimateColor_OnUpdate_m3361490172 (AnimateColor_t3538524452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
