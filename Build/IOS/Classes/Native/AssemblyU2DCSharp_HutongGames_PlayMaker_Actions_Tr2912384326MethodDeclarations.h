﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.TransformPoint
struct TransformPoint_t2912384326;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.TransformPoint::.ctor()
extern "C"  void TransformPoint__ctor_m180237370 (TransformPoint_t2912384326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::Reset()
extern "C"  void TransformPoint_Reset_m3035323731 (TransformPoint_t2912384326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::OnEnter()
extern "C"  void TransformPoint_OnEnter_m1037977147 (TransformPoint_t2912384326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::OnUpdate()
extern "C"  void TransformPoint_OnUpdate_m3353228636 (TransformPoint_t2912384326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.TransformPoint::DoTransformPoint()
extern "C"  void TransformPoint_DoTransformPoint_m136186401 (TransformPoint_t2912384326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
