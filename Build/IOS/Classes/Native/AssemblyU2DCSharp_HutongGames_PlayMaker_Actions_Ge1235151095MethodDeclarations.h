﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetPreviousStateName
struct GetPreviousStateName_t1235151095;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::.ctor()
extern "C"  void GetPreviousStateName__ctor_m3431601077 (GetPreviousStateName_t1235151095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::Reset()
extern "C"  void GetPreviousStateName_Reset_m4228885604 (GetPreviousStateName_t1235151095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetPreviousStateName::OnEnter()
extern "C"  void GetPreviousStateName_OnEnter_m3358721182 (GetPreviousStateName_t1235151095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
