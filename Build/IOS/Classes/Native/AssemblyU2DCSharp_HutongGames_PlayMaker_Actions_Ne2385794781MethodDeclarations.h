﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties
struct NetworkGetConnectedPlayerProperties_t2385794781;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::.ctor()
extern "C"  void NetworkGetConnectedPlayerProperties__ctor_m661961195 (NetworkGetConnectedPlayerProperties_t2385794781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::Reset()
extern "C"  void NetworkGetConnectedPlayerProperties_Reset_m1478731318 (NetworkGetConnectedPlayerProperties_t2385794781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::OnEnter()
extern "C"  void NetworkGetConnectedPlayerProperties_OnEnter_m44369000 (NetworkGetConnectedPlayerProperties_t2385794781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectedPlayerProperties::getPlayerProperties()
extern "C"  void NetworkGetConnectedPlayerProperties_getPlayerProperties_m1204694851 (NetworkGetConnectedPlayerProperties_t2385794781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
