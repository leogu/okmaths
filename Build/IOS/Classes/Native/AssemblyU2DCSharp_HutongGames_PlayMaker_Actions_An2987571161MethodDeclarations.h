﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AnimateRect
struct AnimateRect_t2987571161;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AnimateRect::.ctor()
extern "C"  void AnimateRect__ctor_m678285297 (AnimateRect_t2987571161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::Reset()
extern "C"  void AnimateRect_Reset_m532606542 (AnimateRect_t2987571161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::OnEnter()
extern "C"  void AnimateRect_OnEnter_m2451651460 (AnimateRect_t2987571161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::UpdateVariableValue()
extern "C"  void AnimateRect_UpdateVariableValue_m1588906825 (AnimateRect_t2987571161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AnimateRect::OnUpdate()
extern "C"  void AnimateRect_OnUpdate_m2358151277 (AnimateRect_t2987571161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
