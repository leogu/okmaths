﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount
struct NetworkGetConnectionsCount_t1799155532;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::.ctor()
extern "C"  void NetworkGetConnectionsCount__ctor_m800395550 (NetworkGetConnectionsCount_t1799155532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::Reset()
extern "C"  void NetworkGetConnectionsCount_Reset_m3651670109 (NetworkGetConnectionsCount_t1799155532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::OnEnter()
extern "C"  void NetworkGetConnectionsCount_OnEnter_m398948045 (NetworkGetConnectionsCount_t1799155532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkGetConnectionsCount::OnUpdate()
extern "C"  void NetworkGetConnectionsCount_OnUpdate_m1563696816 (NetworkGetConnectionsCount_t1799155532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
