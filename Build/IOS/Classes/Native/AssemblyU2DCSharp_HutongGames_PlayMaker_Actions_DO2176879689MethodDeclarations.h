﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenAudioSourceFadeVolume
struct DOTweenAudioSourceFadeVolume_t2176879689;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourceFadeVolume::.ctor()
extern "C"  void DOTweenAudioSourceFadeVolume__ctor_m2770160129 (DOTweenAudioSourceFadeVolume_t2176879689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourceFadeVolume::Reset()
extern "C"  void DOTweenAudioSourceFadeVolume_Reset_m1324592634 (DOTweenAudioSourceFadeVolume_t2176879689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourceFadeVolume::OnEnter()
extern "C"  void DOTweenAudioSourceFadeVolume_OnEnter_m3186357208 (DOTweenAudioSourceFadeVolume_t2176879689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourceFadeVolume::<OnEnter>m__1E()
extern "C"  void DOTweenAudioSourceFadeVolume_U3COnEnterU3Em__1E_m3093629755 (DOTweenAudioSourceFadeVolume_t2176879689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenAudioSourceFadeVolume::<OnEnter>m__1F()
extern "C"  void DOTweenAudioSourceFadeVolume_U3COnEnterU3Em__1F_m1830073792 (DOTweenAudioSourceFadeVolume_t2176879689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
