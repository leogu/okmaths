﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// RadialLayout
struct RadialLayout_t3190219395;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties
struct  uGuiRadialLayoutSetProperties_t2821421258  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::fDistance
	FsmFloat_t937133978 * ___fDistance_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::minAngle
	FsmFloat_t937133978 * ___minAngle_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::maxAngle
	FsmFloat_t937133978 * ___maxAngle_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::startAngle
	FsmFloat_t937133978 * ___startAngle_15;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::everyFrame
	bool ___everyFrame_16;
	// RadialLayout HutongGames.PlayMaker.Actions.uGuiRadialLayoutSetProperties::_layoutElement
	RadialLayout_t3190219395 * ____layoutElement_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_fDistance_12() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ___fDistance_12)); }
	inline FsmFloat_t937133978 * get_fDistance_12() const { return ___fDistance_12; }
	inline FsmFloat_t937133978 ** get_address_of_fDistance_12() { return &___fDistance_12; }
	inline void set_fDistance_12(FsmFloat_t937133978 * value)
	{
		___fDistance_12 = value;
		Il2CppCodeGenWriteBarrier(&___fDistance_12, value);
	}

	inline static int32_t get_offset_of_minAngle_13() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ___minAngle_13)); }
	inline FsmFloat_t937133978 * get_minAngle_13() const { return ___minAngle_13; }
	inline FsmFloat_t937133978 ** get_address_of_minAngle_13() { return &___minAngle_13; }
	inline void set_minAngle_13(FsmFloat_t937133978 * value)
	{
		___minAngle_13 = value;
		Il2CppCodeGenWriteBarrier(&___minAngle_13, value);
	}

	inline static int32_t get_offset_of_maxAngle_14() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ___maxAngle_14)); }
	inline FsmFloat_t937133978 * get_maxAngle_14() const { return ___maxAngle_14; }
	inline FsmFloat_t937133978 ** get_address_of_maxAngle_14() { return &___maxAngle_14; }
	inline void set_maxAngle_14(FsmFloat_t937133978 * value)
	{
		___maxAngle_14 = value;
		Il2CppCodeGenWriteBarrier(&___maxAngle_14, value);
	}

	inline static int32_t get_offset_of_startAngle_15() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ___startAngle_15)); }
	inline FsmFloat_t937133978 * get_startAngle_15() const { return ___startAngle_15; }
	inline FsmFloat_t937133978 ** get_address_of_startAngle_15() { return &___startAngle_15; }
	inline void set_startAngle_15(FsmFloat_t937133978 * value)
	{
		___startAngle_15 = value;
		Il2CppCodeGenWriteBarrier(&___startAngle_15, value);
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of__layoutElement_17() { return static_cast<int32_t>(offsetof(uGuiRadialLayoutSetProperties_t2821421258, ____layoutElement_17)); }
	inline RadialLayout_t3190219395 * get__layoutElement_17() const { return ____layoutElement_17; }
	inline RadialLayout_t3190219395 ** get_address_of__layoutElement_17() { return &____layoutElement_17; }
	inline void set__layoutElement_17(RadialLayout_t3190219395 * value)
	{
		____layoutElement_17 = value;
		Il2CppCodeGenWriteBarrier(&____layoutElement_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
