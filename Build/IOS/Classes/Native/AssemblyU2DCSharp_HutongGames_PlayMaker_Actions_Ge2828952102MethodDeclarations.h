﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAnimatorIsHuman
struct GetAnimatorIsHuman_t2828952102;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::.ctor()
extern "C"  void GetAnimatorIsHuman__ctor_m1588682978 (GetAnimatorIsHuman_t2828952102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::Reset()
extern "C"  void GetAnimatorIsHuman_Reset_m3306621803 (GetAnimatorIsHuman_t2828952102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::OnEnter()
extern "C"  void GetAnimatorIsHuman_OnEnter_m3056148059 (GetAnimatorIsHuman_t2828952102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAnimatorIsHuman::DoCheckIsHuman()
extern "C"  void GetAnimatorIsHuman_DoCheckIsHuman_m1226542580 (GetAnimatorIsHuman_t2828952102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
