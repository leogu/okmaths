﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.HasFloatSliderAttribute
struct HasFloatSliderAttribute_t1179041011;

#include "codegen/il2cpp-codegen.h"

// System.Single HutongGames.PlayMaker.HasFloatSliderAttribute::get_MinValue()
extern "C"  float HasFloatSliderAttribute_get_MinValue_m2189049420 (HasFloatSliderAttribute_t1179041011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.HasFloatSliderAttribute::get_MaxValue()
extern "C"  float HasFloatSliderAttribute_get_MaxValue_m2741471762 (HasFloatSliderAttribute_t1179041011 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.HasFloatSliderAttribute::.ctor(System.Single,System.Single)
extern "C"  void HasFloatSliderAttribute__ctor_m2018298298 (HasFloatSliderAttribute_t1179041011 * __this, float ___minValue0, float ___maxValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
