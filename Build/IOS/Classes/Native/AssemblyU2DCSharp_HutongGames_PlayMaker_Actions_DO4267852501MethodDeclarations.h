﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsById
struct DOTweenControlMethodsPlayBackwardsById_t4267852501;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsById::.ctor()
extern "C"  void DOTweenControlMethodsPlayBackwardsById__ctor_m3765355061 (DOTweenControlMethodsPlayBackwardsById_t4267852501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsById::Reset()
extern "C"  void DOTweenControlMethodsPlayBackwardsById_Reset_m1155239750 (DOTweenControlMethodsPlayBackwardsById_t4267852501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenControlMethodsPlayBackwardsById::OnEnter()
extern "C"  void DOTweenControlMethodsPlayBackwardsById_OnEnter_m1192647828 (DOTweenControlMethodsPlayBackwardsById_t4267852501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
