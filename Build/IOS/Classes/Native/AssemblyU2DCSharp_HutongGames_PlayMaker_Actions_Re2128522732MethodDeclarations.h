﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis
struct RectTransformFlipLayoutAxis_t2128522732;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::.ctor()
extern "C"  void RectTransformFlipLayoutAxis__ctor_m630293492 (RectTransformFlipLayoutAxis_t2128522732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::Reset()
extern "C"  void RectTransformFlipLayoutAxis_Reset_m305232137 (RectTransformFlipLayoutAxis_t2128522732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::OnEnter()
extern "C"  void RectTransformFlipLayoutAxis_OnEnter_m3771549801 (RectTransformFlipLayoutAxis_t2128522732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformFlipLayoutAxis::DoFlip()
extern "C"  void RectTransformFlipLayoutAxis_DoFlip_m248721276 (RectTransformFlipLayoutAxis_t2128522732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
