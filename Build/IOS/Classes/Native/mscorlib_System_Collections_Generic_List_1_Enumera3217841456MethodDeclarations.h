﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1371230857(__this, ___l0, method) ((  void (*) (Enumerator_t3217841456 *, List_1_t3683111782 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m819965977(__this, method) ((  void (*) (Enumerator_t3217841456 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3433869337(__this, method) ((  Il2CppObject * (*) (Enumerator_t3217841456 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::Dispose()
#define Enumerator_Dispose_m2262207430(__this, method) ((  void (*) (Enumerator_t3217841456 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::VerifyState()
#define Enumerator_VerifyState_m2688939719(__this, method) ((  void (*) (Enumerator_t3217841456 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::MoveNext()
#define Enumerator_MoveNext_m2848443368(__this, method) ((  bool (*) (Enumerator_t3217841456 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmRect>::get_Current()
#define Enumerator_get_Current_m95964426(__this, method) ((  FsmRect_t19023354 * (*) (Enumerator_t3217841456 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
