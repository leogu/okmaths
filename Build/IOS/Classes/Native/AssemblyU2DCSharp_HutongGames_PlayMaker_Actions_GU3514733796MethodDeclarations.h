﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField
struct GUILayoutConfirmPasswordField_t3514733796;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::.ctor()
extern "C"  void GUILayoutConfirmPasswordField__ctor_m3726852760 (GUILayoutConfirmPasswordField_t3514733796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::Reset()
extern "C"  void GUILayoutConfirmPasswordField_Reset_m2764541725 (GUILayoutConfirmPasswordField_t3514733796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::OnGUI()
extern "C"  void GUILayoutConfirmPasswordField_OnGUI_m1813684448 (GUILayoutConfirmPasswordField_t3514733796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
