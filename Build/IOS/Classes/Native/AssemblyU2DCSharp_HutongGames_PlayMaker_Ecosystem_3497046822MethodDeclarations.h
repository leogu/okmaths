﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy
struct PlayMakerEventProxy_t3497046822;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy::.ctor()
extern "C"  void PlayMakerEventProxy__ctor_m3875466356 (PlayMakerEventProxy_t3497046822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventProxy::SendPlayMakerEvent()
extern "C"  void PlayMakerEventProxy_SendPlayMakerEvent_m342558254 (PlayMakerEventProxy_t3497046822 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
