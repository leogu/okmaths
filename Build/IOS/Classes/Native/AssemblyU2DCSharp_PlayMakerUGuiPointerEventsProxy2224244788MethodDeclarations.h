﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiPointerEventsProxy
struct PlayMakerUGuiPointerEventsProxy_t2224244788;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"

// System.Void PlayMakerUGuiPointerEventsProxy::.ctor()
extern "C"  void PlayMakerUGuiPointerEventsProxy__ctor_m3002166773 (PlayMakerUGuiPointerEventsProxy_t2224244788 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiPointerEventsProxy::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiPointerEventsProxy_OnPointerClick_m1027605133 (PlayMakerUGuiPointerEventsProxy_t2224244788 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiPointerEventsProxy::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiPointerEventsProxy_OnPointerDown_m3225199633 (PlayMakerUGuiPointerEventsProxy_t2224244788 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiPointerEventsProxy::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiPointerEventsProxy_OnPointerEnter_m860937603 (PlayMakerUGuiPointerEventsProxy_t2224244788 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiPointerEventsProxy::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiPointerEventsProxy_OnPointerExit_m1660895427 (PlayMakerUGuiPointerEventsProxy_t2224244788 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerUGuiPointerEventsProxy::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void PlayMakerUGuiPointerEventsProxy_OnPointerUp_m4052283232 (PlayMakerUGuiPointerEventsProxy_t2224244788 * __this, PointerEventData_t1599784723 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
