﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.FindArrayList
struct FindArrayList_t2738033168;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.FindArrayList::.ctor()
extern "C"  void FindArrayList__ctor_m1698083438 (FindArrayList_t2738033168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindArrayList::Reset()
extern "C"  void FindArrayList_Reset_m655563305 (FindArrayList_t2738033168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.FindArrayList::OnEnter()
extern "C"  void FindArrayList_OnEnter_m1291772041 (FindArrayList_t2738033168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
