﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiNavigationSetMode
struct uGuiNavigationSetMode_t1293698679;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::.ctor()
extern "C"  void uGuiNavigationSetMode__ctor_m2610646207 (uGuiNavigationSetMode_t1293698679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::Reset()
extern "C"  void uGuiNavigationSetMode_Reset_m712992628 (uGuiNavigationSetMode_t1293698679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::OnEnter()
extern "C"  void uGuiNavigationSetMode_OnEnter_m1961115538 (uGuiNavigationSetMode_t1293698679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::DoSetValue()
extern "C"  void uGuiNavigationSetMode_DoSetValue_m2523959825 (uGuiNavigationSetMode_t1293698679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiNavigationSetMode::OnExit()
extern "C"  void uGuiNavigationSetMode_OnExit_m3746019338 (uGuiNavigationSetMode_t1293698679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
