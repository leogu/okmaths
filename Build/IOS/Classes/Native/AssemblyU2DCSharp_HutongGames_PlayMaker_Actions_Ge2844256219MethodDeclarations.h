﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmObject
struct GetFsmObject_t2844256219;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::.ctor()
extern "C"  void GetFsmObject__ctor_m2317089381 (GetFsmObject_t2844256219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::Reset()
extern "C"  void GetFsmObject_Reset_m4003326732 (GetFsmObject_t2844256219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::OnEnter()
extern "C"  void GetFsmObject_OnEnter_m429561366 (GetFsmObject_t2844256219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::OnUpdate()
extern "C"  void GetFsmObject_OnUpdate_m115537985 (GetFsmObject_t2844256219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmObject::DoGetFsmVariable()
extern "C"  void GetFsmObject_DoGetFsmVariable_m785389892 (GetFsmObject_t2844256219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
