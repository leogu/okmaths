﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SequenceEvent
struct SequenceEvent_t1656293595;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::.ctor()
extern "C"  void SequenceEvent__ctor_m3744469381 (SequenceEvent_t1656293595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::Reset()
extern "C"  void SequenceEvent_Reset_m290330104 (SequenceEvent_t1656293595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::OnEnter()
extern "C"  void SequenceEvent_OnEnter_m10749298 (SequenceEvent_t1656293595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SequenceEvent::OnUpdate()
extern "C"  void SequenceEvent_OnUpdate_m2419077729 (SequenceEvent_t1656293595 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
