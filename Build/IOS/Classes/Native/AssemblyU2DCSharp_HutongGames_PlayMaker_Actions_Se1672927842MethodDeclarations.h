﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimationWeight
struct SetAnimationWeight_t1672927842;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::.ctor()
extern "C"  void SetAnimationWeight__ctor_m528230718 (SetAnimationWeight_t1672927842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::Reset()
extern "C"  void SetAnimationWeight_Reset_m2209560943 (SetAnimationWeight_t1672927842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::OnEnter()
extern "C"  void SetAnimationWeight_OnEnter_m1374682823 (SetAnimationWeight_t1672927842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::OnUpdate()
extern "C"  void SetAnimationWeight_OnUpdate_m108593792 (SetAnimationWeight_t1672927842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimationWeight::DoSetAnimationWeight(UnityEngine.GameObject)
extern "C"  void SetAnimationWeight_DoSetAnimationWeight_m722090061 (SetAnimationWeight_t1672927842 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
