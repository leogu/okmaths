﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetLightRange
struct SetLightRange_t2001034233;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetLightRange::.ctor()
extern "C"  void SetLightRange__ctor_m2905476165 (SetLightRange_t2001034233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::Reset()
extern "C"  void SetLightRange_Reset_m3469366610 (SetLightRange_t2001034233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::OnEnter()
extern "C"  void SetLightRange_OnEnter_m1543367320 (SetLightRange_t2001034233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::OnUpdate()
extern "C"  void SetLightRange_OnUpdate_m3546908521 (SetLightRange_t2001034233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetLightRange::DoSetLightRange()
extern "C"  void SetLightRange_DoSetLightRange_m1103041089 (SetLightRange_t2001034233 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
