﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DestroyArrayList
struct DestroyArrayList_t1040878829;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DestroyArrayList::.ctor()
extern "C"  void DestroyArrayList__ctor_m2249041083 (DestroyArrayList_t1040878829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyArrayList::Reset()
extern "C"  void DestroyArrayList_Reset_m2066205586 (DestroyArrayList_t1040878829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyArrayList::OnEnter()
extern "C"  void DestroyArrayList_OnEnter_m2975052452 (DestroyArrayList_t1040878829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DestroyArrayList::DoDestroyArrayList()
extern "C"  void DestroyArrayList_DoDestroyArrayList_m3649506241 (DestroyArrayList_t1040878829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
