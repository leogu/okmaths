﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListAddRange
struct ArrayListAddRange_t343917765;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListAddRange::.ctor()
extern "C"  void ArrayListAddRange__ctor_m2813178747 (ArrayListAddRange_t343917765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListAddRange::Reset()
extern "C"  void ArrayListAddRange_Reset_m3402157670 (ArrayListAddRange_t343917765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListAddRange::OnEnter()
extern "C"  void ArrayListAddRange_OnEnter_m2303079248 (ArrayListAddRange_t343917765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListAddRange::DoArrayListAddRange()
extern "C"  void ArrayListAddRange_DoArrayListAddRange_m1453609693 (ArrayListAddRange_t343917765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
