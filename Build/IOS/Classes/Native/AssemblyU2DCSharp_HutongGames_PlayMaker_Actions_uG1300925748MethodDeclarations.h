﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiSetSelectedGameObject
struct uGuiSetSelectedGameObject_t1300925748;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiSetSelectedGameObject::.ctor()
extern "C"  void uGuiSetSelectedGameObject__ctor_m1903582348 (uGuiSetSelectedGameObject_t1300925748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetSelectedGameObject::Reset()
extern "C"  void uGuiSetSelectedGameObject_Reset_m1578529937 (uGuiSetSelectedGameObject_t1300925748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetSelectedGameObject::OnEnter()
extern "C"  void uGuiSetSelectedGameObject_OnEnter_m2839872689 (uGuiSetSelectedGameObject_t1300925748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiSetSelectedGameObject::DoSetSelectedGameObject()
extern "C"  void uGuiSetSelectedGameObject_DoSetSelectedGameObject_m879103091 (uGuiSetSelectedGameObject_t1300925748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
