﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition
struct uGuiScrollRectSetNormalizedPosition_t405312737;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::.ctor()
extern "C"  void uGuiScrollRectSetNormalizedPosition__ctor_m3049441407 (uGuiScrollRectSetNormalizedPosition_t405312737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::Reset()
extern "C"  void uGuiScrollRectSetNormalizedPosition_Reset_m3797553442 (uGuiScrollRectSetNormalizedPosition_t405312737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::OnEnter()
extern "C"  void uGuiScrollRectSetNormalizedPosition_OnEnter_m3708695100 (uGuiScrollRectSetNormalizedPosition_t405312737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::OnUpdate()
extern "C"  void uGuiScrollRectSetNormalizedPosition_OnUpdate_m1946228023 (uGuiScrollRectSetNormalizedPosition_t405312737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::DoSetValue()
extern "C"  void uGuiScrollRectSetNormalizedPosition_DoSetValue_m4126032573 (uGuiScrollRectSetNormalizedPosition_t405312737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiScrollRectSetNormalizedPosition::OnExit()
extern "C"  void uGuiScrollRectSetNormalizedPosition_OnExit_m447304748 (uGuiScrollRectSetNormalizedPosition_t405312737 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
