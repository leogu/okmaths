﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition
struct RectTransformSetAnchorRectPosition_t4121253564;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::.ctor()
extern "C"  void RectTransformSetAnchorRectPosition__ctor_m1314473026 (RectTransformSetAnchorRectPosition_t4121253564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::Reset()
extern "C"  void RectTransformSetAnchorRectPosition_Reset_m239705569 (RectTransformSetAnchorRectPosition_t4121253564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::OnEnter()
extern "C"  void RectTransformSetAnchorRectPosition_OnEnter_m1441242697 (RectTransformSetAnchorRectPosition_t4121253564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::OnActionUpdate()
extern "C"  void RectTransformSetAnchorRectPosition_OnActionUpdate_m1398083006 (RectTransformSetAnchorRectPosition_t4121253564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformSetAnchorRectPosition::DoSetAnchor()
extern "C"  void RectTransformSetAnchorRectPosition_DoSetAnchor_m1596124636 (RectTransformSetAnchorRectPosition_t4121253564 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
