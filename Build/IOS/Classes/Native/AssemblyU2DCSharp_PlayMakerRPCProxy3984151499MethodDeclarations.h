﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerRPCProxy
struct PlayMakerRPCProxy_t3984151499;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayMakerRPCProxy::.ctor()
extern "C"  void PlayMakerRPCProxy__ctor_m1557776902 (PlayMakerRPCProxy_t3984151499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerRPCProxy::Reset()
extern "C"  void PlayMakerRPCProxy_Reset_m571375435 (PlayMakerRPCProxy_t3984151499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerRPCProxy::ForwardEvent(System.String)
extern "C"  void PlayMakerRPCProxy_ForwardEvent_m105203371 (PlayMakerRPCProxy_t3984151499 * __this, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
