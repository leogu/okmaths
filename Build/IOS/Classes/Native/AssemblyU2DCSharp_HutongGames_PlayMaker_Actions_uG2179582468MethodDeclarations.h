﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldDeactivate
struct uGuiInputFieldDeactivate_t2179582468;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldDeactivate::.ctor()
extern "C"  void uGuiInputFieldDeactivate__ctor_m2525442468 (uGuiInputFieldDeactivate_t2179582468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldDeactivate::Reset()
extern "C"  void uGuiInputFieldDeactivate_Reset_m566332749 (uGuiInputFieldDeactivate_t2179582468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldDeactivate::OnEnter()
extern "C"  void uGuiInputFieldDeactivate_OnEnter_m1236699501 (uGuiInputFieldDeactivate_t2179582468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldDeactivate::DoAction()
extern "C"  void uGuiInputFieldDeactivate_DoAction_m1702617521 (uGuiInputFieldDeactivate_t2179582468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldDeactivate::OnExit()
extern "C"  void uGuiInputFieldDeactivate_OnExit_m2272441497 (uGuiInputFieldDeactivate_t2179582468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
