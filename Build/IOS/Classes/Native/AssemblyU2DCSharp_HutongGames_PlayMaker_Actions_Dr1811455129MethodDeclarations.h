﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DrawDebugRay
struct DrawDebugRay_t1811455129;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::.ctor()
extern "C"  void DrawDebugRay__ctor_m2404228669 (DrawDebugRay_t1811455129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::Reset()
extern "C"  void DrawDebugRay_Reset_m686862630 (DrawDebugRay_t1811455129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DrawDebugRay::OnUpdate()
extern "C"  void DrawDebugRay_OnUpdate_m1862881545 (DrawDebugRay_t1811455129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
