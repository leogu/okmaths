﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax
struct RectTransformGetAnchorMax_t412541271;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::.ctor()
extern "C"  void RectTransformGetAnchorMax__ctor_m3708739145 (RectTransformGetAnchorMax_t412541271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::Reset()
extern "C"  void RectTransformGetAnchorMax_Reset_m1562752724 (RectTransformGetAnchorMax_t412541271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::OnEnter()
extern "C"  void RectTransformGetAnchorMax_OnEnter_m173764574 (RectTransformGetAnchorMax_t412541271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::OnActionUpdate()
extern "C"  void RectTransformGetAnchorMax_OnActionUpdate_m2957113583 (RectTransformGetAnchorMax_t412541271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetAnchorMax::DoGetValues()
extern "C"  void RectTransformGetAnchorMax_DoGetValues_m2350333466 (RectTransformGetAnchorMax_t412541271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
