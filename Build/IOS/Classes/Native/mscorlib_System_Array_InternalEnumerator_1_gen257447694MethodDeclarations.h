﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen257447694.h"
#include "mscorlib_System_Array3829468939.h"
#include "UnityEngine_UnityEngine_Resolution3693662728.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Resolution>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2658701886_gshared (InternalEnumerator_1_t257447694 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2658701886(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t257447694 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2658701886_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Resolution>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m543680782_gshared (InternalEnumerator_1_t257447694 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m543680782(__this, method) ((  void (*) (InternalEnumerator_1_t257447694 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m543680782_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Resolution>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1657111898_gshared (InternalEnumerator_1_t257447694 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1657111898(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t257447694 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1657111898_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Resolution>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3202313775_gshared (InternalEnumerator_1_t257447694 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3202313775(__this, method) ((  void (*) (InternalEnumerator_1_t257447694 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3202313775_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Resolution>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1304758022_gshared (InternalEnumerator_1_t257447694 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1304758022(__this, method) ((  bool (*) (InternalEnumerator_1_t257447694 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1304758022_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Resolution>::get_Current()
extern "C"  Resolution_t3693662728  InternalEnumerator_1_get_Current_m1658863423_gshared (InternalEnumerator_1_t257447694 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1658863423(__this, method) ((  Resolution_t3693662728  (*) (InternalEnumerator_1_t257447694 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1658863423_gshared)(__this, method)
