﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetSubstring
struct GetSubstring_t1243663789;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetSubstring::.ctor()
extern "C"  void GetSubstring__ctor_m1477694025 (GetSubstring_t1243663789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::Reset()
extern "C"  void GetSubstring_Reset_m2272528570 (GetSubstring_t1243663789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::OnEnter()
extern "C"  void GetSubstring_OnEnter_m2597235752 (GetSubstring_t1243663789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::OnUpdate()
extern "C"  void GetSubstring_OnUpdate_m406569013 (GetSubstring_t1243663789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetSubstring::DoGetSubstring()
extern "C"  void GetSubstring_DoGetSubstring_m752823809 (GetSubstring_t1243663789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
