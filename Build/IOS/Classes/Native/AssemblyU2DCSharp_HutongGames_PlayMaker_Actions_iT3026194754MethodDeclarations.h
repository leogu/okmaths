﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenLookTo
struct iTweenLookTo_t3026194754;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::.ctor()
extern "C"  void iTweenLookTo__ctor_m275694792 (iTweenLookTo_t3026194754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::Reset()
extern "C"  void iTweenLookTo_Reset_m3125325295 (iTweenLookTo_t3026194754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::OnEnter()
extern "C"  void iTweenLookTo_OnEnter_m2467698879 (iTweenLookTo_t3026194754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::OnExit()
extern "C"  void iTweenLookTo_OnExit_m803853935 (iTweenLookTo_t3026194754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenLookTo::DoiTween()
extern "C"  void iTweenLookTo_DoiTween_m1565331541 (iTweenLookTo_t3026194754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
