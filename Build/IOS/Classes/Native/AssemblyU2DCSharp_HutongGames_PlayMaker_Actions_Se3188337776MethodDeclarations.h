﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetEventData
struct SetEventData_t3188337776;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetEventData::.ctor()
extern "C"  void SetEventData__ctor_m222996312 (SetEventData_t3188337776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventData::Reset()
extern "C"  void SetEventData_Reset_m1943603545 (SetEventData_t3188337776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetEventData::OnEnter()
extern "C"  void SetEventData_OnEnter_m3925395097 (SetEventData_t3188337776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
