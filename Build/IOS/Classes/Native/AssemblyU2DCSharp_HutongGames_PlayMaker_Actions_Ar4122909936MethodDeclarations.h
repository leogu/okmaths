﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListActions
struct ArrayListActions_t4122909936;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PlayMakerArrayListProxy
struct PlayMakerArrayListProxy_t4012441185;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PlayMakerArrayListProxy4012441185.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListActions::.ctor()
extern "C"  void ArrayListActions__ctor_m974446384 (ArrayListActions_t4122909936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ArrayListActions::SetUpArrayListProxyPointer(UnityEngine.GameObject,System.String)
extern "C"  bool ArrayListActions_SetUpArrayListProxyPointer_m347570637 (ArrayListActions_t4122909936 * __this, GameObject_t1756533147 * ___aProxyGO0, String_t* ___nameReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ArrayListActions::SetUpArrayListProxyPointer(PlayMakerArrayListProxy,System.String)
extern "C"  bool ArrayListActions_SetUpArrayListProxyPointer_m1309064168 (ArrayListActions_t4122909936 * __this, PlayMakerArrayListProxy_t4012441185 * ___aProxy0, String_t* ___nameReference1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.ArrayListActions::isProxyValid()
extern "C"  bool ArrayListActions_isProxyValid_m1947906978 (ArrayListActions_t4122909936 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
