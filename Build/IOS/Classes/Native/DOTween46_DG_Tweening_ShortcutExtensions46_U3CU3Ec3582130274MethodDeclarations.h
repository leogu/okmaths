﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_t3582130274;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass32_0__ctor_m2531827733 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
