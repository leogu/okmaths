﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.AddTorque
struct AddTorque_t3721276289;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.AddTorque::.ctor()
extern "C"  void AddTorque__ctor_m1486867271 (AddTorque_t3721276289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::Reset()
extern "C"  void AddTorque_Reset_m3877986938 (AddTorque_t3721276289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnPreprocess()
extern "C"  void AddTorque_OnPreprocess_m1964449770 (AddTorque_t3721276289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnEnter()
extern "C"  void AddTorque_OnEnter_m4141236892 (AddTorque_t3721276289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::OnFixedUpdate()
extern "C"  void AddTorque_OnFixedUpdate_m2758449409 (AddTorque_t3721276289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.AddTorque::DoAddTorque()
extern "C"  void AddTorque_DoAddTorque_m1115080525 (AddTorque_t3721276289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
