﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CallMethod
struct CallMethod_t2249460863;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CallMethod::.ctor()
extern "C"  void CallMethod__ctor_m2223935403 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::Reset()
extern "C"  void CallMethod_Reset_m3295172652 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::OnEnter()
extern "C"  void CallMethod_OnEnter_m385665850 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::OnUpdate()
extern "C"  void CallMethod_OnUpdate_m2312292995 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::DoMethodCall()
extern "C"  void CallMethod_DoMethodCall_m3307750337 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.CallMethod::NeedToUpdateCache()
extern "C"  bool CallMethod_NeedToUpdateCache_m273467767 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallMethod::ClearCache()
extern "C"  void CallMethod_ClearCache_m2177851142 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.CallMethod::DoCache()
extern "C"  bool CallMethod_DoCache_m4102466376 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.CallMethod::ErrorCheck()
extern "C"  String_t* CallMethod_ErrorCheck_m2768516448 (CallMethod_t2249460863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
