﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2750215200(__this, ___l0, method) ((  void (*) (Enumerator_t2000993669 *, List_1_t2466263995 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1846915874(__this, method) ((  void (*) (Enumerator_t2000993669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3277889470(__this, method) ((  Il2CppObject * (*) (Enumerator_t2000993669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::Dispose()
#define Enumerator_Dispose_m1342161395(__this, method) ((  void (*) (Enumerator_t2000993669 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::VerifyState()
#define Enumerator_VerifyState_m924716502(__this, method) ((  void (*) (Enumerator_t2000993669 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::MoveNext()
#define Enumerator_MoveNext_m2766937425(__this, method) ((  bool (*) (Enumerator_t2000993669 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HutongGames.PlayMaker.FsmGameObject>::get_Current()
#define Enumerator_get_Current_m4219000449(__this, method) ((  FsmGameObject_t3097142863 * (*) (Enumerator_t2000993669 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
