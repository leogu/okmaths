﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetAxis
struct GetAxis_t4245882913;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetAxis::.ctor()
extern "C"  void GetAxis__ctor_m505317715 (GetAxis_t4245882913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::Reset()
extern "C"  void GetAxis_Reset_m1144265918 (GetAxis_t4245882913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::OnEnter()
extern "C"  void GetAxis_OnEnter_m648353832 (GetAxis_t4245882913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::OnUpdate()
extern "C"  void GetAxis_OnUpdate_m3723663899 (GetAxis_t4245882913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetAxis::DoGetAxis()
extern "C"  void GetAxis_DoGetAxis_m3940807613 (GetAxis_t4245882913 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
