﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableSetMany
struct HashTableSetMany_t834694381;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableSetMany::.ctor()
extern "C"  void HashTableSetMany__ctor_m1147201575 (HashTableSetMany_t834694381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableSetMany::Reset()
extern "C"  void HashTableSetMany_Reset_m73224566 (HashTableSetMany_t834694381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableSetMany::OnEnter()
extern "C"  void HashTableSetMany_OnEnter_m2851115416 (HashTableSetMany_t834694381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableSetMany::SetHashTable()
extern "C"  void HashTableSetMany_SetHashTable_m3521862317 (HashTableSetMany_t834694381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
