﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta
struct RectTransformGetSizeDelta_t2082322359;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::.ctor()
extern "C"  void RectTransformGetSizeDelta__ctor_m2441997321 (RectTransformGetSizeDelta_t2082322359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::Reset()
extern "C"  void RectTransformGetSizeDelta_Reset_m538137780 (RectTransformGetSizeDelta_t2082322359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::OnEnter()
extern "C"  void RectTransformGetSizeDelta_OnEnter_m1299974926 (RectTransformGetSizeDelta_t2082322359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::OnActionUpdate()
extern "C"  void RectTransformGetSizeDelta_OnActionUpdate_m532985407 (RectTransformGetSizeDelta_t2082322359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformGetSizeDelta::DoGetValues()
extern "C"  void RectTransformGetSizeDelta_DoGetValues_m2050272218 (RectTransformGetSizeDelta_t2082322359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
