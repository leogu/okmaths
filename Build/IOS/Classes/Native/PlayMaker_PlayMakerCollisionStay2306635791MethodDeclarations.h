﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerCollisionStay
struct PlayMakerCollisionStay_t2306635791;
// UnityEngine.Collision
struct Collision_t2876846408;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"

// System.Void PlayMakerCollisionStay::OnCollisionStay(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionStay_OnCollisionStay_m2505937539 (PlayMakerCollisionStay_t2306635791 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerCollisionStay::.ctor()
extern "C"  void PlayMakerCollisionStay__ctor_m508464772 (PlayMakerCollisionStay_t2306635791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
