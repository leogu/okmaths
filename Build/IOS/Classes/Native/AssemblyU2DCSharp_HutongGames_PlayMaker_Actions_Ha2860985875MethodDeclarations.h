﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableIsEmpty
struct HashTableIsEmpty_t2860985875;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableIsEmpty::.ctor()
extern "C"  void HashTableIsEmpty__ctor_m633626869 (HashTableIsEmpty_t2860985875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableIsEmpty::Reset()
extern "C"  void HashTableIsEmpty_Reset_m4130791068 (HashTableIsEmpty_t2860985875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableIsEmpty::OnEnter()
extern "C"  void HashTableIsEmpty_OnEnter_m723259278 (HashTableIsEmpty_t2860985875 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
