﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.ArrayListSortGameObjectByDistance
struct ArrayListSortGameObjectByDistance_t3967778432;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.ArrayListSortGameObjectByDistance::.ctor()
extern "C"  void ArrayListSortGameObjectByDistance__ctor_m1883097320 (ArrayListSortGameObjectByDistance_t3967778432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSortGameObjectByDistance::Reset()
extern "C"  void ArrayListSortGameObjectByDistance_Reset_m1544993877 (ArrayListSortGameObjectByDistance_t3967778432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSortGameObjectByDistance::OnEnter()
extern "C"  void ArrayListSortGameObjectByDistance_OnEnter_m1147730765 (ArrayListSortGameObjectByDistance_t3967778432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSortGameObjectByDistance::OnUpdate()
extern "C"  void ArrayListSortGameObjectByDistance_OnUpdate_m300880934 (ArrayListSortGameObjectByDistance_t3967778432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.ArrayListSortGameObjectByDistance::DoSortByDistance()
extern "C"  void ArrayListSortGameObjectByDistance_DoSortByDistance_m3296889581 (ArrayListSortGameObjectByDistance_t3967778432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
