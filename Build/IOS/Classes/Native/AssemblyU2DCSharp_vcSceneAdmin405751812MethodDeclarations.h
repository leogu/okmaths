﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// vcSceneAdmin
struct vcSceneAdmin_t405751812;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void vcSceneAdmin::.ctor()
extern "C"  void vcSceneAdmin__ctor_m3991818791 (vcSceneAdmin_t405751812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcSceneAdmin::LoadScene(System.String)
extern "C"  void vcSceneAdmin_LoadScene_m627192365 (vcSceneAdmin_t405751812 * __this, String_t* ___sceneNameTemp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void vcSceneAdmin::LoadPreviousScene()
extern "C"  void vcSceneAdmin_LoadPreviousScene_m1322623068 (vcSceneAdmin_t405751812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
