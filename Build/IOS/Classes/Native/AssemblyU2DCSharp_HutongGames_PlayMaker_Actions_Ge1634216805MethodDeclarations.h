﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetFsmMaterial
struct GetFsmMaterial_t1634216805;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::.ctor()
extern "C"  void GetFsmMaterial__ctor_m3412347175 (GetFsmMaterial_t1634216805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::Reset()
extern "C"  void GetFsmMaterial_Reset_m1687922134 (GetFsmMaterial_t1634216805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::OnEnter()
extern "C"  void GetFsmMaterial_OnEnter_m3460192464 (GetFsmMaterial_t1634216805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::OnUpdate()
extern "C"  void GetFsmMaterial_OnUpdate_m1855634487 (GetFsmMaterial_t1634216805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetFsmMaterial::DoGetFsmVariable()
extern "C"  void GetFsmMaterial_DoGetFsmVariable_m1680526290 (GetFsmMaterial_t1634216805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
