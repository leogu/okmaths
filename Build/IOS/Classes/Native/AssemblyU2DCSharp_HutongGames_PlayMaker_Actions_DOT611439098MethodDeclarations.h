﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenCameraOrthoSize
struct DOTweenCameraOrthoSize_t611439098;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraOrthoSize::.ctor()
extern "C"  void DOTweenCameraOrthoSize__ctor_m3074561742 (DOTweenCameraOrthoSize_t611439098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraOrthoSize::Reset()
extern "C"  void DOTweenCameraOrthoSize_Reset_m2891669855 (DOTweenCameraOrthoSize_t611439098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraOrthoSize::OnEnter()
extern "C"  void DOTweenCameraOrthoSize_OnEnter_m142186975 (DOTweenCameraOrthoSize_t611439098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraOrthoSize::<OnEnter>m__2C()
extern "C"  void DOTweenCameraOrthoSize_U3COnEnterU3Em__2C_m2487806465 (DOTweenCameraOrthoSize_t611439098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenCameraOrthoSize::<OnEnter>m__2D()
extern "C"  void DOTweenCameraOrthoSize_U3COnEnterU3Em__2D_m3186712424 (DOTweenCameraOrthoSize_t611439098 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
