﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.CallStaticMethod
struct CallStaticMethod_t3607574037;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.CallStaticMethod::.ctor()
extern "C"  void CallStaticMethod__ctor_m2380419447 (CallStaticMethod_t3607574037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallStaticMethod::OnEnter()
extern "C"  void CallStaticMethod_OnEnter_m3952761872 (CallStaticMethod_t3607574037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallStaticMethod::OnUpdate()
extern "C"  void CallStaticMethod_OnUpdate_m1974455431 (CallStaticMethod_t3607574037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.CallStaticMethod::DoMethodCall()
extern "C"  void CallStaticMethod_DoMethodCall_m1800572193 (CallStaticMethod_t3607574037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Actions.CallStaticMethod::DoCache()
extern "C"  bool CallStaticMethod_DoCache_m3120949658 (CallStaticMethod_t3607574037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.CallStaticMethod::ErrorCheck()
extern "C"  String_t* CallStaticMethod_ErrorCheck_m1817067474 (CallStaticMethod_t3607574037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
