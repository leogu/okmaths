﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Boolean HutongGames.Extensions.RectExtensions::Contains(UnityEngine.Rect,System.Single,System.Single)
extern "C"  bool RectExtensions_Contains_m3629674781 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.Extensions.RectExtensions::Contains(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectExtensions_Contains_m1940340200 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect10, Rect_t3681755626  ___rect21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.Extensions.RectExtensions::IntersectsWith(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool RectExtensions_IntersectsWith_m3450009545 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect10, Rect_t3681755626  ___rect21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::Union(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  Rect_t3681755626  RectExtensions_Union_m370676560 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect10, Rect_t3681755626  ___rect21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::Scale(UnityEngine.Rect,System.Single)
extern "C"  Rect_t3681755626  RectExtensions_Scale_m808317647 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, float ___scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::MinSize(UnityEngine.Rect,System.Single,System.Single)
extern "C"  Rect_t3681755626  RectExtensions_MinSize_m3953718161 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, float ___minWidth1, float ___minHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect HutongGames.Extensions.RectExtensions::MinSize(UnityEngine.Rect,UnityEngine.Vector2)
extern "C"  Rect_t3681755626  RectExtensions_MinSize_m2497816641 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___rect0, Vector2_t2243707579  ___minSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
