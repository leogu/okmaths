﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorFloat
struct SetAnimatorFloat_t1785639679;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::.ctor()
extern "C"  void SetAnimatorFloat__ctor_m1597439127 (SetAnimatorFloat_t1785639679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::Reset()
extern "C"  void SetAnimatorFloat_Reset_m2394164048 (SetAnimatorFloat_t1785639679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::OnEnter()
extern "C"  void SetAnimatorFloat_OnEnter_m1775862462 (SetAnimatorFloat_t1785639679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::OnActionUpdate()
extern "C"  void SetAnimatorFloat_OnActionUpdate_m800914117 (SetAnimatorFloat_t1785639679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorFloat::SetParameter()
extern "C"  void SetAnimatorFloat_SetParameter_m1272420540 (SetAnimatorFloat_t1785639679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
