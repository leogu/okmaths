﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn
struct uGuiToggleGetIsOn_t2261409707;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::.ctor()
extern "C"  void uGuiToggleGetIsOn__ctor_m675549675 (uGuiToggleGetIsOn_t2261409707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::Reset()
extern "C"  void uGuiToggleGetIsOn_Reset_m3132099112 (uGuiToggleGetIsOn_t2261409707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::OnEnter()
extern "C"  void uGuiToggleGetIsOn_OnEnter_m1582782678 (uGuiToggleGetIsOn_t2261409707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::OnUpdate()
extern "C"  void uGuiToggleGetIsOn_OnUpdate_m477875675 (uGuiToggleGetIsOn_t2261409707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiToggleGetIsOn::DoGetValue()
extern "C"  void uGuiToggleGetIsOn_DoGetValue_m3692411009 (uGuiToggleGetIsOn_t2261409707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
