﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerUGuiComponentProxy/FsmVariableSetup
struct FsmVariableSetup_t2450860713;
struct FsmVariableSetup_t2450860713_marshaled_pinvoke;
struct FsmVariableSetup_t2450860713_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct FsmVariableSetup_t2450860713;
struct FsmVariableSetup_t2450860713_marshaled_pinvoke;

extern "C" void FsmVariableSetup_t2450860713_marshal_pinvoke(const FsmVariableSetup_t2450860713& unmarshaled, FsmVariableSetup_t2450860713_marshaled_pinvoke& marshaled);
extern "C" void FsmVariableSetup_t2450860713_marshal_pinvoke_back(const FsmVariableSetup_t2450860713_marshaled_pinvoke& marshaled, FsmVariableSetup_t2450860713& unmarshaled);
extern "C" void FsmVariableSetup_t2450860713_marshal_pinvoke_cleanup(FsmVariableSetup_t2450860713_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct FsmVariableSetup_t2450860713;
struct FsmVariableSetup_t2450860713_marshaled_com;

extern "C" void FsmVariableSetup_t2450860713_marshal_com(const FsmVariableSetup_t2450860713& unmarshaled, FsmVariableSetup_t2450860713_marshaled_com& marshaled);
extern "C" void FsmVariableSetup_t2450860713_marshal_com_back(const FsmVariableSetup_t2450860713_marshaled_com& marshaled, FsmVariableSetup_t2450860713& unmarshaled);
extern "C" void FsmVariableSetup_t2450860713_marshal_com_cleanup(FsmVariableSetup_t2450860713_marshaled_com& marshaled);
