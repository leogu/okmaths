﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Trigger2dEvent
struct Trigger2dEvent_t1033357024;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"

// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::.ctor()
extern "C"  void Trigger2dEvent__ctor_m1513999414 (Trigger2dEvent_t1033357024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::Reset()
extern "C"  void Trigger2dEvent_Reset_m3474191533 (Trigger2dEvent_t1033357024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::OnPreprocess()
extern "C"  void Trigger2dEvent_OnPreprocess_m98945357 (Trigger2dEvent_t1033357024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::StoreCollisionInfo(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_StoreCollisionInfo_m143088356 (Trigger2dEvent_t1033357024 * __this, Collider2D_t646061738 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::DoTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_DoTriggerEnter2D_m1905535630 (Trigger2dEvent_t1033357024 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::DoTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_DoTriggerStay2D_m3738290001 (Trigger2dEvent_t1033357024 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Trigger2dEvent::DoTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void Trigger2dEvent_DoTriggerExit2D_m151371316 (Trigger2dEvent_t1033357024 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Actions.Trigger2dEvent::ErrorCheck()
extern "C"  String_t* Trigger2dEvent_ErrorCheck_m1393027251 (Trigger2dEvent_t1033357024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
