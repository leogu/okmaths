﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetCameraCullingMask
struct SetCameraCullingMask_t2337594823;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::.ctor()
extern "C"  void SetCameraCullingMask__ctor_m1181644727 (SetCameraCullingMask_t2337594823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::Reset()
extern "C"  void SetCameraCullingMask_Reset_m3789026160 (SetCameraCullingMask_t2337594823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::OnEnter()
extern "C"  void SetCameraCullingMask_OnEnter_m1888047558 (SetCameraCullingMask_t2337594823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::OnUpdate()
extern "C"  void SetCameraCullingMask_OnUpdate_m581745623 (SetCameraCullingMask_t2337594823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetCameraCullingMask::DoSetCameraCullingMask()
extern "C"  void SetCameraCullingMask_DoSetCameraCullingMask_m1426544513 (SetCameraCullingMask_t2337594823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
