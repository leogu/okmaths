﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect
struct RectTransformPixelAdjustRect_t1825828749;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::.ctor()
extern "C"  void RectTransformPixelAdjustRect__ctor_m2130717851 (RectTransformPixelAdjustRect_t1825828749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::Reset()
extern "C"  void RectTransformPixelAdjustRect_Reset_m172176690 (RectTransformPixelAdjustRect_t1825828749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::OnEnter()
extern "C"  void RectTransformPixelAdjustRect_OnEnter_m4005512644 (RectTransformPixelAdjustRect_t1825828749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::OnActionUpdate()
extern "C"  void RectTransformPixelAdjustRect_OnActionUpdate_m551011853 (RectTransformPixelAdjustRect_t1825828749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RectTransformPixelAdjustRect::DoAction()
extern "C"  void RectTransformPixelAdjustRect_DoAction_m1527089546 (RectTransformPixelAdjustRect_t1825828749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
