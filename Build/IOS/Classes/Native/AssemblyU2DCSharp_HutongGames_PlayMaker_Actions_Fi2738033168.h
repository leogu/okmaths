﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1918293222.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindArrayList
struct  FindArrayList_t2738033168  : public CollectionsActions_t1918293222
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindArrayList::ArrayListReference
	FsmString_t2414474701 * ___ArrayListReference_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.FindArrayList::store
	FsmGameObject_t3097142863 * ___store_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FindArrayList::foundEvent
	FsmEvent_t1258573736 * ___foundEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FindArrayList::notFoundEvent
	FsmEvent_t1258573736 * ___notFoundEvent_14;

public:
	inline static int32_t get_offset_of_ArrayListReference_11() { return static_cast<int32_t>(offsetof(FindArrayList_t2738033168, ___ArrayListReference_11)); }
	inline FsmString_t2414474701 * get_ArrayListReference_11() const { return ___ArrayListReference_11; }
	inline FsmString_t2414474701 ** get_address_of_ArrayListReference_11() { return &___ArrayListReference_11; }
	inline void set_ArrayListReference_11(FsmString_t2414474701 * value)
	{
		___ArrayListReference_11 = value;
		Il2CppCodeGenWriteBarrier(&___ArrayListReference_11, value);
	}

	inline static int32_t get_offset_of_store_12() { return static_cast<int32_t>(offsetof(FindArrayList_t2738033168, ___store_12)); }
	inline FsmGameObject_t3097142863 * get_store_12() const { return ___store_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_store_12() { return &___store_12; }
	inline void set_store_12(FsmGameObject_t3097142863 * value)
	{
		___store_12 = value;
		Il2CppCodeGenWriteBarrier(&___store_12, value);
	}

	inline static int32_t get_offset_of_foundEvent_13() { return static_cast<int32_t>(offsetof(FindArrayList_t2738033168, ___foundEvent_13)); }
	inline FsmEvent_t1258573736 * get_foundEvent_13() const { return ___foundEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_foundEvent_13() { return &___foundEvent_13; }
	inline void set_foundEvent_13(FsmEvent_t1258573736 * value)
	{
		___foundEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___foundEvent_13, value);
	}

	inline static int32_t get_offset_of_notFoundEvent_14() { return static_cast<int32_t>(offsetof(FindArrayList_t2738033168, ___notFoundEvent_14)); }
	inline FsmEvent_t1258573736 * get_notFoundEvent_14() const { return ___notFoundEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_notFoundEvent_14() { return &___notFoundEvent_14; }
	inline void set_notFoundEvent_14(FsmEvent_t1258573736 * value)
	{
		___notFoundEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___notFoundEvent_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
