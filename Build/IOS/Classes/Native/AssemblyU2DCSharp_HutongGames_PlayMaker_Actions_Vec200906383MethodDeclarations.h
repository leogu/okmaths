﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3HighPassFilter
struct Vector3HighPassFilter_t200906383;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::.ctor()
extern "C"  void Vector3HighPassFilter__ctor_m3281657615 (Vector3HighPassFilter_t200906383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::Reset()
extern "C"  void Vector3HighPassFilter_Reset_m3997499268 (Vector3HighPassFilter_t200906383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::OnEnter()
extern "C"  void Vector3HighPassFilter_OnEnter_m2565486394 (Vector3HighPassFilter_t200906383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3HighPassFilter::OnUpdate()
extern "C"  void Vector3HighPassFilter_OnUpdate_m3311190839 (Vector3HighPassFilter_t200906383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
