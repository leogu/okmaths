﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax
struct  uGuiSliderGetMinMax_t2990253931  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::minValue
	FsmFloat_t937133978 * ___minValue_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::maxValue
	FsmFloat_t937133978 * ___maxValue_13;
	// UnityEngine.UI.Slider HutongGames.PlayMaker.Actions.uGuiSliderGetMinMax::_slider
	Slider_t297367283 * ____slider_14;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiSliderGetMinMax_t2990253931, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_minValue_12() { return static_cast<int32_t>(offsetof(uGuiSliderGetMinMax_t2990253931, ___minValue_12)); }
	inline FsmFloat_t937133978 * get_minValue_12() const { return ___minValue_12; }
	inline FsmFloat_t937133978 ** get_address_of_minValue_12() { return &___minValue_12; }
	inline void set_minValue_12(FsmFloat_t937133978 * value)
	{
		___minValue_12 = value;
		Il2CppCodeGenWriteBarrier(&___minValue_12, value);
	}

	inline static int32_t get_offset_of_maxValue_13() { return static_cast<int32_t>(offsetof(uGuiSliderGetMinMax_t2990253931, ___maxValue_13)); }
	inline FsmFloat_t937133978 * get_maxValue_13() const { return ___maxValue_13; }
	inline FsmFloat_t937133978 ** get_address_of_maxValue_13() { return &___maxValue_13; }
	inline void set_maxValue_13(FsmFloat_t937133978 * value)
	{
		___maxValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___maxValue_13, value);
	}

	inline static int32_t get_offset_of__slider_14() { return static_cast<int32_t>(offsetof(uGuiSliderGetMinMax_t2990253931, ____slider_14)); }
	inline Slider_t297367283 * get__slider_14() const { return ____slider_14; }
	inline Slider_t297367283 ** get_address_of__slider_14() { return &____slider_14; }
	inline void set__slider_14(Slider_t297367283 * value)
	{
		____slider_14 = value;
		Il2CppCodeGenWriteBarrier(&____slider_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
