﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralVector3
struct SetProceduralVector3_t3382612699;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::.ctor()
extern "C"  void SetProceduralVector3__ctor_m3951491139 (SetProceduralVector3_t3382612699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::Reset()
extern "C"  void SetProceduralVector3_Reset_m2268226692 (SetProceduralVector3_t3382612699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::OnEnter()
extern "C"  void SetProceduralVector3_OnEnter_m1234453786 (SetProceduralVector3_t3382612699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::OnUpdate()
extern "C"  void SetProceduralVector3_OnUpdate_m2829201283 (SetProceduralVector3_t3382612699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralVector3::DoSetProceduralVector()
extern "C"  void SetProceduralVector3_DoSetProceduralVector_m906161404 (SetProceduralVector3_t3382612699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
