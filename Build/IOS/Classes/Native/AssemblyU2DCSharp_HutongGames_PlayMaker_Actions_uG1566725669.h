﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha
struct  uGuiCanvasGroupSetAlpha_t1566725669  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::alpha
	FsmFloat_t937133978 * ___alpha_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::resetOnExit
	FsmBool_t664485696 * ___resetOnExit_13;
	// System.Boolean HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::everyFrame
	bool ___everyFrame_14;
	// UnityEngine.CanvasGroup HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::_component
	CanvasGroup_t3296560743 * ____component_15;
	// System.Single HutongGames.PlayMaker.Actions.uGuiCanvasGroupSetAlpha::_originalValue
	float ____originalValue_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetAlpha_t1566725669, ___gameObject_11)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_alpha_12() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetAlpha_t1566725669, ___alpha_12)); }
	inline FsmFloat_t937133978 * get_alpha_12() const { return ___alpha_12; }
	inline FsmFloat_t937133978 ** get_address_of_alpha_12() { return &___alpha_12; }
	inline void set_alpha_12(FsmFloat_t937133978 * value)
	{
		___alpha_12 = value;
		Il2CppCodeGenWriteBarrier(&___alpha_12, value);
	}

	inline static int32_t get_offset_of_resetOnExit_13() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetAlpha_t1566725669, ___resetOnExit_13)); }
	inline FsmBool_t664485696 * get_resetOnExit_13() const { return ___resetOnExit_13; }
	inline FsmBool_t664485696 ** get_address_of_resetOnExit_13() { return &___resetOnExit_13; }
	inline void set_resetOnExit_13(FsmBool_t664485696 * value)
	{
		___resetOnExit_13 = value;
		Il2CppCodeGenWriteBarrier(&___resetOnExit_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetAlpha_t1566725669, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__component_15() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetAlpha_t1566725669, ____component_15)); }
	inline CanvasGroup_t3296560743 * get__component_15() const { return ____component_15; }
	inline CanvasGroup_t3296560743 ** get_address_of__component_15() { return &____component_15; }
	inline void set__component_15(CanvasGroup_t3296560743 * value)
	{
		____component_15 = value;
		Il2CppCodeGenWriteBarrier(&____component_15, value);
	}

	inline static int32_t get_offset_of__originalValue_16() { return static_cast<int32_t>(offsetof(uGuiCanvasGroupSetAlpha_t1566725669, ____originalValue_16)); }
	inline float get__originalValue_16() const { return ____originalValue_16; }
	inline float* get_address_of__originalValue_16() { return &____originalValue_16; }
	inline void set__originalValue_16(float value)
	{
		____originalValue_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
