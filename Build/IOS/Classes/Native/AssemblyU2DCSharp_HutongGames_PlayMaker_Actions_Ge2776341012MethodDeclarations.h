﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetRaycastHitInfo
struct GetRaycastHitInfo_t2776341012;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::.ctor()
extern "C"  void GetRaycastHitInfo__ctor_m1041351306 (GetRaycastHitInfo_t2776341012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::Reset()
extern "C"  void GetRaycastHitInfo_Reset_m1462456205 (GetRaycastHitInfo_t2776341012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::StoreRaycastInfo()
extern "C"  void GetRaycastHitInfo_StoreRaycastInfo_m1504912320 (GetRaycastHitInfo_t2776341012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::OnEnter()
extern "C"  void GetRaycastHitInfo_OnEnter_m1000139837 (GetRaycastHitInfo_t2776341012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetRaycastHitInfo::OnUpdate()
extern "C"  void GetRaycastHitInfo_OnUpdate_m631055748 (GetRaycastHitInfo_t2776341012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
