﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUIAction
struct GUIAction_t1632223173;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUIAction::.ctor()
extern "C"  void GUIAction__ctor_m3559351739 (GUIAction_t1632223173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIAction::Reset()
extern "C"  void GUIAction_Reset_m1786705382 (GUIAction_t1632223173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUIAction::OnGUI()
extern "C"  void GUIAction_OnGUI_m3167250513 (GUIAction_t1632223173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
