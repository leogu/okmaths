﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldOnValueChangeEvent
struct uGuiInputFieldOnValueChangeEvent_t3545086092;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnValueChangeEvent::.ctor()
extern "C"  void uGuiInputFieldOnValueChangeEvent__ctor_m3547775356 (uGuiInputFieldOnValueChangeEvent_t3545086092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnValueChangeEvent::Reset()
extern "C"  void uGuiInputFieldOnValueChangeEvent_Reset_m3361141237 (uGuiInputFieldOnValueChangeEvent_t3545086092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnValueChangeEvent::OnEnter()
extern "C"  void uGuiInputFieldOnValueChangeEvent_OnEnter_m3793225221 (uGuiInputFieldOnValueChangeEvent_t3545086092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnValueChangeEvent::OnExit()
extern "C"  void uGuiInputFieldOnValueChangeEvent_OnExit_m223388513 (uGuiInputFieldOnValueChangeEvent_t3545086092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldOnValueChangeEvent::DoOnValueChange(System.String)
extern "C"  void uGuiInputFieldOnValueChangeEvent_DoOnValueChange_m160959429 (uGuiInputFieldOnValueChangeEvent_t3545086092 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
