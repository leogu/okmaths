﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVertexPosition
struct GetVertexPosition_t2958953717;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::.ctor()
extern "C"  void GetVertexPosition__ctor_m634944385 (GetVertexPosition_t2958953717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::Reset()
extern "C"  void GetVertexPosition_Reset_m322731734 (GetVertexPosition_t2958953717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::OnEnter()
extern "C"  void GetVertexPosition_OnEnter_m240310996 (GetVertexPosition_t2958953717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::OnUpdate()
extern "C"  void GetVertexPosition_OnUpdate_m2914462317 (GetVertexPosition_t2958953717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVertexPosition::DoGetVertexPosition()
extern "C"  void GetVertexPosition_DoGetVertexPosition_m1415209329 (GetVertexPosition_t2958953717 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
