﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldGetText
struct uGuiInputFieldGetText_t1210873929;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetText::.ctor()
extern "C"  void uGuiInputFieldGetText__ctor_m2375146199 (uGuiInputFieldGetText_t1210873929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetText::Reset()
extern "C"  void uGuiInputFieldGetText_Reset_m1103269962 (uGuiInputFieldGetText_t1210873929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetText::OnEnter()
extern "C"  void uGuiInputFieldGetText_OnEnter_m3573115412 (uGuiInputFieldGetText_t1210873929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetText::OnUpdate()
extern "C"  void uGuiInputFieldGetText_OnUpdate_m166999439 (uGuiInputFieldGetText_t1210873929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldGetText::DoGetTextValue()
extern "C"  void uGuiInputFieldGetText_DoGetTextValue_m3368399752 (uGuiInputFieldGetText_t1210873929 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
