﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector2Lerp
struct Vector2Lerp_t2764169142;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::.ctor()
extern "C"  void Vector2Lerp__ctor_m1171939560 (Vector2Lerp_t2764169142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::Reset()
extern "C"  void Vector2Lerp_Reset_m2749751275 (Vector2Lerp_t2764169142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::OnEnter()
extern "C"  void Vector2Lerp_OnEnter_m581058715 (Vector2Lerp_t2764169142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::OnUpdate()
extern "C"  void Vector2Lerp_OnUpdate_m224600166 (Vector2Lerp_t2764169142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector2Lerp::DoVector2Lerp()
extern "C"  void Vector2Lerp_DoVector2Lerp_m4225937193 (Vector2Lerp_t2764169142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
