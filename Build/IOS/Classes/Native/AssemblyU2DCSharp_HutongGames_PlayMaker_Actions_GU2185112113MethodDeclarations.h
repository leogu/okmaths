﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GUILayoutEndArea
struct GUILayoutEndArea_t2185112113;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::.ctor()
extern "C"  void GUILayoutEndArea__ctor_m2393656059 (GUILayoutEndArea_t2185112113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::Reset()
extern "C"  void GUILayoutEndArea_Reset_m2573529666 (GUILayoutEndArea_t2185112113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GUILayoutEndArea::OnGUI()
extern "C"  void GUILayoutEndArea_OnGUI_m3082105685 (GUILayoutEndArea_t2185112113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
