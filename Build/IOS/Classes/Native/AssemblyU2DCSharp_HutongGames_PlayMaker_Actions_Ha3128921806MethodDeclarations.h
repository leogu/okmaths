﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableExists
struct HashTableExists_t3128921806;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableExists::.ctor()
extern "C"  void HashTableExists__ctor_m3620693926 (HashTableExists_t3128921806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableExists::Reset()
extern "C"  void HashTableExists_Reset_m1567753131 (HashTableExists_t3128921806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableExists::OnEnter()
extern "C"  void HashTableExists_OnEnter_m865261451 (HashTableExists_t3128921806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
