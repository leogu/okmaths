﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.NetworkInstantiate
struct NetworkInstantiate_t233689862;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.NetworkInstantiate::.ctor()
extern "C"  void NetworkInstantiate__ctor_m3000830510 (NetworkInstantiate_t233689862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInstantiate::Reset()
extern "C"  void NetworkInstantiate_Reset_m150642575 (NetworkInstantiate_t233689862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.NetworkInstantiate::OnEnter()
extern "C"  void NetworkInstantiate_OnEnter_m3077076991 (NetworkInstantiate_t233689862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
