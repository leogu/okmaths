﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableConcat
struct HashTableConcat_t3568209330;
// System.Collections.Hashtable
struct Hashtable_t909839986;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Hashtable909839986.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableConcat::.ctor()
extern "C"  void HashTableConcat__ctor_m2429567906 (HashTableConcat_t3568209330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableConcat::Reset()
extern "C"  void HashTableConcat_Reset_m567080239 (HashTableConcat_t3568209330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableConcat::OnEnter()
extern "C"  void HashTableConcat_OnEnter_m2306976879 (HashTableConcat_t3568209330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableConcat::DoHashTableConcat(System.Collections.Hashtable)
extern "C"  void HashTableConcat_DoHashTableConcat_m2007326353 (HashTableConcat_t3568209330 * __this, Hashtable_t909839986 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
