﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.HashTableSet
struct HashTableSet_t3369984408;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.HashTableSet::.ctor()
extern "C"  void HashTableSet__ctor_m2105085756 (HashTableSet_t3369984408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableSet::Reset()
extern "C"  void HashTableSet_Reset_m2936757821 (HashTableSet_t3369984408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableSet::OnEnter()
extern "C"  void HashTableSet_OnEnter_m2489568381 (HashTableSet_t3369984408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.HashTableSet::SetHashTable()
extern "C"  void HashTableSet_SetHashTable_m3215579814 (HashTableSet_t3369984408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
