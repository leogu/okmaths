﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetProceduralFloat
struct SetProceduralFloat_t3368219021;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::.ctor()
extern "C"  void SetProceduralFloat__ctor_m1910312137 (SetProceduralFloat_t3368219021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::Reset()
extern "C"  void SetProceduralFloat_Reset_m3876746074 (SetProceduralFloat_t3368219021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::OnEnter()
extern "C"  void SetProceduralFloat_OnEnter_m3637113560 (SetProceduralFloat_t3368219021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::OnUpdate()
extern "C"  void SetProceduralFloat_OnUpdate_m2406811157 (SetProceduralFloat_t3368219021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetProceduralFloat::DoSetProceduralFloat()
extern "C"  void SetProceduralFloat_DoSetProceduralFloat_m2977739457 (SetProceduralFloat_t3368219021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
