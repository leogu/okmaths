﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart
struct uGuiInputFieldMoveCaretToTextStart_t370720346;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::.ctor()
extern "C"  void uGuiInputFieldMoveCaretToTextStart__ctor_m2452502372 (uGuiInputFieldMoveCaretToTextStart_t370720346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::Reset()
extern "C"  void uGuiInputFieldMoveCaretToTextStart_Reset_m3284184931 (uGuiInputFieldMoveCaretToTextStart_t370720346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::OnEnter()
extern "C"  void uGuiInputFieldMoveCaretToTextStart_OnEnter_m66228123 (uGuiInputFieldMoveCaretToTextStart_t370720346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.uGuiInputFieldMoveCaretToTextStart::DoAction()
extern "C"  void uGuiInputFieldMoveCaretToTextStart_DoAction_m3864854603 (uGuiInputFieldMoveCaretToTextStart_t370720346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
