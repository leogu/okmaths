﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.EaseFsmAction
struct EaseFsmAction_t2453967042;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::.ctor()
extern "C"  void EaseFsmAction__ctor_m4079729460 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::Reset()
extern "C"  void EaseFsmAction_Reset_m1517853407 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnEnter()
extern "C"  void EaseFsmAction_OnEnter_m2115523751 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnExit()
extern "C"  void EaseFsmAction_OnExit_m3595990463 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::OnUpdate()
extern "C"  void EaseFsmAction_OnUpdate_m1907270586 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::UpdatePercentage()
extern "C"  void EaseFsmAction_UpdatePercentage_m3100246271 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.EaseFsmAction::SetEasingFunction()
extern "C"  void EaseFsmAction_SetEasingFunction_m2432569505 (EaseFsmAction_t2453967042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::linear(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_linear_m1799845618 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::clerp(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_clerp_m2449412817 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::spring(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_spring_m463803934 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuad_m932780705 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuad_m584924328 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuad(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuad_m1623503259 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInCubic_m2601173212 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutCubic_m3838762515 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutCubic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutCubic_m2737681544 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuart_m347907815 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuart_m1771853242 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuart(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuart_m2514586453 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInQuint_m16704539 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutQuint_m2710147630 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutQuint(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutQuint_m4101075305 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInSine(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInSine_m554538869 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutSine(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutSine_m1052186870 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutSine(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutSine_m3046781319 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInExpo(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInExpo_m2425983554 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutExpo(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutExpo_m3728048507 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutExpo(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutExpo_m1005694518 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInCirc(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInCirc_m3931918755 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutCirc(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutCirc_m3287744400 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutCirc(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutCirc_m2342958121 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::bounce(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_bounce_m1210398605 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInBack_m3828070643 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeOutBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeOutBack_m4292201472 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::easeInOutBack(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_easeInOutBack_m3355134873 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::punch(System.Single,System.Single)
extern "C"  float EaseFsmAction_punch_m2750793668 (EaseFsmAction_t2453967042 * __this, float ___amplitude0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.Actions.EaseFsmAction::elastic(System.Single,System.Single,System.Single)
extern "C"  float EaseFsmAction_elastic_m1554282298 (EaseFsmAction_t2453967042 * __this, float ___start0, float ___end1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
