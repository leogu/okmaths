﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.Vector3Normalize
struct Vector3Normalize_t314901501;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.Vector3Normalize::.ctor()
extern "C"  void Vector3Normalize__ctor_m104706529 (Vector3Normalize_t314901501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Normalize::Reset()
extern "C"  void Vector3Normalize_Reset_m2717766466 (Vector3Normalize_t314901501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Normalize::OnEnter()
extern "C"  void Vector3Normalize_OnEnter_m944799480 (Vector3Normalize_t314901501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.Vector3Normalize::OnUpdate()
extern "C"  void Vector3Normalize_OnUpdate_m2471060613 (Vector3Normalize_t314901501 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
