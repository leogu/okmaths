﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.RandomWait
struct RandomWait_t3965164550;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.RandomWait::.ctor()
extern "C"  void RandomWait__ctor_m3890711002 (RandomWait_t3965164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomWait::Reset()
extern "C"  void RandomWait_Reset_m3059257235 (RandomWait_t3965164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomWait::OnEnter()
extern "C"  void RandomWait_OnEnter_m1320284203 (RandomWait_t3965164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.RandomWait::OnUpdate()
extern "C"  void RandomWait_OnUpdate_m2783006108 (RandomWait_t3965164550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
