﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.SetAnimatorIKGoal
struct SetAnimatorIKGoal_t3328366004;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::.ctor()
extern "C"  void SetAnimatorIKGoal__ctor_m407827328 (SetAnimatorIKGoal_t3328366004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::Reset()
extern "C"  void SetAnimatorIKGoal_Reset_m1089851381 (SetAnimatorIKGoal_t3328366004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::OnPreprocess()
extern "C"  void SetAnimatorIKGoal_OnPreprocess_m2729143181 (SetAnimatorIKGoal_t3328366004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::OnEnter()
extern "C"  void SetAnimatorIKGoal_OnEnter_m1808082869 (SetAnimatorIKGoal_t3328366004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::DoAnimatorIK(System.Int32)
extern "C"  void SetAnimatorIKGoal_DoAnimatorIK_m2575478511 (SetAnimatorIKGoal_t3328366004 * __this, int32_t ___layerIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.SetAnimatorIKGoal::DoSetIKGoal()
extern "C"  void SetAnimatorIKGoal_DoSetIKGoal_m4274498532 (SetAnimatorIKGoal_t3328366004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
