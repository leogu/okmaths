﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.QuaternionInverse
struct QuaternionInverse_t2741901930;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::.ctor()
extern "C"  void QuaternionInverse__ctor_m2309253234 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::Reset()
extern "C"  void QuaternionInverse_Reset_m411607839 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnEnter()
extern "C"  void QuaternionInverse_OnEnter_m605417895 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnUpdate()
extern "C"  void QuaternionInverse_OnUpdate_m3469711388 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnLateUpdate()
extern "C"  void QuaternionInverse_OnLateUpdate_m1757895896 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::OnFixedUpdate()
extern "C"  void QuaternionInverse_OnFixedUpdate_m4275487882 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.QuaternionInverse::DoQuatInverse()
extern "C"  void QuaternionInverse_DoQuatInverse_m3645542056 (QuaternionInverse_t2741901930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
