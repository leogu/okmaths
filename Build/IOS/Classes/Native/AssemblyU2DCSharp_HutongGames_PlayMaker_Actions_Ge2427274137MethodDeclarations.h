﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetVector2Length
struct GetVector2Length_t2427274137;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::.ctor()
extern "C"  void GetVector2Length__ctor_m1907920883 (GetVector2Length_t2427274137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::Reset()
extern "C"  void GetVector2Length_Reset_m3871643338 (GetVector2Length_t2427274137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::OnEnter()
extern "C"  void GetVector2Length_OnEnter_m1245219860 (GetVector2Length_t2427274137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::OnUpdate()
extern "C"  void GetVector2Length_OnUpdate_m677534595 (GetVector2Length_t2427274137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetVector2Length::DoVectorLength()
extern "C"  void GetVector2Length_DoVectorLength_m3166827385 (GetVector2Length_t2427274137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
