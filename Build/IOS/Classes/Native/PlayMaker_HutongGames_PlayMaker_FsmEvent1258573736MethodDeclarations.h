﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerGlobals
struct PlayMakerGlobals_t2120229426;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>
struct List_1_t627694868;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "mscorlib_System_Object2689449295.h"

// PlayMakerGlobals HutongGames.PlayMaker.FsmEvent::get_GlobalsComponent()
extern "C"  PlayMakerGlobals_t2120229426 * FsmEvent_get_GlobalsComponent_m1254697890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> HutongGames.PlayMaker.FsmEvent::get_globalEvents()
extern "C"  List_1_t1398341365 * FsmEvent_get_globalEvents_m4294759679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent> HutongGames.PlayMaker.FsmEvent::get_EventList()
extern "C"  List_1_t627694868 * FsmEvent_get_EventList_m3178839921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_EventList(System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>)
extern "C"  void FsmEvent_set_EventList_m37241368 (Il2CppObject * __this /* static, unused */, List_1_t627694868 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::Initialize()
extern "C"  void FsmEvent_Initialize_m275671689 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEvent::get_Name()
extern "C"  String_t* FsmEvent_get_Name_m879964730 (FsmEvent_t1258573736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_Name(System.String)
extern "C"  void FsmEvent_set_Name_m1664456609 (FsmEvent_t1258573736 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsSystemEvent()
extern "C"  bool FsmEvent_get_IsSystemEvent_m369228735 (FsmEvent_t1258573736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_IsSystemEvent(System.Boolean)
extern "C"  void FsmEvent_set_IsSystemEvent_m3940477116 (FsmEvent_t1258573736 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsMouseEvent()
extern "C"  bool FsmEvent_get_IsMouseEvent_m145475447 (FsmEvent_t1258573736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsApplicationEvent()
extern "C"  bool FsmEvent_get_IsApplicationEvent_m1088187216 (FsmEvent_t1258573736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::get_IsGlobal()
extern "C"  bool FsmEvent_get_IsGlobal_m1271230613 (FsmEvent_t1258573736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_IsGlobal(System.Boolean)
extern "C"  void FsmEvent_set_IsGlobal_m2485871150 (FsmEvent_t1258573736 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::IsNullOrEmpty(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmEvent_IsNullOrEmpty_m848439894 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmEvent::get_Path()
extern "C"  String_t* FsmEvent_get_Path_m2909715246 (FsmEvent_t1258573736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_Path(System.String)
extern "C"  void FsmEvent_set_Path_m1986875495 (FsmEvent_t1258573736 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::.ctor(System.String)
extern "C"  void FsmEvent__ctor_m679476699 (FsmEvent_t1258573736 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::.ctor(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent__ctor_m1391014107 (FsmEvent_t1258573736 * __this, FsmEvent_t1258573736 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmEvent::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t FsmEvent_System_IComparable_CompareTo_m787801997 (FsmEvent_t1258573736 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::EventListContainsEvent(System.Collections.Generic.List`1<HutongGames.PlayMaker.FsmEvent>,System.String)
extern "C"  bool FsmEvent_EventListContainsEvent_m1193153274 (Il2CppObject * __this /* static, unused */, List_1_t627694868 * ___fsmEventList0, String_t* ___fsmEventName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::RemoveEventFromEventList(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_RemoveEventFromEventList_m2331110253 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::FindEvent(System.String)
extern "C"  FsmEvent_t1258573736 * FsmEvent_FindEvent_m131869239 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::IsEventGlobal(System.String)
extern "C"  bool FsmEvent_IsEventGlobal_m3246594678 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::EventListContains(System.String)
extern "C"  bool FsmEvent_EventListContains_m3584520596 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::GetFsmEvent(System.String)
extern "C"  FsmEvent_t1258573736 * FsmEvent_GetFsmEvent_m128779372 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::GetFsmEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  FsmEvent_t1258573736 * FsmEvent_GetFsmEvent_m1108875254 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::AddFsmEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  FsmEvent_t1258573736 * FsmEvent_AddFsmEvent_m774905507 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::AddSystemEvents()
extern "C"  void FsmEvent_AddSystemEvents_m1360968766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::AddSystemEvent(System.String,System.String)
extern "C"  FsmEvent_t1258573736 * FsmEvent_AddSystemEvent_m2457652618 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___path1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameInvisible()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_BecameInvisible_m877332285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_BecameInvisible(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_BecameInvisible_m746761676 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameVisible()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_BecameVisible_m1001535112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_BecameVisible(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_BecameVisible_m1019978449 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionEnter()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_CollisionEnter_m206594687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionEnter(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionEnter_m300723194 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionExit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_CollisionExit_m2943873583 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionExit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionExit_m3124253170 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionStay()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_CollisionStay_m2196124626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionStay(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionStay_m118233359 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionEnter2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_CollisionEnter2D_m3456832661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionEnter2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionEnter2D_m2724466400 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionExit2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_CollisionExit2D_m507353293 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionExit2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionExit2D_m724551568 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_CollisionStay2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_CollisionStay2D_m4006817196 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_CollisionStay2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_CollisionStay2D_m2130365401 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ControllerColliderHit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ControllerColliderHit_m710544222 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ControllerColliderHit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ControllerColliderHit_m3499424477 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_Finished()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_Finished_m1211987123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_Finished(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_Finished_m3969660982 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_LevelLoaded()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_LevelLoaded_m3549150084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_LevelLoaded(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_LevelLoaded_m3887668051 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDown()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseDown_m2455071712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseDown(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseDown_m797360981 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDrag()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseDrag_m3004494826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseDrag(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseDrag_m3513348759 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseEnter()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseEnter_m3676296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseEnter(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseEnter_m2373960307 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseExit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseExit_m2613472564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseExit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseExit_m2052521331 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseOver()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseOver_m4010778446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseOver(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseOver_m4161725581 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUp()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseUp_m2558660009 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseUp(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseUp_m1598930916 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUpAsButton()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseUpAsButton_m2531723093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MouseUpAsButton(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MouseUpAsButton_m135035448 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerEnter()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_TriggerEnter_m3736779069 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerEnter(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerEnter_m646711746 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerExit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_TriggerExit_m2373146537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerExit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerExit_m3156502754 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerStay()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_TriggerStay_m3494073766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerStay(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerStay_m3076316901 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerEnter2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_TriggerEnter2D_m2462869083 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerEnter2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerEnter2D_m3766275904 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerExit2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_TriggerExit2D_m2470785283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerExit2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerExit2D_m1252692396 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_TriggerStay2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_TriggerStay2D_m918405764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_TriggerStay2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_TriggerStay2D_m1467416259 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationFocus()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ApplicationFocus_m209182743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ApplicationFocus(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ApplicationFocus_m514724486 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationPause()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ApplicationPause_m3384135749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ApplicationPause(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ApplicationPause_m1259203792 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationQuit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ApplicationQuit_m409540578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ApplicationQuit(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ApplicationQuit_m47088589 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ParticleCollision()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ParticleCollision_m716454959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ParticleCollision(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ParticleCollision_m3083925780 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_JointBreak()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_JointBreak_m2787785396 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_JointBreak(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_JointBreak_m1991060171 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_JointBreak2D()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_JointBreak2D_m3496038870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_JointBreak2D(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_JointBreak2D_m222026253 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_PlayerConnected()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_PlayerConnected_m1570007781 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_PlayerConnected(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_PlayerConnected_m1792190022 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ServerInitialized()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ServerInitialized_m558810168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ServerInitialized(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ServerInitialized_m734257387 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ConnectedToServer()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ConnectedToServer_m2901903686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_ConnectedToServer(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_ConnectedToServer_m1929059351 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_PlayerDisconnected()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_PlayerDisconnected_m1895460251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_PlayerDisconnected(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_PlayerDisconnected_m2764399570 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_DisconnectedFromServer()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_DisconnectedFromServer_m700824159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_DisconnectedFromServer(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_DisconnectedFromServer_m3412262716 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_FailedToConnect()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_FailedToConnect_m1429252691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_FailedToConnect(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_FailedToConnect_m1532696480 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_FailedToConnectToMasterServer()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_FailedToConnectToMasterServer_m678915213 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_FailedToConnectToMasterServer(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_FailedToConnectToMasterServer_m1247529964 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MasterServerEvent()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MasterServerEvent_m2238530620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_MasterServerEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_MasterServerEvent_m3282727895 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_NetworkInstantiate()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_NetworkInstantiate_m2294396313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_NetworkInstantiate(HutongGames.PlayMaker.FsmEvent)
extern "C"  void FsmEvent_set_NetworkInstantiate_m574624118 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::AddGlobalEvents()
extern "C"  void FsmEvent_AddGlobalEvents_m4248314180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::SanityCheckEventList()
extern "C"  void FsmEvent_SanityCheckEventList_m4140239969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::<set_IsGlobal>b__0(System.String)
extern "C"  bool FsmEvent_U3Cset_IsGlobalU3Eb__0_m3627802959 (FsmEvent_t1258573736 * __this, String_t* ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmEvent::<.ctor>b__2(HutongGames.PlayMaker.FsmEvent)
extern "C"  bool FsmEvent_U3C_ctorU3Eb__2_m2888393089 (FsmEvent_t1258573736 * __this, FsmEvent_t1258573736 * ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::.cctor()
extern "C"  void FsmEvent__cctor_m3470619088 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
