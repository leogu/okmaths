﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos3D
struct DOTweenRectTransformAnchorPos3D_t176982722;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos3D::.ctor()
extern "C"  void DOTweenRectTransformAnchorPos3D__ctor_m129243890 (DOTweenRectTransformAnchorPos3D_t176982722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos3D::Reset()
extern "C"  void DOTweenRectTransformAnchorPos3D_Reset_m2585792831 (DOTweenRectTransformAnchorPos3D_t176982722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos3D::OnEnter()
extern "C"  void DOTweenRectTransformAnchorPos3D_OnEnter_m734901071 (DOTweenRectTransformAnchorPos3D_t176982722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos3D::<OnEnter>m__6A()
extern "C"  void DOTweenRectTransformAnchorPos3D_U3COnEnterU3Em__6A_m1304083315 (DOTweenRectTransformAnchorPos3D_t176982722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRectTransformAnchorPos3D::<OnEnter>m__6B()
extern "C"  void DOTweenRectTransformAnchorPos3D_U3COnEnterU3Em__6B_m1304083154 (DOTweenRectTransformAnchorPos3D_t176982722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
