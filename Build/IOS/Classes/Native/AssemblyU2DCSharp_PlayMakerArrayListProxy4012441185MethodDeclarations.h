﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayMakerArrayListProxy
struct PlayMakerArrayListProxy_t4012441185;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.ICollection
struct ICollection_t91669223;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PlayMakerArrayListProxy::.ctor()
extern "C"  void PlayMakerArrayListProxy__ctor_m3171887348 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList PlayMakerArrayListProxy::get_arrayList()
extern "C"  ArrayList_t4252133567 * PlayMakerArrayListProxy_get_arrayList_m3811394782 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::Awake()
extern "C"  void PlayMakerArrayListProxy_Awake_m1540169437 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerArrayListProxy::isCollectionDefined()
extern "C"  bool PlayMakerArrayListProxy_isCollectionDefined_m4041698121 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::TakeSnapShot()
extern "C"  void PlayMakerArrayListProxy_TakeSnapShot_m3921440247 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::RevertToSnapShot()
extern "C"  void PlayMakerArrayListProxy_RevertToSnapShot_m1839824633 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::Add(System.Object,System.String,System.Boolean)
extern "C"  void PlayMakerArrayListProxy_Add_m4082487156 (PlayMakerArrayListProxy_t4012441185 * __this, Il2CppObject * ___value0, String_t* ___type1, bool ___silent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayMakerArrayListProxy::AddRange(System.Collections.ICollection,System.String)
extern "C"  int32_t PlayMakerArrayListProxy_AddRange_m1942220769 (PlayMakerArrayListProxy_t4012441185 * __this, Il2CppObject * ___collection0, String_t* ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::InspectorEdit(System.Int32)
extern "C"  void PlayMakerArrayListProxy_InspectorEdit_m3238507420 (PlayMakerArrayListProxy_t4012441185 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::Set(System.Int32,System.Object,System.String)
extern "C"  void PlayMakerArrayListProxy_Set_m390500019 (PlayMakerArrayListProxy_t4012441185 * __this, int32_t ___index0, Il2CppObject * ___value1, String_t* ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerArrayListProxy::Remove(System.Object,System.String,System.Boolean)
extern "C"  bool PlayMakerArrayListProxy_Remove_m2162447229 (PlayMakerArrayListProxy_t4012441185 * __this, Il2CppObject * ___value0, String_t* ___type1, bool ___silent2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerArrayListProxy::PreFillArrayList()
extern "C"  void PlayMakerArrayListProxy_PreFillArrayList_m3635118807 (PlayMakerArrayListProxy_t4012441185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
