﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// admob.AdPosition
struct  AdPosition_t1947228246  : public Il2CppObject
{
public:

public:
};

struct AdPosition_t1947228246_StaticFields
{
public:
	// System.Int32 admob.AdPosition::ABSOLUTE
	int32_t ___ABSOLUTE_0;
	// System.Int32 admob.AdPosition::TOP_LEFT
	int32_t ___TOP_LEFT_1;
	// System.Int32 admob.AdPosition::TOP_CENTER
	int32_t ___TOP_CENTER_2;
	// System.Int32 admob.AdPosition::TOP_RIGHT
	int32_t ___TOP_RIGHT_3;
	// System.Int32 admob.AdPosition::MIDDLE_LEFT
	int32_t ___MIDDLE_LEFT_4;
	// System.Int32 admob.AdPosition::MIDDLE_CENTER
	int32_t ___MIDDLE_CENTER_5;
	// System.Int32 admob.AdPosition::MIDDLE_RIGHT
	int32_t ___MIDDLE_RIGHT_6;
	// System.Int32 admob.AdPosition::BOTTOM_LEFT
	int32_t ___BOTTOM_LEFT_7;
	// System.Int32 admob.AdPosition::BOTTOM_CENTER
	int32_t ___BOTTOM_CENTER_8;
	// System.Int32 admob.AdPosition::BOTTOM_RIGHT
	int32_t ___BOTTOM_RIGHT_9;

public:
	inline static int32_t get_offset_of_ABSOLUTE_0() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___ABSOLUTE_0)); }
	inline int32_t get_ABSOLUTE_0() const { return ___ABSOLUTE_0; }
	inline int32_t* get_address_of_ABSOLUTE_0() { return &___ABSOLUTE_0; }
	inline void set_ABSOLUTE_0(int32_t value)
	{
		___ABSOLUTE_0 = value;
	}

	inline static int32_t get_offset_of_TOP_LEFT_1() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___TOP_LEFT_1)); }
	inline int32_t get_TOP_LEFT_1() const { return ___TOP_LEFT_1; }
	inline int32_t* get_address_of_TOP_LEFT_1() { return &___TOP_LEFT_1; }
	inline void set_TOP_LEFT_1(int32_t value)
	{
		___TOP_LEFT_1 = value;
	}

	inline static int32_t get_offset_of_TOP_CENTER_2() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___TOP_CENTER_2)); }
	inline int32_t get_TOP_CENTER_2() const { return ___TOP_CENTER_2; }
	inline int32_t* get_address_of_TOP_CENTER_2() { return &___TOP_CENTER_2; }
	inline void set_TOP_CENTER_2(int32_t value)
	{
		___TOP_CENTER_2 = value;
	}

	inline static int32_t get_offset_of_TOP_RIGHT_3() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___TOP_RIGHT_3)); }
	inline int32_t get_TOP_RIGHT_3() const { return ___TOP_RIGHT_3; }
	inline int32_t* get_address_of_TOP_RIGHT_3() { return &___TOP_RIGHT_3; }
	inline void set_TOP_RIGHT_3(int32_t value)
	{
		___TOP_RIGHT_3 = value;
	}

	inline static int32_t get_offset_of_MIDDLE_LEFT_4() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___MIDDLE_LEFT_4)); }
	inline int32_t get_MIDDLE_LEFT_4() const { return ___MIDDLE_LEFT_4; }
	inline int32_t* get_address_of_MIDDLE_LEFT_4() { return &___MIDDLE_LEFT_4; }
	inline void set_MIDDLE_LEFT_4(int32_t value)
	{
		___MIDDLE_LEFT_4 = value;
	}

	inline static int32_t get_offset_of_MIDDLE_CENTER_5() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___MIDDLE_CENTER_5)); }
	inline int32_t get_MIDDLE_CENTER_5() const { return ___MIDDLE_CENTER_5; }
	inline int32_t* get_address_of_MIDDLE_CENTER_5() { return &___MIDDLE_CENTER_5; }
	inline void set_MIDDLE_CENTER_5(int32_t value)
	{
		___MIDDLE_CENTER_5 = value;
	}

	inline static int32_t get_offset_of_MIDDLE_RIGHT_6() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___MIDDLE_RIGHT_6)); }
	inline int32_t get_MIDDLE_RIGHT_6() const { return ___MIDDLE_RIGHT_6; }
	inline int32_t* get_address_of_MIDDLE_RIGHT_6() { return &___MIDDLE_RIGHT_6; }
	inline void set_MIDDLE_RIGHT_6(int32_t value)
	{
		___MIDDLE_RIGHT_6 = value;
	}

	inline static int32_t get_offset_of_BOTTOM_LEFT_7() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___BOTTOM_LEFT_7)); }
	inline int32_t get_BOTTOM_LEFT_7() const { return ___BOTTOM_LEFT_7; }
	inline int32_t* get_address_of_BOTTOM_LEFT_7() { return &___BOTTOM_LEFT_7; }
	inline void set_BOTTOM_LEFT_7(int32_t value)
	{
		___BOTTOM_LEFT_7 = value;
	}

	inline static int32_t get_offset_of_BOTTOM_CENTER_8() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___BOTTOM_CENTER_8)); }
	inline int32_t get_BOTTOM_CENTER_8() const { return ___BOTTOM_CENTER_8; }
	inline int32_t* get_address_of_BOTTOM_CENTER_8() { return &___BOTTOM_CENTER_8; }
	inline void set_BOTTOM_CENTER_8(int32_t value)
	{
		___BOTTOM_CENTER_8 = value;
	}

	inline static int32_t get_offset_of_BOTTOM_RIGHT_9() { return static_cast<int32_t>(offsetof(AdPosition_t1947228246_StaticFields, ___BOTTOM_RIGHT_9)); }
	inline int32_t get_BOTTOM_RIGHT_9() const { return ___BOTTOM_RIGHT_9; }
	inline int32_t* get_address_of_BOTTOM_RIGHT_9() { return &___BOTTOM_RIGHT_9; }
	inline void set_BOTTOM_RIGHT_9(int32_t value)
	{
		___BOTTOM_RIGHT_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
