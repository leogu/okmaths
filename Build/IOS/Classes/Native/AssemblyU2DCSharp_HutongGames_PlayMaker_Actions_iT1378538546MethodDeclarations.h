﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.iTweenShakeScale
struct iTweenShakeScale_t1378538546;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::.ctor()
extern "C"  void iTweenShakeScale__ctor_m1009753826 (iTweenShakeScale_t1378538546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::Reset()
extern "C"  void iTweenShakeScale_Reset_m4236199707 (iTweenShakeScale_t1378538546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::OnEnter()
extern "C"  void iTweenShakeScale_OnEnter_m4202624427 (iTweenShakeScale_t1378538546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::OnExit()
extern "C"  void iTweenShakeScale_OnExit_m3749268187 (iTweenShakeScale_t1378538546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.iTweenShakeScale::DoiTween()
extern "C"  void iTweenShakeScale_DoiTween_m2723265493 (iTweenShakeScale_t1378538546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
