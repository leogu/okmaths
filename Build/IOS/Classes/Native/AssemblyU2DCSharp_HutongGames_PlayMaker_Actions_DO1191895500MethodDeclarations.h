﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.DOTweenRigidbodyMove
struct DOTweenRigidbodyMove_t1191895500;

#include "codegen/il2cpp-codegen.h"

// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMove::.ctor()
extern "C"  void DOTweenRigidbodyMove__ctor_m189000294 (DOTweenRigidbodyMove_t1191895500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMove::Reset()
extern "C"  void DOTweenRigidbodyMove_Reset_m3685885973 (DOTweenRigidbodyMove_t1191895500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMove::OnEnter()
extern "C"  void DOTweenRigidbodyMove_OnEnter_m2610800925 (DOTweenRigidbodyMove_t1191895500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMove::<OnEnter>m__88()
extern "C"  void DOTweenRigidbodyMove_U3COnEnterU3Em__88_m3863847142 (DOTweenRigidbodyMove_t1191895500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.DOTweenRigidbodyMove::<OnEnter>m__89()
extern "C"  void DOTweenRigidbodyMove_U3COnEnterU3Em__89_m1114760811 (DOTweenRigidbodyMove_t1191895500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
