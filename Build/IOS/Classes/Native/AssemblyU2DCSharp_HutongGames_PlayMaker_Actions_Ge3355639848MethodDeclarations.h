﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HutongGames.PlayMaker.Actions.GetChildNum
struct GetChildNum_t3355639848;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void HutongGames.PlayMaker.Actions.GetChildNum::.ctor()
extern "C"  void GetChildNum__ctor_m202944 (GetChildNum_t3355639848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::Reset()
extern "C"  void GetChildNum_Reset_m3970126013 (GetChildNum_t3355639848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Actions.GetChildNum::OnEnter()
extern "C"  void GetChildNum_OnEnter_m158164773 (GetChildNum_t3355639848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HutongGames.PlayMaker.Actions.GetChildNum::DoGetChildNum(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * GetChildNum_DoGetChildNum_m1697019226 (GetChildNum_t3355639848 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
