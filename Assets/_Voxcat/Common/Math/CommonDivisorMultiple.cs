﻿using UnityEngine;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{

	[ActionCategory("Voxcat")]
	[Tooltip("Get a Greatest common divisor and Least common multiple")]
	[HelpUrl("http://dotween.demigiant.com/documentation.php")]
	public class CommonDivisorMultiple : FsmStateAction
	{

		[RequiredField]
		public FsmInt integer1;

		[RequiredField]
		public FsmInt integer2;

		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmInt storeLC_Divisor;

		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmInt storeGC_Multiple;

		public bool everyFrame;

		public override void Reset()
		{
			integer1 = null;
			integer2 = null;
			storeLC_Divisor = null;
			storeGC_Multiple = null;
			everyFrame = false;
		}

		public override void OnEnter()
		{
			DoCal();

			if (!everyFrame)
				Finish();
		}

		// NOTE: very frame rate dependent!
		public override void OnUpdate()
		{
			DoCal();
		}


		void DoCal (){

			Debug.Log ("Start DoCal");

			int m = integer1.Value;
			int n = integer2.Value;
			int temp, a, b;

			if (m < n) {
				temp = n;
				m = n;
				n = temp;
			}

			a = m;
			b = n;
			while (b != 0) {
				Debug.Log ("a:" + a + "\nb:" +b + "\nn:" +n + "\nm:" + m);
				temp = a % b;
				a = b;
				b = temp;
			}

			storeLC_Divisor = +a;
			storeGC_Multiple = +n * m / a;
		}
	}
}