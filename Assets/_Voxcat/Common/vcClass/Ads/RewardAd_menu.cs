﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
//using Voxcat.SceneMan.Controller;
using UnityEngine.SceneManagement;

public class RewardAd_menu : MonoBehaviour {

	public string zoneId;

//	void OnGUI ()
//	{
//		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
//
//		Rect buttonRect = new Rect (10, 10, 150, 50);
//		string buttonText = Advertisement.IsReady (zoneId) ? "Show Ad" : "Waiting...";
//
//		ShowOptions options = new ShowOptions();
//		options.resultCallback = HandleShowResult;
//
//		if (GUI.Button (buttonRect, buttonText)) {
//			Advertisement.Show (zoneId, options);
//		}
//	}

	public void AdShow()
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;

		if (!Advertisement.IsReady ()) {
			Debug.Log ("Ads Not ready yet");
			return;
		}

		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;

		Advertisement.Show (zoneId, options);
	}

	private void HandleShowResult (ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			Debug.Log ("Video completed. User resurrect now");
			//SceneController.LoadLevel ("vcMain");
			SceneManager.LoadScene ("vcMain");
			break;
		case ShowResult.Skipped:
			Debug.LogWarning ("Video was skipped.");
			break;
		case ShowResult.Failed:
			Debug.LogError ("Video failed to show.");
			break;
		}
	}
}
