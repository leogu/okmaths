﻿using UnityEngine;
using System.Collections;
using Voxcat.SceneMan.Constants;

//Add following header files
using Voxcat.SceneMan.Controller;
using HutongGames.PlayMaker;


/*Example Class
* // Adding header files 
*/

public class vcSceneAdmin: MonoBehaviour
{
	// Your code 
	public void LoadScene(string sceneNameTemp){

	string sceneName = sceneNameTemp;
//	int LevelNo = int.Parse(sceneNameTemp.Substring(4,2));
//	FsmVariables.GlobalVariables.GetFsmInt("gCurrentLevel").Value = LevelNo;
	// SceneController.LoadLevel ("sceneName", x.xf); // if you want to put custom delay
	SceneController.LoadLevel (sceneName); // if you don't want to put any delay
	//	print ("Previous Scene name was : " + SceneController.previousScene);
	}


	//	// This will automatically load the previously loaded scene
	public void LoadPreviousScene ()
	{
		SceneController.LoadPreviousScene ();

		/*You may pass te Delay parameter for the previous scene's Loading scene 
		 *Example : SceneController.LoadPreviousScene(1.25f);
		 */

	}

 }

