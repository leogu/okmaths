﻿using UnityEngine;
using System.Collections;
using Voxcat.SceneMan.Constants;
using Voxcat.SceneMan.Controller;

public class LoadRequestedScene : MonoBehaviour
{
	private LoadingSceneController loadingSC = null;

	void Start ()
	{
		Invoke ("LoadRequestedScenee", Constants.LOADING_SCENE_WAIT_TIME);
	}

	private void LoadRequestedScenee ()
	{
		loadingSC = new LoadingSceneController ();
	}
}
