﻿using UnityEngine;
using System.Collections;

namespace Voxcat.SceneMan.Constants
{
	public class Constants
	{
		public const string LOADING_SCENE_NAME = "LoadingScene";
		public static float LOADING_SCENE_WAIT_TIME = 1.5f;
	}
}