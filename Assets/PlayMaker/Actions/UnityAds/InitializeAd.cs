using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("UnityAd")]
	[Tooltip("Initialize Unity Ads To Prepare For Usage, Only Required When Using The Unity Ad Plugin Downloaded From The Asset Store.")]
	public class InitializeAd : FsmStateAction
	{
		[Tooltip("IOS Game ID | Only Required For An IOS Game, Found In Your Unity Ad Dashboard")]
		public FsmString iosGameId;
		[Tooltip("Android Game ID | Only Required For An Android Game Found In Your Unity Ad Dashboard")]
		public FsmString androidGameId;
		[Tooltip("Enable Test Mode | Enable For Testing Purposes Only")]
		public FsmBool enableTestMode;

		public override void Reset()
		{
			iosGameId = "";
			androidGameId = "";
			enableTestMode = false;
		}

		public override void OnEnter()
		{
			if (!Advertisement.isSupported && iosGameId.Value == null || androidGameId.Value == null) return;
			string gameId = null;
			#if UNITY_IOS
			gameId = iosGameId.Value;
			#elif UNITY_ANDROID
			gameId = androidGameId.Value;
			#endif
			Advertisement.Initialize(gameId, enableTestMode.Value);
			Finish();
		}
	}
}