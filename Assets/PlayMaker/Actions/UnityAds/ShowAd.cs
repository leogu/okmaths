using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("UnityAd")]
	[Tooltip("Show Unity Ad")]
	public class ShowAd : FsmStateAction
	{
		[Tooltip("Integration Id | Found In Your Unity Ads Dashboard, This Can Be Left Blank And Will Automaticly Use The Default Setting")]
		public FsmString integrationId;
		[Tooltip("Finished | Triggers The Finished State, This Can Be Left Blank And Is Not Required")]
		public FsmEvent finished;
		[Tooltip("Skipped | Triggers The Skipped State, This Can Be Left Blank And Is Not Required")]
		public FsmEvent skipped;
		[Tooltip("Failed | Triggers The Failed State, This Can Be Left Blank And Is Not Required")]
		public FsmEvent failed;

		public override void Reset()
		{
			integrationId = "";
		}

		private void HandleShowResult (ShowResult result)
		{
			switch (result)
			{
			case ShowResult.Finished:
				Finish();
				if (finished != null)
				{
					Fsm.Event(finished);
				}
				break;
			case ShowResult.Skipped:
				Finish();
				if (skipped != null)
				{
					Fsm.Event(skipped);
				}
				break;
			case ShowResult.Failed:
				Finish();
				if (failed != null)
				{
					Fsm.Event(failed);
				}
				break;
			}
		}

		public override void OnEnter() {
			ShowOptions options = new ShowOptions();
			options.resultCallback = HandleShowResult;
			Advertisement.Show(integrationId.Value, options);
		}
	}
}