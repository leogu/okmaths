﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using HutongGames.PlayMaker;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory("UnityAd")]
	[Tooltip("Detect If Unity Ad Is Ready To Be Displayed")]
	public class IsAdReady : FsmStateAction
	{
		[Tooltip("Integration Id | Found In Your Unity Ads Dashboard, This Can Be Left Blank And Will Automaticly Use The Default Setting")]
		public FsmString integrationId;
		[Tooltip("Finished | Triggers The Finished State, This Can Be Left Blank And Is Not Required")]
		public FsmEvent finished;
		
		public override void Reset()
		{
			integrationId = "";
		}

		public override void OnUpdate()
		{
			if (Advertisement.IsReady(integrationId.Value)) {
				Finish();
				if (finished != null) {
					Fsm.Event(finished);
				}
			}
		}
	}
}